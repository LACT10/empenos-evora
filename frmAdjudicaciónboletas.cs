﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmAdjudicaciónboletas : Form
    {        
        public Controller.clsCtrlAdjudicacionBoleta ctrlAdjudicacionBoleta;
        public Controller.clsCtrlBoletaEmpeno ctrlBoletaEmpeno;
        public Controller.clsCtrlSucursalParametros ctrlSucursalParametros;
        public Controller.clsCtrlFolios ctrlFoliosAuto = new Controller.clsCtrlFolios();
        public Controller.clsCtrlProcesos ctrlProcesos;
        public Core.clsCoreView clsView;

        public int _intUlitmoCodigo;
        public decimal _decTotalAcum;

        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }
        public decimal decTotalAcum { get => _decTotalAcum; set => _decTotalAcum = value; }



        public List<Properties_Class.clsAdjudicacionBoletas> lsDeshacerAdjudicacionBoleta;

        public frmAdjudicaciónboletas()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();

            this.decTotalAcum = 0;
            this.ctrlAdjudicacionBoleta = new Controller.clsCtrlAdjudicacionBoleta();
            

            //Procesos id de la boleta de empeño
            this.ctrlProcesos = new Controller.clsCtrlProcesos();
            this.ctrlProcesos.BuscarDescripcion("Boletas de Empeño");

            this.ctrlBoletaEmpeno = new Controller.clsCtrlBoletaEmpeno();
            this.ctrlSucursalParametros = new Controller.clsCtrlSucursalParametros();

            this.lsDeshacerAdjudicacionBoleta = new List<Properties_Class.clsAdjudicacionBoletas>();
            this.lsDeshacerAdjudicacionBoleta.Add(this.ctrlAdjudicacionBoleta.adjudicacionBoletas);
        }


        private void frmAdjudicaciónboletas_Load(object sender, EventArgs e)
        {
            

          

            this.ctrlSucursalParametros.Buscar(this.clsView.login.intSucursalId.ToString());
            this.ctrlFoliosAuto.BuscarFolioProceso(this.clsView.strProcesosId, this.clsView.login.intSucursalId.ToString());
            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);

            tsbBuscar.Visible = false;
            btnUndo.Visible = false;

            dgvDetalles.Columns["Capital"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }


        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TsbEliminar_Click(object sender, EventArgs e)
        {

        }


        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlAdjudicacionBoleta.bolResutladoPaginacion)
            {
                this.clsView.principal.epMensaje.Clear();

            }
        }



        private void TsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();
            this.ctrlAdjudicacionBoleta.adjudicacionBoletas.limpiarProps();
           
            dgvDetalles.Rows.Clear();
            btnFooterGuardar.Visible = true;
            tsbGuardar.Visible = true;
            dtpFecha.Value = DateTime.Now;
            txtBoleta.Focus();


        }

        private void TxtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);

        }



        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            //txtCodigo.Enabled = true;
            //txtCodigo.Focus();
            txtTotales.Text = this.clsView.convertMoneda("0");
            dgvDetalles.Rows.Clear();
            dtpFecha.Value = DateTime.Now;
            this.decTotalAcum = 0;
            this.clsView.principal.epMensaje.Clear();

        }

        private bool validacion()
        {

            bool bolValidacion = true;
            //string strCodigo = txtCodigo.Text.TrimStart();            
            

            /*if (strCodigo == "")
            {
                txtCodigo.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigo, this.clsView.mensajeErrorValiForm2("El campo código es obligatorio, favor de ingresar un código"));
                bolValidacion = false;
            }*/

            if (dgvDetalles.Rows.Count == 0) 
            {
                dgvDetalles.Focus();
                this.clsView.principal.epMensaje.SetError(dgvDetalles, this.clsView.mensajeErrorValiForm2("Agregar al menos una boleta para esta adjudicación"));
                bolValidacion = false;
            }

            
            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();

            if (validacion())
            {
                                          
                this.ctrlAdjudicacionBoleta.adjudicacionBoletas.dtAjudicacionFecha = dtpFecha.Value;
                
                this.ctrlAdjudicacionBoleta.adjudicacionBoletas.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlAdjudicacionBoleta.adjudicacionBoletas.intSucursal = this.clsView.login.intSucursalId;

                this.guardarDetalles();

                this.ctrlAdjudicacionBoleta.Guardar();

                if (this.ctrlAdjudicacionBoleta.mdlAdjudicacionBoleta.bolResultado)
                {
                    this.limpiar_textbox();                    
                    this.clsView.mensajeExitoDB("adjudicación boleta", "guardó");                                      

                }
            }
        }

        public void llenar_formulario()
        {

            if (this.ctrlAdjudicacionBoleta.adjudicacionBoletas.strCodigo.Length == 0)
            {

                this.limpiar_textbox();
                return;
            }
            
            this.clsView.principal.epMensaje.Clear();       
            dtpFecha.Text = this.ctrlAdjudicacionBoleta.adjudicacionBoletas.dtAjudicacionFecha.ToString();
            this.intUlitmoCodigo = Int32.Parse(this.ctrlAdjudicacionBoleta.adjudicacionBoletas.strCodigo);            
            this.ctrlAdjudicacionBoleta.adjudicacionBoletas.intUsuario = this.clsView.login.usuario.intId;
           

            btnFooterGuardar.Visible = false;
            tsbGuardar.Visible = false;
        }

        public void guardarDetalles() 
        {
            this.ctrlAdjudicacionBoleta.adjudicacionBoletas.strQueryDetalles = "INSERT INTO adjudicacion_boletas_detalles(adjudicacion_boletas_id, renglon, boleta_empeno_id) VALUES";
            this.ctrlAdjudicacionBoleta.adjudicacionBoletas.strQueryBoletaEmpenoEstatus = " UPDATE boletas_empeno " +
                                                                       " SET estatus =  'ADJUDICADA'," +
                                                                       " fecha_adjudicacion =  '{0}', " +
                                                                       " usuario_adjudicacion = {1}" +                                                                                                                                              
                                                                       " WHERE boleta_empeno_id IN( {2} ); ";
            string strQueryDetalles = "";
            string strBoletasIds = "";
            int countDetalles = dgvDetalles.Rows.Count;
            int i = 0;
            int renglon = 1;
            bool bolUltimoDato = false;
            

            foreach (DataGridViewRow row in dgvDetalles.Rows) 
            {
                strBoletasIds += row.Cells["boleta_empeno_id"].Value.ToString() + ", ";
                if (i == countDetalles - 1)
                {

                    
                    strQueryDetalles += " ( @id," + renglon + ","  + row.Cells["boleta_empeno_id"].Value.ToString() + "); ";
                    bolUltimoDato = true;

                }
                else
                {                    
                    strQueryDetalles += " ( @id," + renglon + "," +  row.Cells["boleta_empeno_id"].Value.ToString() + ")|| ";
                }

                renglon++;
                
                i++;
            }



        
            if (bolUltimoDato)
            {
                strQueryDetalles =  strQueryDetalles.Replace("||", ",");
            }
            else
            {
                int intStrIndex = strQueryDetalles.LastIndexOf("||");
                strQueryDetalles = strQueryDetalles.Remove(intStrIndex).Insert(intStrIndex, ";").Replace("||", ",");
            }

            int intStrIndex3 = strBoletasIds.LastIndexOf(",");
            strBoletasIds = strBoletasIds.Remove(intStrIndex3).Insert(intStrIndex3, " ");

            this.ctrlAdjudicacionBoleta.adjudicacionBoletas.strQueryDetalles += strQueryDetalles;
            this.ctrlAdjudicacionBoleta.adjudicacionBoletas.strQueryBoletaEmpenoEstatus = String.Format(this.ctrlAdjudicacionBoleta.adjudicacionBoletas.strQueryBoletaEmpenoEstatus, dtpFecha.Value.ToString("yyyy-MM-dd HH:mm:ss") , this.clsView.login.usuario.intId, strBoletasIds);


        }




        public void agregarDeshacer()
        {
            this.lsDeshacerAdjudicacionBoleta.RemoveAt(0);
            this.lsDeshacerAdjudicacionBoleta.Add(this.ctrlAdjudicacionBoleta.adjudicacionBoletas);
        }

        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void TsbBuscar_Click(object sender, EventArgs e)
        {

        }

        private void txtBoleta_Leave(object sender, EventArgs e)
        {
            if (txtBoleta.Text == "") return;
            this.ctrlFoliosAuto.BuscarFolioProceso(this.ctrlProcesos.procesos.intId.ToString(), this.clsView.login.intSucursalId.ToString());
            if (txtBoleta.Text.Length < this.ctrlBoletaEmpeno.boletasEmpeno.intNumCerosConsecutivo)
            {
                if (this.ctrlFoliosAuto.folio.intConsecutivo == 0)
                {
                    this.clsView.mensajeErrorValiForm2("No es posible generar el folio, comuníquese con el administrador");
                    tsbGuardar.Enabled = false;
                    btnFooterGuardar.Enabled = false;
                }
                else
                {
                    txtBoleta.Text = this.ctrlFoliosAuto.folio.regresarFolioConsecutivoAuto(this.ctrlBoletaEmpeno.boletasEmpeno.intNumCerosConsecutivo, txtBoleta.Text);
                    this.ctrlBoletaEmpeno.BuscarActivo(txtBoleta.Text, this.clsView.login.intSucursalId.ToString());
                    if (this.ctrlBoletaEmpeno.mdlBoletaEmpeno.bolResultado)
                    {
                        DateTime dtVencimiento = this.ctrlBoletaEmpeno.boletasEmpeno.dtFechaVencimiento.AddDays(this.ctrlSucursalParametros.sucursalParametro.intDiasLiquidarBoletas + this.ctrlSucursalParametros.sucursalParametro.intDiasRefrendarBoletas);

                        if (dtVencimiento < dtpFecha.Value)
                        {
                            dgvDetalles.Rows.Add(
                            this.ctrlBoletaEmpeno.boletasEmpeno.intId, //boleta_id
                            this.ctrlBoletaEmpeno.boletasEmpeno.strFolio, //boleta
                            this.ctrlBoletaEmpeno.boletasEmpeno.cliente.strNombre,//cliente
                            this.ctrlBoletaEmpeno.boletasEmpeno.dtFechaBoleta.ToString("dd/MM/yyyy"),
                            this.ctrlBoletaEmpeno.boletasEmpeno.dtFechaVencimiento.ToString("dd/MM/yyyy"),
                            this.clsView.convertMoneda(this.ctrlBoletaEmpeno.boletasEmpeno.decPrestamo.ToString())
                            );

                            this.decTotalAcum += this.ctrlBoletaEmpeno.boletasEmpeno.decPrestamo;
                            txtTotales.Text = this.clsView.convertMoneda(this.decTotalAcum.ToString());
                            txtBoleta.Text = "";
                        }
                        else
                        {
                            txtBoleta.Text = "";
                            this.clsView.mensajeErrorValiForm2("La boleta no esta vencida, favor de verificar");

                        }


                    }
                    else 
                    {
                        this.clsView.mensajeErrorValiForm2("Debe ser una boleta activa que se encuentre vencida");
                        txtBoleta.Text = "";
                        
                    }
                    txtBoleta.Focus();

                }
            }         
        }

        private void dgvDetalles_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = false;
            }
            if (e.KeyCode == Keys.Down)
            {
                e.Handled = false;
            }
            if (e.KeyCode == Keys.Delete)
            {

                try
                {
                    dgvDetalles.CurrentRow.Selected = true;
                    int dgvIndex = dgvDetalles.CurrentRow.Index;


                    if (this.dgvDetalles.Rows.Count > 0)
                    {
                        this.decTotalAcum -= decimal.Parse(dgvDetalles.CurrentRow.Cells["Capital"].Value.ToString().Replace("$", "").Replace(",",""));
                        dgvDetalles.Rows.RemoveAt(dgvIndex);
                        txtTotales.Text = this.clsView.convertMoneda(this.decTotalAcum.ToString());

                    }

                    e.Handled = true;

                }
                catch (Exception ex) { }

            }
        }


 
    }
}
