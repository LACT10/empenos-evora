﻿namespace EmpenosEvora
{
    partial class frmBoletasEmpeno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtBoletaInicial = new System.Windows.Forms.TextBox();
            this.txtBoleta = new System.Windows.Forms.TextBox();
            this.lblDescripcion = new System.Windows.Forms.Label();
            this.txtNombreEmpleado = new System.Windows.Forms.TextBox();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.txtHora = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpFechaVencimiento = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDescripcionCaja = new System.Windows.Forms.TextBox();
            this.txtCodigoCaja = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPagoFolio = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRefrendadaCon = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtSaldoPPIs = new System.Windows.Forms.TextBox();
            this.txtPrestamo = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtCantidad = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtCodigoPrendaTipo = new System.Windows.Forms.TextBox();
            this.txtDescripcionPrendaTipo = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtDescripcionSubTipoPrenda = new System.Windows.Forms.TextBox();
            this.txtCodigoSubTipoPrenda = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.txtAvaluo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnVerificarHuellaCliente = new System.Windows.Forms.Button();
            this.txtClienteId = new System.Windows.Forms.TextBox();
            this.txtNombreCliente = new System.Windows.Forms.TextBox();
            this.txtCodigoCliente = new System.Windows.Forms.TextBox();
            this.tsAcciones = new System.Windows.Forms.ToolStrip();
            this.tsbNuevo = new System.Windows.Forms.ToolStripButton();
            this.tsbGuardar = new System.Windows.Forms.ToolStripButton();
            this.tsbEliminar = new System.Windows.Forms.ToolStripButton();
            this.tsbReactivar = new System.Windows.Forms.ToolStripButton();
            this.tsbBuscar = new System.Windows.Forms.ToolStripButton();
            this.btnUndo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrimeroPag = new System.Windows.Forms.ToolStripButton();
            this.btnAtras = new System.Windows.Forms.ToolStripButton();
            this.btnAdelante = new System.Windows.Forms.ToolStripButton();
            this.btnUlitmoPag = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbImprimir = new System.Windows.Forms.ToolStripButton();
            this.btnCierra = new System.Windows.Forms.ToolStripButton();
            this.txtPrendaTipotId = new System.Windows.Forms.TextBox();
            this.txtSubTipoPrendaId = new System.Windows.Forms.TextBox();
            this.txtIdentificacionValidar = new System.Windows.Forms.TextBox();
            this.txtIdIdentificacion = new System.Windows.Forms.TextBox();
            this.txtCodigoIdentificacion = new System.Windows.Forms.TextBox();
            this.txtDescripcionIdentificacion = new System.Windows.Forms.TextBox();
            this.lblDescEstatus = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtComentario = new System.Windows.Forms.TextBox();
            this.txtPlazoTope = new System.Windows.Forms.TextBox();
            this.lblQuilataje = new System.Windows.Forms.Label();
            this.txtKilataje = new System.Windows.Forms.TextBox();
            this.lblGramos = new System.Windows.Forms.Label();
            this.txtGramos = new System.Windows.Forms.TextBox();
            this.txtGramaje = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lblEstatus = new System.Windows.Forms.Label();
            this.txtPlazo = new System.Windows.Forms.NumericUpDown();
            this.txtRefrendoA = new System.Windows.Forms.TextBox();
            this.btnFotografia = new System.Windows.Forms.Button();
            this.txtCodigoGramaje = new System.Windows.Forms.TextBox();
            this.txtGramajeId = new System.Windows.Forms.TextBox();
            this.txtGramajeDescripcion = new System.Windows.Forms.TextBox();
            this.lblGramaje = new System.Windows.Forms.Label();
            this.txtPrecioTope = new System.Windows.Forms.TextBox();
            this.txtPrendaTipotCodigoAnterior = new System.Windows.Forms.TextBox();
            this.txtPagoFecha = new System.Windows.Forms.TextBox();
            this.btnBusquedaGramaje = new System.Windows.Forms.Button();
            this.btnBusqueda = new System.Windows.Forms.Button();
            this.btnBusquedaIdentif = new System.Windows.Forms.Button();
            this.txtBusquedaSubTipoPrenda = new System.Windows.Forms.Button();
            this.txtBusquedaTipoPrenda = new System.Windows.Forms.Button();
            this.btnFooterCierra = new System.Windows.Forms.Button();
            this.btnFooterGuardar = new System.Windows.Forms.Button();
            this.btnBusquedaCliente = new System.Windows.Forms.Button();
            this.txtPrestamoAnt = new System.Windows.Forms.TextBox();
            this.btnVerCancelacion = new System.Windows.Forms.Button();
            this.txtIMEI = new System.Windows.Forms.TextBox();
            this.lblIMEI = new System.Windows.Forms.Label();
            this.txtNumeroSerieVehiculo = new System.Windows.Forms.TextBox();
            this.lblSerieVehiculo = new System.Windows.Forms.Label();
            this.lblTipoSerieVehiculo = new System.Windows.Forms.Label();
            this.cmbTipoSerieVehiculo = new System.Windows.Forms.ComboBox();
            this.lkbCliente = new System.Windows.Forms.LinkLabel();
            this.tsAcciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlazo)).BeginInit();
            this.SuspendLayout();
            // 
            // txtBoletaInicial
            // 
            this.txtBoletaInicial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBoletaInicial.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtBoletaInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtBoletaInicial.Location = new System.Drawing.Point(336, 72);
            this.txtBoletaInicial.MaxLength = 50;
            this.txtBoletaInicial.Name = "txtBoletaInicial";
            this.txtBoletaInicial.ReadOnly = true;
            this.txtBoletaInicial.Size = new System.Drawing.Size(96, 20);
            this.txtBoletaInicial.TabIndex = 3;
            // 
            // txtBoleta
            // 
            this.txtBoleta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBoleta.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtBoleta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtBoleta.Location = new System.Drawing.Point(120, 72);
            this.txtBoleta.MaxLength = 10;
            this.txtBoleta.Name = "txtBoleta";
            this.txtBoleta.Size = new System.Drawing.Size(96, 20);
            this.txtBoleta.TabIndex = 1;
            this.txtBoleta.Leave += new System.EventHandler(this.txtBoleta_Leave);
            // 
            // lblDescripcion
            // 
            this.lblDescripcion.AutoSize = true;
            this.lblDescripcion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblDescripcion.Location = new System.Drawing.Point(264, 72);
            this.lblDescripcion.Name = "lblDescripcion";
            this.lblDescripcion.Size = new System.Drawing.Size(66, 13);
            this.lblDescripcion.TabIndex = 260;
            this.lblDescripcion.Text = "Boleta inicial";
            // 
            // txtNombreEmpleado
            // 
            this.txtNombreEmpleado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombreEmpleado.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNombreEmpleado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtNombreEmpleado.Location = new System.Drawing.Point(864, 72);
            this.txtNombreEmpleado.MaxLength = 250;
            this.txtNombreEmpleado.Name = "txtNombreEmpleado";
            this.txtNombreEmpleado.ReadOnly = true;
            this.txtNombreEmpleado.Size = new System.Drawing.Size(208, 20);
            this.txtNombreEmpleado.TabIndex = 7;
            // 
            // dtpFecha
            // 
            this.dtpFecha.Cursor = System.Windows.Forms.Cursors.Default;
            this.dtpFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecha.Location = new System.Drawing.Point(120, 104);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(98, 20);
            this.dtpFecha.TabIndex = 8;
            this.dtpFecha.Leave += new System.EventHandler(this.dtpFecha_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label3.Location = new System.Drawing.Point(8, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 267;
            this.label3.Text = "Fecha";
            // 
            // txtHora
            // 
            this.txtHora.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtHora.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtHora.Location = new System.Drawing.Point(272, 104);
            this.txtHora.MaxLength = 5;
            this.txtHora.Name = "txtHora";
            this.txtHora.ReadOnly = true;
            this.txtHora.Size = new System.Drawing.Size(56, 20);
            this.txtHora.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label4.Location = new System.Drawing.Point(232, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 270;
            this.label4.Text = "Hora";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label5.Location = new System.Drawing.Point(336, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 272;
            this.label5.Text = "Plazo";
            // 
            // dtpFechaVencimiento
            // 
            this.dtpFechaVencimiento.Cursor = System.Windows.Forms.Cursors.Default;
            this.dtpFechaVencimiento.Enabled = false;
            this.dtpFechaVencimiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.dtpFechaVencimiento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaVencimiento.Location = new System.Drawing.Point(624, 104);
            this.dtpFechaVencimiento.Name = "dtpFechaVencimiento";
            this.dtpFechaVencimiento.Size = new System.Drawing.Size(128, 20);
            this.dtpFechaVencimiento.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Cursor = System.Windows.Forms.Cursors.Default;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label7.Location = new System.Drawing.Point(496, 104);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(112, 13);
            this.label7.TabIndex = 273;
            this.label7.Text = "Fecha de vencimiento";
            // 
            // txtDescripcionCaja
            // 
            this.txtDescripcionCaja.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionCaja.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionCaja.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtDescripcionCaja.Location = new System.Drawing.Point(920, 104);
            this.txtDescripcionCaja.MaxLength = 250;
            this.txtDescripcionCaja.Name = "txtDescripcionCaja";
            this.txtDescripcionCaja.ReadOnly = true;
            this.txtDescripcionCaja.Size = new System.Drawing.Size(152, 20);
            this.txtDescripcionCaja.TabIndex = 13;
            // 
            // txtCodigoCaja
            // 
            this.txtCodigoCaja.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoCaja.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoCaja.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtCodigoCaja.Location = new System.Drawing.Point(864, 104);
            this.txtCodigoCaja.MaxLength = 50;
            this.txtCodigoCaja.Name = "txtCodigoCaja";
            this.txtCodigoCaja.ReadOnly = true;
            this.txtCodigoCaja.Size = new System.Drawing.Size(48, 20);
            this.txtCodigoCaja.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Cursor = System.Windows.Forms.Cursors.Default;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label8.Location = new System.Drawing.Point(808, 104);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 13);
            this.label8.TabIndex = 275;
            this.label8.Text = "Caja";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label12.Location = new System.Drawing.Point(544, 136);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 13);
            this.label12.TabIndex = 283;
            this.label12.Text = "Identificación";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Cursor = System.Windows.Forms.Cursors.Default;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label13.Location = new System.Drawing.Point(8, 200);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(108, 13);
            this.label13.TabIndex = 287;
            this.label13.Text = "Pagada con el recibo";
            // 
            // txtPagoFolio
            // 
            this.txtPagoFolio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPagoFolio.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtPagoFolio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtPagoFolio.Location = new System.Drawing.Point(120, 200);
            this.txtPagoFolio.MaxLength = 50;
            this.txtPagoFolio.Name = "txtPagoFolio";
            this.txtPagoFolio.ReadOnly = true;
            this.txtPagoFolio.Size = new System.Drawing.Size(96, 20);
            this.txtPagoFolio.TabIndex = 35;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Cursor = System.Windows.Forms.Cursors.Default;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label14.Location = new System.Drawing.Point(808, 72);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 13);
            this.label14.TabIndex = 291;
            this.label14.Text = "Empleado";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label2.Location = new System.Drawing.Point(536, 200);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 295;
            this.label2.Text = "Refrendada con";
            // 
            // txtRefrendadaCon
            // 
            this.txtRefrendadaCon.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRefrendadaCon.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtRefrendadaCon.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtRefrendadaCon.Location = new System.Drawing.Point(408, 200);
            this.txtRefrendadaCon.MaxLength = 50;
            this.txtRefrendadaCon.Name = "txtRefrendadaCon";
            this.txtRefrendadaCon.ReadOnly = true;
            this.txtRefrendadaCon.Size = new System.Drawing.Size(120, 20);
            this.txtRefrendadaCon.TabIndex = 37;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Cursor = System.Windows.Forms.Cursors.Default;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label11.Location = new System.Drawing.Point(336, 200);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 292;
            this.label11.Text = "Refrendó a";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Cursor = System.Windows.Forms.Cursors.Default;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label15.Location = new System.Drawing.Point(760, 200);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(61, 13);
            this.label15.TabIndex = 297;
            this.label15.Text = "Saldo PPI\'s";
            // 
            // txtSaldoPPIs
            // 
            this.txtSaldoPPIs.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSaldoPPIs.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtSaldoPPIs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtSaldoPPIs.Location = new System.Drawing.Point(832, 200);
            this.txtSaldoPPIs.MaxLength = 50;
            this.txtSaldoPPIs.Name = "txtSaldoPPIs";
            this.txtSaldoPPIs.ReadOnly = true;
            this.txtSaldoPPIs.Size = new System.Drawing.Size(136, 20);
            this.txtSaldoPPIs.TabIndex = 39;
            // 
            // txtPrestamo
            // 
            this.txtPrestamo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrestamo.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtPrestamo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtPrestamo.Location = new System.Drawing.Point(120, 232);
            this.txtPrestamo.MaxLength = 100;
            this.txtPrestamo.Name = "txtPrestamo";
            this.txtPrestamo.Size = new System.Drawing.Size(96, 20);
            this.txtPrestamo.TabIndex = 40;
            this.txtPrestamo.TextChanged += new System.EventHandler(this.txtPrestamo_TextChanged);
            this.txtPrestamo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrestamo_KeyPress);
            this.txtPrestamo.Leave += new System.EventHandler(this.txtPrestamo_Leave);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Cursor = System.Windows.Forms.Cursors.Default;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label16.Location = new System.Drawing.Point(8, 232);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 13);
            this.label16.TabIndex = 298;
            this.label16.Text = "Préstamo ";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Cursor = System.Windows.Forms.Cursors.Default;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label17.Location = new System.Drawing.Point(336, 232);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(40, 13);
            this.label17.TabIndex = 300;
            this.label17.Text = "Avalúo";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Cursor = System.Windows.Forms.Cursors.Default;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label18.Location = new System.Drawing.Point(424, 168);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 13);
            this.label18.TabIndex = 310;
            this.label18.Text = "Cantidad ";
            // 
            // txtCantidad
            // 
            this.txtCantidad.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtCantidad.Location = new System.Drawing.Point(480, 168);
            this.txtCantidad.MaxLength = 100;
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(48, 20);
            this.txtCantidad.TabIndex = 26;
            this.txtCantidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCantidad_KeyPress);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Cursor = System.Windows.Forms.Cursors.Default;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label21.Location = new System.Drawing.Point(440, 72);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(79, 13);
            this.label21.TabIndex = 303;
            this.label21.Text = "Tipo de prenda";
            // 
            // txtCodigoPrendaTipo
            // 
            this.txtCodigoPrendaTipo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoPrendaTipo.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoPrendaTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtCodigoPrendaTipo.Location = new System.Drawing.Point(520, 72);
            this.txtCodigoPrendaTipo.MaxLength = 50;
            this.txtCodigoPrendaTipo.Name = "txtCodigoPrendaTipo";
            this.txtCodigoPrendaTipo.Size = new System.Drawing.Size(48, 20);
            this.txtCodigoPrendaTipo.TabIndex = 4;
            this.txtCodigoPrendaTipo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigoPrendaTipo_KeyPress);
            this.txtCodigoPrendaTipo.Leave += new System.EventHandler(this.txtCodigoPrendaTipo_Leave);
            // 
            // txtDescripcionPrendaTipo
            // 
            this.txtDescripcionPrendaTipo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionPrendaTipo.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionPrendaTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtDescripcionPrendaTipo.Location = new System.Drawing.Point(576, 72);
            this.txtDescripcionPrendaTipo.MaxLength = 250;
            this.txtDescripcionPrendaTipo.Name = "txtDescripcionPrendaTipo";
            this.txtDescripcionPrendaTipo.ReadOnly = true;
            this.txtDescripcionPrendaTipo.Size = new System.Drawing.Size(176, 20);
            this.txtDescripcionPrendaTipo.TabIndex = 5;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Cursor = System.Windows.Forms.Cursors.Default;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label20.Location = new System.Drawing.Point(8, 168);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(94, 13);
            this.label20.TabIndex = 314;
            this.label20.Text = "Subtipo de prenda";
            // 
            // txtDescripcionSubTipoPrenda
            // 
            this.txtDescripcionSubTipoPrenda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionSubTipoPrenda.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionSubTipoPrenda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtDescripcionSubTipoPrenda.Location = new System.Drawing.Point(176, 168);
            this.txtDescripcionSubTipoPrenda.MaxLength = 250;
            this.txtDescripcionSubTipoPrenda.Name = "txtDescripcionSubTipoPrenda";
            this.txtDescripcionSubTipoPrenda.ReadOnly = true;
            this.txtDescripcionSubTipoPrenda.Size = new System.Drawing.Size(200, 20);
            this.txtDescripcionSubTipoPrenda.TabIndex = 24;
            // 
            // txtCodigoSubTipoPrenda
            // 
            this.txtCodigoSubTipoPrenda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoSubTipoPrenda.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoSubTipoPrenda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtCodigoSubTipoPrenda.Location = new System.Drawing.Point(120, 168);
            this.txtCodigoSubTipoPrenda.MaxLength = 50;
            this.txtCodigoSubTipoPrenda.Name = "txtCodigoSubTipoPrenda";
            this.txtCodigoSubTipoPrenda.Size = new System.Drawing.Size(48, 20);
            this.txtCodigoSubTipoPrenda.TabIndex = 23;
            this.txtCodigoSubTipoPrenda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigoSubTipoPrenda_KeyPress);
            this.txtCodigoSubTipoPrenda.Leave += new System.EventHandler(this.txtCodigoSubTipoPrenda_Leave);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Cursor = System.Windows.Forms.Cursors.Default;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label25.Location = new System.Drawing.Point(8, 264);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(63, 13);
            this.label25.TabIndex = 319;
            this.label25.Text = "Descripción";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtDescripcion.Location = new System.Drawing.Point(120, 264);
            this.txtDescripcion.MaxLength = 250;
            this.txtDescripcion.Multiline = true;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(952, 72);
            this.txtDescripcion.TabIndex = 43;
            // 
            // txtAvaluo
            // 
            this.txtAvaluo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAvaluo.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAvaluo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtAvaluo.Location = new System.Drawing.Point(408, 232);
            this.txtAvaluo.MaxLength = 5;
            this.txtAvaluo.Name = "txtAvaluo";
            this.txtAvaluo.ReadOnly = true;
            this.txtAvaluo.Size = new System.Drawing.Size(120, 20);
            this.txtAvaluo.TabIndex = 41;
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label1.Location = new System.Drawing.Point(-8, 376);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1128, 2);
            this.label1.TabIndex = 427;
            // 
            // btnVerificarHuellaCliente
            // 
            this.btnVerificarHuellaCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnVerificarHuellaCliente.Location = new System.Drawing.Point(384, 136);
            this.btnVerificarHuellaCliente.Name = "btnVerificarHuellaCliente";
            this.btnVerificarHuellaCliente.Size = new System.Drawing.Size(64, 21);
            this.btnVerificarHuellaCliente.TabIndex = 17;
            this.btnVerificarHuellaCliente.Text = "Verificar ";
            this.btnVerificarHuellaCliente.UseVisualStyleBackColor = true;
            this.btnVerificarHuellaCliente.Click += new System.EventHandler(this.btnVerificarHuellaCliente_Click);
            // 
            // txtClienteId
            // 
            this.txtClienteId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtClienteId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtClienteId.Location = new System.Drawing.Point(192, 136);
            this.txtClienteId.MaxLength = 3;
            this.txtClienteId.Name = "txtClienteId";
            this.txtClienteId.Size = new System.Drawing.Size(44, 20);
            this.txtClienteId.TabIndex = 546;
            this.txtClienteId.Visible = false;
            // 
            // txtNombreCliente
            // 
            this.txtNombreCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombreCliente.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNombreCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtNombreCliente.Location = new System.Drawing.Point(176, 136);
            this.txtNombreCliente.MaxLength = 250;
            this.txtNombreCliente.Name = "txtNombreCliente";
            this.txtNombreCliente.ReadOnly = true;
            this.txtNombreCliente.Size = new System.Drawing.Size(160, 20);
            this.txtNombreCliente.TabIndex = 15;
            // 
            // txtCodigoCliente
            // 
            this.txtCodigoCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoCliente.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoCliente.Location = new System.Drawing.Point(120, 136);
            this.txtCodigoCliente.MaxLength = 50;
            this.txtCodigoCliente.Name = "txtCodigoCliente";
            this.txtCodigoCliente.Size = new System.Drawing.Size(48, 20);
            this.txtCodigoCliente.TabIndex = 14;
            this.txtCodigoCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigoCliente_KeyPress);
            this.txtCodigoCliente.Leave += new System.EventHandler(this.txtCodigoCliente_Leave);
            // 
            // tsAcciones
            // 
            this.tsAcciones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbNuevo,
            this.tsbGuardar,
            this.tsbEliminar,
            this.tsbReactivar,
            this.tsbBuscar,
            this.btnUndo,
            this.toolStripSeparator2,
            this.btnPrimeroPag,
            this.btnAtras,
            this.btnAdelante,
            this.btnUlitmoPag,
            this.toolStripSeparator1,
            this.tsbImprimir,
            this.btnCierra});
            this.tsAcciones.Location = new System.Drawing.Point(0, 0);
            this.tsAcciones.Name = "tsAcciones";
            this.tsAcciones.Size = new System.Drawing.Size(1090, 25);
            this.tsAcciones.TabIndex = 551;
            this.tsAcciones.Text = "toolStrip1";
            // 
            // tsbNuevo
            // 
            this.tsbNuevo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbNuevo.Image = global::EmpenosEvora.Properties.Resources.AddFile_16x;
            this.tsbNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbNuevo.Name = "tsbNuevo";
            this.tsbNuevo.Size = new System.Drawing.Size(23, 22);
            this.tsbNuevo.Text = "Nuevo";
            this.tsbNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical90;
            this.tsbNuevo.Click += new System.EventHandler(this.tsbNuevo_Click);
            // 
            // tsbGuardar
            // 
            this.tsbGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbGuardar.Image = global::EmpenosEvora.Properties.Resources.Save_16x;
            this.tsbGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGuardar.Name = "tsbGuardar";
            this.tsbGuardar.Size = new System.Drawing.Size(23, 22);
            this.tsbGuardar.Tag = "";
            this.tsbGuardar.Text = "Guardar";
            this.tsbGuardar.Click += new System.EventHandler(this.tsbGuardar_Click);
            // 
            // tsbEliminar
            // 
            this.tsbEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbEliminar.Image = global::EmpenosEvora.Properties.Resources.Cancel_16x;
            this.tsbEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEliminar.Name = "tsbEliminar";
            this.tsbEliminar.Size = new System.Drawing.Size(23, 22);
            this.tsbEliminar.Text = "Desactivar";
            this.tsbEliminar.ToolTipText = "Desactivar";
            this.tsbEliminar.Click += new System.EventHandler(this.tsbEliminar_Click);
            // 
            // tsbReactivar
            // 
            this.tsbReactivar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbReactivar.Image = global::EmpenosEvora.Properties.Resources.CompareValidator_16x;
            this.tsbReactivar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbReactivar.Name = "tsbReactivar";
            this.tsbReactivar.Size = new System.Drawing.Size(23, 22);
            this.tsbReactivar.Text = "Reactivar";
            this.tsbReactivar.ToolTipText = "Reactivar";
            this.tsbReactivar.Visible = false;
            this.tsbReactivar.Click += new System.EventHandler(this.tsbReactivar_Click);
            // 
            // tsbBuscar
            // 
            this.tsbBuscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBuscar.Image = global::EmpenosEvora.Properties.Resources.VBSearch_16x;
            this.tsbBuscar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBuscar.Name = "tsbBuscar";
            this.tsbBuscar.Size = new System.Drawing.Size(23, 22);
            this.tsbBuscar.Text = "Buscar";
            this.tsbBuscar.Click += new System.EventHandler(this.tsbBuscar_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUndo.Image = global::EmpenosEvora.Properties.Resources.Undo_16x;
            this.btnUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(23, 22);
            this.btnUndo.Text = "Deshacer";
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrimeroPag
            // 
            this.btnPrimeroPag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrimeroPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeftGroup_16xSM;
            this.btnPrimeroPag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrimeroPag.Name = "btnPrimeroPag";
            this.btnPrimeroPag.Size = new System.Drawing.Size(23, 22);
            this.btnPrimeroPag.Text = "Primero";
            this.btnPrimeroPag.Click += new System.EventHandler(this.btnPrimeroPag_Click);
            // 
            // btnAtras
            // 
            this.btnAtras.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAtras.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeft_16x;
            this.btnAtras.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(23, 22);
            this.btnAtras.Text = "Atras";
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // btnAdelante
            // 
            this.btnAdelante.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdelante.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronRight_16x;
            this.btnAdelante.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdelante.Name = "btnAdelante";
            this.btnAdelante.Size = new System.Drawing.Size(23, 22);
            this.btnAdelante.Text = "Siguente";
            this.btnAdelante.Click += new System.EventHandler(this.btnAdelante_Click);
            // 
            // btnUlitmoPag
            // 
            this.btnUlitmoPag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUlitmoPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronRightGroup_16x;
            this.btnUlitmoPag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUlitmoPag.Name = "btnUlitmoPag";
            this.btnUlitmoPag.Size = new System.Drawing.Size(23, 22);
            this.btnUlitmoPag.Text = "Ultimo";
            this.btnUlitmoPag.Click += new System.EventHandler(this.btnUlitmoPag_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbImprimir
            // 
            this.tsbImprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbImprimir.Image = global::EmpenosEvora.Properties.Resources.Print_16x;
            this.tsbImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbImprimir.Name = "tsbImprimir";
            this.tsbImprimir.Size = new System.Drawing.Size(23, 22);
            this.tsbImprimir.Text = "Imprimir";
            this.tsbImprimir.ToolTipText = "Imprimir";
            // 
            // btnCierra
            // 
            this.btnCierra.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnCierra.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCierra.Name = "btnCierra";
            this.btnCierra.Size = new System.Drawing.Size(23, 22);
            this.btnCierra.Text = "Salir";
            this.btnCierra.Click += new System.EventHandler(this.btnCierra_Click);
            // 
            // txtPrendaTipotId
            // 
            this.txtPrendaTipotId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtPrendaTipotId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtPrendaTipotId.Location = new System.Drawing.Point(688, 72);
            this.txtPrendaTipotId.MaxLength = 3;
            this.txtPrendaTipotId.Name = "txtPrendaTipotId";
            this.txtPrendaTipotId.Size = new System.Drawing.Size(44, 20);
            this.txtPrendaTipotId.TabIndex = 555;
            this.txtPrendaTipotId.Visible = false;
            // 
            // txtSubTipoPrendaId
            // 
            this.txtSubTipoPrendaId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtSubTipoPrendaId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtSubTipoPrendaId.Location = new System.Drawing.Point(272, 168);
            this.txtSubTipoPrendaId.MaxLength = 3;
            this.txtSubTipoPrendaId.Name = "txtSubTipoPrendaId";
            this.txtSubTipoPrendaId.Size = new System.Drawing.Size(56, 20);
            this.txtSubTipoPrendaId.TabIndex = 557;
            this.txtSubTipoPrendaId.Visible = false;
            // 
            // txtIdentificacionValidar
            // 
            this.txtIdentificacionValidar.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtIdentificacionValidar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdentificacionValidar.Location = new System.Drawing.Point(808, 136);
            this.txtIdentificacionValidar.MaxLength = 3;
            this.txtIdentificacionValidar.Name = "txtIdentificacionValidar";
            this.txtIdentificacionValidar.Size = new System.Drawing.Size(44, 20);
            this.txtIdentificacionValidar.TabIndex = 562;
            this.txtIdentificacionValidar.Visible = false;
            // 
            // txtIdIdentificacion
            // 
            this.txtIdIdentificacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtIdIdentificacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdIdentificacion.Location = new System.Drawing.Point(864, 136);
            this.txtIdIdentificacion.MaxLength = 3;
            this.txtIdIdentificacion.Name = "txtIdIdentificacion";
            this.txtIdIdentificacion.Size = new System.Drawing.Size(44, 20);
            this.txtIdIdentificacion.TabIndex = 561;
            this.txtIdIdentificacion.Visible = false;
            // 
            // txtCodigoIdentificacion
            // 
            this.txtCodigoIdentificacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoIdentificacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoIdentificacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoIdentificacion.Location = new System.Drawing.Point(624, 136);
            this.txtCodigoIdentificacion.MaxLength = 50;
            this.txtCodigoIdentificacion.Name = "txtCodigoIdentificacion";
            this.txtCodigoIdentificacion.Size = new System.Drawing.Size(48, 20);
            this.txtCodigoIdentificacion.TabIndex = 20;
            this.txtCodigoIdentificacion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigoIdentificacion_KeyPress);
            this.txtCodigoIdentificacion.Leave += new System.EventHandler(this.txtCodigoIdentificacion_Leave);
            // 
            // txtDescripcionIdentificacion
            // 
            this.txtDescripcionIdentificacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionIdentificacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionIdentificacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcionIdentificacion.Location = new System.Drawing.Point(680, 136);
            this.txtDescripcionIdentificacion.MaxLength = 250;
            this.txtDescripcionIdentificacion.Name = "txtDescripcionIdentificacion";
            this.txtDescripcionIdentificacion.ReadOnly = true;
            this.txtDescripcionIdentificacion.Size = new System.Drawing.Size(352, 20);
            this.txtDescripcionIdentificacion.TabIndex = 21;
            // 
            // lblDescEstatus
            // 
            this.lblDescEstatus.AutoSize = true;
            this.lblDescEstatus.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblDescEstatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblDescEstatus.ForeColor = System.Drawing.Color.Black;
            this.lblDescEstatus.Location = new System.Drawing.Point(8, 40);
            this.lblDescEstatus.Name = "lblDescEstatus";
            this.lblDescEstatus.Size = new System.Drawing.Size(42, 13);
            this.lblDescEstatus.TabIndex = 563;
            this.lblDescEstatus.Text = "Estatus";
            this.lblDescEstatus.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label6.Location = new System.Drawing.Point(8, 344);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 565;
            this.label6.Text = "Comentario";
            // 
            // txtComentario
            // 
            this.txtComentario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtComentario.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtComentario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtComentario.Location = new System.Drawing.Point(120, 344);
            this.txtComentario.MaxLength = 100;
            this.txtComentario.Name = "txtComentario";
            this.txtComentario.Size = new System.Drawing.Size(952, 20);
            this.txtComentario.TabIndex = 44;
            // 
            // txtPlazoTope
            // 
            this.txtPlazoTope.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtPlazoTope.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtPlazoTope.Location = new System.Drawing.Point(440, 104);
            this.txtPlazoTope.MaxLength = 3;
            this.txtPlazoTope.Name = "txtPlazoTope";
            this.txtPlazoTope.Size = new System.Drawing.Size(24, 20);
            this.txtPlazoTope.TabIndex = 568;
            this.txtPlazoTope.Visible = false;
            // 
            // lblQuilataje
            // 
            this.lblQuilataje.AutoSize = true;
            this.lblQuilataje.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblQuilataje.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblQuilataje.Location = new System.Drawing.Point(872, 168);
            this.lblQuilataje.Name = "lblQuilataje";
            this.lblQuilataje.Size = new System.Drawing.Size(41, 13);
            this.lblQuilataje.TabIndex = 569;
            this.lblQuilataje.Text = "Kilataje";
            this.lblQuilataje.Visible = false;
            // 
            // txtKilataje
            // 
            this.txtKilataje.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtKilataje.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtKilataje.Location = new System.Drawing.Point(920, 168);
            this.txtKilataje.MaxLength = 100;
            this.txtKilataje.Name = "txtKilataje";
            this.txtKilataje.Size = new System.Drawing.Size(48, 20);
            this.txtKilataje.TabIndex = 30;
            this.txtKilataje.Visible = false;
            this.txtKilataje.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKilataje_KeyPress);
            this.txtKilataje.Leave += new System.EventHandler(this.txtKilataje_Leave);
            // 
            // lblGramos
            // 
            this.lblGramos.AutoSize = true;
            this.lblGramos.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblGramos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblGramos.Location = new System.Drawing.Point(976, 168);
            this.lblGramos.Name = "lblGramos";
            this.lblGramos.Size = new System.Drawing.Size(43, 13);
            this.lblGramos.TabIndex = 571;
            this.lblGramos.Text = "Gramos";
            this.lblGramos.Visible = false;
            // 
            // txtGramos
            // 
            this.txtGramos.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtGramos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtGramos.Location = new System.Drawing.Point(1024, 168);
            this.txtGramos.MaxLength = 100;
            this.txtGramos.Name = "txtGramos";
            this.txtGramos.Size = new System.Drawing.Size(48, 20);
            this.txtGramos.TabIndex = 31;
            this.txtGramos.Visible = false;
            this.txtGramos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGramos_KeyPress);
            this.txtGramos.Leave += new System.EventHandler(this.txtGramos_Leave);
            // 
            // txtGramaje
            // 
            this.txtGramaje.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtGramaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtGramaje.Location = new System.Drawing.Point(576, 72);
            this.txtGramaje.MaxLength = 3;
            this.txtGramaje.Name = "txtGramaje";
            this.txtGramaje.Size = new System.Drawing.Size(44, 20);
            this.txtGramaje.TabIndex = 573;
            this.txtGramaje.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Cursor = System.Windows.Forms.Cursors.Default;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label9.Location = new System.Drawing.Point(8, 72);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 574;
            this.label9.Text = "Boleta";
            // 
            // lblEstatus
            // 
            this.lblEstatus.AutoSize = true;
            this.lblEstatus.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblEstatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstatus.ForeColor = System.Drawing.Color.Red;
            this.lblEstatus.Location = new System.Drawing.Point(120, 40);
            this.lblEstatus.Name = "lblEstatus";
            this.lblEstatus.Size = new System.Drawing.Size(50, 13);
            this.lblEstatus.TabIndex = 575;
            this.lblEstatus.Text = "NUEVO";
            this.lblEstatus.Visible = false;
            // 
            // txtPlazo
            // 
            this.txtPlazo.Location = new System.Drawing.Point(376, 104);
            this.txtPlazo.Name = "txtPlazo";
            this.txtPlazo.Size = new System.Drawing.Size(64, 20);
            this.txtPlazo.TabIndex = 10;
            this.txtPlazo.Leave += new System.EventHandler(this.txtPlazo_Leave);
            // 
            // txtRefrendoA
            // 
            this.txtRefrendoA.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRefrendoA.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtRefrendoA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtRefrendoA.Location = new System.Drawing.Point(624, 200);
            this.txtRefrendoA.MaxLength = 50;
            this.txtRefrendoA.Name = "txtRefrendoA";
            this.txtRefrendoA.ReadOnly = true;
            this.txtRefrendoA.Size = new System.Drawing.Size(120, 20);
            this.txtRefrendoA.TabIndex = 38;
            // 
            // btnFotografia
            // 
            this.btnFotografia.Location = new System.Drawing.Point(464, 136);
            this.btnFotografia.Name = "btnFotografia";
            this.btnFotografia.Size = new System.Drawing.Size(64, 21);
            this.btnFotografia.TabIndex = 19;
            this.btnFotografia.Text = "Fotografía";
            this.btnFotografia.UseVisualStyleBackColor = true;
            this.btnFotografia.Click += new System.EventHandler(this.btnFotografia_Click);
            // 
            // txtCodigoGramaje
            // 
            this.txtCodigoGramaje.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoGramaje.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoGramaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoGramaje.Location = new System.Drawing.Point(624, 168);
            this.txtCodigoGramaje.MaxLength = 50;
            this.txtCodigoGramaje.Name = "txtCodigoGramaje";
            this.txtCodigoGramaje.Size = new System.Drawing.Size(48, 20);
            this.txtCodigoGramaje.TabIndex = 27;
            this.txtCodigoGramaje.Visible = false;
            this.txtCodigoGramaje.Leave += new System.EventHandler(this.txtCodigoGramaje_Leave);
            // 
            // txtGramajeId
            // 
            this.txtGramajeId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtGramajeId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtGramajeId.Location = new System.Drawing.Point(696, 168);
            this.txtGramajeId.MaxLength = 3;
            this.txtGramajeId.Name = "txtGramajeId";
            this.txtGramajeId.Size = new System.Drawing.Size(44, 20);
            this.txtGramajeId.TabIndex = 583;
            this.txtGramajeId.Visible = false;
            // 
            // txtGramajeDescripcion
            // 
            this.txtGramajeDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGramajeDescripcion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtGramajeDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtGramajeDescripcion.Location = new System.Drawing.Point(680, 168);
            this.txtGramajeDescripcion.MaxLength = 50;
            this.txtGramajeDescripcion.Name = "txtGramajeDescripcion";
            this.txtGramajeDescripcion.ReadOnly = true;
            this.txtGramajeDescripcion.Size = new System.Drawing.Size(144, 20);
            this.txtGramajeDescripcion.TabIndex = 28;
            this.txtGramajeDescripcion.Visible = false;
            // 
            // lblGramaje
            // 
            this.lblGramaje.AutoSize = true;
            this.lblGramaje.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblGramaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblGramaje.Location = new System.Drawing.Point(544, 168);
            this.lblGramaje.Name = "lblGramaje";
            this.lblGramaje.Size = new System.Drawing.Size(46, 13);
            this.lblGramaje.TabIndex = 580;
            this.lblGramaje.Text = "Gramaje";
            this.lblGramaje.Visible = false;
            // 
            // txtPrecioTope
            // 
            this.txtPrecioTope.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtPrecioTope.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtPrecioTope.Location = new System.Drawing.Point(736, 168);
            this.txtPrecioTope.MaxLength = 3;
            this.txtPrecioTope.Name = "txtPrecioTope";
            this.txtPrecioTope.Size = new System.Drawing.Size(44, 20);
            this.txtPrecioTope.TabIndex = 585;
            this.txtPrecioTope.Visible = false;
            // 
            // txtPrendaTipotCodigoAnterior
            // 
            this.txtPrendaTipotCodigoAnterior.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtPrendaTipotCodigoAnterior.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtPrendaTipotCodigoAnterior.Location = new System.Drawing.Point(640, 72);
            this.txtPrendaTipotCodigoAnterior.MaxLength = 3;
            this.txtPrendaTipotCodigoAnterior.Name = "txtPrendaTipotCodigoAnterior";
            this.txtPrendaTipotCodigoAnterior.Size = new System.Drawing.Size(44, 20);
            this.txtPrendaTipotCodigoAnterior.TabIndex = 586;
            this.txtPrendaTipotCodigoAnterior.Visible = false;
            // 
            // txtPagoFecha
            // 
            this.txtPagoFecha.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPagoFecha.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtPagoFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtPagoFecha.Location = new System.Drawing.Point(224, 200);
            this.txtPagoFecha.MaxLength = 50;
            this.txtPagoFecha.Name = "txtPagoFecha";
            this.txtPagoFecha.ReadOnly = true;
            this.txtPagoFecha.Size = new System.Drawing.Size(96, 20);
            this.txtPagoFecha.TabIndex = 36;
            // 
            // btnBusquedaGramaje
            // 
            this.btnBusquedaGramaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnBusquedaGramaje.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusquedaGramaje.Location = new System.Drawing.Point(832, 168);
            this.btnBusquedaGramaje.Name = "btnBusquedaGramaje";
            this.btnBusquedaGramaje.Size = new System.Drawing.Size(32, 21);
            this.btnBusquedaGramaje.TabIndex = 29;
            this.btnBusquedaGramaje.UseVisualStyleBackColor = true;
            this.btnBusquedaGramaje.Visible = false;
            this.btnBusquedaGramaje.Click += new System.EventHandler(this.txtBusquedaGramaje_Click);
            // 
            // btnBusqueda
            // 
            this.btnBusqueda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnBusqueda.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusqueda.Location = new System.Drawing.Point(224, 72);
            this.btnBusqueda.Name = "btnBusqueda";
            this.btnBusqueda.Size = new System.Drawing.Size(32, 21);
            this.btnBusqueda.TabIndex = 2;
            this.btnBusqueda.UseVisualStyleBackColor = true;
            this.btnBusqueda.Click += new System.EventHandler(this.btnBusqueda_Click);
            // 
            // btnBusquedaIdentif
            // 
            this.btnBusquedaIdentif.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBusquedaIdentif.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusquedaIdentif.Location = new System.Drawing.Point(1040, 136);
            this.btnBusquedaIdentif.Name = "btnBusquedaIdentif";
            this.btnBusquedaIdentif.Size = new System.Drawing.Size(32, 21);
            this.btnBusquedaIdentif.TabIndex = 22;
            this.btnBusquedaIdentif.UseVisualStyleBackColor = true;
            this.btnBusquedaIdentif.Click += new System.EventHandler(this.btnBusquedaIdentif_Click);
            // 
            // txtBusquedaSubTipoPrenda
            // 
            this.txtBusquedaSubTipoPrenda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtBusquedaSubTipoPrenda.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.txtBusquedaSubTipoPrenda.Location = new System.Drawing.Point(384, 168);
            this.txtBusquedaSubTipoPrenda.Name = "txtBusquedaSubTipoPrenda";
            this.txtBusquedaSubTipoPrenda.Size = new System.Drawing.Size(32, 21);
            this.txtBusquedaSubTipoPrenda.TabIndex = 25;
            this.txtBusquedaSubTipoPrenda.UseVisualStyleBackColor = true;
            this.txtBusquedaSubTipoPrenda.Click += new System.EventHandler(this.txtBusquedaSubTipoPrenda_Click);
            // 
            // txtBusquedaTipoPrenda
            // 
            this.txtBusquedaTipoPrenda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtBusquedaTipoPrenda.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.txtBusquedaTipoPrenda.Location = new System.Drawing.Point(760, 72);
            this.txtBusquedaTipoPrenda.Name = "txtBusquedaTipoPrenda";
            this.txtBusquedaTipoPrenda.Size = new System.Drawing.Size(32, 21);
            this.txtBusquedaTipoPrenda.TabIndex = 6;
            this.txtBusquedaTipoPrenda.UseVisualStyleBackColor = true;
            this.txtBusquedaTipoPrenda.Click += new System.EventHandler(this.txtBusquedaTipoPrenda_Click);
            // 
            // btnFooterCierra
            // 
            this.btnFooterCierra.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnFooterCierra.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterCierra.Location = new System.Drawing.Point(984, 392);
            this.btnFooterCierra.Name = "btnFooterCierra";
            this.btnFooterCierra.Size = new System.Drawing.Size(75, 40);
            this.btnFooterCierra.TabIndex = 46;
            this.btnFooterCierra.Text = "Salir";
            this.btnFooterCierra.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterCierra.UseVisualStyleBackColor = true;
            this.btnFooterCierra.Click += new System.EventHandler(this.btnFooterCierra_Click);
            // 
            // btnFooterGuardar
            // 
            this.btnFooterGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterGuardar.Image = global::EmpenosEvora.Properties.Resources.Save_16x;
            this.btnFooterGuardar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterGuardar.Location = new System.Drawing.Point(896, 392);
            this.btnFooterGuardar.Name = "btnFooterGuardar";
            this.btnFooterGuardar.Size = new System.Drawing.Size(75, 40);
            this.btnFooterGuardar.TabIndex = 45;
            this.btnFooterGuardar.Text = "Guardar";
            this.btnFooterGuardar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterGuardar.UseVisualStyleBackColor = true;
            this.btnFooterGuardar.Click += new System.EventHandler(this.btnFooterGuardar_Click);
            // 
            // btnBusquedaCliente
            // 
            this.btnBusquedaCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnBusquedaCliente.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusquedaCliente.Location = new System.Drawing.Point(344, 136);
            this.btnBusquedaCliente.Name = "btnBusquedaCliente";
            this.btnBusquedaCliente.Size = new System.Drawing.Size(32, 21);
            this.btnBusquedaCliente.TabIndex = 16;
            this.btnBusquedaCliente.UseVisualStyleBackColor = true;
            this.btnBusquedaCliente.Click += new System.EventHandler(this.btnBusquedaCliente_Click);
            // 
            // txtPrestamoAnt
            // 
            this.txtPrestamoAnt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrestamoAnt.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtPrestamoAnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrestamoAnt.Location = new System.Drawing.Point(152, 232);
            this.txtPrestamoAnt.MaxLength = 3;
            this.txtPrestamoAnt.Name = "txtPrestamoAnt";
            this.txtPrestamoAnt.Size = new System.Drawing.Size(44, 20);
            this.txtPrestamoAnt.TabIndex = 587;
            this.txtPrestamoAnt.Visible = false;
            // 
            // btnVerCancelacion
            // 
            this.btnVerCancelacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnVerCancelacion.Location = new System.Drawing.Point(624, 232);
            this.btnVerCancelacion.Name = "btnVerCancelacion";
            this.btnVerCancelacion.Size = new System.Drawing.Size(144, 21);
            this.btnVerCancelacion.TabIndex = 42;
            this.btnVerCancelacion.Text = "Ver motivo de cancelación";
            this.btnVerCancelacion.UseVisualStyleBackColor = true;
            this.btnVerCancelacion.Visible = false;
            this.btnVerCancelacion.Click += new System.EventHandler(this.btnVerCancelacion_Click);
            // 
            // txtIMEI
            // 
            this.txtIMEI.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIMEI.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtIMEI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtIMEI.Location = new System.Drawing.Point(272, 424);
            this.txtIMEI.MaxLength = 15;
            this.txtIMEI.Name = "txtIMEI";
            this.txtIMEI.Size = new System.Drawing.Size(120, 20);
            this.txtIMEI.TabIndex = 34;
            this.txtIMEI.Visible = false;
            this.txtIMEI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIMEI_KeyPress);
            // 
            // lblIMEI
            // 
            this.lblIMEI.AutoSize = true;
            this.lblIMEI.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblIMEI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblIMEI.Location = new System.Drawing.Point(232, 424);
            this.lblIMEI.Name = "lblIMEI";
            this.lblIMEI.Size = new System.Drawing.Size(29, 13);
            this.lblIMEI.TabIndex = 600;
            this.lblIMEI.Text = "IMEI";
            this.lblIMEI.Visible = false;
            // 
            // txtNumeroSerieVehiculo
            // 
            this.txtNumeroSerieVehiculo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNumeroSerieVehiculo.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNumeroSerieVehiculo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtNumeroSerieVehiculo.Location = new System.Drawing.Point(264, 392);
            this.txtNumeroSerieVehiculo.MaxLength = 50;
            this.txtNumeroSerieVehiculo.Name = "txtNumeroSerieVehiculo";
            this.txtNumeroSerieVehiculo.Size = new System.Drawing.Size(296, 20);
            this.txtNumeroSerieVehiculo.TabIndex = 33;
            this.txtNumeroSerieVehiculo.Visible = false;
            this.txtNumeroSerieVehiculo.TextChanged += new System.EventHandler(this.txtNumeroSerieVehiculo_TextChanged);
            // 
            // lblSerieVehiculo
            // 
            this.lblSerieVehiculo.AutoSize = true;
            this.lblSerieVehiculo.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblSerieVehiculo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblSerieVehiculo.Location = new System.Drawing.Point(200, 392);
            this.lblSerieVehiculo.Name = "lblSerieVehiculo";
            this.lblSerieVehiculo.Size = new System.Drawing.Size(51, 13);
            this.lblSerieVehiculo.TabIndex = 602;
            this.lblSerieVehiculo.Text = "No. Serie";
            this.lblSerieVehiculo.Visible = false;
            this.lblSerieVehiculo.Click += new System.EventHandler(this.lblSerieVehiculo_Click);
            // 
            // lblTipoSerieVehiculo
            // 
            this.lblTipoSerieVehiculo.AutoSize = true;
            this.lblTipoSerieVehiculo.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblTipoSerieVehiculo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblTipoSerieVehiculo.Location = new System.Drawing.Point(8, 392);
            this.lblTipoSerieVehiculo.Name = "lblTipoSerieVehiculo";
            this.lblTipoSerieVehiculo.Size = new System.Drawing.Size(77, 13);
            this.lblTipoSerieVehiculo.TabIndex = 603;
            this.lblTipoSerieVehiculo.Text = "Fra/Pedimento";
            this.lblTipoSerieVehiculo.Visible = false;
            // 
            // cmbTipoSerieVehiculo
            // 
            this.cmbTipoSerieVehiculo.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmbTipoSerieVehiculo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTipoSerieVehiculo.FormattingEnabled = true;
            this.cmbTipoSerieVehiculo.Items.AddRange(new object[] {
            "Selecione una opción",
            "FACTURA",
            "PEDIMENTO"});
            this.cmbTipoSerieVehiculo.Location = new System.Drawing.Point(104, 392);
            this.cmbTipoSerieVehiculo.Name = "cmbTipoSerieVehiculo";
            this.cmbTipoSerieVehiculo.Size = new System.Drawing.Size(80, 21);
            this.cmbTipoSerieVehiculo.TabIndex = 32;
            this.cmbTipoSerieVehiculo.Visible = false;
            // 
            // lkbCliente
            // 
            this.lkbCliente.AutoSize = true;
            this.lkbCliente.Location = new System.Drawing.Point(8, 136);
            this.lkbCliente.Name = "lkbCliente";
            this.lkbCliente.Size = new System.Drawing.Size(39, 13);
            this.lkbCliente.TabIndex = 604;
            this.lkbCliente.TabStop = true;
            this.lkbCliente.Text = "Cliente";
            this.lkbCliente.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lkbCliente_LinkClicked);
            // 
            // frmBoletasEmpeno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(1090, 443);
            this.Controls.Add(this.lkbCliente);
            this.Controls.Add(this.cmbTipoSerieVehiculo);
            this.Controls.Add(this.lblTipoSerieVehiculo);
            this.Controls.Add(this.txtNumeroSerieVehiculo);
            this.Controls.Add(this.lblSerieVehiculo);
            this.Controls.Add(this.txtIMEI);
            this.Controls.Add(this.lblIMEI);
            this.Controls.Add(this.btnVerCancelacion);
            this.Controls.Add(this.txtPrestamoAnt);
            this.Controls.Add(this.txtPagoFecha);
            this.Controls.Add(this.txtPrendaTipotCodigoAnterior);
            this.Controls.Add(this.txtPrecioTope);
            this.Controls.Add(this.txtCodigoGramaje);
            this.Controls.Add(this.txtGramajeId);
            this.Controls.Add(this.btnBusquedaGramaje);
            this.Controls.Add(this.txtGramajeDescripcion);
            this.Controls.Add(this.lblGramaje);
            this.Controls.Add(this.btnFotografia);
            this.Controls.Add(this.txtRefrendoA);
            this.Controls.Add(this.txtPlazo);
            this.Controls.Add(this.lblEstatus);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtGramaje);
            this.Controls.Add(this.txtGramos);
            this.Controls.Add(this.lblGramos);
            this.Controls.Add(this.txtKilataje);
            this.Controls.Add(this.lblQuilataje);
            this.Controls.Add(this.txtPlazoTope);
            this.Controls.Add(this.txtComentario);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnBusqueda);
            this.Controls.Add(this.lblDescEstatus);
            this.Controls.Add(this.txtIdentificacionValidar);
            this.Controls.Add(this.btnBusquedaIdentif);
            this.Controls.Add(this.txtIdIdentificacion);
            this.Controls.Add(this.txtCodigoIdentificacion);
            this.Controls.Add(this.txtDescripcionIdentificacion);
            this.Controls.Add(this.txtSubTipoPrendaId);
            this.Controls.Add(this.txtBusquedaSubTipoPrenda);
            this.Controls.Add(this.txtPrendaTipotId);
            this.Controls.Add(this.txtBusquedaTipoPrenda);
            this.Controls.Add(this.tsAcciones);
            this.Controls.Add(this.btnFooterCierra);
            this.Controls.Add(this.btnFooterGuardar);
            this.Controls.Add(this.txtCodigoCliente);
            this.Controls.Add(this.btnVerificarHuellaCliente);
            this.Controls.Add(this.txtClienteId);
            this.Controls.Add(this.btnBusquedaCliente);
            this.Controls.Add(this.txtNombreCliente);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtAvaluo);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.txtDescripcionSubTipoPrenda);
            this.Controls.Add(this.txtCodigoSubTipoPrenda);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.txtDescripcionPrendaTipo);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txtCantidad);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.txtCodigoPrendaTipo);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtPrestamo);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtSaldoPPIs);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtRefrendadaCon);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtPagoFolio);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtDescripcionCaja);
            this.Controls.Add(this.txtCodigoCaja);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dtpFechaVencimiento);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtHora);
            this.Controls.Add(this.dtpFecha);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNombreEmpleado);
            this.Controls.Add(this.txtBoletaInicial);
            this.Controls.Add(this.txtBoleta);
            this.Controls.Add(this.lblDescripcion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBoletasEmpeno";
            this.Text = "Boletas de Empeño";
            this.Load += new System.EventHandler(this.FrmBoletasEmpeno_Load);
            this.tsAcciones.ResumeLayout(false);
            this.tsAcciones.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlazo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtBoletaInicial;
        private System.Windows.Forms.TextBox txtBoleta;
        private System.Windows.Forms.Label lblDescripcion;
        private System.Windows.Forms.TextBox txtNombreEmpleado;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox txtHora;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpFechaVencimiento;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDescripcionCaja;
        private System.Windows.Forms.TextBox txtCodigoCaja;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtPagoFolio;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtRefrendadaCon;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtSaldoPPIs;
        private System.Windows.Forms.TextBox txtPrestamo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtCantidad;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtCodigoPrendaTipo;
        private System.Windows.Forms.TextBox txtDescripcionPrendaTipo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtDescripcionSubTipoPrenda;
        private System.Windows.Forms.TextBox txtCodigoSubTipoPrenda;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.TextBox txtAvaluo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnVerificarHuellaCliente;
        private System.Windows.Forms.TextBox txtClienteId;
        private System.Windows.Forms.Button btnBusquedaCliente;
        private System.Windows.Forms.TextBox txtNombreCliente;
        private System.Windows.Forms.TextBox txtCodigoCliente;
        private System.Windows.Forms.Button btnFooterCierra;
        private System.Windows.Forms.Button btnFooterGuardar;
        private System.Windows.Forms.ToolStrip tsAcciones;
        private System.Windows.Forms.ToolStripButton tsbNuevo;
        private System.Windows.Forms.ToolStripButton tsbGuardar;
        private System.Windows.Forms.ToolStripButton tsbBuscar;
        private System.Windows.Forms.ToolStripButton btnUndo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnPrimeroPag;
        private System.Windows.Forms.ToolStripButton btnAtras;
        private System.Windows.Forms.ToolStripButton btnAdelante;
        private System.Windows.Forms.ToolStripButton btnUlitmoPag;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbImprimir;
        private System.Windows.Forms.ToolStripButton btnCierra;
        private System.Windows.Forms.TextBox txtPrendaTipotId;
        private System.Windows.Forms.Button txtBusquedaTipoPrenda;
        private System.Windows.Forms.TextBox txtSubTipoPrendaId;
        private System.Windows.Forms.Button txtBusquedaSubTipoPrenda;
        private System.Windows.Forms.TextBox txtIdentificacionValidar;
        private System.Windows.Forms.Button btnBusquedaIdentif;
        private System.Windows.Forms.TextBox txtIdIdentificacion;
        private System.Windows.Forms.TextBox txtCodigoIdentificacion;
        private System.Windows.Forms.TextBox txtDescripcionIdentificacion;
        private System.Windows.Forms.Label lblDescEstatus;
        private System.Windows.Forms.Button btnBusqueda;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtComentario;
        private System.Windows.Forms.TextBox txtPlazoTope;
        private System.Windows.Forms.Label lblQuilataje;
        private System.Windows.Forms.TextBox txtKilataje;
        private System.Windows.Forms.Label lblGramos;
        private System.Windows.Forms.TextBox txtGramos;
        private System.Windows.Forms.TextBox txtGramaje;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblEstatus;
        private System.Windows.Forms.NumericUpDown txtPlazo;
        private System.Windows.Forms.TextBox txtRefrendoA;
        private System.Windows.Forms.Button btnFotografia;
        private System.Windows.Forms.TextBox txtCodigoGramaje;
        private System.Windows.Forms.TextBox txtGramajeId;
        private System.Windows.Forms.Button btnBusquedaGramaje;
        private System.Windows.Forms.TextBox txtGramajeDescripcion;
        private System.Windows.Forms.Label lblGramaje;
        private System.Windows.Forms.TextBox txtPrecioTope;
        private System.Windows.Forms.TextBox txtPrendaTipotCodigoAnterior;
        private System.Windows.Forms.TextBox txtPagoFecha;
        private System.Windows.Forms.TextBox txtPrestamoAnt;
        private System.Windows.Forms.ToolStripButton tsbEliminar;
        private System.Windows.Forms.Button btnVerCancelacion;
        private System.Windows.Forms.TextBox txtIMEI;
        private System.Windows.Forms.Label lblIMEI;
        private System.Windows.Forms.TextBox txtNumeroSerieVehiculo;
        private System.Windows.Forms.Label lblSerieVehiculo;
        private System.Windows.Forms.Label lblTipoSerieVehiculo;
        private System.Windows.Forms.ComboBox cmbTipoSerieVehiculo;
        private System.Windows.Forms.ToolStripButton tsbReactivar;
        private System.Windows.Forms.LinkLabel lkbCliente;
    }
}