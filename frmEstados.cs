﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Configuration;



namespace EmpenosEvora
{
    public partial class frmEstados : Form
    {
        
        public Controller.clsCtrlEstado ctrlEstado;
        public Core.clsCoreView clsView;
           
        public int _intUlitmoCodigo;        
        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }
        public List<Properties_Class.clsEstado> lsDeshacerEstado;         
        
        public frmEstados()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();
         
            this.ctrlEstado = new Controller.clsCtrlEstado();
            this.ctrlEstado.Paginacion();
            this.lsDeshacerEstado = new List<Properties_Class.clsEstado>();
            this.lsDeshacerEstado.Add(this.ctrlEstado.estado);            
        }       

        private void FrmEstados_Load(object sender, EventArgs e)
        {
            txtCodigo.MaxLength = this.ctrlEstado.estado.intNumCerosConsecutivo;

            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);

        }

        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnBusqueda_Click(object sender, EventArgs e)
        {
            
            if (this.ctrlEstado.bolResutladoPaginacion)
            {
                this.ctrlEstado.CargarFrmBusqueda();                
                if (this.ctrlEstado.bolResultadoBusqueda) 
                {
                    this.llenar_formulario();
                    txtCodigo.Focus();
                }
                
               
            }
        }

        private void TsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlEstado.estado.intId == 0) return; 

            if (this.clsView.confirmacionEliminar(this.Text))
            {
                this.ctrlEstado.Eliminar();
                if (this.ctrlEstado.bolResultado)
                {        
                    //Hacer un llamado a la funcion para obtener los datos del registro(modificado)
                    this.regresarRegistro("eliminó");
                }
            }
        }

        private void TsbReactivar_Click(object sender, EventArgs e)
        {
            if (this.ctrlEstado.estado.intId == 0) return;

            if (this.clsView.confirmacionAccion(this.Text, "¿Desea reactivar este registro?"))
            {
                this.ctrlEstado.Reactivar();
                if (this.ctrlEstado.bolResultado)
                {
                    //Hacer un llamado a la funcion para obtener los datos del registro(modificado)
                    this.regresarRegistro("activó");
                }
            }
        }


        private void BtnUndo_Click(object sender, EventArgs e)
        {

            if (this.ctrlEstado.estado.intId > 0)
            {
                llenar_formulario();
            }
            else
            {
                txtCodigo.Enabled = true;
                this.limpiar_textbox();
                
            }

            txtCodigo.Focus();         
        }


        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {            
            this.ctrlEstado.PrimeroPaginacion();
            if (!this.ctrlEstado.bolResutladoPaginacion) return;
            this.llenar_formulario();           
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {            
            this.ctrlEstado.SiguentePaginacion();
            if (!this.ctrlEstado.bolResutladoPaginacion) return;
            this.llenar_formulario();
            
        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {            
            this.ctrlEstado.AtrasPaginacion();
            if (!this.ctrlEstado.bolResutladoPaginacion) return;
            this.llenar_formulario();
            
        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {            
            this.ctrlEstado.UltimoPaginacion();
            if (!this.ctrlEstado.bolResutladoPaginacion) return;
            this.llenar_formulario();
            
        }
        private void TsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();           
            this.ctrlEstado.estado.limpiarProps();

            lblDescEstatus.Visible = true;
            lblEstatus.Text = "NUEVO";
            lblEstatus.Visible = true;

            txtCodigo.Enabled = false;
            txtCodigo.Text = "AUTOGENERADO";
            txtDescripcion.Focus();


            this.clsView.ocultarGuardar(tsAcciones, btnFooterGuardar, "NUEVO");

        }

        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {
        }

        private void TxtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);

        }


        private void TxtCodigo_Leave(object sender, EventArgs e)
        {
            if (txtCodigo.Text == "") return;
            string strCodigo = this.ctrlEstado.estado.generaCodigoConsecutivo(txtCodigo.Text);
            this.ctrlEstado.Buscar(strCodigo);
            if (!this.ctrlEstado.bolResultado) 
            {
                txtCodigo.Focus();    
            }
            this.llenar_formulario();

        }

        private void txtDescripcion_Leave(object sender, EventArgs e)
        {
            if (txtDescripcion.Text != txtDescripcionAnt.Text) 
            {
                this.ctrlEstado.ValidarDescripcion(txtDescripcion.Text);
                if (this.ctrlEstado.bolResultado) 
                {
                    this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("La descripción debe ser única, favor de verificar"));
                    txtDescripcion.Text = "";
                    txtDescripcion.Focus();
                }
            }
        }

        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            txtDescripcionAnt.Text = "";
            lblDescEstatus.Visible = false;
            lblEstatus.Text = "";
            lblEstatus.Visible = false;
            txtCodigo.Enabled = true;
            txtCodigo.Focus();
            this.clsView.principal.epMensaje.Clear();



        }

        private bool validacion()
        {

            bool bolValidacion = true;
            string strCodigo = txtCodigo.Text.TrimStart();
            string strDescripcion = txtDescripcion.Text.TrimStart();

            if (strCodigo == "")
            {
                txtCodigo.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigo, this.clsView.mensajeErrorValiForm2("El campo código es obligatorio, favor de ingresar un código"));
                bolValidacion = false;
            }

            if (strDescripcion == "")
            {
                txtDescripcion.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("El campo descripción es obligatorio, favor de ingresar una descripción"));
                bolValidacion = false;
            }
            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();

            if (validacion())
            {
                if (!(this.ctrlEstado.estado.intId > 0))
                {
                    if (this.ctrlEstado.bolResutladoPaginacion)
                    {
                        DataTable dtDatos = this.ctrlEstado.objDataSet.Tables[0];
                        this.intUlitmoCodigo = Int32.Parse(dtDatos.Rows[dtDatos.Rows.Count - 1][1].ToString()) + 1;                       
                    }
                    else
                    {
                        this.intUlitmoCodigo = 1;
                    }

                    txtCodigo.Text = this.intUlitmoCodigo.ToString().PadLeft(this.ctrlEstado.estado.intNumCerosConsecutivo, '0');
                }
                

                this.ctrlEstado.estado.strCodigo = this.intUlitmoCodigo.ToString().PadLeft(this.ctrlEstado.estado.intNumCerosConsecutivo, '0');
                this.ctrlEstado.estado.strDescripcion = txtDescripcion.Text.ToString();
                this.ctrlEstado.estado.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlEstado.Guardar();

                if (this.ctrlEstado.mdlEstado.bolResultado)
                {


                    //Hacer un llamado a la funcion para obtener los datos del registro(modificado)
                    this.regresarRegistro("guardó");


                }
            }
        }

        public void llenar_formulario()
        {
            this.limpiar_textbox();
            if (this.ctrlEstado.estado.strCodigo.Length == 0) return;
            
            txtCodigo.Enabled = true;            
            txtCodigo.Text = this.ctrlEstado.estado.strCodigo;            
            this.intUlitmoCodigo = Int32.Parse(this.ctrlEstado.estado.strCodigo);
            txtDescripcion.Text = this.ctrlEstado.estado.strDescripcion;
            txtDescripcionAnt.Text = this.ctrlEstado.estado.strDescripcion;
            lblDescEstatus.Visible = true;
            lblEstatus.Text = this.ctrlEstado.estado.strEstatus;
            lblEstatus.Visible = true;
            this.ctrlEstado.estado.intUsuario = this.clsView.login.usuario.intId;

            this.clsView.permisosMovimientoMenu(tsAcciones, this.ctrlEstado.estado.strEstatus);
            this.clsView.ocultarGuardar(tsAcciones, btnFooterGuardar, this.ctrlEstado.estado.strEstatus);

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData)) 
            {
                return true;
            } 
            
            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void TsbBuscar_Click(object sender, EventArgs e)
        {
            if (this.ctrlEstado.bolResutladoPaginacion)
            {
                this.ctrlEstado.CargarFrmBusqueda();
                if (this.ctrlEstado.bolResultadoBusqueda) 
                {
                    this.llenar_formulario();
                    txtCodigo.Focus();
                }
                
            }
            
        }


        /*Metodo para regresar los datos de un registro*/
        public void regresarRegistro(string stMsj) 
        {
            this.limpiar_textbox();
            this.ctrlEstado.Paginacion();
            this.clsView.mensajeExitoDB(this.Text.ToLower(), stMsj);

            this.ctrlEstado.Buscar(this.ctrlEstado.estado.strCodigo);
            llenar_formulario();
            txtCodigo.Enabled = true;
            txtCodigo.Focus();
        }

    }
}
