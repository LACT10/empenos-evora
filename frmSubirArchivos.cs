﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmSubirArchivos : Form
    {
        

        public string _strCodigo;
        public string _strUbicacionFrontal;
        public string _strNombreFrontal;
        public string _strUbicacionAtras;
        public string _strNombreAtras;
        public string strCodigo { get => _strCodigo; set => _strCodigo = value; }
        public string strUbicacionFrontal { get => _strUbicacionFrontal; set => _strUbicacionFrontal = value; }
        public string strNombreFrontal { get => _strNombreFrontal; set => _strNombreFrontal = value; }
        public string strUbicacionAtras { get => _strUbicacionAtras; set => _strUbicacionAtras = value; }
        public string strNombreAtras { get => _strNombreAtras; set => _strNombreAtras = value; }
        





        public frmSubirArchivos()
        {
            InitializeComponent();
        }

        

        private void btnSubirFrente_Click(object sender, EventArgs e)
        {
            ofdSubirFrente.Title = "Seleccione un archivo";
            ofdSubirFrente.Filter = "PDF files|*.pdf|JPG files|*.jpg|JPEG files|*.jpeg*;";
            ofdSubirFrente.FileName = "";
            DialogResult subirFrente = ofdSubirFrente.ShowDialog();
            
            //ofdSubirFrente
            if (subirFrente == DialogResult.OK)
            {
                //Me mustra la extension del archivo
                string strExt = Path.GetExtension(ofdSubirFrente.FileName);
                string pathImagen = Application.StartupPath.Replace("bin\\Debug", "Resources\\identificacionCliente\\");
                if (strExt == ".jpg" || strExt == ".jpeg" || strExt == ".pdf")
                {
                    // Para copiar un archivo a otra ubicación y
                    // sobrescribe el archivo de destino si ya existe.
                    string nombreArchivo = pathImagen + "identificacion_imagen_frente_" + this.strCodigo + strExt;
                    this.strUbicacionFrontal = ofdSubirFrente.FileName;
                    this.strNombreFrontal = nombreArchivo;
                    //System.IO.File.Copy(ofdSubirFrente.FileName, nombreArchivo, true);
                    lblIdentificacionFrontal.Text = "Archivo se sunbio 100%";
                }
                else
                {
                    MessageBox.Show("El archivo debe de ser tipo PDF, JPG, o JPEG", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        private void btnSubirAtras_Click(object sender, EventArgs e)
        {
            ofdSubirFrente.Title = "Seleccione un archivo";
            ofdSubirFrente.Filter = "PDF files|*.pdf|JPG files|*.jpg|JPEG files|*.jpeg*;";
            ofdSubirFrente.FileName = "";
            DialogResult subirFrente = ofdSubirFrente.ShowDialog();            
            //ofdSubirFrente
            if (subirFrente == DialogResult.OK)
            {
                //Me mustra la extension del archivo
                string strExt = Path.GetExtension(ofdSubirFrente.FileName);
                string pathImagen = Application.StartupPath.Replace("bin\\Debug", "Resources\\identificacionCliente\\");
                if (strExt == ".jpg" || strExt == ".jpeg" || strExt == ".pdf")
                {
                    // Para copiar un archivo a otra ubicación y
                    // sobrescribe el archivo de destino si ya existe.
                    string nombreArchivo = pathImagen + "identificacion_imagen_atras_" + this.strCodigo + strExt;
                    this.strUbicacionAtras = ofdSubirFrente.FileName;
                    this.strNombreAtras = nombreArchivo;
                    //System.IO.File.Copy(ofdSubirFrente.FileName, nombreArchivo, true);
                    lblIdentificacionTrasera.Text = "Archivo se sunbio 100%";

                }
                else
                {
                    MessageBox.Show("El archivo debe de ser tipo PDF, JPG, o JPEG", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void frmSubirArchivos_Load(object sender, EventArgs e)
        {

        }

        private void btnCaptura_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
