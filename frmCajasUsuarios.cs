﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmCajasUsuarios : Form
    {

        public DataGridViewRow _rowsIndex;
        public bool _bolCierra;

        public DataGridViewRow rowsIndex { get => _rowsIndex; set => _rowsIndex = value; }
        public bool bolCierra { get => _bolCierra; set => _bolCierra = value; }

        public Properties_Class.clsLogin login;

        public frmCajasUsuarios()
        {
            InitializeComponent();
        }

        private void frmCajasUsuarios_Load(object sender, EventArgs e)
        {


            if (this.login.objCajas == null) return;


            dgvResultado.AutoGenerateColumns = true;

            dgvResultado.DataSource = this.login.objCajas.Tables[0].DefaultView; // dataset                    

            dgvResultado.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvResultado.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dgvResultado.AllowUserToAddRows = false;


            dgvResultado.Columns["caja_id"].Visible = false;
            dgvResultado.Columns["descripcion"].Visible = false;
            dgvResultado.Columns["codigo"].Visible = false;


            
        }

        private void dgvResultado_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dgvResultado_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.bolCierra = dgvResultado.CurrentRow.Selected;
            this.rowsIndex = dgvResultado.SelectedRows[0];

            this.bolCierra = true;
            this.Close();
        }
    }
}
