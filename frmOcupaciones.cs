﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmOcupaciones : Form
    {
        public Controller.clsCtrlOcupacion ctrlOcupacion;
        public Core.clsCoreView clsView;

        public int _intUlitmoCodigo;
        public int _intNumCerosConsecutivo;
        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }
        public int intNumCerosConsecutivo { get => _intNumCerosConsecutivo; set => _intNumCerosConsecutivo = value; }

        public List<Properties_Class.clsOcupacion> lsDeshacerOcupacion;
        
        public frmOcupaciones()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();
            this.ctrlOcupacion = new Controller.clsCtrlOcupacion();
            this.ctrlOcupacion.Paginacion();
            this.lsDeshacerOcupacion = new List<Properties_Class.clsOcupacion>();
            this.lsDeshacerOcupacion.Add(this.ctrlOcupacion.ocupacion);
            //Asignar número de ceros pata generar código consecutivo
            this.intNumCerosConsecutivo = 3;

        }
        private void Frmcajas_Load(object sender, EventArgs e)
        {
        }

        private void FrmOcupaciones_KeyDown(object sender, KeyEventArgs e)
        {
            //this.clsView.cierraFormularioEsc(this, e);
        }

        private void FrmOcupaciones_KeyPress(object sender, KeyPressEventArgs e)
        {
           // this.clsView.cierraFormularioEsc(this, e);
        }

        private void FrmOcupaciones_KeyUp(object sender, KeyEventArgs e)
        {
           // this.clsView.cierraFormularioEsc(this, e);
        }



        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlOcupacion.ocupacion.intId == 0) return;

            if (this.clsView.confirmacionEliminar("Ocupación"))
            {
                this.ctrlOcupacion.Eliminar();
                if (this.ctrlOcupacion.bolResultado)
                {
                    this.ctrlOcupacion.Paginacion();
                    this.limpiar_textbox();
                    this.clsView.mensajeExitoDB("ocupación", "elimino");
                }
            }
        }

        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlOcupacion.bolResutladoPaginacion)
            {
                txtCodigo.Text = this.lsDeshacerOcupacion[0].strCodigo.ToString();
                txtDescripcion.Text = this.lsDeshacerOcupacion[0].strDescripcion.ToString();
            }
        }

        private void tsbBuscar_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();

        }

        private void BtnBusqueda_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();
        }

        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlOcupacion.PrimeroPaginacion();
            if (!this.ctrlOcupacion.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlOcupacion.SiguentePaginacion();
            if (!this.ctrlOcupacion.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlOcupacion.AtrasPaginacion();
            if (!this.ctrlOcupacion.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlOcupacion.UltimoPaginacion();
            if (!this.ctrlOcupacion.bolResultado) return;
            this.llenar_formulario();
        }
        private void TsbNuevo_Click(object sender, EventArgs e)
        {
           
            this.limpiar_textbox();
            this.ctrlOcupacion.ocupacion.limpiarProps();
            txtCodigo.Enabled = false;

            if (this.ctrlOcupacion.bolResutladoPaginacion)
            {
                DataTable dtDatos = this.ctrlOcupacion.objDataSet.Tables[0];
                this.intUlitmoCodigo = Int32.Parse(dtDatos.Rows[dtDatos.Rows.Count - 1][1].ToString()) + 1;
                this.agregarDeshacer();
            }
            else
            {
                this.intUlitmoCodigo = 1;
            }

            txtCodigo.Text = this.intUlitmoCodigo.ToString().PadLeft(this.intNumCerosConsecutivo, '0');

        }
        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {
            txtDescripcion.Text = "";
        }

        private void TxtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);

        }
        private void txtCodigo_Leave(object sender, EventArgs e)
        {
            this.ctrlOcupacion.Buscar(txtCodigo.Text);
            if (!(this.ctrlOcupacion.bolResultado) || txtCodigo.Text.Trim().Length == 0) {
                this.limpiar_textbox();
                return;
            };
            this.llenar_formulario();
        }

        private void txtDescripcion_Leave(object sender, EventArgs e)
        {
            if (txtDescripcion.Text != txtDescripcionAnt.Text)
            {
                this.ctrlOcupacion.ValidarDescripcion(txtDescripcion.Text);
                if (this.ctrlOcupacion.bolResultado)
                {
                    this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("La descripción debe ser única, favor de verificar"));
                    txtDescripcion.Text = "";
                }
            }
        }

        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            txtCodigo.Enabled = true;
            txtDescripcionAnt.Text = "";
        }

        private bool validacion()
        {

            bool bolValidacion = true;
            string strCodigo = txtCodigo.Text.TrimStart();
            string strDescripcion = txtDescripcion.Text.TrimStart();

            if (strCodigo == "")
            {
                txtCodigo.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigo, this.clsView.mensajeErrorValiForm2("El campo código es obligatorio, favor de ingresar un código "));
                bolValidacion = false;
            }

            if (strDescripcion == "")
            {
                txtDescripcion.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("El campo descripción es obligatorio, favor de ingresar una descripción "));
                bolValidacion = false;
            }
            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();

            if (validacion())
            {
                this.ctrlOcupacion.ocupacion.strCodigo = this.intUlitmoCodigo.ToString().PadLeft(this.intNumCerosConsecutivo, '0');
                this.ctrlOcupacion.ocupacion.strDescripcion = txtDescripcion.Text.ToString();
                this.ctrlOcupacion.ocupacion.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlOcupacion.Guardar();

                if (this.ctrlOcupacion.mdlOcupacion.bolResultado)
                {
                    this.limpiar_textbox();
                    this.ctrlOcupacion.Paginacion();
                    this.clsView.mensajeExitoDB("ocupación", "guardó");

                }
            }
        }

        public void mostraBusqueda()
        {
            if (this.ctrlOcupacion.bolResutladoPaginacion)
            {
                this.ctrlOcupacion.CargarFrmBusqueda();

                if (this.ctrlOcupacion.bolResultadoBusqueda)
                {
                    this.llenar_formulario();
                    txtCodigo.Focus();
                }
            }

            
        }

        public void llenar_formulario()
        {
            if (this.ctrlOcupacion.ocupacion.strCodigo.Length == 0) return;
            txtCodigo.Text = this.ctrlOcupacion.ocupacion.strCodigo;
            txtCodigo.Enabled = true;
            txtDescripcion.Text = this.ctrlOcupacion.ocupacion.strDescripcion;
            txtDescripcionAnt.Text = this.ctrlOcupacion.ocupacion.strDescripcion;
            this.intUlitmoCodigo = Int32.Parse(this.ctrlOcupacion.ocupacion.strCodigo);
        }

        public void agregarDeshacer()
        {
            if (this.ctrlOcupacion.bolResutladoPaginacion)
            {
                this.lsDeshacerOcupacion.RemoveAt(0);
                this.lsDeshacerOcupacion.Add(this.ctrlOcupacion.ocupacion);
            }
        }

        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void FrmOcupaciones_Load(object sender, EventArgs e)
        {
            txtCodigo.MaxLength = this.intNumCerosConsecutivo;

            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);
        }


    }
}
