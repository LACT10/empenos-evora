﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmMunicipios : Form
    {
        
        public Controller.clsCtrlMunicipio ctrlMunicipio;
        public Controller.clsCtrlEstado ctrlEstado;
        public Core.clsCoreView clsView;

        public int _intUlitmoCodigo;
        public int _intNumCerosConsecutivo;
        
        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }
      
        public int intNumCerosConsecutivo { get => _intNumCerosConsecutivo; set => _intNumCerosConsecutivo = value; }

        public List<Properties_Class.clsMunicipio> lsDeshacerMunicipio;
        
        public frmMunicipios()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();
            this.ctrlMunicipio = new Controller.clsCtrlMunicipio();
            this.ctrlMunicipio.Paginacion();

            this.ctrlEstado = new Controller.clsCtrlEstado();
            this.ctrlEstado.Paginacion();

            this.lsDeshacerMunicipio = new List<Properties_Class.clsMunicipio>();
            this.lsDeshacerMunicipio.Add(this.ctrlMunicipio.municipio);
            //Asignar número de ceros pata generar código consecutivo
            this.intNumCerosConsecutivo = 5;


        }
        

        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlMunicipio.municipio.intId == 0) return;

            if (this.clsView.confirmacionEliminar("Municipios"))
            {
                this.ctrlMunicipio.Eliminar();
                if (this.ctrlMunicipio.bolResultado)
                {
                    this.ctrlMunicipio.Paginacion();
                    this.limpiar_textbox();
                    this.clsView.mensajeExitoDB("municipio", "eliminó");
                }
            }
        }

        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlMunicipio.bolResutladoPaginacion)
            {
                txtCodigo.Text = this.lsDeshacerMunicipio[0].strCodigo.ToString();
                txtDescripcion.Text = this.lsDeshacerMunicipio[0].strDescripcion.ToString();
                txtCodigoEstado.Text = this.lsDeshacerMunicipio[0].estado.strCodigo;
                txtDescripcionEstado.Text = this.lsDeshacerMunicipio[0].estado.strDescripcion;
                txtEstadoId.Text = this.lsDeshacerMunicipio[0].estado.intId.ToString();
            }
        }

        private void TsbBuscar_Click(object sender, EventArgs e)
        {
            Dictionary<int, Properties_Class.clsTextbox> dicFocus = new Dictionary<int, Properties_Class.clsTextbox>()
            {
                { 0, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcion, strBusqueda = "municipios"}
                },
                { 1, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigo, strBusqueda = "municipios"}
                },                
                { 2, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoEstado, strBusqueda = "estado"}
                },
                { 3, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcionEstado, strBusqueda = "estado"}
                }
            };
            foreach (KeyValuePair<int, Properties_Class.clsTextbox> data in dicFocus)
            {
                if (data.Value.txtGenerico.Focused)
                {
                    if (data.Value.strBusqueda == "municipios")
                    {
                        this.mostraBusqueda();
                    }
                    else if (data.Value.strBusqueda == "estado")
                    {
                        this.mostraBusquedaEstado();
                    }
                }

            }
        }

        private void BtnBusqueda_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();
        }

        private void BtnBusquedaEstado_Click(object sender, EventArgs e)
        {
            this.mostraBusquedaEstado();
        }

        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlMunicipio.PrimeroPaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlMunicipio.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlMunicipio.SiguentePaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlMunicipio.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlMunicipio.AtrasPaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlMunicipio.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlMunicipio.UltimoPaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlMunicipio.bolResultado) return;
            this.llenar_formulario();
        }
        private void TsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();
            this.ctrlMunicipio.municipio.limpiarProps();
            txtCodigo.Text = "AUTOGENERADO";
            txtCodigo.Enabled = false;
            txtDescripcion.Focus();


        }
        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {
            //txtDescripcion.Text = "";
        }

        private void TxtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);

        }

        private void txtCodigoEstado_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }
        private void TxtCodigo_Leave(object sender, EventArgs e)
        {
            this.ctrlMunicipio.Buscar(txtCodigo.Text);
            if (!(this.ctrlMunicipio.bolResultado) || txtCodigo.Text.Trim().Length == 0) {
                this.limpiar_textbox();
                return;
            }
            this.llenar_formulario();
        }

        private void txtDescripcion_Leave(object sender, EventArgs e)
        {
            if (txtDescripcion.Text != txtDescripcionAnt.Text)
            {
                this.ctrlMunicipio.ValidarDescripcion(txtDescripcion.Text);
                if (this.ctrlMunicipio.bolResultado)
                {
                    this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("La descripción debe ser única, favor de verificar"));
                    txtDescripcion.Text = "";
                }
            }
        }

        private void TxtCodigoEstado_Leave(object sender, EventArgs e)
        {
            if (txtCodigoEstado.Text == "")
            {
                txtEstadoId.Text = "";
                txtCodigoEstado.Text = "";
                txtCodigoEstado.Text = "";
                this.ctrlMunicipio.municipio.limpiarProps();
                return;
            }

            string strCodigo = this.ctrlMunicipio.municipio.estado.generaCodigoConsecutivo(txtCodigoEstado.Text);

            this.ctrlEstado.Buscar(strCodigo);
            if (!(this.ctrlEstado.bolResultado)) {
                txtEstadoId.Text = "";
                txtCodigoEstado.Text = "";
                txtDescripcionEstado.Text = "";
                txtCodigoEstado.Focus();
                return;
            }
            
            txtEstadoId.Text = this.ctrlEstado.estado.intId.ToString();
            txtCodigoEstado.Text = this.ctrlEstado.estado.strCodigo;
            txtDescripcionEstado.Text = this.ctrlEstado.estado.strDescripcion;
        }

        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            txtCodigo.Enabled = true;
            lblDescEstatus.Visible = false;
            lblEstatus.Text = "";
            txtDescripcionAnt.Text = "";
        }

        private bool validacion()
        {

            bool bolValidacion = true;
            string strCodigo = txtCodigo.Text.TrimStart();
            string strDescripcion = txtDescripcion.Text.TrimStart();
            string strDescripcionEstado = txtDescripcionEstado.Text.TrimStart();

            if (strCodigo == "")
            {
                txtCodigo.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigo, this.clsView.mensajeErrorValiForm2("El campo código es obligatorio, favor de ingresar un código "));
                bolValidacion = false;
            }

            if (strDescripcion == "")
            {
                txtDescripcion.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("El campo descripción es obligatorio, favor de ingresar una descripción "));
                bolValidacion = false;
            }

            if (strDescripcionEstado == "")
            {
                txtCodigoEstado.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigoEstado, this.clsView.mensajeErrorValiForm2("El campo estado es obligatorio, favor de ingresar una estado "));
                bolValidacion = false;
            }
            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();

            if (validacion())
            {
                if (this.ctrlMunicipio.bolResutladoPaginacion)
                {
                    DataTable dtDatos = this.ctrlMunicipio.objDataSet.Tables[0];
                    this.intUlitmoCodigo = Int32.Parse(dtDatos.Rows[dtDatos.Rows.Count - 1][1].ToString()) + 1;
                    this.agregarDeshacer();
                }
                else
                {
                    this.intUlitmoCodigo = 1;
                }

                txtCodigo.Text = this.intUlitmoCodigo.ToString().PadLeft(this.intNumCerosConsecutivo, '0');

                this.ctrlMunicipio.municipio.strCodigo = this.intUlitmoCodigo.ToString().PadLeft(this.intNumCerosConsecutivo, '0');
                this.ctrlMunicipio.municipio.strDescripcion = txtDescripcion.Text.ToString();
                this.ctrlMunicipio.municipio.estado.intId = Int32.Parse((txtEstadoId.Text == "") ? "0" : txtEstadoId.Text);
                this.ctrlMunicipio.municipio.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlMunicipio.Guardar();

                if (this.ctrlMunicipio.mdlMunicipio.bolResultado)
                {
                    this.limpiar_textbox();
                    this.ctrlMunicipio.Paginacion();
                    this.clsView.mensajeExitoDB("municipio", "guardó");

                    this.ctrlMunicipio.Buscar(this.ctrlMunicipio.municipio.strCodigo);
                    llenar_formulario();
                    txtCodigo.Enabled = true;
                    txtCodigo.Focus();

                }
            }
        }
        
        private void mostraBusqueda()
        {
            if (this.ctrlMunicipio.bolResutladoPaginacion)
            {
                this.ctrlMunicipio.CargarFrmBusqueda();

                if (this.ctrlMunicipio.bolResultadoBusqueda)
                {
                    this.ctrlEstado.estado.intId = this.ctrlMunicipio.municipio.estado.intId;
                    this.ctrlEstado.estado.strDescripcion = this.ctrlMunicipio.municipio.estado.strDescripcion;
                    this.ctrlEstado.estado.strCodigo = this.ctrlMunicipio.municipio.estado.strCodigo;
                    this.llenar_formulario();
                    txtCodigo.Focus();
                }
            }
            
        }

        private void mostraBusquedaEstado()
        {

            this.ctrlEstado.Paginacion();
            if (this.ctrlEstado.bolResutladoPaginacion)
            {
                ctrlEstado.CargarFrmBusqueda();


                if (this.ctrlEstado.bolResultadoBusqueda)
                {
                    txtEstadoId.Text = this.ctrlEstado.estado.intId.ToString();
                    txtCodigoEstado.Text = this.ctrlEstado.estado.strCodigo;
                    txtDescripcionEstado.Text = this.ctrlEstado.estado.strDescripcion;
                    txtCodigoEstado.Focus();
                }
            }
            
        }


        public void llenar_formulario()
        {

            if (this.ctrlMunicipio.municipio.strCodigo.Length == 0) return;
            txtCodigo.Text = this.ctrlMunicipio.municipio.strCodigo;
            this.intUlitmoCodigo = Int32.Parse(this.ctrlMunicipio.municipio.strCodigo);
            txtDescripcion.Text = this.ctrlMunicipio.municipio.strDescripcion;
            txtDescripcionAnt.Text = this.ctrlMunicipio.municipio.strDescripcion;
            txtEstadoId.Text = this.ctrlMunicipio.municipio.estado.intId.ToString();
            txtCodigoEstado.Text = this.ctrlMunicipio.municipio.estado.strCodigo;
            txtDescripcionEstado.Text = this.ctrlMunicipio.municipio.estado.strDescripcion;

            lblDescEstatus.Visible = true;
            lblEstatus.Text = this.ctrlMunicipio.municipio.strEstatus;

            this.clsView.permisosMovimientoMenu(tsAcciones, this.ctrlMunicipio.municipio.strEstatus);
        }

        public void agregarDeshacer()
        {
            if (this.ctrlMunicipio.bolResutladoPaginacion)
            {
                this.lsDeshacerMunicipio.RemoveAt(0);
                this.lsDeshacerMunicipio.Add(this.ctrlMunicipio.municipio);
            }
        }

        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void FrmMunicipios_Load(object sender, EventArgs e)
        {
            txtCodigo.MaxLength = this.intNumCerosConsecutivo;

            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);
        }

        private void txtCodigo_TextChanged_1(object sender, EventArgs e)
        {

        }


    }
}