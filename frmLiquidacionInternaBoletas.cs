﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmLiquidacionInternaBoletas : Form
    {

        public bool _bolPermisos;
        public string _strMotivoBonificacion;

        public string _strIndexRow;
        public decimal _decAcumCapital;
        public decimal _decAcumIntereses;
        public decimal _decAcumSubtotal;


        public decimal _decAcumPagar;
        public int _intTotalDetallesSel;


        public Controller.clsCtrlLiquidacionInternaBoletas ctrlInterna;
        public Controller.clsCtrlClientes ctrlClientes;
        public Controller.clsCtrlFoliosProcesos ctrlFoliosProcesos;
        public Controller.clCtrlFormaPago ctrlFormaPago;
        public Controller.clsCtrlBoletaEmpeno ctrlBoletaEmpeno;
        public Controller.clsCtrlSucursalParametros ctrlSucursalParametros;
        public Controller.clsCtrlSucursalesCobrosTipos ctrlSucursalCobrosTipos;
        public Controller.clsCtrlSucursalesPromocion ctrlSucursalesPromocion;
        public Controller.clsCtrlFolios ctrlFolios;
        public Controller.clsCtrlProcesos ctrlProcesos;
        public Controller.clsCtrlProcesos ctrlProcesosPagos;
        public Properties_Class.clsConstante clsConstante;

        private DPFP.Template template;
        public Core.clsCoreView clsView = new Core.clsCoreView();
        public Controller.clsCtrlUsuarios ctrlUsuario;
        public Controller.clsCtrlFolios ctrlFoliosAuto = new Controller.clsCtrlFolios();
        

        public bool bolNuevo = false;



        public bool bolPermisos { get => _bolPermisos; set => _bolPermisos = value; }

        public int intTotalDetallesSel { get => _intTotalDetallesSel; set => _intTotalDetallesSel = value; }


        public List<Properties_Class.clsRecibosLiquidacionRefrendos> lsDeshacerRefrendos;

        public List<int> lsBoletaEmpenoId;

        public string strIndexRow { get => _strIndexRow; set => _strIndexRow = value; }


        public string strMotivoBonificacion { get => _strMotivoBonificacion; set => _strMotivoBonificacion = value; }


        /*Variables que se utlizan para calcular totales*/
        public decimal decAcumCapital { get => _decAcumCapital; set => _decAcumCapital = value; }
        public decimal decAcumIntereses { get => _decAcumIntereses; set => _decAcumIntereses = value; }        
        public decimal decAcumSubtotal { get => _decAcumSubtotal; set => _decAcumSubtotal = value; }
        
        public decimal decAcumPagar { get => _decAcumPagar; set => _decAcumPagar = value; }
        public frmLiquidacionInternaBoletas()
        {
            InitializeComponent();

            this.ctrlBoletaEmpeno = new Controller.clsCtrlBoletaEmpeno();
            this.ctrlInterna = new Controller.clsCtrlLiquidacionInternaBoletas();
            this.ctrlSucursalCobrosTipos = new Controller.clsCtrlSucursalesCobrosTipos();
            this.ctrlClientes = new Controller.clsCtrlClientes();
            this.ctrlSucursalParametros = new Controller.clsCtrlSucursalParametros();

            this.clsConstante = new Properties_Class.clsConstante();

            //Procesos id de la boleta de empeño
            this.ctrlProcesos = new Controller.clsCtrlProcesos();
            this.ctrlProcesos.BuscarDescripcion("Boletas de Empeño");

            //Procesos id de la pagos
            this.ctrlProcesosPagos = new Controller.clsCtrlProcesos();
            this.ctrlProcesosPagos.BuscarDescripcion("Recibos de Liquidación y Refrendos");

            rbtAPorcentajeIntereses.CheckedChanged += new EventHandler(rbtRefrendos_CheckedChanged);
            rbtANumeroMesesDias.CheckedChanged += new EventHandler(rbtRefrendos_CheckedChanged);
            rbtCInteresesFechaLiquidacion.CheckedChanged += new EventHandler(rbtRefrendos_CheckedChanged);
            rbtCInteresesFechaVencimiento.CheckedChanged += new EventHandler(rbtRefrendos_CheckedChanged);

            this.lsBoletaEmpenoId = new List<int>();
           

            dgvDetalles.Columns["Capital"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvDetalles.Columns["IntAlmMan"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvDetalles.Columns["Subtotal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;


            this.decAcumCapital = 0;
            this.decAcumIntereses = 0;
            this.decAcumSubtotal = 0;

        }

        private void frmLiquidacionInternaBoletas_Load(object sender, EventArgs e)
        {

            this.ctrlSucursalParametros.Buscar(this.clsView.login.intSucursalId.ToString());

            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                }/*,
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }*/
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);

            rbtAPorcentajeIntereses.Checked = false;
            this.bolPermisos = tsbGuardar.Enabled;
        }

        private void tsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();
            btnUndo.Visible = true;
            this.ctrlInterna.limpiarProps();
            
            txtNombreEmpleado.Text = this.clsView.login.usuario.strNombre + " " + this.clsView.login.usuario.strApellidoPaterno + " " + this.clsView.login.usuario.strApellidoMaterno;

            txtHora.Text = DateTime.Now.ToString("HH:mm:ss");
            rbtAPorcentajeIntereses.Checked = true;
            txtCodigoCaja.Text = this.clsView.login.caja.strCodigo;
            txtDescripcionCaja.Text = this.clsView.login.caja.strDescripcion;
            dtpFecha.MinDate = DateTime.Today;
            txtPorcentaje.Text = "0.00%";
            txtBoleta.Focus();
            chkCliente.Checked = false;

            this.bolNuevo = true;

            if (this.bolPermisos)
            {
                tsbGuardar.Visible = true;
                btnFooterGuardar.Visible = true;
            }
            else
            {
                tsbGuardar.Visible = false;
                btnFooterGuardar.Visible = false;
            }
        }

        private void tsbGuardar_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            dtpFecha.MinDate = DateTime.Today;
            dtpFecha.Text = DateTime.Today.ToString();

            chkCliente.Checked = false;
            rbtAPorcentajeIntereses.Checked = false;
            rbtANumeroMesesDias.Checked = false;
            rbtCInteresesFechaLiquidacion.Checked = false;
            rbtCInteresesFechaVencimiento.Checked = false;
            txtPorcentaje.Text = "";
            dgvDetalles.Rows.Clear();
            this.lsBoletaEmpenoId.Clear();
            this.clsView.principal.epMensaje.Clear();            
            this.bolNuevo = false;
            this.strIndexRow = "";


            if (rbtAPorcentajeIntereses.Checked)
            {
                txtPorcentaje.Visible = true;
            }

            lblMeses.Visible = false;
            lblDias.Visible = false;
            txtMeses.Text = "";
            txtMeses.Visible = false;
            txtDias.Text = "";
            txtDias.Visible = false;


            tsbGuardar.Visible = true;
            btnFooterGuardar.Visible = true;                                            

        }

        //Metodo que controlar los radio buttons del formulario
        private void rbtRefrendos_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb.Checked)
            {

                if (rb.Name == "rbtAPorcentajeIntereses")
                {
                    lblPorcentaje.Visible = true;
                    txtPorcentaje.Visible = true;
                    lblMeses.Visible = false;
                    lblDias.Visible = false;                    
                    txtMeses.Visible = false;                    
                    txtDias.Visible = false;
                    limpiar_dataGridView();
                    calcularPorcentaje();


                }
                else if (rb.Name == "rbtANumeroMesesDias")
                {
                    lblPorcentaje.Visible = false;
                    txtPorcentaje.Visible = false;
                    lblMeses.Visible = true;
                    lblDias.Visible = true;                    
                    txtMeses.Visible = true;                    
                    txtDias.Visible = true;
                    limpiar_dataGridView();
                    calcularMesesDias();
                }
                else if(rb.Name == "rbtCInteresesFechaLiquidacion")
                {
                    lblPorcentaje.Visible = false;
                    txtPorcentaje.Visible = false;
                    lblMeses.Visible = false;
                    lblDias.Visible = false;                    
                    txtMeses.Visible = false;                    
                    txtDias.Visible = false;
                    limpiar_dataGridView();
                    calcularFechaAFechaInterna();
                }
                else if (rb.Name == "rbtCInteresesFechaVencimiento")
                {

                    lblPorcentaje.Visible = false;
                    txtPorcentaje.Visible = false;
                    lblMeses.Visible = false;
                    lblDias.Visible = false;                    
                    txtMeses.Visible = false;                    
                    txtDias.Visible = false;
                    limpiar_dataGridView();
                    calcularFechaAFechaVencimiento();
                }
            }


        }

        private void txtPorcentaje_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumValiDec(sender, e);
        }

        private void txtMeses_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtDias_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtPorcentaje_Leave(object sender, EventArgs e)
        {

            string strPorcentaje = txtPorcentaje.Text.Replace("%", "");
            if (decimal.Parse(strPorcentaje) > 100) return;

            txtPorcentaje.Text = this.clsView.convertPorcentaje(strPorcentaje);
            if (dgvDetalles.Rows.Count > 0) 
            {
                decimal decPorcentaje = 0;
                decimal decIntereses = 0;
                decimal decCapital = 0;
                
                this.decAcumIntereses = 0;
                this.decAcumSubtotal = 0;

                if (strPorcentaje != "") decPorcentaje = decimal.Parse(strPorcentaje) / 100;
                
                foreach (DataGridViewRow row in dgvDetalles.Rows) 
                {
                    decCapital = decimal.Parse(row.Cells["Capital"].Value.ToString().Replace("$", "").Replace(",", ""));
                    decIntereses = decCapital * decPorcentaje;
                    row.Cells["IntAlmMan"].Value = this.clsView.convertMoneda(decIntereses.ToString());
                    row.Cells["Subtotal"].Value = this.clsView.convertMoneda((decCapital + decIntereses).ToString());
                    
                    this.decAcumIntereses += decIntereses;
                    this.decAcumSubtotal += decimal.Parse(row.Cells["Subtotal"].Value.ToString().Replace("$","").Replace(",",""));
                


                }
                
                txtAcumInt.Text = this.clsView.convertMoneda(this.decAcumIntereses.ToString());
                txtAcumSubtotal.Text = this.clsView.convertMoneda(this.decAcumSubtotal.ToString());
                
            }
        }

        public void limpiar_dataGridView() 
        {
            this.decAcumCapital = 0;
            this.decAcumIntereses = 0;
            this.decAcumPagar = 0;
            foreach (DataGridViewRow row in dgvDetalles.Rows)
            {
               
                row.Cells["IntAlmMan"].Value = this.clsView.convertMoneda("0");
                row.Cells["Subtotal"].Value = row.Cells["Capital"].Value.ToString();
                this.decAcumCapital += decimal.Parse(row.Cells["Capital"].Value.ToString().Replace("$", "").Replace(",",""));
                this.decAcumIntereses += decimal.Parse(row.Cells["IntAlmMan"].Value.ToString().Replace("$", "").Replace(",", ""));
                this.decAcumPagar += decimal.Parse(row.Cells["Capital"].Value.ToString().Replace("$","").Replace(",",""));
            }

            txtAcumCapial.Text = this.clsView.convertMoneda(this.decAcumCapital.ToString());
            txtAcumInt.Text = this.clsView.convertMoneda(this.decAcumIntereses.ToString());
            txtAcumSubtotal.Text = this.clsView.convertMoneda(this.decAcumPagar.ToString());

        }

        private void txtMeses_Leave(object sender, EventArgs e)
        {

            if (txtMeses.Text == "") return;

            if (dgvDetalles.Rows.Count > 0)
            {
                calcularMesesDias();
            }
        }

        private void txtDias_Leave(object sender, EventArgs e)
        {
            if (txtDias.Text == "") return;

            if (dgvDetalles.Rows.Count > 0)
            {
                calcularMesesDias();
            }
        }
        public void calcularPorcentaje() 
        {
            decimal decPorcentaje = (txtPorcentaje.Text.Replace("%", "") == "")?0 : decimal.Parse(txtPorcentaje.Text.Replace("%", ""));
            decimal decIntereses = 0;
            decimal decCapital = 0;

            this.decAcumCapital = 0;
            this.decAcumIntereses = 0;
            this.decAcumSubtotal = 0;

            


            foreach (DataGridViewRow row in dgvDetalles.Rows)
            {

                
                decCapital = decimal.Parse(row.Cells["Capital"].Value.ToString().Replace("$", "").Replace(",", ""));
                decIntereses = (decCapital * decPorcentaje) / 100;
                row.Cells["IntAlmMan"].Value = this.clsView.convertMoneda(decIntereses.ToString());
                row.Cells["Subtotal"].Value = this.clsView.convertMoneda((decCapital + decIntereses).ToString());

                this.decAcumCapital += decCapital;
                this.decAcumIntereses += decIntereses;
                this.decAcumSubtotal += decimal.Parse(row.Cells["Subtotal"].Value.ToString().Replace("$", "").Replace(",", ""));
            }

            txtAcumCapial.Text = this.clsView.convertMoneda(this.decAcumCapital.ToString());
            txtAcumInt.Text = this.clsView.convertMoneda(this.decAcumIntereses.ToString());
            txtAcumSubtotal.Text = this.clsView.convertMoneda(this.decAcumSubtotal.ToString());
        }
        public void calcularMesesDias()
        {
            decimal decPorcentaje = 0;
            decimal decIntereses = 0;
            decimal decCapital = 0;

            this.decAcumCapital = 0;
            this.decAcumIntereses = 0;
            this.decAcumSubtotal = 0;

            int intDiasMes = (this.clsConstante.intDiasMes * int.Parse((txtMeses.Text == "") ? "0" : txtMeses.Text)) + int.Parse((txtDias.Text == "") ? "0" : txtDias.Text);

            
            foreach (DataGridViewRow row in dgvDetalles.Rows)
            {

                decPorcentaje = decimal.Parse(row.Cells["porcentajeTotal"].Value.ToString());
                decCapital = decimal.Parse(row.Cells["Capital"].Value.ToString().Replace("$", "").Replace(",", ""));
                decIntereses = ((decCapital * decPorcentaje) / 100) * intDiasMes;
                row.Cells["IntAlmMan"].Value = this.clsView.convertMoneda(decIntereses.ToString());
                row.Cells["Subtotal"].Value = this.clsView.convertMoneda((decCapital + decIntereses).ToString());

                this.decAcumCapital += decCapital;
                this.decAcumIntereses += decIntereses;
                this.decAcumSubtotal += decimal.Parse(row.Cells["Subtotal"].Value.ToString().Replace("$","").Replace(",",""));
            }

            txtAcumCapial.Text = this.clsView.convertMoneda(this.decAcumCapital.ToString());
            txtAcumInt.Text = this.clsView.convertMoneda(this.decAcumIntereses.ToString());
            txtAcumSubtotal.Text = this.clsView.convertMoneda(this.decAcumSubtotal.ToString());
        }

        public void calcularFechaAFechaInterna() 
        {
            double doubleDias = 0;
            decimal decPorcentaje = 0;
            decimal decIntereses = 0;
            decimal decCapital = 0;

            this.decAcumCapital = 0;
            this.decAcumIntereses = 0;
            this.decAcumSubtotal = 0;


            /*Don't forget to bring the 0.34 from the databas*/
            foreach (DataGridViewRow row in dgvDetalles.Rows)
            {

                decPorcentaje = decimal.Parse(row.Cells["porcentajeTotal"].Value.ToString());
                doubleDias = ( dtpFecha.Value -  Convert.ToDateTime(row.Cells["Fecha"].Value.ToString())).TotalDays;
                decCapital = decimal.Parse(row.Cells["Capital"].Value.ToString().Replace("$", "").Replace(",", ""));
                decIntereses = ((decCapital * decPorcentaje) / 100 ) * decimal.Parse(doubleDias.ToString());
                row.Cells["IntAlmMan"].Value = this.clsView.convertMoneda(decIntereses.ToString());
                row.Cells["Subtotal"].Value = this.clsView.convertMoneda((decCapital + decIntereses).ToString());


                this.decAcumCapital += decCapital;
                this.decAcumIntereses += decIntereses;
                this.decAcumSubtotal += decimal.Parse(row.Cells["Subtotal"].Value.ToString().Replace("$","").Replace(",",""));
            }

            txtAcumCapial.Text = this.clsView.convertMoneda(this.decAcumCapital.ToString());
            txtAcumInt.Text = this.clsView.convertMoneda(this.decAcumIntereses.ToString());
            txtAcumSubtotal.Text = this.clsView.convertMoneda(this.decAcumSubtotal.ToString());
        }


        public void calcularFechaAFechaVencimiento() 
        {
            double doubleDias = 0;
            decimal decPorcentaje = 0;
            decimal decIntereses = 0;
            decimal decCapital = 0;

            this.decAcumCapital = 0;
            this.decAcumIntereses = 0;
            this.decAcumSubtotal = 0;

            /*Don't forget to bring the 0.34 from the databas*/
            foreach (DataGridViewRow row in dgvDetalles.Rows)
            {

                decPorcentaje = decimal.Parse(row.Cells["porcentajeTotal"].Value.ToString());
                doubleDias = (Convert.ToDateTime(row.Cells["Vence"].Value.ToString()) - Convert.ToDateTime(row.Cells["Fecha"].Value.ToString()) ).TotalDays;
                decCapital = decimal.Parse(row.Cells["Capital"].Value.ToString().Replace("$", "").Replace(",", ""));
                decIntereses = ((decCapital * decPorcentaje) / 100) * decimal.Parse(doubleDias.ToString());
                row.Cells["IntAlmMan"].Value = this.clsView.convertMoneda(decIntereses.ToString());
                row.Cells["Subtotal"].Value = this.clsView.convertMoneda((decCapital + decIntereses).ToString());


                this.decAcumCapital += decCapital;
                this.decAcumIntereses += decIntereses;
                this.decAcumSubtotal += decimal.Parse(row.Cells["Subtotal"].Value.ToString().Replace("$", "").Replace(",", ""));
            }

            txtAcumCapial.Text = this.clsView.convertMoneda(this.decAcumCapital.ToString());
            txtAcumInt.Text = this.clsView.convertMoneda(this.decAcumIntereses.ToString());
            txtAcumSubtotal.Text = this.clsView.convertMoneda(this.decAcumSubtotal.ToString());
        }

        private void txtBoleta_Leave(object sender, EventArgs e)
        {

            if (txtNombreEmpleado.Text == "") 
            {
                this.clsView.mensajeErrorValiForm2("No es posible agregar boleta, favor de presionar el icono nuevo");
                return;
            }

            if (txtBoleta.Text == "") return;

            bool bolBoletaCaputrada = false;
            this.ctrlFoliosAuto.BuscarFolioProceso(this.ctrlProcesos.procesos.intId.ToString(), this.clsView.login.intSucursalId.ToString());
            if (txtBoleta.Text.Length < this.ctrlBoletaEmpeno.boletasEmpeno.intNumCerosConsecutivo)
            {
                if (this.ctrlFoliosAuto.folio.intConsecutivo == 0)
                {
                    this.clsView.mensajeErrorValiForm2("No es posible generar el folio, comuníquese con el administrador");
                }
                else
                {
                    txtBoleta.Text = this.ctrlFoliosAuto.folio.regresarFolioConsecutivoAuto(this.ctrlBoletaEmpeno.boletasEmpeno.intNumCerosConsecutivo, txtBoleta.Text);

                    //Si el cliente varios esta en checked entonces se puede tener boletas que estan en un estatus ACTIVO y ADJUDICADA.
                    if (chkCliente.Checked)
                    {
                        this.ctrlBoletaEmpeno.BuscarActivoAdjudicada(txtBoleta.Text, this.clsView.login.intSucursalId.ToString());
                    }
                    else 
                    {
                        if (dgvDetalles.Rows.Count > 0)
                        {
                            this.ctrlBoletaEmpeno.BuscarActivoAdjudicadaCliente(txtBoleta.Text, this.clsView.login.intSucursalId.ToString(),  dgvDetalles.Rows[0].Cells["cliente_id"].Value.ToString());
                            if (!this.ctrlBoletaEmpeno.mdlBoletaEmpeno.bolResultado)
                            {
                                txtBoleta.Text = "";
                                this.clsView.mensajeErrorValiForm2("Favor de ingresar una boleta del cliente "+dgvDetalles.Rows[0].Cells["Cliente"].Value.ToString());
                                txtBoleta.Focus();
                                return;
                            }
                        }
                        else 
                        {
                            this.ctrlBoletaEmpeno.BuscarActivoAdjudicada(txtBoleta.Text, this.clsView.login.intSucursalId.ToString());
                        }
                    }

                   
                    if (this.ctrlBoletaEmpeno.mdlBoletaEmpeno.bolResultado)
                    {
                        DateTime dtVencimiento = this.ctrlBoletaEmpeno.boletasEmpeno.dtFechaVencimiento.AddDays(this.ctrlSucursalParametros.sucursalParametro.intDiasLiquidarBoletas + this.ctrlSucursalParametros.sucursalParametro.intDiasRefrendarBoletas);

                        if (dtVencimiento < dtpFecha.Value)
                        {
                            /*En caso que ya existe en mismo folio en el data grid vie, se va ser un return*/
                            foreach (DataGridViewRow row in dgvDetalles.Rows)
                            {
                                if (row.Cells["Boleta"].Value.ToString() == this.ctrlBoletaEmpeno.boletasEmpeno.strFolio)
                                {
                                    bolBoletaCaputrada = true;
                                    this.clsView.mensajeErrorValiForm2("La boleta ya fué capturada");
                                    break;
                                }
                            }

                            //Si encuentro una boleta capturada en el datagridview entonces haces un return
                            if (bolBoletaCaputrada)
                            {
                                txtBoleta.Text = "";
                                return;
                            }

                            decimal decInteres = 0;
                            decimal decImporte = 0;

                            int intCobroTipoIdInteres = 0;
                            int intCobroTipoIdAlmacenaje = 0;
                            int intCobroTipoIdManejo = 0;

                            string strCobroTipoId = "";
                            string strCobroTipoNombres = "";

                            decimal decPorcentajeInteres = 0;
                            decimal decPorcentajeAlmacenaje = 0;
                            decimal decPorcentajeManejo = 0;
                            decimal decPrestamo = 0;

                            string strPorcentajeCobrosTipo = "";

                            decimal decImporteInteres = 0;
                            decimal decImporteAlmacenaje = 0;
                            decimal decImporteManejo = 0;


                            string strImporteCobrosTipo = "";

                            decimal decPorcentajeTotal = 0;


                            //decImporte = decimal.Parse(row["importe"].ToString();
                            DateTime dtpFechaDB = this.ctrlBoletaEmpeno.boletasEmpeno.dtFechaBoleta;
                            this.ctrlSucursalCobrosTipos.Paginacion(this.clsView.login.intSucursalId);
                            //Hacer recorido para obtener valores de los tipo de cobros que se van a utilizar en refrendos 
                            foreach (DataRow rowCT in this.ctrlSucursalCobrosTipos.objDataSet.Tables[0].Rows)
                            {
                                //Dependiendo de tipo de cobros asignar valores
                                if (rowCT["descripcion"].ToString() == "INTERES")
                                {


                                    intCobroTipoIdInteres = int.Parse(rowCT["cobro_tipo_id"].ToString());
                                    decPorcentajeInteres = decimal.Parse(rowCT["porcentaje"].ToString()) / this.clsConstante.intDiasMesCuota;
                                    decPorcentajeTotal += decPorcentajeInteres;

                                    strCobroTipoId += rowCT["cobro_tipo_id"].ToString() + "|";
                                    strCobroTipoNombres += rowCT["descripcion"].ToString() + "|";
                                    strPorcentajeCobrosTipo += decPorcentajeInteres.ToString() + "|";

                                }
                                else if (rowCT["descripcion"].ToString() == "ALMACENAJE")
                                {
                                    intCobroTipoIdAlmacenaje = int.Parse(rowCT["cobro_tipo_id"].ToString());
                                    decPorcentajeAlmacenaje = decimal.Parse(rowCT["porcentaje"].ToString()) / this.clsConstante.intDiasMesCuota;
                                    decPorcentajeTotal += decPorcentajeAlmacenaje;

                                    strCobroTipoId += rowCT["cobro_tipo_id"].ToString() + "|";
                                    strCobroTipoNombres += rowCT["descripcion"].ToString() + "|";
                                    strPorcentajeCobrosTipo += decPorcentajeAlmacenaje.ToString() + "|";
                                }
                                else if (rowCT["descripcion"].ToString() == "MANEJO")
                                {
                                    intCobroTipoIdManejo = int.Parse(rowCT["cobro_tipo_id"].ToString());
                                    decPorcentajeManejo = decimal.Parse(rowCT["porcentaje"].ToString()) / this.clsConstante.intDiasMesCuota;
                                    decPorcentajeTotal += decPorcentajeManejo;

                                    strCobroTipoId += rowCT["cobro_tipo_id"].ToString() + "|";
                                    strCobroTipoNombres += rowCT["descripcion"].ToString() + "|";
                                    strPorcentajeCobrosTipo += decPorcentajeManejo.ToString() + "|";
                                }
                            }


                            decPrestamo = this.ctrlBoletaEmpeno.boletasEmpeno.decPrestamo;

                            /*Calcular importe de interes por cada tipo de cobro*/
                            if (decPorcentajeInteres > 0)
                            {

                                strImporteCobrosTipo += decImporteInteres.ToString() + "|";
                            }

                            if (decPorcentajeAlmacenaje > 0)
                            {

                                strImporteCobrosTipo += decImporteAlmacenaje.ToString() + "|";
                            }

                            if (decPorcentajeManejo > 0)
                            {

                                strImporteCobrosTipo += decImporteManejo.ToString() + "|";
                            }

                            double doubleDias = 0;
                            decimal decIntAlmMan = 0;
                            decimal decSubtotal = 0;


                            if (rbtAPorcentajeIntereses.Checked)
                            {
                                if (txtPorcentaje.Text.Replace("%", "") != "")
                                {
                                    decIntAlmMan = (this.ctrlBoletaEmpeno.boletasEmpeno.decPrestamo * decimal.Parse(txtPorcentaje.Text.Replace("%", "")) / 100);
                                    //decIntAlmMan = this.ctrlBoletaEmpeno.boletasEmpeno.decPrestamo * (decimal.Parse(txtPorcentaje.Text.Replace("%", "")) / 100);

                                }
                            }
                            else if (rbtANumeroMesesDias.Checked)
                            {
                                int intDiasMes = (this.clsConstante.intDiasMes * int.Parse((txtMeses.Text == "") ? "0" : txtMeses.Text)) + int.Parse((txtDias.Text == "") ? "0" : txtDias.Text);
                                decIntAlmMan = ((this.ctrlBoletaEmpeno.boletasEmpeno.decPrestamo * decPorcentajeTotal) / 100) * intDiasMes;
                            }
                            else if (rbtCInteresesFechaLiquidacion.Checked)
                            {
                                doubleDias = (dtpFecha.Value - Convert.ToDateTime(this.ctrlBoletaEmpeno.boletasEmpeno.dtFechaBoleta.ToString("dd/MM/yyyy"))).TotalDays;
                                decIntAlmMan = ((this.ctrlBoletaEmpeno.boletasEmpeno.decPrestamo * decPorcentajeTotal) / 100) * decimal.Parse(doubleDias.ToString());
                            }
                            else if (rbtCInteresesFechaVencimiento.Checked)
                            {

                                doubleDias = (Convert.ToDateTime(this.ctrlBoletaEmpeno.boletasEmpeno.dtFechaVencimiento.ToString("dd/MM/yyyy")) - Convert.ToDateTime(this.ctrlBoletaEmpeno.boletasEmpeno.dtFechaBoleta.ToString("dd/MM/yyyy"))).TotalDays;
                                decIntAlmMan = ((this.ctrlBoletaEmpeno.boletasEmpeno.decPrestamo * decPorcentajeTotal) / 100) * decimal.Parse(doubleDias.ToString());
                            }


                            decSubtotal = this.ctrlBoletaEmpeno.boletasEmpeno.decPrestamo + decIntAlmMan;
                            dgvDetalles.Rows.Add(
                                            this.ctrlBoletaEmpeno.boletasEmpeno.cliente.intId,//cliente_id
                                            decPorcentajeTotal, //porcentajeTotal
                                            this.ctrlBoletaEmpeno.boletasEmpeno.prendaTipo.intId,//prendaTipoId
                                            strCobroTipoId,//CobroTipoId
                                            strPorcentajeCobrosTipo,//porcentajeCobroTipo
                                            this.ctrlBoletaEmpeno.boletasEmpeno.intId,//boleta_empeno_id
                                            this.ctrlBoletaEmpeno.boletasEmpeno.strFolio,//boleta
                                            this.ctrlBoletaEmpeno.boletasEmpeno.cliente.strNombre,//Cliente
                                            this.ctrlBoletaEmpeno.boletasEmpeno.dtFechaBoleta.ToString("dd/MM/yyyy"),//Fecha
                                            this.ctrlBoletaEmpeno.boletasEmpeno.dtFechaVencimiento.ToString("dd/MM/yyyy"),//Vence
                                            this.clsView.convertMoneda(this.ctrlBoletaEmpeno.boletasEmpeno.decPrestamo.ToString()),//Capital
                                            this.clsView.convertMoneda(decIntAlmMan.ToString()),//IntAlmMan
                                            this.clsView.convertMoneda(decSubtotal.ToString())//subtotal
                                       );


                            this.decAcumCapital += this.ctrlBoletaEmpeno.boletasEmpeno.decPrestamo;
                            this.decAcumIntereses += decIntAlmMan;
                            this.decAcumSubtotal += this.ctrlBoletaEmpeno.boletasEmpeno.decPrestamo;

                            txtAcumCapial.Text = this.clsView.convertMoneda(this.decAcumCapital.ToString());
                            txtAcumInt.Text = this.clsView.convertMoneda(this.decAcumIntereses.ToString());
                            txtAcumSubtotal.Text = this.clsView.convertMoneda(this.decAcumSubtotal.ToString());
                            txtBoleta.Text = "";

                        }
                        else
                        {
                            txtBoleta.Text = "";
                            this.clsView.mensajeErrorValiForm2("La boleta no esta vencida, favor de verificar");

                        }


                    }
                    else 
                    {
                        this.clsView.mensajeErrorValiForm2("Debe ser una Boleta Activa ó Adjudicada");
                        txtBoleta.Text = "";
                    }

                    txtBoleta.Focus();
                }
            }
        }

        public bool validacion() 
        {
            bool bolValidacion = true;
            decimal decBoleta = txtPlazo.Value;



            if (decBoleta == 0)
            {
                txtPlazo.Focus();
                this.clsView.principal.epMensaje.SetError(txtPlazo, this.clsView.mensajeErrorValiForm2("El campo No. de boletas por recibo es obligatorio, favor de ingresar un numero"));
                bolValidacion = false;
            }

            if (dgvDetalles.Rows.Count == 0)
            {

                this.clsView.mensajeErrorValiForm2("Agregar al menos una boleta para este liquidación interna ");
                bolValidacion = false;
            }



            return bolValidacion; 
        }
        private void btnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        public void guardar() 
        {
            if (validacion())
            {

                if (rbtAPorcentajeIntereses.Checked && decimal.Parse(txtPorcentaje.Text.Replace("%", "")) == 0)
                {
                    if (!this.clsView.confirmacionAccion(this.Text, "¿Desea generar los recibos sin intereses?"))
                    {
                        return;
                    }
                }

                /*=============================================================================================================
                 *                  
                =============================================================================================================*/
                decimal decNoBoleta = txtPlazo.Value;
                decimal i = 0;
                decimal decTotalRecibo = (dgvDetalles.Rows.Count / decNoBoleta);
                string strClienteId = "";
                
                string strFechaPago = dtpFecha.Value.ToString("yyyy-MM-dd") + " " + txtHora.Text;

                this.ctrlInterna.internaBoletas.lsQuerysTraccionesPagos = new List<string>();
                string strQueryPagos = "";

                if (chkCliente.Checked)
                {
                    this.ctrlClientes.Buscar(this.clsConstante.strCodigoClientesVarios);
                    if (this.ctrlClientes.mdlClientes.bolResultado)
                    {
                        strClienteId = this.ctrlClientes.cliente.intId.ToString();
                    }
                }
                else
                {
                    strClienteId = dgvDetalles.Rows[0].Cells["cliente_id"].Value.ToString();
                }

                for (decimal count = 0; count < decTotalRecibo; count++)
                {
                    strQueryPagos = "INSERT INTO pagos(sucursal_id, caja_id, usuario_id, folio, fecha, cliente_id, tipo_recibo, total_pagar, importe_pagado, estatus, fecha_creacion, usuario_creacion) ";
                    strQueryPagos += String.Format(" Values({0}, {1}, {2}, {3}, '{4}', {5}, '{6}', {7}, {8}, '{9}', '{10}', {11});", this.clsView.login.intSucursalId, this.clsView.login.caja.intId, this.clsView.login.usuario.intId, "@folio_pago", strFechaPago, strClienteId, "LIQUIDACION", "@total_pagar", "0", "LIQUIDACION INTERNA", dtpFecha.Value.ToString("yyyy-MM-dd HH:mm:ss"), this.clsView.login.usuario.intId);
                    this.ctrlInterna.internaBoletas.lsQuerysTraccionesPagos.Add(strQueryPagos);
                }

                /*=============================================================================================================
                 *                  
                 =============================================================================================================*/
                this.ctrlInterna.internaBoletas.strQueryBoletaEmpenoEstatus = " UPDATE boletas_empeno " +
                                                                            " SET estatus = 'LIQUIDACION INTERNA', " +
                                                                            "     fecha_liquidacion_interna = '{0}', " +
                                                                            "     usuario_liquidacion_interna = {1}" +
                                                                            " WHERE boleta_empeno_id IN({2}); ";
                string strQueryPagosDetalles = "INSERT INTO pagos_detalles( pago_id,  renglon,  boleta_empeno_id, meses_intereses, dias_intereses, porcentaje_interes_interna) VALUES";
                string strQueryPagosDetallesValuesTemp = "";
                string strQueryPagosDetallesValuesTemplate = "({0}, {1}, {2}, {3}, {4}, {5}) ||";
                string strEstatusUpdateBoletasIds = "";

                //Cadena para concatenar query de pagos detalles cobors tipos
                string strQueryPagosDetallesCobrosTipos = "";

                this.ctrlInterna.internaBoletas.lsQuerysTraccionesPagosDetalles = new List<string>();
                this.ctrlInterna.internaBoletas.lsQuerysTraccionesPagosDetallesCobrosTipos = new List<string>();
                string strQuerysTraccionesPagosDetallesCobrosTipos = " INSERT INTO pagos_detalles_cobros_tipos(pago_id, renglon_detalle, renglon, cobro_tipo_id, tipo , importe, porcentaje) VALUES ";
                int intRegnlon = 0;
                int intRegnlonAumentada = 0;
                int intReglonDetallesCobrosTipo = 0;
                int intMes = 0;
                int intDias = 0;
                int intStartIndex = 0;
                decimal decPorcentajeInteres = 0;
                decimal decPorcentajeCobroTipo = 0;
                int countadorPorcentajeCobroTipo = 0;
                decimal decPagarTotal = 0;
                int intContadorPagos = 0;
                decimal decImporteInteres = 0;



                if (rbtAPorcentajeIntereses.Checked)
                {
                    decPorcentajeInteres = decimal.Parse(txtPorcentaje.Text.ToString().Replace("%", ""));
                }
                else if (rbtANumeroMesesDias.Checked)
                {
                    intMes = int.Parse(txtMeses.Text);
                    intDias = int.Parse(txtDias.Text);
                }

                foreach (DataGridViewRow row in dgvDetalles.Rows)
                {
                    strEstatusUpdateBoletasIds += row.Cells["boleta_empeno_id"].Value.ToString() + ",";
                    decPagarTotal += decimal.Parse(row.Cells["Subtotal"].Value.ToString().Replace("$", "").Replace(",",""));

                    intRegnlonAumentada = ++intRegnlon;
                    strQueryPagosDetallesValuesTemp += String.Format(strQueryPagosDetallesValuesTemplate, "@pago_id", intRegnlonAumentada, row.Cells["boleta_empeno_id"].Value, intMes, intDias, decPorcentajeInteres);

                    string[] cobrosTipoId = row.Cells["cobroTipoId"].Value.ToString().Split('|');
                    string[] porcentajeCobroTipo = row.Cells["porcentajeCobroTipo"].Value.ToString().Split('|');

                    foreach (string cobros in cobrosTipoId)
                    {

                        if (porcentajeCobroTipo[countadorPorcentajeCobroTipo] == "")
                        {
                            countadorPorcentajeCobroTipo = 0;
                            break;
                        }

                        
                        decPorcentajeCobroTipo = decimal.Parse(porcentajeCobroTipo[countadorPorcentajeCobroTipo]);
                        decImporteInteres = (decimal.Parse(row.Cells["IntAlmMan"].Value.ToString().Replace("$", "").Replace(",", "")) * decPorcentajeCobroTipo) / decimal.Parse(row.Cells["porcentajeTotal"].Value.ToString());

                        strQueryPagosDetallesCobrosTipos += " (@pago_id ," +
                                       " " + intRegnlonAumentada + " , " +
                                       " " + ++intReglonDetallesCobrosTipo + " , " +
                                       " " + cobros + " ," +
                                       " '" + "INTERESES" + "' , " +
                                       " " + decImporteInteres + " , " +
                                       " " + decimal.Parse(porcentajeCobroTipo[countadorPorcentajeCobroTipo]) + ") ||";
                        countadorPorcentajeCobroTipo++;
                    }


                    //Incrementar el contador
                    i++;

                    if (intRegnlon == decNoBoleta || dgvDetalles.Rows.Count == i)
                    {

                        intStartIndex = strQueryPagosDetallesValuesTemp.LastIndexOf("||");
                        strQueryPagosDetallesValuesTemp = strQueryPagosDetallesValuesTemp.Remove(intStartIndex).Insert(intStartIndex, ";").Replace("||", ",");


                        intStartIndex = strQueryPagosDetallesCobrosTipos.LastIndexOf("||");
                        strQueryPagosDetallesCobrosTipos = strQueryPagosDetallesCobrosTipos.Remove(intStartIndex).Insert(intStartIndex, ";").Replace("||", ",");

                        this.ctrlInterna.internaBoletas.lsQuerysTraccionesPagos[intContadorPagos] = this.ctrlInterna.internaBoletas.lsQuerysTraccionesPagos[intContadorPagos].Replace("@total_pagar", decPagarTotal.ToString());
                        this.ctrlInterna.internaBoletas.lsQuerysTraccionesPagosDetalles.Add(strQueryPagosDetalles + strQueryPagosDetallesValuesTemp);
                        this.ctrlInterna.internaBoletas.lsQuerysTraccionesPagosDetallesCobrosTipos.Add(strQuerysTraccionesPagosDetallesCobrosTipos + strQueryPagosDetallesCobrosTipos);

                        //Contador para saber a que pagos detalles le pertence al pago
                        intContadorPagos++;
                        //Reniciar variables
                        intRegnlon = 0;
                        intStartIndex = 0;
                        decPagarTotal = 0;                        
                        strQueryPagosDetallesValuesTemp = "";
                        strQueryPagosDetallesCobrosTipos = "";
                    }

                    //Reniciar countador de detalles de pago
                    intReglonDetallesCobrosTipo = 0;
                }

                int intStartIndexBoletasIds = strEstatusUpdateBoletasIds.LastIndexOf(",");
                strEstatusUpdateBoletasIds = strEstatusUpdateBoletasIds.Remove(intStartIndexBoletasIds).Insert(intStartIndex, "");

                this.ctrlInterna.internaBoletas.strQueryBoletaEmpenoEstatus = String.Format(this.ctrlInterna.internaBoletas.strQueryBoletaEmpenoEstatus, strFechaPago, this.clsView.login.usuario.intId, strEstatusUpdateBoletasIds); 
                
                //Trame el folio id del pago
                this.ctrlInterna.internaBoletas.intFolioIdPago = this.ctrlProcesosPagos.procesos.intId;
                this.ctrlInterna.internaBoletas.intSucursal = this.clsView.login.intSucursalId;
                this.ctrlInterna.internaBoletas.intUsuario = this.clsView.login.usuario.intId;

                this.ctrlInterna.Guardar();

                if (this.ctrlInterna.mdlInternaBoletas.bolResultado) 
                {
                    //Variable que se utiliza para asignar el mensaje informativo
                    string strMsg = "";
                    int intTotalFolio = this.ctrlInterna.mdlInternaBoletas.lsFoliosGenerados.Count;
                    string strPrimerFolio = this.ctrlInterna.mdlInternaBoletas.lsFoliosGenerados[0];
                    string strUtlimoFolio = this.ctrlInterna.mdlInternaBoletas.lsFoliosGenerados[intTotalFolio - 1];
                    //Si se generó un recibo
                    if (intTotalFolio == 1) 
                    {
                        strMsg = "Se generó el recibo "+strPrimerFolio+ ". Para imprimirlo";
                    }
                    else
                    {
                        strMsg = "Se generaron los recibos del "+strPrimerFolio+" al "+strUtlimoFolio + ". Para imprimirlos";
                    }

                    strMsg  += " pasar a Procesos / Recibos de Liquidación y Refrendos";

                    this.limpiar_textbox();
                    MessageBox.Show(strMsg, "Éxito", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                    
                }
            }
        }

        private void dgvDetalles_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {

                try
                {
                    dgvDetalles.CurrentRow.Selected = true;
                    int dgvIndex = dgvDetalles.CurrentRow.Index;


                    if (this.dgvDetalles.Rows.Count > 0)
                    {

                        this.decAcumCapital -= decimal.Parse(dgvDetalles.CurrentRow.Cells["Capital"].Value.ToString().Replace("$", "").Replace(",", ""));
                        this.decAcumIntereses -= decimal.Parse(dgvDetalles.CurrentRow.Cells["IntAlmMan"].Value.ToString().Replace("$", "").Replace(",", ""));
                        this.decAcumSubtotal -= decimal.Parse(dgvDetalles.CurrentRow.Cells["Subtotal"].Value.ToString().Replace("$", "").Replace(",", ""));

                        txtAcumCapial.Text = this.clsView.convertMoneda(this.decAcumCapital.ToString());
                        txtAcumInt.Text = this.clsView.convertMoneda(this.decAcumIntereses.ToString());
                        txtAcumSubtotal.Text = this.clsView.convertMoneda(this.decAcumSubtotal.ToString());

                        dgvDetalles.Rows.RemoveAt(dgvIndex);
                    }

                    e.Handled = true;

                }
                catch (Exception ex) { }

            }
        }

        private void tsbImprimir_Click(object sender, EventArgs e)
        {

        }

        private void btnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void chkCliente_CheckedChanged(object sender, EventArgs e)
        {
            
            if (!chkCliente.Checked) 
            {
                if (dgvDetalles.Rows.Count > 0) 
                {
                    dgvDetalles.Rows.Clear();
                }
            }


        }
    }
}
