﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmPrincipal : Form
    {


        public bool _bolAcessoCaja;

       public  Properties_Class.clsLogin clsLogin;

        public bool bolAcessoCaja { get => _bolAcessoCaja; set => _bolAcessoCaja = value; }

        /*La lista indica que proceso llevan cajas, se van a mostrar en caso de que el usuario tenga una caja asignada */
        public List<string> lsAcessoCajas = new List<string>() 
        {
            "tsmBoletasDeEmpeno",
            "tsmRecibosLiqRefrendos",
            "tsmMovimientosCaja"
        };

        public frmPrincipal()
        {
            InitializeComponent();
            
        }


        private void FrmPrincipal_Load(object sender, EventArgs e)
        {

            muPrincipal.Visible = false;
            frmLogin formulario = new frmLogin();
            formulario.ShowDialog();


            if (formulario.bolResultado)
            {

                this.bolAcessoCaja = false;

                this.clsLogin = formulario.ctrlLogin.login;
                 

                this.mostraCajas();

                this.Text = this.Text + " " + "[Empresa: " + this.clsLogin.strEmpresa + "] [Sucursal: "+ this.clsLogin.strSucursal + "] [Usuario: "+ this.clsLogin.usuario.strUsuario + " - " + this.clsLogin.usuario.strNombre + " " + this.clsLogin.usuario.strApellidoPaterno + " " + this.clsLogin.usuario.strApellidoMaterno + "]";
                tlsEmpresa.Text = tlsEmpresa.Text + " " + this.clsLogin.strEmpresa;
                tlsSucursal.Text = tlsSucursal.Text + " " + this.clsLogin.strSucursal;
                tlsUsuario.Text = tlsUsuario.Text + " " + this.clsLogin.usuario.strUsuario + " - " + this.clsLogin.usuario.strNombre + " " + this.clsLogin.usuario.strApellidoPaterno + " " + this.clsLogin.usuario.strApellidoMaterno;

                if (this.clsLogin.objProcesos != null)
                {

                    foreach (ToolStripMenuItem row in muPrincipal.Items)
                    {

                        if (row.DropDownItems.Count > 0)
                        {
                            this.subMenu(row.DropDownItems, false);
                        }
                    }
                    muPrincipal.Visible = true;
                }

                if (this.bolAcessoCaja)
                {

                    tsmBoletasDeEmpeno.Visible = true;
                    tsmRecibosLiqRefrendos.Visible = true;
                    tsmMovimientosCaja.Visible = true;
                }
                /* if (this.clsLogin.objCajas != null) 
                 {
                     this.mostraCajas();
                 }*/
                //Es saber si el usuario tiene permisos al la pantalla



            }

        }


        public void subMenu(ToolStripItemCollection item, bool bolMinElemento) 
        {
            //El bool es para saber si un elemento tiene sus hijos con permiso de acesso al usuario que se encuentra logeado al sistema.
            //bool bolMinElemento = false;
            foreach (ToolStripMenuItem row in item)
            {

                if (row.DropDownItems.Count > 0)
                {
                    this.subMenu(row.DropDownItems, bolMinElemento);
                }
                else
                {

                   
                        foreach (DataRow rowPro in this.clsLogin.objProcesos.Tables[0].Rows)
                        {
                            if (row.Name.ToString() == rowPro["nombre_menu"].ToString())
                            {
                                row.Visible = true;
                                row.Tag = rowPro["proceso_id"].ToString();
                                bolMinElemento = true;

                                //Hacer recorido para obtener los procesos que se tienen que asignar a una caja.
                                foreach (string rowCaja in this.lsAcessoCajas)
                                {
                                    if (rowPro["nombre_menu"].ToString() == rowCaja) 
                                    {
                                        if (this.clsLogin.caja.intId > 0)
                                        {
                                            this.bolAcessoCaja = true;
                                        }
                                        else
                                        {
                                            row.Visible = false;
                                        }
                                    }

                                }                          
                            break;
                            }

                            

                        }
                    }
                    
                

                if (bolMinElemento)
                {
                    bolMinElemento = false;
                    
                    row.OwnerItem.Visible = true;
                    this.padreMenu(row.OwnerItem);

                }


            }

          

        }


        public void padreMenu(ToolStripItem row) 
        {
            if (row.OwnerItem != null) 
            {
                row.OwnerItem.Visible = true;
                this.padreMenu(row.OwnerItem);
            }
            
        }

        private void MstEstado_Click(object sender, EventArgs e)
        {

            /*if (this.FormularioAbierto("frmEstados")) return;
            frmEstados formulario = new frmEstados();
            formulario.clsView.login = this.clsLogin;   
            formulario.Show();
            formulario.MdiParent = this;*/
            //estado.ShowDialog();
        }



        private void ToolStripButton1_Click(object sender, EventArgs e)
        {
           

        }



        private void MstMunicipios_Click(object sender, EventArgs e)
        {
            frmMunicipios formulario = new frmMunicipios();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmMunicipios.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void MstColonias_Click(object sender, EventArgs e)
        {
            
        }

        private void MstTiposPrendas_Click(object sender, EventArgs e)
        {
            frmTiposDePrendas formulario = new frmTiposDePrendas();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmTiposDePrendas.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void MstPrendas_Click(object sender, EventArgs e)
        {
           /* frmPrendas formulario = new frmPrendas();
            formulario.Show();
            formulario.MdiParent = this;*/
        }

        private void VerificarHuellaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void SucursalesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //frmSucursales formulario = new frmSucursales();
            frmSucursales formulario = new frmSucursales();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmSucursales.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void EstadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmEstados")) return;
            frmEstados formulario = new frmEstados();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmEstados.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void SucursalesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmSucursales")) return;
            frmSucursales formulario = new frmSucursales();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmSucursales.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void EmpresasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmEmpresas")) return;
            frmEmpresas formulario = new frmEmpresas();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmEmpresas.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void MunicipiosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmMunicipios")) return;
            frmMunicipios formulario = new frmMunicipios();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmMunicipios.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }



        private void ColoniasToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void TiposDePrendasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmTiposDePrendas")) return;
            frmTiposDePrendas formulario = new frmTiposDePrendas();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmTiposDePrendas.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void PrendasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*frmPrendas formulario = new frmPrendas();
            formulario.Show();
            formulario.MdiParent = this;*/
        }

        private void ClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmClientes")) return;
            frmClientes formulario = new frmClientes();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmClientes.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void ClientesToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            if (this.FormularioAbierto("frmClientes")) return;
            frmClientes formulario = new frmClientes();
            formulario.clsView.login = this.clsLogin;            
            formulario.clsView.strProcesosId = tsmClientes.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void CajasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("tsmCajas")) return;
            frmCajas formulario = new frmCajas();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmCajas.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void FormaDePagosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmFormasPago formulario = new frmFormasPago();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmFormaPago.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void OcupacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmOcupaciones")) return;
            frmOcupaciones formulario = new frmOcupaciones();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmOcupaciones.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void PagosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmFormasPago formulario = new frmFormasPago();
            formulario.clsView.login = this.clsLogin;
            //formulario.clsView.strProcesosId = .Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void TipoDeMovimientosDeCajaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTipoMovimientosCaja formulario = new frmTipoMovimientosCaja();
            formulario.clsView.login = this.clsLogin;
            //formulario.clsView.strProcesosId = tsmTipode.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void TipoDeIdentificacionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTiposIdentificacion formulario = new frmTiposIdentificacion();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmTipoDeIdentificacion.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void TipoDeCobrosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTiposCobros formulario = new frmTiposCobros();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmTiposCobros.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void RecibosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*frmBoletasEmpeno formulario = new frmBoletasEmpeno();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmRepRecibos.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;*/
        }

        private void BoletasDeEmpeñoToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (this.FormularioAbierto("frmBoletasEmpeno")) return;

            frmBoletasEmpeno formulario = new frmBoletasEmpeno();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = this.regresarProcesoId("tsmBoletasDeEmpeno");
            formulario.Show();          
           formulario.MdiParent = this;
            
            
            
        }

        private void RecibosDeLiquidaciónYRefrendosToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (this.FormularioAbierto("frmRecibosLiquidacionRegrendos")) return;
            frmRecibosLiquidacionRegrendos formulario = new frmRecibosLiquidacionRegrendos();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmRecibosLiqRefrendos.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void MovimientosDeCajaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmMovimientosCaja")) return;
            frmMovimientosCaja formulario = new frmMovimientosCaja();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmMovimientosCaja.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void AdjudicaciónDeBoletasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmAdjudicaciónboletas")) return;
            frmAdjudicaciónboletas formulario = new frmAdjudicaciónboletas();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmAjudicacionBoletas.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void LiquidaciónInternaDeBoletasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmLiquidacionInternaBoletas")) return;
            frmLiquidacionInternaBoletas formulario = new frmLiquidacionInternaBoletas();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmLiqInternaBoletas.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void ReportesContablesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmReporteContables formulario = new frmReporteContables();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void RelaciónDeCortesDeCajaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRelacionCortesCaja formulario = new frmRelacionCortesCaja();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void RelaciónDeMovimientosDeCajaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRelacionMovimientosCaja formulario = new frmRelacionMovimientosCaja();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void RelaciónDeRecibosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRelacionRecibos formulario = new frmRelacionRecibos();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void RelaciónDePrendasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRelacionPrendas formulario = new frmRelacionPrendas();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void RelaciónDeBoletasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRelacionBoletas formulario = new frmRelacionBoletas();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void TipoDeAsentamientosToolStripMenuItem_Click(object sender, EventArgs e)
        {            
            frmAsentamientosTipos formulario = new frmAsentamientosTipos();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void TipoDeSubToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmTiposSubPrendas")) return;
            frmTiposSubPrendas formulario = new frmTiposSubPrendas();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmTipoDeSub.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void AsentamientosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmAsentamientos")) return;
            frmAsentamientos formulario = new frmAsentamientos();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmAsentamientos.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void TipoDeIdentificacionToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmTiposIdentificacion")) return;
            frmTiposIdentificacion formulario = new frmTiposIdentificacion();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmTipoDeIdentificacion.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void TsmFormaPago_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmFormasPago")) return;
            frmFormasPago formulario = new frmFormasPago();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmFormaPago.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void TsmTiposAsentamientos_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmAsentamientosTipos")) return;
            frmAsentamientosTipos formulario = new frmAsentamientosTipos();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmTiposAsentamientos.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void TsmCajas_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmCajas")) return;
            frmCajas formulario = new frmCajas();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmCajas.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

      /*  private void TsmTiposMovimientosCaja_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmTipoMovimientosCaja")) return;
            frmTipoMovimientosCaja formulario = new frmTipoMovimientosCaja();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmTiposMovimientosCaja.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }
        */
        private void TsmTiposCobros_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmTiposCobros")) return;
            frmTiposCobros formulario = new frmTiposCobros();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmTiposCobros.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }


        private void TsmSucursalesCobrosTipos_Click(object sender, EventArgs e)
        {
            /*if (this.FormularioAbierto("frmSucursalesCobrosTipos")) return;
            frmSucursalesCobrosTipos formulario = new frmSucursalesCobrosTipos();
            formulario.clsView.login = this.clsLogin;
            //formulario.clsView.strProcesosId = tsmSuc.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;*/
        }

        private void TsmGramajes_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmGramajes")) return;
            frmGramajes formulario = new frmGramajes();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmGramajes.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }


        private void CATALOGOSToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void ParámetrosDelSistemaToolStripMenuItem_Click(object sender, EventArgs e)
        {

            /*if (this.FormularioAbierto("frmTiposCobros")) return;
            frmTiposCobros formulario = new frmTiposCobros();
            formulario.Show();
            formulario.MdiParent = this;*/
        }

        private void procesosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            
            if (this.FormularioAbierto("frmProcesos")) return;
            frmProcesos formulario = new frmProcesos();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmProcesos.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        /// <summary>
        /// Valida que no se abra el mismo formulario más de una vez.
        /// </summary>
        /// <param name="strFormulario">Nombre del formulario que se quiere abrir.</param>
        /// <returns>Regresa false si el formulario no está abierto; si ya está abierto regresa true.</returns>
        private bool FormularioAbierto(string strFormulario)
        {
            // Se recorre la lista de formularios abiertos dentro de la aplicación.
            foreach (Form frmFormulario in Application.OpenForms)
            {
                // Se pregunta a cada formulario abierto si se llama (propiedad Name) como el que se quiere abrir.
                if (frmFormulario.Name == strFormulario)
                {
                    // Se manda el foco al formulario abierto.
                    frmFormulario.Activate();
                    // Se centra el formulario con respecto al MdiParent.
                    //frmFormulario.Location = new System.Drawing.Point(((frmFormulario.Parent.Width - frmFormulario.Width) / 2), ((frmFormulario.Parent.Height - frmFormulario.Height) / 2));

                    return true;
                }
            }

            return false;
        }

        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmLogin")) return;
            frmLogin formulario = new frmLogin();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmUsuarios")) return;
            frmUsuarios formulario = new frmUsuarios();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmUsuarios.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void registroDeHuellaDigitalToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void tsmFolios_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmFolios")) return;
            frmFolios formulario = new frmFolios();
            formulario.clsView.login = this.clsLogin;            
            formulario.clsView.strProcesosId = tsmFolios.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        public string regresarProcesoId(string nombreMenu) 
        {


            string strProcesosId = "";
            foreach (DataRow row in this.clsLogin.objProcesos.Tables[0].Rows)
            {
                if (row["nombre_menu"].ToString() == nombreMenu)
                {
                    strProcesosId = row["proceso_id"].ToString();
                    break;
                }

            }

            return strProcesosId;
        }

        private void tsmCajasSel_Click(object sender, EventArgs e)
        {


            frmCajasUsuarios frm = new frmCajasUsuarios();
            frm.login = this.clsLogin;
            frm.ShowDialog();

            if (frm.bolCierra)
            {
                


                this.clsLogin.caja.intId = int.Parse(frm.rowsIndex.Cells[0].Value.ToString());
                this.clsLogin.caja.strDescripcion = frm.rowsIndex.Cells[1].Value.ToString();
                this.clsLogin.caja.strCodigo = frm.rowsIndex.Cells[2].Value.ToString();


                if (this.bolAcessoCaja)
                {
                    tsmBoletasDeEmpeno.Visible = true;
                    tsmRecibosLiqRefrendos.Visible = true;
                    tsmMovimientosCaja.Visible = true;
                }
            }
            else
            {
                tsmBoletasDeEmpeno.Visible = false;
                tsmRecibosLiqRefrendos.Visible = false;
                tsmMovimientosCaja.Visible = false;
            }

        }


        private void mostraCajas() 
        {

            frmCajaUsuario frmCajaUsuario = new frmCajaUsuario();
            frmCajaUsuario.login = this.clsLogin;
            frmCajaUsuario.ShowDialog();

            this.clsLogin.intSucursalId = frmCajaUsuario.intSucursalId;
            this.clsLogin.intEmpresaId = frmCajaUsuario.intEmpresaId;
            this.clsLogin.strSucursal = frmCajaUsuario.strSucursal;
            this.clsLogin.strEmpresa = frmCajaUsuario.strEmpresa;
            this.clsLogin.strPermisos = frmCajaUsuario.strPermisos;
            this.clsLogin.objProcesos = frmCajaUsuario.objProcesos;
            this.clsLogin.objSubProcesos = frmCajaUsuario.objSubProcesos;
            this.clsLogin.objCajas = frmCajaUsuario.objCajas;


            if (frmCajaUsuario.intCajaId > 0 && frmCajaUsuario.bolCierra)
            {
                

                this.clsLogin.caja.intId = frmCajaUsuario.intCajaId;
                this.clsLogin.caja.strDescripcion = frmCajaUsuario.strDescripcionCaja;
                this.clsLogin.caja.strCodigo = frmCajaUsuario.strCodigoCaja;

                /*this.clsLogin.caja.intId = int.Parse(frmCajaUsuario.rowsIndex.Cells[0].Value.ToString());
                this.clsLogin.caja.strDescripcion = frmCajaUsuario.rowsIndex.Cells[1].Value.ToString();
                this.clsLogin.caja.strCodigo = frmCajaUsuario.rowsIndex.Cells[2].Value.ToString();*/

             
                /*if (this.bolAcessoCaja) 
                {
                    
                    tsmBoletasDeEmpeno.Visible = true;
                    tsmRecibosLiqRefrendos.Visible = true;
                }*/

            }
            else 
            {
                tsmBoletasDeEmpeno.Visible = false;
                tsmRecibosLiqRefrendos.Visible = false;
                tsmMovimientosCaja.Visible = false;
            }
            
        }

        private void cierrarSessionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void folioProcesoToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (this.FormularioAbierto("tsmFoliosProceso")) return;
            frmFoliosProcesos formulario = new frmFoliosProcesos();
            formulario.clsView.login = this.clsLogin;            
            formulario.clsView.strProcesosId = tsmFoliosProceso.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void cierreDeCajaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.FormularioAbierto("frmCorteCaja")) return;
            frmCorteCaja formulario = new frmCorteCaja();
            formulario.clsView.login = this.clsLogin;
            formulario.clsView.strProcesosId = tsmCorteCaja.Tag.ToString();
            formulario.Show();
            formulario.MdiParent = this;
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
