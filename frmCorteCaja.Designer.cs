﻿namespace EmpenosEvora
{
    partial class frmCorteCaja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tsAcciones = new System.Windows.Forms.ToolStrip();
            this.tsbNuevo = new System.Windows.Forms.ToolStripButton();
            this.tsbGuardar = new System.Windows.Forms.ToolStripButton();
            this.tsbEliminar = new System.Windows.Forms.ToolStripButton();
            this.tsbBuscar = new System.Windows.Forms.ToolStripButton();
            this.btnUndo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrimeroPag = new System.Windows.Forms.ToolStripButton();
            this.btnAtras = new System.Windows.Forms.ToolStripButton();
            this.btnAdelante = new System.Windows.Forms.ToolStripButton();
            this.btnUlitmoPag = new System.Windows.Forms.ToolStripButton();
            this.tsbImprimir = new System.Windows.Forms.ToolStripButton();
            this.btnCierra = new System.Windows.Forms.ToolStripButton();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.lblFechaCorte = new System.Windows.Forms.Label();
            this.lblFechaUltimoCorte = new System.Windows.Forms.Label();
            this.gbCorte = new System.Windows.Forms.GroupBox();
            this.lblCorteCaja = new System.Windows.Forms.Label();
            this.txtTotalIngresos = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtEntradasCaja = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTotalLiqBoletas = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtBonificacion = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtManejo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAlmacenaje = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtInteres = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCapital = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTotalEngresos = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtSalidasCaja = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtBoletasEmpenadas = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtEgresos = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtIngresos = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtSaldoInicial = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSaldoFinal = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.cmbTipo = new System.Windows.Forms.ComboBox();
            this.gbCantidadBilletes = new System.Windows.Forms.GroupBox();
            this.txtDiferencia = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtTotalImporteFisico = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.txtMonedas5c = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtMonedas10c = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.txtMonedas20c = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtMonedas50c = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtMonedas1 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtMonedas2 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtMonedas5 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtMonedas10 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtBilletes20 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtBilletes50 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtBilletes100 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtBilletes200 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtBilletes500 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtBilletes1000 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.txtUltimoCorte = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.btnFooterUlitmoPag = new System.Windows.Forms.Button();
            this.btnFooterAdelantePag = new System.Windows.Forms.Button();
            this.btnFooterAtrasPag = new System.Windows.Forms.Button();
            this.btnFooterPrimeroPag = new System.Windows.Forms.Button();
            this.btnFooterCierra = new System.Windows.Forms.Button();
            this.btnFooterGuardar = new System.Windows.Forms.Button();
            this.btnBusqueda = new System.Windows.Forms.Button();
            this.lblEstatus = new System.Windows.Forms.Label();
            this.lblDescEstatus = new System.Windows.Forms.Label();
            this.tsAcciones.SuspendLayout();
            this.gbCorte.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gbCantidadBilletes.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsAcciones
            // 
            this.tsAcciones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbNuevo,
            this.tsbGuardar,
            this.tsbEliminar,
            this.tsbBuscar,
            this.btnUndo,
            this.toolStripSeparator2,
            this.btnPrimeroPag,
            this.btnAtras,
            this.btnAdelante,
            this.btnUlitmoPag,
            this.tsbImprimir,
            this.btnCierra});
            this.tsAcciones.Location = new System.Drawing.Point(0, 0);
            this.tsAcciones.Name = "tsAcciones";
            this.tsAcciones.Size = new System.Drawing.Size(983, 25);
            this.tsAcciones.TabIndex = 583;
            this.tsAcciones.Text = "toolStrip1";
            // 
            // tsbNuevo
            // 
            this.tsbNuevo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbNuevo.Image = global::EmpenosEvora.Properties.Resources.AddFile_16x;
            this.tsbNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbNuevo.Name = "tsbNuevo";
            this.tsbNuevo.Size = new System.Drawing.Size(23, 22);
            this.tsbNuevo.Text = " ";
            this.tsbNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical90;
            this.tsbNuevo.Click += new System.EventHandler(this.tsbNuevo_Click);
            // 
            // tsbGuardar
            // 
            this.tsbGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbGuardar.Image = global::EmpenosEvora.Properties.Resources.Save_16x;
            this.tsbGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGuardar.Name = "tsbGuardar";
            this.tsbGuardar.Size = new System.Drawing.Size(23, 22);
            this.tsbGuardar.Tag = "";
            this.tsbGuardar.Text = "Guardar";
            // 
            // tsbEliminar
            // 
            this.tsbEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbEliminar.Image = global::EmpenosEvora.Properties.Resources.Cancel_16x;
            this.tsbEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEliminar.Name = "tsbEliminar";
            this.tsbEliminar.Size = new System.Drawing.Size(23, 22);
            this.tsbEliminar.Text = "Desactivar";
            this.tsbEliminar.ToolTipText = "Desactivar";
            // 
            // tsbBuscar
            // 
            this.tsbBuscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBuscar.Image = global::EmpenosEvora.Properties.Resources.VBSearch_16x;
            this.tsbBuscar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBuscar.Name = "tsbBuscar";
            this.tsbBuscar.Size = new System.Drawing.Size(23, 22);
            this.tsbBuscar.Text = "Buscar";
            // 
            // btnUndo
            // 
            this.btnUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUndo.Image = global::EmpenosEvora.Properties.Resources.Undo_16x;
            this.btnUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(23, 22);
            this.btnUndo.Text = "Deshacer";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrimeroPag
            // 
            this.btnPrimeroPag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrimeroPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeftGroup_16xSM;
            this.btnPrimeroPag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrimeroPag.Name = "btnPrimeroPag";
            this.btnPrimeroPag.Size = new System.Drawing.Size(23, 22);
            this.btnPrimeroPag.Text = "Primero";
            this.btnPrimeroPag.Click += new System.EventHandler(this.btnPrimeroPag_Click);
            // 
            // btnAtras
            // 
            this.btnAtras.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAtras.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeft_16x;
            this.btnAtras.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(23, 22);
            this.btnAtras.Text = "Atras";
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // btnAdelante
            // 
            this.btnAdelante.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdelante.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronRight_16x;
            this.btnAdelante.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdelante.Name = "btnAdelante";
            this.btnAdelante.Size = new System.Drawing.Size(23, 22);
            this.btnAdelante.Text = "Siguente";
            this.btnAdelante.Click += new System.EventHandler(this.btnAdelante_Click);
            // 
            // btnUlitmoPag
            // 
            this.btnUlitmoPag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUlitmoPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronRightGroup_16x;
            this.btnUlitmoPag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUlitmoPag.Name = "btnUlitmoPag";
            this.btnUlitmoPag.Size = new System.Drawing.Size(23, 22);
            this.btnUlitmoPag.Text = "Ultimo";
            this.btnUlitmoPag.Click += new System.EventHandler(this.btnUlitmoPag_Click);
            // 
            // tsbImprimir
            // 
            this.tsbImprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbImprimir.Image = global::EmpenosEvora.Properties.Resources.Print_16x;
            this.tsbImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbImprimir.Name = "tsbImprimir";
            this.tsbImprimir.Size = new System.Drawing.Size(23, 22);
            this.tsbImprimir.Text = "Imprimir";
            this.tsbImprimir.ToolTipText = "Imprimir";
            // 
            // btnCierra
            // 
            this.btnCierra.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnCierra.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCierra.Name = "btnCierra";
            this.btnCierra.Size = new System.Drawing.Size(23, 22);
            this.btnCierra.Text = "Salir";
            this.btnCierra.Click += new System.EventHandler(this.btnCierra_Click);
            // 
            // dtpFecha
            // 
            this.dtpFecha.Cursor = System.Windows.Forms.Cursors.Default;
            this.dtpFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecha.Location = new System.Drawing.Point(336, 64);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(98, 20);
            this.dtpFecha.TabIndex = 584;
            // 
            // lblFechaCorte
            // 
            this.lblFechaCorte.AutoSize = true;
            this.lblFechaCorte.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblFechaCorte.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaCorte.Location = new System.Drawing.Point(240, 64);
            this.lblFechaCorte.Name = "lblFechaCorte";
            this.lblFechaCorte.Size = new System.Drawing.Size(81, 13);
            this.lblFechaCorte.TabIndex = 585;
            this.lblFechaCorte.Text = "Fecha del corte";
            // 
            // lblFechaUltimoCorte
            // 
            this.lblFechaUltimoCorte.AutoSize = true;
            this.lblFechaUltimoCorte.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblFechaUltimoCorte.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaUltimoCorte.Location = new System.Drawing.Point(8, 64);
            this.lblFechaUltimoCorte.Name = "lblFechaUltimoCorte";
            this.lblFechaUltimoCorte.Size = new System.Drawing.Size(108, 13);
            this.lblFechaUltimoCorte.TabIndex = 587;
            this.lblFechaUltimoCorte.Text = "Último corte realizado";
            // 
            // gbCorte
            // 
            this.gbCorte.Controls.Add(this.lblCorteCaja);
            this.gbCorte.Controls.Add(this.txtTotalIngresos);
            this.gbCorte.Controls.Add(this.label12);
            this.gbCorte.Controls.Add(this.label11);
            this.gbCorte.Controls.Add(this.txtEntradasCaja);
            this.gbCorte.Controls.Add(this.label10);
            this.gbCorte.Controls.Add(this.label9);
            this.gbCorte.Controls.Add(this.txtTotalLiqBoletas);
            this.gbCorte.Controls.Add(this.label8);
            this.gbCorte.Controls.Add(this.txtBonificacion);
            this.gbCorte.Controls.Add(this.label7);
            this.gbCorte.Controls.Add(this.txtManejo);
            this.gbCorte.Controls.Add(this.label6);
            this.gbCorte.Controls.Add(this.txtAlmacenaje);
            this.gbCorte.Controls.Add(this.label5);
            this.gbCorte.Controls.Add(this.txtInteres);
            this.gbCorte.Controls.Add(this.label3);
            this.gbCorte.Controls.Add(this.label2);
            this.gbCorte.Controls.Add(this.label1);
            this.gbCorte.Controls.Add(this.txtCapital);
            this.gbCorte.Controls.Add(this.label4);
            this.gbCorte.Cursor = System.Windows.Forms.Cursors.Default;
            this.gbCorte.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbCorte.Location = new System.Drawing.Point(8, 96);
            this.gbCorte.Name = "gbCorte";
            this.gbCorte.Size = new System.Drawing.Size(288, 304);
            this.gbCorte.TabIndex = 590;
            this.gbCorte.TabStop = false;
            // 
            // lblCorteCaja
            // 
            this.lblCorteCaja.AutoSize = true;
            this.lblCorteCaja.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCorteCaja.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCorteCaja.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblCorteCaja.Location = new System.Drawing.Point(8, 0);
            this.lblCorteCaja.Name = "lblCorteCaja";
            this.lblCorteCaja.Size = new System.Drawing.Size(118, 16);
            this.lblCorteCaja.TabIndex = 598;
            this.lblCorteCaja.Text = "Corte de la caja";
            // 
            // txtTotalIngresos
            // 
            this.txtTotalIngresos.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTotalIngresos.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtTotalIngresos.Enabled = false;
            this.txtTotalIngresos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalIngresos.Location = new System.Drawing.Point(160, 256);
            this.txtTotalIngresos.MaxLength = 100;
            this.txtTotalIngresos.Name = "txtTotalIngresos";
            this.txtTotalIngresos.ReadOnly = true;
            this.txtTotalIngresos.Size = new System.Drawing.Size(100, 20);
            this.txtTotalIngresos.TabIndex = 437;
            this.txtTotalIngresos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(32, 256);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(105, 13);
            this.label12.TabIndex = 438;
            this.label12.Text = "Total de ingresos";
            // 
            // label11
            // 
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label11.Location = new System.Drawing.Point(160, 248);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 2);
            this.label11.TabIndex = 436;
            // 
            // txtEntradasCaja
            // 
            this.txtEntradasCaja.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEntradasCaja.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtEntradasCaja.Enabled = false;
            this.txtEntradasCaja.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEntradasCaja.Location = new System.Drawing.Point(160, 224);
            this.txtEntradasCaja.MaxLength = 100;
            this.txtEntradasCaja.Name = "txtEntradasCaja";
            this.txtEntradasCaja.ReadOnly = true;
            this.txtEntradasCaja.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtEntradasCaja.Size = new System.Drawing.Size(100, 20);
            this.txtEntradasCaja.TabIndex = 434;
            this.txtEntradasCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Cursor = System.Windows.Forms.Cursors.Default;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(32, 224);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 435;
            this.label10.Text = "Entradas de caja";
            // 
            // label9
            // 
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label9.Location = new System.Drawing.Point(160, 192);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 2);
            this.label9.TabIndex = 433;
            // 
            // txtTotalLiqBoletas
            // 
            this.txtTotalLiqBoletas.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTotalLiqBoletas.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtTotalLiqBoletas.Enabled = false;
            this.txtTotalLiqBoletas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalLiqBoletas.Location = new System.Drawing.Point(160, 200);
            this.txtTotalLiqBoletas.MaxLength = 100;
            this.txtTotalLiqBoletas.Name = "txtTotalLiqBoletas";
            this.txtTotalLiqBoletas.ReadOnly = true;
            this.txtTotalLiqBoletas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTotalLiqBoletas.Size = new System.Drawing.Size(100, 20);
            this.txtTotalLiqBoletas.TabIndex = 342;
            this.txtTotalLiqBoletas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Cursor = System.Windows.Forms.Cursors.Default;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(32, 200);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 13);
            this.label8.TabIndex = 343;
            this.label8.Text = "Total liq. de boletas";
            // 
            // txtBonificacion
            // 
            this.txtBonificacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBonificacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtBonificacion.Enabled = false;
            this.txtBonificacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBonificacion.Location = new System.Drawing.Point(160, 168);
            this.txtBonificacion.MaxLength = 100;
            this.txtBonificacion.Name = "txtBonificacion";
            this.txtBonificacion.ReadOnly = true;
            this.txtBonificacion.Size = new System.Drawing.Size(100, 20);
            this.txtBonificacion.TabIndex = 340;
            this.txtBonificacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Cursor = System.Windows.Forms.Cursors.Default;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(56, 168);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 13);
            this.label7.TabIndex = 341;
            this.label7.Text = "-Bonificaciones";
            // 
            // txtManejo
            // 
            this.txtManejo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtManejo.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtManejo.Enabled = false;
            this.txtManejo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtManejo.Location = new System.Drawing.Point(160, 144);
            this.txtManejo.MaxLength = 100;
            this.txtManejo.Name = "txtManejo";
            this.txtManejo.ReadOnly = true;
            this.txtManejo.Size = new System.Drawing.Size(100, 20);
            this.txtManejo.TabIndex = 338;
            this.txtManejo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(56, 144);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 339;
            this.label6.Text = "Manejo";
            // 
            // txtAlmacenaje
            // 
            this.txtAlmacenaje.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAlmacenaje.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAlmacenaje.Enabled = false;
            this.txtAlmacenaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAlmacenaje.Location = new System.Drawing.Point(160, 120);
            this.txtAlmacenaje.MaxLength = 100;
            this.txtAlmacenaje.Name = "txtAlmacenaje";
            this.txtAlmacenaje.ReadOnly = true;
            this.txtAlmacenaje.Size = new System.Drawing.Size(100, 20);
            this.txtAlmacenaje.TabIndex = 336;
            this.txtAlmacenaje.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(56, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 337;
            this.label5.Text = "Almacenaje";
            // 
            // txtInteres
            // 
            this.txtInteres.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtInteres.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtInteres.Enabled = false;
            this.txtInteres.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInteres.Location = new System.Drawing.Point(160, 96);
            this.txtInteres.MaxLength = 100;
            this.txtInteres.Name = "txtInteres";
            this.txtInteres.ReadOnly = true;
            this.txtInteres.Size = new System.Drawing.Size(100, 20);
            this.txtInteres.TabIndex = 334;
            this.txtInteres.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(56, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 335;
            this.label3.Text = "Interes";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(32, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 13);
            this.label2.TabIndex = 333;
            this.label2.Text = "Liquidación de boletas:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 15);
            this.label1.TabIndex = 332;
            this.label1.Text = "INGRESOS";
            // 
            // txtCapital
            // 
            this.txtCapital.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCapital.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCapital.Enabled = false;
            this.txtCapital.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCapital.Location = new System.Drawing.Point(160, 72);
            this.txtCapital.MaxLength = 100;
            this.txtCapital.Name = "txtCapital";
            this.txtCapital.ReadOnly = true;
            this.txtCapital.Size = new System.Drawing.Size(100, 20);
            this.txtCapital.TabIndex = 31;
            this.txtCapital.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(56, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 331;
            this.label4.Text = "Capital";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.txtTotalEngresos);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.txtSalidasCaja);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.txtBoletasEmpenadas);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(296, 96);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(256, 160);
            this.groupBox1.TabIndex = 591;
            this.groupBox1.TabStop = false;
            // 
            // label16
            // 
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label16.Location = new System.Drawing.Point(128, 120);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(100, 2);
            this.label16.TabIndex = 433;
            // 
            // txtTotalEngresos
            // 
            this.txtTotalEngresos.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTotalEngresos.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtTotalEngresos.Enabled = false;
            this.txtTotalEngresos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalEngresos.Location = new System.Drawing.Point(128, 128);
            this.txtTotalEngresos.MaxLength = 100;
            this.txtTotalEngresos.Name = "txtTotalEngresos";
            this.txtTotalEngresos.ReadOnly = true;
            this.txtTotalEngresos.Size = new System.Drawing.Size(100, 20);
            this.txtTotalEngresos.TabIndex = 342;
            this.txtTotalEngresos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Cursor = System.Windows.Forms.Cursors.Default;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(16, 128);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(110, 13);
            this.label17.TabIndex = 343;
            this.label17.Text = "Total de egresos  ";
            // 
            // txtSalidasCaja
            // 
            this.txtSalidasCaja.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSalidasCaja.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtSalidasCaja.Enabled = false;
            this.txtSalidasCaja.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalidasCaja.Location = new System.Drawing.Point(128, 96);
            this.txtSalidasCaja.MaxLength = 100;
            this.txtSalidasCaja.Name = "txtSalidasCaja";
            this.txtSalidasCaja.ReadOnly = true;
            this.txtSalidasCaja.Size = new System.Drawing.Size(100, 20);
            this.txtSalidasCaja.TabIndex = 334;
            this.txtSalidasCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Cursor = System.Windows.Forms.Cursors.Default;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(24, 96);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(79, 13);
            this.label21.TabIndex = 335;
            this.label21.Text = "Salidas de caja";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Cursor = System.Windows.Forms.Cursors.Default;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(16, 24);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(73, 15);
            this.label23.TabIndex = 332;
            this.label23.Text = "EGRESOS";
            // 
            // txtBoletasEmpenadas
            // 
            this.txtBoletasEmpenadas.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBoletasEmpenadas.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtBoletasEmpenadas.Enabled = false;
            this.txtBoletasEmpenadas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoletasEmpenadas.Location = new System.Drawing.Point(128, 72);
            this.txtBoletasEmpenadas.MaxLength = 100;
            this.txtBoletasEmpenadas.Name = "txtBoletasEmpenadas";
            this.txtBoletasEmpenadas.ReadOnly = true;
            this.txtBoletasEmpenadas.Size = new System.Drawing.Size(100, 20);
            this.txtBoletasEmpenadas.TabIndex = 31;
            this.txtBoletasEmpenadas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Cursor = System.Windows.Forms.Cursors.Default;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(24, 72);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(100, 13);
            this.label24.TabIndex = 331;
            this.label24.Text = "Boletas empeñadas";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtEgresos);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.txtIngresos);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.txtSaldoInicial);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtSaldoFinal);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Cursor = System.Windows.Forms.Cursors.Default;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(296, 248);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(256, 152);
            this.groupBox2.TabIndex = 592;
            this.groupBox2.TabStop = false;
            // 
            // txtEgresos
            // 
            this.txtEgresos.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEgresos.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtEgresos.Enabled = false;
            this.txtEgresos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEgresos.Location = new System.Drawing.Point(128, 72);
            this.txtEgresos.MaxLength = 100;
            this.txtEgresos.Name = "txtEgresos";
            this.txtEgresos.ReadOnly = true;
            this.txtEgresos.Size = new System.Drawing.Size(100, 20);
            this.txtEgresos.TabIndex = 438;
            this.txtEgresos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Cursor = System.Windows.Forms.Cursors.Default;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(24, 72);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(82, 15);
            this.label19.TabIndex = 437;
            this.label19.Text = "- EGRESOS";
            // 
            // txtIngresos
            // 
            this.txtIngresos.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIngresos.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtIngresos.Enabled = false;
            this.txtIngresos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIngresos.Location = new System.Drawing.Point(128, 48);
            this.txtIngresos.MaxLength = 100;
            this.txtIngresos.Name = "txtIngresos";
            this.txtIngresos.ReadOnly = true;
            this.txtIngresos.Size = new System.Drawing.Size(100, 20);
            this.txtIngresos.TabIndex = 436;
            this.txtIngresos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Cursor = System.Windows.Forms.Cursors.Default;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(24, 48);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(90, 15);
            this.label15.TabIndex = 435;
            this.label15.Text = "+ INGRESOS";
            // 
            // txtSaldoInicial
            // 
            this.txtSaldoInicial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSaldoInicial.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtSaldoInicial.Enabled = false;
            this.txtSaldoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSaldoInicial.Location = new System.Drawing.Point(128, 24);
            this.txtSaldoInicial.MaxLength = 100;
            this.txtSaldoInicial.Name = "txtSaldoInicial";
            this.txtSaldoInicial.ReadOnly = true;
            this.txtSaldoInicial.Size = new System.Drawing.Size(100, 20);
            this.txtSaldoInicial.TabIndex = 434;
            this.txtSaldoInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label13.Location = new System.Drawing.Point(128, 96);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 2);
            this.label13.TabIndex = 433;
            // 
            // txtSaldoFinal
            // 
            this.txtSaldoFinal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSaldoFinal.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtSaldoFinal.Enabled = false;
            this.txtSaldoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSaldoFinal.Location = new System.Drawing.Point(112, 104);
            this.txtSaldoFinal.MaxLength = 100;
            this.txtSaldoFinal.Name = "txtSaldoFinal";
            this.txtSaldoFinal.ReadOnly = true;
            this.txtSaldoFinal.Size = new System.Drawing.Size(116, 24);
            this.txtSaldoFinal.TabIndex = 342;
            this.txtSaldoFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Cursor = System.Windows.Forms.Cursors.Default;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(16, 104);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 13);
            this.label14.TabIndex = 343;
            this.label14.Text = "SALDO FINAL";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Cursor = System.Windows.Forms.Cursors.Default;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(16, 24);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(103, 15);
            this.label18.TabIndex = 332;
            this.label18.Text = "SALDO INICIAL";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Cursor = System.Windows.Forms.Cursors.Default;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(496, 64);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(28, 13);
            this.label20.TabIndex = 593;
            this.label20.Text = "Tipo";
            // 
            // cmbTipo
            // 
            this.cmbTipo.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmbTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTipo.FormattingEnabled = true;
            this.cmbTipo.Items.AddRange(new object[] {
            "Selecione una opción",
            "ARQUEO",
            "CIERRE"});
            this.cmbTipo.Location = new System.Drawing.Point(536, 64);
            this.cmbTipo.Name = "cmbTipo";
            this.cmbTipo.Size = new System.Drawing.Size(128, 21);
            this.cmbTipo.TabIndex = 594;
            // 
            // gbCantidadBilletes
            // 
            this.gbCantidadBilletes.Controls.Add(this.txtDiferencia);
            this.gbCantidadBilletes.Controls.Add(this.label41);
            this.gbCantidadBilletes.Controls.Add(this.txtTotalImporteFisico);
            this.gbCantidadBilletes.Controls.Add(this.label40);
            this.gbCantidadBilletes.Controls.Add(this.label39);
            this.gbCantidadBilletes.Controls.Add(this.txtMonedas5c);
            this.gbCantidadBilletes.Controls.Add(this.label37);
            this.gbCantidadBilletes.Controls.Add(this.txtMonedas10c);
            this.gbCantidadBilletes.Controls.Add(this.label38);
            this.gbCantidadBilletes.Controls.Add(this.txtMonedas20c);
            this.gbCantidadBilletes.Controls.Add(this.label35);
            this.gbCantidadBilletes.Controls.Add(this.txtMonedas50c);
            this.gbCantidadBilletes.Controls.Add(this.label36);
            this.gbCantidadBilletes.Controls.Add(this.txtMonedas1);
            this.gbCantidadBilletes.Controls.Add(this.label33);
            this.gbCantidadBilletes.Controls.Add(this.txtMonedas2);
            this.gbCantidadBilletes.Controls.Add(this.label34);
            this.gbCantidadBilletes.Controls.Add(this.txtMonedas5);
            this.gbCantidadBilletes.Controls.Add(this.label31);
            this.gbCantidadBilletes.Controls.Add(this.txtMonedas10);
            this.gbCantidadBilletes.Controls.Add(this.label32);
            this.gbCantidadBilletes.Controls.Add(this.txtBilletes20);
            this.gbCantidadBilletes.Controls.Add(this.label30);
            this.gbCantidadBilletes.Controls.Add(this.txtBilletes50);
            this.gbCantidadBilletes.Controls.Add(this.label29);
            this.gbCantidadBilletes.Controls.Add(this.txtBilletes100);
            this.gbCantidadBilletes.Controls.Add(this.label25);
            this.gbCantidadBilletes.Controls.Add(this.txtBilletes200);
            this.gbCantidadBilletes.Controls.Add(this.label22);
            this.gbCantidadBilletes.Controls.Add(this.txtBilletes500);
            this.gbCantidadBilletes.Controls.Add(this.label26);
            this.gbCantidadBilletes.Controls.Add(this.label27);
            this.gbCantidadBilletes.Controls.Add(this.txtBilletes1000);
            this.gbCantidadBilletes.Controls.Add(this.label28);
            this.gbCantidadBilletes.Cursor = System.Windows.Forms.Cursors.Default;
            this.gbCantidadBilletes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbCantidadBilletes.Location = new System.Drawing.Point(552, 96);
            this.gbCantidadBilletes.Name = "gbCantidadBilletes";
            this.gbCantidadBilletes.Size = new System.Drawing.Size(416, 304);
            this.gbCantidadBilletes.TabIndex = 592;
            this.gbCantidadBilletes.TabStop = false;
            // 
            // txtDiferencia
            // 
            this.txtDiferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDiferencia.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDiferencia.Enabled = false;
            this.txtDiferencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiferencia.Location = new System.Drawing.Point(304, 272);
            this.txtDiferencia.MaxLength = 100;
            this.txtDiferencia.Name = "txtDiferencia";
            this.txtDiferencia.Size = new System.Drawing.Size(100, 20);
            this.txtDiferencia.TabIndex = 441;
            this.txtDiferencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Cursor = System.Windows.Forms.Cursors.Default;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(120, 272);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(81, 13);
            this.label41.TabIndex = 442;
            this.label41.Text = "DIFERENCIA";
            // 
            // txtTotalImporteFisico
            // 
            this.txtTotalImporteFisico.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTotalImporteFisico.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtTotalImporteFisico.Enabled = false;
            this.txtTotalImporteFisico.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalImporteFisico.Location = new System.Drawing.Point(304, 248);
            this.txtTotalImporteFisico.MaxLength = 100;
            this.txtTotalImporteFisico.Name = "txtTotalImporteFisico";
            this.txtTotalImporteFisico.Size = new System.Drawing.Size(100, 20);
            this.txtTotalImporteFisico.TabIndex = 439;
            this.txtTotalImporteFisico.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Cursor = System.Windows.Forms.Cursors.Default;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(120, 248);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(172, 13);
            this.label40.TabIndex = 440;
            this.label40.Text = "TOTAL DE IMPORTE FÍSICO";
            // 
            // label39
            // 
            this.label39.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label39.Cursor = System.Windows.Forms.Cursors.Default;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label39.Location = new System.Drawing.Point(16, 240);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(390, 2);
            this.label39.TabIndex = 439;
            // 
            // txtMonedas5c
            // 
            this.txtMonedas5c.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMonedas5c.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtMonedas5c.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMonedas5c.Location = new System.Drawing.Point(336, 216);
            this.txtMonedas5c.MaxLength = 100;
            this.txtMonedas5c.Name = "txtMonedas5c";
            this.txtMonedas5c.Size = new System.Drawing.Size(72, 20);
            this.txtMonedas5c.TabIndex = 358;
            this.txtMonedas5c.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonedas5c.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMonedas5c_KeyPress);
            this.txtMonedas5c.Leave += new System.EventHandler(this.txtMonedas5c_Leave);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Cursor = System.Windows.Forms.Cursors.Default;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(217, 216);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(87, 13);
            this.label37.TabIndex = 359;
            this.label37.Text = "Monedas de 5 c.";
            // 
            // txtMonedas10c
            // 
            this.txtMonedas10c.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMonedas10c.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtMonedas10c.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMonedas10c.Location = new System.Drawing.Point(128, 216);
            this.txtMonedas10c.MaxLength = 100;
            this.txtMonedas10c.Name = "txtMonedas10c";
            this.txtMonedas10c.Size = new System.Drawing.Size(72, 20);
            this.txtMonedas10c.TabIndex = 356;
            this.txtMonedas10c.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonedas10c.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMonedas10c_KeyPress);
            this.txtMonedas10c.Leave += new System.EventHandler(this.txtMonedas10c_Leave);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Cursor = System.Windows.Forms.Cursors.Default;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(16, 216);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(93, 13);
            this.label38.TabIndex = 357;
            this.label38.Text = "Monedas de 10 c.";
            // 
            // txtMonedas20c
            // 
            this.txtMonedas20c.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMonedas20c.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtMonedas20c.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMonedas20c.Location = new System.Drawing.Point(336, 192);
            this.txtMonedas20c.MaxLength = 100;
            this.txtMonedas20c.Name = "txtMonedas20c";
            this.txtMonedas20c.Size = new System.Drawing.Size(72, 20);
            this.txtMonedas20c.TabIndex = 354;
            this.txtMonedas20c.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonedas20c.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMonedas20c_KeyPress);
            this.txtMonedas20c.Leave += new System.EventHandler(this.txtMonedas20c_Leave);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Cursor = System.Windows.Forms.Cursors.Default;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(216, 192);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(96, 13);
            this.label35.TabIndex = 355;
            this.label35.Text = " Monedas de 20 c.";
            // 
            // txtMonedas50c
            // 
            this.txtMonedas50c.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMonedas50c.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtMonedas50c.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMonedas50c.Location = new System.Drawing.Point(128, 192);
            this.txtMonedas50c.MaxLength = 100;
            this.txtMonedas50c.Name = "txtMonedas50c";
            this.txtMonedas50c.Size = new System.Drawing.Size(72, 20);
            this.txtMonedas50c.TabIndex = 352;
            this.txtMonedas50c.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonedas50c.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMonedas50c_KeyPress);
            this.txtMonedas50c.Leave += new System.EventHandler(this.txtMonedas50c_Leave);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Cursor = System.Windows.Forms.Cursors.Default;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(16, 192);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(93, 13);
            this.label36.TabIndex = 353;
            this.label36.Text = "Monedas de 50 c.";
            // 
            // txtMonedas1
            // 
            this.txtMonedas1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMonedas1.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtMonedas1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMonedas1.Location = new System.Drawing.Point(336, 168);
            this.txtMonedas1.MaxLength = 100;
            this.txtMonedas1.Name = "txtMonedas1";
            this.txtMonedas1.Size = new System.Drawing.Size(72, 20);
            this.txtMonedas1.TabIndex = 350;
            this.txtMonedas1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonedas1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMonedas1_KeyPress);
            this.txtMonedas1.Leave += new System.EventHandler(this.txtMonedas1_Leave);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Cursor = System.Windows.Forms.Cursors.Default;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(217, 168);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(96, 13);
            this.label33.TabIndex = 351;
            this.label33.Text = "Monedas de $1.00";
            // 
            // txtMonedas2
            // 
            this.txtMonedas2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMonedas2.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtMonedas2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMonedas2.Location = new System.Drawing.Point(128, 168);
            this.txtMonedas2.MaxLength = 100;
            this.txtMonedas2.Name = "txtMonedas2";
            this.txtMonedas2.Size = new System.Drawing.Size(72, 20);
            this.txtMonedas2.TabIndex = 348;
            this.txtMonedas2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonedas2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMonedas2_KeyPress);
            this.txtMonedas2.Leave += new System.EventHandler(this.txtMonedas2_Leave);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Cursor = System.Windows.Forms.Cursors.Default;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(16, 168);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(96, 13);
            this.label34.TabIndex = 349;
            this.label34.Text = "Monedas de $2.00";
            // 
            // txtMonedas5
            // 
            this.txtMonedas5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMonedas5.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtMonedas5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMonedas5.Location = new System.Drawing.Point(336, 144);
            this.txtMonedas5.MaxLength = 100;
            this.txtMonedas5.Name = "txtMonedas5";
            this.txtMonedas5.Size = new System.Drawing.Size(72, 20);
            this.txtMonedas5.TabIndex = 346;
            this.txtMonedas5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonedas5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMonedas5_KeyPress);
            this.txtMonedas5.Leave += new System.EventHandler(this.txtMonedas5_Leave);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Cursor = System.Windows.Forms.Cursors.Default;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(215, 144);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(102, 13);
            this.label31.TabIndex = 347;
            this.label31.Text = " Monedas de $5.00,";
            // 
            // txtMonedas10
            // 
            this.txtMonedas10.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMonedas10.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtMonedas10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMonedas10.Location = new System.Drawing.Point(128, 144);
            this.txtMonedas10.MaxLength = 100;
            this.txtMonedas10.Name = "txtMonedas10";
            this.txtMonedas10.Size = new System.Drawing.Size(72, 20);
            this.txtMonedas10.TabIndex = 344;
            this.txtMonedas10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonedas10.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMonedas10_KeyPress);
            this.txtMonedas10.Leave += new System.EventHandler(this.txtMonedas10_Leave);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Cursor = System.Windows.Forms.Cursors.Default;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(16, 144);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(102, 13);
            this.label32.TabIndex = 345;
            this.label32.Text = "Monedas de $10.00";
            // 
            // txtBilletes20
            // 
            this.txtBilletes20.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBilletes20.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtBilletes20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBilletes20.Location = new System.Drawing.Point(336, 120);
            this.txtBilletes20.MaxLength = 100;
            this.txtBilletes20.Name = "txtBilletes20";
            this.txtBilletes20.Size = new System.Drawing.Size(72, 20);
            this.txtBilletes20.TabIndex = 342;
            this.txtBilletes20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBilletes20.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBilletes20_KeyPress);
            this.txtBilletes20.Leave += new System.EventHandler(this.txtBilletes20_Leave);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Cursor = System.Windows.Forms.Cursors.Default;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(216, 120);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(91, 13);
            this.label30.TabIndex = 343;
            this.label30.Text = "Billetes de $20.00";
            // 
            // txtBilletes50
            // 
            this.txtBilletes50.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBilletes50.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtBilletes50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBilletes50.Location = new System.Drawing.Point(128, 120);
            this.txtBilletes50.MaxLength = 100;
            this.txtBilletes50.Name = "txtBilletes50";
            this.txtBilletes50.Size = new System.Drawing.Size(72, 20);
            this.txtBilletes50.TabIndex = 340;
            this.txtBilletes50.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBilletes50.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBilletes50_KeyPress);
            this.txtBilletes50.Leave += new System.EventHandler(this.txtBilletes50_Leave);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Cursor = System.Windows.Forms.Cursors.Default;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(16, 120);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(91, 13);
            this.label29.TabIndex = 341;
            this.label29.Text = "Billetes de $50.00";
            // 
            // txtBilletes100
            // 
            this.txtBilletes100.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBilletes100.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtBilletes100.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBilletes100.Location = new System.Drawing.Point(336, 96);
            this.txtBilletes100.MaxLength = 100;
            this.txtBilletes100.Name = "txtBilletes100";
            this.txtBilletes100.Size = new System.Drawing.Size(72, 20);
            this.txtBilletes100.TabIndex = 338;
            this.txtBilletes100.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBilletes100.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBilletes100_KeyPress);
            this.txtBilletes100.Leave += new System.EventHandler(this.txtBilletes100_Leave);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Cursor = System.Windows.Forms.Cursors.Default;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(217, 96);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(97, 13);
            this.label25.TabIndex = 339;
            this.label25.Text = "Billetes de $100.00";
            // 
            // txtBilletes200
            // 
            this.txtBilletes200.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBilletes200.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtBilletes200.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBilletes200.Location = new System.Drawing.Point(128, 96);
            this.txtBilletes200.MaxLength = 100;
            this.txtBilletes200.Name = "txtBilletes200";
            this.txtBilletes200.Size = new System.Drawing.Size(72, 20);
            this.txtBilletes200.TabIndex = 336;
            this.txtBilletes200.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBilletes200.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBilletes200_KeyPress);
            this.txtBilletes200.Leave += new System.EventHandler(this.txtBilletes200_Leave);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Cursor = System.Windows.Forms.Cursors.Default;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(16, 96);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(97, 13);
            this.label22.TabIndex = 337;
            this.label22.Text = "Billetes de $200.00";
            // 
            // txtBilletes500
            // 
            this.txtBilletes500.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBilletes500.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtBilletes500.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBilletes500.Location = new System.Drawing.Point(336, 72);
            this.txtBilletes500.MaxLength = 100;
            this.txtBilletes500.Name = "txtBilletes500";
            this.txtBilletes500.Size = new System.Drawing.Size(72, 20);
            this.txtBilletes500.TabIndex = 334;
            this.txtBilletes500.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBilletes500.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBilletes500_KeyPress);
            this.txtBilletes500.Leave += new System.EventHandler(this.txtBilletes500_Leave);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Cursor = System.Windows.Forms.Cursors.Default;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(216, 72);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(100, 13);
            this.label26.TabIndex = 335;
            this.label26.Text = " Billetes de $500.00";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Cursor = System.Windows.Forms.Cursors.Default;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(16, 24);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(392, 15);
            this.label27.TabIndex = 332;
            this.label27.Text = "CANTIDAD DE BILLETES Y MONEDAS POR DENOMINACIÓN";
            // 
            // txtBilletes1000
            // 
            this.txtBilletes1000.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBilletes1000.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtBilletes1000.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBilletes1000.Location = new System.Drawing.Point(128, 72);
            this.txtBilletes1000.MaxLength = 100;
            this.txtBilletes1000.Name = "txtBilletes1000";
            this.txtBilletes1000.Size = new System.Drawing.Size(72, 20);
            this.txtBilletes1000.TabIndex = 31;
            this.txtBilletes1000.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBilletes1000.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBilletes1000_KeyPress);
            this.txtBilletes1000.Leave += new System.EventHandler(this.txtBilletes1000_Leave);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Cursor = System.Windows.Forms.Cursors.Default;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(16, 72);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(106, 13);
            this.label28.TabIndex = 331;
            this.label28.Text = "Billetes de $1,000.00";
            // 
            // label42
            // 
            this.label42.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label42.Location = new System.Drawing.Point(0, 416);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(980, 2);
            this.label42.TabIndex = 595;
            // 
            // txtUltimoCorte
            // 
            this.txtUltimoCorte.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUltimoCorte.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtUltimoCorte.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUltimoCorte.Location = new System.Drawing.Point(128, 64);
            this.txtUltimoCorte.MaxLength = 5;
            this.txtUltimoCorte.Name = "txtUltimoCorte";
            this.txtUltimoCorte.ReadOnly = true;
            this.txtUltimoCorte.Size = new System.Drawing.Size(96, 20);
            this.txtUltimoCorte.TabIndex = 598;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Cursor = System.Windows.Forms.Cursors.Default;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(8, 432);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(83, 13);
            this.label43.TabIndex = 600;
            this.label43.Text = "Cambiar de caja";
            // 
            // btnFooterUlitmoPag
            // 
            this.btnFooterUlitmoPag.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterUlitmoPag.Image = global::EmpenosEvora.Properties.Resources.ExpandChevronRightGroup_16x;
            this.btnFooterUlitmoPag.Location = new System.Drawing.Point(224, 432);
            this.btnFooterUlitmoPag.Name = "btnFooterUlitmoPag";
            this.btnFooterUlitmoPag.Size = new System.Drawing.Size(40, 24);
            this.btnFooterUlitmoPag.TabIndex = 603;
            this.btnFooterUlitmoPag.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterUlitmoPag.UseVisualStyleBackColor = true;
            this.btnFooterUlitmoPag.Click += new System.EventHandler(this.btnFooterUlitmoPag_Click);
            // 
            // btnFooterAdelantePag
            // 
            this.btnFooterAdelantePag.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterAdelantePag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronRight_16x;
            this.btnFooterAdelantePag.Location = new System.Drawing.Point(184, 432);
            this.btnFooterAdelantePag.Name = "btnFooterAdelantePag";
            this.btnFooterAdelantePag.Size = new System.Drawing.Size(40, 24);
            this.btnFooterAdelantePag.TabIndex = 602;
            this.btnFooterAdelantePag.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterAdelantePag.UseVisualStyleBackColor = true;
            this.btnFooterAdelantePag.Click += new System.EventHandler(this.btnFooterAdelantePag_Click);
            // 
            // btnFooterAtrasPag
            // 
            this.btnFooterAtrasPag.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterAtrasPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeft_16x;
            this.btnFooterAtrasPag.Location = new System.Drawing.Point(144, 432);
            this.btnFooterAtrasPag.Name = "btnFooterAtrasPag";
            this.btnFooterAtrasPag.Size = new System.Drawing.Size(40, 24);
            this.btnFooterAtrasPag.TabIndex = 601;
            this.btnFooterAtrasPag.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterAtrasPag.UseVisualStyleBackColor = true;
            this.btnFooterAtrasPag.Click += new System.EventHandler(this.btnFooterAtrasPag_Click);
            // 
            // btnFooterPrimeroPag
            // 
            this.btnFooterPrimeroPag.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterPrimeroPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeftGroup_16x;
            this.btnFooterPrimeroPag.Location = new System.Drawing.Point(104, 432);
            this.btnFooterPrimeroPag.Name = "btnFooterPrimeroPag";
            this.btnFooterPrimeroPag.Size = new System.Drawing.Size(40, 24);
            this.btnFooterPrimeroPag.TabIndex = 599;
            this.btnFooterPrimeroPag.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterPrimeroPag.UseVisualStyleBackColor = true;
            this.btnFooterPrimeroPag.Click += new System.EventHandler(this.btnFooterPrimeroPag_Click);
            // 
            // btnFooterCierra
            // 
            this.btnFooterCierra.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnFooterCierra.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterCierra.Location = new System.Drawing.Point(888, 432);
            this.btnFooterCierra.Name = "btnFooterCierra";
            this.btnFooterCierra.Size = new System.Drawing.Size(75, 40);
            this.btnFooterCierra.TabIndex = 597;
            this.btnFooterCierra.Text = "Salir";
            this.btnFooterCierra.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterCierra.UseVisualStyleBackColor = true;
            this.btnFooterCierra.Click += new System.EventHandler(this.btnFooterCierra_Click);
            // 
            // btnFooterGuardar
            // 
            this.btnFooterGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterGuardar.Image = global::EmpenosEvora.Properties.Resources.Save_16x;
            this.btnFooterGuardar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterGuardar.Location = new System.Drawing.Point(800, 432);
            this.btnFooterGuardar.Name = "btnFooterGuardar";
            this.btnFooterGuardar.Size = new System.Drawing.Size(75, 40);
            this.btnFooterGuardar.TabIndex = 596;
            this.btnFooterGuardar.Text = "Guardar";
            this.btnFooterGuardar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterGuardar.UseVisualStyleBackColor = true;
            this.btnFooterGuardar.Click += new System.EventHandler(this.btnFooterGuardar_Click);
            // 
            // btnBusqueda
            // 
            this.btnBusqueda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnBusqueda.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusqueda.Location = new System.Drawing.Point(448, 64);
            this.btnBusqueda.Name = "btnBusqueda";
            this.btnBusqueda.Size = new System.Drawing.Size(32, 21);
            this.btnBusqueda.TabIndex = 604;
            this.btnBusqueda.UseVisualStyleBackColor = true;
            // 
            // lblEstatus
            // 
            this.lblEstatus.AutoSize = true;
            this.lblEstatus.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblEstatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstatus.ForeColor = System.Drawing.Color.Red;
            this.lblEstatus.Location = new System.Drawing.Point(128, 40);
            this.lblEstatus.Name = "lblEstatus";
            this.lblEstatus.Size = new System.Drawing.Size(50, 13);
            this.lblEstatus.TabIndex = 606;
            this.lblEstatus.Text = "NUEVO";
            this.lblEstatus.Visible = false;
            // 
            // lblDescEstatus
            // 
            this.lblDescEstatus.AutoSize = true;
            this.lblDescEstatus.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblDescEstatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblDescEstatus.ForeColor = System.Drawing.Color.Black;
            this.lblDescEstatus.Location = new System.Drawing.Point(8, 40);
            this.lblDescEstatus.Name = "lblDescEstatus";
            this.lblDescEstatus.Size = new System.Drawing.Size(42, 13);
            this.lblDescEstatus.TabIndex = 605;
            this.lblDescEstatus.Text = "Estatus";
            this.lblDescEstatus.Visible = false;
            // 
            // frmCorteCaja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(983, 513);
            this.Controls.Add(this.lblEstatus);
            this.Controls.Add(this.lblDescEstatus);
            this.Controls.Add(this.btnBusqueda);
            this.Controls.Add(this.btnFooterUlitmoPag);
            this.Controls.Add(this.btnFooterAdelantePag);
            this.Controls.Add(this.btnFooterAtrasPag);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.btnFooterPrimeroPag);
            this.Controls.Add(this.txtUltimoCorte);
            this.Controls.Add(this.btnFooterCierra);
            this.Controls.Add(this.btnFooterGuardar);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.gbCantidadBilletes);
            this.Controls.Add(this.cmbTipo);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbCorte);
            this.Controls.Add(this.lblFechaUltimoCorte);
            this.Controls.Add(this.dtpFecha);
            this.Controls.Add(this.lblFechaCorte);
            this.Controls.Add(this.tsAcciones);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCorteCaja";
            this.Text = "Corte de caja";
            this.Load += new System.EventHandler(this.frmCorteCaja_Load);
            this.tsAcciones.ResumeLayout(false);
            this.tsAcciones.PerformLayout();
            this.gbCorte.ResumeLayout(false);
            this.gbCorte.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gbCantidadBilletes.ResumeLayout(false);
            this.gbCantidadBilletes.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip tsAcciones;
        private System.Windows.Forms.ToolStripButton tsbNuevo;
        private System.Windows.Forms.ToolStripButton tsbGuardar;
        private System.Windows.Forms.ToolStripButton tsbEliminar;
        private System.Windows.Forms.ToolStripButton tsbBuscar;
        private System.Windows.Forms.ToolStripButton btnUndo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnPrimeroPag;
        private System.Windows.Forms.ToolStripButton btnAtras;
        private System.Windows.Forms.ToolStripButton btnAdelante;
        private System.Windows.Forms.ToolStripButton btnUlitmoPag;
        private System.Windows.Forms.ToolStripButton tsbImprimir;
        private System.Windows.Forms.ToolStripButton btnCierra;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label lblFechaCorte;
        private System.Windows.Forms.Label lblFechaUltimoCorte;
        private System.Windows.Forms.GroupBox gbCorte;
        private System.Windows.Forms.TextBox txtCapital;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtManejo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAlmacenaje;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtInteres;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTotalLiqBoletas;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtBonificacion;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtEntradasCaja;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtTotalIngresos;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtTotalEngresos;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtSalidasCaja;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtBoletasEmpenadas;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtEgresos;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtIngresos;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtSaldoInicial;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtSaldoFinal;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cmbTipo;
        private System.Windows.Forms.GroupBox gbCantidadBilletes;
        private System.Windows.Forms.TextBox txtBilletes500;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtBilletes1000;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtMonedas5c;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtMonedas10c;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtMonedas20c;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtMonedas50c;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtMonedas1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtMonedas2;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtMonedas5;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtMonedas10;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtBilletes20;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtBilletes50;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtBilletes100;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtBilletes200;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtTotalImporteFisico;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtDiferencia;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Button btnFooterCierra;
        private System.Windows.Forms.Button btnFooterGuardar;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label lblCorteCaja;
        private System.Windows.Forms.TextBox txtUltimoCorte;
        private System.Windows.Forms.Button btnFooterPrimeroPag;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Button btnFooterAtrasPag;
        private System.Windows.Forms.Button btnFooterAdelantePag;
        private System.Windows.Forms.Button btnFooterUlitmoPag;
        private System.Windows.Forms.Button btnBusqueda;
        private System.Windows.Forms.Label lblEstatus;
        private System.Windows.Forms.Label lblDescEstatus;
    }
}