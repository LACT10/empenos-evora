﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmAsentamientosTipos : Form
    {

        public Controller.clsCtrlAsentamientosTipos ctrlAsentamientosTipos;
        public Core.clsCoreView clsView;

        public int _intUlitmoCodigo;
        

        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }
        
        public List<Properties_Class.clsAsentamientosTipos> lsDeshacerEstadoAsentamientosTipo;

        public frmAsentamientosTipos()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();
            this.ctrlAsentamientosTipos = new Controller.clsCtrlAsentamientosTipos();
            this.ctrlAsentamientosTipos.Paginacion();
            this.lsDeshacerEstadoAsentamientosTipo = new List<Properties_Class.clsAsentamientosTipos>();
            this.lsDeshacerEstadoAsentamientosTipo.Add(this.ctrlAsentamientosTipos.asentamientosTipo);            
        }
        private void FrmasentamientosTipos_Load(object sender, EventArgs e)
        {
        }

        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlAsentamientosTipos.asentamientosTipo.intId == 0) return;
            if (this.clsView.confirmacionEliminar("Tipo de asentamiento"))
            {
                this.ctrlAsentamientosTipos.Eliminar();
                if (this.ctrlAsentamientosTipos.bolResultado)
                {
                    this.ctrlAsentamientosTipos.Paginacion();
                    this.limpiar_textbox();
                    this.clsView.mensajeExitoDB("tipo de asentamiento", "elimino");
                }
            }
        }

        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlAsentamientosTipos.bolResutladoPaginacion)
            {
                this.clsView.principal.epMensaje.Clear();
                txtCodigo.Enabled = true;
                txtCodigo.Text = this.lsDeshacerEstadoAsentamientosTipo[0].strCodigo.ToString();
                txtDescripcion.Text = this.lsDeshacerEstadoAsentamientosTipo[0].strDescripcion.ToString();
                txtDescripcionAnt.Text = this.lsDeshacerEstadoAsentamientosTipo[0].strDescripcion.ToString();
                txtCodigo.Focus();
            }
        }


        private void tsbBuscar_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();

        }

        private void BtnBusqueda_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();
        }


        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlAsentamientosTipos.PrimeroPaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlAsentamientosTipos.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlAsentamientosTipos.SiguentePaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlAsentamientosTipos.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlAsentamientosTipos.AtrasPaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlAsentamientosTipos.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlAsentamientosTipos.UltimoPaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlAsentamientosTipos.bolResultado) return;
            this.llenar_formulario();
        }
        private void TsbNuevo_Click(object sender, EventArgs e)
        {            
            this.limpiar_textbox();
            this.ctrlAsentamientosTipos.asentamientosTipo.limpiarProps();            
            txtCodigo.Enabled = false;
            if (this.ctrlAsentamientosTipos.bolResutladoPaginacion)
            {
                DataTable dtDatos = this.ctrlAsentamientosTipos.objDataSet.Tables[0];
                this.intUlitmoCodigo = Int32.Parse(dtDatos.Rows[dtDatos.Rows.Count - 1][1].ToString()) + 1;
                this.agregarDeshacer();
            }
            else
            {
                this.intUlitmoCodigo = 1;
            }
           
            txtCodigo.Text = this.intUlitmoCodigo.ToString().PadLeft(this.ctrlAsentamientosTipos.asentamientosTipo.intNumCerosConsecutivo, '0');
            txtDescripcion.Focus();            

        }
        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {             
            txtDescripcion.Text = "";
        }

        private void TxtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);

        }
        private void TxtCodigo_Leave(object sender, EventArgs e)
        {
            if (txtCodigo.Text == "") return;
            string strCodigo = this.ctrlAsentamientosTipos.asentamientosTipo.generaCodigoConsecutivo(txtCodigo.Text);            
            this.ctrlAsentamientosTipos.Buscar(strCodigo);
            if (!(this.ctrlAsentamientosTipos.bolResultado) || txtCodigo.Text.Trim().Length == 0) return;
            
            this.llenar_formulario();
        }

        private void txtDescripcion_Leave(object sender, EventArgs e)
        {
            if (txtDescripcion.Text != txtDescripcionAnt.Text)
            {
                this.ctrlAsentamientosTipos.ValidarDescripcion(txtDescripcion.Text);
                if (this.ctrlAsentamientosTipos.bolResultado)
                {
                    this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("La descripción debe ser única, favor de verificar"));
                    txtDescripcion.Text = "";
                    txtDescripcion.Focus();
                }
            }
        }

        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            txtCodigo.Enabled = true;
            txtCodigo.Focus();
            this.clsView.principal.epMensaje.Clear();
        }

        private bool validacion()
        {

            bool bolValidacion = true;
            string strCodigo = txtCodigo.Text.TrimStart();
            string strDescripcion = txtDescripcion.Text.TrimStart();

            if (strCodigo == "")
            {
                txtCodigo.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigo, this.clsView.mensajeErrorValiForm2("El campo código es obligatorio, favor de ingresar un código "));
                bolValidacion = false;
            }

            if (strDescripcion == "")
            {
                txtDescripcion.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("El campo descripción es obligatorio, favor de ingresar una descripción "));
                bolValidacion = false;
            }
            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();

            if (validacion())
            {                
                this.ctrlAsentamientosTipos.asentamientosTipo.strCodigo = this.intUlitmoCodigo.ToString().PadLeft(this.ctrlAsentamientosTipos.asentamientosTipo.intNumCerosConsecutivo, '0');
                this.ctrlAsentamientosTipos.asentamientosTipo.strDescripcion = txtDescripcion.Text.ToString();
                this.ctrlAsentamientosTipos.asentamientosTipo.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlAsentamientosTipos.Guardar();

                if (this.ctrlAsentamientosTipos.mdlAsentamientosTipos.bolResultado)
                {
                    this.limpiar_textbox();
                    this.ctrlAsentamientosTipos.Paginacion();
                    this.clsView.mensajeExitoDB("tipo de asentamiento", "guardó");


                }
            }
        }

        private void mostraBusqueda()
        {
            if (this.ctrlAsentamientosTipos.bolResutladoPaginacion)
            {
                this.ctrlAsentamientosTipos.CargarFrmBusqueda();
                if (this.ctrlAsentamientosTipos.bolResultadoBusqueda)
                {
                    
                    this.llenar_formulario();
                    txtCodigo.Focus();                    
                }
            }

            
        }

        public void llenar_formulario()
        {
            if (this.ctrlAsentamientosTipos.asentamientosTipo.strCodigo.Length == 0) return;
            this.clsView.principal.epMensaje.Clear();
            txtCodigo.Enabled = true;
            txtCodigo.Text = this.ctrlAsentamientosTipos.asentamientosTipo.strCodigo;
            this.intUlitmoCodigo = Int32.Parse(this.ctrlAsentamientosTipos.asentamientosTipo.strCodigo);
            txtDescripcion.Text = this.ctrlAsentamientosTipos.asentamientosTipo.strDescripcion;
            txtDescripcionAnt.Text = this.ctrlAsentamientosTipos.asentamientosTipo.strDescripcion;
            lblEstatus.Text = this.ctrlAsentamientosTipos.asentamientosTipo.strEstatus;

        }

        public void agregarDeshacer()
        {
            if (this.ctrlAsentamientosTipos.bolResutladoPaginacion)
            { 
                this.lsDeshacerEstadoAsentamientosTipo.RemoveAt(0);
                this.lsDeshacerEstadoAsentamientosTipo.Add(this.ctrlAsentamientosTipos.asentamientosTipo);
            }
        }

        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void FrmAsentamientosTipos_Load_1(object sender, EventArgs e)
        {

            txtCodigo.MaxLength = this.ctrlAsentamientosTipos.asentamientosTipo.intNumCerosConsecutivo;

            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);
        }


    }
}