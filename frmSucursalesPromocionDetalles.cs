﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmSucursalesPromocionDetalles : Form
    {

        private int _intSucursalId;
        private int _intSucursalPromocionId;
        private bool _bolResultado;
        private bool _bolDgvValidar;

        public int intSucursalId { get => _intSucursalId; set => _intSucursalId = value; }
        public int intSucursalPromocionId { get => _intSucursalPromocionId; set => _intSucursalPromocionId = value; }
        public bool bolResultado { get => _bolResultado; set => _bolResultado = value; }

        public bool bolDgvValidar { get => _bolDgvValidar; set => _bolDgvValidar = value; }

        public Controller.clsCtrlSucursalesPromocion ctrlSucursalesPromocion;
        public Core.clsCoreView clsView;


        public frmSucursalesPromocionDetalles()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();


        }

        private void frmSucursalesPromocionDetalles_Load(object sender, EventArgs e)
        {
            
            cmbDias.SelectedIndex = 0;
            cmbEstatus.SelectedIndex = 0;
            this.bolDgvValidar = false;

            this.ctrlSucursalesPromocion = new  Controller.clsCtrlSucursalesPromocion();
            this.ctrlSucursalesPromocion.Buscar(this.intSucursalPromocionId);

            if (this.ctrlSucursalesPromocion.bolResultado) 
            {
                cmbDias.Text = this.ctrlSucursalesPromocion.sucursalesPromocion.strDia;
                txtNumDiasMin.Value = this.ctrlSucursalesPromocion.sucursalesPromocion.decDiaMin;
                txtPorentaje.Value = this.ctrlSucursalesPromocion.sucursalesPromocion.decPorcentaje;
                cmbEstatus.Text = this.ctrlSucursalesPromocion.sucursalesPromocion.strEstatus;
            }

            try
            {
                this.ctrlSucursalesPromocion.Paginacion_detalles(this.intSucursalId, this.intSucursalPromocionId);
                if (this.ctrlSucursalesPromocion.bolResultado)
                {
                    foreach (DataRow row in this.ctrlSucursalesPromocion.objDataSet.Tables[0].Rows)
                    {
                        dgvDetalles.Rows.Add(
                            row["prenda_tipo_id"].ToString(),
                            row["descripcion"].ToString(),
                            (row["renglon"].ToString() == "0") ? false : true
                        );
                    }
                }
            }
            catch (Exception ex) 
            { }


            
        }

        private void btnFooterGuardar_Click(object sender, EventArgs e)
        {

            this.clsView.principal.epMensaje.Clear();
            if (validacion())
            { 
                this.ctrlSucursalesPromocion.sucursalesPromocion.strDia = cmbDias.SelectedItem.ToString();
                this.ctrlSucursalesPromocion.sucursalesPromocion.decDiaMin = txtNumDiasMin.Value;
                this.ctrlSucursalesPromocion.sucursalesPromocion.decPorcentaje = txtPorentaje.Value;
                this.ctrlSucursalesPromocion.sucursalesPromocion.strEstatus = cmbEstatus.SelectedItem.ToString();

                this.ctrlSucursalesPromocion.sucursalesPromocion.intSucursal = this.intSucursalId;
                guardarDetalles();
                this.ctrlSucursalesPromocion.Guardar();
                if (this.ctrlSucursalesPromocion.mdlSucursalesPromocion.bolResultado)
                {
                    this.bolResultado = this.ctrlSucursalesPromocion.mdlSucursalesPromocion.bolResultado;
                    this.limpiar_textbox();                    
                    this.clsView.mensajeExitoDB("promocion", "guardó");
                    this.Close();          
                }
            }

            

        }

        private void btnFooterCierra_Click(object sender, EventArgs e)
        {
            this.bolResultado = this.ctrlSucursalesPromocion.bolResultado;
            this.Close();
        }


        public bool validacion() 
        {
            bool bolValidacion = true;
            int intDias = cmbDias.SelectedIndex;
            decimal intPorcentaje = txtPorentaje.Value;
            int intDiasMin = int.Parse(txtNumDiasMin.Value.ToString());
            
            //string strEstatus = cmbEstatus.SelectedItem.ToString();

            if (intDias <= 0)
            {
                cmbDias.Focus();
                this.clsView.principal.epMensaje.SetError(cmbDias, this.clsView.mensajeErrorValiForm2("El campo día es obligatorio, favor de selecionar un día "));
                bolValidacion = false;
            }

            if (intDiasMin <= 0) 
            {
                txtNumDiasMin.Focus();
                this.clsView.principal.epMensaje.SetError(txtNumDiasMin, this.clsView.mensajeErrorValiForm2("El campo número de días mínimo es obligatorio, favor de ingresar por lo menos un día "));
                bolValidacion = false;
            }

            if (intPorcentaje <= 0 || intPorcentaje > 100 )
            {
                txtPorentaje.Focus();
                this.clsView.principal.epMensaje.SetError(txtPorentaje, this.clsView.mensajeErrorValiForm2("El campo porcentaje es obligatorio, favor de ingresar un porcentaje "));
                bolValidacion = false;
            }

            /*if (strEstatus == "")
            {
                cmbEstatus.Focus();
                this.clsView.principal.epMensaje.SetError(cmbEstatus, this.clsView.mensajeErrorValiForm2("El campo género es obligatorio, favor de selecionar un género "));
                bolValidacion = false;
            }*/

            foreach (DataGridViewRow row in dgvDetalles.Rows)
            {
                if (Convert.ToBoolean(row.Cells["Sel"].Value.ToString()))
                {
                    this.bolDgvValidar = true;
                    break;
                }

            }

            if (!this.bolDgvValidar)
            {
                bolValidacion = false;
                this.clsView.mensajeErrorValiForm2("Seleccionar al menos un tipo de prenda para esta promoción ");
            }           
            return bolValidacion;
        }


        public void guardarDetalles() 
        {

            this.bolDgvValidar = false;
            string strSucursalPromocionId = "";
            if (this.ctrlSucursalesPromocion.sucursalesPromocion.intId == 0)
            {
                strSucursalPromocionId = "@id";
            }
            else
            {
                strSucursalPromocionId = this.ctrlSucursalesPromocion.sucursalesPromocion.intId.ToString();
            }
            int countGramajes = this.ctrlSucursalesPromocion.objDataSet.Tables[0].Rows.Count;
            this.ctrlSucursalesPromocion.sucursalesPromocion.strQueryDetalles = "INSERT INTO sucursales_promociones_detalles(sucursal_promocion_id,renglon, prenda_tipo_id)VALUES ";
            int i = 0;
            int j = 1;
            bool bolUltimoDato = false;
            bool bolNiguanDato = false;
            foreach (DataGridViewRow row in dgvDetalles.Rows)
            {
                if (i == countGramajes) break;

                if (Convert.ToBoolean(row.Cells["Sel"].Value.ToString()))
                {
                    bolNiguanDato = true;
                    this.bolDgvValidar = true;
                    if (i == countGramajes - 1)
                    {
                        this.ctrlSucursalesPromocion.sucursalesPromocion.strQueryDetalles += " (" + strSucursalPromocionId + "," + j + "," + row.Cells["prendaTipoId"].Value.ToString() + " ); ";
                        bolUltimoDato = true;
                        break;
                    }
                    else
                    {
                        this.ctrlSucursalesPromocion.sucursalesPromocion.strQueryDetalles += " (" + strSucursalPromocionId + "," + j + " , " + row.Cells["prendaTipoId"].Value.ToString() + " )|| ";
                    }
                    j++;
                }

                i++;
               
            }
            if (!bolNiguanDato)
            {
                this.ctrlSucursalesPromocion.sucursalesPromocion.strQueryDetalles = "";
            }
            else
            {
                if (bolUltimoDato)
                {
                    this.ctrlSucursalesPromocion.sucursalesPromocion.strQueryDetalles = this.ctrlSucursalesPromocion.sucursalesPromocion.strQueryDetalles.Replace("||", ",");
                }
                else
                {
                    int intStrIndex = this.ctrlSucursalesPromocion.sucursalesPromocion.strQueryDetalles.LastIndexOf("||");
                    this.ctrlSucursalesPromocion.sucursalesPromocion.strQueryDetalles = this.ctrlSucursalesPromocion.sucursalesPromocion.strQueryDetalles.Remove(intStrIndex).Insert(intStrIndex, ";").Replace("||", ",");
                }
            }

        }

        public void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            this.clsView.principal.epMensaje.Clear();
            cmbDias.SelectedIndex = 0;
            cmbEstatus.SelectedIndex = 0;
            txtPorentaje.Value = 0;
            cmbDias.Focus();
            foreach (DataGridViewRow row in dgvDetalles.Rows)
            {
                if (Convert.ToBoolean(row.Cells["Sel"].Value.ToString()))
                {
                    row.Cells["Sel"].Value = false;
                }
            }
        }
        private void chkDeselecionar_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDeselecionar.Checked) 
            {
                foreach (DataGridViewRow row in dgvDetalles.Rows)
                {
                    row.Cells["Sel"].Value = false;
                }
            }
        }
    }
}
