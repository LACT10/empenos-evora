﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebCam_Capture;


namespace EmpenosEvora
{
    public partial class frmFoto : Form
    {

        public PictureBox _pbGenerico;
        Library.clsWebCam webCam;
        public PictureBox pbGenerico { get => _pbGenerico; set => _pbGenerico = value; }

        public frmFoto()
        {
            InitializeComponent();
          
        }

        private void frmFoto_Load(object sender, EventArgs e)
        {
            this.webCam = new Library.clsWebCam();
            this.webCam.InitializeWebCam(ref imgVideo);
            this.webCam.Start();
        }

        private void bntCapture_Click(object sender, EventArgs e)
        {
            imgCapture.Image = imgVideo.Image;
            this.pbGenerico = imgCapture;
        }

        private void imgVideo_Click(object sender, EventArgs e)
        {

        }

        private void frmFoto_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.webCam.Stop();
        }

        private void btnCaptura_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
    }
}
