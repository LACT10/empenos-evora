﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmTiposDePrendas : Form
    {        
        public Controller.clsCtrlPrendasTipo ctrlPrendasTipo;
        public Core.clsCoreView clsView;

        public int _intUlitmoCodigo;
        
        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }
        

        public List<Properties_Class.clsPrendasTipo> lsDeshacerPrendasTipo;        

        public frmTiposDePrendas()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();
            this.ctrlPrendasTipo = new Controller.clsCtrlPrendasTipo();
            this.ctrlPrendasTipo.Paginacion();
            this.lsDeshacerPrendasTipo = new List<Properties_Class.clsPrendasTipo>();
            this.lsDeshacerPrendasTipo.Add(this.ctrlPrendasTipo.prendasTipo);
            
        }
        private void Frmcajas_Load(object sender, EventArgs e)
        {
        }

        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlPrendasTipo.prendasTipo.intId == 0) return;
            if (this.clsView.confirmacionEliminar("Tipos de prenda"))
            {
                this.ctrlPrendasTipo.Eliminar();
                if (this.ctrlPrendasTipo.bolResultado)
                {
                    this.ctrlPrendasTipo.Paginacion();
                    this.limpiar_textbox();

                    this.ctrlPrendasTipo.Buscar(this.ctrlPrendasTipo.prendasTipo.strCodigo);
                    this.llenar_formulario();

                    txtCodigo.Enabled = true;
                    txtCodigo.Focus();

                    this.clsView.mensajeExitoDB("tipo de prenda", "elimino");
                }
            }
        }

        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlPrendasTipo.bolResutladoPaginacion)
            {
                txtCodigo.Text = this.lsDeshacerPrendasTipo[0].strCodigo.ToString();
                txtDescripcion.Text = this.lsDeshacerPrendasTipo[0].strDescripcion.ToString();
                rbtGramaje.Checked = this.ctrlPrendasTipo.prendasTipo.asignarCapturar(this.ctrlPrendasTipo.prendasTipo.strCapturarGramaje);
                rbtEmail.Checked = this.ctrlPrendasTipo.prendasTipo.asignarCapturar(this.ctrlPrendasTipo.prendasTipo.strCapturarEmailCelular);
                rbtSerie.Checked = this.ctrlPrendasTipo.prendasTipo.asignarCapturar(this.ctrlPrendasTipo.prendasTipo.strCapturarSerieVehiculo);

            }
        }

        private void tsbBuscar_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();

        }

        private void BtnBusqueda_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();
        }

        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlPrendasTipo.PrimeroPaginacion();
            if (!this.ctrlPrendasTipo.bolResutladoPaginacion) return;
            this.llenar_formulario();
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlPrendasTipo.SiguentePaginacion();
            if (!this.ctrlPrendasTipo.bolResutladoPaginacion) return;
            this.llenar_formulario();
        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlPrendasTipo.AtrasPaginacion();
            if (!this.ctrlPrendasTipo.bolResutladoPaginacion) return;
            this.llenar_formulario();
        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlPrendasTipo.UltimoPaginacion();
            if (!this.ctrlPrendasTipo.bolResutladoPaginacion) return;
            this.llenar_formulario();
        }
        private void TsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();
            this.ctrlPrendasTipo.prendasTipo.limpiarProps();
            txtCodigo.Enabled = false;
            txtCodigo.Text = "NUEVO";

            lblDescEstatus.Visible = true;
            lblEstatus.Visible = true;
            lblEstatus.Text = "NUEVO";
            /*if (this.ctrlPrendasTipo.bolResutladoPaginacion)
            {
                DataTable dtDatos = this.ctrlPrendasTipo.objDataSet.Tables[0];
                this.intUlitmoCodigo = Int32.Parse(dtDatos.Rows[dtDatos.Rows.Count - 1][1].ToString()) + 1;
                this.agregarDeshacer();
            }
            else
            {
                this.intUlitmoCodigo = 1;
            }

            txtCodigo.Text = this.intUlitmoCodigo.ToString().PadLeft(this.ctrlPrendasTipo.prendasTipo.intNumCerosConsecutivo, '0'); */

        }
        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {
            txtDescripcion.Text = "";
        }

        private void TxtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);

        }
        private void txtCodigo_Leave(object sender, EventArgs e)
        {

            if (txtCodigo.Text == "") return;
            string strCodigo = this.ctrlPrendasTipo.prendasTipo.generaCodigoConsecutivo(txtCodigo.Text);
            this.ctrlPrendasTipo.Buscar(strCodigo);
            if (!(this.ctrlPrendasTipo.bolResultado)) 
            {
                this.limpiar_textbox();
                return;
            }
            this.llenar_formulario();
        }

        private void txtDescripcion_Leave(object sender, EventArgs e)
        {
            if (txtDescripcion.Text != txtDescripcionAnt.Text)
            {
                this.ctrlPrendasTipo.ValidarDescripcion(txtDescripcion.Text);
                if (this.ctrlPrendasTipo.bolResultado)
                {
                    this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("La descripción debe ser única, favor de verificar"));
                    txtDescripcion.Text = "";
                }
            }
        }

        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            lblEstatus.Text = "";
            txtCodigo.Enabled = true;
            txtDescripcionAnt.Text = "";
            rbtGramaje.Enabled = true;
            rbtGramaje.Checked = false;
            rbtEmail.Enabled = true;
            rbtEmail.Checked = false;
            rbtSerie.Enabled = true;
            rbtSerie.Checked = false;
            lblEstatus.Text = "";
            chkDeselecionar.Checked = false;
        }

        private bool validacion()
        {

            bool bolValidacion = true;
            string strCodigo = txtCodigo.Text.TrimStart();
            string strDescripcion = txtDescripcion.Text.TrimStart();

            if (strCodigo == "")
            {
                txtCodigo.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigo, this.clsView.mensajeErrorValiForm2("El campo código es obligatorio, favor de ingresar un código "));
                bolValidacion = false;
            }

            if (strDescripcion == "")
            {
                txtDescripcion.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("El campo descripción es obligatorio, favor de ingresar una descripción "));
                bolValidacion = false;
            }
            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();

            if (validacion())
            {

                if (this.ctrlPrendasTipo.bolResutladoPaginacion)
                {
                    DataTable dtDatos = this.ctrlPrendasTipo.objDataSet.Tables[0];
                    this.intUlitmoCodigo = Int32.Parse(dtDatos.Rows[dtDatos.Rows.Count - 1][1].ToString()) + 1;
                    this.agregarDeshacer();
                }
                else
                {
                    this.intUlitmoCodigo = 1;
                }

                txtCodigo.Text = this.intUlitmoCodigo.ToString().PadLeft(this.ctrlPrendasTipo.prendasTipo.intNumCerosConsecutivo, '0');

                this.ctrlPrendasTipo.prendasTipo.strCodigo = this.intUlitmoCodigo.ToString().PadLeft(this.ctrlPrendasTipo.prendasTipo.intNumCerosConsecutivo, '0');
                this.ctrlPrendasTipo.prendasTipo.strDescripcion = txtDescripcion.Text.ToString();
                this.ctrlPrendasTipo.prendasTipo.strCapturarGramaje = (rbtGramaje.Checked) ? "SI" : "NO";
                this.ctrlPrendasTipo.prendasTipo.strCapturarEmailCelular = (rbtEmail.Checked) ? "SI" : "NO";
                this.ctrlPrendasTipo.prendasTipo.strCapturarSerieVehiculo = (rbtSerie.Checked) ? "SI" : "NO";
                this.ctrlPrendasTipo.prendasTipo.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlPrendasTipo.Guardar();

                if (this.ctrlPrendasTipo.mdlPrendasTipo.bolResultado)
                {
                    this.limpiar_textbox();
                    this.ctrlPrendasTipo.Paginacion();
                    
                    this.ctrlPrendasTipo.Buscar(this.ctrlPrendasTipo.prendasTipo.strCodigo);
                    this.llenar_formulario();

                    txtCodigo.Enabled = true;
                    txtCodigo.Focus();
                    this.clsView.mensajeExitoDB("tipo de prenda", "guardó");

                }
            }
        }

        public void mostraBusqueda()
        {
            if (this.ctrlPrendasTipo.bolResutladoPaginacion)
            {
                this.ctrlPrendasTipo.CargarFrmBusqueda();

                if (this.ctrlPrendasTipo.bolResultadoBusqueda)
                {
                    this.llenar_formulario();
                    txtCodigo.Focus();
                }
            }

            
        }

        public void llenar_formulario()
        {
            if (this.ctrlPrendasTipo.prendasTipo.strCodigo.Length == 0)
            {

                lblDescEstatus.Visible = false;
                lblEstatus.Visible = false;
                return;
            }

            txtCodigo.Text = this.ctrlPrendasTipo.prendasTipo.strCodigo;
            txtCodigo.Enabled = true;
            txtDescripcion.Text = this.ctrlPrendasTipo.prendasTipo.strDescripcion;
            txtDescripcionAnt.Text = this.ctrlPrendasTipo.prendasTipo.strDescripcion;            
            rbtGramaje.Checked = this.ctrlPrendasTipo.prendasTipo.asignarCapturar(this.ctrlPrendasTipo.prendasTipo.strCapturarGramaje);
            rbtEmail.Checked = this.ctrlPrendasTipo.prendasTipo.asignarCapturar(this.ctrlPrendasTipo.prendasTipo.strCapturarEmailCelular);
            rbtSerie.Checked = this.ctrlPrendasTipo.prendasTipo.asignarCapturar(this.ctrlPrendasTipo.prendasTipo.strCapturarSerieVehiculo);
            lblDescEstatus.Visible = true;
            lblEstatus.Visible = true;
            lblEstatus.Text = this.ctrlPrendasTipo.prendasTipo.strEstatus;
            this.intUlitmoCodigo = Int32.Parse(this.ctrlPrendasTipo.prendasTipo.strCodigo);

            this.clsView.permisosMovimientoMenu(tsAcciones, this.ctrlPrendasTipo.prendasTipo.strEstatus);


        }

        public void agregarDeshacer()
        {
            if (this.ctrlPrendasTipo.bolResutladoPaginacion)
            {
                this.lsDeshacerPrendasTipo.RemoveAt(0);
                this.lsDeshacerPrendasTipo.Add(this.ctrlPrendasTipo.prendasTipo);
            }
        }

        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void FrmTiposDePrendas_Load(object sender, EventArgs e)
        {
            txtCodigo.MaxLength = this.ctrlPrendasTipo.prendasTipo.intNumCerosConsecutivo;
            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);

        }

        private void chkDeselecionarBoleta_CheckedChanged(object sender, EventArgs e)
        {
            rbtEmail.Checked = false;
            rbtGramaje.Checked = false;
            rbtSerie.Checked = false;
        }

        private void tsbReactivar_Click(object sender, EventArgs e)
        {

        }
    }
}