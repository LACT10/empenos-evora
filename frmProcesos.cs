﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmProcesos : Form
    {

        DataGridView dgvSubProcesosGenerico;

        public Controller.clsCtrlProcesos ctrlProcesos;
        public Controller.clsCtrlSubProcesos ctrlSubProcesos;
        public Core.clsCoreView clsView;
        public bool _bolGuardarSubProcesos;
        public bool _bolBtnEliminar;

        public bool bolGuardarSubProcesos  { get => _bolGuardarSubProcesos; set => _bolGuardarSubProcesos = value; }
        public bool bolBtnEliminar { get => _bolBtnEliminar; set => _bolBtnEliminar = value; }


        public int _intUlitmoCodigo;
        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }

        public List<Properties_Class.clsProcesos> lsDeshacerProcesos;

        public List<string> intListEliminar;

        public frmProcesos()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();

            this.ctrlProcesos = new Controller.clsCtrlProcesos();
            this.ctrlProcesos.Paginacion();

            this.lsDeshacerProcesos = new List<Properties_Class.clsProcesos>();
            this.lsDeshacerProcesos.Add(this.ctrlProcesos.procesos);

            this.bolGuardarSubProcesos = false;
            this.dgvSubProcesosGenerico = dgvSubProcesosProceso;
            this.dgvSubProcesosGenerico.AutoGenerateColumns = true;

            /*this.dgvSubProcesosGenerico = dgvSubProcesosProceso2;
            this.dgvSubProcesosGenerico.AutoGenerateColumns = true;*/

            this.intListEliminar  = new List<string>();
            //this.bolBtnEliminar = true;
            this.dgvSubprocesos();
        }

        private void frmProcesos_Load(object sender, EventArgs e)
        {
            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);

        }

        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void TsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlProcesos.procesos.intId == 0) return;
            if (this.clsView.confirmacionEliminar("Procesos")) 
            {
                this.ctrlProcesos.Eliminar();
                if (this.ctrlProcesos.bolResultado)
                {
                    this.ctrlProcesos.Paginacion();
                    this.limpiar_textbox();
                    this.clsView.mensajeExitoDB("procesos", "elimino");
                    this.dgvSubprocesos();
                }
            }
            
        }


        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlProcesos.bolResutladoPaginacion)
            {
                txtDescripcion.Text = this.lsDeshacerProcesos[0].strDescripcion.ToString();
                txtNombreMenu.Text = this.lsDeshacerProcesos[0].strNombreMenu.ToString();                
                txtDescripcionProcesosPadre.Text = this.lsDeshacerProcesos[0].strProcesoPadreDescripcion.ToString();

            }
        }


        private void TsbBuscar_Click(object sender, EventArgs e)
        {
           this.mostraBusqueda();
        }


        private void btnBusqueda_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();
        }

        private void btnBusquedaPadre_Click(object sender, EventArgs e)
        {
            this.mostraBusquedaPadre();
        }

        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlProcesos.PrimeroPaginacion();
            if (!this.ctrlProcesos.bolResultado) return;

            this.subprocesos(this.ctrlProcesos.procesos.intId);
            this.intListEliminar.Clear();
            this.llenar_formulario();
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlProcesos.SiguentePaginacion();
            if (!this.ctrlProcesos.bolResultado) return;
            this.subprocesos(this.ctrlProcesos.procesos.intId);
            this.intListEliminar.Clear();
            this.llenar_formulario();

        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlProcesos.AtrasPaginacion();
            if (!this.ctrlProcesos.bolResultado) return;
            this.subprocesos(this.ctrlProcesos.procesos.intId);
            this.intListEliminar.Clear();
            this.llenar_formulario();

        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlProcesos.UltimoPaginacion();
            if (!this.ctrlProcesos.bolResultado) return;
            this.subprocesos(this.ctrlProcesos.procesos.intId);
            this.intListEliminar.Clear();
            this.llenar_formulario();

        }
        private void TsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();
            this.ctrlProcesos.procesos.limpiarProps();
            txtId.Text = "";
            txtProcesosPadreId.Text = "";
            this.dgvSubprocesos();

            if (this.ctrlProcesos.bolResutladoPaginacion)
            {   
                this.agregarDeshacer();
            }

        }


        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            //this.clsView.soloTextoVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            this.bolGuardarSubProcesos = false;
            this.intListEliminar.Clear();
        }

        private bool validacion()
        {

            bool bolValidacion = true;            
            string strDescripcion = txtDescripcion.Text.TrimStart();
            string strNombreMenu = txtNombreMenu.Text.TrimStart();

            if (strDescripcion == "")
            {
                txtDescripcion.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("El campo descripción es obligatorio, favor de ingresar una descripción "));
                bolValidacion = false;
            }

            if (strNombreMenu == "")
            {
                txtNombreMenu.Focus();
                this.clsView.principal.epMensaje.SetError(txtNombreMenu, this.clsView.mensajeErrorValiForm2("El campo nombre menú es obligatorio, favor de ingresar un nombre menú "));
                bolValidacion = false;
            }

            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();
            if (validacion())
            {
                this.ctrlProcesos.procesos.intId = Int32.Parse((txtId.Text == "") ? "0" : txtId.Text);
                this.ctrlProcesos.procesos.strDescripcion = txtDescripcion.Text.ToString();
                this.ctrlProcesos.procesos.strNombreMenu = txtNombreMenu.Text.ToString();
                this.ctrlProcesos.procesos.intProcesoPadreId = Int32.Parse((txtProcesosPadreId.Text == "") ? "0" : txtProcesosPadreId.Text);

                this.guardarSubProcesos();
                
                this.ctrlProcesos.Guardar();

                if (this.ctrlProcesos.mdlProcesos.bolResultado)
                {
                    this.limpiar_textbox();
                    this.ctrlProcesos.Paginacion();
                    this.dgvSubprocesos();
                    this.clsView.mensajeExitoDB("procesos", "guardó");
                    

                }
            }
        }

        public void mostraBusqueda()
        {
            if (this.ctrlProcesos.bolResutladoPaginacion)
            {
                this.ctrlProcesos.CargarFrmBusqueda();

                if (this.ctrlProcesos.bolResultadoBusqueda)
                {
                    this.subprocesos(this.ctrlProcesos.procesos.intId);
                    this.llenar_formulario();
                    txtDescripcion.Focus();
                }
            }
            
        }

        public void mostraBusquedaPadre()
        {
            if (this.ctrlProcesos.bolResutladoPaginacion)
            {
                this.ctrlProcesos.CargarFrmBusqueda();

                if (this.ctrlProcesos.bolResultadoBusqueda)
                {
                    this.llenar_formulario_padre();
                    txtDescripcion.Focus();
                }
            }
            
        }

       

        public void llenar_formulario()
        {

            if (this.ctrlProcesos.procesos.strDescripcion.Length == 0) return;
            txtId.Text = this.ctrlProcesos.procesos.intId.ToString();
            txtDescripcion.Text = this.ctrlProcesos.procesos.strDescripcion;
            txtNombreMenu.Text = this.ctrlProcesos.procesos.strNombreMenu;
            txtProcesosPadreId.Text = this.ctrlProcesos.procesos.intProcesoPadreId.ToString();
            txtDescripcionProcesosPadre.Text = this.ctrlProcesos.procesos.strProcesoPadreDescripcion;       
        }

        public void llenar_formulario_padre()
        {

            if (this.ctrlProcesos.procesos.strDescripcion.Length == 0) return;

            txtProcesosPadreId.Text = this.ctrlProcesos.procesos.intId.ToString();
            txtDescripcionProcesosPadre.Text = this.ctrlProcesos.procesos.strDescripcion;
        }
        


        public void agregarDeshacer()
        {
            this.lsDeshacerProcesos.RemoveAt(0);
            this.lsDeshacerProcesos.Add(this.ctrlProcesos.procesos);
        }





        public void guardarSubProcesos() 
        {
            string intProcesoId = "";
            int numRows = this.dgvSubProcesosGenerico.Rows.Count;
            this.ctrlProcesos.procesos.strQuerySubProcesos = "INSERT INTO subprocesos(proceso_id,descripcion, nombre_menu) VALUES ";
            this.ctrlProcesos.procesos.strQuerySubProcesosActualizar = " UPDATE subprocesos "+
                                                                            " SET descripcion = CASE "+
                                                                                " {0} "+                                                                                
                                                                            " ELSE descripcion END,"+
                                                                            " nombre_menu = CASE "+
                                                                                " {1} "+                                                                                
                                                                            " ELSE nombre_menu END "+
                                                                            " WHERE subproceso_id IN( {2} ); ";

            string strDescripcion = "";
            string strNombreMenu = "";

            string strDescripcionUpdate = "";
            string strNombreMenuUpdate = "";
            string strSubprocesosIds = "";
            bool bolSubprocesosId = false;

            int i = 0;

            if (this.ctrlProcesos.procesos.intId == 0)
            {
                intProcesoId = "@id";
            }
            else
            {
                intProcesoId = this.ctrlProcesos.procesos.intId.ToString();
            }

            if (numRows >= 1)
            {

                foreach (DataGridViewRow row in this.dgvSubProcesosGenerico.Rows)
                {
                    if (i == numRows - 1) break;

                    
                    //[0]//subproceso_id
                    //[4]//Desprcion
                    //[5]//Nombre                 
                    string strSubprocesosId = "";

                    if (row.Cells[0].Value.ToString() != "")
                    {
                        strSubprocesosId = row.Cells[0].Value.ToString();
                    }



                    strDescripcion = row.Cells[4].Value.ToString().Trim();
                    strNombreMenu = row.Cells[5].Value.ToString().Trim();

                    if (strDescripcion == "" || strNombreMenu == "")
                    {
                        this.bolGuardarSubProcesos = false;
                        break;
                    }

                    if (strSubprocesosId == "")
                    {

                        if (i == numRows - 1)
                        {
                            this.ctrlProcesos.procesos.strQuerySubProcesos += " (" + intProcesoId + ",'" + strDescripcion + "','" + strNombreMenu + "' ); ";
                            break;
                        }
                        else
                        {
                            this.ctrlProcesos.procesos.strQuerySubProcesos += " (" + intProcesoId + ",'" + strDescripcion + "','" + strNombreMenu + "' )|| ";
                        }


                        this.bolGuardarSubProcesos = true;
                    }
                    else
                    {
                        strDescripcionUpdate += " WHEN subproceso_id = " + strSubprocesosId + " THEN '" + strDescripcion + "' ";
                        strNombreMenuUpdate += " WHEN subproceso_id = " + strSubprocesosId + " THEN '" + strNombreMenu + "' ";
                        strSubprocesosIds += strSubprocesosId + ", ";
                        bolSubprocesosId = true;
                    }

                    i++;
                }

                if (bolSubprocesosId)
                {

                    int intStrIndex = strSubprocesosIds.LastIndexOf(",");
                    strSubprocesosIds = strSubprocesosIds.Remove(intStrIndex).Insert(intStrIndex, " ");
                    this.ctrlProcesos.procesos.strQuerySubProcesosActualizar = String.Format(this.ctrlProcesos.procesos.strQuerySubProcesosActualizar, strDescripcionUpdate, strNombreMenuUpdate, strSubprocesosIds);


                }
                else 
                {
                    this.ctrlProcesos.procesos.strQuerySubProcesosActualizar = "";
                }


                if (this.intListEliminar.Count > 0)
                {
                    this.ctrlProcesos.procesos.strQuerySubProcesosEliminar = "DELETE from subprocesos WHERE subproceso_id in ({0});";
                    string idEliminar = "";
                    int intEliminar = 0;
                    this.intListEliminar.ForEach(delegate (string id)
                    {

                        if (id != "") 
                        {
                            if (this.intListEliminar.Count - 1 == intEliminar)
                            {
                                idEliminar += id.ToString();
                            }
                            else
                            {
                                idEliminar += id.ToString() + ",";
                            }

                            intEliminar++;
                        }
                        


                        

                    });

                    if (intEliminar == 0) 
                    {
                        this.ctrlProcesos.procesos.strQuerySubProcesosEliminar = "";
                    }
                    else
                    {
                        this.ctrlProcesos.procesos.strQuerySubProcesosEliminar = String.Format(this.ctrlProcesos.procesos.strQuerySubProcesosEliminar, idEliminar);
                    }
                    

                }

                if (this.bolGuardarSubProcesos == false)
                {

                    this.ctrlProcesos.procesos.strQuerySubProcesos = "";
                }
                else
                {
                    int intStrIndex = this.ctrlProcesos.procesos.strQuerySubProcesos.LastIndexOf("||");
                    this.ctrlProcesos.procesos.strQuerySubProcesos = this.ctrlProcesos.procesos.strQuerySubProcesos.Remove(intStrIndex).Insert(intStrIndex, ";").Replace("||", ",");
                }

            }
            else
            {
                this.ctrlProcesos.procesos.strQuerySubProcesos = "";
            }
        }

        public void subprocesos(int intSubProcesos)
        {
            this.ctrlSubProcesos = new Controller.clsCtrlSubProcesos();
            this.ctrlSubProcesos.Buscar(intSubProcesos.ToString());

            if (this.ctrlSubProcesos.bolResultado)
            {

                dgvSubProcesosProceso.DataSource = this.ctrlSubProcesos.objDataSet.Tables[0].DefaultView; // dataset       
                                                                                                          //Son los nombre de campos que no quiero que se muestran en el datagridview
                this.clsView.lsBloquearCol = new List<string>()
                {
                        "subproceso_id",
                        "descripcion",
                        "nombre_menu",
                        "proceso_id"
                };
                //Pasar datagridview para que se puede usar en core
                this.clsView.dgvGenrico = dgvSubProcesosProceso;
                this.clsView.bloquearColumnasGrid();
                this.clsView.configuracionDataGridView();
            }
            else
            {
                dgvSubProcesosProceso.DataSource = null;
                this.dgvSubprocesos();
            }


        }

        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void bloquearColumnasGrid()
        {
            this.dgvSubProcesosGenerico.Columns["proceso_id"].Visible = false;
            this.dgvSubProcesosGenerico.Columns["subproceso_id"].Visible = false;
            this.dgvSubProcesosGenerico.Columns["descripcion"].Visible = false;
            this.dgvSubProcesosGenerico.Columns["nombre_menu"].Visible = false;
        }

        private void btnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();

        }

        private void btnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void dgvSubprocesos() 
        {
            this.dgvSubProcesosGenerico = dgvSubProcesosProceso;
            this.dgvSubProcesosGenerico.AutoGenerateColumns = true;

            DataTable dtProcesos = new DataTable();

            dtProcesos.Columns.Add(new DataColumn("proceso_id", typeof(int)));
            dtProcesos.Columns.Add(new DataColumn("subproceso_id", typeof(int)));
            dtProcesos.Columns.Add(new DataColumn("descripcion", typeof(string)));
            dtProcesos.Columns.Add(new DataColumn("nombre_menu", typeof(string)));
            dtProcesos.Columns.Add(new DataColumn("Descripción", typeof(string)));
            dtProcesos.Columns.Add(new DataColumn("Nombre de menú", typeof(string)));
           
            this.dgvSubProcesosGenerico.DataSource = dtProcesos.DefaultView;
            this.bloquearColumnasGrid();
            this.dgvSubProcesosGenerico.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSubProcesosGenerico.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;

            
        }

       /* public void dgvSubprocesos()
        { 

            this.dgvSubProcesosGenerico.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSubProcesosGenerico.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
           
            DataGridViewButtonColumn btnEliminar = new DataGridViewButtonColumn();
            btnEliminar.HeaderText = "Eliminar";
            btnEliminar.Name = "btnEliminar"; btnEliminar.Text = "Eliminar";
            btnEliminar.UseColumnTextForButtonValue = true;
            this.dgvSubProcesosGenerico.Columns.Add(btnEliminar);
               
        }*/

        private void dgvSubProcesosProceso_CellClick(object sender, DataGridViewCellEventArgs e)
        {

           /* if (e.ColumnIndex == 0) 
            {
                dgvSubProcesosProceso.CurrentRow.Selected = true;
                int dgvIndex = dgvSubProcesosProceso.CurrentRow.Index;

                if (dgvSubProcesosProceso.Rows[dgvIndex].Cells[1] != null) 
                {   
                    this.intListEliminar.Add(dgvSubProcesosProceso.Rows[dgvIndex].Cells[1].Value.ToString());
                }
                dgvSubProcesosProceso.Rows.RemoveAt(dgvIndex);
            }*/

        }


        private void Eliminar_Click(object sender, EventArgs e)
        {
            

        }
        private void Porcentaje_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && !(e.KeyChar == '.'))
            {
                e.Handled = true;
            }
        }

        private void dgvSubProcesosProceso_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dgvSubProcesosProceso_KeyPress(object sender, KeyPressEventArgs e)
        {
            /*if (e.KeyCode == Keys.Delete)
            {
                MessageBox.Show("delete pressed");
                e.Handled = true;
            }*/
        }

        private void dgvSubProcesosProceso_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                dgvSubProcesosProceso.CurrentRow.Selected = true;
                int dgvIndex = dgvSubProcesosProceso.CurrentRow.Index;

                if (dgvSubProcesosProceso.Rows[dgvIndex].Cells[0] != null)
                {
                    this.intListEliminar.Add(dgvSubProcesosProceso.Rows[dgvIndex].Cells[0].Value.ToString());
                }


                if (this.dgvSubProcesosGenerico.Rows.Count > 1) 
                {
                    dgvSubProcesosProceso.Rows.RemoveAt(dgvIndex);
                }
                
                e.Handled = true;
            }
        }
    }
}
