﻿namespace EmpenosEvora
{
    partial class frmClientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCodigo = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.dtpFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.label19 = new System.Windows.Forms.Label();
            this.cmbGenero = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCodigoOcupacion = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtDescripcionOcupacion = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtRFC = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtCURP = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtCorreoElectronico = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtTelefono01 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtComentario = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.txtApellidoPaterno = new System.Windows.Forms.TextBox();
            this.txtDescripcionIdentificacion = new System.Windows.Forms.TextBox();
            this.txtCodigoIdentificacion = new System.Windows.Forms.TextBox();
            this.txtTelefono02 = new System.Windows.Forms.TextBox();
            this.txtCalle = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtApellidoMaterno = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtApellidoMaternoCotitular = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtApellidoPaternoCotitular = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNombreCotitular = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtApellidoMaternoBeneficario = new System.Windows.Forms.TextBox();
            this.txtApellidoPaternoBeneficario = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtNombreBeneficario = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnFotografia = new System.Windows.Forms.Button();
            this.btnHuella = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNoExt = new System.Windows.Forms.TextBox();
            this.txtNoInt = new System.Windows.Forms.TextBox();
            this.txtIdOcupacion = new System.Windows.Forms.TextBox();
            this.txtAsentamientoId = new System.Windows.Forms.TextBox();
            this.txtDescripcionAsentamiento = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtIdIdentificacion = new System.Windows.Forms.TextBox();
            this.txtCodigoPostal = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCodigoMunicipio = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCodigoEstado = new System.Windows.Forms.TextBox();
            this.txtDescripcionEstado = new System.Windows.Forms.TextBox();
            this.txtDescripcionMunicipio = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tsAcciones = new System.Windows.Forms.ToolStrip();
            this.tsbNuevo = new System.Windows.Forms.ToolStripButton();
            this.tsbGuardar = new System.Windows.Forms.ToolStripButton();
            this.tsbBuscar = new System.Windows.Forms.ToolStripButton();
            this.btnUndo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrimeroPag = new System.Windows.Forms.ToolStripButton();
            this.btnAtras = new System.Windows.Forms.ToolStripButton();
            this.btnAdelante = new System.Windows.Forms.ToolStripButton();
            this.btnUlitmoPag = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.btnCierra = new System.Windows.Forms.ToolStripButton();
            this.txtClienteId = new System.Windows.Forms.TextBox();
            this.txtIdentificacionValidar = new System.Windows.Forms.TextBox();
            this.btnIdentificacion = new System.Windows.Forms.Button();
            this.cmbEstatus = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnVerificarHuella = new System.Windows.Forms.Button();
            this.txtCurpAntri = new System.Windows.Forms.TextBox();
            this.btnBusquedaIdentif = new System.Windows.Forms.Button();
            this.btnFooterCierra = new System.Windows.Forms.Button();
            this.btnFooterGuardar = new System.Windows.Forms.Button();
            this.btnBusquedaAsentamiento = new System.Windows.Forms.Button();
            this.btnBusqueda = new System.Windows.Forms.Button();
            this.btnBusquedaOcupacion = new System.Windows.Forms.Button();
            this.txtNumeroIdentificacion = new System.Windows.Forms.TextBox();
            this.lblSerieIdentificacion = new System.Windows.Forms.Label();
            this.tsbEliminar = new System.Windows.Forms.ToolStripButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tsAcciones.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(8, 40);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(40, 13);
            this.lblCodigo.TabIndex = 160;
            this.lblCodigo.Text = "Código";
            // 
            // txtCodigo
            // 
            this.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigo.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigo.Location = new System.Drawing.Point(104, 40);
            this.txtCodigo.MaxLength = 5;
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(48, 20);
            this.txtCodigo.TabIndex = 1;
            this.txtCodigo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtCodigo_KeyPress);
            this.txtCodigo.Leave += new System.EventHandler(this.txtCodigo_Leave);
            // 
            // dtpFechaNacimiento
            // 
            this.dtpFechaNacimiento.Cursor = System.Windows.Forms.Cursors.Default;
            this.dtpFechaNacimiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaNacimiento.Location = new System.Drawing.Point(496, 72);
            this.dtpFechaNacimiento.Name = "dtpFechaNacimiento";
            this.dtpFechaNacimiento.Size = new System.Drawing.Size(96, 20);
            this.dtpFechaNacimiento.TabIndex = 7;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Cursor = System.Windows.Forms.Cursors.Default;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(384, 72);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(106, 13);
            this.label19.TabIndex = 208;
            this.label19.Text = "Fecha de nacimiento";
            // 
            // cmbGenero
            // 
            this.cmbGenero.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmbGenero.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGenero.FormattingEnabled = true;
            this.cmbGenero.Items.AddRange(new object[] {
            "Selecione una opción",
            "MASCULINO",
            "FEMENINO",
            "SIN ESPECIFICAR"});
            this.cmbGenero.Location = new System.Drawing.Point(248, 72);
            this.cmbGenero.Name = "cmbGenero";
            this.cmbGenero.Size = new System.Drawing.Size(120, 21);
            this.cmbGenero.TabIndex = 6;
            this.cmbGenero.SelectedIndexChanged += new System.EventHandler(this.cmbGenero_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Cursor = System.Windows.Forms.Cursors.Default;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(200, 72);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 211;
            this.label9.Text = "Género";
            // 
            // txtCodigoOcupacion
            // 
            this.txtCodigoOcupacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoOcupacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoOcupacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoOcupacion.Location = new System.Drawing.Point(104, 168);
            this.txtCodigoOcupacion.MaxLength = 50;
            this.txtCodigoOcupacion.Name = "txtCodigoOcupacion";
            this.txtCodigoOcupacion.Size = new System.Drawing.Size(48, 20);
            this.txtCodigoOcupacion.TabIndex = 17;
            this.txtCodigoOcupacion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigoOcupacion_KeyPress);
            this.txtCodigoOcupacion.Leave += new System.EventHandler(this.txtCodigoOcupacion_Leave);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Cursor = System.Windows.Forms.Cursors.Default;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(8, 168);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 13);
            this.label16.TabIndex = 213;
            this.label16.Text = "Ocupación";
            // 
            // txtDescripcionOcupacion
            // 
            this.txtDescripcionOcupacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionOcupacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionOcupacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcionOcupacion.Location = new System.Drawing.Point(160, 168);
            this.txtDescripcionOcupacion.MaxLength = 250;
            this.txtDescripcionOcupacion.Name = "txtDescripcionOcupacion";
            this.txtDescripcionOcupacion.ReadOnly = true;
            this.txtDescripcionOcupacion.Size = new System.Drawing.Size(760, 20);
            this.txtDescripcionOcupacion.TabIndex = 18;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Cursor = System.Windows.Forms.Cursors.Default;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(8, 136);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 13);
            this.label17.TabIndex = 216;
            this.label17.Text = "Identificación";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Cursor = System.Windows.Forms.Cursors.Default;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(624, 72);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(28, 13);
            this.label20.TabIndex = 219;
            this.label20.Text = "RFC";
            // 
            // txtRFC
            // 
            this.txtRFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRFC.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtRFC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRFC.Location = new System.Drawing.Point(664, 72);
            this.txtRFC.MaxLength = 13;
            this.txtRFC.Name = "txtRFC";
            this.txtRFC.Size = new System.Drawing.Size(88, 20);
            this.txtRFC.TabIndex = 8;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Cursor = System.Windows.Forms.Cursors.Default;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(776, 72);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(37, 13);
            this.label21.TabIndex = 221;
            this.label21.Text = "CURP";
            // 
            // txtCURP
            // 
            this.txtCURP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCURP.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCURP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCURP.Location = new System.Drawing.Point(824, 72);
            this.txtCURP.MaxLength = 18;
            this.txtCURP.Name = "txtCURP";
            this.txtCURP.Size = new System.Drawing.Size(136, 20);
            this.txtCURP.TabIndex = 9;
            this.txtCURP.Leave += new System.EventHandler(this.txtCURP_Leave);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Cursor = System.Windows.Forms.Cursors.Default;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(8, 104);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(93, 13);
            this.label22.TabIndex = 223;
            this.label22.Text = "Correo electrónico";
            // 
            // txtCorreoElectronico
            // 
            this.txtCorreoElectronico.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCorreoElectronico.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCorreoElectronico.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorreoElectronico.Location = new System.Drawing.Point(104, 104);
            this.txtCorreoElectronico.MaxLength = 50;
            this.txtCorreoElectronico.Name = "txtCorreoElectronico";
            this.txtCorreoElectronico.Size = new System.Drawing.Size(376, 20);
            this.txtCorreoElectronico.TabIndex = 10;
            this.txtCorreoElectronico.Leave += new System.EventHandler(this.txtCorreoElectronico_Leave);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Cursor = System.Windows.Forms.Cursors.Default;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(504, 104);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(60, 13);
            this.label24.TabIndex = 226;
            this.label24.Text = "Teléfono(s)";
            // 
            // txtTelefono01
            // 
            this.txtTelefono01.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTelefono01.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtTelefono01.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefono01.Location = new System.Drawing.Point(600, 104);
            this.txtTelefono01.MaxLength = 10;
            this.txtTelefono01.Name = "txtTelefono01";
            this.txtTelefono01.Size = new System.Drawing.Size(88, 20);
            this.txtTelefono01.TabIndex = 11;
            this.txtTelefono01.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTelefono01_KeyPress);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Cursor = System.Windows.Forms.Cursors.Default;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(8, 432);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(60, 13);
            this.label25.TabIndex = 229;
            this.label25.Text = "Comentario";
            // 
            // txtComentario
            // 
            this.txtComentario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtComentario.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtComentario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComentario.Location = new System.Drawing.Point(104, 432);
            this.txtComentario.MaxLength = 250;
            this.txtComentario.Multiline = true;
            this.txtComentario.Name = "txtComentario";
            this.txtComentario.Size = new System.Drawing.Size(856, 56);
            this.txtComentario.TabIndex = 41;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(728, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 242;
            this.label1.Text = "Apellido materno";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Cursor = System.Windows.Forms.Cursors.Default;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(488, 40);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 13);
            this.label13.TabIndex = 240;
            this.label13.Text = "Apellido paterno";
            // 
            // txtNombre
            // 
            this.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombre.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.Location = new System.Drawing.Point(216, 40);
            this.txtNombre.MaxLength = 100;
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(152, 20);
            this.txtNombre.TabIndex = 2;
            this.txtNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_KeyPress);
            this.txtNombre.Leave += new System.EventHandler(this.txtNombre_Leave);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Cursor = System.Windows.Forms.Cursors.Default;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(160, 40);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 13);
            this.label14.TabIndex = 238;
            this.label14.Text = "Nombre";
            // 
            // txtApellidoPaterno
            // 
            this.txtApellidoPaterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtApellidoPaterno.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtApellidoPaterno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellidoPaterno.Location = new System.Drawing.Point(583, 40);
            this.txtApellidoPaterno.MaxLength = 100;
            this.txtApellidoPaterno.Name = "txtApellidoPaterno";
            this.txtApellidoPaterno.Size = new System.Drawing.Size(136, 20);
            this.txtApellidoPaterno.TabIndex = 4;
            this.txtApellidoPaterno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtApellidoPaterno_KeyPress);
            // 
            // txtDescripcionIdentificacion
            // 
            this.txtDescripcionIdentificacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionIdentificacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionIdentificacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcionIdentificacion.Location = new System.Drawing.Point(160, 136);
            this.txtDescripcionIdentificacion.MaxLength = 250;
            this.txtDescripcionIdentificacion.Name = "txtDescripcionIdentificacion";
            this.txtDescripcionIdentificacion.ReadOnly = true;
            this.txtDescripcionIdentificacion.Size = new System.Drawing.Size(288, 20);
            this.txtDescripcionIdentificacion.TabIndex = 14;
            // 
            // txtCodigoIdentificacion
            // 
            this.txtCodigoIdentificacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoIdentificacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoIdentificacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoIdentificacion.Location = new System.Drawing.Point(104, 136);
            this.txtCodigoIdentificacion.MaxLength = 50;
            this.txtCodigoIdentificacion.Name = "txtCodigoIdentificacion";
            this.txtCodigoIdentificacion.Size = new System.Drawing.Size(48, 20);
            this.txtCodigoIdentificacion.TabIndex = 13;
            this.txtCodigoIdentificacion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigoIdentificacion_KeyPress);
            this.txtCodigoIdentificacion.Leave += new System.EventHandler(this.txtCodigoIdentificacion_Leave);
            // 
            // txtTelefono02
            // 
            this.txtTelefono02.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTelefono02.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtTelefono02.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefono02.Location = new System.Drawing.Point(696, 104);
            this.txtTelefono02.MaxLength = 10;
            this.txtTelefono02.Name = "txtTelefono02";
            this.txtTelefono02.Size = new System.Drawing.Size(88, 20);
            this.txtTelefono02.TabIndex = 12;
            this.txtTelefono02.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTelefono02_KeyPress);
            // 
            // txtCalle
            // 
            this.txtCalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCalle.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCalle.Location = new System.Drawing.Point(368, 200);
            this.txtCalle.MaxLength = 250;
            this.txtCalle.Name = "txtCalle";
            this.txtCalle.Size = new System.Drawing.Size(248, 20);
            this.txtCalle.TabIndex = 22;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Cursor = System.Windows.Forms.Cursors.Default;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(328, 200);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(30, 13);
            this.label38.TabIndex = 317;
            this.label38.Text = "Calle";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Cursor = System.Windows.Forms.Cursors.Default;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(832, 200);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(71, 13);
            this.label35.TabIndex = 310;
            this.label35.Text = "Código postal";
            // 
            // txtApellidoMaterno
            // 
            this.txtApellidoMaterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtApellidoMaterno.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtApellidoMaterno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellidoMaterno.Location = new System.Drawing.Point(824, 40);
            this.txtApellidoMaterno.MaxLength = 100;
            this.txtApellidoMaterno.Name = "txtApellidoMaterno";
            this.txtApellidoMaterno.Size = new System.Drawing.Size(136, 20);
            this.txtApellidoMaterno.TabIndex = 5;
            this.txtApellidoMaterno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtApellidoMaterno_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtApellidoMaternoCotitular);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtApellidoPaternoCotitular);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtNombreCotitular);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(0, 264);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(960, 56);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cotitular";
            // 
            // txtApellidoMaternoCotitular
            // 
            this.txtApellidoMaternoCotitular.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtApellidoMaternoCotitular.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtApellidoMaternoCotitular.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellidoMaternoCotitular.Location = new System.Drawing.Point(728, 24);
            this.txtApellidoMaternoCotitular.MaxLength = 100;
            this.txtApellidoMaternoCotitular.Name = "txtApellidoMaternoCotitular";
            this.txtApellidoMaternoCotitular.Size = new System.Drawing.Size(200, 20);
            this.txtApellidoMaternoCotitular.TabIndex = 33;
            this.txtApellidoMaternoCotitular.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtApellidoMaternoCotitular_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Cursor = System.Windows.Forms.Cursors.Default;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(616, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 13);
            this.label10.TabIndex = 331;
            this.label10.Text = "Apellido materno";
            // 
            // txtApellidoPaternoCotitular
            // 
            this.txtApellidoPaternoCotitular.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtApellidoPaternoCotitular.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtApellidoPaternoCotitular.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellidoPaternoCotitular.Location = new System.Drawing.Point(424, 24);
            this.txtApellidoPaternoCotitular.MaxLength = 100;
            this.txtApellidoPaternoCotitular.Name = "txtApellidoPaternoCotitular";
            this.txtApellidoPaternoCotitular.Size = new System.Drawing.Size(176, 20);
            this.txtApellidoPaternoCotitular.TabIndex = 32;
            this.txtApellidoPaternoCotitular.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtApellidoPaternoCotitular_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Cursor = System.Windows.Forms.Cursors.Default;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(320, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 331;
            this.label8.Text = "Apellido paterno";
            // 
            // txtNombreCotitular
            // 
            this.txtNombreCotitular.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombreCotitular.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNombreCotitular.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreCotitular.Location = new System.Drawing.Point(104, 24);
            this.txtNombreCotitular.MaxLength = 100;
            this.txtNombreCotitular.Name = "txtNombreCotitular";
            this.txtNombreCotitular.Size = new System.Drawing.Size(192, 20);
            this.txtNombreCotitular.TabIndex = 31;
            this.txtNombreCotitular.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombreCotitular_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 331;
            this.label4.Text = "Nombre";
            this.label4.UseWaitCursor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtApellidoMaternoBeneficario);
            this.groupBox2.Controls.Add(this.txtApellidoPaternoBeneficario);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.txtNombreBeneficario);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Cursor = System.Windows.Forms.Cursors.Default;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(0, 328);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(960, 56);
            this.groupBox2.TabIndex = 34;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Beneficario";
            // 
            // txtApellidoMaternoBeneficario
            // 
            this.txtApellidoMaternoBeneficario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtApellidoMaternoBeneficario.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtApellidoMaternoBeneficario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellidoMaternoBeneficario.Location = new System.Drawing.Point(728, 24);
            this.txtApellidoMaternoBeneficario.MaxLength = 100;
            this.txtApellidoMaternoBeneficario.Name = "txtApellidoMaternoBeneficario";
            this.txtApellidoMaternoBeneficario.Size = new System.Drawing.Size(200, 20);
            this.txtApellidoMaternoBeneficario.TabIndex = 37;
            this.txtApellidoMaternoBeneficario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtApellidoMaternoBeneficario_KeyPress);
            // 
            // txtApellidoPaternoBeneficario
            // 
            this.txtApellidoPaternoBeneficario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtApellidoPaternoBeneficario.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtApellidoPaternoBeneficario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellidoPaternoBeneficario.Location = new System.Drawing.Point(424, 24);
            this.txtApellidoPaternoBeneficario.MaxLength = 100;
            this.txtApellidoPaternoBeneficario.Name = "txtApellidoPaternoBeneficario";
            this.txtApellidoPaternoBeneficario.Size = new System.Drawing.Size(176, 20);
            this.txtApellidoPaternoBeneficario.TabIndex = 36;
            this.txtApellidoPaternoBeneficario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtApellidoPaternoBeneficario_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Cursor = System.Windows.Forms.Cursors.Default;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(613, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 13);
            this.label11.TabIndex = 333;
            this.label11.Text = "Apellido materno";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(16, 23);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(44, 13);
            this.label23.TabIndex = 335;
            this.label23.Text = "Nombre";
            this.label23.UseWaitCursor = true;
            // 
            // txtNombreBeneficario
            // 
            this.txtNombreBeneficario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombreBeneficario.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNombreBeneficario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreBeneficario.Location = new System.Drawing.Point(104, 24);
            this.txtNombreBeneficario.MaxLength = 100;
            this.txtNombreBeneficario.Name = "txtNombreBeneficario";
            this.txtNombreBeneficario.Size = new System.Drawing.Size(192, 20);
            this.txtNombreBeneficario.TabIndex = 35;
            this.txtNombreBeneficario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombreBeneficario_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(320, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 13);
            this.label12.TabIndex = 334;
            this.label12.Text = "Apellido paterno";
            // 
            // btnFotografia
            // 
            this.btnFotografia.Location = new System.Drawing.Point(208, 392);
            this.btnFotografia.Name = "btnFotografia";
            this.btnFotografia.Size = new System.Drawing.Size(96, 21);
            this.btnFotografia.TabIndex = 39;
            this.btnFotografia.Text = "Fotografía";
            this.btnFotografia.UseVisualStyleBackColor = true;
            this.btnFotografia.Click += new System.EventHandler(this.btnFotografia_Click);
            // 
            // btnHuella
            // 
            this.btnHuella.Location = new System.Drawing.Point(104, 392);
            this.btnHuella.Name = "btnHuella";
            this.btnHuella.Size = new System.Drawing.Size(96, 21);
            this.btnHuella.TabIndex = 38;
            this.btnHuella.Text = "Huella";
            this.btnHuella.UseVisualStyleBackColor = true;
            this.btnHuella.Click += new System.EventHandler(this.btnHuella_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(624, 200);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 336;
            this.label2.Text = "No. Ext. y Int";
            // 
            // txtNoExt
            // 
            this.txtNoExt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNoExt.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNoExt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoExt.Location = new System.Drawing.Point(704, 200);
            this.txtNoExt.MaxLength = 15;
            this.txtNoExt.Name = "txtNoExt";
            this.txtNoExt.Size = new System.Drawing.Size(50, 20);
            this.txtNoExt.TabIndex = 23;
            this.txtNoExt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNoExt_KeyPress);
            // 
            // txtNoInt
            // 
            this.txtNoInt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNoInt.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNoInt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoInt.Location = new System.Drawing.Point(768, 200);
            this.txtNoInt.MaxLength = 15;
            this.txtNoInt.Name = "txtNoInt";
            this.txtNoInt.Size = new System.Drawing.Size(50, 20);
            this.txtNoInt.TabIndex = 24;
            this.txtNoInt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNoInt_KeyPress);
            // 
            // txtIdOcupacion
            // 
            this.txtIdOcupacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtIdOcupacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdOcupacion.Location = new System.Drawing.Point(384, 168);
            this.txtIdOcupacion.MaxLength = 3;
            this.txtIdOcupacion.Name = "txtIdOcupacion";
            this.txtIdOcupacion.Size = new System.Drawing.Size(44, 20);
            this.txtIdOcupacion.TabIndex = 514;
            this.txtIdOcupacion.Visible = false;
            // 
            // txtAsentamientoId
            // 
            this.txtAsentamientoId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAsentamientoId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsentamientoId.Location = new System.Drawing.Point(240, 200);
            this.txtAsentamientoId.MaxLength = 3;
            this.txtAsentamientoId.Name = "txtAsentamientoId";
            this.txtAsentamientoId.Size = new System.Drawing.Size(44, 20);
            this.txtAsentamientoId.TabIndex = 524;
            this.txtAsentamientoId.Visible = false;
            // 
            // txtDescripcionAsentamiento
            // 
            this.txtDescripcionAsentamiento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionAsentamiento.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionAsentamiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcionAsentamiento.Location = new System.Drawing.Point(104, 200);
            this.txtDescripcionAsentamiento.MaxLength = 250;
            this.txtDescripcionAsentamiento.Name = "txtDescripcionAsentamiento";
            this.txtDescripcionAsentamiento.Size = new System.Drawing.Size(176, 20);
            this.txtDescripcionAsentamiento.TabIndex = 20;
            this.txtDescripcionAsentamiento.Leave += new System.EventHandler(this.txtDescripcionAsentamiento_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 200);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 523;
            this.label7.Text = "Asentamiento";
            // 
            // txtIdIdentificacion
            // 
            this.txtIdIdentificacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtIdIdentificacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdIdentificacion.Location = new System.Drawing.Point(400, 136);
            this.txtIdIdentificacion.MaxLength = 3;
            this.txtIdIdentificacion.Name = "txtIdIdentificacion";
            this.txtIdIdentificacion.Size = new System.Drawing.Size(44, 20);
            this.txtIdIdentificacion.TabIndex = 526;
            this.txtIdIdentificacion.Visible = false;
            // 
            // txtCodigoPostal
            // 
            this.txtCodigoPostal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoPostal.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoPostal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoPostal.Location = new System.Drawing.Point(912, 200);
            this.txtCodigoPostal.MaxLength = 5;
            this.txtCodigoPostal.Name = "txtCodigoPostal";
            this.txtCodigoPostal.ReadOnly = true;
            this.txtCodigoPostal.Size = new System.Drawing.Size(48, 20);
            this.txtCodigoPostal.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 232);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 528;
            this.label6.Text = "Municipio";
            // 
            // txtCodigoMunicipio
            // 
            this.txtCodigoMunicipio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoMunicipio.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoMunicipio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoMunicipio.Location = new System.Drawing.Point(104, 232);
            this.txtCodigoMunicipio.MaxLength = 50;
            this.txtCodigoMunicipio.Name = "txtCodigoMunicipio";
            this.txtCodigoMunicipio.ReadOnly = true;
            this.txtCodigoMunicipio.Size = new System.Drawing.Size(48, 20);
            this.txtCodigoMunicipio.TabIndex = 26;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(512, 232);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 530;
            this.label5.Text = "Estado";
            // 
            // txtCodigoEstado
            // 
            this.txtCodigoEstado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoEstado.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoEstado.Location = new System.Drawing.Point(568, 232);
            this.txtCodigoEstado.MaxLength = 50;
            this.txtCodigoEstado.Name = "txtCodigoEstado";
            this.txtCodigoEstado.ReadOnly = true;
            this.txtCodigoEstado.Size = new System.Drawing.Size(48, 20);
            this.txtCodigoEstado.TabIndex = 28;
            // 
            // txtDescripcionEstado
            // 
            this.txtDescripcionEstado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionEstado.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcionEstado.Location = new System.Drawing.Point(624, 232);
            this.txtDescripcionEstado.MaxLength = 250;
            this.txtDescripcionEstado.Name = "txtDescripcionEstado";
            this.txtDescripcionEstado.ReadOnly = true;
            this.txtDescripcionEstado.Size = new System.Drawing.Size(336, 20);
            this.txtDescripcionEstado.TabIndex = 29;
            // 
            // txtDescripcionMunicipio
            // 
            this.txtDescripcionMunicipio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionMunicipio.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionMunicipio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcionMunicipio.Location = new System.Drawing.Point(160, 232);
            this.txtDescripcionMunicipio.MaxLength = 250;
            this.txtDescripcionMunicipio.Name = "txtDescripcionMunicipio";
            this.txtDescripcionMunicipio.ReadOnly = true;
            this.txtDescripcionMunicipio.Size = new System.Drawing.Size(328, 20);
            this.txtDescripcionMunicipio.TabIndex = 27;
            // 
            // label15
            // 
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(-8, 504);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(974, 2);
            this.label15.TabIndex = 536;
            // 
            // tsAcciones
            // 
            this.tsAcciones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbNuevo,
            this.tsbGuardar,
            this.tsbEliminar,
            this.tsbBuscar,
            this.btnUndo,
            this.toolStripSeparator2,
            this.btnPrimeroPag,
            this.btnAtras,
            this.btnAdelante,
            this.btnUlitmoPag,
            this.toolStripSeparator1,
            this.toolStripButton3,
            this.btnCierra});
            this.tsAcciones.Location = new System.Drawing.Point(0, 0);
            this.tsAcciones.Name = "tsAcciones";
            this.tsAcciones.Size = new System.Drawing.Size(980, 25);
            this.tsAcciones.TabIndex = 537;
            this.tsAcciones.Text = "toolStrip2";
            this.tsAcciones.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip2_ItemClicked);
            // 
            // tsbNuevo
            // 
            this.tsbNuevo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbNuevo.Image = global::EmpenosEvora.Properties.Resources.AddFile_16x;
            this.tsbNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbNuevo.Name = "tsbNuevo";
            this.tsbNuevo.Size = new System.Drawing.Size(23, 22);
            this.tsbNuevo.Text = "Nuevo";
            this.tsbNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical90;
            this.tsbNuevo.Click += new System.EventHandler(this.TsbNuevo_Click);
            // 
            // tsbGuardar
            // 
            this.tsbGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbGuardar.Image = global::EmpenosEvora.Properties.Resources.Save_16x;
            this.tsbGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGuardar.Name = "tsbGuardar";
            this.tsbGuardar.Size = new System.Drawing.Size(23, 22);
            this.tsbGuardar.Tag = "";
            this.tsbGuardar.Text = "Guardar";
            this.tsbGuardar.Click += new System.EventHandler(this.TsbGuardar_Click);
            // 
            // tsbBuscar
            // 
            this.tsbBuscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBuscar.Image = global::EmpenosEvora.Properties.Resources.VBSearch_16x;
            this.tsbBuscar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBuscar.Name = "tsbBuscar";
            this.tsbBuscar.Size = new System.Drawing.Size(23, 22);
            this.tsbBuscar.Text = "Buscar";
            this.tsbBuscar.Click += new System.EventHandler(this.TsbBuscar_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUndo.Image = global::EmpenosEvora.Properties.Resources.Undo_16x;
            this.btnUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(23, 22);
            this.btnUndo.Text = "Deshacer";
            this.btnUndo.Click += new System.EventHandler(this.BtnUndo_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrimeroPag
            // 
            this.btnPrimeroPag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrimeroPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeftGroup_16xSM;
            this.btnPrimeroPag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrimeroPag.Name = "btnPrimeroPag";
            this.btnPrimeroPag.Size = new System.Drawing.Size(23, 22);
            this.btnPrimeroPag.Text = "Primero";
            this.btnPrimeroPag.Click += new System.EventHandler(this.BtnPrimeroPag_Click);
            // 
            // btnAtras
            // 
            this.btnAtras.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAtras.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeft_16x;
            this.btnAtras.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(23, 22);
            this.btnAtras.Text = "Atras";
            this.btnAtras.Click += new System.EventHandler(this.BtnAtras_Click);
            // 
            // btnAdelante
            // 
            this.btnAdelante.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdelante.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronRight_16x;
            this.btnAdelante.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdelante.Name = "btnAdelante";
            this.btnAdelante.Size = new System.Drawing.Size(23, 22);
            this.btnAdelante.Text = "Siguente";
            this.btnAdelante.Click += new System.EventHandler(this.BtnAdelante_Click);
            // 
            // btnUlitmoPag
            // 
            this.btnUlitmoPag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUlitmoPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronRightGroup_16x;
            this.btnUlitmoPag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUlitmoPag.Name = "btnUlitmoPag";
            this.btnUlitmoPag.Size = new System.Drawing.Size(23, 22);
            this.btnUlitmoPag.Text = "Ultimo";
            this.btnUlitmoPag.Click += new System.EventHandler(this.BtnUlitmoPag_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::EmpenosEvora.Properties.Resources.Print_16x;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "Imprimir";
            this.toolStripButton3.ToolTipText = "Imprimir";
            // 
            // btnCierra
            // 
            this.btnCierra.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnCierra.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCierra.Name = "btnCierra";
            this.btnCierra.Size = new System.Drawing.Size(23, 22);
            this.btnCierra.Text = "Salir";
            this.btnCierra.Click += new System.EventHandler(this.BtnCierra_Click);
            // 
            // txtClienteId
            // 
            this.txtClienteId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtClienteId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClienteId.Location = new System.Drawing.Point(288, 40);
            this.txtClienteId.MaxLength = 3;
            this.txtClienteId.Name = "txtClienteId";
            this.txtClienteId.Size = new System.Drawing.Size(44, 20);
            this.txtClienteId.TabIndex = 539;
            this.txtClienteId.Visible = false;
            // 
            // txtIdentificacionValidar
            // 
            this.txtIdentificacionValidar.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtIdentificacionValidar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdentificacionValidar.Location = new System.Drawing.Point(320, 136);
            this.txtIdentificacionValidar.MaxLength = 3;
            this.txtIdentificacionValidar.Name = "txtIdentificacionValidar";
            this.txtIdentificacionValidar.Size = new System.Drawing.Size(44, 20);
            this.txtIdentificacionValidar.TabIndex = 540;
            this.txtIdentificacionValidar.Visible = false;
            // 
            // btnIdentificacion
            // 
            this.btnIdentificacion.Location = new System.Drawing.Point(312, 392);
            this.btnIdentificacion.Name = "btnIdentificacion";
            this.btnIdentificacion.Size = new System.Drawing.Size(96, 21);
            this.btnIdentificacion.TabIndex = 40;
            this.btnIdentificacion.Text = "Identificación";
            this.btnIdentificacion.UseVisualStyleBackColor = true;
            this.btnIdentificacion.Click += new System.EventHandler(this.btnIdentificacion_Click);
            // 
            // cmbEstatus
            // 
            this.cmbEstatus.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmbEstatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEstatus.FormattingEnabled = true;
            this.cmbEstatus.Items.AddRange(new object[] {
            "ACTIVO",
            "NO DESEADO"});
            this.cmbEstatus.Location = new System.Drawing.Point(104, 72);
            this.cmbEstatus.Name = "cmbEstatus";
            this.cmbEstatus.Size = new System.Drawing.Size(88, 21);
            this.cmbEstatus.TabIndex = 541;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 542;
            this.label3.Text = "Estatus";
            // 
            // btnVerificarHuella
            // 
            this.btnVerificarHuella.Location = new System.Drawing.Point(416, 40);
            this.btnVerificarHuella.Name = "btnVerificarHuella";
            this.btnVerificarHuella.Size = new System.Drawing.Size(64, 21);
            this.btnVerificarHuella.TabIndex = 543;
            this.btnVerificarHuella.Text = "Verificar ";
            this.btnVerificarHuella.UseVisualStyleBackColor = true;
            this.btnVerificarHuella.Click += new System.EventHandler(this.btnVerificarHuella_Click);
            // 
            // txtCurpAntri
            // 
            this.txtCurpAntri.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCurpAntri.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurpAntri.Location = new System.Drawing.Point(896, 72);
            this.txtCurpAntri.MaxLength = 3;
            this.txtCurpAntri.Name = "txtCurpAntri";
            this.txtCurpAntri.Size = new System.Drawing.Size(44, 20);
            this.txtCurpAntri.TabIndex = 544;
            this.txtCurpAntri.Visible = false;
            // 
            // btnBusquedaIdentif
            // 
            this.btnBusquedaIdentif.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBusquedaIdentif.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusquedaIdentif.Location = new System.Drawing.Point(456, 136);
            this.btnBusquedaIdentif.Name = "btnBusquedaIdentif";
            this.btnBusquedaIdentif.Size = new System.Drawing.Size(32, 21);
            this.btnBusquedaIdentif.TabIndex = 15;
            this.btnBusquedaIdentif.UseVisualStyleBackColor = true;
            this.btnBusquedaIdentif.Click += new System.EventHandler(this.btnBusquedaIdentif_Click);
            // 
            // btnFooterCierra
            // 
            this.btnFooterCierra.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnFooterCierra.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterCierra.Location = new System.Drawing.Point(880, 520);
            this.btnFooterCierra.Name = "btnFooterCierra";
            this.btnFooterCierra.Size = new System.Drawing.Size(75, 40);
            this.btnFooterCierra.TabIndex = 43;
            this.btnFooterCierra.Text = "Salir";
            this.btnFooterCierra.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterCierra.UseVisualStyleBackColor = true;
            this.btnFooterCierra.Click += new System.EventHandler(this.BtnFooterCierra_Click);
            // 
            // btnFooterGuardar
            // 
            this.btnFooterGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterGuardar.Image = global::EmpenosEvora.Properties.Resources.Save_16x;
            this.btnFooterGuardar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterGuardar.Location = new System.Drawing.Point(792, 520);
            this.btnFooterGuardar.Name = "btnFooterGuardar";
            this.btnFooterGuardar.Size = new System.Drawing.Size(75, 40);
            this.btnFooterGuardar.TabIndex = 42;
            this.btnFooterGuardar.Text = "Guardar";
            this.btnFooterGuardar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterGuardar.UseVisualStyleBackColor = true;
            this.btnFooterGuardar.Click += new System.EventHandler(this.BtnFooterGuardar_Click);
            // 
            // btnBusquedaAsentamiento
            // 
            this.btnBusquedaAsentamiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBusquedaAsentamiento.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusquedaAsentamiento.Location = new System.Drawing.Point(288, 200);
            this.btnBusquedaAsentamiento.Name = "btnBusquedaAsentamiento";
            this.btnBusquedaAsentamiento.Size = new System.Drawing.Size(32, 23);
            this.btnBusquedaAsentamiento.TabIndex = 21;
            this.btnBusquedaAsentamiento.UseVisualStyleBackColor = true;
            this.btnBusquedaAsentamiento.Click += new System.EventHandler(this.BtnBusquedaAsentamiento_Click);
            // 
            // btnBusqueda
            // 
            this.btnBusqueda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBusqueda.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusqueda.Location = new System.Drawing.Point(376, 40);
            this.btnBusqueda.Name = "btnBusqueda";
            this.btnBusqueda.Size = new System.Drawing.Size(32, 21);
            this.btnBusqueda.TabIndex = 3;
            this.btnBusqueda.UseVisualStyleBackColor = true;
            this.btnBusqueda.Click += new System.EventHandler(this.BtnBusqueda_Click);
            // 
            // btnBusquedaOcupacion
            // 
            this.btnBusquedaOcupacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBusquedaOcupacion.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusquedaOcupacion.Location = new System.Drawing.Point(928, 168);
            this.btnBusquedaOcupacion.Name = "btnBusquedaOcupacion";
            this.btnBusquedaOcupacion.Size = new System.Drawing.Size(32, 21);
            this.btnBusquedaOcupacion.TabIndex = 19;
            this.btnBusquedaOcupacion.UseVisualStyleBackColor = true;
            this.btnBusquedaOcupacion.Click += new System.EventHandler(this.btnBusquedaOcupacion_Click);
            // 
            // txtNumeroIdentificacion
            // 
            this.txtNumeroIdentificacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNumeroIdentificacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNumeroIdentificacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtNumeroIdentificacion.Location = new System.Drawing.Point(600, 136);
            this.txtNumeroIdentificacion.MaxLength = 50;
            this.txtNumeroIdentificacion.Name = "txtNumeroIdentificacion";
            this.txtNumeroIdentificacion.Size = new System.Drawing.Size(360, 20);
            this.txtNumeroIdentificacion.TabIndex = 16;
            // 
            // lblSerieIdentificacion
            // 
            this.lblSerieIdentificacion.AutoSize = true;
            this.lblSerieIdentificacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblSerieIdentificacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblSerieIdentificacion.Location = new System.Drawing.Point(504, 136);
            this.lblSerieIdentificacion.Name = "lblSerieIdentificacion";
            this.lblSerieIdentificacion.Size = new System.Drawing.Size(90, 13);
            this.lblSerieIdentificacion.TabIndex = 604;
            this.lblSerieIdentificacion.Text = "No. Identificación";
            // 
            // tsbEliminar
            // 
            this.tsbEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbEliminar.Image = global::EmpenosEvora.Properties.Resources.Cancel_16x;
            this.tsbEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEliminar.Name = "tsbEliminar";
            this.tsbEliminar.Size = new System.Drawing.Size(23, 22);
            this.tsbEliminar.Text = "Desactivar";
            this.tsbEliminar.ToolTipText = "Desactivar";
            this.tsbEliminar.Visible = false;
            // 
            // frmClientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(980, 573);
            this.Controls.Add(this.txtNumeroIdentificacion);
            this.Controls.Add(this.lblSerieIdentificacion);
            this.Controls.Add(this.txtCurpAntri);
            this.Controls.Add(this.btnVerificarHuella);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbEstatus);
            this.Controls.Add(this.btnIdentificacion);
            this.Controls.Add(this.txtIdentificacionValidar);
            this.Controls.Add(this.txtClienteId);
            this.Controls.Add(this.btnBusquedaIdentif);
            this.Controls.Add(this.tsAcciones);
            this.Controls.Add(this.btnFooterCierra);
            this.Controls.Add(this.btnFooterGuardar);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtCodigoMunicipio);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCodigoEstado);
            this.Controls.Add(this.txtDescripcionEstado);
            this.Controls.Add(this.txtDescripcionMunicipio);
            this.Controls.Add(this.txtCodigoPostal);
            this.Controls.Add(this.txtIdIdentificacion);
            this.Controls.Add(this.txtAsentamientoId);
            this.Controls.Add(this.btnBusquedaAsentamiento);
            this.Controls.Add(this.txtDescripcionAsentamiento);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnBusqueda);
            this.Controls.Add(this.txtIdOcupacion);
            this.Controls.Add(this.btnBusquedaOcupacion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNoExt);
            this.Controls.Add(this.txtNoInt);
            this.Controls.Add(this.btnHuella);
            this.Controls.Add(this.btnFotografia);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtApellidoMaterno);
            this.Controls.Add(this.txtCalle);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.txtTelefono02);
            this.Controls.Add(this.txtCodigoIdentificacion);
            this.Controls.Add(this.txtDescripcionIdentificacion);
            this.Controls.Add(this.txtApellidoPaterno);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.txtComentario);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.txtTelefono01);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.txtCorreoElectronico);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.txtCURP);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.txtRFC);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtCodigoOcupacion);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtDescripcionOcupacion);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cmbGenero);
            this.Controls.Add(this.dtpFechaNacimiento);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.txtCodigo);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmClientes";
            this.Text = "Clientes";
            this.Load += new System.EventHandler(this.frmClientes_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tsAcciones.ResumeLayout(false);
            this.tsAcciones.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.DateTimePicker dtpFechaNacimiento;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cmbGenero;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtCodigoOcupacion;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtDescripcionOcupacion;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtRFC;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtCURP;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtCorreoElectronico;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtTelefono01;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtComentario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.TextBox txtApellidoPaterno;
        private System.Windows.Forms.TextBox txtDescripcionIdentificacion;
        private System.Windows.Forms.TextBox txtCodigoIdentificacion;
        private System.Windows.Forms.TextBox txtTelefono02;
        private System.Windows.Forms.TextBox txtCalle;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtApellidoMaterno;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtApellidoPaternoCotitular;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNombreCotitular;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtApellidoMaternoCotitular;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtApellidoMaternoBeneficario;
        private System.Windows.Forms.TextBox txtApellidoPaternoBeneficario;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtNombreBeneficario;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnFotografia;
        private System.Windows.Forms.Button btnHuella;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNoExt;
        private System.Windows.Forms.TextBox txtNoInt;
        private System.Windows.Forms.TextBox txtIdOcupacion;
        private System.Windows.Forms.Button btnBusquedaOcupacion;
        private System.Windows.Forms.Button btnBusqueda;
        private System.Windows.Forms.TextBox txtAsentamientoId;
        private System.Windows.Forms.Button btnBusquedaAsentamiento;
        private System.Windows.Forms.TextBox txtDescripcionAsentamiento;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtIdIdentificacion;
        //private System.Windows.Forms.Button btnBusquedaIdentificacion;
        private System.Windows.Forms.TextBox txtCodigoPostal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCodigoMunicipio;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCodigoEstado;
        private System.Windows.Forms.TextBox txtDescripcionEstado;
        private System.Windows.Forms.TextBox txtDescripcionMunicipio;
        private System.Windows.Forms.Button btnFooterCierra;
        private System.Windows.Forms.Button btnFooterGuardar;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ToolStrip tsAcciones;
        private System.Windows.Forms.ToolStripButton tsbNuevo;
        private System.Windows.Forms.ToolStripButton tsbGuardar;
        private System.Windows.Forms.ToolStripButton tsbBuscar;
        private System.Windows.Forms.ToolStripButton btnUndo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnPrimeroPag;
        private System.Windows.Forms.ToolStripButton btnAtras;
        private System.Windows.Forms.ToolStripButton btnAdelante;
        private System.Windows.Forms.ToolStripButton btnUlitmoPag;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton btnCierra;
        private System.Windows.Forms.Button btnBusquedaIdentif;
        private System.Windows.Forms.TextBox txtClienteId;
        private System.Windows.Forms.TextBox txtIdentificacionValidar;
        private System.Windows.Forms.Button btnIdentificacion;
        private System.Windows.Forms.ComboBox cmbEstatus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnVerificarHuella;
        private System.Windows.Forms.TextBox txtCurpAntri;
        private System.Windows.Forms.TextBox txtNumeroIdentificacion;
        private System.Windows.Forms.Label lblSerieIdentificacion;
        private System.Windows.Forms.ToolStripButton tsbEliminar;
    }
}