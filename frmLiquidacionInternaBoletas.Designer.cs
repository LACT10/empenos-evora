﻿namespace EmpenosEvora
{
    partial class frmLiquidacionInternaBoletas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label12 = new System.Windows.Forms.Label();
            this.txtAcumSubtotal = new System.Windows.Forms.TextBox();
            this.txtAcumInt = new System.Windows.Forms.TextBox();
            this.txtAcumCapial = new System.Windows.Forms.TextBox();
            this.dgvDetalles = new System.Windows.Forms.DataGridView();
            this.cliente_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.porcentajeTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prendaTipoId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cobroTipoId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.porcentajeCobroTipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.boleta_empeno_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Boleta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Capital = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IntAlmMan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Subtotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rbtCInteresesFechaLiquidacion = new System.Windows.Forms.RadioButton();
            this.rbtANumeroMesesDias = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.rbtAPorcentajeIntereses = new System.Windows.Forms.RadioButton();
            this.rbtCInteresesFechaVencimiento = new System.Windows.Forms.RadioButton();
            this.txtPorcentaje = new System.Windows.Forms.TextBox();
            this.chkCliente = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPlazo = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.txtHora = new System.Windows.Forms.TextBox();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBoleta = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDescripcionCaja = new System.Windows.Forms.TextBox();
            this.txtCodigoCaja = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNombreEmpleado = new System.Windows.Forms.TextBox();
            this.tsAcciones = new System.Windows.Forms.ToolStrip();
            this.tsbNuevo = new System.Windows.Forms.ToolStripButton();
            this.tsbGuardar = new System.Windows.Forms.ToolStripButton();
            this.tsbEliminar = new System.Windows.Forms.ToolStripButton();
            this.tsbBuscar = new System.Windows.Forms.ToolStripButton();
            this.btnUndo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrimeroPag = new System.Windows.Forms.ToolStripButton();
            this.btnAtras = new System.Windows.Forms.ToolStripButton();
            this.btnAdelante = new System.Windows.Forms.ToolStripButton();
            this.btnUlitmoPag = new System.Windows.Forms.ToolStripButton();
            this.tsbImprimir = new System.Windows.Forms.ToolStripButton();
            this.btnCierra = new System.Windows.Forms.ToolStripButton();
            this.btnFooterCierra = new System.Windows.Forms.Button();
            this.btnFooterGuardar = new System.Windows.Forms.Button();
            this.txtMeses = new System.Windows.Forms.TextBox();
            this.lblMeses = new System.Windows.Forms.Label();
            this.lblDias = new System.Windows.Forms.Label();
            this.txtDias = new System.Windows.Forms.TextBox();
            this.lblPorcentaje = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlazo)).BeginInit();
            this.tsAcciones.SuspendLayout();
            this.SuspendLayout();
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label12.Location = new System.Drawing.Point(8, 375);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 13);
            this.label12.TabIndex = 421;
            this.label12.Text = "Número de boleta ";
            // 
            // txtAcumSubtotal
            // 
            this.txtAcumSubtotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAcumSubtotal.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAcumSubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtAcumSubtotal.Location = new System.Drawing.Point(752, 376);
            this.txtAcumSubtotal.MaxLength = 5;
            this.txtAcumSubtotal.Name = "txtAcumSubtotal";
            this.txtAcumSubtotal.ReadOnly = true;
            this.txtAcumSubtotal.Size = new System.Drawing.Size(120, 20);
            this.txtAcumSubtotal.TabIndex = 19;
            this.txtAcumSubtotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtAcumInt
            // 
            this.txtAcumInt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAcumInt.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAcumInt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtAcumInt.Location = new System.Drawing.Point(632, 376);
            this.txtAcumInt.MaxLength = 5;
            this.txtAcumInt.Name = "txtAcumInt";
            this.txtAcumInt.ReadOnly = true;
            this.txtAcumInt.Size = new System.Drawing.Size(120, 20);
            this.txtAcumInt.TabIndex = 18;
            this.txtAcumInt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtAcumCapial
            // 
            this.txtAcumCapial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAcumCapial.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAcumCapial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtAcumCapial.Location = new System.Drawing.Point(520, 376);
            this.txtAcumCapial.MaxLength = 5;
            this.txtAcumCapial.Name = "txtAcumCapial";
            this.txtAcumCapial.ReadOnly = true;
            this.txtAcumCapial.Size = new System.Drawing.Size(112, 20);
            this.txtAcumCapial.TabIndex = 17;
            this.txtAcumCapial.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // dgvDetalles
            // 
            this.dgvDetalles.AllowUserToAddRows = false;
            this.dgvDetalles.AllowUserToDeleteRows = false;
            this.dgvDetalles.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDetalles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetalles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cliente_id,
            this.porcentajeTotal,
            this.prendaTipoId,
            this.cobroTipoId,
            this.porcentajeCobroTipo,
            this.boleta_empeno_id,
            this.Boleta,
            this.Cliente,
            this.Fecha,
            this.Vence,
            this.Capital,
            this.IntAlmMan,
            this.Subtotal});
            this.dgvDetalles.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgvDetalles.Location = new System.Drawing.Point(8, 213);
            this.dgvDetalles.Name = "dgvDetalles";
            this.dgvDetalles.Size = new System.Drawing.Size(866, 150);
            this.dgvDetalles.TabIndex = 15;
            this.dgvDetalles.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvDetalles_KeyDown);
            // 
            // cliente_id
            // 
            this.cliente_id.HeaderText = "cliente_id";
            this.cliente_id.Name = "cliente_id";
            this.cliente_id.Visible = false;
            // 
            // porcentajeTotal
            // 
            this.porcentajeTotal.HeaderText = "porcentajeTotal";
            this.porcentajeTotal.Name = "porcentajeTotal";
            this.porcentajeTotal.Visible = false;
            // 
            // prendaTipoId
            // 
            this.prendaTipoId.HeaderText = "prendaTipoId";
            this.prendaTipoId.Name = "prendaTipoId";
            this.prendaTipoId.Visible = false;
            // 
            // cobroTipoId
            // 
            this.cobroTipoId.HeaderText = "cobroTipoId";
            this.cobroTipoId.Name = "cobroTipoId";
            this.cobroTipoId.Visible = false;
            // 
            // porcentajeCobroTipo
            // 
            this.porcentajeCobroTipo.HeaderText = "porcentajeCobroTipo";
            this.porcentajeCobroTipo.Name = "porcentajeCobroTipo";
            this.porcentajeCobroTipo.Visible = false;
            // 
            // boleta_empeno_id
            // 
            this.boleta_empeno_id.HeaderText = "boleta_empeno_id";
            this.boleta_empeno_id.Name = "boleta_empeno_id";
            this.boleta_empeno_id.Visible = false;
            // 
            // Boleta
            // 
            this.Boleta.HeaderText = "Boleta";
            this.Boleta.Name = "Boleta";
            // 
            // Cliente
            // 
            this.Cliente.HeaderText = "Cliente";
            this.Cliente.Name = "Cliente";
            // 
            // Fecha
            // 
            this.Fecha.HeaderText = "Fecha";
            this.Fecha.Name = "Fecha";
            // 
            // Vence
            // 
            this.Vence.HeaderText = "Vence";
            this.Vence.Name = "Vence";
            // 
            // Capital
            // 
            this.Capital.HeaderText = "Capital";
            this.Capital.Name = "Capital";
            // 
            // IntAlmMan
            // 
            this.IntAlmMan.HeaderText = "Int + Alm + Man ";
            this.IntAlmMan.Name = "IntAlmMan";
            // 
            // Subtotal
            // 
            this.Subtotal.HeaderText = "Subtotal";
            this.Subtotal.Name = "Subtotal";
            // 
            // rbtCInteresesFechaLiquidacion
            // 
            this.rbtCInteresesFechaLiquidacion.AutoSize = true;
            this.rbtCInteresesFechaLiquidacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbtCInteresesFechaLiquidacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.rbtCInteresesFechaLiquidacion.Location = new System.Drawing.Point(8, 153);
            this.rbtCInteresesFechaLiquidacion.Name = "rbtCInteresesFechaLiquidacion";
            this.rbtCInteresesFechaLiquidacion.Size = new System.Drawing.Size(404, 17);
            this.rbtCInteresesFechaLiquidacion.TabIndex = 8;
            this.rbtCInteresesFechaLiquidacion.TabStop = true;
            this.rbtCInteresesFechaLiquidacion.Text = "Calcular los intereses desde el Empeño hasta la Fecha de la Liquidación Interna.";
            this.rbtCInteresesFechaLiquidacion.UseVisualStyleBackColor = true;
            // 
            // rbtANumeroMesesDias
            // 
            this.rbtANumeroMesesDias.AutoSize = true;
            this.rbtANumeroMesesDias.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbtANumeroMesesDias.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.rbtANumeroMesesDias.Location = new System.Drawing.Point(8, 130);
            this.rbtANumeroMesesDias.Name = "rbtANumeroMesesDias";
            this.rbtANumeroMesesDias.Size = new System.Drawing.Size(386, 17);
            this.rbtANumeroMesesDias.TabIndex = 7;
            this.rbtANumeroMesesDias.TabStop = true;
            this.rbtANumeroMesesDias.Text = "Aplicar el número de Meses y Días especificados a cada una de las Boletas.";
            this.rbtANumeroMesesDias.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label2.Location = new System.Drawing.Point(8, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(148, 13);
            this.label2.TabIndex = 396;
            this.label2.Text = "Forma de cálculo de interesas";
            // 
            // rbtAPorcentajeIntereses
            // 
            this.rbtAPorcentajeIntereses.AutoSize = true;
            this.rbtAPorcentajeIntereses.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbtAPorcentajeIntereses.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.rbtAPorcentajeIntereses.Location = new System.Drawing.Point(8, 107);
            this.rbtAPorcentajeIntereses.Name = "rbtAPorcentajeIntereses";
            this.rbtAPorcentajeIntereses.Size = new System.Drawing.Size(310, 17);
            this.rbtAPorcentajeIntereses.TabIndex = 6;
            this.rbtAPorcentajeIntereses.TabStop = true;
            this.rbtAPorcentajeIntereses.Text = "Aplicar un porcentaje de intereses a cada una de las boletas";
            this.rbtAPorcentajeIntereses.UseVisualStyleBackColor = true;
            // 
            // rbtCInteresesFechaVencimiento
            // 
            this.rbtCInteresesFechaVencimiento.AutoSize = true;
            this.rbtCInteresesFechaVencimiento.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbtCInteresesFechaVencimiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.rbtCInteresesFechaVencimiento.Location = new System.Drawing.Point(8, 176);
            this.rbtCInteresesFechaVencimiento.Name = "rbtCInteresesFechaVencimiento";
            this.rbtCInteresesFechaVencimiento.Size = new System.Drawing.Size(361, 17);
            this.rbtCInteresesFechaVencimiento.TabIndex = 9;
            this.rbtCInteresesFechaVencimiento.TabStop = true;
            this.rbtCInteresesFechaVencimiento.Text = "Calcular los intereses desde el Empeño hasta la Fecha de Vencimiento.";
            this.rbtCInteresesFechaVencimiento.UseVisualStyleBackColor = true;
            // 
            // txtPorcentaje
            // 
            this.txtPorcentaje.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPorcentaje.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtPorcentaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtPorcentaje.Location = new System.Drawing.Point(488, 102);
            this.txtPorcentaje.MaxLength = 5;
            this.txtPorcentaje.Name = "txtPorcentaje";
            this.txtPorcentaje.Size = new System.Drawing.Size(44, 20);
            this.txtPorcentaje.TabIndex = 10;
            this.txtPorcentaje.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPorcentaje_KeyPress);
            this.txtPorcentaje.Leave += new System.EventHandler(this.txtPorcentaje_Leave);
            // 
            // chkCliente
            // 
            this.chkCliente.AutoSize = true;
            this.chkCliente.Cursor = System.Windows.Forms.Cursors.Default;
            this.chkCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkCliente.Location = new System.Drawing.Point(792, 106);
            this.chkCliente.Name = "chkCliente";
            this.chkCliente.Size = new System.Drawing.Size(89, 17);
            this.chkCliente.TabIndex = 13;
            this.chkCliente.Text = "Cliente varios";
            this.chkCliente.UseVisualStyleBackColor = true;
            this.chkCliente.CheckedChanged += new System.EventHandler(this.chkCliente_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label5.Location = new System.Drawing.Point(680, 184);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 13);
            this.label5.TabIndex = 430;
            this.label5.Text = "No. de boletas por recibo";
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label1.Location = new System.Drawing.Point(-8, 424);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(920, 2);
            this.label1.TabIndex = 432;
            // 
            // txtPlazo
            // 
            this.txtPlazo.Location = new System.Drawing.Point(816, 184);
            this.txtPlazo.Name = "txtPlazo";
            this.txtPlazo.Size = new System.Drawing.Size(56, 20);
            this.txtPlazo.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(178, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 440;
            this.label4.Text = "Hora";
            // 
            // txtHora
            // 
            this.txtHora.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtHora.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtHora.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHora.Location = new System.Drawing.Point(216, 40);
            this.txtHora.MaxLength = 5;
            this.txtHora.Name = "txtHora";
            this.txtHora.ReadOnly = true;
            this.txtHora.Size = new System.Drawing.Size(62, 20);
            this.txtHora.TabIndex = 2;
            // 
            // dtpFecha
            // 
            this.dtpFecha.Cursor = System.Windows.Forms.Cursors.Default;
            this.dtpFecha.Enabled = false;
            this.dtpFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecha.Location = new System.Drawing.Point(58, 40);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(98, 20);
            this.dtpFecha.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 439;
            this.label3.Text = "Fecha";
            // 
            // txtBoleta
            // 
            this.txtBoleta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBoleta.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtBoleta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtBoleta.Location = new System.Drawing.Point(112, 376);
            this.txtBoleta.MaxLength = 10;
            this.txtBoleta.Name = "txtBoleta";
            this.txtBoleta.Size = new System.Drawing.Size(96, 20);
            this.txtBoleta.TabIndex = 16;
            this.txtBoleta.Leave += new System.EventHandler(this.txtBoleta_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Cursor = System.Windows.Forms.Cursors.Default;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(448, 376);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 13);
            this.label11.TabIndex = 442;
            this.label11.Text = "TOTALES";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Cursor = System.Windows.Forms.Cursors.Default;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label14.Location = new System.Drawing.Point(320, 40);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 13);
            this.label14.TabIndex = 447;
            this.label14.Text = "Empleado";
            // 
            // txtDescripcionCaja
            // 
            this.txtDescripcionCaja.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionCaja.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionCaja.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtDescripcionCaja.Location = new System.Drawing.Point(720, 40);
            this.txtDescripcionCaja.MaxLength = 250;
            this.txtDescripcionCaja.Name = "txtDescripcionCaja";
            this.txtDescripcionCaja.ReadOnly = true;
            this.txtDescripcionCaja.Size = new System.Drawing.Size(152, 20);
            this.txtDescripcionCaja.TabIndex = 5;
            // 
            // txtCodigoCaja
            // 
            this.txtCodigoCaja.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoCaja.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoCaja.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtCodigoCaja.Location = new System.Drawing.Point(664, 40);
            this.txtCodigoCaja.MaxLength = 50;
            this.txtCodigoCaja.Name = "txtCodigoCaja";
            this.txtCodigoCaja.ReadOnly = true;
            this.txtCodigoCaja.Size = new System.Drawing.Size(48, 20);
            this.txtCodigoCaja.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Cursor = System.Windows.Forms.Cursors.Default;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label8.Location = new System.Drawing.Point(608, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 13);
            this.label8.TabIndex = 446;
            this.label8.Text = "Caja";
            // 
            // txtNombreEmpleado
            // 
            this.txtNombreEmpleado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombreEmpleado.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNombreEmpleado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtNombreEmpleado.Location = new System.Drawing.Point(376, 40);
            this.txtNombreEmpleado.MaxLength = 250;
            this.txtNombreEmpleado.Name = "txtNombreEmpleado";
            this.txtNombreEmpleado.ReadOnly = true;
            this.txtNombreEmpleado.Size = new System.Drawing.Size(208, 20);
            this.txtNombreEmpleado.TabIndex = 3;
            // 
            // tsAcciones
            // 
            this.tsAcciones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbNuevo,
            this.tsbGuardar,
            this.tsbEliminar,
            this.tsbBuscar,
            this.btnUndo,
            this.toolStripSeparator2,
            this.btnPrimeroPag,
            this.btnAtras,
            this.btnAdelante,
            this.btnUlitmoPag,
            this.tsbImprimir,
            this.btnCierra});
            this.tsAcciones.Location = new System.Drawing.Point(0, 0);
            this.tsAcciones.Name = "tsAcciones";
            this.tsAcciones.Size = new System.Drawing.Size(897, 25);
            this.tsAcciones.TabIndex = 582;
            this.tsAcciones.Text = "toolStrip1";
            // 
            // tsbNuevo
            // 
            this.tsbNuevo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbNuevo.Image = global::EmpenosEvora.Properties.Resources.AddFile_16x;
            this.tsbNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbNuevo.Name = "tsbNuevo";
            this.tsbNuevo.Size = new System.Drawing.Size(23, 22);
            this.tsbNuevo.Text = "Nuevo";
            this.tsbNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical90;
            this.tsbNuevo.Click += new System.EventHandler(this.tsbNuevo_Click);
            // 
            // tsbGuardar
            // 
            this.tsbGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbGuardar.Image = global::EmpenosEvora.Properties.Resources.Save_16x;
            this.tsbGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGuardar.Name = "tsbGuardar";
            this.tsbGuardar.Size = new System.Drawing.Size(23, 22);
            this.tsbGuardar.Tag = "";
            this.tsbGuardar.Text = "Guardar";
            this.tsbGuardar.Click += new System.EventHandler(this.tsbGuardar_Click);
            // 
            // tsbEliminar
            // 
            this.tsbEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbEliminar.Image = global::EmpenosEvora.Properties.Resources.Cancel_16x;
            this.tsbEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEliminar.Name = "tsbEliminar";
            this.tsbEliminar.Size = new System.Drawing.Size(23, 22);
            this.tsbEliminar.Text = "Desactivar";
            this.tsbEliminar.ToolTipText = "Desactivar";
            this.tsbEliminar.Visible = false;
            // 
            // tsbBuscar
            // 
            this.tsbBuscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBuscar.Image = global::EmpenosEvora.Properties.Resources.VBSearch_16x;
            this.tsbBuscar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBuscar.Name = "tsbBuscar";
            this.tsbBuscar.Size = new System.Drawing.Size(23, 22);
            this.tsbBuscar.Text = "Buscar";
            this.tsbBuscar.Visible = false;
            // 
            // btnUndo
            // 
            this.btnUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUndo.Image = global::EmpenosEvora.Properties.Resources.Undo_16x;
            this.btnUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(23, 22);
            this.btnUndo.Text = "Deshacer";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrimeroPag
            // 
            this.btnPrimeroPag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrimeroPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeftGroup_16xSM;
            this.btnPrimeroPag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrimeroPag.Name = "btnPrimeroPag";
            this.btnPrimeroPag.Size = new System.Drawing.Size(23, 22);
            this.btnPrimeroPag.Text = "Primero";
            this.btnPrimeroPag.Visible = false;
            // 
            // btnAtras
            // 
            this.btnAtras.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAtras.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeft_16x;
            this.btnAtras.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(23, 22);
            this.btnAtras.Text = "Atras";
            this.btnAtras.Visible = false;
            // 
            // btnAdelante
            // 
            this.btnAdelante.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdelante.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronRight_16x;
            this.btnAdelante.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdelante.Name = "btnAdelante";
            this.btnAdelante.Size = new System.Drawing.Size(23, 22);
            this.btnAdelante.Text = "Siguente";
            this.btnAdelante.Visible = false;
            // 
            // btnUlitmoPag
            // 
            this.btnUlitmoPag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUlitmoPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronRightGroup_16x;
            this.btnUlitmoPag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUlitmoPag.Name = "btnUlitmoPag";
            this.btnUlitmoPag.Size = new System.Drawing.Size(23, 22);
            this.btnUlitmoPag.Text = "Ultimo";
            this.btnUlitmoPag.Visible = false;
            // 
            // tsbImprimir
            // 
            this.tsbImprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbImprimir.Image = global::EmpenosEvora.Properties.Resources.Print_16x;
            this.tsbImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbImprimir.Name = "tsbImprimir";
            this.tsbImprimir.Size = new System.Drawing.Size(23, 22);
            this.tsbImprimir.Text = "Imprimir";
            this.tsbImprimir.ToolTipText = "Imprimir";
            this.tsbImprimir.Visible = false;
            this.tsbImprimir.Click += new System.EventHandler(this.tsbImprimir_Click);
            // 
            // btnCierra
            // 
            this.btnCierra.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnCierra.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCierra.Name = "btnCierra";
            this.btnCierra.Size = new System.Drawing.Size(23, 22);
            this.btnCierra.Text = "Salir";
            this.btnCierra.Click += new System.EventHandler(this.btnCierra_Click);
            // 
            // btnFooterCierra
            // 
            this.btnFooterCierra.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnFooterCierra.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterCierra.Location = new System.Drawing.Point(800, 440);
            this.btnFooterCierra.Name = "btnFooterCierra";
            this.btnFooterCierra.Size = new System.Drawing.Size(75, 40);
            this.btnFooterCierra.TabIndex = 21;
            this.btnFooterCierra.Text = "Salir";
            this.btnFooterCierra.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterCierra.UseVisualStyleBackColor = true;
            this.btnFooterCierra.Click += new System.EventHandler(this.btnFooterCierra_Click);
            // 
            // btnFooterGuardar
            // 
            this.btnFooterGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterGuardar.Image = global::EmpenosEvora.Properties.Resources.Save_16x;
            this.btnFooterGuardar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterGuardar.Location = new System.Drawing.Point(712, 440);
            this.btnFooterGuardar.Name = "btnFooterGuardar";
            this.btnFooterGuardar.Size = new System.Drawing.Size(75, 40);
            this.btnFooterGuardar.TabIndex = 20;
            this.btnFooterGuardar.Text = "Guardar";
            this.btnFooterGuardar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterGuardar.UseVisualStyleBackColor = true;
            this.btnFooterGuardar.Click += new System.EventHandler(this.btnFooterGuardar_Click);
            // 
            // txtMeses
            // 
            this.txtMeses.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMeses.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtMeses.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtMeses.Location = new System.Drawing.Point(488, 128);
            this.txtMeses.MaxLength = 5;
            this.txtMeses.Name = "txtMeses";
            this.txtMeses.Size = new System.Drawing.Size(44, 20);
            this.txtMeses.TabIndex = 11;
            this.txtMeses.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMeses_KeyPress);
            this.txtMeses.Leave += new System.EventHandler(this.txtMeses_Leave);
            // 
            // lblMeses
            // 
            this.lblMeses.AutoSize = true;
            this.lblMeses.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblMeses.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblMeses.Location = new System.Drawing.Point(424, 128);
            this.lblMeses.Name = "lblMeses";
            this.lblMeses.Size = new System.Drawing.Size(38, 13);
            this.lblMeses.TabIndex = 588;
            this.lblMeses.Text = "Meses";
            // 
            // lblDias
            // 
            this.lblDias.AutoSize = true;
            this.lblDias.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblDias.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblDias.Location = new System.Drawing.Point(544, 128);
            this.lblDias.Name = "lblDias";
            this.lblDias.Size = new System.Drawing.Size(30, 13);
            this.lblDias.TabIndex = 590;
            this.lblDias.Text = "Días";
            // 
            // txtDias
            // 
            this.txtDias.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDias.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDias.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtDias.Location = new System.Drawing.Point(592, 128);
            this.txtDias.MaxLength = 5;
            this.txtDias.Name = "txtDias";
            this.txtDias.Size = new System.Drawing.Size(44, 20);
            this.txtDias.TabIndex = 12;
            this.txtDias.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDias_KeyPress);
            this.txtDias.Leave += new System.EventHandler(this.txtDias_Leave);
            // 
            // lblPorcentaje
            // 
            this.lblPorcentaje.AutoSize = true;
            this.lblPorcentaje.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblPorcentaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblPorcentaje.Location = new System.Drawing.Point(424, 104);
            this.lblPorcentaje.Name = "lblPorcentaje";
            this.lblPorcentaje.Size = new System.Drawing.Size(58, 13);
            this.lblPorcentaje.TabIndex = 591;
            this.lblPorcentaje.Text = "Porcentaje";
            // 
            // frmLiquidacionInternaBoletas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(897, 481);
            this.Controls.Add(this.lblPorcentaje);
            this.Controls.Add(this.lblDias);
            this.Controls.Add(this.txtDias);
            this.Controls.Add(this.lblMeses);
            this.Controls.Add(this.txtMeses);
            this.Controls.Add(this.btnFooterCierra);
            this.Controls.Add(this.btnFooterGuardar);
            this.Controls.Add(this.tsAcciones);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtDescripcionCaja);
            this.Controls.Add(this.txtCodigoCaja);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtNombreEmpleado);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtBoleta);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtHora);
            this.Controls.Add(this.dtpFecha);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtPlazo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.chkCliente);
            this.Controls.Add(this.txtPorcentaje);
            this.Controls.Add(this.rbtCInteresesFechaVencimiento);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtAcumSubtotal);
            this.Controls.Add(this.txtAcumInt);
            this.Controls.Add(this.txtAcumCapial);
            this.Controls.Add(this.dgvDetalles);
            this.Controls.Add(this.rbtCInteresesFechaLiquidacion);
            this.Controls.Add(this.rbtANumeroMesesDias);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rbtAPorcentajeIntereses);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLiquidacionInternaBoletas";
            this.Text = "Liquidación interna de boletas";
            this.Load += new System.EventHandler(this.frmLiquidacionInternaBoletas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlazo)).EndInit();
            this.tsAcciones.ResumeLayout(false);
            this.tsAcciones.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtAcumSubtotal;
        private System.Windows.Forms.TextBox txtAcumInt;
        private System.Windows.Forms.TextBox txtAcumCapial;
        private System.Windows.Forms.DataGridView dgvDetalles;
        private System.Windows.Forms.RadioButton rbtCInteresesFechaLiquidacion;
        private System.Windows.Forms.RadioButton rbtANumeroMesesDias;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbtAPorcentajeIntereses;
        private System.Windows.Forms.RadioButton rbtCInteresesFechaVencimiento;
        private System.Windows.Forms.TextBox txtPorcentaje;
        private System.Windows.Forms.CheckBox chkCliente;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown txtPlazo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtHora;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBoleta;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtDescripcionCaja;
        private System.Windows.Forms.TextBox txtCodigoCaja;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNombreEmpleado;
        private System.Windows.Forms.ToolStrip tsAcciones;
        private System.Windows.Forms.ToolStripButton tsbNuevo;
        private System.Windows.Forms.ToolStripButton tsbGuardar;
        private System.Windows.Forms.ToolStripButton tsbEliminar;
        private System.Windows.Forms.ToolStripButton tsbBuscar;
        private System.Windows.Forms.ToolStripButton btnUndo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnPrimeroPag;
        private System.Windows.Forms.ToolStripButton btnAtras;
        private System.Windows.Forms.ToolStripButton btnAdelante;
        private System.Windows.Forms.ToolStripButton btnUlitmoPag;
        private System.Windows.Forms.ToolStripButton tsbImprimir;
        private System.Windows.Forms.ToolStripButton btnCierra;
        private System.Windows.Forms.Button btnFooterCierra;
        private System.Windows.Forms.Button btnFooterGuardar;
        private System.Windows.Forms.TextBox txtMeses;
        private System.Windows.Forms.Label lblMeses;
        private System.Windows.Forms.Label lblDias;
        private System.Windows.Forms.TextBox txtDias;
        private System.Windows.Forms.Label lblPorcentaje;
        private System.Windows.Forms.DataGridViewTextBoxColumn cliente_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn porcentajeTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn prendaTipoId;
        private System.Windows.Forms.DataGridViewTextBoxColumn cobroTipoId;
        private System.Windows.Forms.DataGridViewTextBoxColumn porcentajeCobroTipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn boleta_empeno_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Boleta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vence;
        private System.Windows.Forms.DataGridViewTextBoxColumn Capital;
        private System.Windows.Forms.DataGridViewTextBoxColumn IntAlmMan;
        private System.Windows.Forms.DataGridViewTextBoxColumn Subtotal;
    }
}