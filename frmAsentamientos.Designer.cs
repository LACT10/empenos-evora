﻿namespace EmpenosEvora
{
    partial class frmAsentamientos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txAsentamientoTipoId = new System.Windows.Forms.TextBox();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.tsAcciones = new System.Windows.Forms.ToolStrip();
            this.tsbNuevo = new System.Windows.Forms.ToolStripButton();
            this.tsbGuardar = new System.Windows.Forms.ToolStripButton();
            this.tsbEliminar = new System.Windows.Forms.ToolStripButton();
            this.tsbBuscar = new System.Windows.Forms.ToolStripButton();
            this.btnUndo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrimeroPag = new System.Windows.Forms.ToolStripButton();
            this.btnAtras = new System.Windows.Forms.ToolStripButton();
            this.btnAdelante = new System.Windows.Forms.ToolStripButton();
            this.btnUlitmoPag = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbImprimir = new System.Windows.Forms.ToolStripButton();
            this.btnCierra = new System.Windows.Forms.ToolStripButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCodigoAsentamientoTipo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDescripcionAsentamientoTipo = new System.Windows.Forms.TextBox();
            this.btnBusqueda = new System.Windows.Forms.Button();
            this.btnFooterCierra = new System.Windows.Forms.Button();
            this.btnFooterGuardar = new System.Windows.Forms.Button();
            this.txtMunicipioId = new System.Windows.Forms.TextBox();
            this.btnBusquedaMunicipio = new System.Windows.Forms.Button();
            this.txtMunicipioCodigo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMunicipioDescripacion = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCiudad = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCodigoPostal = new System.Windows.Forms.TextBox();
            this.btnBusquedaAsentamientoTipo = new System.Windows.Forms.Button();
            this.txtEstadoId = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtEstadoDescripcion = new System.Windows.Forms.TextBox();
            this.txtEstadoCodigo = new System.Windows.Forms.TextBox();
            this.txtDescripcionAnt = new System.Windows.Forms.TextBox();
            this.tsAcciones.SuspendLayout();
            this.SuspendLayout();
            // 
            // txAsentamientoTipoId
            // 
            this.txAsentamientoTipoId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txAsentamientoTipoId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txAsentamientoTipoId.Location = new System.Drawing.Point(352, 104);
            this.txAsentamientoTipoId.MaxLength = 3;
            this.txAsentamientoTipoId.Name = "txAsentamientoTipoId";
            this.txAsentamientoTipoId.Size = new System.Drawing.Size(44, 20);
            this.txAsentamientoTipoId.TabIndex = 183;
            this.txAsentamientoTipoId.Visible = false;
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcion.Location = new System.Drawing.Point(112, 40);
            this.txtDescripcion.MaxLength = 250;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(280, 20);
            this.txtDescripcion.TabIndex = 1;
            this.txtDescripcion.Leave += new System.EventHandler(this.txtDescripcion_Leave);
            // 
            // tsAcciones
            // 
            this.tsAcciones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbNuevo,
            this.tsbGuardar,
            this.tsbEliminar,
            this.tsbBuscar,
            this.btnUndo,
            this.toolStripSeparator2,
            this.btnPrimeroPag,
            this.btnAtras,
            this.btnAdelante,
            this.btnUlitmoPag,
            this.toolStripSeparator1,
            this.tsbImprimir,
            this.btnCierra});
            this.tsAcciones.Location = new System.Drawing.Point(0, 0);
            this.tsAcciones.Name = "tsAcciones";
            this.tsAcciones.Size = new System.Drawing.Size(458, 25);
            this.tsAcciones.TabIndex = 179;
            this.tsAcciones.Text = "toolStrip1";
            // 
            // tsbNuevo
            // 
            this.tsbNuevo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbNuevo.Image = global::EmpenosEvora.Properties.Resources.AddFile_16x;
            this.tsbNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbNuevo.Name = "tsbNuevo";
            this.tsbNuevo.Size = new System.Drawing.Size(23, 22);
            this.tsbNuevo.Text = "Nuevo";
            this.tsbNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical90;
            this.tsbNuevo.Click += new System.EventHandler(this.TsbNuevo_Click);
            // 
            // tsbGuardar
            // 
            this.tsbGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbGuardar.Image = global::EmpenosEvora.Properties.Resources.Save_16x;
            this.tsbGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGuardar.Name = "tsbGuardar";
            this.tsbGuardar.Size = new System.Drawing.Size(23, 22);
            this.tsbGuardar.Tag = "";
            this.tsbGuardar.Text = "Guardar";
            this.tsbGuardar.Click += new System.EventHandler(this.TsbGuardar_Click);
            // 
            // tsbEliminar
            // 
            this.tsbEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbEliminar.Image = global::EmpenosEvora.Properties.Resources.Cancel_16x;
            this.tsbEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEliminar.Name = "tsbEliminar";
            this.tsbEliminar.Size = new System.Drawing.Size(23, 22);
            this.tsbEliminar.Text = "Desactivar";
            this.tsbEliminar.ToolTipText = "Desactivar";
            this.tsbEliminar.Click += new System.EventHandler(this.TsbEliminar_Click);
            // 
            // tsbBuscar
            // 
            this.tsbBuscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBuscar.Image = global::EmpenosEvora.Properties.Resources.VBSearch_16x;
            this.tsbBuscar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBuscar.Name = "tsbBuscar";
            this.tsbBuscar.Size = new System.Drawing.Size(23, 22);
            this.tsbBuscar.Text = "Buscar";
            this.tsbBuscar.Click += new System.EventHandler(this.TsbBuscar_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUndo.Image = global::EmpenosEvora.Properties.Resources.Undo_16x;
            this.btnUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(23, 22);
            this.btnUndo.Text = "Deshacer";
            this.btnUndo.Click += new System.EventHandler(this.BtnUndo_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrimeroPag
            // 
            this.btnPrimeroPag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrimeroPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeftGroup_16xSM;
            this.btnPrimeroPag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrimeroPag.Name = "btnPrimeroPag";
            this.btnPrimeroPag.Size = new System.Drawing.Size(23, 22);
            this.btnPrimeroPag.Text = "Primero";
            this.btnPrimeroPag.Click += new System.EventHandler(this.BtnPrimeroPag_Click);
            // 
            // btnAtras
            // 
            this.btnAtras.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAtras.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeft_16x;
            this.btnAtras.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(23, 22);
            this.btnAtras.Text = "Atras";
            this.btnAtras.Click += new System.EventHandler(this.BtnAtras_Click);
            // 
            // btnAdelante
            // 
            this.btnAdelante.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdelante.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronRight_16x;
            this.btnAdelante.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdelante.Name = "btnAdelante";
            this.btnAdelante.Size = new System.Drawing.Size(23, 22);
            this.btnAdelante.Text = "Siguente";
            this.btnAdelante.Click += new System.EventHandler(this.BtnAdelante_Click);
            // 
            // btnUlitmoPag
            // 
            this.btnUlitmoPag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUlitmoPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronRightGroup_16x;
            this.btnUlitmoPag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUlitmoPag.Name = "btnUlitmoPag";
            this.btnUlitmoPag.Size = new System.Drawing.Size(23, 22);
            this.btnUlitmoPag.Text = "Ultimo";
            this.btnUlitmoPag.Click += new System.EventHandler(this.BtnUlitmoPag_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbImprimir
            // 
            this.tsbImprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbImprimir.Image = global::EmpenosEvora.Properties.Resources.Print_16x;
            this.tsbImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbImprimir.Name = "tsbImprimir";
            this.tsbImprimir.Size = new System.Drawing.Size(23, 22);
            this.tsbImprimir.Text = "Imprimir";
            this.tsbImprimir.ToolTipText = "Imprimir";
            // 
            // btnCierra
            // 
            this.btnCierra.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnCierra.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCierra.Name = "btnCierra";
            this.btnCierra.Size = new System.Drawing.Size(23, 22);
            this.btnCierra.Text = "toolStripButton4";
            this.btnCierra.Click += new System.EventHandler(this.BtnCierra_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 178;
            this.label2.Text = "Descripción";
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Location = new System.Drawing.Point(0, 200);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(496, 2);
            this.label3.TabIndex = 173;
            // 
            // txtCodigoAsentamientoTipo
            // 
            this.txtCodigoAsentamientoTipo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoAsentamientoTipo.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoAsentamientoTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoAsentamientoTipo.Location = new System.Drawing.Point(112, 104);
            this.txtCodigoAsentamientoTipo.MaxLength = 50;
            this.txtCodigoAsentamientoTipo.Name = "txtCodigoAsentamientoTipo";
            this.txtCodigoAsentamientoTipo.Size = new System.Drawing.Size(48, 20);
            this.txtCodigoAsentamientoTipo.TabIndex = 5;
            this.txtCodigoAsentamientoTipo.Leave += new System.EventHandler(this.TxtCodigoAsentamientoTipo_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 171;
            this.label1.Text = "Tipo asentamiento";
            // 
            // txtDescripcionAsentamientoTipo
            // 
            this.txtDescripcionAsentamientoTipo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionAsentamientoTipo.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionAsentamientoTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcionAsentamientoTipo.Location = new System.Drawing.Point(168, 104);
            this.txtDescripcionAsentamientoTipo.MaxLength = 250;
            this.txtDescripcionAsentamientoTipo.Name = "txtDescripcionAsentamientoTipo";
            this.txtDescripcionAsentamientoTipo.ReadOnly = true;
            this.txtDescripcionAsentamientoTipo.Size = new System.Drawing.Size(224, 20);
            this.txtDescripcionAsentamientoTipo.TabIndex = 6;
            // 
            // btnBusqueda
            // 
            this.btnBusqueda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBusqueda.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusqueda.Location = new System.Drawing.Point(400, 40);
            this.btnBusqueda.Name = "btnBusqueda";
            this.btnBusqueda.Size = new System.Drawing.Size(32, 23);
            this.btnBusqueda.TabIndex = 2;
            this.btnBusqueda.UseVisualStyleBackColor = true;
            this.btnBusqueda.Click += new System.EventHandler(this.BtnBusqueda_Click);
            // 
            // btnFooterCierra
            // 
            this.btnFooterCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnFooterCierra.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterCierra.Location = new System.Drawing.Point(360, 208);
            this.btnFooterCierra.Name = "btnFooterCierra";
            this.btnFooterCierra.Size = new System.Drawing.Size(75, 40);
            this.btnFooterCierra.TabIndex = 14;
            this.btnFooterCierra.Text = "Salir";
            this.btnFooterCierra.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterCierra.UseVisualStyleBackColor = true;
            this.btnFooterCierra.Click += new System.EventHandler(this.BtnFooterCierra_Click);
            // 
            // btnFooterGuardar
            // 
            this.btnFooterGuardar.Image = global::EmpenosEvora.Properties.Resources.Save_16x;
            this.btnFooterGuardar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterGuardar.Location = new System.Drawing.Point(272, 208);
            this.btnFooterGuardar.Name = "btnFooterGuardar";
            this.btnFooterGuardar.Size = new System.Drawing.Size(75, 40);
            this.btnFooterGuardar.TabIndex = 13;
            this.btnFooterGuardar.Text = "Guardar";
            this.btnFooterGuardar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterGuardar.UseVisualStyleBackColor = true;
            this.btnFooterGuardar.Click += new System.EventHandler(this.BtnFooterGuardar_Click);
            // 
            // txtMunicipioId
            // 
            this.txtMunicipioId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtMunicipioId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMunicipioId.Location = new System.Drawing.Point(344, 136);
            this.txtMunicipioId.MaxLength = 3;
            this.txtMunicipioId.Name = "txtMunicipioId";
            this.txtMunicipioId.Size = new System.Drawing.Size(44, 20);
            this.txtMunicipioId.TabIndex = 188;
            this.txtMunicipioId.Visible = false;
            // 
            // btnBusquedaMunicipio
            // 
            this.btnBusquedaMunicipio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBusquedaMunicipio.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusquedaMunicipio.Location = new System.Drawing.Point(400, 136);
            this.btnBusquedaMunicipio.Name = "btnBusquedaMunicipio";
            this.btnBusquedaMunicipio.Size = new System.Drawing.Size(32, 23);
            this.btnBusquedaMunicipio.TabIndex = 10;
            this.btnBusquedaMunicipio.UseVisualStyleBackColor = true;
            this.btnBusquedaMunicipio.Click += new System.EventHandler(this.BtnBusquedaMunicipio_Click);
            // 
            // txtMunicipioCodigo
            // 
            this.txtMunicipioCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMunicipioCodigo.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtMunicipioCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMunicipioCodigo.Location = new System.Drawing.Point(112, 136);
            this.txtMunicipioCodigo.MaxLength = 50;
            this.txtMunicipioCodigo.Name = "txtMunicipioCodigo";
            this.txtMunicipioCodigo.Size = new System.Drawing.Size(48, 20);
            this.txtMunicipioCodigo.TabIndex = 8;
            this.txtMunicipioCodigo.TextChanged += new System.EventHandler(this.TxtCodigo_TextChanged);
            this.txtMunicipioCodigo.Leave += new System.EventHandler(this.TxtMunicipioCodigo_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 185;
            this.label4.Text = "Municipio";
            // 
            // txtMunicipioDescripacion
            // 
            this.txtMunicipioDescripacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMunicipioDescripacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtMunicipioDescripacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMunicipioDescripacion.Location = new System.Drawing.Point(168, 136);
            this.txtMunicipioDescripacion.MaxLength = 250;
            this.txtMunicipioDescripacion.Name = "txtMunicipioDescripacion";
            this.txtMunicipioDescripacion.ReadOnly = true;
            this.txtMunicipioDescripacion.Size = new System.Drawing.Size(224, 20);
            this.txtMunicipioDescripacion.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 189;
            this.label5.Text = "Ciudad";
            // 
            // txtCiudad
            // 
            this.txtCiudad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCiudad.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCiudad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCiudad.Location = new System.Drawing.Point(112, 72);
            this.txtCiudad.MaxLength = 250;
            this.txtCiudad.Name = "txtCiudad";
            this.txtCiudad.Size = new System.Drawing.Size(184, 20);
            this.txtCiudad.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(312, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 191;
            this.label6.Text = "Código postal";
            // 
            // txtCodigoPostal
            // 
            this.txtCodigoPostal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoPostal.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoPostal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoPostal.Location = new System.Drawing.Point(392, 72);
            this.txtCodigoPostal.MaxLength = 5;
            this.txtCodigoPostal.Name = "txtCodigoPostal";
            this.txtCodigoPostal.Size = new System.Drawing.Size(40, 20);
            this.txtCodigoPostal.TabIndex = 4;
            this.txtCodigoPostal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtCodigoPostal_KeyPress);
            // 
            // btnBusquedaAsentamientoTipo
            // 
            this.btnBusquedaAsentamientoTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBusquedaAsentamientoTipo.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusquedaAsentamientoTipo.Location = new System.Drawing.Point(400, 104);
            this.btnBusquedaAsentamientoTipo.Name = "btnBusquedaAsentamientoTipo";
            this.btnBusquedaAsentamientoTipo.Size = new System.Drawing.Size(32, 23);
            this.btnBusquedaAsentamientoTipo.TabIndex = 7;
            this.btnBusquedaAsentamientoTipo.UseVisualStyleBackColor = true;
            this.btnBusquedaAsentamientoTipo.Click += new System.EventHandler(this.BtnBusquedaAsentamientoTipo_Click);
            // 
            // txtEstadoId
            // 
            this.txtEstadoId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtEstadoId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEstadoId.Location = new System.Drawing.Point(344, 168);
            this.txtEstadoId.MaxLength = 3;
            this.txtEstadoId.Name = "txtEstadoId";
            this.txtEstadoId.Size = new System.Drawing.Size(44, 20);
            this.txtEstadoId.TabIndex = 197;
            this.txtEstadoId.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Cursor = System.Windows.Forms.Cursors.Default;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 168);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 196;
            this.label7.Text = "Estado";
            // 
            // txtEstadoDescripcion
            // 
            this.txtEstadoDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEstadoDescripcion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtEstadoDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEstadoDescripcion.Location = new System.Drawing.Point(168, 168);
            this.txtEstadoDescripcion.MaxLength = 250;
            this.txtEstadoDescripcion.Name = "txtEstadoDescripcion";
            this.txtEstadoDescripcion.ReadOnly = true;
            this.txtEstadoDescripcion.Size = new System.Drawing.Size(264, 20);
            this.txtEstadoDescripcion.TabIndex = 12;
            // 
            // txtEstadoCodigo
            // 
            this.txtEstadoCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEstadoCodigo.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtEstadoCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEstadoCodigo.Location = new System.Drawing.Point(112, 168);
            this.txtEstadoCodigo.MaxLength = 50;
            this.txtEstadoCodigo.Name = "txtEstadoCodigo";
            this.txtEstadoCodigo.ReadOnly = true;
            this.txtEstadoCodigo.Size = new System.Drawing.Size(48, 20);
            this.txtEstadoCodigo.TabIndex = 11;
            // 
            // txtDescripcionAnt
            // 
            this.txtDescripcionAnt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionAnt.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionAnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcionAnt.Location = new System.Drawing.Point(192, 40);
            this.txtDescripcionAnt.MaxLength = 3;
            this.txtDescripcionAnt.Name = "txtDescripcionAnt";
            this.txtDescripcionAnt.Size = new System.Drawing.Size(44, 20);
            this.txtDescripcionAnt.TabIndex = 543;
            this.txtDescripcionAnt.Visible = false;
            // 
            // frmAsentamientos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(458, 258);
            this.Controls.Add(this.txtDescripcionAnt);
            this.Controls.Add(this.txtEstadoCodigo);
            this.Controls.Add(this.txtEstadoId);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtEstadoDescripcion);
            this.Controls.Add(this.btnBusquedaAsentamientoTipo);
            this.Controls.Add(this.txtCodigoPostal);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtCiudad);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtMunicipioId);
            this.Controls.Add(this.btnBusquedaMunicipio);
            this.Controls.Add(this.txtMunicipioCodigo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtMunicipioDescripacion);
            this.Controls.Add(this.txAsentamientoTipoId);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.tsAcciones);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnBusqueda);
            this.Controls.Add(this.btnFooterCierra);
            this.Controls.Add(this.btnFooterGuardar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtCodigoAsentamientoTipo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtDescripcionAsentamientoTipo);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAsentamientos";
            this.Text = "Asentamientos ";
            this.Load += new System.EventHandler(this.FrmAsentamientos_Load);
            this.tsAcciones.ResumeLayout(false);
            this.tsAcciones.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txAsentamientoTipoId;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.ToolStrip tsAcciones;
        private System.Windows.Forms.ToolStripButton tsbNuevo;
        private System.Windows.Forms.ToolStripButton tsbGuardar;
        private System.Windows.Forms.ToolStripButton tsbEliminar;
        private System.Windows.Forms.ToolStripButton btnUndo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnPrimeroPag;
        private System.Windows.Forms.ToolStripButton btnAtras;
        private System.Windows.Forms.ToolStripButton btnAdelante;
        private System.Windows.Forms.ToolStripButton btnUlitmoPag;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbImprimir;
        private System.Windows.Forms.ToolStripButton btnCierra;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnBusqueda;
        private System.Windows.Forms.Button btnFooterCierra;
        private System.Windows.Forms.Button btnFooterGuardar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCodigoAsentamientoTipo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDescripcionAsentamientoTipo;
        private System.Windows.Forms.TextBox txtMunicipioId;
        private System.Windows.Forms.Button btnBusquedaMunicipio;
        private System.Windows.Forms.TextBox txtMunicipioCodigo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMunicipioDescripacion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCiudad;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCodigoPostal;
        private System.Windows.Forms.Button btnBusquedaAsentamientoTipo;
        private System.Windows.Forms.ToolStripButton tsbBuscar;
        private System.Windows.Forms.TextBox txtEstadoId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtEstadoDescripcion;
        private System.Windows.Forms.TextBox txtEstadoCodigo;
        private System.Windows.Forms.TextBox txtDescripcionAnt;
    }
}