﻿namespace EmpenosEvora
{
    partial class frmSubirArchivos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ofdSubirFrente = new System.Windows.Forms.OpenFileDialog();
            this.btnSubirFrente = new System.Windows.Forms.Button();
            this.btnSubirAtras = new System.Windows.Forms.Button();
            this.ofdSubirAtras = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblIdentificacionFrontal = new System.Windows.Forms.Label();
            this.lblIdentificacionTrasera = new System.Windows.Forms.Label();
            this.btnCaptura = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ofdSubirFrente
            // 
            this.ofdSubirFrente.FileName = "openFileDialog1";
            // 
            // btnSubirFrente
            // 
            this.btnSubirFrente.Location = new System.Drawing.Point(8, 40);
            this.btnSubirFrente.Name = "btnSubirFrente";
            this.btnSubirFrente.Size = new System.Drawing.Size(75, 23);
            this.btnSubirFrente.TabIndex = 0;
            this.btnSubirFrente.Text = "Subir";
            this.btnSubirFrente.UseVisualStyleBackColor = true;
            this.btnSubirFrente.Click += new System.EventHandler(this.btnSubirFrente_Click);
            // 
            // btnSubirAtras
            // 
            this.btnSubirAtras.Location = new System.Drawing.Point(248, 40);
            this.btnSubirAtras.Name = "btnSubirAtras";
            this.btnSubirAtras.Size = new System.Drawing.Size(75, 23);
            this.btnSubirAtras.TabIndex = 1;
            this.btnSubirAtras.Text = "Subir";
            this.btnSubirAtras.UseVisualStyleBackColor = true;
            this.btnSubirAtras.Click += new System.EventHandler(this.btnSubirAtras_Click);
            // 
            // ofdSubirAtras
            // 
            this.ofdSubirAtras.FileName = "openFileDialog1";
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(8, 16);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(102, 13);
            this.lblCodigo.TabIndex = 44;
            this.lblCodigo.Text = "Identificación frontal";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(248, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 45;
            this.label1.Text = "Identificación trasera";
            // 
            // lblIdentificacionFrontal
            // 
            this.lblIdentificacionFrontal.AutoSize = true;
            this.lblIdentificacionFrontal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdentificacionFrontal.Location = new System.Drawing.Point(8, 72);
            this.lblIdentificacionFrontal.Name = "lblIdentificacionFrontal";
            this.lblIdentificacionFrontal.Size = new System.Drawing.Size(63, 13);
            this.lblIdentificacionFrontal.TabIndex = 46;
            this.lblIdentificacionFrontal.Text = "Archivo  0%";
            // 
            // lblIdentificacionTrasera
            // 
            this.lblIdentificacionTrasera.AutoSize = true;
            this.lblIdentificacionTrasera.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdentificacionTrasera.Location = new System.Drawing.Point(248, 72);
            this.lblIdentificacionTrasera.Name = "lblIdentificacionTrasera";
            this.lblIdentificacionTrasera.Size = new System.Drawing.Size(60, 13);
            this.lblIdentificacionTrasera.TabIndex = 47;
            this.lblIdentificacionTrasera.Text = "Archivo 0%";
            // 
            // btnCaptura
            // 
            this.btnCaptura.Location = new System.Drawing.Point(8, 104);
            this.btnCaptura.Name = "btnCaptura";
            this.btnCaptura.Size = new System.Drawing.Size(75, 23);
            this.btnCaptura.TabIndex = 48;
            this.btnCaptura.Text = "Captura Huella";
            this.btnCaptura.UseVisualStyleBackColor = true;
            this.btnCaptura.Click += new System.EventHandler(this.btnCaptura_Click);
            // 
            // frmSubirArchivos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(367, 136);
            this.Controls.Add(this.btnCaptura);
            this.Controls.Add(this.lblIdentificacionTrasera);
            this.Controls.Add(this.lblIdentificacionFrontal);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.btnSubirAtras);
            this.Controls.Add(this.btnSubirFrente);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSubirArchivos";
            this.Text = "Subir Archivo";
            this.Load += new System.EventHandler(this.frmSubirArchivos_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog ofdSubirFrente;
        private System.Windows.Forms.Button btnSubirFrente;
        private System.Windows.Forms.Button btnSubirAtras;
        private System.Windows.Forms.OpenFileDialog ofdSubirAtras;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblIdentificacionFrontal;
        private System.Windows.Forms.Label lblIdentificacionTrasera;
        private System.Windows.Forms.Button btnCaptura;
    }
}