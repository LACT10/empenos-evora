﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmBusquedaRangoFechaBoleta : Form
    {
        public DataSet _objDataSet;

        public string _strTipoFormulario;
        public string _strQuery;
        public List<string> _lsBloquearCol;
        public DataGridViewRow _rowsIndex;
        public bool _bolCierra;
        public bool _bolPrimeraBusq;
        public DataSet objDataSet { get => _objDataSet; set => _objDataSet = value; }

        public string strTipoFormulario { get => _strTipoFormulario; set => _strTipoFormulario = value; }

        public string strQuery { get => _strQuery; set => _strQuery = value; }



        public bool bolCierra { get => _bolCierra; set => _bolCierra = value; }
        public bool bolPrimeraBusq { get => _bolPrimeraBusq; set => _bolPrimeraBusq = value; }

        public Core.clsCoreView clsView = new Core.clsCoreView();

        public Properties_Class.clsCliente cliente;

        public Dictionary<string, string> dicTipoFormulario = new Dictionary<string, string>()
        {
            {"Simple", "Simple"},
            {"Filtro", "Filtro"},
        };

        public Dictionary<string, string> dicReemplazo = new Dictionary<string, string>()
        {
            {"texto", "@txt"},
            {"entero", "@int"},
            {"decimal", "@float"},
            {"fecha", "@datetime"},
            {"fecha2", "@fechafinal"}
        };

        public List<string> lsBloquearCol { get => _lsBloquearCol; set => _lsBloquearCol = value; }


        public DataGridViewRow rowsIndex { get => _rowsIndex; set => _rowsIndex = value; }        

        public frmBusquedaRangoFechaBoleta()
        {
            InitializeComponent();
            this.bolCierra = false;
        }

        private void frmBusquedaRangoFechaBoleta_Load(object sender, EventArgs e)
        {

            DateTime now = DateTime.Today;


            this.bolPrimeraBusq = true;
            this.filtro();
            //dgvResultado.AutoGenerateColumns = true;

            dgvResultado.DataSource = this.objDataSet.Tables[0].DefaultView; // dataset                    
            this.bloquearColumnasGrid();
            //dgvResultado.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            //dgvResultado.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dgvResultado.AllowUserToAddRows = false;

            llenar_formulario();
        }

        private void btnBusqueda_Click(object sender, EventArgs e)
        {
            this.filtro();
        }

        private void DgvResultado_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //dgvResultado.Rows[e.RowIndex]

        }

        private void dgvResultado_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvResultado.CurrentRow.Selected = true;
            this.rowsIndex = dgvResultado.SelectedRows[0];
            this.bolCierra = true;
            this.Close();

        }

        private void TxtDescripcion_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.filtro();
            }
        }

        public void bloquearColumnasGrid()
        {
            this.lsBloquearCol.ForEach(delegate (string col)
            {
                dgvResultado.Columns[col].Visible = false;
            });
        }

        public void filtro()
        {


          



            /*this.strQuery = this.strQuery.Replace(this.dicReemplazo["texto"], txtDescripcion.Text.Trim().ToString());
            this.objDataSet.Tables[0].DefaultView.RowFilter = this.strQuery;*/

        }

        private void FrmBusqueda_FormClosing(object sender, FormClosingEventArgs e)
        {
            /*txtDescripcion.Text = "";
            this.filtro();*/
        }

        private void frmBusqueda_FormClosed(object sender, FormClosedEventArgs e)
        {
            /*txtDescripcion.Text = "";
            this.filtro();*/
        }

        public void llenar_formulario() 
        {
            string strDireccion = this.cliente.strCalle;
            if (this.cliente.strNumeroExterior != "") 
            {
                strDireccion += " No. Ext. " + this.cliente.strNumeroExterior;
            }

            if (this.cliente.strNumeroInterior != "")
            {
                strDireccion += " No. Int. " + this.cliente.strNumeroInterior;
            }

            strDireccion += " C.P. " + this.cliente.asentamiento.strCodigoPostal;

            strDireccion += " , " + this.cliente.asentamiento.municipio.strDescripcion;
            strDireccion += " , " + this.cliente.asentamiento.municipio.estado.strDescripcion;
            


            txtNombre.Text = this.cliente.strNombre + " " + this.cliente.strApellidoPaterno + " " + this.cliente.strApellidoMaterno;
            txtNombreCotitular.Text = this.cliente.strCotitularNombre;
            txtNombreBeneficario.Text = this.cliente.strBeneficiarioNombre;
            txtDireccion.Text = strDireccion;
            txtRfc.Text = this.cliente.strRFC;
            txtCurp.Text = this.cliente.strCURP;
            txtAsentamiento.Text = this.cliente.asentamiento.strDescripcion;
            txtCodigoPostal.Text = this.cliente.asentamiento.strCodigoPostal;
            txtTelefono01.Text = this.cliente.strTelefono01;
            txtTelefono02.Text = this.cliente.strTelefono02;
            txtIdentificacion.Text = this.cliente.identificacionesTipo.strCodigo + " - " + this.cliente.identificacionesTipo.strDescripcion;
            txtNoIdentificacion.Text = this.cliente.strNumeroIdentificacion;
            txtComentario.Text = this.cliente.strComentario;

            decimal decEmpenado = 0;
            decimal decIntCobrados = 0;
            decimal decActivo = 0;

            foreach (DataRow row in this.objDataSet.Tables[0].Rows) 
            {
                if (row["Importe "].ToString() != "")
                {
                    decEmpenado += decimal.Parse(row["Importe "].ToString().Replace("$", "").Replace(",", ""));
                }
                if (row["Int.Cobrados"].ToString() != "") 
                {
                    decIntCobrados += decimal.Parse(row["Int.Cobrados"].ToString().Replace("$", "").Replace(",", ""));
                }
                if (row["estatus"].ToString() == "ACTIVO") 
                {
                    decActivo += decimal.Parse(row["Importe "].ToString().Replace("$", "").Replace(",", ""));
                }


                
            }


            txtTotalEmpenado.Text = this.clsView.convertMoneda(decEmpenado.ToString());
            txtInteresesCobrados.Text = this.clsView.convertMoneda(decIntCobrados.ToString());
            txtTotalActivo.Text = this.clsView.convertMoneda(decActivo.ToString());


        
        }

        private void btnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.clsView.cierraFormulariosESC(this, this.Text.ToLower());
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }



    }
}
