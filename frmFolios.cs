﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmFolios : Form
    {
        public Controller.clsCtrlFolios ctrlFolio;
        public Core.clsCoreView clsView;

        public int _intUlitmoCodigo;
        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }

        public List<Properties_Class.clsFolios> lsDeshacerFolio;

        public frmFolios()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();

            this.ctrlFolio = new Controller.clsCtrlFolios();
            this.ctrlFolio.Paginacion();
            this.lsDeshacerFolio = new List<Properties_Class.clsFolios>();
            this.lsDeshacerFolio.Add(this.ctrlFolio.folio);
        }

        private void FrmEstados_Load(object sender, EventArgs e)
        {
            /*this.clsView.strForm = this.Name;
            //this.clsView.strForm = "frmEstados";
            this.clsView.revisarPantallas();

            if (this.clsView.bolIsopenForm) return;
            this.ctrlFolio = new Controller.clsCtrlFolios();
            this.ctrlFolio.Paginacion();
            this.lsDeshacerFolio = new List<Properties_Class.clsFolios>();
            this.lsDeshacerFolio.Add(this.ctrlFolio.estado);*/
        }

        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnBusqueda_Click(object sender, EventArgs e)
        {
            if (this.ctrlFolio.bolResutladoPaginacion)
            {
                this.ctrlFolio.CargarFrmBusqueda();

                if (this.ctrlFolio.bolResultadoBusqueda)
                {
                    this.llenar_formulario();
                    txtDescripcion.Focus();
                }
            }
        }

        private void TsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlFolio.folio.intId == 0) return;
            if (this.clsView.confirmacionEliminar("Estados"))
            {
                this.ctrlFolio.Eliminar();
                if (this.ctrlFolio.bolResultado)
                {
                    this.ctrlFolio.Paginacion();
                    this.limpiar_textbox();
                    this.clsView.mensajeExitoDB("estado", "elimino");
                }
            }
        }


        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlFolio.bolResutladoPaginacion)
            {

                this.clsView.principal.epMensaje.Clear();
                //txtCodigo.Text = this.lsDeshacerFolio[0].strCodigo.ToString();
                //if (this.lsDeshacerFolio.Count == 0) return;
                this.clsView.principal.epMensaje.Clear();
                txtFolioId.Text = this.lsDeshacerFolio[0].intId.ToString();
                txtDescripcion.Text = this.lsDeshacerFolio[0].strDescripcion.ToString();
                txtConsecutivo.Text = this.lsDeshacerFolio[0].intConsecutivo.ToString();
                txtSeries.Text = this.lsDeshacerFolio[0].strSerie;
                txtDescripcionAnt.Text = this.lsDeshacerFolio[0].strDescripcion;
                txtDescripcion.Focus();


            }
        }


        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlFolio.PrimeroPaginacion();
            if (!this.ctrlFolio.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlFolio.SiguentePaginacion();
            if (!this.ctrlFolio.bolResultado) return;
            this.llenar_formulario();

        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlFolio.AtrasPaginacion();
            if (!this.ctrlFolio.bolResultado) return;
            this.llenar_formulario();

        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlFolio.UltimoPaginacion();
            if (!this.ctrlFolio.bolResultado) return;
            this.llenar_formulario();

        }
        private void TsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();
            this.ctrlFolio.folio.limpiarProps();
            txtConsecutivo.Enabled = true;

            //txtCodigo.Enabled = false;
            if (this.ctrlFolio.bolResutladoPaginacion)
            {
                /*DataTable dtDatos = this.ctrlFolio.objDataSet.Tables[0];
                this.intUlitmoCodigo = Int32.Parse(dtDatos.Rows[dtDatos.Rows.Count - 1][1].ToString()) + 1;*/
                this.agregarDeshacer();
            }
            else
            {
               // this.intUlitmoCodigo = 1;
            }

            //txtCodigo.Text = this.intUlitmoCodigo.ToString().PadLeft(3, '0');

        }


        private void txtDescripcion_Leave(object sender, EventArgs e)
        {
            if (txtDescripcionAnt.Text.ToString() == txtDescripcion.Text.ToString()) return;
            Controller.clsCtrlFolios ctrlFolio = new Controller.clsCtrlFolios();
            ctrlFolio.folio.strDescripcion = txtDescripcion.Text;
            ctrlFolio.BuscarDescripcion();
            if (ctrlFolio.mdlFolios.bolResultado)
            {
                this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("La descripción debe ser única, favor de verificar"));
                txtDescripcion.Text = "";
                txtDescripcion.Focus();
                
            }

        }


        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {
        }

        private void TxtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);

        }

        private void txtConsecutivo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }


        private void TxtCodigo_Leave(object sender, EventArgs e)
        {
            //this.ctrlFolio.Buscar(txtCodigo.Text);
           // if (!(this.ctrlFolio.bolResultado) || txtCodigo.Text.Trim().Length == 0) return;
            this.llenar_formulario();
           // txtCodigo.Focus();
        }

        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            txtConsecutivo.Enabled = false;
            this.clsView.principal.epMensaje.Clear();
            txtDescripcion.Focus();
        }

        private bool validacion()
        {

            bool bolValidacion = true;
            //string strCodigo = txtCodigo.Text.TrimStart();
            string strDescripcion = txtDescripcion.Text.TrimStart();
            string strConsecutivo = txtConsecutivo.Text;

            if (strConsecutivo == "")
            {
                txtConsecutivo.Focus();
                this.clsView.principal.epMensaje.SetError(txtConsecutivo, this.clsView.mensajeErrorValiForm2("El campo consecutivo es obligatorio, favor de ingresar un consecutivo"));
                bolValidacion = false;
            }

            if (strDescripcion == "")
            {
                txtDescripcion.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("El campo descripción es obligatorio, favor de ingresar una descripción"));
                bolValidacion = false;
            }
            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();

            if (validacion())
            {
                //this.ctrlFolio.folio.strCodigo = this.intUlitmoCodigo.ToString().PadLeft(3, '0                
                this.ctrlFolio.folio.strDescripcion = txtDescripcion.Text.ToString();
                this.ctrlFolio.folio.intConsecutivo = int.Parse(txtConsecutivo.Text);
                this.ctrlFolio.folio.strSerie = txtSeries.Text;
                this.ctrlFolio.folio.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlFolio.Guardar();

                if (this.ctrlFolio.mdlFolios.bolResultado)
                {
                    this.limpiar_textbox();
                    this.ctrlFolio.Paginacion();
                    this.clsView.mensajeExitoDB("folios", "guardó");

                }
            }
        }

        public void llenar_formulario()
        {

            if (this.ctrlFolio.folio.intId  == 0) return;
            /*txtCodigo.Text = this.ctrlFolio.estado.strCodigo;
            this.intUlitmoCodigo = Int32.Parse(this.ctrlFolio.estado.strCodigo);*/
            this.clsView.principal.epMensaje.Clear();
            txtFolioId.Text = this.ctrlFolio.folio.intId.ToString();
            txtDescripcionAnt.Text = this.ctrlFolio.folio.strDescripcion;
            txtDescripcion.Text = this.ctrlFolio.folio.strDescripcion;
            txtSeries.Text = this.ctrlFolio.folio.strSerie;
            txtConsecutivo.Text = this.ctrlFolio.folio.intConsecutivo.ToString();
            txtConsecutivo.Enabled = false;
            this.ctrlFolio.folio.intUsuario = this.clsView.login.usuario.intId;

            txtDescripcion.Focus();

            
        }


        public void agregarDeshacer()
        {
            this.lsDeshacerFolio.RemoveAt(0);
            this.lsDeshacerFolio.Add(this.ctrlFolio.folio);
        }

        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void TsbBuscar_Click(object sender, EventArgs e)
        {
            if (this.ctrlFolio.bolResutladoPaginacion)
            {
                this.ctrlFolio.CargarFrmBusqueda();

                if (this.ctrlFolio.bolResultadoBusqueda) 
                {
                    this.llenar_formulario();
                }
                
            }
           // txtCodigo.Focus();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void txtDescripcion_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblCodigo_Click(object sender, EventArgs e)
        {

        }

        private void frmFolios_Load(object sender, EventArgs e)
        {
            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);
        }
    }
}
