﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmGramajes : Form
    {
        
        public Controller.clsCtrlGramajes ctrlGramajes;
        public Controller.clsCtrlPrendasTipo ctrlPrendasTipo;
        public Core.clsCoreView clsView;

        public int _intUlitmoCodigo;
        public int _intNumCerosConsecutivo;
        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }

        public int intNumCerosConsecutivo { get => _intNumCerosConsecutivo; set => _intNumCerosConsecutivo = value; }

        public List<Properties_Class.clsGramaje> lsDeshacerGramajaes;

        public frmGramajes()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();

            this.ctrlGramajes = new Controller.clsCtrlGramajes();
            this.ctrlGramajes.Paginacion();

            this.ctrlPrendasTipo = new Controller.clsCtrlPrendasTipo();
            this.ctrlPrendasTipo.Paginacion();

            this.lsDeshacerGramajaes = new List<Properties_Class.clsGramaje>();
            this.lsDeshacerGramajaes.Add(this.ctrlGramajes.gramaje);
            //Asignar número de ceros pata generar código consecutivo
            this.intNumCerosConsecutivo = 2;

        }


        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlGramajes.gramaje.intId == 0) return;

            if (this.clsView.confirmacionEliminar("Gramajes"))
            {
                this.ctrlGramajes.gramaje.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlGramajes.Eliminar();
                if (this.ctrlGramajes.bolResultado)
                {
                    this.ctrlGramajes.Paginacion();
                    this.limpiar_textbox();
                    this.clsView.mensajeExitoDB("gramajes", "elimino");
                }
            }
        }

        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlGramajes.bolResutladoPaginacion)
            {
                txtCodigo.Text = this.lsDeshacerGramajaes[0].strCodigo.ToString();
                txtDescripcion.Text = this.lsDeshacerGramajaes[0].strDescripcion.ToString();

            }
        }

        private void tsbBuscar_Click(object sender, EventArgs e)
        {
            Dictionary<int, Properties_Class.clsTextbox> dicFocus = new Dictionary<int, Properties_Class.clsTextbox>()
            {
                { 0,new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcion, strBusqueda = "gramajes"}
                },
                { 1,new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigo, strBusqueda = "gramajes"}
                },
                { 2,new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoTipoPrenda, strBusqueda = "tipoPrenda"}
                },
                { 3,new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcionTipoPrenda, strBusqueda = "tipoPrenda"}
                }

            };
            foreach (KeyValuePair<int, Properties_Class.clsTextbox> data in dicFocus)
            {
                if (data.Value.txtGenerico.Focused)
                {
                    if (data.Value.strBusqueda == "gramajes")
                    {
                        this.mostraBusqueda();
                    }
                    else if (data.Value.strBusqueda == "tipoPrenda")
                    {
                        this.mostraBusquedaaTipoPrenda();
                    }

                }

            }
        }


        private void BtnBusqueda_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();
        }

        private void BtnBusquedaTipoPrenda_Click(object sender, EventArgs e)
        {            
            this.mostraBusquedaaTipoPrenda();
        }


        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlGramajes.PrimeroPaginacion();
            if (!this.ctrlGramajes.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlGramajes.SiguentePaginacion();
            if (!this.ctrlGramajes.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlGramajes.AtrasPaginacion();
            if (!this.ctrlGramajes.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlGramajes.UltimoPaginacion();
            if (!this.ctrlGramajes.bolResultado) return;
            this.llenar_formulario();
        }
        private void TsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();
            this.ctrlGramajes.gramaje.limpiarProps();
            txtCodigo.Enabled = false;
            if (this.ctrlGramajes.bolResutladoPaginacion)
            {
                DataTable dtDatos = this.ctrlGramajes.objDataSet.Tables[0];
                this.intUlitmoCodigo = Int32.Parse(dtDatos.Rows[dtDatos.Rows.Count - 1][1].ToString()) +1 ;
                this.agregarDeshacer();
            }
            else
            {
                this.intUlitmoCodigo = 1;
            }

            txtCodigo.Text = this.intUlitmoCodigo.ToString().PadLeft(this.intNumCerosConsecutivo, '0');

        }
        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {
            //txtDescripcion.Text = "";
        }

        private void TxtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);

        }

        private void txtCodigoTipoPrenda_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }
        private void txtCodigo_Leave(object sender, EventArgs e)
        {
            this.ctrlGramajes.Buscar(txtCodigo.Text);
            if (!(this.ctrlGramajes.bolResultado) || txtCodigo.Text.Trim().Length == 0) return;
            this.llenar_formulario();
        }

        private void txtDescripcion_Leave(object sender, EventArgs e)
        {
            if (txtDescripcion.Text != txtDescripcionAnt.Text)
            {
                this.ctrlGramajes.ValidarDescripcion(txtDescripcion.Text);
                if (this.ctrlGramajes.bolResultado)
                {
                    this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("La descripción debe ser única, favor de verificar"));
                    txtDescripcion.Text = "";
                }
            }
        }

        private void txtCodigoTipoPrenda_Leave(object sender, EventArgs e)
        {
            

            this.ctrlPrendasTipo.Buscar(txtCodigoTipoPrenda.Text);
            if (this.ctrlPrendasTipo.bolResultado)
            {
                txtTipoPrendaId.Text = this.ctrlPrendasTipo.prendasTipo.intId.ToString();
                txtCodigoTipoPrenda.Text = this.ctrlPrendasTipo.prendasTipo.strCodigo;
                txtDescripcionTipoPrenda.Text = this.ctrlPrendasTipo.prendasTipo.strDescripcion;
            }

        }

        private void TxtCodigoEstado_Leave(object sender, EventArgs e)
        {
            this.ctrlPrendasTipo.Buscar(txtCodigoTipoPrenda.Text);
            if (!(this.ctrlPrendasTipo.bolResultado) || txtCodigo.Text.Trim().Length == 0) return;
            txtTipoPrendaId.Text = this.ctrlPrendasTipo.prendasTipo.intId.ToString();
            txtCodigoTipoPrenda.Text = this.ctrlPrendasTipo.prendasTipo.strCodigo;
            txtDescripcionTipoPrenda.Text = this.ctrlPrendasTipo.prendasTipo.strDescripcion;
        }

        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            txtCodigo.Enabled = true;
            txtDescripcionAnt.Text = "";
        }

        private bool validacion()
        {

            bool bolValidacion = true;
            string strCodigo = txtCodigo.Text.TrimStart();
            string strDescripcion = txtDescripcion.Text.TrimStart();
            string strDescripcionTipoPrenda = txtDescripcionTipoPrenda.Text.TrimStart();

            if (strCodigo == "")
            {
                txtCodigo.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigo, this.clsView.mensajeErrorValiForm2("El campo código es obligatorio, favor de ingresar un código "));
                bolValidacion = false;
            }

            if (strDescripcion == "")
            {
                txtDescripcion.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("El campo descripción es obligatorio, favor de ingresar una descripción "));
                bolValidacion = false;
            }

            if (strDescripcionTipoPrenda == "")
            {
                txtCodigoTipoPrenda.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigoTipoPrenda, this.clsView.mensajeErrorValiForm2("El campo tipo de prenda es obligatorio, favor de ingresar un tipo de prenda "));
                bolValidacion = false;
            }

            
            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();

            if (validacion())
            {
                this.ctrlGramajes.gramaje.strCodigo = this.intUlitmoCodigo.ToString().PadLeft(this.intNumCerosConsecutivo, '0');
                this.ctrlGramajes.gramaje.strDescripcion = txtDescripcion.Text.ToString();
                this.ctrlGramajes.gramaje.prendasTipo.intId = Int32.Parse((txtTipoPrendaId.Text == "") ? "0" : txtTipoPrendaId.Text);
                this.ctrlGramajes.gramaje.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlGramajes.Guardar();

                if (this.ctrlGramajes.mdlGramajes.bolResultado)
                {
                    this.limpiar_textbox();
                    this.ctrlGramajes.Paginacion();
                    this.clsView.mensajeExitoDB("municipio", "guardó");

                }
            }
        }

      

       public void mostraBusqueda()
        {
            if (this.ctrlGramajes.bolResutladoPaginacion)
            {
                this.ctrlGramajes.CargarFrmBusqueda();

                if (this.ctrlGramajes.bolResultadoBusqueda)
                {
                    this.llenar_formulario();
                    txtCodigo.Focus();
                }
            }
            
        }

        private void mostraBusquedaaTipoPrenda()
        {

            this.ctrlPrendasTipo.Paginacion();
            if (this.ctrlPrendasTipo.bolResutladoPaginacion)
            {
                this.ctrlPrendasTipo.CargarFrmBusqueda();

                if (this.ctrlPrendasTipo.bolResultadoBusqueda)
                {
                    txtTipoPrendaId.Text = this.ctrlPrendasTipo.prendasTipo.intId.ToString();
                    txtCodigoTipoPrenda.Text = this.ctrlPrendasTipo.prendasTipo.strCodigo;
                    txtDescripcionTipoPrenda.Text = this.ctrlPrendasTipo.prendasTipo.strDescripcion;
                }
            }

            //txtCodigoTipoPrenda.Focus();
        }

        public void llenar_formulario()
        {

            if (this.ctrlGramajes.gramaje.strCodigo.Length == 0) return;
            txtCodigo.Text = this.ctrlGramajes.gramaje.strCodigo;
            txtCodigo.Enabled = true;
            this.intUlitmoCodigo = Int32.Parse(this.ctrlGramajes.gramaje.strCodigo);
            txtDescripcion.Text = this.ctrlGramajes.gramaje.strDescripcion;
            txtDescripcionAnt.Text = this.ctrlGramajes.gramaje.strDescripcion;
            txtTipoPrendaId.Text = this.ctrlGramajes.gramaje.prendasTipo.intId.ToString();
            txtCodigoTipoPrenda.Text = this.ctrlGramajes.gramaje.prendasTipo.strCodigo;
            txtDescripcionTipoPrenda.Text = this.ctrlGramajes.gramaje.prendasTipo.strDescripcion;
        }

        public void agregarDeshacer()
        {
            if (this.ctrlGramajes.bolResutladoPaginacion)
            {
                this.lsDeshacerGramajaes.RemoveAt(0);
                this.lsDeshacerGramajaes.Add(this.ctrlGramajes.gramaje);
            }
        }

        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void FrmMunicipios_Load(object sender, EventArgs e)
        {

        }

        private void frmGramajes_Load(object sender, EventArgs e)
        {
            txtCodigo.MaxLength = this.intNumCerosConsecutivo;

            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);
        }


    }
}