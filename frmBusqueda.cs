﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmBusqueda : Form
    {

        public DataSet _objDataSet;

        public string _strTipoFormulario;
        public string _strQuery;
        public List<string> _lsBloquearCol;
        public DataGridViewRow _rowsIndex;        
        public bool _bolCierra;        
        public DataSet objDataSet { get => _objDataSet; set => _objDataSet = value; }
       
        public string strTipoFormulario { get => _strTipoFormulario; set => _strTipoFormulario = value; }

        public string strQuery { get => _strQuery; set => _strQuery = value; }

        public bool bolCierra { get => _bolCierra; set => _bolCierra = value; }

        public Dictionary<string, string> dicTipoFormulario = new Dictionary<string, string>()
        {
            {"Simple", "Simple"},
            {"Filtro", "Filtro"},
        };

        public Dictionary<string, string> dicReemplazo = new Dictionary<string, string>()
        {
            {"texto", "@txt"},
            {"entero", "@int"},
            {"decimal", "@float"},
            {"fecha", "@datetime"}
            
        };

        public List<string> lsBloquearCol { get => _lsBloquearCol; set => _lsBloquearCol = value; }
        
        
        public DataGridViewRow rowsIndex { get => _rowsIndex; set => _rowsIndex = value; }

        public Core.clsCoreView clsView = new Core.clsCoreView();

        public frmBusqueda()
        {
            InitializeComponent();
            this.bolCierra = false;
        }
        private void FrmBusqueda_Load(object sender, EventArgs e)
        {

             txtDescripcion.Text = "";
             this.filtro();
             dgvResultado.AutoGenerateColumns = true;
                       
             dgvResultado.DataSource = this.objDataSet.Tables[0].DefaultView; // dataset                    
             this.bloquearColumnasGrid();
             dgvResultado.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
             dgvResultado.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dgvResultado.AllowUserToAddRows = false;
        }



        private void BtnBusqueda_Click(object sender, EventArgs e)
        {
            this.filtro();
        }

        private void DgvResultado_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //dgvResultado.Rows[e.RowIndex]

        }

        private void DgvResultado_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvResultado.CurrentRow.Selected = true;
            this.rowsIndex = dgvResultado.SelectedRows[0];
            this.bolCierra = true;
            this.Close();
            
        }

        private void TxtDescripcion_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.filtro();
            }
        }

        public void bloquearColumnasGrid()
        {            
            this.lsBloquearCol.ForEach(delegate (string col)
            {
                dgvResultado.Columns[col].Visible = false;                
            });
        }

        public void filtro()
        {


            string query  = this.strQuery.Replace(this.dicReemplazo["texto"], txtDescripcion.Text.Trim().ToString());
            this.objDataSet.Tables[0].DefaultView.RowFilter = query;

            /*this.strQuery = this.strQuery.Replace(this.dicReemplazo["texto"], txtDescripcion.Text.Trim().ToString());
            this.objDataSet.Tables[0].DefaultView.RowFilter = this.strQuery;*/

        }

        private void FrmBusqueda_FormClosing(object sender, FormClosingEventArgs e)
        {
            /*txtDescripcion.Text = "";
            this.filtro();*/
        }

        private void frmBusqueda_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.clsView.cierraFormulariosESC(this, this.Text.ToLower());
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        /*protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.clsView.cierraFormulariosESC(this, "tipos asentamientos");
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }*/



    }
}
