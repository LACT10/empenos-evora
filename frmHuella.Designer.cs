﻿namespace EmpenosEvora
{
    partial class frmHuella
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ptbHuella = new System.Windows.Forms.PictureBox();
            this.bntCapture = new System.Windows.Forms.Button();
            this.lblNumeroVeces = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ptbHuella)).BeginInit();
            this.SuspendLayout();
            // 
            // ptbHuella
            // 
            this.ptbHuella.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ptbHuella.Location = new System.Drawing.Point(8, 8);
            this.ptbHuella.Name = "ptbHuella";
            this.ptbHuella.Size = new System.Drawing.Size(304, 168);
            this.ptbHuella.TabIndex = 1;
            this.ptbHuella.TabStop = false;
            this.ptbHuella.Click += new System.EventHandler(this.ptbHuella_Click);
            // 
            // bntCapture
            // 
            this.bntCapture.Location = new System.Drawing.Point(240, 192);
            this.bntCapture.Name = "bntCapture";
            this.bntCapture.Size = new System.Drawing.Size(75, 23);
            this.bntCapture.TabIndex = 2;
            this.bntCapture.Text = "Guardar";
            this.bntCapture.UseVisualStyleBackColor = true;
            this.bntCapture.Click += new System.EventHandler(this.bntCapture_Click);
            // 
            // lblNumeroVeces
            // 
            this.lblNumeroVeces.AutoSize = true;
            this.lblNumeroVeces.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroVeces.Location = new System.Drawing.Point(8, 192);
            this.lblNumeroVeces.Name = "lblNumeroVeces";
            this.lblNumeroVeces.Size = new System.Drawing.Size(144, 13);
            this.lblNumeroVeces.TabIndex = 21;
            this.lblNumeroVeces.Text = "Necesitas pasar el dedo";
            // 
            // frmHuella
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(320, 224);
            this.Controls.Add(this.lblNumeroVeces);
            this.Controls.Add(this.bntCapture);
            this.Controls.Add(this.ptbHuella);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmHuella";
            this.Text = "Captura de huella";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmHuella_FormClosed);
            this.Load += new System.EventHandler(this.frmHuella_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ptbHuella)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox ptbHuella;
        private System.Windows.Forms.Button bntCapture;
        private System.Windows.Forms.Label lblNumeroVeces;
    }
}