﻿namespace EmpenosEvora
{
    partial class frmIdentificacionImagen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bntCaptureFrontal = new System.Windows.Forms.Button();
            this.imgVideoFrontal = new System.Windows.Forms.PictureBox();
            this.btnGuardarTrasera = new System.Windows.Forms.Button();
            this.bntCaptureTrasera = new System.Windows.Forms.Button();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.imgVideoTrasera = new System.Windows.Forms.PictureBox();
            this.imgCaptureFrontal = new System.Windows.Forms.PictureBox();
            this.imgCaptureTrasera = new System.Windows.Forms.PictureBox();
            this.btnIniciarFrontal = new System.Windows.Forms.Button();
            this.btnIniciarTrasera = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.imgVideoFrontal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVideoTrasera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCaptureFrontal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCaptureTrasera)).BeginInit();
            this.SuspendLayout();
            // 
            // bntCaptureFrontal
            // 
            this.bntCaptureFrontal.Location = new System.Drawing.Point(88, 192);
            this.bntCaptureFrontal.Name = "bntCaptureFrontal";
            this.bntCaptureFrontal.Size = new System.Drawing.Size(75, 23);
            this.bntCaptureFrontal.TabIndex = 3;
            this.bntCaptureFrontal.Text = "Tomar foto";
            this.bntCaptureFrontal.UseVisualStyleBackColor = true;
            this.bntCaptureFrontal.Click += new System.EventHandler(this.bntCaptureFrontal_Click);
            // 
            // imgVideoFrontal
            // 
            this.imgVideoFrontal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.imgVideoFrontal.Location = new System.Drawing.Point(8, 40);
            this.imgVideoFrontal.Name = "imgVideoFrontal";
            this.imgVideoFrontal.Size = new System.Drawing.Size(256, 144);
            this.imgVideoFrontal.TabIndex = 2;
            this.imgVideoFrontal.TabStop = false;
            // 
            // btnGuardarTrasera
            // 
            this.btnGuardarTrasera.Location = new System.Drawing.Point(472, 408);
            this.btnGuardarTrasera.Name = "btnGuardarTrasera";
            this.btnGuardarTrasera.Size = new System.Drawing.Size(75, 23);
            this.btnGuardarTrasera.TabIndex = 7;
            this.btnGuardarTrasera.Text = "Guardar";
            this.btnGuardarTrasera.UseVisualStyleBackColor = true;
            this.btnGuardarTrasera.Click += new System.EventHandler(this.btnGuardarTrasera_Click);
            // 
            // bntCaptureTrasera
            // 
            this.bntCaptureTrasera.Location = new System.Drawing.Point(88, 408);
            this.bntCaptureTrasera.Name = "bntCaptureTrasera";
            this.bntCaptureTrasera.Size = new System.Drawing.Size(75, 23);
            this.bntCaptureTrasera.TabIndex = 6;
            this.bntCaptureTrasera.Text = "Tomar foto";
            this.bntCaptureTrasera.UseVisualStyleBackColor = true;
            this.bntCaptureTrasera.Click += new System.EventHandler(this.bntCaptureTrasera_Click);
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(8, 16);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(102, 13);
            this.lblCodigo.TabIndex = 45;
            this.lblCodigo.Text = "Identificación frontal";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 232);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 46;
            this.label1.Text = "Identificación trasera";
            // 
            // imgVideoTrasera
            // 
            this.imgVideoTrasera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.imgVideoTrasera.Location = new System.Drawing.Point(8, 256);
            this.imgVideoTrasera.Name = "imgVideoTrasera";
            this.imgVideoTrasera.Size = new System.Drawing.Size(256, 144);
            this.imgVideoTrasera.TabIndex = 47;
            this.imgVideoTrasera.TabStop = false;
            // 
            // imgCaptureFrontal
            // 
            this.imgCaptureFrontal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.imgCaptureFrontal.Location = new System.Drawing.Point(288, 40);
            this.imgCaptureFrontal.Name = "imgCaptureFrontal";
            this.imgCaptureFrontal.Size = new System.Drawing.Size(256, 144);
            this.imgCaptureFrontal.TabIndex = 48;
            this.imgCaptureFrontal.TabStop = false;
            // 
            // imgCaptureTrasera
            // 
            this.imgCaptureTrasera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.imgCaptureTrasera.Location = new System.Drawing.Point(288, 256);
            this.imgCaptureTrasera.Name = "imgCaptureTrasera";
            this.imgCaptureTrasera.Size = new System.Drawing.Size(256, 144);
            this.imgCaptureTrasera.TabIndex = 49;
            this.imgCaptureTrasera.TabStop = false;
            // 
            // btnIniciarFrontal
            // 
            this.btnIniciarFrontal.Location = new System.Drawing.Point(8, 192);
            this.btnIniciarFrontal.Name = "btnIniciarFrontal";
            this.btnIniciarFrontal.Size = new System.Drawing.Size(75, 23);
            this.btnIniciarFrontal.TabIndex = 50;
            this.btnIniciarFrontal.Text = "Iniciar";
            this.btnIniciarFrontal.UseVisualStyleBackColor = true;
            this.btnIniciarFrontal.Click += new System.EventHandler(this.btnIniciarFrontal_Click);
            // 
            // btnIniciarTrasera
            // 
            this.btnIniciarTrasera.Location = new System.Drawing.Point(8, 408);
            this.btnIniciarTrasera.Name = "btnIniciarTrasera";
            this.btnIniciarTrasera.Size = new System.Drawing.Size(75, 23);
            this.btnIniciarTrasera.TabIndex = 51;
            this.btnIniciarTrasera.Text = "Iniciar";
            this.btnIniciarTrasera.UseVisualStyleBackColor = true;
            this.btnIniciarTrasera.Click += new System.EventHandler(this.btnIniciarTrasera_Click);
            // 
            // frmIdentificacionImagen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(557, 439);
            this.Controls.Add(this.btnIniciarTrasera);
            this.Controls.Add(this.btnIniciarFrontal);
            this.Controls.Add(this.imgCaptureTrasera);
            this.Controls.Add(this.imgCaptureFrontal);
            this.Controls.Add(this.imgVideoTrasera);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.btnGuardarTrasera);
            this.Controls.Add(this.bntCaptureTrasera);
            this.Controls.Add(this.bntCaptureFrontal);
            this.Controls.Add(this.imgVideoFrontal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmIdentificacionImagen";
            this.Text = "Identificación";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmIdentificacionImagen_FormClosing);
            this.Load += new System.EventHandler(this.frmIdentificacionImagen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imgVideoFrontal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVideoTrasera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCaptureFrontal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCaptureTrasera)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bntCaptureFrontal;
        private System.Windows.Forms.PictureBox imgVideoFrontal;
        private System.Windows.Forms.Button btnGuardarTrasera;
        private System.Windows.Forms.Button bntCaptureTrasera;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox imgVideoTrasera;
        private System.Windows.Forms.PictureBox imgCaptureFrontal;
        private System.Windows.Forms.PictureBox imgCaptureTrasera;
        private System.Windows.Forms.Button btnIniciarFrontal;
        private System.Windows.Forms.Button btnIniciarTrasera;
    }
}