﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmTiposSubPrendas : Form
    {                
        public Controller.clsCtrlPrendasSubtipos ctrlPrendasSubtipos;
        public Controller.clsCtrlPrendasTipo ctrlPrendasTipo;
        public Core.clsCoreView clsView;

        public int _intUlitmoCodigo;
        public int _intNumCerosConsecutivo;
        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }
        public int intNumCerosConsecutivo { get => _intNumCerosConsecutivo; set => _intNumCerosConsecutivo = value; }

        public List<Properties_Class.clsPrendasSubtipo> lsDeshacerPrendasSubtipo;

        

        public frmTiposSubPrendas()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();
            this.ctrlPrendasSubtipos = new Controller.clsCtrlPrendasSubtipos();
            this.ctrlPrendasSubtipos.Paginacion();

            this.ctrlPrendasTipo = new Controller.clsCtrlPrendasTipo();
            this.ctrlPrendasTipo.Paginacion();

            this.lsDeshacerPrendasSubtipo = new List<Properties_Class.clsPrendasSubtipo>();
            this.lsDeshacerPrendasSubtipo.Add(this.ctrlPrendasSubtipos.prendasSubtipos);
            //Asignar número de ceros pata generar código consecutivo
            this.intNumCerosConsecutivo = 3;

        }


        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlPrendasSubtipos.prendasSubtipos.intId == 0) return;
            if (this.clsView.confirmacionEliminar("Tipo de subprenda"))
            {
                this.ctrlPrendasSubtipos.Eliminar();
                if (this.ctrlPrendasSubtipos.bolResultado)
                {
                    this.ctrlPrendasSubtipos.Paginacion();
                    this.limpiar_textbox();
                    this.clsView.mensajeExitoDB("tipo de subprenda", "elimino");
                }
            }
        }

        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlPrendasSubtipos.bolResutladoPaginacion)
            {
                txtCodigo.Text = this.lsDeshacerPrendasSubtipo[0].strCodigo.ToString();
                txtDescripcion.Text = this.lsDeshacerPrendasSubtipo[0].strDescripcion.ToString();
            }
        }

        private void TsbBuscar_Click(object sender, EventArgs e)
        {


            Dictionary<int, Properties_Class.clsTextbox> dicFocus = new Dictionary<int, Properties_Class.clsTextbox>()
            {
                { 0, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcion, strBusqueda = "subTiposPrendas"}
                },
                { 1, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigo, strBusqueda = "subTiposPrendas"}
                },
                { 2, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoPrendaTipo, strBusqueda = "tipoPrenda"}
                },
                { 3, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcionPrendaTipo, strBusqueda = "tipoPrenda"}
                }

            };
            foreach (KeyValuePair<int, Properties_Class.clsTextbox> data in dicFocus)
            {
                if (data.Value.txtGenerico.Focused)
                {
                    if (data.Value.strBusqueda == "subTiposPrendas")
                    {
                        this.mostraBusqueda();
                    }
                    else if (data.Value.strBusqueda == "tipoPrenda")
                    {
                        this.mostraBusquedaTipoPrenda();
                    }
                }
            }
        }


        private void BtnBusqueda_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();
        }      

        private void BtnBusquedaEstado_Click(object sender, EventArgs e)
        {

            this.mostraBusquedaTipoPrenda();
        }


        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlPrendasSubtipos.PrimeroPaginacion();
            if (!this.ctrlPrendasSubtipos.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlPrendasSubtipos.SiguentePaginacion();
            if (!this.ctrlPrendasSubtipos.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlPrendasSubtipos.AtrasPaginacion();
            if (!this.ctrlPrendasSubtipos.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlPrendasSubtipos.UltimoPaginacion();
            if (!this.ctrlPrendasSubtipos.bolResultado) return;
            this.llenar_formulario();
        }
        private void TsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();
            this.ctrlPrendasSubtipos.prendasSubtipos.limpiarProps();
            txtCodigo.Enabled = false;
            if (this.ctrlPrendasSubtipos.bolResutladoPaginacion)
            {
                DataTable dtDatos = this.ctrlPrendasSubtipos.objDataSet.Tables[0];
                this.intUlitmoCodigo = Int32.Parse(dtDatos.Rows[dtDatos.Rows.Count - 1][1].ToString()) + 1;
                this.agregarDeshacer();
            }
            else
            {
                this.intUlitmoCodigo = 1;
            }

            txtCodigo.Text = this.intUlitmoCodigo.ToString().PadLeft(this.intNumCerosConsecutivo, '0');

        }
        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {
            //txtDescripcion.Text = "";
        }

        private void TxtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);

        }

        private void txtCodigoPrendaTipo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }
        private void TxtCodigo_Leave(object sender, EventArgs e)
        {
            this.ctrlPrendasSubtipos.Buscar(txtCodigo.Text);
            if (!(this.ctrlPrendasSubtipos.bolResultado) || txtCodigo.Text.Trim().Length == 0) return;
            this.llenar_formulario();
        }

        private void TxtCodigoPrendaTipo_Leave(object sender, EventArgs e)
        {
            this.ctrlPrendasTipo.Buscar(txtCodigoPrendaTipo.Text);
            if (txtCodigo.Text.Trim().Length == 0) return;
            txPrendaTipotId.Text = this.ctrlPrendasTipo.prendasTipo.intId.ToString();
            txtCodigoPrendaTipo.Text = this.ctrlPrendasTipo.prendasTipo.strCodigo;
            txtDescripcionPrendaTipo.Text = this.ctrlPrendasTipo.prendasTipo.strDescripcion;
        }

        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            txtCodigo.Enabled = true;
        }

        private bool validacion()
        {

            bool bolValidacion = true;
            string strCodigo = txtCodigo.Text.TrimStart();
            string strDescripcion = txtDescripcion.Text.TrimStart();

            if (strCodigo == "")
            {
                txtCodigo.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigo, this.clsView.mensajeErrorValiForm2("El campo código es obligatorio, favor de ingresar un código "));
                bolValidacion = false;
            }

            if (strDescripcion == "")
            {
                txtDescripcion.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("El campo descripción es obligatorio, favor de ingresar una descripción "));
                bolValidacion = false;
            }
            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();

            if (validacion())
            {
                this.ctrlPrendasSubtipos.prendasSubtipos.strCodigo = this.intUlitmoCodigo.ToString().PadLeft(this.intNumCerosConsecutivo, '0'); ;
                this.ctrlPrendasSubtipos.prendasSubtipos.strDescripcion = txtDescripcion.Text.ToString();
                this.ctrlPrendasSubtipos.prendasSubtipos.prendasTipo.intId = Int32.Parse((txPrendaTipotId.Text == "") ? "0" : txPrendaTipotId.Text);
                this.ctrlPrendasSubtipos.prendasSubtipos.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlPrendasSubtipos.Guardar();

                if (this.ctrlPrendasSubtipos.mdlPrendasSubtipos.bolResultado)
                {
                    this.limpiar_textbox();
                    this.ctrlPrendasSubtipos.Paginacion();
                    this.clsView.mensajeExitoDB("tipo de subprenda", "guardó");

                }
            }
        }

        public void mostraBusqueda()
        {
            if (this.ctrlPrendasSubtipos.bolResutladoPaginacion)
            {
                this.ctrlPrendasSubtipos.CargarFrmBusqueda();

                if (this.ctrlPrendasSubtipos.bolResultadoBusqueda)
                {
                    this.llenar_formulario();
                    txtCodigo.Focus();
                }
            }
            
        }

        public void mostraBusquedaTipoPrenda()
        {
            this.ctrlPrendasTipo.Paginacion();
            if (this.ctrlPrendasTipo.bolResutladoPaginacion)
            {
                this.ctrlPrendasTipo.CargarFrmBusqueda();

                if (this.ctrlPrendasTipo.bolResultadoBusqueda)
                {
                    txPrendaTipotId.Text = this.ctrlPrendasTipo.prendasTipo.intId.ToString();
                    txtCodigoPrendaTipo.Text = this.ctrlPrendasTipo.prendasTipo.strCodigo;
                    txtDescripcionPrendaTipo.Text = this.ctrlPrendasTipo.prendasTipo.strDescripcion;
                    txtCodigoPrendaTipo.Focus();
                }
            }
            
        }

        public void llenar_formulario()
        {
            if (this.ctrlPrendasSubtipos.prendasSubtipos.strCodigo.Length == 0) return;
            txtCodigo.Text = this.ctrlPrendasSubtipos.prendasSubtipos.strCodigo;
            txtDescripcion.Text = this.ctrlPrendasSubtipos.prendasSubtipos.strDescripcion;
            this.intUlitmoCodigo = Int32.Parse(this.ctrlPrendasSubtipos.prendasSubtipos.strCodigo);
            txPrendaTipotId.Text = this.ctrlPrendasSubtipos.prendasSubtipos.prendasTipo.intId.ToString();
            txtCodigoPrendaTipo.Text = this.ctrlPrendasSubtipos.prendasSubtipos.prendasTipo.strCodigo;
            txtDescripcionPrendaTipo.Text = this.ctrlPrendasSubtipos.prendasSubtipos.prendasTipo.strDescripcion;
        }

        public void agregarDeshacer()
        {
            if (this.ctrlPrendasSubtipos.bolResutladoPaginacion)
            {
                this.lsDeshacerPrendasSubtipo.RemoveAt(0);
                this.lsDeshacerPrendasSubtipo.Add(this.ctrlPrendasSubtipos.prendasSubtipos);
            }
        }


        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void FrmTiposSubPrendas_Load(object sender, EventArgs e)
        {
            txtCodigo.MaxLength = this.intNumCerosConsecutivo;
            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);

        }

    }
}