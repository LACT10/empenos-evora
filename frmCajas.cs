﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmCajas : Form
    {
        public Controller.clsCtrlCaja ctrlCajas;
        public Core.clsCoreView clsView;

        public int _intUlitmoCodigo;
        public int _intNumCerosConsecutivo;
        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }
        public int intNumCerosConsecutivo { get => _intNumCerosConsecutivo; set => _intNumCerosConsecutivo = value; }
        
        public List<Properties_Class.clsCaja> lsDeshacerCaja;
        
        public frmCajas()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();
            this.ctrlCajas = new Controller.clsCtrlCaja();
            this.ctrlCajas.Paginacion();
            this.lsDeshacerCaja = new List<Properties_Class.clsCaja>();
            this.lsDeshacerCaja.Add(this.ctrlCajas.caja);
            //Asignar número de ceros pata generar código consecutivo
            this.intNumCerosConsecutivo = 2;

        }
        private void Frmcajas_Load(object sender, EventArgs e)
        {
        }

        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlCajas.caja.intId == 0) return;
            if (this.clsView.confirmacionEliminar("Caja"))
            {
                this.ctrlCajas.Eliminar();
                if (this.ctrlCajas.bolResultado)
                {
                    this.ctrlCajas.Paginacion();
                    this.limpiar_textbox();
                    this.clsView.mensajeExitoDB("caja", "elimino");
                }
            }
        }

        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlCajas.bolResutladoPaginacion)
            {
                this.clsView.principal.epMensaje.Clear();
                txtCodigo.Enabled = true;
                txtCodigo.Text = this.lsDeshacerCaja[0].strCodigo.ToString();
                txtDescripcion.Text = this.lsDeshacerCaja[0].strDescripcion.ToString();
                txtDescripcionAnt.Text = this.lsDeshacerCaja[0].strDescripcion.ToString();
                txtCodigo.Focus();
            }
        }

        private void tsbBuscar_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();

        }

        private void BtnBusqueda_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();
        }


        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlCajas.PrimeroPaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlCajas.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlCajas.SiguentePaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlCajas.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlCajas.AtrasPaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlCajas.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlCajas.UltimoPaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlCajas.bolResultado) return;
            this.llenar_formulario();
        }
        private void TsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();
            this.ctrlCajas.caja.limpiarProps();            
            txtCodigo.Enabled = false;
            if (this.ctrlCajas.bolResutladoPaginacion)
            {
                DataTable dtDatos = this.ctrlCajas.objDataSet.Tables[0];
                this.intUlitmoCodigo = Int32.Parse(dtDatos.Rows[dtDatos.Rows.Count - 1][1].ToString()) + 1;
                this.agregarDeshacer();
            }
            else
            {
                this.intUlitmoCodigo = 1;
            }

            txtCodigo.Text = this.intUlitmoCodigo.ToString().PadLeft(this.intNumCerosConsecutivo, '0');
            txtDescripcion.Focus();

        }
        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {
            txtDescripcion.Text = "";
        }

        private void TxtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);

        }
        private void TxtCodigo_Leave(object sender, EventArgs e)
        {
            this.ctrlCajas.Buscar(txtCodigo.Text);                        
            if (!(this.ctrlCajas.bolResultado) || txtCodigo.Text.Trim().Length == 0) return;
            this.llenar_formulario();
        }

        private void txtDescripcion_Leave(object sender, EventArgs e)
        {
            if (txtDescripcion.Text != txtDescripcionAnt.Text)
            {
                this.ctrlCajas.ValidarDescripcion(txtDescripcion.Text);
                if (this.ctrlCajas.bolResultado)
                {
                    this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("La descripción debe ser única, favor de verificar"));
                    txtDescripcion.Text = "";
                }
            }
        }

        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            this.clsView.principal.epMensaje.Clear();
            txtCodigo.Enabled = true;
            txtCodigo.Focus();
        }

        private bool validacion()
        {

            bool bolValidacion = true;
            string strCodigo = txtCodigo.Text.TrimStart();
            string strDescripcion = txtDescripcion.Text.TrimStart();

            if (strCodigo == "")
            {
                txtCodigo.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigo, this.clsView.mensajeErrorValiForm2("El campo código es obligatorio, favor de ingresar un código "));
                bolValidacion = false;
            }

            if (strDescripcion == "")
            {
                txtDescripcion.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcion,  this.clsView.mensajeErrorValiForm2("El campo descripción es obligatorio, favor de ingresar una descripción "));
                bolValidacion = false;
            }
            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();
            
            if (validacion())
            {
                this.ctrlCajas.caja.strCodigo = this.intUlitmoCodigo.ToString().PadLeft(this.intNumCerosConsecutivo, '0');
                this.ctrlCajas.caja.strDescripcion = txtDescripcion.Text.ToString();
                this.ctrlCajas.caja.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlCajas.Guardar();

                if (this.ctrlCajas.mdlCaja.bolResultado)
                {
                    this.limpiar_textbox();
                    this.ctrlCajas.Paginacion();
                    this.clsView.mensajeExitoDB("caja", "guardó");
                    txtCodigo.Focus();

                }
            }
        }

        private void mostraBusqueda()
        {
            if (this.ctrlCajas.bolResutladoPaginacion)
            {
                this.ctrlCajas.CargarFrmBusqueda();

                if (this.ctrlCajas.bolResultadoBusqueda)
                {
                    this.llenar_formulario();
                    txtCodigo.Focus();
                }
            }

            
        }

        public void llenar_formulario()
        {

            if (this.ctrlCajas.caja.strCodigo.Length == 0) return;
            this.clsView.principal.epMensaje.Clear();
            txtCodigo.Text = this.ctrlCajas.caja.strCodigo;
            txtCodigo.Enabled = true;
            this.intUlitmoCodigo = Int32.Parse(this.ctrlCajas.caja.strCodigo);            
            txtDescripcion.Text = this.ctrlCajas.caja.strDescripcion;
            txtDescripcionAnt.Text = this.ctrlCajas.caja.strDescripcion;
        }

        public void agregarDeshacer()
        {
            if (this.ctrlCajas.bolResutladoPaginacion)
            {
                this.lsDeshacerCaja.RemoveAt(0);
                this.lsDeshacerCaja.Add(this.ctrlCajas.caja);
            }
        }

        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void ToolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void FrmCajas_Load_1(object sender, EventArgs e)
        {
            txtCodigo.MaxLength = this.intNumCerosConsecutivo;

            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);
        }


    }
}