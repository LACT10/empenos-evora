﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmMovimientosCaja : Form
    {

        public Controller.clsCtrlMovimientosCaja ctrlMovimientosCaja;
        //public Controller.clsCtrlMovimientosCajaTipos ctrlMovimientosCajaTipos;
        public Controller.clsCtrlFoliosProcesos ctrlFoliosProcesos;
        public Controller.clsCtrlSucursales ctrlSucursales;
        public Controller.clsCtrlMoviemientosCajaDetalles ctrlMoviemientosCajaDetalles;
        public Core.clsCoreView clsView;
        public Controller.clsCtrlFolios ctrlFoliosAuto = new Controller.clsCtrlFolios();

        public int _intUlitmoCodigo;        
        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }

        public int intRows = 10;

        public List<Properties_Class.clsMovimientosCaja> lsDeshacerMovimientosCaja;

        public List<string> intListEliminar;

        public int intNumCerosConsecutivo = 10;
        public frmMovimientosCaja()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();

            this.ctrlMovimientosCaja = new Controller.clsCtrlMovimientosCaja();
                        
            this.ctrlFoliosProcesos = new Controller.clsCtrlFoliosProcesos();
            this.ctrlMoviemientosCajaDetalles = new Controller.clsCtrlMoviemientosCajaDetalles();

            this.lsDeshacerMovimientosCaja = new List<Properties_Class.clsMovimientosCaja>();
            this.lsDeshacerMovimientosCaja.Add(this.ctrlMovimientosCaja.movimientosCaja);

        }

        private void frmMovimientosCaja_Load(object sender, EventArgs e)
        {
            txtFolio.MaxLength = intNumCerosConsecutivo;
            this.Text = this.Text + " " + "[Caja: " + this.clsView.login.caja.strDescripcion + "]";

            this.ctrlFoliosAuto.BuscarFolioProceso(this.clsView.strProcesosId, this.clsView.login.intSucursalId.ToString());

            this.ctrlMovimientosCaja.Paginacion(this.clsView.login.intSucursalId, this.clsView.login.caja.intId);

            this.ctrlSucursales = new Controller.clsCtrlSucursales();
            this.ctrlSucursales.PaginacionOtrasSuc(this.clsView.login.intSucursalId, this.clsView.login.intEmpresaId);
            this.dgvMovimientosCajaDetalles();

            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);


            if (this.clsView.login.usuario.validarMovimientos() && this.clsView.bolPermisosEliminar)
            {
                tsbEliminar.Enabled = true;
            }
            else
            {
                tsbEliminar.Enabled = false;
            }
        }

        private void tsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void btnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void btnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBusqueda_Click(object sender, EventArgs e)
        {

            if (this.ctrlMovimientosCaja.bolResutladoPaginacion)
            {
                txtFolio.Enabled = true;
                this.ctrlMovimientosCaja.CargarFrmBusqueda();

                if (this.ctrlMovimientosCaja.bolResultadoBusqueda)
                {
                    this.llenar_formulario();
                }
                
            }
        }
        
        private void btnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlMovimientosCaja.movimientosCaja.intId > 0)
            {
                llenar_formulario();
            }
            else
            {
                txtFolio.Enabled = true;
                this.limpiar_textbox();
            }
            txtFolio.Focus();
        }


        private void btnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            txtFolio.Enabled = true;
            this.ctrlMoviemientosCajaDetalles.intCountIndex = this.ctrlMoviemientosCajaDetalles.movimientosCajaDetalles.intRowIndex;
            this.ctrlMovimientosCaja.PrimeroPaginacion();
           if (!this.ctrlMovimientosCaja.bolResutladoPaginacion) return;
            this.llenar_formulario();
        }

        private void btnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            txtFolio.Enabled = true;
            this.ctrlMoviemientosCajaDetalles.intCountIndex = this.ctrlMoviemientosCajaDetalles.movimientosCajaDetalles.intRowIndex;
            this.ctrlMovimientosCaja.SiguentePaginacion();
            if (!this.ctrlMovimientosCaja.bolResutladoPaginacion) return;
            this.movimientosCajaDetalles(this.ctrlMovimientosCaja.movimientosCaja.intId);
            this.llenar_formulario();

        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            txtFolio.Enabled = true;
            this.ctrlMoviemientosCajaDetalles.intCountIndex = this.ctrlMoviemientosCajaDetalles.movimientosCajaDetalles.intRowIndex;
            this.ctrlMovimientosCaja.AtrasPaginacion();
            if (!this.ctrlMovimientosCaja.bolResutladoPaginacion) return;
            this.llenar_formulario();

        }

        private void btnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            txtFolio.Enabled = true;
            this.ctrlMoviemientosCajaDetalles.intCountIndex = this.ctrlMoviemientosCajaDetalles.movimientosCajaDetalles.intRowIndex;
            this.ctrlMovimientosCaja.UltimoPaginacion();
            if (!this.ctrlMovimientosCaja.bolResutladoPaginacion) return;
            this.llenar_formulario();

        }
        private void tsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();
            
            this.ctrlMovimientosCaja.movimientosCaja.limpiarProps();
            txtFolio.Enabled = false;


                txtNombreEmpleado.Text = this.clsView.login.usuario.strNombre + " " + this.clsView.login.usuario.strApellidoPaterno + " " + this.clsView.login.usuario.strApellidoMaterno;
                txtHora.Text = DateTime.Now.ToString("HH:mm:ss");
                dtpFecha.MinDate = DateTime.Today;
                txtCodigoCaja.Text = this.clsView.login.caja.strCodigo;
                txtDescripcionCaja.Text = this.clsView.login.caja.strDescripcion;
                lblEstatus.Visible = true;
                lblDescEstatus.Visible = true;
                lblEstatus.Text = "NUEVO";
                txtFolio.Text = "AUTOGENERADO";



            if (this.clsView.login.usuario.validarMovimientos() && this.clsView.bolPermisosGuardar)
            {
                tsbGuardar.Enabled = true;
                btnFooterGuardar.Enabled = true;
                btnFooterGuardar.Visible = true;                
            }
            else 
            {
                tsbGuardar.Enabled = false;
                btnFooterGuardar.Enabled = false;
                btnFooterGuardar.Visible = false;
            }
            txtFolio.Focus();

            this.dgvMovimientosCajaDetalles();
            
        }
  
        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            dtpFecha.MinDate = DateTime.Today;
            dtpFecha.Text = DateTime.Today.ToString();

            lblDescEstatus.Visible = false;
            lblEstatus.Visible = false;

            btnVerCancelacion.Visible = false;

            dgvDetalles.DataSource = null;
            if (this.clsView.bolPermisosNuevo)
            {
                tsbNuevo.Enabled = true;
            }

            if (this.clsView.login.usuario.validarMovimientos() && this.clsView.bolPermisosGuardar)
            {
                btnFooterGuardar.Enabled = true;
                tsbGuardar.Enabled = true;
            }
            else
            {
                btnFooterGuardar.Enabled = false;
                tsbGuardar.Enabled = false;
            }


            if (this.clsView.login.usuario.validarMovimientos() && this.clsView.bolPermisosEliminar)
            {
                tsbEliminar.Enabled = true;
            }
            else
            {
                tsbEliminar.Enabled = false;
            }


        }

        private bool validacion()
        {

            bool bolValidacion = true;
            bool bolValidacionTipo = false;
            bool bolValidacionConcepto = false;
            //bool bolValidacionSucursal = false;
            bool bolValidacionImporte = false;
            List<bool> lsValidacion = new List<bool>();
            int i = 0;
            string strConcepto = txtConcepto.Text;

            if (strConcepto == "")
            {
                txtConcepto.Focus();
                this.clsView.principal.epMensaje.SetError(txtConcepto, this.clsView.mensajeErrorValiForm2("El campo concepto es obligatorio, favor de ingresar un concepto"));
                bolValidacion = false;
            }

            if (dgvDetalles.Rows.Count <= 1) 
            {
                this.clsView.mensajeErrorValiForm2("Agregar al menos un detalle para este movimiento de caja ");
                bolValidacion = false;
            }

            foreach (DataGridViewRow row in dgvDetalles.Rows)
            {
                if (i == dgvDetalles.Rows.Count - 1) break;


              


                if (row.Cells["Concepto"].Value.ToString() == "") 
                {
                    lsValidacion.Add(false);
                    this.clsView.mensajeErrorValiForm2(" El campo concepto es obligatorio, favor de ingresar un concepto en la fila " + (i + 1) );
                }

                if (row.Cells["Tipo"].Value.ToString() == "")
                {
                    lsValidacion.Add(false);
                    this.clsView.mensajeErrorValiForm2(" El campo tipo es obligatorio, favor de ingresar un tipo en la fila " + (i + 1)  );
                }


                if (row.Cells["Importe"].Value.ToString() == "")
                {
                    lsValidacion.Add(false);
                    this.clsView.mensajeErrorValiForm2(" El campo importe es obligatorio, favor de proporcionar un valor numérico en la fila " + (i + 1));
                }

                i++;
            }

            if (lsValidacion.Count > 0) 
            {
                bolValidacion = false;
            }
            
            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();

            if (validacion())
            {               
                if (this.ctrlMovimientosCaja.movimientosCaja.intId == 0) 
                {
                    Controller.clsCtrlFolios ctrlFolios = new Controller.clsCtrlFolios();
                    ctrlFolios.BuscarFolioProceso(this.clsView.strProcesosId, this.clsView.login.intSucursalId.ToString());
                    if (ctrlFolios.folio.intConsecutivo == 0)
                    {
                        this.clsView.mensajeErrorValiForm2("No es posible generar el folio, comuníquese con el administrador.");
                        tsbGuardar.Enabled = false;
                        btnFooterGuardar.Enabled = false;
                    }
                    else
                    {

                        txtFolio.Text = ctrlFolios.folio.regresarFolioConsecutivo(this.intNumCerosConsecutivo);
                    }
                }



                this.ctrlMovimientosCaja.movimientosCaja.strFolio = txtFolio.Text;
                this.ctrlMovimientosCaja.movimientosCaja.strConcepto = txtConcepto.Text;
                this.ctrlMovimientosCaja.movimientosCaja.strHora = txtHora.Text;
                this.ctrlMovimientosCaja.movimientosCaja.dtFechaMovimiento = Convert.ToDateTime(dtpFecha.Value.ToString("yyyy-MM-dd") + " " + this.ctrlMovimientosCaja.movimientosCaja.strHora);
                this.ctrlMovimientosCaja.movimientosCaja.caja.intId = this.clsView.login.caja.intId;
                this.ctrlMovimientosCaja.movimientosCaja.empleado.intId = this.clsView.login.usuario.intId;
                this.ctrlMovimientosCaja.movimientosCaja.intSucursal = this.clsView.login.intSucursalId;
                this.ctrlMovimientosCaja.movimientosCaja.intUsuario = this.clsView.login.usuario.intId;



                this.guardarMovimientoDetalles();
                this.ctrlFoliosProcesos.BuscarFolioProcesoMenu(int.Parse(this.clsView.strProcesosId), this.clsView.login.intSucursalId);
                if (this.ctrlFoliosProcesos.mdlFolioProcesos.bolResultado)
                {
                    this.ctrlMovimientosCaja.movimientosCaja.intFolioId = this.ctrlFoliosProcesos.foliosProcesos.folio.intId;

                    this.ctrlMovimientosCaja.Guardar();

                }
                else
                {
                    this.clsView.mensajeErrorValiForm2("No es posible generar el folio, comuníquese con el administrador.");
                }
                

                if (this.ctrlMovimientosCaja.mdlMovimientosCaja.bolResultado)
                {

                    //Hacer un llamado a la funcion para obtener los datos del registro(modificado)
                    this.regresarRegistro("guardó");

                }
            }
        }


        public void guardarMovimientoDetalles() 
        {

            string movimiento_caja_id = "";
            string sucursal_id = this.clsView.login.intSucursalId.ToString();
            if (this.ctrlMovimientosCaja.movimientosCaja.intId == 0)
            {
                movimiento_caja_id = "@id";
            }
            else
            {
                movimiento_caja_id = this.ctrlMovimientosCaja.movimientosCaja.intId.ToString();
            }
            int countDetalles = dgvDetalles.Rows.Count;            
            this.ctrlMovimientosCaja.movimientosCaja.strQueryMovimientos = "INSERT INTO movimientos_caja_detalles(movimiento_caja_id,sucursal_id, renglon,  tipo,concepto, importe)VALUES ";
            int i = 0;
            bool bolUltimoDato = false;
            bool bolNiguanDato = false;
            string renglon = "";
            string strTipo = "";

            if (countDetalles >= 1)
            {

                bolNiguanDato = true;
                foreach (DataGridViewRow row in dgvDetalles.Rows)
                {
                    
                    renglon = (row.Cells["renglon"].Value.ToString() == "") ? (i + 1).ToString() : row.Cells["renglon"].Value.ToString();
                    strTipo = (row.Cells["Tipo"].Value.ToString() == "E") ? "ENTRADA" : "SALIDA";
                    if (i == countDetalles - 2)
                    {
                        //this.ctrlMovimientosCaja.movimientosCaja.strQueryMovimientos += " (" + sucursal_id + "," + renglon + " , @id, @sucursal_movimiento," + row.Cells["Importe"].Value.ToString() + "); ";
                        this.ctrlMovimientosCaja.movimientosCaja.strQueryMovimientos += " (" + movimiento_caja_id + "," + sucursal_id + "," + renglon + ", '"+ strTipo + "' , '" + row.Cells["Concepto"].Value.ToString() + "'," + row.Cells["Importe"].Value.ToString().Replace(",", "").Replace("$", "") + "); ";
                        bolUltimoDato = true;
                        
                    }
                    else
                    {
                        //this.ctrlMovimientosCaja.movimientosCaja.strQueryMovimientos += " (" + sucursal_id + "," + renglon + " , @id, @sucursal_movimiento," + row.Cells["Importe"].Value.ToString() + ")|| ";
                        this.ctrlMovimientosCaja.movimientosCaja.strQueryMovimientos += " (" + movimiento_caja_id + "," + sucursal_id + "," + renglon + " , '"+ strTipo + "','"+ row.Cells["Concepto"].Value.ToString() + "'," + row.Cells["Importe"].Value.ToString().Replace(",", "").Replace("$", "") + ")|| ";
                    }

                 
                    //En caso que ya estuvo en el ultmio registro de datagridview se hacer un break 
                    if (bolUltimoDato) break;
                    i++;
                }

            }

            if (!bolNiguanDato)
            {
                this.ctrlMovimientosCaja.movimientosCaja.strQueryMovimientos = "";
            }
            else
            {
                if (bolUltimoDato)
                {
                    this.ctrlMovimientosCaja.movimientosCaja.strQueryMovimientos = this.ctrlMovimientosCaja.movimientosCaja.strQueryMovimientos.Replace("||", ",");
                }
                else
                {
                    int intStrIndex = this.ctrlMovimientosCaja.movimientosCaja.strQueryMovimientos.LastIndexOf("||");
                    this.ctrlMovimientosCaja.movimientosCaja.strQueryMovimientos = this.ctrlMovimientosCaja.movimientosCaja.strQueryMovimientos.Remove(intStrIndex).Insert(intStrIndex, ";").Replace("||", ",");
                }
            }


        }

        public void llenar_formulario()
        {
            this.limpiar_textbox();
            if (this.ctrlMovimientosCaja.movimientosCaja.intId == 0)
            {
                lblDescEstatus.Visible = false;
                lblEstatus.Visible = false;
                return;
            }

            txtFolio.Enabled = true;
            txtFolio.Text = this.ctrlMovimientosCaja.movimientosCaja.strFolio;
            dtpFecha.MinDate = this.ctrlMovimientosCaja.movimientosCaja.dtFechaMovimiento;
            dtpFecha.Value = this.ctrlMovimientosCaja.movimientosCaja.dtFechaMovimiento;
            dtpFecha.Text = this.ctrlMovimientosCaja.movimientosCaja.dtFechaMovimiento.ToString();
            txtHora.Text = this.ctrlMovimientosCaja.movimientosCaja.strHora;
            txtNombreEmpleado.Text = this.ctrlMovimientosCaja.movimientosCaja.empleado.strNombre;
            txtCodigoCaja.Text = this.ctrlMovimientosCaja.movimientosCaja.caja.strCodigo;
            txtDescripcionCaja.Text = this.ctrlMovimientosCaja.movimientosCaja.caja.strDescripcion;
            txtConcepto.Text = this.ctrlMovimientosCaja.movimientosCaja.strConcepto;
            lblEstatus.Text = this.ctrlMovimientosCaja.movimientosCaja.strEstatus;

            lblDescEstatus.Visible = true;
            lblEstatus.Visible = true;
            this.movimientosCajaDetalles(this.ctrlMovimientosCaja.movimientosCaja.intId);

            if (this.clsView.login.usuario.validarMovimientos() && this.clsView.bolPermisosGuardar && this.ctrlMovimientosCaja.movimientosCaja.strEstatus == this.ctrlMovimientosCaja.movimientosCaja.dicEstatus["ACTIVO"])
            {
                tsbGuardar.Enabled = true;
                btnFooterGuardar.Enabled = true;
                btnFooterGuardar.Visible = true;
            }
            else if (this.ctrlMovimientosCaja.movimientosCaja.strEstatus == this.ctrlMovimientosCaja.movimientosCaja.dicEstatus["CANCELADO"])
            {
                tsbGuardar.Enabled = false;
                tsbEliminar.Enabled = false;
                btnFooterGuardar.Enabled = false;
                btnFooterGuardar.Visible = false;
                btnVerCancelacion.Visible = true;
            }
 
            
        }


        public void agregarDeshacer()
        {
            this.lsDeshacerMovimientosCaja.RemoveAt(0);
            this.lsDeshacerMovimientosCaja.Add(this.ctrlMovimientosCaja.movimientosCaja);
        }

        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void tsbBuscar_Click(object sender, EventArgs e)
        {
           if (this.ctrlMovimientosCaja.bolResutladoPaginacion)
            {
                this.ctrlMovimientosCaja.CargarFrmBusqueda();
                if (this.ctrlMovimientosCaja.bolResultadoBusqueda) 
                {
                    this.llenar_formulario();
                }
                
            }
            
        }

        public void dgvMovimientosCajaDetalles()
        {
            dgvDetalles.DataSource = null;
            dgvDetalles.AutoGenerateColumns = true;

            DataTable dtProcesos = new DataTable();

            dtProcesos.Columns.Add(new DataColumn("movimiento_caja_tipo_id", typeof(string)));
            dtProcesos.Columns.Add(new DataColumn("sucursal_id", typeof(string)));
            dtProcesos.Columns.Add(new DataColumn("renglon", typeof(string)));            
            dtProcesos.Columns.Add(new DataColumn("Tipo", typeof(string)));
            dtProcesos.Columns.Add(new DataColumn("Concepto", typeof(string)));
            dtProcesos.Columns.Add(new DataColumn("Importe", typeof(string)));            

            dgvDetalles.DataSource = dtProcesos.DefaultView;

            this.bloquearColumnasGrid();
            dgvDetalles.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvDetalles.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dgvDetalles.Columns["Tipo"].Width = 25;
            dgvDetalles.Columns["Importe"].Width = 150;
            dgvDetalles.Columns["Importe"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvDetalles.Columns["Tipo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;


        }

        

        public void movimientosCajaDetalles(int intSubProcesos)
        {
            
            this.ctrlMoviemientosCajaDetalles.Buscar(this.ctrlMovimientosCaja.movimientosCaja.intId);

            if (this.ctrlMoviemientosCajaDetalles.bolResultado)
            {

                dgvDetalles.DataSource = this.ctrlMoviemientosCajaDetalles.objDataSet.Tables[0].DefaultView; // dataset       
               //Son los nombre de campos que no quiero que se muestran en el datagridview

                this.clsView.lsBloquearCol = new List<string>()
                {
                        "movimiento_caja_id",
                        "sucursal_id" ,
                        "renglon"
                        
                };

                //Pasar datagridview para que se puede usar en core
                this.clsView.dgvGenrico = dgvDetalles;
                this.clsView.bloquearColumnasGrid();
                this.clsView.configuracionDataGridView();

                dgvDetalles.Columns["Importe"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
            else
            {
                dgvDetalles.DataSource = null;
                this.dgvMovimientosCajaDetalles();
            }


        }

        private void dgvDetalles_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {


            string header = dgvDetalles.Columns[dgvDetalles.CurrentCell.ColumnIndex].HeaderText;
            
            

            TextBox txt = e.Control as TextBox;
            if (txt != null)
            {
                txt.AutoCompleteMode = AutoCompleteMode.Suggest;
                txt.AutoCompleteSource = AutoCompleteSource.CustomSource;
                AutoCompleteStringCollection DataCollection = new AutoCompleteStringCollection();
               if (header == "Tipo" || header == "Concepto")               
               {
                    ((TextBox)(e.Control)).CharacterCasing = CharacterCasing.Upper;                    
                }
                else if (header == "Importe") 
               {
                   this.editarNum(dgvDetalles, e);
               }

               
            }
        }

        private void bloquearColumnasGrid()
        {
            dgvDetalles.Columns["renglon"].Visible = false;
            dgvDetalles.Columns["sucursal_id"].Visible = false;
            dgvDetalles.Columns["movimiento_caja_tipo_id"].Visible = false;
            //dgvDetalles.Columns["sucursal_movimiento"].Visible = false;
            
        }

        private void Importe_KeyPress(object sender, KeyPressEventArgs e)
        {
            string header = dgvDetalles.Columns[dgvDetalles.CurrentCell.ColumnIndex].HeaderText;
            if (header == "Importe") 
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && !(e.KeyChar == '.'))                
                {
                    e.Handled = true;
                }
           
            }
            
        }

        public void editarNum(DataGridView dgvGenerico, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= new KeyPressEventHandler(Importe_KeyPress);
            if (dgvGenerico.CurrentCell.ColumnIndex == dgvGenerico.ColumnCount - 1) //Desired Column
            {
                TextBox tb = e.Control as TextBox;
                if (tb != null)                
                {
                    tb.KeyPress += new KeyPressEventHandler(Importe_KeyPress);
                }
            }
        }
            

        private void dgvDetalles_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            
                string header = dgvDetalles.Columns[e.ColumnIndex].HeaderText;
                string strTipo = "";
                string strImporte = "";
                if (header == "Tipo" || header == "Importe")
                {
                    strTipo = dgvDetalles.Rows[e.RowIndex].Cells["Tipo"].EditedFormattedValue.ToString().ToUpper();
                    strImporte = dgvDetalles.Rows[e.RowIndex].Cells["Importe"].EditedFormattedValue.ToString();
                }

                if (header == "Tipo" && (strTipo != "E" && strTipo != "S"))
                {
                    dgvDetalles.Rows[e.RowIndex].Cells["Tipo"].Value = "";
                    dgvDetalles.CurrentCell = dgvDetalles.Rows[e.RowIndex].Cells[e.ColumnIndex];                    
                } 
                else if (header == "Importe") 
                {                    
                           
                  if(dgvDetalles.Rows[e.RowIndex].Cells["Importe"].Value.ToString() != "")
                  {

                     dgvDetalles.Rows[e.RowIndex].Cells["Importe"].Value = dgvDetalles.Rows[e.RowIndex].Cells["Importe"].Value.ToString().Replace(",", "").Replace("$", "");
                      dgvDetalles.Rows[e.RowIndex].Cells["Importe"].Value = decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["Importe"].Value.ToString()).ToString("$#,##0.00");
                  }

                }

        }
        private void dgvDetalles_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {            

            string header = dgvDetalles.Columns[dgvDetalles.CurrentCell.ColumnIndex].HeaderText;
            string strTipo = "";            
            string strImporte = "";
            int i = 0;
            bool bolUnico = false;
            if (header == "Tipo" || header == "Importe")
            {
                strTipo = dgvDetalles.Rows[e.RowIndex].Cells["Tipo"].EditedFormattedValue.ToString().ToUpper();                
                strImporte = dgvDetalles.Rows[e.RowIndex].Cells["Importe"].EditedFormattedValue.ToString();
            }


            if (header == "Tipo" && (strTipo != "E" && strTipo != "S")) 
            {
                var cell = (sender as DataGridView).CurrentCell;
                cell.Value = String.Empty;                
                this.clsView.mensajeErrorValiForm2("Favor de ingresar una 'E' para Entrada ó 'S' para Salida");

            }
                
        } 


        private void dgvDetalles_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvDetalles.Rows.Count > this.intRows)
            {
                dgvDetalles.AllowUserToAddRows = false;
            }
            else 
            {
                dgvDetalles.AllowUserToAddRows = true;
            }
        }

        private void dgvDetalles_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void dgvDetalles_KeyUp(object sender, KeyEventArgs e)
        {
            
        }

        private void dgvDetalles_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.KeyCode == Keys.Enter) 
            {
                e.Handled = false;
            }
            if (e.KeyCode == Keys.Down) 
            {
                e.Handled = false;
            }
            if (e.KeyCode == Keys.Delete)
            {

                try
                {
                    dgvDetalles.CurrentRow.Selected = true;
                    int dgvIndex = dgvDetalles.CurrentRow.Index;


                    if (this.dgvDetalles.Rows.Count > 1)
                    {
                        dgvDetalles.Rows.RemoveAt(dgvIndex);
                    }

                    e.Handled = true;

                }
                catch (Exception ex) { }
               
            }
        }

        private void txtFolio_Leave(object sender, EventArgs e)
        {


            if (txtFolio.Text == "")
            {
                limpiar_textbox();
                return;
            }

            if (txtFolio.Text.Length < this.ctrlMovimientosCaja.movimientosCaja.intNumCerosConsecutivo)
            {
                if (this.ctrlFoliosAuto.folio.intConsecutivo == 0)
                {
                    this.clsView.mensajeErrorValiForm2("No es posible generar el folio, comuníquese con el administrador.");
                    tsbGuardar.Enabled = false;
                    btnFooterGuardar.Enabled = false;
                }
                else
                {
                    txtFolio.Text = this.ctrlFoliosAuto.folio.regresarFolioConsecutivoAuto(this.ctrlMovimientosCaja.movimientosCaja.intNumCerosConsecutivo, txtFolio.Text);
                }
            }

            this.ctrlMovimientosCaja.Buscar(txtFolio.Text);
            if (this.ctrlMovimientosCaja.mdlMovimientosCaja.bolResultado)
            {
                this.llenar_formulario();
            }
            else 
            {
                this.limpiar_textbox();
                txtFolio.Focus();
            }
            
        }

        private void dgvDetalles_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlMovimientosCaja.movimientosCaja.intId > 0 && this.ctrlMovimientosCaja.movimientosCaja.strEstatus == this.ctrlMovimientosCaja.movimientosCaja.dicEstatus["ACTIVO"] && this.clsView.login.usuario.validarMovimientos())
            {
                if (this.clsView.confirmacionAccion("Motivo de cancelación", "¿Desea cancelar este registro?"))
                {
                    frmMotivosCancelacion frmMotivos = new frmMotivosCancelacion();
                    frmMotivos.strTipo = "G";
                    frmMotivos.ShowDialog();
                    if (frmMotivos.bolResultado)
                    {
                        this.ctrlMovimientosCaja.movimientosCaja.strMotivosCancelacion = frmMotivos.strMotivo;
                        this.ctrlMovimientosCaja.movimientosCaja.dtFecha = DateTime.Now;
                        this.ctrlMovimientosCaja.movimientosCaja.intUsuario = this.clsView.login.usuario.intId;

                        this.ctrlMovimientosCaja.Eliminar();
                        if (this.ctrlMovimientosCaja.bolResultado)
                        {
                            //Hacer un llamado a la funcion para obtener los datos del registro(modificado)
                            this.regresarRegistro("cancelo");

                        }
                    }
                }
            }
        }

        private void tsbReactivar_Click(object sender, EventArgs e)
        {
            /*if (this.clsView.confirmacionAccion("Reactivar", "¿Desea reactivar este registro?"))
            {
                this.ctrlMovimientosCaja.movimientosCaja.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlMovimientosCaja.Reactivar();
                if (this.ctrlMovimientosCaja.bolResultado)
                {                    
                    this.regresarRegistro("activó");
                }
            }*/
        }

        /*Metodo para regresar los datos de un registro*/
        public void regresarRegistro(string stMsj)
        {
            this.limpiar_textbox();
            this.ctrlMovimientosCaja.Paginacion(this.clsView.login.intSucursalId, this.clsView.login.caja.intId);
            this.clsView.mensajeExitoDB(this.Text.ToLower(), stMsj);

            this.ctrlMovimientosCaja.Buscar(this.ctrlMovimientosCaja.movimientosCaja.strFolio);
            llenar_formulario();
            txtFolio.Enabled = true;
            txtFolio.Focus();
        }

        private void btnVerCancelacion_Click(object sender, EventArgs e)
        {
            frmMotivosCancelacion frmMotivos = new frmMotivosCancelacion();
            frmMotivos.strTipo = "V";
            frmMotivos.strMotivo = this.ctrlMovimientosCaja.movimientosCaja.strMotivosCancelacion + " \r\n\r\n\r\n Usuario cancelación: " + this.ctrlMovimientosCaja.movimientosCaja.strUsuarioEliminacion + "\r\n  Fecha: " + this.ctrlMovimientosCaja.movimientosCaja.strFechaEliminacion;
            frmMotivos.ShowDialog();
        }
    }
}

