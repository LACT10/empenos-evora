﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UI_Support;

namespace EmpenosEvora
{
    public partial class frmBoletasEmpeno : Form
    {

        public bool _bolGramaje;
        public bool _bolIMEICelular;
        public bool _bolSerieVehiculo;
        public string _strFoto;
        public PictureBox _pbFoto;
        
        public bool _bolAutorizarKilataje;
        public DataRow _objDataSetSingleAutorizarKilataje;
        public DateTime _dtpFechaAutorizacionKilataje;


        public Controller.clsCtrlBoletaEmpeno ctrlBoletaEmpeno;
        public Controller.clsCtrlClientes ctrlClientes;
        public Controller.clsCtrlGramajes ctrlGramajes;
        public Controller.clsCtrlFoliosProcesos ctrlFoliosProcesos;
        public Controller.clsCtrlSucursalParametros clsCtrlSucursalParametros;
        private DPFP.Template template;
        public Core.clsCoreView clsView = new Core.clsCoreView();
        public Controller.clsCtrlUsuarios ctrlUsuario;
        public Controller.clsCtrlFolios ctrlFoliosAuto = new Controller.clsCtrlFolios();


        public bool bolNuevo = false;

        public bool bolGramaje { get => _bolGramaje; set => _bolGramaje = value; }
        public bool bolIMEICelular { get => _bolIMEICelular; set => _bolIMEICelular = value; }
        public bool bolSerieVehiculo { get => _bolSerieVehiculo; set => _bolSerieVehiculo = value; }
        public string strFoto { get => _strFoto; set => _strFoto = value; }
        public PictureBox pbFoto { get => _pbFoto; set => _pbFoto = value; }        

        public bool bolAutorizarKilataje { get => _bolAutorizarKilataje; set => _bolAutorizarKilataje = value; }

        public DataRow objDataSetSingleAutorizarKilataje { get => _objDataSetSingleAutorizarKilataje; set => _objDataSetSingleAutorizarKilataje = value; }
        public DateTime dtpFechaAutorizacionKilataje { get => _dtpFechaAutorizacionKilataje; set => _dtpFechaAutorizacionKilataje = value; }

        public List<Properties_Class.clsBoletasEmpeno> lsDeshacerBoletasEmpeno;
        public int intNumCerosConsecutivo = 10;
        public frmBoletasEmpeno()
        {
            InitializeComponent();


            this.ctrlClientes = new Controller.clsCtrlClientes();            


            this.ctrlUsuario = new Controller.clsCtrlUsuarios();
            this.ctrlUsuario.Paginacion();


            this.ctrlGramajes = new Controller.clsCtrlGramajes();

            this.ctrlBoletaEmpeno = new Controller.clsCtrlBoletaEmpeno();
            

            this.ctrlFoliosProcesos = new Controller.clsCtrlFoliosProcesos();

            this.strFoto = "";

            this.lsDeshacerBoletasEmpeno = new List<Properties_Class.clsBoletasEmpeno>();
            this.lsDeshacerBoletasEmpeno.Add(this.ctrlBoletaEmpeno.boletasEmpeno);

            this.bolAutorizarKilataje = false;


        }

        private void FrmBoletasEmpeno_Load(object sender, EventArgs e)
        {
            txtBoleta.MaxLength = this.intNumCerosConsecutivo;

            this.ctrlBoletaEmpeno.Paginacion(this.clsView.login.intSucursalId);

            this.Text = this.Text + " " + "[Caja: " + this.clsView.login.caja.strDescripcion + "]";

            this.ctrlFoliosAuto.BuscarFolioProceso(this.clsView.strProcesosId, this.clsView.login.intSucursalId.ToString());



            this.clsCtrlSucursalParametros = new Controller.clsCtrlSucursalParametros();
            this.clsCtrlSucursalParametros.Buscar(this.clsView.login.intSucursalId.ToString());

            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                }/*,
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }*/
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);

            //this.bolPermisos = tsbGuardar.Enabled;


            if (this.clsView.login.usuario.validarMovimientos() && this.clsView.bolPermisosEliminar)
            {                
                tsbEliminar.Enabled = true;
            }
            else
            {
                tsbEliminar.Enabled = false;
            }
        }


        private void tsbBuscar_Click(object sender, EventArgs e)
        {
            Dictionary<int, Properties_Class.clsTextbox> dicFocus = new Dictionary<int, Properties_Class.clsTextbox>()
            {
                { 0, new Properties_Class.clsTextbox()
                                { txtGenerico = txtBoleta, strBusqueda = "boleta"}
                },
                { 1, new Properties_Class.clsTextbox()
                                { txtGenerico = txtBoletaInicial, strBusqueda = "boleta"}
                },
                { 2, new Properties_Class.clsTextbox()
                                { txtGenerico = txtHora, strBusqueda = "boleta"}
                },
                { 3, new Properties_Class.clsTextbox()
                                { txtGenerico = txtBoletaInicial, strBusqueda = "boleta"}
                },
                { 4, new Properties_Class.clsTextbox()
                                  { txtGenerico = txtCodigoCliente, strBusqueda = "cliente"}
                },
                { 5, new Properties_Class.clsTextbox()
                                { txtGenerico = txtNombreCliente, strBusqueda = "cliente"}
                },
                { 6, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoIdentificacion, strBusqueda = "identificacion"}
                },
                { 7, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcionIdentificacion, strBusqueda = "identificacion"}
                },
                { 8, new Properties_Class.clsTextbox()
                                { txtGenerico = txtNombreCliente, strBusqueda = "identificacion"}
                },
                { 9, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoPrendaTipo, strBusqueda = "tipoPrenda"}
                },
                { 10, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcionPrendaTipo, strBusqueda = "tipoPrenda"}
                },
                { 11, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoSubTipoPrenda, strBusqueda = "subTipoPrenda"}
                },
                { 12, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcionSubTipoPrenda, strBusqueda = "subTipoPrenda"}
                },
                { 13, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoGramaje, strBusqueda = "gramaje"}
                },
                { 14, new Properties_Class.clsTextbox()
                                { txtGenerico = txtGramajeDescripcion, strBusqueda = "gramaje"}
                },
                { 15, new Properties_Class.clsTextbox()
                                { txtGenerico = txtKilataje, strBusqueda = "gramaje"}
                },
                { 16, new Properties_Class.clsTextbox()
                                { txtGenerico = txtGramos, strBusqueda = "gramaje"}
                },
                { 17, new Properties_Class.clsTextbox()
                                { txtGenerico = txtGramos, strBusqueda = "gramaje"}
                },
                { 18, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCantidad, strBusqueda = "boleta"}
                },
                { 19, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCantidad, strBusqueda = "boleta"}
                },
                { 20, new Properties_Class.clsTextbox()
                                { txtGenerico = txtPrestamo, strBusqueda = "boleta"}
                },
                { 21, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcion, strBusqueda = "boleta"}
                },
                { 22, new Properties_Class.clsTextbox()
                                { txtGenerico = txtComentario, strBusqueda = "boleta"}
                },
                { 23, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoCaja, strBusqueda = "boleta"}
                },
                { 24, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcionCaja, strBusqueda = "boleta"}
                }
                                           
        };
             foreach (KeyValuePair<int, Properties_Class.clsTextbox> data in dicFocus)
             {
                 if (data.Value.txtGenerico.Focused)
                 {
                     if (data.Value.strBusqueda == "boleta")
                     {
                         this.mostraBusqueda();
                     }
                     else if (data.Value.strBusqueda == "cliente")
                     {
                         this.mostraBusquedaCliente();
                     }
                    else if (data.Value.strBusqueda == "identificacion")
                    {
                        this.mostraBusquedaIdentificacion();
                    }
                    else if (data.Value.strBusqueda == "tipoPrenda")
                    {
                        this.mostraBusquedaTipoPrenda();
                    }
                    else if (data.Value.strBusqueda == "subTipoPrenda")
                    {
                        this.mostraBusquedaSubTipoPrenda();
                    }
                    else if (data.Value.strBusqueda == "gramaje")
                    {
                        this.mostraBusquedaGramaje();
                    }
                }

             }


            if (dtpFecha.Focused || dtpFechaVencimiento.Focused) 
            {
                this.mostraBusqueda();
            }


        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlBoletaEmpeno.boletasEmpeno.intId > 0)
            {
                llenar_formulario();                
            }
            else
            {
                txtBoleta.Enabled = true;
                this.limpiar_textbox();
            }
            txtBoleta.Focus();
        }

        private void btnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlBoletaEmpeno.PrimeroPaginacion();
         
            txtBoleta.Enabled = true;
            if (!this.ctrlBoletaEmpeno.bolResultado) return;
            
            this.llenar_formulario();
        }

        private void btnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlBoletaEmpeno.SiguentePaginacion();
          
            txtBoleta.Enabled = true;
            if (!this.ctrlBoletaEmpeno.bolResultado) return;
            this.llenar_formulario();

        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlBoletaEmpeno.AtrasPaginacion();
         
            txtBoleta.Enabled = true;
            if (!this.ctrlBoletaEmpeno.bolResultado) return;
            this.llenar_formulario();

        }

        private void btnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlBoletaEmpeno.UltimoPaginacion();
        
            txtBoleta.Enabled = true;
            if (!this.ctrlBoletaEmpeno.bolResultado) return;
            this.llenar_formulario();

        }

        private void tsbNuevo_Click(object sender, EventArgs e)
        {
           
            this.limpiar_textbox();
            this.ctrlBoletaEmpeno.boletasEmpeno.limpiarProps();
            txtBoleta.Enabled = false;

            
            txtBoleta.Text = "AUTOGENERADO";
            
            txtNombreEmpleado.Text = this.clsView.login.usuario.strNombre + " " + this.clsView.login.usuario.strApellidoPaterno + " " + this.clsView.login.usuario.strApellidoMaterno;
               
            txtHora.Text = DateTime.Now.ToString("HH:mm:ss");

            txtCodigoCaja.Text = this.clsView.login.caja.strCodigo;
            txtDescripcionCaja.Text = this.clsView.login.caja.strDescripcion;
            dtpFecha.MinDate = DateTime.Today;
            lblEstatus.Visible = true;
            lblDescEstatus.Visible = true;
            lblEstatus.Text = "NUEVO";
            this.bolNuevo = true;

            if (this.clsView.bolPermisosGuardar)
            {                                    
                tsbGuardar.Enabled = true;                
                btnFooterGuardar.Enabled = true;
            }
            else
            {                                    
                tsbGuardar.Enabled = false;
                btnFooterGuardar.Enabled = false;                
            }
            txtCodigoPrendaTipo.Focus();
        }

        private void tsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();

        }
        private void DateTimePicker4_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtCodigoCliente_Leave(object sender, EventArgs e)
        {
            if (txtCodigoCliente.Text == "") 
            {
                txtClienteId.Text = "";
                txtCodigoCliente.Text = "";
                txtNombreCliente.Text = "";
                this.ctrlClientes.limpiarProps();
                return;
            }
            
            string strCodigo = this.ctrlClientes.cliente.generaCodigoConsecutivo(txtCodigoCliente.Text);
            this.ctrlClientes.Buscar(strCodigo);
            if (!(this.ctrlClientes.mdlClientes.bolResultado))
            {
                txtClienteId.Text = "";
                txtCodigoCliente.Text = "";
                txtNombreCliente.Text = "";
                txtCodigoCliente.Focus();
                return;
            }

            // this.llenar_formulario();
            this.llenar_formulario_clientes();
        }

        private void txtPrestamo_Leave(object sender, EventArgs e)
        {
            if (txtPrestamo.Text == "" || (txtPrestamo.Text.Replace("$", "").Replace(",", "") == txtPrestamoAnt.Text)) return;
            this.calcularAvaluo();
            txtPrestamo.Text = this.clsView.convertMoneda(txtPrestamo.Text);
        }

        private void dtpFecha_Leave(object sender, EventArgs e)
        {
            this.calcularPlazo();
        }

        private void txtCodigoGramaje_Leave(object sender, EventArgs e)
        {
            if (txtPrendaTipotId.Text == "" || txtCodigoGramaje.Text == "")
            {
                txtCodigoGramaje.Text = "";
                txtGramajeDescripcion.Text = "";
                txtGramajeId.Text = "";
                txtPrecioTope.Text = "";
                txtKilataje.Text = "";
                txtGramos.Text = "";
                txtPrestamo.Text = "";
                txtAvaluo.Text = "";
                this.ctrlGramajes.limpiarProps();

                return;
            }
            
            string strCodigo = this.ctrlGramajes.gramaje.generaCodigoConsecutivo(txtCodigoGramaje.Text);
            this.ctrlGramajes.BuscarPrendaSucursal(strCodigo, txtPrendaTipotId.Text, this.clsView.login.intSucursalId.ToString());

            if (!this.ctrlGramajes.mdlGramajes.bolResultado) 
            {
                this.limpiarFormGramaje();
                txtCodigoGramaje.Focus();
                return;
            }
            
            txtCodigoGramaje.Text = this.ctrlGramajes.gramaje.strCodigo;
            txtGramajeDescripcion.Text = this.ctrlGramajes.gramaje.strDescripcion;
            txtGramajeId.Text = this.ctrlGramajes.gramaje.intId.ToString();
            txtPrecioTope.Text = this.ctrlGramajes.gramaje.decPrecioTope.ToString();
            txtKilataje.Text = txtPrecioTope.Text;
            this.calcularPrestamoGramaje();

        }


        private void txtCodigoPrendaTipo_Leave(object sender, EventArgs e)
        {
            if (txtCodigoPrendaTipo.Text == "") 
            {
                txtPrendaTipotId.Text = "";
                txtCodigoPrendaTipo.Text = "";
                txtDescripcionPrendaTipo.Text = "";
                this.bolGramaje = false;
                this.bolIMEICelular = false;
                this.bolSerieVehiculo = false;                
                mostrarCamposGramaje();
                mostrarEmailCel();
                mostrarSerieVehiculo();
                this.limpiarFormSubTipoPrenda();
                this.limpiarFormGramaje();
            }
            if (txtPrendaTipotCodigoAnterior.Text == txtCodigoPrendaTipo.Text || txtCodigoPrendaTipo.Text == "") return;                           
            this.limpiarFormSubTipoPrenda();
            this.limpiarFormGramaje();
            Controller.clsCtrlSucursalPlazo ctrlSucursalPlazo = new Controller.clsCtrlSucursalPlazo();
            ctrlSucursalPlazo.sucursalPlazo.sucursal.intId = this.clsView.login.intSucursalId;
            string strCodigo = ctrlSucursalPlazo.sucursalPlazo.prendasTipo.generaCodigoConsecutivo(txtCodigoPrendaTipo.Text);
            ctrlSucursalPlazo.sucursalPlazo.prendasTipo.strCodigo = strCodigo;
            ctrlSucursalPlazo.BuscarSucTipoPrenda();

            if (ctrlSucursalPlazo.bolResutladoPaginacion)
            {
                txtCodigoPrendaTipo.Text = ctrlSucursalPlazo.sucursalPlazo.prendasTipo.strCodigo;
                txtPrendaTipotId.Text = ctrlSucursalPlazo.sucursalPlazo.prendasTipo.intId.ToString();
                txtDescripcionPrendaTipo.Text = ctrlSucursalPlazo.sucursalPlazo.prendasTipo.strDescripcion;
                txtPlazoTope.Text = ctrlSucursalPlazo.sucursalPlazo.ftPlazo.ToString();
                txtPlazo.Value = Convert.ToDecimal(ctrlSucursalPlazo.sucursalPlazo.ftPlazo.ToString(), new CultureInfo("en-US"));
                //txtGramaje.Text = ctrlSucursalPlazo.sucursalPlazo.prendasTipo.strCapturarGramaje;

                if (txtPlazo.Value > 0)
                {
                    dtpFechaVencimiento.Text = dtpFecha.Text;
                    dtpFechaVencimiento.Text = dtpFecha.Value.AddMonths(int.Parse(txtPlazo.Text)).ToString();
                }
                this.bolGramaje = false;
                this.bolIMEICelular = false;
                this.bolSerieVehiculo = false;


                if (ctrlSucursalPlazo.sucursalPlazo.prendasTipo.asignarCapturar(ctrlSucursalPlazo.sucursalPlazo.prendasTipo.strCapturarGramaje))
                {

                    this.bolGramaje = true;
                    txtPrestamo.Enabled = false;
                }
                else if (ctrlSucursalPlazo.sucursalPlazo.prendasTipo.asignarCapturar(ctrlSucursalPlazo.sucursalPlazo.prendasTipo.strCapturarEmailCelular))
                {
                    this.bolIMEICelular = true;
                    txtPrestamo.Enabled = true;
                }
                else if (ctrlSucursalPlazo.sucursalPlazo.prendasTipo.asignarCapturar(ctrlSucursalPlazo.sucursalPlazo.prendasTipo.strCapturarSerieVehiculo))
                {
                    this.bolSerieVehiculo = true;
                    txtPrestamo.Enabled = true;
                }
                else
                {
                    txtPrestamo.Enabled = true;
                }

                txtGramajeId.Text = "";
                txtGramajeDescripcion.Text = "";
                txtCodigoGramaje.Text = "";

                txtKilataje.Text = "";
                txtGramos.Text = "";


            }
            else
            {
                ctrlSucursalPlazo.sucursalPlazo.limpiarProps();
                txtCodigoPrendaTipo.Text = "";
                txtPrendaTipotId.Text = "";
                txtDescripcionPrendaTipo.Text = "";
                txtPlazoTope.Text = "";
                txtPlazo.Value = 0;
                //txtGramaje.Text = "";

                txtSubTipoPrendaId.Text = "";
                txtCodigoSubTipoPrenda.Text = "";
                txtDescripcionSubTipoPrenda.Text = "";

                txtGramajeId.Text = "";
                txtGramajeDescripcion.Text = "";
                txtCodigoGramaje.Text = "";

                txtKilataje.Text = "";
                txtGramos.Text = "";

                txtCodigoPrendaTipo.Focus();

                this.bolGramaje = false;
                this.bolIMEICelular = false;
                this.bolSerieVehiculo = false;

            }

            
            mostrarCamposGramaje();
            mostrarEmailCel();
            mostrarSerieVehiculo();

        }

        private void txtCodigoSubTipoPrenda_Leave(object sender, EventArgs e)
        {

            if (txtCodigoSubTipoPrenda.Text == "")
            {
                txtSubTipoPrendaId.Text = "";
                txtCodigoSubTipoPrenda.Text = "";
                txtDescripcionSubTipoPrenda.Text = "";
                return;
            }
            

            if (txtPrendaTipotId.Text == "") 
            {
                txtSubTipoPrendaId.Text = "";
                txtCodigoSubTipoPrenda.Text = "";
                txtDescripcionSubTipoPrenda.Text = "";
                return;
            }
            Controller.clsCtrlPrendasSubtipos ctrlPrendasSubtipos = new Controller.clsCtrlPrendasSubtipos();
            string strCodigo = ctrlPrendasSubtipos.prendasSubtipos.generaCodigoConsecutivo(txtCodigoSubTipoPrenda.Text);
            ctrlPrendasSubtipos.BuscarPrendaTipoId(strCodigo, txtPrendaTipotId.Text);

            if (!ctrlPrendasSubtipos.bolResultado)
            {

                txtSubTipoPrendaId.Text = "";
                txtCodigoSubTipoPrenda.Text = "";
                txtDescripcionSubTipoPrenda.Text = "";
                txtCodigoSubTipoPrenda.Focus();
                return;
            }

            txtSubTipoPrendaId.Text = ctrlPrendasSubtipos.prendasSubtipos.intId.ToString();
            txtCodigoSubTipoPrenda.Text = ctrlPrendasSubtipos.prendasSubtipos.strCodigo;
            txtDescripcionSubTipoPrenda.Text = ctrlPrendasSubtipos.prendasSubtipos.strDescripcion;
        }


        private void btnBusquedaCliente_Click(object sender, EventArgs e)
        {
            this.mostraBusquedaCliente();
        }



        private void txtBusquedaTipoPrenda_Click(object sender, EventArgs e)
        {
            this.mostraBusquedaTipoPrenda();
        }

        private void txtBusquedaSubTipoPrenda_Click(object sender, EventArgs e)
        {
            this.mostraBusquedaSubTipoPrenda();
        }
        private void btnVerificarHuellaCliente_Click(object sender, EventArgs e)
        {
            VerificationForm frmVerificarHuella = new VerificationForm();
            frmVerificarHuella.objDataSet = this.ctrlClientes.objDataSet;

            frmVerificarHuella.ShowDialog();

            if (frmVerificarHuella.bolResultado)
            {
                this.ctrlClientes.BuscarHuella(frmVerificarHuella.objDataSetSingle);
                this.llenar_formulario_clientes();
            }
           
        }

        public bool validacion()
        {
            this.clsView.principal.epMensaje.Clear();
            bool bolValidacion = true;
            

            string strFolio = txtBoleta.Text;
            //string strBoletaInicial = txtBoletaInicial.Text;
            string strNombreEmpleado = txtNombreEmpleado.Text;
            string strFechaBoleta = dtpFecha.Text;
            string strHora = txtHora.Text;
            string strPlazo = txtPlazo.Text;
            string strFechaVencimiento = dtpFechaVencimiento.Text;
            string strClienteId = txtClienteId.Text;
            string strIdentificacionValidar = txtIdentificacionValidar.Text;
            string strIdIdentificacion = txtIdIdentificacion.Text;
            string strRefrendoA = txtRefrendoA.Text;
            string strRefrendadaCon = txtRefrendadaCon.Text;
            string strSaldoPPI = txtSaldoPPIs.Text;
            string strPrestamo = txtPrestamo.Text;
            string strAvaluo = txtAvaluo.Text;

            string strPrendaTipoId = txtPrendaTipotId.Text;
            string strComentario = txtComentario.Text;
            string strSubTipoPrendaId = txtSubTipoPrendaId.Text;
            string strCantidad = txtCantidad.Text;
            string strKilataje = txtKilataje.Text;
            string strGramo = txtGramos.Text;
            string strDescripcion = txtDescripcion.Text;
            string strGramajesId = txtGramajeId.Text;
            string strIMEI = txtIMEI.Text;

            int intTipoSerieVehiculo = cmbTipoSerieVehiculo.SelectedIndex;
            string strNumeroSerieVehiculo = txtNumeroSerieVehiculo.Text;

            if (strFolio == "")
            {
                btnBusqueda.Focus();
                this.clsView.principal.epMensaje.SetError(btnBusqueda, this.clsView.mensajeErrorValiForm2("El campo boleta es obligatorio, favor de ingresar una boleta "));
                bolValidacion = false;
            }

            if (strPlazo == "" || strPlazo == "0")
            {
                txtPlazo.Focus();
                this.clsView.principal.epMensaje.SetError(txtPlazo, this.clsView.mensajeErrorValiForm2("El campo plazo es obligatorio, favor de ingresar un plazo "));
                bolValidacion = false;
            }

            if (strClienteId == "")
            {
                txtCodigoCliente.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigoCliente, this.clsView.mensajeErrorValiForm2("El campo cliente es obligatorio, favor de ingresar un cliente "));
                bolValidacion = false;
            }

            if (strIdIdentificacion == "" || strIdIdentificacion == "0")
            {
                txtCodigoIdentificacion.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigoIdentificacion, this.clsView.mensajeErrorValiForm2("El campo identificación es obligatorio, favor de ingresar una identificación "));
                bolValidacion = false;
            }


            if (strPrestamo == "")
            {
                txtPrestamo.Focus();
                this.clsView.principal.epMensaje.SetError(txtPrestamo, this.clsView.mensajeErrorValiForm2("El campo préstamo es obligatorio, favor de ingresar un préstamo "));
                bolValidacion = false;
            }

            if (strPrendaTipoId == "")
            {
                txtCodigoPrendaTipo.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigoPrendaTipo, this.clsView.mensajeErrorValiForm2("El campo tipo de prenda es obligatorio, favor de ingresar un tipo de prenda "));
                bolValidacion = false;
            }

            if (strSubTipoPrendaId == "")
            {
                txtCodigoSubTipoPrenda.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigoSubTipoPrenda, this.clsView.mensajeErrorValiForm2("El campo subtipo de prenda es obligatorio, favor de ingresar un subtipo de prenda "));
                bolValidacion = false;
            }

            if (strCantidad == "")
            {
                txtCantidad.Focus();
                this.clsView.principal.epMensaje.SetError(txtCantidad, this.clsView.mensajeErrorValiForm2("El campo cantidad es obligatorio, favor de ingresar una cantidad "));
                bolValidacion = false;
            }

            if (this.bolGramaje)
            {
                if (strKilataje == "")
                {
                    txtKilataje.Focus();
                    this.clsView.principal.epMensaje.SetError(txtKilataje, this.clsView.mensajeErrorValiForm2("El campo kilataje es obligatorio, favor de ingresar un kilataje "));
                    bolValidacion = false;
                }

                if (strGramo == "" || strGramo == "0")
                {
                    txtGramos.Focus();
                    this.clsView.principal.epMensaje.SetError(txtGramos, this.clsView.mensajeErrorValiForm2("El campo gramos es obligatorio, favor de ingresar gramos "));
                    bolValidacion = false;
                }

                if (strGramajesId == "")
                {
                    txtCodigoGramaje.Focus();
                    this.clsView.principal.epMensaje.SetError(txtCodigoGramaje, this.clsView.mensajeErrorValiForm2("El campo gramaje es obligatorio, favor de ingresar un gramaje "));
                    bolValidacion = false;
                }
            }

            if (this.bolIMEICelular) 
            {
                if (strIMEI == "" || strIMEI.Length < 15 )
                {
                    txtIMEI.Focus();
                    this.clsView.principal.epMensaje.SetError(txtIMEI, this.clsView.mensajeErrorValiForm2("El campo IMEI es obligatorio, favor de ingresar serie numérica "));
                    bolValidacion = false;
                }
            }

            if (this.bolSerieVehiculo) 
            {

                

                if (intTipoSerieVehiculo == 0)
                {
                    cmbTipoSerieVehiculo.Focus();
                    this.clsView.principal.epMensaje.SetError(cmbTipoSerieVehiculo, this.clsView.mensajeErrorValiForm2("El campo Fra/Pedimento es obligatorio, favor de seleccionar una opción "));
                    bolValidacion = false;
                }

                if (strNumeroSerieVehiculo == "")
                {
                    txtNumeroSerieVehiculo.Focus();
                    this.clsView.principal.epMensaje.SetError(txtNumeroSerieVehiculo, this.clsView.mensajeErrorValiForm2("El campo No. Serie es obligatorio, favor de ingresar un No. Serie "));
                    bolValidacion = false;
                }
            }

            if (strDescripcion == "")
            {
                txtDescripcion.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("El campo descripción es obligatorio, favor de ingresar una descripción "));
                bolValidacion = false;
            }

            if (this.strFoto == "")
            {
                btnFotografia.Focus();
                this.clsView.principal.epMensaje.SetError(btnFotografia, this.clsView.mensajeErrorValiForm2("La fotografía es obligatoria, favor de capturarla "));
                bolValidacion = false;
            }



            return bolValidacion;
        }

        public void guardar()
        {
            if (this.validacion())
            {

                bool bolValidacion = false;                
                if (this.clsView.login.usuario.validarAutorizacion() && this.ctrlClientes.cliente.validarClienteNoDeseado())
                {

                    if (this.clsView.confirmacionGuardarCliente(this.Text))
                    {
                        bolValidacion = true;
                    }
                }
                else
                {
                    bolValidacion = true;
                }


                if (bolValidacion)
                {
                   
                    frmGuardar frmGuardar = new frmGuardar();
                    frmGuardar.bolEditar = (this.ctrlBoletaEmpeno.boletasEmpeno.intId > 0)? true: false;                    
                    frmGuardar.ShowDialog();
                    if (frmGuardar.bolResultado) 
                    {

                        if (this.bolNuevo)
                        {

                            //Folio de pagos
                            Controller.clsCtrlFolios ctrlFolios = new Controller.clsCtrlFolios();
                            ctrlFolios.BuscarFolioProceso(this.clsView.strProcesosId, this.clsView.login.intSucursalId.ToString());
                            if (ctrlFolios.folio.intConsecutivo == 0)
                            {
                                this.clsView.mensajeErrorValiForm2("No es posible generar el folio, comuníquese con el administrador.");
                                tsbGuardar.Enabled = false;
                                btnFooterGuardar.Enabled = false;
                            }
                            else
                            {
                                this.ctrlBoletaEmpeno.boletasEmpeno.strFolio = ctrlFolios.folio.regresarFolioConsecutivo(this.intNumCerosConsecutivo);
                            }


                        }

                        this.strFoto = "boletaEmpeno" + this.ctrlBoletaEmpeno.boletasEmpeno.strFolio + ".jpg";

                        this.ctrlBoletaEmpeno.boletasEmpeno.intUsuario = this.clsView.login.usuario.intId;
                        //this.ctrlBoletaEmpeno.boletasEmpeno.strFolio = txtBoleta.Text;
                        this.ctrlBoletaEmpeno.boletasEmpeno.strBoletaInicial = txtBoletaInicial.Text;
                        this.ctrlBoletaEmpeno.boletasEmpeno.empleado.intId = this.clsView.login.usuario.intId;                        
                        this.ctrlBoletaEmpeno.boletasEmpeno.strHora = txtHora.Text;
                        this.ctrlBoletaEmpeno.boletasEmpeno.dtFechaBoleta = Convert.ToDateTime(dtpFecha.Value.ToString("yyyy-MM-dd") + " " + this.ctrlBoletaEmpeno.boletasEmpeno.strHora);
                        this.ctrlBoletaEmpeno.boletasEmpeno.strHora = txtHora.Text;
                        this.ctrlBoletaEmpeno.boletasEmpeno.intPlazo = int.Parse(txtPlazo.Text);
                        this.ctrlBoletaEmpeno.boletasEmpeno.dtFechaVencimiento = Convert.ToDateTime(dtpFechaVencimiento.Text);
                        this.ctrlBoletaEmpeno.boletasEmpeno.cliente.intId = int.Parse(txtClienteId.Text);

                        //string strIdentificacionValidar = txtIdentificacionValidar.Text;
                        this.ctrlBoletaEmpeno.boletasEmpeno.identificacionesTipo.intId = int.Parse(txtIdIdentificacion.Text);
                        this.ctrlBoletaEmpeno.boletasEmpeno.decPrestamo = decimal.Parse(txtPrestamo.Text.Replace("$", "").Replace(",", ""));
                        this.ctrlBoletaEmpeno.boletasEmpeno.decImporteAvaluo = decimal.Parse(txtAvaluo.Text.Replace("$", "").Replace(",", ""));
                        this.ctrlBoletaEmpeno.boletasEmpeno.prendaTipo.intId = int.Parse(txtPrendaTipotId.Text);

                        this.ctrlBoletaEmpeno.boletasEmpeno.strComentario = txtComentario.Text;
                        this.ctrlBoletaEmpeno.boletasEmpeno.subTipoPrenda.intId = int.Parse(txtSubTipoPrendaId.Text);
                        this.ctrlBoletaEmpeno.boletasEmpeno.intCantidad = int.Parse(txtCantidad.Text);
                        //Si el tipo de prenda incluye gramaje
                        if (this.bolGramaje)
                        {
                            this.ctrlBoletaEmpeno.boletasEmpeno.flKilataje = float.Parse(txtKilataje.Text);
                            this.ctrlBoletaEmpeno.boletasEmpeno.flGramos = float.Parse(txtGramos.Text);
                            this.ctrlBoletaEmpeno.boletasEmpeno.gramaje.intId = int.Parse(txtGramajeId.Text);
                        }
                        else 
                        {
                            this.ctrlBoletaEmpeno.boletasEmpeno.flKilataje = 0;
                            this.ctrlBoletaEmpeno.boletasEmpeno.flGramos = 0;
                            this.ctrlBoletaEmpeno.boletasEmpeno.gramaje.intId = 0;
                        }

                        if (this.bolIMEICelular)
                        {
                            this.ctrlBoletaEmpeno.boletasEmpeno.strIMEICelular = txtIMEI.Text;
                        }
                        else 
                        {
                            this.ctrlBoletaEmpeno.boletasEmpeno.strIMEICelular = "";
                        }

                        if (this.bolSerieVehiculo)
                        {
                            this.ctrlBoletaEmpeno.boletasEmpeno.strTipoSerieVehiculo = cmbTipoSerieVehiculo.SelectedItem.ToString();
                            this.ctrlBoletaEmpeno.boletasEmpeno.strNumeroSerieVehiculo = txtNumeroSerieVehiculo.Text;
                        }
                        else 
                        {
                            this.ctrlBoletaEmpeno.boletasEmpeno.strTipoSerieVehiculo = "";
                            this.ctrlBoletaEmpeno.boletasEmpeno.strNumeroSerieVehiculo = "";
                        }

                        this.ctrlBoletaEmpeno.boletasEmpeno.strDescripcion = txtDescripcion.Text;
                        this.ctrlBoletaEmpeno.boletasEmpeno.strFotografiaImagen = this.strFoto;
                        this.ctrlBoletaEmpeno.boletasEmpeno.intSucursal = this.clsView.login.intSucursalId;
                        this.ctrlBoletaEmpeno.boletasEmpeno.caja.intId = this.clsView.login.caja.intId;

                        this.ctrlUsuario.BuscarHuella(frmGuardar.objDataSetSingle);                        


                        //Si el usuario que cuenta con el privilegio de autorizacion acepto el aumento de precio por kilataje
                        if (this.objDataSetSingleAutorizarKilataje != null)
                        {
                            this.ctrlUsuario.BuscarHuella(this.objDataSetSingleAutorizarKilataje);
                            this.ctrlBoletaEmpeno.boletasEmpeno.usuarioAutorizacion.intId = this.ctrlUsuario.usuario.intId;
                            this.ctrlBoletaEmpeno.boletasEmpeno.usuarioAutorizacion.dtFecha = this.dtpFechaAutorizacionKilataje;
                        } else if (this.bolAutorizarKilataje) 
                        {
                            //Asignar los datos del usuario que se encuentra logeado
                            this.ctrlBoletaEmpeno.boletasEmpeno.usuarioAutorizacion.intId = this.ctrlUsuario.usuario.intId;
                            this.ctrlBoletaEmpeno.boletasEmpeno.usuarioAutorizacion.dtFecha = this.dtpFechaAutorizacionKilataje;
                        }

                        


                        this.ctrlBoletaEmpeno.boletasEmpeno.intProcesoMenuId = int.Parse(this.clsView.strProcesosId);


                        this.ctrlFoliosProcesos.BuscarFolioProcesoMenu(int.Parse(this.clsView.strProcesosId), this.clsView.login.intSucursalId);

                        if (this.ctrlFoliosProcesos.mdlFolioProcesos.bolResultado)
                        {
                            this.ctrlBoletaEmpeno.boletasEmpeno.intFolioId = this.ctrlFoliosProcesos.foliosProcesos.folio.intId;

                            this.ctrlBoletaEmpeno.Guardar();
                        }
                        else
                        {
                            this.clsView.mensajeErrorValiForm2("No es posible generar el folio, comuníquese con el administrador.");
                        }

                        if (this.ctrlBoletaEmpeno.mdlBoletaEmpeno.bolResultado)
                        {
                            if (!(this.pbFoto == null))
                            {
                                string pathImagenBoleta = Application.StartupPath.Replace("bin\\Debug", "Resources\\boletaEmpeno\\");
                                this.pbFoto.Image.Save(pathImagenBoleta + this.ctrlBoletaEmpeno.boletasEmpeno.strFotografiaImagen, System.Drawing.Imaging.ImageFormat.Jpeg);
                            }

                            //Hacer un llamado a la funcion para obtener los datos del registro(modificado)
                            this.regresarRegistro("guardó");
                        }

                    }

                }
            }
        }

        private void txtCodigoCliente_KeyPress(object sender, KeyPressEventArgs e)
        {

            this.clsView.soloNumVali(e);

        }
        private void txtPrestamo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumValiDec(sender, e);
        }

        private void txtCantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }


        private void txtCodigoPrendaTipo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtCodigoEmpleado_KeyPress(object sender, KeyPressEventArgs e)
        {

            this.clsView.soloNumVali(e);

        }

        private void txtIMEI_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtCodigoSubTipoPrenda_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        public void llenar_formulario_clientes()
        {

            if (this.ctrlClientes.cliente.strCodigo.Length == 0) return;

            txtClienteId.Text = this.ctrlClientes.cliente.intId.ToString();
            txtCodigoCliente.Text = this.ctrlClientes.cliente.strCodigo;
            txtNombreCliente.Text = this.ctrlClientes.cliente.strNombre + " " + this.ctrlClientes.cliente.strApellidoPaterno + " " + this.ctrlClientes.cliente.strApellidoMaterno;

            bool bolEstatus = true;

            if (this.ctrlClientes.cliente.strEstatus == this.ctrlClientes.cliente.dicEstatus["NO DESEADO"])
            {

                string strMsjAdmin = "";
                if (this.clsView.login.usuario.strAutorizar == this.clsView.login.usuario.dicAutorizar["NO"])
                {
                    bolEstatus = false;
                    strMsjAdmin = ", favor de contactar al administrador";

                }
                else
                {
                    bolEstatus = true;
                }

                this.clsView.principal.epMensaje.SetError(txtNombreCliente, this.clsView.mensajeErrorValiForm2("El cliente " + this.ctrlClientes.cliente.strNombre + " " + this.ctrlClientes.cliente.strApellidoPaterno + " " + this.ctrlClientes.cliente.strApellidoMaterno + " es un cliente no deseado" + strMsjAdmin + "."));
            }

            if ((this.clsView.login.usuario.validarMovimientos() || this.ctrlBoletaEmpeno.boletasEmpeno.intId == 0) && this.clsView.bolPermisosGuardar) 
            {
                btnFooterGuardar.Enabled = bolEstatus;
                tsbGuardar.Enabled = bolEstatus;
            }
                
        }

        private void limpiar_textbox()
        {

            this.clsView.limpiar_textbox(this);
            lblEstatus.Text = "";
            dtpFecha.MinDate = DateTime.Today;
            dtpFecha.Text = DateTime.Today.ToString();            
            txtBoletaInicial.Text = "";
            dtpFechaVencimiento.Text = dtpFecha.Text;            
            txtKilataje.Text = "";
            txtCodigoGramaje.Text = "";
            txtGramajeDescripcion.Text = "";
            txtGramajeId.Text = "";
            txtPrestamo.Enabled = true;
            txtPrestamoAnt.Text = "";
            txtAvaluo.Text = "";
            this.objDataSetSingleAutorizarKilataje = null;
            this.dtpFechaAutorizacionKilataje = DateTime.Now;
            this.bolAutorizarKilataje = false;
            btnVerCancelacion.Visible = false;
            this.clsView.principal.epMensaje.Clear();
            this.bolNuevo = false;
            this.bolGramaje = false;
            this.bolIMEICelular = false;
            this.bolSerieVehiculo = false;
            btnUndo.Visible = true;
            btnUndo.Enabled = true;
            lblDescEstatus.Visible = false;
            lblEstatus.Visible = false;
            btnFooterGuardar.Visible = true;

            if (this.clsView.bolPermisosNuevo) 
            {
                tsbNuevo.Enabled = true;
            }

            if (this.clsView.login.usuario.validarMovimientos() && this.clsView.bolPermisosGuardar)
            {
                btnFooterGuardar.Enabled = true;
                tsbGuardar.Enabled = true;
            }
            else 
            {
                btnFooterGuardar.Enabled = false;
                tsbGuardar.Enabled = false;
            }


            if (this.clsView.login.usuario.validarMovimientos() && this.clsView.bolPermisosEliminar)
            {
                tsbEliminar.Enabled = true;
            }
            else 
            {
                tsbEliminar.Enabled = false;
            }
            
            
            tsbEliminar.Visible = true;
            
            mostrarCamposGramaje();
            mostrarEmailCel();
            mostrarSerieVehiculo();
        }

        private void mostraBusqueda()
        {
            this.ctrlBoletaEmpeno.Paginacion(this.clsView.login.intSucursalId);
            if (this.ctrlBoletaEmpeno.bolResutladoPaginacion)
            {
                this.ctrlBoletaEmpeno.CargarFrmBusqueda();
                if (this.ctrlBoletaEmpeno.bolResultadoBusqueda)
                {

                    this.llenar_formulario();
                }
            }
        }

        private void mostraBusquedaCliente()
        {
            this.ctrlClientes.Paginacion();
            if (this.ctrlClientes.bolResutladoPaginacion)
            {
                this.ctrlClientes.CargarFrmBusqueda();

                if (this.ctrlClientes.bolResultadoBusqueda)
                {
                    this.llenar_formulario_clientes();
                }
            }

        }

        private void mostraBusquedaTipoPrenda()
        {            
            Controller.clsCtrlSucursalPlazo ctrlSucursalPlazo = new Controller.clsCtrlSucursalPlazo();
            ctrlSucursalPlazo.PaginacionSucursalInner(this.clsView.login.intSucursalId);

            if (ctrlSucursalPlazo.bolResutladoPaginacion)
            {
                ctrlSucursalPlazo.CargarFrmBusqueda();

                if (ctrlSucursalPlazo.bolResultadoBusqueda)
                {
                    this.limpiarFormSubTipoPrenda();
                    this.limpiarFormGramaje();
                    

                    txtCodigoPrendaTipo.Text = ctrlSucursalPlazo.sucursalPlazo.prendasTipo.strCodigo;
                    txtPrendaTipotId.Text = ctrlSucursalPlazo.sucursalPlazo.prendasTipo.intId.ToString();
                    txtDescripcionPrendaTipo.Text = ctrlSucursalPlazo.sucursalPlazo.prendasTipo.strDescripcion;
                    txtPlazoTope.Text = ctrlSucursalPlazo.sucursalPlazo.ftPlazo.ToString();
                    txtPlazo.Value = Convert.ToDecimal(ctrlSucursalPlazo.sucursalPlazo.ftPlazo.ToString(), new CultureInfo("en-US"));
                    //txtGramaje.Text = ctrlSucursalPlazo.sucursalPlazo.prendasTipo.strCapturarGramaje;
                    if (txtPlazo.Value > 0)
                    {
                        dtpFechaVencimiento.Text = dtpFecha.Text;
                        dtpFechaVencimiento.Text = dtpFecha.Value.AddMonths(int.Parse(txtPlazo.Text)).ToString();
                    }
                    this.bolGramaje = false;
                    this.bolIMEICelular = false;
                    this.bolSerieVehiculo = false;


                    if (ctrlSucursalPlazo.sucursalPlazo.prendasTipo.asignarCapturar(ctrlSucursalPlazo.sucursalPlazo.prendasTipo.strCapturarGramaje))
                    {
                        this.bolGramaje = true;
                        txtPrestamo.Enabled = false;
                    }
                    else if (ctrlSucursalPlazo.sucursalPlazo.prendasTipo.asignarCapturar(ctrlSucursalPlazo.sucursalPlazo.prendasTipo.strCapturarEmailCelular))
                    {
                        this.bolIMEICelular = true;
                        txtPrestamo.Enabled = true;
                    }
                    else if (ctrlSucursalPlazo.sucursalPlazo.prendasTipo.asignarCapturar(ctrlSucursalPlazo.sucursalPlazo.prendasTipo.strCapturarSerieVehiculo)) 
                    {
                        this.bolSerieVehiculo = true;
                        txtPrestamo.Enabled = true;
                    }
                    else
                    {
                        txtPrestamo.Enabled = true;
                    }
                        mostrarCamposGramaje();                  
                        mostrarEmailCel();                   
                        mostrarSerieVehiculo();
                }

            } 

        }


        private void mostrarCamposGramaje()
        {

            lblQuilataje.Visible = this.bolGramaje;
            lblGramos.Visible = this.bolGramaje;
            lblGramaje.Visible = this.bolGramaje;
            txtKilataje.Visible = this.bolGramaje;
            txtGramos.Visible = this.bolGramaje;
            txtCodigoGramaje.Visible = this.bolGramaje;
            txtGramajeDescripcion.Visible = this.bolGramaje;
            btnBusquedaGramaje.Visible = this.bolGramaje;

            /*Limpiar las siguentes cajas de texto*/
            txtGramajeId.Text = "";
            txtCodigoGramaje.Text = "";
        }

        private void mostrarEmailCel()
        {
            lblIMEI.Visible = this.bolIMEICelular;
            txtIMEI.Visible = this.bolIMEICelular;
            lblIMEI.Location = lblGramaje.Location;
            txtIMEI.Location = txtCodigoGramaje.Location;
            
        }

        private void mostrarSerieVehiculo() 
        {
            lblTipoSerieVehiculo.Visible = this.bolSerieVehiculo;
            cmbTipoSerieVehiculo.Visible = this.bolSerieVehiculo;
            cmbTipoSerieVehiculo.SelectedIndex = 0;
            lblTipoSerieVehiculo.Visible = this.bolSerieVehiculo;
            txtNumeroSerieVehiculo.Visible = this.bolSerieVehiculo;
            lblTipoSerieVehiculo.Location = lblGramaje.Location;
            cmbTipoSerieVehiculo.Location = txtCodigoGramaje.Location;
            lblSerieVehiculo.Visible = this.bolSerieVehiculo;
            lblSerieVehiculo.Location = new Point(720, 168);
            txtNumeroSerieVehiculo.Location = new Point(776, 168);
        }

        private void mostraBusquedaSubTipoPrenda()
        {

            if (txtPrendaTipotId.Text == "") return;
            Controller.clsCtrlPrendasSubtipos ctrlPrendasSubtipos = new Controller.clsCtrlPrendasSubtipos();
            ctrlPrendasSubtipos.PaginacionPrendaTipoId(txtPrendaTipotId.Text);
            if (ctrlPrendasSubtipos.bolResutladoPaginacion)
            {
                ctrlPrendasSubtipos.CargarFrmBusqueda();

                if (ctrlPrendasSubtipos.bolResultadoBusqueda)
                {
                    // if (ctrlPrendasSubtipos.prendasSubtipos.strCodigo.Length == 0) return;
                    txtSubTipoPrendaId.Text = ctrlPrendasSubtipos.prendasSubtipos.intId.ToString();
                    txtCodigoSubTipoPrenda.Text = ctrlPrendasSubtipos.prendasSubtipos.strCodigo;
                    txtDescripcionSubTipoPrenda.Text = ctrlPrendasSubtipos.prendasSubtipos.strDescripcion;
                }

            }
        }

        public void mostraBusquedaGramaje() 
        {
            if (txtPrendaTipotId.Text == "") return;
            this.ctrlGramajes.PaginacionPrendaSucursal(txtPrendaTipotId.Text, this.clsView.login.intSucursalId.ToString());
            if (!this.ctrlGramajes.bolResutladoPaginacion) return;

           
            this.ctrlGramajes.CargarFrmBusquedaTipoPrendaSuc();

            if (this.ctrlGramajes.bolResultadoBusqueda)
            {

                txtCodigoGramaje.Text = this.ctrlGramajes.gramaje.strCodigo;
                txtGramajeDescripcion.Text = this.ctrlGramajes.gramaje.strDescripcion;
                txtGramajeId.Text = this.ctrlGramajes.gramaje.intId.ToString();
                txtPrecioTope.Text = this.ctrlGramajes.gramaje.decPrecioTope.ToString();
                txtKilataje.Text = txtPrecioTope.Text;

                this.calcularPrestamoGramaje();
            }
        }

        public void limpiarFormSubTipoPrenda()
        {
            txtSubTipoPrendaId.Text = "";
            txtDescripcionSubTipoPrenda.Text = "";
            txtCodigoSubTipoPrenda.Text = "";
        }

        public void limpiarFormGramaje()
        {
            txtGramajeId.Text = "";
            txtGramajeDescripcion.Text = "";
            txtCodigoGramaje.Text = "";
            txtKilataje.Text = "";
            txtGramos.Text = "";
            txtPrestamo.Text = "";
            txtAvaluo.Text = "";
            txtIMEI.Text = "";
            txtNumeroSerieVehiculo.Text = "";
            cmbTipoSerieVehiculo.SelectedIndex = 0;
            
        }

        

        private void btnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void btnBusquedaIdentif_Click(object sender, EventArgs e)
        {
            this.mostraBusquedaIdentificacion();
        }

        private void txtCodigoIdentificacion_Leave(object sender, EventArgs e)
        {

            if (txtCodigoIdentificacion.Text == "") 
            {
                txtIdIdentificacion.Text = "";
                txtCodigoIdentificacion.Text = "";
                txtDescripcionIdentificacion.Text = "";
                return;

            } 
            Controller.clsCtrlIdentificacionesTipo ctrlIdentificacionesTipo = new Controller.clsCtrlIdentificacionesTipo();
            string strCodigo = ctrlIdentificacionesTipo.identificacionTipo.generaCodigoConsecutivo(txtCodigoIdentificacion.Text);
            ctrlIdentificacionesTipo.Buscar(strCodigo);

            if (ctrlIdentificacionesTipo.mdlIdentificacionesTipo.bolResultado)
            {
                txtIdIdentificacion.Text = ctrlIdentificacionesTipo.identificacionTipo.intId.ToString();
                txtCodigoIdentificacion.Text = ctrlIdentificacionesTipo.identificacionTipo.strCodigo;
                txtDescripcionIdentificacion.Text = ctrlIdentificacionesTipo.identificacionTipo.strDescripcion;
                txtIdentificacionValidar.Text = ctrlIdentificacionesTipo.identificacionTipo.strValidarCurp;
            }
            else 
            {
                txtIdIdentificacion.Text = "";
                txtCodigoIdentificacion.Text = "";
                txtDescripcionIdentificacion.Text = "";
                txtIdentificacionValidar.Text = "";
                txtCodigoIdentificacion.Focus();
            }
            
        }

        private void txtCodigoIdentificacion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        public void mostraBusquedaIdentificacion()
        {
            Controller.clsCtrlIdentificacionesTipo ctrlIdentificacionesTipo = new Controller.clsCtrlIdentificacionesTipo();
            ctrlIdentificacionesTipo.Paginacion();
            if (ctrlIdentificacionesTipo.bolResutladoPaginacion)
            {
                ctrlIdentificacionesTipo.CargarFrmBusqueda();

                if (ctrlIdentificacionesTipo.bolResultadoBusqueda)
                {
                    txtIdIdentificacion.Text = ctrlIdentificacionesTipo.identificacionTipo.intId.ToString();
                    txtCodigoIdentificacion.Text = ctrlIdentificacionesTipo.identificacionTipo.strCodigo;
                    txtDescripcionIdentificacion.Text = ctrlIdentificacionesTipo.identificacionTipo.strDescripcion;
                    txtIdentificacionValidar.Text = ctrlIdentificacionesTipo.identificacionTipo.strValidarCurp;
                }
            }
            
        }

        private void txtPlazo_Leave(object sender, EventArgs e)
        {

            this.calcularPlazo();
        }

        private void btnFotografia_Click(object sender, EventArgs e)
        {
            /*if (!this.bolNuevo)
            {
                this.clsView.principal.epMensaje.SetError(txtBoleta, this.clsView.mensajeErrorValiForm2("Deber de tener valor el campo boleta para registrar fotografía"));
            }
            else
            {

                frmFoto frmFoto = new frmFoto();
                frmFoto.ShowDialog();
                this.strFoto = "boletaEmpeno" + txtBoleta.Text + ".jpg";
                this.pbFoto = frmFoto.pbGenerico;
            }*/

            if (txtBoleta.Text == "")
            {
                this.clsView.principal.epMensaje.SetError(txtBoleta, this.clsView.mensajeErrorValiForm2("Deber de tener valor el campo boleta para registrar fotografía"));
            }
            else
            {

                frmFoto frmFoto = new frmFoto();
                frmFoto.ShowDialog();
                this.strFoto = "boletaEmpeno";
                this.pbFoto = frmFoto.pbGenerico;
            }
        }

        public void calcularPlazo()
        {
            if (txtPlazoTope.Text == "" || txtPrendaTipotId.Text == "") return;
            if (int.Parse(txtPlazo.Text) > float.Parse(txtPlazoTope.Text))
            {
                txtPlazo.Text = txtPlazoTope.Text;
                this.clsView.principal.epMensaje.SetError(txtPlazo, this.clsView.mensajeErrorValiForm2("El plazo no debe de exceder del plazo que tiene el tipo de prenda selecionado  "));
            }
            else
            {
                dtpFechaVencimiento.Text = dtpFecha.Text;
                dtpFechaVencimiento.Text = dtpFecha.Value.AddMonths(int.Parse(txtPlazo.Text)).ToString();

            }
        }

        private void txtBusquedaGramaje_Click(object sender, EventArgs e)
        {
            this.mostraBusquedaGramaje();
        }

        private void txtPrestamo_TextChanged(object sender, EventArgs e)
        {


        }

        private void txtKilataje_Leave(object sender, EventArgs e)
        {

            if (!(txtGramajeId.Text == "") && decimal.Parse(txtKilataje.Text) > decimal.Parse(txtPrecioTope.Text))
            {

                string strPrecioTope = txtPrecioTope.Text;
                string strMensaje = "El kilataje no puede exceder de " + this.clsView.convertMoneda(strPrecioTope);
                if (this.clsView.login.usuario.validarAutorizacion())
                {

                    strMensaje += ", ¿Desea aumentar el precio?";
                    if (!(this.clsView.confirmacionVerificacion(this.Text, strMensaje)))
                    {
                        txtKilataje.Text = strPrecioTope;

                    }
                    else 
                    {
                        this.dtpFechaAutorizacionKilataje = DateTime.Now;
                        this.bolAutorizarKilataje = true;
                    }

                }
                else
                {

                    strMensaje += ", ¿Desea pedir autorización?.";
                    //this.clsView.principal.epMensaje.SetError(txtKilataje, this.clsView.mensajeErrorValiForm2(strMensaje));*/
                    if (this.clsView.confirmacionAccion(this.Text, strMensaje))
                    {
                        frmAutorizar frmAutorizar = new frmAutorizar();
                        frmAutorizar.ShowDialog();
                        if (frmAutorizar.bolResultado)
                        {
                            this.objDataSetSingleAutorizarKilataje = frmAutorizar.objDataSetSingle;
                            this.dtpFechaAutorizacionKilataje = DateTime.Now;
                        }
                        else 
                        {
                            txtKilataje.Text = strPrecioTope;
                        }
                    }
                    else 
                    {
                        txtKilataje.Text = strPrecioTope;
                    }
                    
                }


            }
            this.calcularPrestamoGramaje();


        }

        private void txtGramos_Leave(object sender, EventArgs e)
        {
            this.calcularPrestamoGramaje();
        }

        public void calcularPrestamoGramaje()
        {
            if (txtGramos.Text == "" || txtKilataje.Text == "" || txtGramajeId.Text == "")
            {
                txtPrestamo.Text = "";
                txtAvaluo.Text = "";
                return;
            }


            

            txtPrestamo.Text = this.clsView.convertMoneda((decimal.Parse(txtGramos.Text) * decimal.Parse(txtKilataje.Text)).ToString());

            this.calcularAvaluo();
        }

        public void calcularAvaluo() 
        {
            decimal decPrestamo = decimal.Parse(txtPrestamo.Text.Replace("$", "").Replace(",", ""));
            txtAvaluo.Text = this.clsView.convertMoneda((decPrestamo + (decPrestamo * (this.clsCtrlSucursalParametros.sucursalParametro.decAvaluo / 100))).ToString());
        }
        private void txtKilataje_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumValiDec(sender, e);
        }

        private void txtGramos_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumValiDec(sender, e);
        }

        private void btnBusqueda_Click(object sender, EventArgs e)
        {
            mostraBusqueda();

        }

        private void btnHistorial_Click(object sender, EventArgs e)
        {
            if (txtClienteId.Text == "") return;
            Controller.clsCtrlBoletaEmpeno ctrlBoletaEmpeno = new Controller.clsCtrlBoletaEmpeno();
            ctrlBoletaEmpeno.PaginacionCliente(int.Parse(txtClienteId.Text));
            if (ctrlBoletaEmpeno.mdlBoletaEmpeno.bolResultado) 
            {
                ctrlBoletaEmpeno.CargarFrmBusquedaRangoFechaBoleta(this.ctrlClientes.cliente);
            }
            

        }

        public void llenar_formulario()
        {
            
            this.limpiar_textbox();

            if (this.ctrlBoletaEmpeno.boletasEmpeno.intId == 0)
            {
                lblDescEstatus.Visible = false;
                lblEstatus.Visible = false;

                return;
            }

            txtBoleta.Enabled = true;
            txtBoleta.Text = this.ctrlBoletaEmpeno.boletasEmpeno.strFolio;
            txtBoletaInicial.Text = this.ctrlBoletaEmpeno.boletasEmpeno.strBoletaInicial;
            dtpFecha.MinDate = this.ctrlBoletaEmpeno.boletasEmpeno.dtFechaBoleta;
            dtpFecha.Text = this.ctrlBoletaEmpeno.boletasEmpeno.dtFechaBoleta.ToString();
            txtHora.Text = this.ctrlBoletaEmpeno.boletasEmpeno.strHora;
            dtpFechaVencimiento.Text = this.ctrlBoletaEmpeno.boletasEmpeno.dtFechaVencimiento.ToString();
            txtClienteId.Text = this.ctrlBoletaEmpeno.boletasEmpeno.cliente.intId.ToString();
            txtCodigoCliente.Text = this.ctrlBoletaEmpeno.boletasEmpeno.cliente.strCodigo;
            txtNombreCliente.Text = this.ctrlBoletaEmpeno.boletasEmpeno.cliente.strNombre;
            txtNombreEmpleado.Text = this.ctrlBoletaEmpeno.boletasEmpeno.empleado.strNombre;
            txtIdIdentificacion.Text = this.ctrlBoletaEmpeno.boletasEmpeno.identificacionesTipo.intId.ToString();
            txtCodigoIdentificacion.Text = this.ctrlBoletaEmpeno.boletasEmpeno.identificacionesTipo.strCodigo;
            txtDescripcionIdentificacion.Text = this.ctrlBoletaEmpeno.boletasEmpeno.identificacionesTipo.strDescripcion;
            txtPrendaTipotId.Text = this.ctrlBoletaEmpeno.boletasEmpeno.prendaTipo.intId.ToString();
            txtPrendaTipotCodigoAnterior.Text = this.ctrlBoletaEmpeno.boletasEmpeno.prendaTipo.strCodigo;
            txtCodigoPrendaTipo.Text = this.ctrlBoletaEmpeno.boletasEmpeno.prendaTipo.strCodigo;
            txtDescripcionPrendaTipo.Text = this.ctrlBoletaEmpeno.boletasEmpeno.prendaTipo.strDescripcion;
            this.bolGramaje = this.ctrlBoletaEmpeno.boletasEmpeno.prendaTipo.asignarCapturar(this.ctrlBoletaEmpeno.boletasEmpeno.prendaTipo.strCapturarGramaje);

            if (this.ctrlBoletaEmpeno.boletasEmpeno.strIMEICelular == "")
            {
                this.bolIMEICelular = false;
            }
            else 
            {
                this.bolIMEICelular = true;
                txtIMEI.Text = this.ctrlBoletaEmpeno.boletasEmpeno.strIMEICelular;
            }

            if (this.ctrlBoletaEmpeno.boletasEmpeno.strNumeroSerieVehiculo == "")
            {
                this.bolSerieVehiculo = false;
            }
            else 
            {
                this.bolSerieVehiculo = true;

            }



            this.mostrarCamposGramaje();
            this.mostrarEmailCel();
            this.mostrarSerieVehiculo();


            if (this.bolSerieVehiculo) 
            {
                txtNumeroSerieVehiculo.Text = this.ctrlBoletaEmpeno.boletasEmpeno.strNumeroSerieVehiculo;
                cmbTipoSerieVehiculo.Text = this.ctrlBoletaEmpeno.boletasEmpeno.strTipoSerieVehiculo.ToString();                
            }

            txtPlazoTope.Text = this.ctrlBoletaEmpeno.boletasEmpeno.sucursalPlazo.ftPlazo.ToString();
            txtSubTipoPrendaId.Text = this.ctrlBoletaEmpeno.boletasEmpeno.subTipoPrenda.intId.ToString();
            txtCodigoSubTipoPrenda.Text = this.ctrlBoletaEmpeno.boletasEmpeno.subTipoPrenda.strCodigo;
            txtDescripcionSubTipoPrenda.Text = this.ctrlBoletaEmpeno.boletasEmpeno.subTipoPrenda.strDescripcion;

            txtGramajeId.Text = this.ctrlBoletaEmpeno.boletasEmpeno.gramaje.intId.ToString();
            txtCodigoGramaje.Text = this.ctrlBoletaEmpeno.boletasEmpeno.gramaje.strCodigo;
            txtGramajeDescripcion.Text = this.ctrlBoletaEmpeno.boletasEmpeno.gramaje.strDescripcion;

            txtPrecioTope.Text = this.ctrlBoletaEmpeno.boletasEmpeno.sucursalGramaje.ftPreciotope.ToString();
            txtKilataje.Text = this.ctrlBoletaEmpeno.boletasEmpeno.flKilataje.ToString();
            txtGramos.Text = this.ctrlBoletaEmpeno.boletasEmpeno.flGramos.ToString();
            txtCantidad.Text = this.ctrlBoletaEmpeno.boletasEmpeno.intCantidad.ToString();
            txtPlazo.Value = this.ctrlBoletaEmpeno.boletasEmpeno.intPlazo;
            txtPrestamo.Text = this.clsView.convertMoneda(this.ctrlBoletaEmpeno.boletasEmpeno.decPrestamo.ToString());
            txtPrestamoAnt.Text = this.ctrlBoletaEmpeno.boletasEmpeno.decPrestamo.ToString();
            txtAvaluo.Text = this.clsView.convertMoneda(this.ctrlBoletaEmpeno.boletasEmpeno.decImporteAvaluo.ToString());
            txtDescripcion.Text = this.ctrlBoletaEmpeno.boletasEmpeno.strDescripcion;
            txtComentario.Text = this.ctrlBoletaEmpeno.boletasEmpeno.strComentario;
            this.strFoto = this.ctrlBoletaEmpeno.boletasEmpeno.strFotografiaImagen;
            lblEstatus.Text = this.ctrlBoletaEmpeno.boletasEmpeno.strEstatus;

            txtCodigoCaja.Text = this.ctrlBoletaEmpeno.boletasEmpeno.caja.strCodigo;
            txtDescripcionCaja.Text = this.ctrlBoletaEmpeno.boletasEmpeno.caja.strDescripcion;

            if (this.ctrlBoletaEmpeno.boletasEmpeno.strEstatus == this.ctrlBoletaEmpeno.boletasEmpeno.dicEstatus["LIQUIDADA"])
            {
                txtPagoFolio.Text = this.ctrlBoletaEmpeno.boletasEmpeno.strPagoFolio;
                txtPagoFecha.Text = this.ctrlBoletaEmpeno.boletasEmpeno.strPagoFecha;
                btnUndo.Enabled = false;
                tsbEliminar.Visible = false;

            }
            else if (this.ctrlBoletaEmpeno.boletasEmpeno.strEstatus == this.ctrlBoletaEmpeno.boletasEmpeno.dicEstatus["ACTIVO"])
            {
                btnUndo.Enabled = true;
                if (this.clsView.bolPermisosEliminar) 
                {
                    tsbEliminar.Visible = true;
                }
                
            }
            else 
            {
                btnUndo.Enabled = false;
                tsbEliminar.Enabled = false;
            }
            txtRefrendoA.Text = this.ctrlBoletaEmpeno.boletasEmpeno.strBoletaRefrendada; 
            txtRefrendadaCon.Text = this.ctrlBoletaEmpeno.boletasEmpeno.strBoletaRefrendo;
            if (this.ctrlBoletaEmpeno.boletasEmpeno.decPPI > 0 && this.ctrlBoletaEmpeno.boletasEmpeno.strEstatus == this.ctrlBoletaEmpeno.boletasEmpeno.dicEstatus["ACTIVO"])
            {

                txtSaldoPPIs.Text = this.clsView.convertMoneda(this.ctrlBoletaEmpeno.boletasEmpeno.decPPI.ToString());
            }

            lblDescEstatus.Visible = true;
            lblEstatus.Visible = true;


            if (this.bolGramaje)
            {

                txtPrestamo.Enabled = false;
            }
            else
            {
                txtPrestamo.Enabled = true;
            }

            if (this.bolIMEICelular)
            {
                
                lblIMEI.Location = lblGramaje.Location;
                txtIMEI.Location = txtCodigoGramaje.Location;
                lblGramaje.Visible = false;
                txtCodigoGramaje.Visible = false;
                lblIMEI.Visible = true;
                txtIMEI.Visible = true;
            }
            else 
            {
              
                lblIMEI.Visible = false;
                txtIMEI.Visible = false;
            }

            if (this.clsView.login.usuario.validarMovimientos() && this.clsView.bolPermisosGuardar && this.ctrlBoletaEmpeno.boletasEmpeno.strEstatus == this.ctrlBoletaEmpeno.boletasEmpeno.dicEstatus["ACTIVO"])
            {
                tsbGuardar.Enabled = true;
                btnFooterGuardar.Enabled = true;
                btnFooterGuardar.Visible = true;
                btnVerCancelacion.Visible = false;                
                tsbReactivar.Visible = false;

            }
            else if (this.ctrlBoletaEmpeno.boletasEmpeno.strEstatus == "CANCELADA")
            {
                tsbGuardar.Enabled = false;
                btnFooterGuardar.Enabled = false;
                btnFooterGuardar.Visible = false;
                tsbEliminar.Visible = true;
                tsbEliminar.Enabled = false;
                btnVerCancelacion.Visible = true;
                tsbReactivar.Visible = false;
            }
            else 
            {
                tsbGuardar.Enabled = false;
                btnFooterGuardar.Enabled = false;
                btnFooterGuardar.Visible = false;
                tsbEliminar.Enabled = false;
                tsbEliminar.Visible = true;
                btnVerCancelacion.Visible = false;
                tsbReactivar.Visible = false;
            }

            if (this.clsView.login.usuario.validarMovimientos() && this.clsView.bolPermisosReactivar && this.ctrlBoletaEmpeno.boletasEmpeno.strEstatus == this.ctrlBoletaEmpeno.boletasEmpeno.dicEstatus["ADJUDICADA"])
            {
                tsbGuardar.Enabled = false;
                btnFooterGuardar.Enabled = false;
                tsbEliminar.Visible = false;
                btnVerCancelacion.Visible = false;
                tsbReactivar.Visible = true;
                tsbReactivar.Enabled = true;

            }
            
            
        }

        public void agregarDeshacer()
        {
            this.lsDeshacerBoletasEmpeno.RemoveAt(0);
            this.lsDeshacerBoletasEmpeno.Add(this.ctrlBoletaEmpeno.boletasEmpeno);
        }

        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void btnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtBoleta_Leave(object sender, EventArgs e)
        {
            if (txtBoleta.Text == "") 
            {
                limpiar_textbox();
                return;
            } 
            if (txtBoleta.Text.Length < this.ctrlBoletaEmpeno.boletasEmpeno.intNumCerosConsecutivo) 
            {
                if (this.ctrlFoliosAuto.folio.intConsecutivo == 0)
                {
                    this.clsView.mensajeErrorValiForm2("No es posible generar el folio, comuníquese con el administrador.");
                    tsbGuardar.Enabled = false;
                    btnFooterGuardar.Enabled = false;
                }
                else
                {
                    txtBoleta.Text = this.ctrlFoliosAuto.folio.regresarFolioConsecutivoAuto(this.ctrlBoletaEmpeno.boletasEmpeno.intNumCerosConsecutivo, txtBoleta.Text);
                }
            }
           


            this.ctrlBoletaEmpeno.Buscar(txtBoleta.Text);
            if (this.ctrlBoletaEmpeno.mdlBoletaEmpeno.bolResultado)
            {
                this.llenar_formulario();
            }
            else 
            {

                this.limpiar_textbox();
                txtBoleta.Text = "";
                txtBoleta.Focus();
            }
            
        }

        private void tsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlBoletaEmpeno.boletasEmpeno.intId > 0 && this.ctrlBoletaEmpeno.boletasEmpeno.strEstatus == this.ctrlBoletaEmpeno.boletasEmpeno.dicEstatus["ACTIVO"] && this.clsView.login.usuario.validarMovimientos())
            {
                if (this.clsView.confirmacionAccion("Motivo de cancelación", "¿Desea cancelar este registro?"))
                {
                    frmMotivosCancelacion frmMotivos = new frmMotivosCancelacion();
                    frmMotivos.strTipo = "G";
                    frmMotivos.ShowDialog();
                    if (frmMotivos.bolResultado)
                    {
                        this.ctrlBoletaEmpeno.boletasEmpeno.strMotivosCancelacion = frmMotivos.strMotivo;
                        this.ctrlBoletaEmpeno.boletasEmpeno.dtFecha = DateTime.Now;
                        this.ctrlBoletaEmpeno.boletasEmpeno.intUsuario = this.clsView.login.usuario.intId;
                       
                       
                        this.ctrlBoletaEmpeno.Cancelar();
                        if (this.ctrlBoletaEmpeno.bolResultado)
                        {
                            //Hacer un llamado a la funcion para obtener los datos del registro(modificado)
                            this.regresarRegistro("eliminó");

                        }
                    }
                }
            }


        }

        private void btnVerCancelacion_Click(object sender, EventArgs e)
        {
            frmMotivosCancelacion frmMotivos = new frmMotivosCancelacion();
            frmMotivos.strTipo = "V";
            frmMotivos.strMotivo = this.ctrlBoletaEmpeno.boletasEmpeno.strMotivosCancelacion + " \r\n\r\n\r\n Usuario cancelación: " + this.ctrlBoletaEmpeno.boletasEmpeno.strUsuarioEliminacion + "\r\n  Fecha: " + this.ctrlBoletaEmpeno.boletasEmpeno.strFechaEliminacion;
            frmMotivos.ShowDialog();
        }

        private void btnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtNumeroSerieVehiculo_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblSerieVehiculo_Click(object sender, EventArgs e)
        {

        }

        private void tsbReactivar_Click(object sender, EventArgs e)
        {
            if (this.clsView.confirmacionAccion("Reactivar", "¿Desea reactivar este registro?"))
            {
                this.ctrlBoletaEmpeno.boletasEmpeno.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlBoletaEmpeno.Reactivar();
                if (this.ctrlBoletaEmpeno.bolResultado)
                {
                    //Hacer un llamado a la funcion para obtener los datos del registro(modificado)
                    this.regresarRegistro("activó");
                }
            }
        }

        private void lkbCliente_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (txtClienteId.Text == "") return;
            Controller.clsCtrlBoletaEmpeno ctrlBoletaEmpeno = new Controller.clsCtrlBoletaEmpeno();
            ctrlBoletaEmpeno.PaginacionCliente(int.Parse(txtClienteId.Text));
            this.ctrlClientes.Buscar(txtCodigoCliente.Text);
            if (ctrlBoletaEmpeno.mdlBoletaEmpeno.bolResultado)
            {
                ctrlBoletaEmpeno.CargarFrmBusquedaRangoFechaBoleta(this.ctrlClientes.cliente);
            }


        }


        public void regresarRegistro(string stMsj)
        {            
            this.limpiar_textbox();
            this.ctrlBoletaEmpeno.Paginacion(this.clsView.login.intSucursalId);
            this.clsView.mensajeExitoDB(this.Text.ToLower(), stMsj);

            this.ctrlBoletaEmpeno.Buscar(this.ctrlBoletaEmpeno.boletasEmpeno.strFolio);
            this.llenar_formulario();
            txtBoleta.Enabled = true;
            txtBoleta.Focus();
        }

    }
}
