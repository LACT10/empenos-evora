﻿namespace EmpenosEvora
{
    partial class frmTipoMovimientosCaja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.cmbTipo = new System.Windows.Forms.ComboBox();
            this.btnFooterCierra = new System.Windows.Forms.Button();
            this.btnFooterGuardar = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.tsAcciones = new System.Windows.Forms.ToolStrip();
            this.tsbNuevo = new System.Windows.Forms.ToolStripButton();
            this.tsbGuardar = new System.Windows.Forms.ToolStripButton();
            this.tsbEliminar = new System.Windows.Forms.ToolStripButton();
            this.tsbBuscar = new System.Windows.Forms.ToolStripButton();
            this.btnUndo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrimeroPag = new System.Windows.Forms.ToolStripButton();
            this.btnAtras = new System.Windows.Forms.ToolStripButton();
            this.btnAdelante = new System.Windows.Forms.ToolStripButton();
            this.btnUlitmoPag = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbImprimir = new System.Windows.Forms.ToolStripButton();
            this.btnCierra = new System.Windows.Forms.ToolStripButton();
            this.btnBusqueda = new System.Windows.Forms.Button();
            this.txtDescripcionAnt = new System.Windows.Forms.TextBox();
            this.txtTipoAnt = new System.Windows.Forms.TextBox();
            this.tsAcciones.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 127;
            this.label1.Text = "Descripción";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcion.Location = new System.Drawing.Point(80, 72);
            this.txtDescripcion.MaxLength = 250;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(322, 20);
            this.txtDescripcion.TabIndex = 3;
            this.txtDescripcion.Leave += new System.EventHandler(this.txtDescripcion_Leave);
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(8, 40);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(28, 13);
            this.lblCodigo.TabIndex = 123;
            this.lblCodigo.Text = "Tipo";
            // 
            // cmbTipo
            // 
            this.cmbTipo.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmbTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTipo.FormattingEnabled = true;
            this.cmbTipo.Items.AddRange(new object[] {
            "Seleccione una opción",
            "ENTRADA",
            "SALIDA"});
            this.cmbTipo.Location = new System.Drawing.Point(80, 40);
            this.cmbTipo.Name = "cmbTipo";
            this.cmbTipo.Size = new System.Drawing.Size(152, 21);
            this.cmbTipo.TabIndex = 1;
            this.cmbTipo.SelectedIndexChanged += new System.EventHandler(this.cmbTipo_SelectedIndexChanged);
            // 
            // btnFooterCierra
            // 
            this.btnFooterCierra.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnFooterCierra.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterCierra.Location = new System.Drawing.Point(328, 120);
            this.btnFooterCierra.Name = "btnFooterCierra";
            this.btnFooterCierra.Size = new System.Drawing.Size(75, 40);
            this.btnFooterCierra.TabIndex = 5;
            this.btnFooterCierra.Text = "Salir";
            this.btnFooterCierra.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterCierra.UseVisualStyleBackColor = true;
            this.btnFooterCierra.Click += new System.EventHandler(this.BtnFooterCierra_Click);
            // 
            // btnFooterGuardar
            // 
            this.btnFooterGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterGuardar.Image = global::EmpenosEvora.Properties.Resources.Save_16x;
            this.btnFooterGuardar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterGuardar.Location = new System.Drawing.Point(240, 120);
            this.btnFooterGuardar.Name = "btnFooterGuardar";
            this.btnFooterGuardar.Size = new System.Drawing.Size(75, 40);
            this.btnFooterGuardar.TabIndex = 4;
            this.btnFooterGuardar.Text = "Guardar";
            this.btnFooterGuardar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterGuardar.UseVisualStyleBackColor = true;
            this.btnFooterGuardar.Click += new System.EventHandler(this.BtnFooterGuardar_Click);
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(-16, 104);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(496, 2);
            this.label8.TabIndex = 424;
            // 
            // tsAcciones
            // 
            this.tsAcciones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbNuevo,
            this.tsbGuardar,
            this.tsbEliminar,
            this.tsbBuscar,
            this.btnUndo,
            this.toolStripSeparator2,
            this.btnPrimeroPag,
            this.btnAtras,
            this.btnAdelante,
            this.btnUlitmoPag,
            this.toolStripSeparator1,
            this.tsbImprimir,
            this.btnCierra});
            this.tsAcciones.Location = new System.Drawing.Point(0, 0);
            this.tsAcciones.Name = "tsAcciones";
            this.tsAcciones.Size = new System.Drawing.Size(423, 25);
            this.tsAcciones.TabIndex = 427;
            this.tsAcciones.Text = "toolStrip1";
            // 
            // tsbNuevo
            // 
            this.tsbNuevo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbNuevo.Image = global::EmpenosEvora.Properties.Resources.AddFile_16x;
            this.tsbNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbNuevo.Name = "tsbNuevo";
            this.tsbNuevo.Size = new System.Drawing.Size(23, 22);
            this.tsbNuevo.Text = "Nuevo";
            this.tsbNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical90;
            this.tsbNuevo.Click += new System.EventHandler(this.TsbNuevo_Click);
            // 
            // tsbGuardar
            // 
            this.tsbGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbGuardar.Image = global::EmpenosEvora.Properties.Resources.Save_16x;
            this.tsbGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGuardar.Name = "tsbGuardar";
            this.tsbGuardar.Size = new System.Drawing.Size(23, 22);
            this.tsbGuardar.Tag = "";
            this.tsbGuardar.Text = "Guardar";
            this.tsbGuardar.Click += new System.EventHandler(this.TsbGuardar_Click);
            // 
            // tsbEliminar
            // 
            this.tsbEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbEliminar.Image = global::EmpenosEvora.Properties.Resources.Cancel_16x;
            this.tsbEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEliminar.Name = "tsbEliminar";
            this.tsbEliminar.Size = new System.Drawing.Size(23, 22);
            this.tsbEliminar.Text = "Cancelar";
            this.tsbEliminar.ToolTipText = "Desactivar";
            this.tsbEliminar.Click += new System.EventHandler(this.TsbEliminar_Click);
            // 
            // tsbBuscar
            // 
            this.tsbBuscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBuscar.Image = global::EmpenosEvora.Properties.Resources.VBSearch_16x;
            this.tsbBuscar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBuscar.Name = "tsbBuscar";
            this.tsbBuscar.Size = new System.Drawing.Size(23, 22);
            this.tsbBuscar.Text = "Buscar";
            this.tsbBuscar.Click += new System.EventHandler(this.tsbBuscar_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUndo.Image = global::EmpenosEvora.Properties.Resources.Undo_16x;
            this.btnUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(23, 22);
            this.btnUndo.Text = "Deshacer";
            this.btnUndo.Click += new System.EventHandler(this.BtnUndo_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrimeroPag
            // 
            this.btnPrimeroPag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrimeroPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeftGroup_16xSM;
            this.btnPrimeroPag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrimeroPag.Name = "btnPrimeroPag";
            this.btnPrimeroPag.Size = new System.Drawing.Size(23, 22);
            this.btnPrimeroPag.Text = "Primero";
            this.btnPrimeroPag.Click += new System.EventHandler(this.BtnPrimeroPag_Click);
            // 
            // btnAtras
            // 
            this.btnAtras.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAtras.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeft_16x;
            this.btnAtras.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(23, 22);
            this.btnAtras.Text = "Atras";
            this.btnAtras.Click += new System.EventHandler(this.BtnAtras_Click);
            // 
            // btnAdelante
            // 
            this.btnAdelante.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdelante.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronRight_16x;
            this.btnAdelante.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdelante.Name = "btnAdelante";
            this.btnAdelante.Size = new System.Drawing.Size(23, 22);
            this.btnAdelante.Text = "Siguente";
            this.btnAdelante.Click += new System.EventHandler(this.BtnAdelante_Click);
            // 
            // btnUlitmoPag
            // 
            this.btnUlitmoPag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUlitmoPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronRightGroup_16x;
            this.btnUlitmoPag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUlitmoPag.Name = "btnUlitmoPag";
            this.btnUlitmoPag.Size = new System.Drawing.Size(23, 22);
            this.btnUlitmoPag.Text = "Ultimo";
            this.btnUlitmoPag.Click += new System.EventHandler(this.BtnUlitmoPag_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbImprimir
            // 
            this.tsbImprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbImprimir.Image = global::EmpenosEvora.Properties.Resources.Print_16x;
            this.tsbImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbImprimir.Name = "tsbImprimir";
            this.tsbImprimir.Size = new System.Drawing.Size(23, 22);
            this.tsbImprimir.Text = "Imprimir";
            this.tsbImprimir.ToolTipText = "Imprimir";
            // 
            // btnCierra
            // 
            this.btnCierra.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnCierra.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCierra.Name = "btnCierra";
            this.btnCierra.Size = new System.Drawing.Size(23, 22);
            this.btnCierra.Text = "Salir";
            this.btnCierra.Click += new System.EventHandler(this.BtnCierra_Click);
            // 
            // btnBusqueda
            // 
            this.btnBusqueda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBusqueda.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusqueda.Location = new System.Drawing.Point(240, 40);
            this.btnBusqueda.Name = "btnBusqueda";
            this.btnBusqueda.Size = new System.Drawing.Size(32, 23);
            this.btnBusqueda.TabIndex = 2;
            this.btnBusqueda.UseVisualStyleBackColor = true;
            this.btnBusqueda.Click += new System.EventHandler(this.BtnBusqueda_Click);
            // 
            // txtDescripcionAnt
            // 
            this.txtDescripcionAnt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionAnt.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionAnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcionAnt.Location = new System.Drawing.Point(184, 72);
            this.txtDescripcionAnt.MaxLength = 3;
            this.txtDescripcionAnt.Name = "txtDescripcionAnt";
            this.txtDescripcionAnt.Size = new System.Drawing.Size(44, 20);
            this.txtDescripcionAnt.TabIndex = 543;
            this.txtDescripcionAnt.Visible = false;
            // 
            // txtTipoAnt
            // 
            this.txtTipoAnt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTipoAnt.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtTipoAnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTipoAnt.Location = new System.Drawing.Point(296, 40);
            this.txtTipoAnt.MaxLength = 3;
            this.txtTipoAnt.Name = "txtTipoAnt";
            this.txtTipoAnt.Size = new System.Drawing.Size(44, 20);
            this.txtTipoAnt.TabIndex = 544;
            this.txtTipoAnt.Visible = false;
            // 
            // frmTipoMovimientosCaja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(423, 168);
            this.Controls.Add(this.txtTipoAnt);
            this.Controls.Add(this.txtDescripcionAnt);
            this.Controls.Add(this.btnBusqueda);
            this.Controls.Add(this.tsAcciones);
            this.Controls.Add(this.btnFooterCierra);
            this.Controls.Add(this.btnFooterGuardar);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cmbTipo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.lblCodigo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmTipoMovimientosCaja";
            this.Text = "Tipo de movimientos de caja";
            this.Load += new System.EventHandler(this.FrmTipoMovimientosCaja_Load);
            this.tsAcciones.ResumeLayout(false);
            this.tsAcciones.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.ComboBox cmbTipo;
        private System.Windows.Forms.Button btnFooterCierra;
        private System.Windows.Forms.Button btnFooterGuardar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ToolStrip tsAcciones;
        private System.Windows.Forms.ToolStripButton tsbNuevo;
        private System.Windows.Forms.ToolStripButton tsbGuardar;
        private System.Windows.Forms.ToolStripButton tsbEliminar;
        private System.Windows.Forms.ToolStripButton btnUndo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnPrimeroPag;
        private System.Windows.Forms.ToolStripButton btnAtras;
        private System.Windows.Forms.ToolStripButton btnAdelante;
        private System.Windows.Forms.ToolStripButton btnUlitmoPag;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbImprimir;
        private System.Windows.Forms.ToolStripButton btnCierra;
        private System.Windows.Forms.Button btnBusqueda;
        private System.Windows.Forms.ToolStripButton tsbBuscar;
        private System.Windows.Forms.TextBox txtDescripcionAnt;
        private System.Windows.Forms.TextBox txtTipoAnt;
    }
}