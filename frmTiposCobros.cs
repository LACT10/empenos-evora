﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmTiposCobros : Form
    {
        public Controller.clsCtrlCobrosTipos ctrlCobrosTipos;
        public Core.clsCoreView clsView;

        public int _intUlitmoCodigo;
        public int _intNumCerosConsecutivo;
        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }

        public int intNumCerosConsecutivo { get => _intNumCerosConsecutivo; set => _intNumCerosConsecutivo = value; }
        
        public List<Properties_Class.clsCobrosTipos> lsDeshacerCobrosTipos;
        

        public frmTiposCobros()
        {            
            InitializeComponent();
            this.clsView = new Core.clsCoreView();
            this.ctrlCobrosTipos = new Controller.clsCtrlCobrosTipos();
            this.ctrlCobrosTipos.Paginacion();
            this.lsDeshacerCobrosTipos = new List<Properties_Class.clsCobrosTipos>();
            this.lsDeshacerCobrosTipos.Add(this.ctrlCobrosTipos.cobrosTipos);
            //Asignar número de ceros pata generar código consecutivo
            this.intNumCerosConsecutivo = 2;

        }
        private void FrmcobrosTipos_Load(object sender, EventArgs e)
        {
        }

        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlCobrosTipos.cobrosTipos.intId == 0) return;
            if (this.clsView.confirmacionEliminar("Tipos de cobro"))
            {
                this.ctrlCobrosTipos.Eliminar();
                if (this.ctrlCobrosTipos.bolResultado)
                {
                    this.ctrlCobrosTipos.Paginacion();
                    this.limpiar_textbox();
                    this.clsView.mensajeExitoDB("tipos de cobro", "elimino");
                }
            }
        }

        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlCobrosTipos.bolResutladoPaginacion)
            {
                this.clsView.principal.epMensaje.Clear();
                txtCodigo.Enabled = true;
                txtCodigo.Text = this.lsDeshacerCobrosTipos[0].strCodigo.ToString();
                txtDescripcion.Text = this.lsDeshacerCobrosTipos[0].strDescripcion.ToString();
                txtDescripcionAnt.Text = this.lsDeshacerCobrosTipos[0].strDescripcion.ToString();
                txtCodigo.Focus();
            }
        }

        private void tsbBuscar_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();

        }

        private void BtnBusqueda_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();
        }

        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlCobrosTipos.PrimeroPaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlCobrosTipos.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlCobrosTipos.SiguentePaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlCobrosTipos.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlCobrosTipos.AtrasPaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlCobrosTipos.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlCobrosTipos.UltimoPaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlCobrosTipos.bolResultado) return;
            this.llenar_formulario();
        }
        private void TsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();
            this.ctrlCobrosTipos.cobrosTipos.limpiarProps();
            txtCodigo.Enabled = false;
            if (this.ctrlCobrosTipos.bolResutladoPaginacion)
            {
                DataTable dtDatos = this.ctrlCobrosTipos.objDataSet.Tables[0];
                this.intUlitmoCodigo = Int32.Parse(dtDatos.Rows[dtDatos.Rows.Count - 1][1].ToString()) + 1;
                this.agregarDeshacer();
            }
            else
            {
                this.intUlitmoCodigo = 1;
            }

            txtCodigo.Text = this.intUlitmoCodigo.ToString().PadLeft(this.intNumCerosConsecutivo, '0');
            txtDescripcion.Focus();

        }
        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {
            txtDescripcion.Text = "";
        }

        private void TxtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);

        }
        private void TxtCodigo_Leave(object sender, EventArgs e)
        {
            this.ctrlCobrosTipos.Buscar(txtCodigo.Text);
            if (!(this.ctrlCobrosTipos.bolResultado) || txtCodigo.Text.Trim().Length == 0) return;
            this.llenar_formulario();
        }

        private void txtDescripcion_Leave(object sender, EventArgs e)
        {
            if (txtDescripcion.Text != txtDescripcionAnt.Text)
            {
                this.ctrlCobrosTipos.ValidarDescripcion(txtDescripcion.Text);
                if (this.ctrlCobrosTipos.bolResultado)
                {
                    this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("La descripción debe ser única, favor de verificar"));
                    txtDescripcion.Text = "";
                }
            }
        }

        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            txtDescripcionAnt.Text = "";
            txtCodigo.Enabled = true;
            txtCodigo.Focus();
            this.clsView.principal.epMensaje.Clear();
        }

        private bool validacion()
        {

            bool bolValidacion = true;
            string strCodigo = txtCodigo.Text.TrimStart();
            string strDescripcion = txtDescripcion.Text.TrimStart();

            if (strCodigo == "")
            {
                txtCodigo.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigo, this.clsView.mensajeErrorValiForm2("El campo código es obligatorio, favor de ingresar un código "));
                bolValidacion = false;
            }

            if (strDescripcion == "")
            {
                txtDescripcion.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("El campo descripción es obligatorio, favor de ingresar una descripción "));
                bolValidacion = false;
            }
            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();

            if (validacion())
            {
                this.ctrlCobrosTipos.cobrosTipos.strCodigo = this.intUlitmoCodigo.ToString().PadLeft(this.intNumCerosConsecutivo, '0');
                this.ctrlCobrosTipos.cobrosTipos.strDescripcion = txtDescripcion.Text.ToString();
                this.ctrlCobrosTipos.cobrosTipos.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlCobrosTipos.Guardar();
                
                if (this.ctrlCobrosTipos.mdlCobrosTipos.bolResultado)
                {
                    this.limpiar_textbox();
                    this.ctrlCobrosTipos.Paginacion();
                    this.clsView.mensajeExitoDB("tipos de cobro", "guardó");
                    txtCodigo.Focus();

                }
            }
        }


        public void mostraBusqueda()
        {
            if (this.ctrlCobrosTipos.bolResutladoPaginacion)
            {
                this.ctrlCobrosTipos.CargarFrmBusqueda();

                if (this.ctrlCobrosTipos.bolResultadoBusqueda)
                {
                    this.llenar_formulario();
                }
            }
        }
        public void llenar_formulario()
        {
            if (this.ctrlCobrosTipos.cobrosTipos.strCodigo.Length == 0) return;
            this.clsView.principal.epMensaje.Clear();
            txtCodigo.Enabled = true;
            txtCodigo.Text = this.ctrlCobrosTipos.cobrosTipos.strCodigo;
            txtDescripcion.Text = this.ctrlCobrosTipos.cobrosTipos.strDescripcion;
            txtDescripcionAnt.Text = this.ctrlCobrosTipos.cobrosTipos.strDescripcion;
            this.intUlitmoCodigo = Int32.Parse(this.ctrlCobrosTipos.cobrosTipos.strCodigo);
            
        }

        public void agregarDeshacer()
        {
            if (this.ctrlCobrosTipos.bolResutladoPaginacion)
            {
                this.lsDeshacerCobrosTipos.RemoveAt(0);
                this.lsDeshacerCobrosTipos.Add(this.ctrlCobrosTipos.cobrosTipos);
            }
        }


        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void ToolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void FrmTiposCobros_Load(object sender, EventArgs e)
        {
            txtCodigo.MaxLength = this.intNumCerosConsecutivo;

            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);
        }


    }
}