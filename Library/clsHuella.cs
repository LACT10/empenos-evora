﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora.Library
{
    public class clsHuella: Form, DPFP.Capture.EventHandler
    {
        public Form _frmGenerico;        
        public DPFP.Capture.Capture captura;
        public DPFP.Processing.Enrollment enroller;
        public DPFP.Template template;
        public Form frmGenerico { get => _frmGenerico; set => _frmGenerico = value; }
        //Delegado que se utiliza para mostrar el número de veces que hace falta pasar el dedo por el lector de huella  
        delegate void delegadoMuestra();
        //Delegado que se utiliza para habilitar controles después de la captura de huella
        delegate void delegadoControles();



        public clsHuella() 
        {
            this.captura = new DPFP.Capture.Capture();
            try
            {
                captura = new DPFP.Capture.Capture();
                //Si existe captura
                if (this.captura != null)
                {

                    captura.EventHandler = this;
                    //this.captura.EventHandler = this.frmGenerico;
                    enroller = new DPFP.Processing.Enrollment();
                    StringBuilder strNumeroVeces = new StringBuilder();
                    //Asignar número de veces que es necesario pasar el dedo por el lector de huella
                    strNumeroVeces.AppendFormat("Necesitas pasar el dedo {0} veces", enroller.FeaturesNeeded);
                    //this.lblNumeroVeces.Text = strNumeroVeces.ToString();
                }
                else
                {
                    //Envia un mensaje de error, para informarle al usuario que no es posible instanciar la captura del lector de huella
                    MessageBox.Show("No es posible instanciar la captura","Registro de Huella", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
            }
            catch (Exception ex)
            {
                //Envia un mensaje de error, para informarle al usuario que no es posible inicializar la captura del lector de huella
                MessageBox.Show("No es posible inicializar la captura " + ex.Message, "Registro de Huella", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);

            }
        }

        /// <summary>
        /// Método para iniciar la captura de la huella
        /// </summary>
        public void comenzarHuella() 
        {
            //Si existe captura
            if (this.captura != null)
            {
                try
                {
                    //Iniciar captura del lector de huella digital
                    captura.StartCapture();
                }
                catch (Exception ex)
                {
                    //Envia un mensaje de error, para informarle al usuario que no es posible iniciar la captura del lector de huella
                    MessageBox.Show(String.Format("No es posible iniciar la captura {0} " , ex.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }



        #region Eventos del Lector de Huella Digital
        //Completa captura
        public void OnComplete(object Capture, string ReaderSerialNumber, DPFP.Sample Sample)
        {
            //Hacer un llamado al método para asignar imagen de la huella en el picture box
            //this.asignar_imagen(convertir_sample_mapa_bits(Sample));
            //Hacer un llamado al método para procesar las caracteristicas de la huella 
            //this.procesar(Sample);
        }

        public void OnFingerGone(object Capture, string ReaderSerialNumber)
        {
        }

        public void OnFingerTouch(object Capture, string ReaderSerialNumber)
        {

        }

        public void OnReaderConnect(object Capture, string ReaderSerialNumber)
        {
        }

        public void OnReaderDisconnect(object Capture, string ReaderSerialNumber)
        {
        }

        public void OnSampleQuality(object Capture, string ReaderSerialNumber, DPFP.Capture.CaptureFeedback CaptureFeedback)
        {
        }

        #endregion

    }

}
