﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UI_Support;

namespace EmpenosEvora
{
    public partial class frmGuardar : Form
    {
        public Core.clsCoreView clsView;

        private bool _bolSucursal;
        private bool _bolEmpresa;
        private bool _bolResultado;
        public DataRow _objDataSetSingle;
        private bool _bolEditar;

        public bool bolSucursal { get => _bolSucursal; set => _bolSucursal = value; }
        public bool bolEmpresa { get => _bolEmpresa; set => _bolEmpresa = value; }
        public bool bolResultado { get => _bolResultado; set => _bolResultado = value; }

        public DataRow objDataSetSingle { get => _objDataSetSingle; set => _objDataSetSingle = value; }
        public bool bolEditar { get => _bolEditar; set => _bolEditar = value; }

        public Controller.clsCtrlLogin ctrlLogin;


        public frmGuardar()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();
            this.ctrlLogin = new Controller.clsCtrlLogin();

            //btnVerificarHuella.Enabled = false;
            this.bolResultado = false;
        }

        private void frmGuardar_Load(object sender, EventArgs e)
        {

        }




        private void btnVerificarHuella_Click(object sender, EventArgs e)
        {
            Controller.clsCtrlUsuarios ctrlUsuarios = new Controller.clsCtrlUsuarios();
            ctrlUsuarios.Paginacion();


            VerificationForm frmVerificarHuella = new VerificationForm();
            frmVerificarHuella.objDataSet = ctrlUsuarios.objDataSet;

            frmVerificarHuella.ShowDialog();

            if (frmVerificarHuella.bolResultado)
            {
                this.objDataSetSingle = frmVerificarHuella.objDataSetSingle;
                ctrlUsuarios.BuscarHuella(frmVerificarHuella.objDataSetSingle);

                Properties_Class.clsConstante clsCon = new Properties_Class.clsConstante();



                if (ctrlUsuarios.usuario.intId > 0)
                {
                    if (ctrlUsuarios.usuario.byteIntentos == clsCon.intIntentos)
                    {
                        MessageBox.Show("Su cuenta se encuentra suspendida, comuníquese con el administrador", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else if (!ctrlUsuarios.usuario.validarMovimientos() && this.bolEditar) 
                    {
                        MessageBox.Show("Su cuenta no puede realizar modificaciones, comuníquese con el administrador", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {

                        this.bolResultado = true;


                        this.Close();
                    }
                }

            }
        }

        private void btnFooterAcceder_Click(object sender, EventArgs e)
        {
            if (validacion())
            {
                if (txtUsuarioId.Text != "")
                {


                }
                else
                {

                    this.ctrlLogin = new Controller.clsCtrlLogin();
                    ctrlLogin.Login(txtUsuario.Text.TrimStart(), txtContrasena.Text.TrimStart());
                    Properties_Class.clsConstante clsCon = new Properties_Class.clsConstante();

                    if (this.ctrlLogin.bolResultado)
                    {

                        if (this.ctrlLogin.login.usuario.byteIntentos >= clsCon.intIntentos)
                        {
                            MessageBox.Show("Su cuenta se encuentra suspendida ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else if (!this.ctrlLogin.login.usuario.validarMovimientos() && this.bolEditar)
                        {
                            MessageBox.Show("Su cuenta no puede realizar modificaciones, comuníquese con el administrador", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            this.objDataSetSingle = this.ctrlLogin.objDataSet.Tables[0].Rows[0];
                            this.bolResultado = true;
                            this.Close();

                        }

                    }
                    else
                    {


                        ctrlLogin.limpiarProps();
                        ctrlLogin.BuscarUsuario(txtUsuario.Text.TrimStart());

                        if (ctrlLogin.bolResultado && ctrlLogin.login.usuario.strEstatus == ctrlLogin.login.dicEstatus["ACTIVO"])
                        {

                            if (!(ctrlLogin.login.usuario.byteIntentos == clsCon.intIntentos))
                            {
                                MessageBox.Show("Usuario y contraseña son incorrectas, intenta de nuevo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {

                            MessageBox.Show("Su cuenta se encuentra suspendida , comuníquese con el administrador ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }


                    }

                }

            }

        }


        private bool validacion()
        {

            bool bolValidacion = true;

            string strUsuario = txtUsuario.Text.TrimStart();
            string strContrasena = txtContrasena.Text.TrimStart();
            string strUsuarioId = txtUsuarioId.Text;

            if (strUsuario == "")
            {
                txtUsuario.Focus();
                this.clsView.principal.epMensaje.SetError(txtUsuario, this.clsView.mensajeErrorValiForm2("El campo usuario es obligatorio, favor de ingresar un usuario "));
                bolValidacion = false;
            }

            if (strContrasena == "")
            {
                txtContrasena.Focus();
                this.clsView.principal.epMensaje.SetError(txtContrasena, this.clsView.mensajeErrorValiForm2("El campo contraseña es obligatorio, favor de ingresar una contraseña "));
                bolValidacion = false;
            }

            return bolValidacion;
        }


    }
}
