﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmCajaUsuario : Form
    {

        public DataGridViewRow _rowsIndex;
        public bool _bolCierra;
        public int _intEmpresaId;
        public int _intSucursalId;
        public int _intCajaId;
        public string _strEmpresa;        
        public string _strSucursal;
        public string _strCodigoCaja;
        public string _strDescripcionCaja;
        public string _strPermisos;
        public DataSet _objProcesos;
        public DataSet _objSubProcesos;
        public DataSet _objCajas;

        public DataGridViewRow rowsIndex { get => _rowsIndex; set => _rowsIndex = value; }
        public bool bolCierra { get => _bolCierra; set => _bolCierra = value; }
        public int intEmpresaId { get => _intEmpresaId; set => _intEmpresaId = value; }
        public int intSucursalId { get => _intSucursalId; set => _intSucursalId = value; }
        
        public string strEmpresa { get => _strEmpresa; set => _strEmpresa = value; }
        public string strSucursal { get => _strSucursal; set => _strSucursal = value; }
        public string strCodigoCaja { get => _strCodigoCaja; set => _strCodigoCaja = value; }
        public string strDescripcionCaja { get => _strDescripcionCaja; set => _strDescripcionCaja = value; }
        public int intCajaId { get => _intCajaId; set => _intCajaId = value; }




        public string strPermisos { get => _strPermisos; set => _strPermisos = value; }
        public DataSet objProcesos { get => _objProcesos; set => _objProcesos = value; }
        public DataSet objSubProcesos { get => _objSubProcesos; set => _objSubProcesos = value; }
        public DataSet objCajas { get => _objCajas; set => _objCajas = value; }

        public Core.clsCoreView clsView = new Core.clsCoreView();

        Controller.clsCtrlLogin ctrlLogin = new Controller.clsCtrlLogin();
        Controller.clsCtrlCajasUsuario ctrlCajasUsuario = new Controller.clsCtrlCajasUsuario();


        public Properties_Class.clsLogin login;


        public frmCajaUsuario()
        {
            InitializeComponent();
         

        }

        private void frmCajaUsuario_Load(object sender, EventArgs e)
        {

            this.bolCierra = false;

            
        }

        private void dgvResultado_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        
        }

        private void dgvResultado_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.ctrlLogin.login.objCajas != null)
            //if (this.login.objCajas != null)
            {
               /* this.bolCierra = dgvResultado.CurrentRow.Selected;
                this.rowsIndex = dgvResultado.SelectedRows[0];
                this.bolCierra = true;*/
                //this.Close();
            }
        }

        private void btnBusquedaSucursal_Click(object sender, EventArgs e)
        {


            if (txtEmpresaId.Text == "") return;

          
                Controller.clsCtrlSucursales ctrlSucursales = new Controller.clsCtrlSucursales();
                ctrlSucursales.BuscarSucursalUsuario(login.usuario.intId);

                txtSucursalId.Text = ctrlSucursales.sucursal.intId.ToString();
                txtCodigoSucursal.Text = ctrlSucursales.sucursal.strCodigo;
                txtDescripcionSucursal.Text = ctrlSucursales.sucursal.strNombre;

                txtCodigoCajas.Text = "";
                txtDescripcionCajas.Text = "";
                txtCajasId.Text = "";
                
                this.mostraCajas();

                this.cajasSucursal();

        }

        private void txtCodigoSucursal_Leave(object sender, EventArgs e)
        {

            if (txtCodigoSucursal.Text.ToString() == "") return;
            Controller.clsCtrlSucursales ctrlSucursales = new Controller.clsCtrlSucursales();
            ctrlSucursales.Buscar(txtCodigoSucursal.Text);
            txtSucursalId.Text = ctrlSucursales.sucursal.intId.ToString();
            txtDescripcionSucursal.Text = ctrlSucursales.sucursal.strNombre.ToString();
            txtCodigoSucursal.Text = ctrlSucursales.sucursal.strCodigo.ToString();

            txtCodigoCajas.Text = "";
            txtDescripcionCajas.Text = "";
            txtCajasId.Text = "";

            this.mostraCajas();
            this.cajasSucursal();


        }

        private void btnBusquedaEmpresa_Click(object sender, EventArgs e)
        {
            

            Controller.clsCtrlEmpresas ctrlEmpresas = new Controller.clsCtrlEmpresas();
            ctrlEmpresas.Paginacion();
            ctrlEmpresas.CargarFrmBusqueda();

            if (ctrlEmpresas.bolResultado)
            {
                txtEmpresaId.Text = ctrlEmpresas.empresa.intId.ToString();
                txtDescripcionEmpresas.Text = ctrlEmpresas.empresa.strNombreComercial;
            }
        }

        public bool validacion() 
        {
            bool bolValidacion = true;

            string strSucursalId = txtSucursalId.Text;
            string strEmpresaId = txtEmpresaId.Text;
            string strCajasId = txtCajasId.Text;

            if (strSucursalId == "")
            {
                txtCodigoSucursal.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigoSucursal, this.clsView.mensajeErrorValiForm2("El campo sucursal es obligatorio, favor de ingresar una sucursal"));
                bolValidacion = false;
            }

            if (strEmpresaId == "") 
            {
                txtDescripcionEmpresas.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcionEmpresas, this.clsView.mensajeErrorValiForm2("El campo empresa es obligatorio, favor de ingresar una empresa"));
                bolValidacion = false;
            }

            if (txtCodigoCajas.Enabled && strCajasId == "" || strCajasId == "0") {
                txtDescripcionCajas.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcionCajas, this.clsView.mensajeErrorValiForm2("El campo caja es obligatorio, favor de ingresar una caja"));
                bolValidacion = false;
            }

                return bolValidacion;


        }


        private void btnFooterAcceder_Click(object sender, EventArgs e)
        {
            if (this.validacion()) 
            {

               
                this.ctrlLogin.login.usuario.intId = this.login.usuario.intId;
                this.ctrlLogin.BuscarPermisosAcessoUsuario();
                if (this.ctrlLogin.bolResultado)
                {
                    this.ctrlLogin.BuscarPermisosAcessoProcessoUsuario();
                    this.ctrlLogin.BuscarPermisosAcessoSubProcessoUsuario();
                }

                // ctrlLogin.BuscarCaja();

                this.strEmpresa = txtDescripcionEmpresas.Text;
                this.strSucursal = txtDescripcionSucursal.Text;
                this.intSucursalId = int.Parse(txtSucursalId.Text);
                this.intEmpresaId = int.Parse(txtEmpresaId.Text);
                this.strPermisos = this.ctrlLogin.login.strPermisos;
                this.objProcesos = this.ctrlLogin.login.objProcesos;
                this.objSubProcesos = this.ctrlLogin.login.objSubProcesos;
                this.objCajas = this.ctrlLogin.login.objCajas;
                this.strDescripcionCaja = txtDescripcionCajas.Text;
                this.strCodigoCaja = txtCodigoCajas.Text;
                this.intCajaId = (txtCajasId.Text == "") ? 0 : int.Parse(txtCajasId.Text) ;
                this.bolCierra = true;


                this.Close();
                
            }
        }

        public void mostraCajas() 
        {
            this.ctrlLogin.login.usuario.intId = this.login.usuario.intId;
            this.ctrlLogin.login.intSucursalId = int.Parse(txtSucursalId.Text);
            this.ctrlLogin.BuscarCaja();
            if (this.ctrlLogin.login.objCajas != null)           
            {

                /*dgvResultado.AutoGenerateColumns = true;

                dgvResultado.DataSource = this.ctrlLogin.login.objCajas.Tables[0].DefaultView; // dataset                    

                dgvResultado.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                dgvResultado.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
                dgvResultado.AllowUserToAddRows = false;


                dgvResultado.Columns["caja_id"].Visible = false;
                dgvResultado.Columns["descripcion"].Visible = false;
                dgvResultado.Columns["codigo"].Visible = false;*/

            }

        }

        private void btnBusquedaCajas_Click(object sender, EventArgs e)
        {

            if (txtSucursalId.Text == "") return;

            this.ctrlCajasUsuario.Paginacion(this.login.usuario.intId, int.Parse(txtSucursalId.Text));
            this.login.objCajas = this.ctrlCajasUsuario.objDataSet;
            if (this.ctrlCajasUsuario.bolResutladoPaginacion)
            {
                this.ctrlCajasUsuario.CargarFrmBusqueda();

                if (this.ctrlCajasUsuario.bolResultado)
                {
                    txtCajasId.Text = this.ctrlCajasUsuario.caja.intId.ToString();
                    txtDescripcionCajas.Text = this.ctrlCajasUsuario.caja.strDescripcion;
                    txtCodigoCajas.Text = this.ctrlCajasUsuario.caja.strCodigo;
                    
                }
            }


            

        }

        private void txtCodigoCajas_Leave(object sender, EventArgs e)
        {
            if (txtSucursalId.Text == "") return;

            this.ctrlCajasUsuario.Paginacion(this.login.usuario.intId, int.Parse(txtSucursalId.Text));
            this.login.objCajas = this.ctrlCajasUsuario.objDataSet;
            this.ctrlCajasUsuario.BuscarCodigo(this.login.usuario.intId, int.Parse(txtSucursalId.Text), txtCodigoCajas.Text);
            if (this.ctrlCajasUsuario.bolResultado)
            {
                
                txtCajasId.Text = this.ctrlCajasUsuario.caja.intId.ToString();
                txtDescripcionCajas.Text = this.ctrlCajasUsuario.caja.strDescripcion;
                txtCodigoCajas.Text = this.ctrlCajasUsuario.caja.strCodigo;
            }
        }

       public void cajasSucursal() {

            Controller.clsCtrlCajasUsuario ctrlCajasUsuario = new Controller.clsCtrlCajasUsuario();
            ctrlCajasUsuario.Paginacion(this.login.usuario.intId, int.Parse(txtSucursalId.Text));
            if (ctrlCajasUsuario.objDataSet == null)
            {
                txtCodigoCajas.Enabled = false;
                txtDescripcionCajas.Enabled = false;
                btnBusquedaCajas.Enabled = false;

            }
            else {
                txtCodigoCajas.Enabled = true;
                txtDescripcionCajas.Enabled = true;
                btnBusquedaCajas.Enabled = true;
            }
        }
    }
}
