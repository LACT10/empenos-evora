﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace EmpenosEvora.Core
{
    public class clsCoreView
    {

        public frmPrincipal principal = new frmPrincipal();

        //public const string mensajeExito = "Se {0} con éxito el registro";
        public const string mensajeExito = "El registro se {0} con éxito";

        public const string mensajeErrorValidacion = "El campo '{0}' es obligatorio, favor de ingresar {1} {0}";

        public string _strForm;
        public string _strProcesosId;
        public bool _bolIsopenForm;
        public bool _bolPermisosGuardar;
        public bool _bolPermisosReactivar;
        public bool _bolPermisosEliminar;
        public bool _bolPermisosNuevo;
        public bool _bolPermisosBuscar;        
        public List<string> _lsBloquearCol;
        public DataGridView _dgvGenrico;

        public string strForm { get => _strForm; set => _strForm = value; }

        public string strProcesosId { get => _strProcesosId; set => _strProcesosId = value; }

        public bool bolIsopenForm { get => _bolIsopenForm; set => _bolIsopenForm = value; }

        //Variable para saber si tiene permisos de reactivar
        public bool bolPermisosGuardar { get => _bolPermisosGuardar; set => _bolPermisosGuardar = value; }
        //Variable para saber si tiene permisos de reactivar
        public bool bolPermisosReactivar { get => _bolPermisosReactivar; set => _bolPermisosReactivar = value; }
        //Variable para saber si tiene permisos de eliminar
        public bool bolPermisosEliminar { get => _bolPermisosEliminar; set => _bolPermisosEliminar = value; }
        //Variable para saber si tiene permisos de nuevo
        public bool bolPermisosNuevo { get => _bolPermisosNuevo; set => _bolPermisosNuevo = value; }
        //Variable para saber si tiene permisos de buscar
        public bool bolPermisosBuscar { get => _bolPermisosBuscar; set => _bolPermisosBuscar = value; }

        public List<string> lsBloquearCol { get => _lsBloquearCol; set => _lsBloquearCol = value; }

        public DataGridView dgvGenrico { get => _dgvGenrico; set => _dgvGenrico = value; }

        public Properties_Class.clsLogin login;

        public Controller.clsCtrlFolios ctrlFoliosAuto = new Controller.clsCtrlFolios();

        


        public Dictionary<string, bool> dicInputs = new Dictionary<string, bool>()
        {            
            {"DESHABILITAR", false},
            {"HABILITAR", true},            
        };

        public clsCoreView()
        {

            //Inicializar variables 
            this.bolPermisosEliminar = false;
            this.bolPermisosReactivar = false;
            this.bolPermisosGuardar = false;
            this.bolPermisosNuevo = false;
            this.bolPermisosBuscar = false;




        }
        
        public void mensajeExitoDB(string msj, string msj2)
        {
            MessageBox.Show(String.Format(mensajeExito, msj2), "Éxito", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        public string mensajeErrorValiForm(string msj, string un)
        {
            return MessageBox.Show(String.Format(mensajeErrorValidacion, msj, un), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error).ToString();
        }

        public string mensajeErrorValiForm2(string msj)
        {
            return MessageBox.Show(msj, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error).ToString();
        }

        public void soloNumVali(KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }
        public void soloNumValiDec(object sender,KeyPressEventArgs e )
        {
           
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        public void soloTextoVali(KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsLetter(ch) && ch != 8 && !char.IsWhiteSpace(ch))
            {
                e.Handled = true;
            }
        }

        
        public void limpiar_textbox(Form f)
        {
            foreach (Control c in f.Controls)
            {
                if (c is TextBox)
                {
                    c.Text = "";
                }
                else if (c is DateTimePicker)
                {

                }
                else if (c is CheckBox)
                {
                    //c.Checked = false;
                }
                else if (c is ComboBox)
                {

                }
                else if (c is NumericUpDown) {
                    c.Text = "0";
                                        
                }
           
            }
        }
        
        public void desHabilitarInputs(Form f, bool bolEstatus)
        {
            foreach (Control c in f.Controls)
            {
                if (c is TextBox)
                {                    
                    c.Enabled = bolEstatus;                    
                }
            }
        }


        public void revisarPantallas()
        {

            foreach (Form f in Application.OpenForms)
            {
                if (f.Name == this.strForm)
                {
                    this._bolIsopenForm = true;
                    f.BringToFront();
                    break;
                }
            }
        }

        public void cierraFormulariosESC(Form f, string strNombreForm)
        {            
            var result = MessageBox.Show(String.Format("¿Desea cerrar esta ventana?", strNombreForm), String.Format("Cerrar {0}", strNombreForm),
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question);

            // If the no button was pressed ...
            if (result == DialogResult.Yes)
            {
                //Función que cierra el formulario. 
                f.Close();

            }
          
         }



         
        public void accionMenu(ToolStrip toolStrip, List<Properties_Class.clsTextbox> lsBtnExtra)
        {            

            foreach (ToolStripItem row in toolStrip.Items)
            {
                foreach (DataRow rowSubProcesos in this.login.objSubProcesos.Tables[0].Rows)
                {

                    row.Enabled = false;

                    if (row.Name.ToString() == rowSubProcesos["nombre_menu"].ToString() && this.strProcesosId == rowSubProcesos["proceso_id"].ToString())
                    {
                        row.Enabled = true;
                        if (row.Name.ToString() == "tsbBuscar")
                        {
                            this.bolPermisosBuscar = true;
                            

                        }

                        if (row.Name.ToString() == "tsbGuardar")
                        {
                            this.bolPermisosGuardar = true;                            
                        }

                        if (row.Name.ToString() == "tsbReactivar") 
                        {
                            this.bolPermisosReactivar = true;
                        }

                        if (row.Name.ToString() == "tsbEliminar") 
                        {
                            this.bolPermisosEliminar = true;
                        }

                        if (row.Name.ToString() == "tsbNuevo") 
                        {
                            this.bolPermisosNuevo = true;
                        }


                        break;
                    }

                }
            }

            foreach (Properties_Class.clsTextbox row in lsBtnExtra)
            {

                
                row.BtnGenerico.Enabled = false;
                if (row.strBusqueda == "frmBuscar")
                {

                    if (this.bolPermisosBuscar)
                    {

                        row.BtnGenerico.Enabled = true;
                    }

                }
                else if (row.strBusqueda == "footerGuardar") 
                {
                    if (this.bolPermisosGuardar)
                    {

                        row.BtnGenerico.Enabled = true;
                    }
                }
            }



            //btnBuscar.Enabled = this.bolPermisosBuscar;
            toolStrip.Items["btnPrimeroPag"].Enabled = this.bolPermisosBuscar;
            toolStrip.Items["btnAtras"].Enabled = this.bolPermisosBuscar;
            toolStrip.Items["btnAdelante"].Enabled = this.bolPermisosBuscar;
            toolStrip.Items["btnUlitmoPag"].Enabled = this.bolPermisosBuscar;

            toolStrip.Items["btnUndo"].Enabled = true;
            toolStrip.Items["btnUndo"].Enabled = true;
            toolStrip.Items["btnCierra"].Enabled = true;

            if (!this.login.usuario.validarMovimientos()) 
            {
                toolStrip.Items["tsbEliminar"].Enabled = false;
            }

           // btnGuardar.Enabled = bolGuardar;

        }

        public bool confirmacionEliminar(string strNombreForm)
        {
            var result = MessageBox.Show(String.Format("¿Desea desactivar este registro?", strNombreForm), String.Format("{0}", strNombreForm),
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question);
            bool bolResultado = false;

            // If the no button was pressed ...
            if (result == DialogResult.Yes)
            {
                //Función que cierra el formulario. 
                bolResultado = true;

            }

            return bolResultado;
        }

        public bool confirmacionAccion(string strNombreForm, string strMensaje)
        {
            var result = MessageBox.Show(String.Format(strMensaje, strNombreForm), String.Format("{0}", strNombreForm),
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question);
            bool bolResultado = false;

          
            if (result == DialogResult.Yes)
            {
                //Función que cierra el formulario. 
                bolResultado = true;

            }

            return bolResultado;
        }


        public bool confirmacionGuardarCliente(string strNombreForm)
        {
            var result = MessageBox.Show(String.Format("¿Desea guardar la información de este cliente no deseado? (Es necesario ir al catálogo de clientes para modificar el estatus)", strNombreForm), String.Format("{0}", strNombreForm),
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question);
            bool bolResultado = false;

            // If the no button was pressed ...
            if (result == DialogResult.Yes)
            {
                //Función que cierra el formulario. 
                bolResultado = true;

            }

            return bolResultado;


        }

        public bool confirmacionVerificacion(string strNombreForm, string strMensaje)
        {
            var result = MessageBox.Show(strMensaje, String.Format("{0}", strNombreForm),
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question);
            bool bolResultado = false;

            // If the no button was pressed ...
            if (result == DialogResult.Yes)
            {
                //Función que cierra el formulario. 
                bolResultado = true;

            }

            return bolResultado;


        }


        public void bloquearColumnasGrid()
        {
            this.lsBloquearCol.ForEach(delegate (string col)
            {
                this.dgvGenrico.Columns[col].Visible = false;
            });
        }

        public void configuracionDataGridView()
        {
            this.dgvGenrico.AutoGenerateColumns = true;
            this.dgvGenrico.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvGenrico.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
        }


        //Metado que se utilizar para validar dirrecion de correo electronico
        public bool validarCorreoElectronico(string correo) 
        {

            String expresion;
            bool validar = true;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(correo, expresion))
            {
                if (Regex.Replace(correo, expresion, String.Empty).Length > 1)
                {
                    validar = false;
                }               
            }
            else
            {
                validar = false;
            }

            return validar;
        }

        //Metado que se utilizar para dar formato tipo moneda
        public string convertMoneda(string valor) 
        {
            Double value;
            string resultado = "";
            valor = valor.Replace("$","").Replace(",", "");
            if (Double.TryParse(valor, out value))
            {
                resultado = String.Format(System.Globalization.CultureInfo.CurrentCulture, "{0:C2}", value);
            }
            else {
                resultado = String.Empty;
            }
                
            return resultado;
        }

        public string convertPorcentaje(string valor)
        {                                    
            return valor + "%";
        }

        /*Metado que verificar que tienes el permisos de movimientos para mostra opciones de menu dependiendo estatus*/
        public void permisosMovimientoMenu(ToolStrip toolStrip, string strEstatus) 
        {


            if (this.login.usuario.validarMovimientos())
            {

                if (this.bolPermisosEliminar && strEstatus == "ACTIVO")
                {
                    toolStrip.Items["tsbEliminar"].Visible = true;
                    toolStrip.Items["tsbEliminar"].Enabled = true;
                    toolStrip.Items["tsbReactivar"].Visible = false;

                }
                else if (this.bolPermisosReactivar && strEstatus == "INACTIVO")
                {
                    toolStrip.Items["tsbEliminar"].Visible = false;
                    toolStrip.Items["tsbReactivar"].Visible = true;
                }
                else if (strEstatus == "INACTIVO")
                {
                    toolStrip.Items["tsbEliminar"].Enabled = false;
                    toolStrip.Items["tsbEliminar"].Visible = false;
                    toolStrip.Items["tsbReactivar"].Enabled = false;
                    toolStrip.Items["tsbReactivar"].Visible = true;

                }

            }
            else 
            {
                if (strEstatus == "INACTIVO")
                {
                    toolStrip.Items["tsbEliminar"].Enabled = false;
                    toolStrip.Items["tsbEliminar"].Visible = false;
                    toolStrip.Items["tsbReactivar"].Enabled = false;
                    toolStrip.Items["tsbReactivar"].Visible = true;

                }
                else if (strEstatus == "ACTIVO") 
                {
                    toolStrip.Items["tsbEliminar"].Enabled = false;
                    toolStrip.Items["tsbEliminar"].Visible = true;
                    toolStrip.Items["tsbReactivar"].Enabled = false;
                    toolStrip.Items["tsbReactivar"].Visible = false;
                }
            }
           
        }


        /*Metado que verificar que tienes el permisos de guardar*/
        public void ocultarGuardar(ToolStrip toolStrip, Button btnGuardar,  string strEstatus)
        {

            if (this.bolPermisosGuardar && strEstatus == "ACTIVO")
            {

                toolStrip.Items["tsbGuardar"].Enabled = true;
                btnGuardar.Visible = true;
                btnGuardar.Enabled = true;

            }
            else if (strEstatus == "INACTIVO")
            {
                toolStrip.Items["tsbGuardar"].Enabled = false;
                btnGuardar.Visible = false;
                btnGuardar.Enabled = false;
            }
            else if (strEstatus == "NUEVO") 
            {
                if (this.login.usuario.validarMovimientos() && this.bolPermisosEliminar)
                {
                    toolStrip.Items["tsbEliminar"].Enabled = true;
                    toolStrip.Items["tsbEliminar"].Visible = true;
                    toolStrip.Items["tsbReactivar"].Enabled = false;
                    toolStrip.Items["tsbReactivar"].Visible = false;
                }
                else 
                {
                    toolStrip.Items["tsbEliminar"].Enabled = false;
                    toolStrip.Items["tsbEliminar"].Visible = true;
                    toolStrip.Items["tsbReactivar"].Enabled = false;
                    toolStrip.Items["tsbReactivar"].Visible = false;
                }

                if (bolPermisosGuardar)
                {
                    toolStrip.Items["tsbGuardar"].Enabled = true;
                    btnGuardar.Visible = true;
                    btnGuardar.Enabled = true;
                }
                else 
                {
                    toolStrip.Items["tsbGuardar"].Enabled = false;
                    btnGuardar.Visible = true;
                    btnGuardar.Enabled = false;
                }
                
            }

        }

        public bool teclasEspeciales(string strFormulario, ToolStrip tsMenu, Form f, Keys keyData) 
        {
            if (keyData == Keys.Escape)
            {
                this.cierraFormulariosESC(f, strFormulario.ToLower());
                return true;
            }
            else if (keyData == Keys.F5 && this.bolPermisosNuevo)
            {
                tsMenu.Items["tsbNuevo"].PerformClick();
                return true;
            }
            else if (keyData == Keys.F2 && this.bolPermisosBuscar)
            {
                tsMenu.Items["tsbBuscar"].PerformClick();
                return true;
            }
            return false;
        }
    

    }
}
