﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Data;


namespace EmpenosEvora.Core
{
    public abstract class clsCoreModel
    {
        
        public bool _bolResultado;

        public bool _bolResultadoConnexion;

        public int _intRowsAffected;
        public int _intLastId;


        public DataSet _objResultado;

        public List<string> _lsQuerysTracciones;
        public List<string> _lsQuerysTraccionesID;
        public List<string> _lsQuerysTraccionesElminacionID;

        public bool bolResultado { get => _bolResultado; set => _bolResultado = value; }

        public bool bolResultadoConnexion { get => _bolResultadoConnexion; set => _bolResultadoConnexion = value; }

        public int  intRowsAffected { get => _intRowsAffected; set => _intRowsAffected = value; }

        public int intLastId { get => _intLastId; set => _intLastId = value; }

        public DataSet objResultado { get => _objResultado; set => _objResultado = value; }

        //
        public List<string> lsQuerysTracciones{ get => _lsQuerysTracciones; set => _lsQuerysTracciones = value; }

        public List<string> lsQuerysTraccionesID { get => _lsQuerysTraccionesID; set => _lsQuerysTraccionesID = value; }

        public List<string> lsQuerysTraccionesElminacionID { get => _lsQuerysTraccionesElminacionID; set => _lsQuerysTraccionesElminacionID = value; }

        public db.clsDBConnection connexion;

        public clsCoreModel()
        {
            this.intRowsAffected = 0;
            this.bolResultado = false;
            this.bolResultadoConnexion = false;

            try
            {
                //Crear instancia de connnexion
                this.connexion = new db.clsDBConnection();
                //Abrrir connnexion
                this.connexion.open_connection();
                //Crear commando para la consulta
                connexion.mscCmd = connexion.mscCon.CreateCommand();
                //Asignar true en caso de que la connexion sea existosa
                this.bolResultadoConnexion = true;
            }
            catch (MySqlException e)
            {
                //Enviar mensaje de error
                MessageBox.Show(e.Message);
                //Salir de la clase
                return;
            }            

        }

        /// <summary>
        /// Metodo que regesar un resultado en caso que el registro 
        ///         
        ///             
        /// </summary>
        public void resultadoGuardar(db.clsDBConnection connexion)
        {
            try
            {
                //Asignar numeros de filas afectadas en la consulta
                this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();
                //Si existe filas, asignar resultado exitoso
                if (this.intRowsAffected > 0)
                {
                    this.bolResultado = true;
                }
                //Cerrar connexion                
                this.connexion.close_connection();

            }
            catch (MySqlException e)
            {
                //Cerrar connexion         
                this.connexion.close_connection();
                //Enviar mensaje de error
                MessageBox.Show(e.Message);
            }            
        }



        /// <summary>
        /// Metodo que  se te ulizar para hacer una transaccion 
        ///         
        ///             
        /// </summary>
        public void resultadoGuardarTransaccion(db.clsDBConnection connexion)
        {

            connexion.inicializarMSTA();
            try
            {
                //Funcione que instancia la transaccion                
                this.lsQuerysTracciones.ForEach(delegate (string strQuery)
                {
                    connexion.mscCmd.CommandText = strQuery;
                    if (!(strQuery == "")) this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();                    
                });

                connexion.msTa.Commit();
                //Si existe filas, asignar resultado exitoso
                if (this.intRowsAffected > 0)
                {
                    this.bolResultado = true;
                }
                //Cerrar connexion                
                this.connexion.close_connection();

            }
            catch (MySqlException e)
            {
                try
                {
                    connexion.msTa.Rollback();
                    //Cerrar connexion         
                    this.connexion.close_connection();
                }
                catch (MySqlException ex)
                {
                    if (connexion.msTa.Connection != null)
                    {
                        /*Console.WriteLine("An exception of type " + ex.GetType() +
                        " was encountered while attempting to roll back the transaction.");*/
                    }
                    //Cerrar connexion         
                    this.connexion.close_connection();
                    //Enviar mensaje de error
                    MessageBox.Show(ex.Message);
                }
                //Enviar mensaje de error
                MessageBox.Show(e.Message);
            }
        }


        /// <summary>
        /// Metodo que  se te ulizar para hacer una transaccion 
        ///         
        ///             
        /// </summary>
        public void resultadoGuardarTransaccionID(db.clsDBConnection connexion)
        {

            connexion.inicializarMSTA();
            try
            {
                //SELECT SCOPE_IDENTITY()

                this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();

                string strId = "";
                //Si existe filas, asignar resultado exitoso
                if (this.intRowsAffected > 0)
                {
                    this.bolResultado = true;
                    strId = connexion.mscCmd.LastInsertedId.ToString();
                    //int intId = Int32.Parse(connexion.mscCmd.ExecuteScalar());
                }


                this.lsQuerysTraccionesID.ForEach(delegate (string strQuery)
                {
                    connexion.mscCmd.CommandText = strQuery.Replace("@id", strId);
                    if (!(strQuery == "")) this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();                    
                });

                //Funcione que instancia la transaccion                


                if (!(this.lsQuerysTracciones == null)) 
                {
                    this.lsQuerysTracciones.ForEach(delegate (string strQuery)
                    {
                        connexion.mscCmd.CommandText = strQuery;
                        if (!(strQuery == "")) this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();
                    });

                }


                connexion.msTa.Commit();
                //Si existe filas, asignar resultado exitoso
                if (this.intRowsAffected > 0)
                {
                    this.bolResultado = true;
                }
                //Cerrar connexion                
                this.connexion.close_connection();

            }
            catch (MySqlException e)
            {
                try
                {
                    connexion.msTa.Rollback();
                    //Cerrar connexion         
                    this.connexion.close_connection();
                }
                catch (MySqlException ex)
                {
                    if (connexion.msTa.Connection != null)
                    {
                        /*Console.WriteLine("An exception of type " + ex.GetType() +
                        " was encountered while attempting to roll back the transaction.");*/
                    }
                    //Cerrar connexion         
                    this.connexion.close_connection();
                    //Enviar mensaje de error
                    MessageBox.Show(ex.Message);
                }
                //Enviar mensaje de error
                MessageBox.Show(e.Message);
            }
        }
        /// <summary>
        /// Metodo que  se te ulizar para hacer una transaccion 
        ///         
        ///             
        /// </summary>
        public void resultadoActulizarTransaccionID(db.clsDBConnection connexion)
        {

            connexion.inicializarMSTA();
            try
            {
                //SELECT SCOPE_IDENTITY()

                this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();
                
                //Si existe filas, asignar resultado exitoso
                if (this.intRowsAffected > 0)
                {
                    this.bolResultado = true;                    
                }


                this.lsQuerysTraccionesID.ForEach(delegate (string strQuery)
                {                    
                    connexion.mscCmd.CommandText = strQuery;
                    if (!(strQuery == "")) this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();
                });

                //Funcione que instancia la transaccion                
                this.lsQuerysTracciones.ForEach(delegate (string strQuery)
                {
                    connexion.mscCmd.CommandText = strQuery;
                    if (!(strQuery == "")) this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();
                });

                connexion.msTa.Commit();
                //Si existe filas, asignar resultado exitoso
                if (this.intRowsAffected > 0)
                {
                    this.bolResultado = true;
                }
                //Cerrar connexion                
                this.connexion.close_connection();

            }
            catch (MySqlException e)
            {
                try
                {
                    connexion.msTa.Rollback();
                    //Cerrar connexion         
                    this.connexion.close_connection();
                }
                catch (MySqlException ex)
                {
                    if (connexion.msTa.Connection != null)
                    {
                        /*Console.WriteLine("An exception of type " + ex.GetType() +
                        " was encountered while attempting to roll back the transaction.");*/
                    }
                    //Cerrar connexion         
                    this.connexion.close_connection();
                    //Enviar mensaje de error
                    MessageBox.Show(ex.Message);
                }
                //Enviar mensaje de error
                MessageBox.Show(e.Message);
            }
        }




        public void resultadoActulizarTransaccionEliminacionID(db.clsDBConnection connexion)
        {

            connexion.inicializarMSTA();
            try
            {
                //SELECT SCOPE_IDENTITY()

                this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();
                
                this.lsQuerysTraccionesElminacionID.ForEach(delegate (string strQuery)
                {
                    connexion.mscCmd.CommandText = strQuery;
                    if (!(strQuery == "")) this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();                    
                });


                this.lsQuerysTraccionesID.ForEach(delegate (string strQuery)
                {

                    connexion.mscCmd.CommandText = strQuery;
                    if(!(strQuery == "")) this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();

                });


                //Funcione que instancia la transaccion                
                this.lsQuerysTracciones.ForEach(delegate (string strQuery)
                {
                    connexion.mscCmd.CommandText = strQuery;
                    if (!(strQuery == "")) this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();                    
                });

                connexion.msTa.Commit();
                //Si existe filas, asignar resultado exitoso
                if (this.intRowsAffected > 0)
                {
                    this.bolResultado = true;
                }
                //Cerrar connexion                
                this.connexion.close_connection();

            }
            catch (MySqlException e)
            {
                try
                {
                    connexion.msTa.Rollback();
                    this.intRowsAffected = 0;
                    this.bolResultado = false;
                    //Cerrar connexion         
                    this.connexion.close_connection();
                }
                catch (MySqlException ex)
                {
                    if (connexion.msTa.Connection != null)
                    {
                        /*Console.WriteLine("An exception of type " + ex.GetType() +
                        " was encountered while attempting to roll back the transaction.");*/
                       
                    }
                    this.intRowsAffected = 0;
                    this.bolResultado = false;
                    //Cerrar connexion         
                    this.connexion.close_connection();
                    //Enviar mensaje de error
                    MessageBox.Show(ex.Message);
                }
                //Enviar mensaje de error
                MessageBox.Show(e.Message);
            }
        }


        public void resultadoTransaccion(db.clsDBConnection connexion)
        {

            connexion.inicializarMSTA();
            try
            {
                this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();

                string strId = "";
                //Si existe filas, asignar resultado exitoso
                if (this.intRowsAffected > 0)
                {
                    this.bolResultado = true;
                    strId = connexion.mscCmd.LastInsertedId.ToString();
                    this.intLastId = int.Parse(strId);


                }

                if (!(this.lsQuerysTraccionesElminacionID == null))
                {
                    this.lsQuerysTraccionesElminacionID.ForEach(delegate (string strQuery)
                    {
                        connexion.mscCmd.CommandText = strQuery;
                        if (!(strQuery == "")) this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();
                    });
                }

                if (!(this.lsQuerysTraccionesID == null))
                {
                    this.lsQuerysTraccionesID.ForEach(delegate (string strQuery)
                    {
                        connexion.mscCmd.CommandText = strQuery.Replace("@id", strId);
                        if (!(strQuery == "")) this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();
                    });
                }

                if (!(this.lsQuerysTracciones == null))
                {
                    //Funcione que instancia la transaccion                
                    this.lsQuerysTracciones.ForEach(delegate (string strQuery)
                    {
                        connexion.mscCmd.CommandText = strQuery;
                        if (!(strQuery == "")) this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();
                    });
                }

                connexion.msTa.Commit();
                //Si existe filas, asignar resultado exitoso
                if (this.intRowsAffected > 0)
                {
                    this.bolResultado = true;
                }
                //Cerrar connexion                
                this.connexion.close_connection();

            }
            catch (MySqlException e)
            {
                try
                {
                    connexion.msTa.Rollback();
                    this.intRowsAffected = 0;
                    this.bolResultado = false;
                    //Cerrar connexion         
                    this.connexion.close_connection();
                }
                catch (MySqlException ex)
                {
                    if (connexion.msTa.Connection != null)
                    {
                        /*Console.WriteLine("An exception of type " + ex.GetType() +
                        " was encountered while attempting to roll back the transaction.");*/

                    }
                    this.intRowsAffected = 0;
                    this.bolResultado = false;
                    //Cerrar connexion         
                    this.connexion.close_connection();
                    //Enviar mensaje de error
                    MessageBox.Show(ex.Message);
                }
                //Enviar mensaje de error
                MessageBox.Show(e.Message);
            }
        }



        public void resultadoObtener(db.clsDBConnection connexion)
        {
            try
            {
                //Asignar numeros de filas afectadas en la consulta
                this.bolResultado = connexion.mscCmd.ExecuteReader().HasRows;
                //Funcione que instancia adapter
                this.connexion.inicializarMSDA();
                //Cerrar connexion                
                this.connexion.close_connection();
                //Si existe filas, asignar resultado exitoso
                if (this.bolResultado)
                {
                    //Crear instancia de dataset
                    this.objResultado = new DataSet();
                    //Agregar resutlado en el dataset                    
                    connexion.msdaAdap.Fill(this.objResultado);
                }
            }
            catch (MySqlException e)
            {
                //Cerrar connexion         
                this.connexion.close_connection();
                //Enviar mensaje de error
                MessageBox.Show(e.Message);
            }

        }

       
    }
}
