﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
//using System.Windows.Forms;
using System.Configuration;
using System.Threading.Tasks;


namespace EmpenosEvora.Core
{
    public abstract class clsCore
    {

        /*============================================================
         * Los datos se utilizar para controlar la paginacion
         =============================================================*/
        public DataSet _objDataSet;
        public DataSet _objDataSetSecundario;
        public DataRow _objDataSetSingle;
        public int _intCountIndex;
        public bool _bolPrimer;
        public bool _bolSiguente;
        public bool _bolResultado;
        public bool _bolResultadoBusqueda;
        public bool _bolResutladoPaginacion;
        public DataSet objDataSet { get => _objDataSet; set => _objDataSet = value; }

        public DataSet objDataSetSecundario { get => _objDataSetSecundario; set => _objDataSetSecundario = value; }

        public DataRow objDataSetSingle { get => _objDataSetSingle; set => _objDataSetSingle = value; }

        public int intCountIndex { get => _intCountIndex; set => _intCountIndex = value; }

        public bool bolPrimer { get => _bolPrimer; set => _bolPrimer = value; }
        public bool bolSiguente { get => _bolSiguente; set => _bolSiguente = value; }

        public bool bolResultado { get => _bolResultado; set => _bolResultado = value; } 

        public bool bolResultadoBusqueda { get => _bolResultadoBusqueda; set => _bolResultadoBusqueda = value; }

        public bool bolResutladoPaginacion { get => _bolResutladoPaginacion; set => _bolResutladoPaginacion = value; }
        /*============================================================
         * Fin los datos se utilizar para controlar la paginacion
         =============================================================*/
        public void primerPagina()
        {            
            if (this.bolResultado)
            {
                this.intCountIndex = 0;
                this.bolPrimer = true;
                this.objDataSetSingle = this.objDataSet.Tables[0].Rows[0];
            }                
        }

        public void atrasPagina()
        {
            if (this.bolResultado)
            {
                if (this.intCountIndex < this.objDataSet.Tables[0].Rows.Count)
                {
                    if (this.intCountIndex > 0)
                    {
                        this.intCountIndex--;
                    }
                    this.objDataSetSingle = this.objDataSet.Tables[0].Rows[this.intCountIndex];
                }
            }
            
        }

        public void siguentePagina()
        {
            if (this.bolResultado)
            {
                if (this.intCountIndex < this.objDataSet.Tables[0].Rows.Count - 1 || this.intCountIndex == 0)
                {
                    if (this.intCountIndex > 0 || this.bolPrimer)
                    {
                        this.intCountIndex++;
                    }
                    try
                    {
                        
                            this.objDataSetSingle = this.objDataSet.Tables[0].Rows[this.intCountIndex];
                        
                        
                    }
                    catch (Exception e) {

                    }
                    
                    if (this.intCountIndex == 0)
                    {
                        this.bolPrimer = true;
                    }
                }
                               
            }
            
        }



        public void ultimoPagina()
        {
            if (this.bolResultado)
            {
                this.intCountIndex = this.objDataSet.Tables[0].Rows.Count - 1;
                this.objDataSetSingle = this.objDataSet.Tables[0].Rows[this.intCountIndex];
            }
        }


    }
}
