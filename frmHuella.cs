﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmHuella : Form, DPFP.Capture.EventHandler
    {
        //Library.clsHuella huella;

        public DPFP.Capture.Capture captura;
        public DPFP.Processing.Enrollment enroller;
        public DPFP.Template template;

        //Delegado que se utiliza para mostrar el número de veces que hace falta pasar el dedo por el lector de huella  
        delegate void delegadoMuestra();        
        //Delegado que se utiliza para habilitar controles después de la captura de huella
        delegate void delegadoControles();

        public string strNombreForm = "Registro Huella";

        public frmHuella()
        {
            InitializeComponent();
            //this.captura = new DPFP.Capture.Capture();

            try
            {
                captura = new DPFP.Capture.Capture();
                //Si existe captura
                if (captura != null)
                {
                    captura.EventHandler = this;
                    enroller = new DPFP.Processing.Enrollment();
                    StringBuilder strNumeroVeces = new StringBuilder();
                    //Asignar número de veces que es necesario pasar el dedo por el lector de huella
                    strNumeroVeces.AppendFormat("Necesitas pasar el dedo {0} veces", enroller.FeaturesNeeded);
                    this.lblNumeroVeces.Text = strNumeroVeces.ToString();
                }
                else
                {

                    //Envia un mensaje de error, para informarle al usuario que no es posible instanciar la captura del lector de huella
                    MessageBox.Show("No es posible instanciar la captura", this.strNombreForm, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
            }
            catch (Exception ex)
            {

                //Envia un mensaje de error, para informarle al usuario que no es posible inicializar la captura del lector de huella
                MessageBox.Show(String.Format("No es posible inicializar la captura {0}", ex.Message), this.strNombreForm, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);

            }

        }

        private void frmHuella_Load(object sender, EventArgs e)
        {
            /*DPFP.Capture.Capture captura = new DPFP.Capture.Capture();
            captura.EventHandler = this;*/
            this.iniciar_captura();
        }

        
        #region Lector de Huella Digital
        /// <summary>
        /// Método para iniciar la captura de la huella
        /// </summary>
        private void iniciar_captura()
        {
            //Si existe captura
            if (captura != null)
            {
                try
                {
                    //Iniciar captura del lector de huella digital
                    captura.StartCapture();
                }
                catch (Exception ex)
                {
                    //Envia un mensaje de error, para informarle al usuario que no es posible iniciar la captura del lector de huella
                    MessageBox.Show("No es posible iniciar la captura " + ex.Message, this.strNombreForm, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
            }
        }

        /// <summary>
        /// Método para detener la captura de la huella
        /// </summary>
        private void detener_captura()
        {
            //Si existe captura
            if (captura != null)
            {
                try
                {
                    //Detener captura del lector de huella digital
                    captura.StopCapture();
                }
                catch (Exception ex)
                {
                    //Envia un mensaje de error, para informarle al usuario que no es posible detener la captura del lector de huella
                    MessageBox.Show("No es posible detener la captura " + ex.Message, this.strNombreForm, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
            }

        }

        /// <summary>
        /// Función que convierte DPFP.Sample a mapa de bits
        /// </summary>
        private static Bitmap convertir_sample_mapa_bits(DPFP.Sample sample)
        {
            DPFP.Capture.SampleConversion convertidor = new DPFP.Capture.SampleConversion();//Es una variable de tipo conversor de un DPFP.Sample
            Bitmap imgHuella = null;
            convertidor.ConvertToPicture(sample, ref imgHuella);
            return imgHuella;
        }

        /// <summary>
        /// Método para asignar mapa de bits (template) al picture box
        /// </summary>
        private void asignar_imagen(Bitmap bmp)
        {
            //this.ptbHuella.Image = bmp;
            this.ptbHuella.Image = new Bitmap(bmp, this.ptbHuella.Size);   // fit the image into the picture box
            /*this.Invoke(new Function(delegate () {
                this.ptbHuella.Image = new Bitmap(bitmap, Picture.Size);   // fit the image into the picture box
            }));*/

        }

        /// <summary>
        /// Método para mostrar el número de veces que es necesario pasar el dedo por el lector de huella y así poder realizar el registro 
        /// </summary>
        private void mostrar_numero_veces(string texto)
        {
            //Si la etiqueta es requerida (invocada)
            if (this.lblNumeroVeces.InvokeRequired)
            {
                //Invocar delegado para habilitar botón de guardar dentro del subproceso
                this.Invoke(new delegadoMuestra(delegate ()
                {
                    //Hacer un llamado al método para mostrar el número de veces que es necesario pasar el dedo por el lector de huella
                    mostrar_numero_veces(texto);

                }));

            }
            else
            {
                //Asignar el número de veces que es necesario pasar el dedo por el lector de huella
                this.lblNumeroVeces.Text = texto;
            }
        }

        /// <summary>
        /// Método para habilitar el botón guardar 
        /// </summary>
        private void habilitar_controles()
        {
            //Si el botón es requerido (invocada)
            /*if (this.btnGuardar.InvokeRequired)
            {
                //Invocar delegado para habilitar botón de guardar dentro del subproceso
                this.Invoke(new delegadoControles(delegate ()
                {
                    //Hacer un llamado a la función para habilitar controles
                    habilitar_controles();

                }));

            }
            else
            {
                //Habilitar botón Guardar
                this.btnGuardar.Enabled = true;
            }*/
        }

        /// <summary>
        /// Función que extrae las caracteristicas de la huella
        /// </summary>
        private static DPFP.FeatureSet extraer_caracteristicas_huella(DPFP.Sample sample, DPFP.Processing.DataPurpose purpose)
        {
            //Es una variable de tipo extracción de un DPFP.Sample
            DPFP.Processing.FeatureExtraction extractor = new DPFP.Processing.FeatureExtraction();
            DPFP.Capture.CaptureFeedback alimentacion = DPFP.Capture.CaptureFeedback.None;
            DPFP.FeatureSet caracteristicas = new DPFP.FeatureSet();
            //Extraer caracteristicas de la huella 
            extractor.CreateFeatureSet(sample, purpose, ref alimentacion, ref caracteristicas);
            //Si la alimentación de la captura es buena
            if (alimentacion == DPFP.Capture.CaptureFeedback.Good)
            {
                return caracteristicas;
            }
            else
            {
                return null;
            }

        }

        /// <summary>
        /// Método para procesar las caracteristicas de la huella
        /// </summary>
        protected void procesar(DPFP.Sample sample)
        {
            //Crear variable para recuperar las caracteristicas de la huella
            DPFP.FeatureSet caracteristicas = extraer_caracteristicas_huella(sample, DPFP.Processing.DataPurpose.Enrollment);
            //Si las caracteristicas no estan vacias
            if (caracteristicas != null)
            {
                try
                {
                    //Añadir caracteristicas
                    enroller.AddFeatures(caracteristicas);
                }
                finally
                {
                    //Iniciar variable para la captura de huella
                    StringBuilder strNumeroVeces = new StringBuilder();
                    //Asignar número de veces que es necesario pasar el dedo por el lector de huella 
                    strNumeroVeces.AppendFormat("Necesitas pasar el dedo {0} veces", enroller.FeaturesNeeded);
                    //Hacer un llamado al método para mostrar el número de veces que es necesario pasar el dedo por el lector de huella
                    mostrar_numero_veces(strNumeroVeces.ToString());
                    //Case con el estatus del suscriptor
                    switch (enroller.TemplateStatus)
                    {
                        //Si ya esta listo el suscriptor
                        case DPFP.Processing.Enrollment.Status.Ready:
                            //Extraer el templete
                            this.template = enroller.Template;
                            //Hacer un llamado al método para detener captura de la huella
                            detener_captura();
                            //Habilitar botón Guardar
                            //habilitar_controles();                            
                            break;
                        //Si falló el suscriptor
                        case DPFP.Processing.Enrollment.Status.Failed:
                            //Limpiar suscriptor
                            enroller.Clear();
                            //Hacer un llamado al método para detener captura de la huella
                            detener_captura();
                            //Hacer un llamado al método para iniciar captura de la huella
                            iniciar_captura();
                            break;
                    }
                }

            }
        }

        #endregion


        #region Eventos del Lector de Huella Digital
        //Completa captura
        public void OnComplete(object Capture, string ReaderSerialNumber, DPFP.Sample Sample)
        {
            //Hacer un llamado al método para asignar imagen de la huella en el picture box
            this.asignar_imagen(convertir_sample_mapa_bits(Sample));
            //Hacer un llamado al método para procesar las caracteristicas de la huella 
            this.procesar(Sample);

        }

        public void OnFingerGone(object Capture, string ReaderSerialNumber)
        {

        }

        public void OnFingerTouch(object Capture, string ReaderSerialNumber)
        {

        }

        public void OnReaderConnect(object Capture, string ReaderSerialNumber)
        {

        }

        public void OnReaderDisconnect(object Capture, string ReaderSerialNumber)
        {

        }

        public void OnSampleQuality(object Capture, string ReaderSerialNumber, DPFP.Capture.CaptureFeedback CaptureFeedback)
        {

        }
        #endregion

        private void ptbHuella_Click(object sender, EventArgs e)
        {

        }

        private void bntCapture_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmHuella_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Hacer un llamado al método para detener captura de la huella
            this.detener_captura();
            
        }
    }
}