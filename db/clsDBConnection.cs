﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace EmpenosEvora.db
{
    public class clsDBConnection
    {

        private string _strQuery;
        private string _strAccess;
        public MySqlCommand _mscCmd;
        public MySqlConnection _mscCon;
        public MySqlDataAdapter _msdaAdap;
        public MySqlTransaction _msTa;

        public string strAccess { get => _strAccess; set => _strAccess = value; }
        public string strQuery { get => _strQuery; set => _strQuery = value; }

        public MySqlCommand mscCmd { get => _mscCmd; set => _mscCmd = value; }
        public MySqlConnection mscCon { get => _mscCon; set => _mscCon = value; }

        public MySqlDataAdapter msdaAdap { get => _msdaAdap; set => _msdaAdap = value; }

        public MySqlTransaction msTa { get => _msTa; set => _msTa = value; }




        public clsDBConnection()
        {

            this._strQuery = "";
            this._strAccess = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;
            this.mscCon = new MySqlConnection(this._strAccess);
        }


        public void open_connection()
        {
            this.mscCon.Open();
        }


        public void close_connection()
        {

            if (this.mscCon.State == ConnectionState.Open)
            {
                this.mscCon.Close();
            }
        }

        public void inicializarMSTA()
        {
           this.msTa = this.mscCon.BeginTransaction();
           this.mscCmd.Connection = this.mscCon;
           this.mscCmd.Transaction = this.msTa;

        }
        public void inicializarMSDA()
        {
            
            this.msdaAdap = new MySqlDataAdapter(this.mscCmd);
        }


    




    }
}
