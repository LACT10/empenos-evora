﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UI_Support;

namespace EmpenosEvora
{
    public partial class frmClientes : Form
    {
        public Controller.clsCtrlClientes ctrlClientes;        
        public Controller.clsCtrlAsentamiento ctrlAsentamiento;
        public Controller.clsCtrlIdentificacionesTipo ctrlIdentificacionesTipo;
        public Controller.clsCtrlOcupacion ctrlOcupacion;
        private DPFP.Template template;
        public Core.clsCoreView clsView;

        public int _intUlitmoCodigo;
        
        public PictureBox _pbFoto;
        public PictureBox _pbFotoFrontal;
        public PictureBox _pbFotoTrasera;
        public string _strUbicacionFrontal;
        public string _strNombreFrontal;
        public string _strUbicacionAtras;
        public string _strNombreAtras;
        public string _strFoto;

        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }        
        public PictureBox pbFoto { get => _pbFoto; set => _pbFoto = value; }

        public PictureBox pbFotoFrontal { get => _pbFotoFrontal; set => _pbFotoFrontal = value; }
        public PictureBox pbFotoTrasera { get => _pbFotoTrasera; set => _pbFotoTrasera = value; }
        public string strUbicacionFrontal { get => _strUbicacionFrontal; set => _strUbicacionFrontal = value; }
        public string strNombreFrontal { get => _strNombreFrontal; set => _strNombreFrontal = value; }
        public string strUbicacionAtras { get => _strUbicacionAtras; set => _strUbicacionAtras = value; }
        public string strNombreAtras { get => _strNombreAtras; set => _strNombreAtras = value; }
        public string strFoto { get => _strFoto; set => _strFoto = value; }

        public List<Properties_Class.clsCliente> lsDeshacerCliente;

        public frmClientes()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();

            this.ctrlClientes = new Controller.clsCtrlClientes();
            this.ctrlClientes.Paginacion();
           
            this.ctrlAsentamiento = new Controller.clsCtrlAsentamiento();
            this.ctrlIdentificacionesTipo = new Controller.clsCtrlIdentificacionesTipo();
            this.ctrlOcupacion = new Controller.clsCtrlOcupacion();

            this.lsDeshacerCliente = new List<Properties_Class.clsCliente>();
            this.lsDeshacerCliente.Add(this.ctrlClientes.cliente);            

        }

        private void frmClientes_Load(object sender, EventArgs e)
        {
            cmbGenero.SelectedIndex = 0;
            cmbEstatus.SelectedIndex = 0;
            txtCodigo.MaxLength = this.ctrlClientes.cliente.intNumCerosConsecutivo;

            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);
        }

        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void TsbEliminar_Click(object sender, EventArgs e)
        {
            this.ctrlClientes.Eliminar();
            if (this.ctrlClientes.bolResultado)
            {
                this.ctrlClientes.Paginacion();
                this.limpiar_textbox();
                this.clsView.mensajeExitoDB("cliente", "elimino");
            }
        }


        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlClientes.bolResutladoPaginacion && this.lsDeshacerCliente[0].intId > 0)
            {
                
                txtCodigo.Text = this.lsDeshacerCliente[0].strCodigo;
                txtClienteId.Text = this.lsDeshacerCliente[0].intId.ToString();
                this.intUlitmoCodigo = Int32.Parse(this.lsDeshacerCliente[0].strCodigo);
                txtNombre.Text = this.lsDeshacerCliente[0].strNombre;
                txtApellidoPaterno.Text = this.lsDeshacerCliente[0].strApellidoPaterno;
                txtApellidoMaterno.Text = this.lsDeshacerCliente[0].strApellidoMaterno;
                dtpFechaNacimiento.Text = this.lsDeshacerCliente[0].dtFechaNacimiento.ToString();
                cmbEstatus.Text = this.lsDeshacerCliente[0].strEstatus.ToString();
                cmbGenero.Text = this.lsDeshacerCliente[0].strGenero.ToString();
                txtRFC.Text = this.lsDeshacerCliente[0].strRFC;
                txtCURP.Text = this.lsDeshacerCliente[0].strCURP;
                txtTelefono01.Text = this.lsDeshacerCliente[0].strTelefono01;
                txtTelefono02.Text = this.lsDeshacerCliente[0].strTelefono02;
                txtCorreoElectronico.Text = this.lsDeshacerCliente[0].strCorreoElectronico;
                txtCalle.Text = this.lsDeshacerCliente[0].strCalle;
                txtNoExt.Text = this.lsDeshacerCliente[0].strNumeroExterior;
                txtNoInt.Text = this.lsDeshacerCliente[0].strNumeroInterior;
                txtNombreCotitular.Text = this.lsDeshacerCliente[0].strCotitularNombre;
                txtApellidoPaternoCotitular.Text = this.lsDeshacerCliente[0].strCotitularApellidoPaterno;
                txtApellidoMaternoCotitular.Text = this.lsDeshacerCliente[0].strCotitularApellidoMaterno;
                txtNombreBeneficario.Text = this.lsDeshacerCliente[0].strBeneficiarioNombre;
                txtApellidoPaternoBeneficario.Text = this.lsDeshacerCliente[0].strBeneficiarioApellidoPaterno;
                txtApellidoMaternoBeneficario.Text = this.lsDeshacerCliente[0].strBeneficiarioApellidoMaterno;
                txtComentario.Text = this.lsDeshacerCliente[0].strComentario;
                this.strFoto = this.lsDeshacerCliente[0].strFotografiaImagen;
                this.strNombreFrontal = this.lsDeshacerCliente[0].strIdentificacionImagenFrente;
                this.strNombreAtras = this.lsDeshacerCliente[0].strIdentificacionImagenReverso;
                txtIdOcupacion.Text = this.lsDeshacerCliente[0].ocupacion.intId.ToString();
                txtCodigoOcupacion.Text = this.lsDeshacerCliente[0].ocupacion.strCodigo;
                txtDescripcionOcupacion.Text = this.lsDeshacerCliente[0].ocupacion.strDescripcion;
                txtIdIdentificacion.Text = this.lsDeshacerCliente[0].identificacionesTipo.intId.ToString();
                txtCodigoIdentificacion.Text = this.lsDeshacerCliente[0].identificacionesTipo.strCodigo;
                txtDescripcionIdentificacion.Text = this.lsDeshacerCliente[0].identificacionesTipo.strDescripcion;
                txtAsentamientoId.Text = this.lsDeshacerCliente[0].asentamiento.intId.ToString();
                txtDescripcionAsentamiento.Text = this.lsDeshacerCliente[0].asentamiento.strDescripcion;
                txtCodigoPostal.Text = this.lsDeshacerCliente[0].asentamiento.strCodigoPostal;
                txtCodigoMunicipio.Text = this.lsDeshacerCliente[0].asentamiento.municipio.strCodigo;
                txtDescripcionMunicipio.Text = this.lsDeshacerCliente[0].asentamiento.municipio.strDescripcion;
                txtCodigoEstado.Text = this.lsDeshacerCliente[0].asentamiento.municipio.estado.strCodigo;
                txtDescripcionEstado.Text = this.lsDeshacerCliente[0].asentamiento.municipio.estado.strDescripcion;

                bool bolEstatus = true;
                if (this.ctrlClientes.cliente.strEstatus == this.ctrlClientes.cliente.dicEstatus["NO DESEADO"])
                {

                    string strMsjAdmin = "";
                    if (this.clsView.login.usuario.strAutorizar == this.clsView.login.usuario.dicAutorizar["NO"])
                    {
                        bolEstatus = false;
                        strMsjAdmin = ", favor de contactar al administrador";

                    }
                    else
                    {
                        bolEstatus = true;
                    }

                    this.clsView.principal.epMensaje.SetError(txtNombre, this.clsView.mensajeErrorValiForm2("El cliente " + this.ctrlClientes.cliente.strNombre + " " + this.ctrlClientes.cliente.strApellidoPaterno + " " + this.ctrlClientes.cliente.strApellidoMaterno + " es un cliente no deseado" + strMsjAdmin + "."));
                }

                txtCodigo.Focus();
            }
            else 
            {
                limpiar_textbox();
            }

            this.clsView.principal.epMensaje.Clear();
            txtCodigo.Enabled = true;
            txtCodigo.Focus();
        }


        private void TsbBuscar_Click(object sender, EventArgs e)
        {


            Dictionary<int, Properties_Class.clsTextbox> dicFocus = new Dictionary<int, Properties_Class.clsTextbox>()
            {
                { 0, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigo, strBusqueda = "cliente"}
                },
                { 1, new Properties_Class.clsTextbox()
                                { txtGenerico = txtNombre, strBusqueda = "cliente"}
                },
                { 2, new Properties_Class.clsTextbox()
                                { txtGenerico = txtApellidoPaterno, strBusqueda = "cliente"}
                },
                { 3, new Properties_Class.clsTextbox()
                                { txtGenerico = txtApellidoMaterno, strBusqueda = "cliente"}
                },
                { 4, new Properties_Class.clsTextbox()
                                { txtGenerico = txtApellidoMaterno, strBusqueda = "cliente"}
                },
                { 5, new Properties_Class.clsTextbox()
                                { txtGenerico = txtRFC, strBusqueda = "cliente"}
                },
                { 6, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCURP, strBusqueda = "cliente"}
                },
                { 7, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCorreoElectronico, strBusqueda = "cliente"}
                },
                { 8, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCorreoElectronico, strBusqueda = "cliente"}
                },
                { 9, new Properties_Class.clsTextbox()
                                { txtGenerico = txtTelefono01, strBusqueda = "cliente"}
                },
                { 10, new Properties_Class.clsTextbox()
                                { txtGenerico = txtTelefono02, strBusqueda = "cliente"}
                },
                { 11, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoIdentificacion, strBusqueda = "identificacion"}
                },
                { 12, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcionIdentificacion, strBusqueda = "identificacion"}
                },
                { 13, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoOcupacion, strBusqueda = "ocupacion"}
                },
                { 14, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcionOcupacion, strBusqueda = "ocupacion"}
                },
                { 15, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcionAsentamiento, strBusqueda = "asentamiento"}
                },
                { 16, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCalle, strBusqueda = "cliente"}
                },
                { 17, new Properties_Class.clsTextbox()
                                { txtGenerico = txtNoExt, strBusqueda = "asentamiento"}
                },
                { 18, new Properties_Class.clsTextbox()
                                { txtGenerico = txtNoInt, strBusqueda = "asentamiento"}
                },
                { 19, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoPostal, strBusqueda = "asentamiento"}
                },
                { 20, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoMunicipio, strBusqueda = "asentamiento"}
                },
                { 21, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoMunicipio, strBusqueda = "asentamiento"}
                },
                { 22, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcionMunicipio, strBusqueda = "asentamiento"}
                },
                { 23, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoEstado, strBusqueda = "asentamiento"}
                },
                { 24, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcionEstado, strBusqueda = "asentamiento"}
                },
                { 25, new Properties_Class.clsTextbox()
                                { txtGenerico = txtNombreCotitular, strBusqueda = "cliente"}
                },
                { 26, new Properties_Class.clsTextbox()
                                { txtGenerico = txtApellidoPaternoCotitular, strBusqueda = "cliente"}
                },
                { 27, new Properties_Class.clsTextbox()
                                { txtGenerico = txtApellidoMaternoCotitular, strBusqueda = "cliente"}
                },
                { 28, new Properties_Class.clsTextbox()
                                { txtGenerico = txtNombreBeneficario, strBusqueda = "cliente"}
                },
                { 29, new Properties_Class.clsTextbox()
                                { txtGenerico = txtApellidoPaternoBeneficario, strBusqueda = "cliente"}
                },
                { 30, new Properties_Class.clsTextbox()
                                { txtGenerico = txtApellidoMaternoBeneficario, strBusqueda = "cliente"}
                },
                { 31, new Properties_Class.clsTextbox()
                                { txtGenerico = txtComentario, strBusqueda = "cliente"}
                }

            };
            foreach (KeyValuePair<int, Properties_Class.clsTextbox> data in dicFocus)
            {
                if (data.Value.txtGenerico.Focused)
                {
                    if (data.Value.strBusqueda == "cliente")
                    {
                        this.mostraBusqueda();
                    }
                    else if (data.Value.strBusqueda == "identificacion")
                    {
                        this.mostraBusquedaIdentificacion();
                    }
                    else if (data.Value.strBusqueda == "ocupacion")
                    {
                        this.mostraBusquedaOcupacion();
                        
                    }
                    else if (data.Value.strBusqueda == "asentamiento") 
                    {
                        this.mostraBusquedaAsentamiento();
                    }
                }
            }

            if (cmbGenero.Focused || cmbEstatus.Focused)
            {
                this.mostraBusqueda();
            }



        }
        private void btnFotografia_Click(object sender, EventArgs e)
        {
            if (txtCodigo.Text == "")
            {
                this.clsView.principal.epMensaje.SetError(txtCodigo, this.clsView.mensajeErrorValiForm2("Deber de tener valor el campo código para registrar fotografía"));
            }
            else
            {

                frmFoto frmFoto = new frmFoto();
                frmFoto.ShowDialog();
                this.strFoto = "perfilCliente" + txtCodigo.Text + ".jpg";
                this.pbFoto = frmFoto.pbGenerico;
            }
        }

        private void btnHuella_Click(object sender, EventArgs e)
        {
            if (txtCodigo.Text == "")
            {
                this.clsView.principal.epMensaje.SetError(txtCodigo, this.clsView.mensajeErrorValiForm2("Deber de tener valor el campo código para registrar heulla"));
            }
            else
            {
                frmHuella frmHuella = new frmHuella();
                frmHuella.ShowDialog();
                this.template = frmHuella.template;
            }
            
        }

        private void btnIdentificacion_Click(object sender, EventArgs e)
        {


            if (txtCodigo.Text == "")
            {
                this.clsView.principal.epMensaje.SetError(txtCodigo, this.clsView.mensajeErrorValiForm2("Deber de tener valor el campo código para registrar identificación"));
            }
            else
            {

                frmIdentificacionImagen frmImagen = new frmIdentificacionImagen();
                frmImagen.ShowDialog();             
                this.strNombreAtras = "identificacion_imagen_trasera_"+txtCodigo.Text+ ".jpg";
                this.strNombreFrontal = "identificacion_imagen_frontal_" + txtCodigo.Text + ".jpg";
                
                this.pbFotoFrontal = frmImagen.pbFrontal;
                this.pbFotoTrasera = frmImagen.pbTrasera;

                /* frmSubirArchivos frmSubir = new frmSubirArchivos();            
                 frmSubir.strCodigo = txtCodigo.Text;
                 frmSubir.ShowDialog();

                 if (!(frmSubir.strNombreFrontal == ""))
                 {
                     this.strUbicacionFrontal = frmSubir.strUbicacionFrontal;
                     this.strNombreFrontal = frmSubir.strNombreFrontal;
                 }


                 if (!(frmSubir.strNombreAtras == ""))
                 {
                     this.strUbicacionAtras = frmSubir.strUbicacionAtras;
                     this.strNombreAtras = frmSubir.strNombreAtras;
                 }*/
            }
           

        }

        private void btnVerificarHuella_Click(object sender, EventArgs e)
        {

            VerificationForm frmVerificarHuella = new VerificationForm();
            frmVerificarHuella.objDataSet = this.ctrlClientes.objDataSet;
            
            frmVerificarHuella.ShowDialog();

            if (frmVerificarHuella.bolResultado)
            {
                this.ctrlClientes.BuscarHuella(frmVerificarHuella.objDataSetSingle);
                this.llenar_formulario();
            }

        }


        private void BtnBusqueda_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();
        }

        private void btnBusquedaIdentif_Click(object sender, EventArgs e)
        {
            this.mostraBusquedaIdentificacion();
        }

        private void btnBusquedaOcupacion_Click(object sender, EventArgs e)
        {
            this.mostraBusquedaOcupacion();
        }
        private void BtnBusquedaAsentamiento_Click(object sender, EventArgs e)
        {
            this.mostraBusquedaAsentamiento();
        }

        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlClientes.PrimeroPaginacion();
            if (!this.ctrlClientes.bolResultado) return;            
            this.llenar_formulario();
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlClientes.SiguentePaginacion();
            if (!this.ctrlClientes.bolResultado) return;            
            this.llenar_formulario();
        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlClientes.AtrasPaginacion();
            if (!this.ctrlClientes.bolResultado) return;            
            this.llenar_formulario();

        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlClientes.UltimoPaginacion();
            if (!this.ctrlClientes.bolResultado) return;            
            this.llenar_formulario();
        }
        private void TsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();
            this.ctrlClientes.cliente.limpiarProps();
            
            txtCodigo.Enabled = false;
            txtCodigo.Text = "AUTOGENERADO";

        }

        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);

        }

        private void txtTelefono1_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtTelefono2_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtNoExt_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void txtApellidoPaterno_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void txtApellidoMaterno_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void txtTelefono01_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtTelefono02_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtCodigoIdentificacion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtCodigoOcupacion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }


        private void txtNoInt_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtNombreCotitular_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }


        private void txtApellidoPaternoCotitular_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void txtApellidoMaternoCotitular_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void txtNombreBeneficario_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void txtApellidoPaternoBeneficario_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void txtApellidoMaternoBeneficario_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }


        private void txtCodigo_Leave(object sender, EventArgs e)
        {
            if (txtCodigo.Text == "") 
            {
                limpiar_textbox();
                return;
            }
            
            string strCodigo = this.ctrlClientes.cliente.generaCodigoConsecutivo(txtCodigo.Text);
            this.ctrlClientes.Buscar(strCodigo);
            if (!(this.ctrlClientes.mdlClientes.bolResultado) || txtCodigo.Text.Trim().Length == 0) {
                this.limpiar_textbox();
                return;
            };
            
            this.llenar_formulario();
        }


        private void txtCorreoElectronico_Leave(object sender, EventArgs e)
        {
            if (!(txtCorreoElectronico.Text == "") && !(this.clsView.validarCorreoElectronico(txtCorreoElectronico.Text)))
            {
                txtCorreoElectronico.Focus();
                this.clsView.principal.epMensaje.SetError(txtCorreoElectronico, this.clsView.mensajeErrorValiForm2("El campo correo electrónico no es válido, favor de ingresar un correo electrónico "));
            }
        }

        private void txtCodigoIdentificacion_Leave(object sender, EventArgs e)
        {
            if (txtCodigoIdentificacion.Text == "") 
            {
                txtIdIdentificacion.Text = "";
                txtCodigoIdentificacion.Text = "";
                txtDescripcionIdentificacion.Text = "";
                this.ctrlIdentificacionesTipo.limpiarProps();
                return;
            } 
            string strCodigo = this.ctrlIdentificacionesTipo.identificacionTipo.generaCodigoConsecutivo(txtCodigoIdentificacion.Text);

            this.ctrlIdentificacionesTipo.Buscar(strCodigo);            
            txtIdIdentificacion.Text = this.ctrlIdentificacionesTipo.identificacionTipo.intId.ToString();
            txtCodigoIdentificacion.Text = this.ctrlIdentificacionesTipo.identificacionTipo.strCodigo;
            txtDescripcionIdentificacion.Text = this.ctrlIdentificacionesTipo.identificacionTipo.strDescripcion;
            txtIdentificacionValidar.Text = this.ctrlIdentificacionesTipo.identificacionTipo.strValidarCurp;
        }

        private void txtCodigoOcupacion_Leave(object sender, EventArgs e)
        {
            if (txtCodigoOcupacion.Text == "") 
            {
                txtIdOcupacion.Text = "";
                txtCodigoOcupacion.Text = "";
                txtDescripcionOcupacion.Text = "";
                this.ctrlOcupacion.limpiarProps();
                return;
            } 
            string strCodigo = this.ctrlOcupacion.ocupacion.generaCodigoConsecutivo(txtCodigoOcupacion.Text);
            this.ctrlOcupacion.Buscar(strCodigo);            
            txtIdOcupacion.Text = this.ctrlOcupacion.ocupacion.intId.ToString();
            txtCodigoOcupacion.Text = this.ctrlOcupacion.ocupacion.strCodigo;
            txtDescripcionOcupacion.Text = this.ctrlOcupacion.ocupacion.strDescripcion;
        }

        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            //this.clsView.soloTextoVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            this.clsView.principal.epMensaje.Clear();
            cmbGenero.SelectedIndex = 0;
            cmbEstatus.SelectedIndex = 0;
            this.pbFoto = null;
            this.pbFotoFrontal = null;
            this.pbFotoTrasera = null;
            this.template = null;
            txtApellidoPaternoCotitular.Text = "";
            txtApellidoMaternoCotitular.Text = "";
            txtNombreCotitular.Text = "";
            txtApellidoMaternoBeneficario.Text = "";
            txtApellidoPaternoBeneficario.Text = "";
            txtNumeroIdentificacion.Text = "";
            txtNombreBeneficario.Text = "";
            this.strUbicacionFrontal = "";
            this.strUbicacionAtras = "";
            btnFooterGuardar.Enabled = true;
            tsbGuardar.Enabled = true;
           // txtCodigo.Enabled = true;
        }

        private bool validacion()
        {

            bool bolValidacion = true;
            string strCodigo = txtCodigo.Text.TrimStart();
            string strNombre = txtNombre.Text.TrimStart();
            string strApellidoPaterno = txtApellidoPaterno.Text.TrimStart();
            string strApellidoMaterno = txtApellidoMaterno.Text.TrimStart();            
          
            string dtFechaNacimiento = dtpFechaNacimiento.Text.TrimStart();
           
            string strCURP = txtCURP.Text.TrimStart();
            string strCorreoElectronico = txtCorreoElectronico.Text.TrimStart();
            int intGenero = cmbGenero.SelectedIndex;

            string strIdentificacion = txtDescripcionIdentificacion.Text.TrimStart();
            //string strOcupacion = txtDescripcionOcupacion.Text.TrimStart();
            string strAsentamiento = txtDescripcionAsentamiento.Text.TrimStart();

            string strCalle = txtCalle.Text.TrimStart();
            string strNumeroInterior = txtNoInt.Text.TrimStart();
            string strNumeroExterior = txtNoExt.Text.TrimStart();
            string strNumeroIdentificacion = txtNumeroIdentificacion.Text.TrimStart();


            /*string strNombreCotitular = txtNombreCotitular.Text.TrimStart();
            string strApellidoPaternoCotitular = txtApellidoPaternoCotitular.Text.TrimStart();
            string strApellidoMaternoCotitular = txtApellidoMaternoCotitular.Text.TrimStart();
            string strNombreBeneficario = txtNombreBeneficario.Text.TrimStart();
            string strApellidoPaternoBeneficario = txtApellidoPaternoBeneficario.Text.TrimStart();
            string strApellidoMaternoBeneficario = txtApellidoMaternoBeneficario.Text.TrimStart();
            string strComentario = txtComentario.Text.TrimStart();*/

            string strIdentificacionValidar = txtIdentificacionValidar.Text.TrimStart();
            
            if (strCodigo == "")
            {
                txtCodigo.Focus();                
                this.clsView.principal.epMensaje.SetError(txtCodigo, this.clsView.mensajeErrorValiForm2("El campo código es obligatorio, favor de ingresar un código "));
                bolValidacion = false;
            }

            if (strNombre == "")
            {
                txtNombre.Focus();
                this.clsView.principal.epMensaje.SetError(txtNombre, this.clsView.mensajeErrorValiForm2("El campo descripción es obligatorio, favor de ingresar una descripción "));
                bolValidacion = false;
            }

            if (strApellidoPaterno == "" && strApellidoMaterno == "")
            {
                txtApellidoPaterno.Focus();
                this.clsView.principal.epMensaje.SetError(txtApellidoPaterno, this.clsView.mensajeErrorValiForm2("El campo apellido  es obligatorio, favor de ingresar un apellido  "));
                bolValidacion = false;
            }


            if (intGenero == 0)
            {
                cmbGenero.Focus();
                this.clsView.principal.epMensaje.SetError(cmbGenero, this.clsView.mensajeErrorValiForm2("El campo género es obligatorio, favor de selecionar un género "));
                bolValidacion = false;
            }

            if (strApellidoMaterno == "" && strApellidoPaterno == "" )
            {
                txtApellidoMaterno.Focus();
                this.clsView.principal.epMensaje.SetError(txtApellidoMaterno, this.clsView.mensajeErrorValiForm2("El campo apellido es obligatorio, favor de ingresar un apellido "));
                bolValidacion = false;
            }


            if (dtFechaNacimiento == "")
            {
                dtpFechaNacimiento.Focus();
                this.clsView.principal.epMensaje.SetError(dtpFechaNacimiento, this.clsView.mensajeErrorValiForm2("El campo fecah de nacimiento es obligatorio, favor de ingresar un fecah de nacimiento "));
                bolValidacion = false;
            }


            if (strCURP == "" && strIdentificacionValidar == "SI")
            {
                txtCURP.Focus();
                this.clsView.principal.epMensaje.SetError(txtCURP, this.clsView.mensajeErrorValiForm2("El campo CURP es obligatorio y debe ser de 18 caracteres, favor de ingresar un CURP "));
                bolValidacion = false;
            }
            
            if (!(strCorreoElectronico == "") && !(this.clsView.validarCorreoElectronico(txtCorreoElectronico.Text)))
            {
                txtCorreoElectronico.Focus();
                this.clsView.principal.epMensaje.SetError(txtCorreoElectronico, this.clsView.mensajeErrorValiForm2("El campo correo electrónico no es valido , favor de ingresar un correo electrónico "));
                bolValidacion = false;
            }


            if (strIdentificacion == "" )
            {
                txtDescripcionIdentificacion.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcionIdentificacion, this.clsView.mensajeErrorValiForm2("El campo identificación es obligatorio, favor de ingresar una identificación "));
                bolValidacion = false;
            }

            /*if (strOcupacion == "")
            {
                txtDescripcionOcupacion.Focus();                
                this.clsView.principal.epMensaje.SetError(txtDescripcionOcupacion, this.clsView.mensajeErrorValiForm2("El campo ocupación, favor de ingresar un ocupación "));
                bolValidacion = false;
            }*/

            if (strAsentamiento == "")
            {
                txtDescripcionAsentamiento.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcionAsentamiento, this.clsView.mensajeErrorValiForm2("El campo asentamiento, favor de ingresar un asentamiento "));
                bolValidacion = false;
            }


            if (strCalle == "")
            {
                txtCalle.Focus();
                this.clsView.principal.epMensaje.SetError(txtCalle, this.clsView.mensajeErrorValiForm2("El campo calle es obligatorio, favor de ingresar un calle "));
                bolValidacion = false;
            }

            if (strNumeroIdentificacion == "")
            {
                txtNumeroIdentificacion.Focus();
                this.clsView.principal.epMensaje.SetError(txtNumeroIdentificacion, this.clsView.mensajeErrorValiForm2("El campo número de identificación es obligatorio, favor de ingresar un número de identificación "));
                bolValidacion = false;
            }

            

            /* if (strNumeroInterior == "")
             {
                 txtNoInt.Focus();
                 this.clsView.principal.epMensaje.SetError(txtNoInt, this.clsView.mensajeErrorValiForm2("El campo numero interior es obligatorio, favor de ingresar un numero interior "));
                 bolValidacion = false;
             }*/

            /*if (strNumeroExterior == "")
            {
                txtNoExt.Focus();
                this.clsView.principal.epMensaje.SetError(txtNoExt, this.clsView.mensajeErrorValiForm2("El campo numero exterior es obligatorio, favor de ingresar un numero exterior "));
                bolValidacion = false;
            }

            if (strNombreCotitular == "")
            {
                txtNombreCotitular.Focus();
                this.clsView.principal.epMensaje.SetError(txtNombreCotitular, this.clsView.mensajeErrorValiForm2("El campo nombre cotitular es obligatorio, favor de ingresar un nombre cotitular "));
                bolValidacion = false;
            }

            if (strApellidoPaternoCotitular == "")
            {
                txtApellidoPaternoCotitular.Focus();
                this.clsView.principal.epMensaje.SetError(txtApellidoPaternoCotitular, this.clsView.mensajeErrorValiForm2("El campo apellido paterno cotitular es obligatorio, favor de ingresar un apellido paterno cotitular "));
                bolValidacion = false;
            }

            if (strApellidoMaternoCotitular == "")
            {
                txtApellidoMaternoCotitular.Focus();
                this.clsView.principal.epMensaje.SetError(txtApellidoMaternoCotitular, this.clsView.mensajeErrorValiForm2("El campo apellido materno cotitular es obligatorio, favor de ingresar un apellido materno cotitular "));
                bolValidacion = false;
            }

            if (strNombreBeneficario == "")
            {
                txtNombreBeneficario.Focus();
                this.clsView.principal.epMensaje.SetError(txtNombreBeneficario, this.clsView.mensajeErrorValiForm2("El campo nombre beneficario es obligatorio, favor de ingresar un nombre beneficario "));
                bolValidacion = false;
            }

            if (strApellidoPaternoBeneficario == "")
            {
                txtApellidoPaternoBeneficario.Focus();
                this.clsView.principal.epMensaje.SetError(txtApellidoPaternoBeneficario, this.clsView.mensajeErrorValiForm2("El campo apellido paterno beneficario es obligatorio, favor de ingresar un apellido paterno beneficario "));
                bolValidacion = false;
            }

            if (strApellidoMaternoBeneficario == "")
            {
                txtApellidoMaternoBeneficario.Focus();
                this.clsView.principal.epMensaje.SetError(txtApellidoMaternoBeneficario, this.clsView.mensajeErrorValiForm2("El campo apellido materno beneficario es obligatorio, favor de ingresar un apellido materno beneficario "));
                bolValidacion = false;
            }

            if (strComentario == "")
            {
                txtComentario.Focus();
                this.clsView.principal.epMensaje.SetError(txtComentario, this.clsView.mensajeErrorValiForm2("El campo comentario es obligatorio, favor de ingresar un comentario "));
                bolValidacion = false;
            }*/
            if (this.template == null)
            {
                this.clsView.principal.epMensaje.SetError(btnHuella, this.clsView.mensajeErrorValiForm2("La huella es obligatoria, favor de capturarla "));
                bolValidacion = false;
            }

            if (this.pbFoto == null)
            {
                this.clsView.principal.epMensaje.SetError(btnFotografia, this.clsView.mensajeErrorValiForm2("La fotografía es obligatoria, favor de capturarla "));
                bolValidacion = false;
            }

            if (this.pbFotoFrontal == null)
            {
                this.clsView.principal.epMensaje.SetError(btnIdentificacion, this.clsView.mensajeErrorValiForm2("La identificación frontal es obligatoria, favor de capturarla "));
                bolValidacion = false;
            }

            if (this.pbFotoTrasera == null)
            {
                this.clsView.principal.epMensaje.SetError(btnIdentificacion, this.clsView.mensajeErrorValiForm2("La identificación trasera es obligatoria, favor de capturarla "));
                bolValidacion = false;
            }

            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();
            bool bolValidacion = false;

            if (validacion())
            {


                if (this.ctrlClientes.bolResutladoPaginacion)
                {
                    DataTable dtDatos = this.ctrlClientes.objDataSet.Tables[0];
                    this.intUlitmoCodigo = Int32.Parse(dtDatos.Rows[dtDatos.Rows.Count - 1][1].ToString()) + 1;
                    this.agregarDeshacer();
                }
                else
                {
                    this.intUlitmoCodigo = 1;
                }

                txtCodigo.Text = this.intUlitmoCodigo.ToString().PadLeft(this.ctrlClientes.cliente.intNumCerosConsecutivo, '0');

                this.ctrlClientes.cliente.intId = Int32.Parse((txtClienteId.Text == "") ? "0" : txtClienteId.Text);
                this.ctrlClientes.cliente.strCodigo = this.intUlitmoCodigo.ToString().PadLeft(this.ctrlClientes.cliente.intNumCerosConsecutivo, '0');
                this.ctrlClientes.cliente.strNombre = txtNombre.Text.ToString();
                this.ctrlClientes.cliente.strApellidoPaterno = txtApellidoPaterno.Text.ToString();
                this.ctrlClientes.cliente.strApellidoMaterno = txtApellidoMaterno.Text.ToString();
                this.ctrlClientes.cliente.strEstatus = cmbEstatus.SelectedItem.ToString();
                this.ctrlClientes.cliente.strGenero = cmbGenero.SelectedItem.ToString();
                this.ctrlClientes.cliente.dtFechaNacimiento = Convert.ToDateTime(dtpFechaNacimiento.Text.ToString());                
                this.ctrlClientes.cliente.strRFC = txtRFC.Text.ToString();
                this.ctrlClientes.cliente.strCURP = txtCURP.Text.ToString();
                this.ctrlClientes.cliente.strCorreoElectronico = txtCorreoElectronico.Text.ToString();
                this.ctrlClientes.cliente.strTelefono01 = txtTelefono01.Text.ToString();
                this.ctrlClientes.cliente.strTelefono02 = txtTelefono02.Text.ToString();
                this.ctrlClientes.cliente.identificacionesTipo.intId = Int32.Parse((txtIdIdentificacion.Text == "") ? "0" : txtIdIdentificacion.Text);
                this.ctrlClientes.cliente.ocupacion.intId = Int32.Parse((txtIdOcupacion.Text == "") ? "0" : txtIdOcupacion.Text);
                this.ctrlClientes.cliente.asentamiento.intId = Int32.Parse((txtAsentamientoId.Text == "") ? "0" : txtAsentamientoId.Text);
                this.ctrlClientes.cliente.strCalle = txtCalle.Text.ToString();
                this.ctrlClientes.cliente.strNumeroExterior = txtNoExt.Text.ToString();
                this.ctrlClientes.cliente.strNumeroInterior = txtNoInt.Text.ToString();
                this.ctrlClientes.cliente.strCotitularNombre = txtNombreCotitular.Text.TrimStart();
                this.ctrlClientes.cliente.strCotitularApellidoPaterno = txtApellidoPaternoCotitular.Text.TrimStart();
                this.ctrlClientes.cliente.strCotitularApellidoMaterno = txtApellidoMaternoCotitular.Text.TrimStart();
                this.ctrlClientes.cliente.strBeneficiarioNombre = txtNombreBeneficario.Text.TrimStart();
                this.ctrlClientes.cliente.strBeneficiarioApellidoPaterno = txtApellidoPaternoBeneficario.Text.TrimStart();
                this.ctrlClientes.cliente.strBeneficiarioApellidoMaterno = txtApellidoMaternoBeneficario.Text.TrimStart();
                this.ctrlClientes.cliente.strComentario = txtComentario.Text.TrimStart();
                this.ctrlClientes.cliente.strNumeroIdentificacion = txtNumeroIdentificacion.Text.TrimStart();
                this.ctrlClientes.cliente.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlClientes.cliente.intSucursal = this.clsView.login.intSucursalId;

                if (!(this.template == null)) 
                {
                    this.ctrlClientes.cliente.msHuella = new MemoryStream(this.template.Bytes);
                }                

                this.ctrlClientes.cliente.strFotografiaImagen = this.strFoto;
                this.ctrlClientes.cliente.strIdentificacionImagenFrente = this.strNombreFrontal;
                this.ctrlClientes.cliente.strIdentificacionImagenReverso = this.strNombreAtras;
                
                string pathImagen = Application.StartupPath.Replace("bin\\Debug", "Resources\\PerfilCliente\\");
                string pathImagenIdentificacion = Application.StartupPath.Replace("bin\\Debug", "Resources\\identificacionCliente\\");

                if (this.clsView.login.usuario.strAutorizar == this.clsView.login.usuario.dicAutorizar["SI"] && this.ctrlClientes.cliente.strEstatus == this.ctrlClientes.cliente.dicEstatus["NO DESEADO"])
                {

                    if (this.clsView.confirmacionGuardarCliente(this.Text))
                    {
                        bolValidacion = true;
                    }
                }
                else
                {
                    bolValidacion = true;
                }

                if (!(bolValidacion)) return;

                this.ctrlClientes.Guardar();

                if (this.ctrlClientes.mdlClientes.bolResultado)
                {

                    if (!(this.pbFoto == null)) 
                    {
                        this.pbFoto.Image.Save(pathImagen + this.ctrlClientes.cliente.strFotografiaImagen, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }

                    if (!(this.pbFotoFrontal == null)) 
                    {
                        this.pbFoto.Image.Save(pathImagenIdentificacion + this.ctrlClientes.cliente.strIdentificacionImagenFrente, System.Drawing.Imaging.ImageFormat.Jpeg);
                        //System.IO.File.Copy(this.strUbicacionFrontal, this.ctrlClientes.cliente.strIdentificacionImagenFrente, true);
                    }

                    if (!(this.pbFotoTrasera == null)) 
                    {
                        this.pbFoto.Image.Save(pathImagenIdentificacion + this.ctrlClientes.cliente.strIdentificacionImagenReverso, System.Drawing.Imaging.ImageFormat.Jpeg);
                        //System.IO.File.Copy(this.strUbicacionAtras, this.ctrlClientes.cliente.strIdentificacionImagenReverso, true);
                    }
                    
                    
                    this.limpiar_textbox();
                    this.ctrlClientes.Paginacion();

                    this.ctrlClientes.Buscar(this.ctrlClientes.cliente.strCodigo);
                    this.llenar_formulario();
                    this.clsView.mensajeExitoDB("cliente", "guardó");
                    txtCodigo.Enabled = true;
                    txtCodigo.Focus();
                }
            }
        }

        public void mostraBusqueda()
        {
            if (this.ctrlClientes.bolResutladoPaginacion)
            {
                this.ctrlClientes.CargarFrmBusqueda();

                if (this.ctrlClientes.bolResultadoBusqueda)
                {
                    this.llenar_formulario();
                }
            }

            
        }


        public void mostraBusquedaIdentificacion()
        {
            this.ctrlIdentificacionesTipo.Paginacion();
            if (this.ctrlIdentificacionesTipo.bolResutladoPaginacion)
            {
                ctrlIdentificacionesTipo.CargarFrmBusqueda();


                if (ctrlIdentificacionesTipo.bolResultadoBusqueda)
                {
                    txtIdIdentificacion.Text = this.ctrlIdentificacionesTipo.identificacionTipo.intId.ToString();
                    txtCodigoIdentificacion.Text = this.ctrlIdentificacionesTipo.identificacionTipo.strCodigo;
                    txtDescripcionIdentificacion.Text = this.ctrlIdentificacionesTipo.identificacionTipo.strDescripcion;
                    txtIdentificacionValidar.Text = this.ctrlIdentificacionesTipo.identificacionTipo.strValidarCurp;
                    txtCodigoIdentificacion.Focus();
                    
                }
            }
            
        }

        public void mostraBusquedaOcupacion()
        {
            this.ctrlOcupacion.Paginacion();
            if (this.ctrlOcupacion.bolResutladoPaginacion)
            {
                ctrlOcupacion.CargarFrmBusqueda();

                if (ctrlOcupacion.bolResultadoBusqueda)
                {
                    txtIdOcupacion.Text = this.ctrlOcupacion.ocupacion.intId.ToString();
                    txtCodigoOcupacion.Text = this.ctrlOcupacion.ocupacion.strCodigo;
                    txtDescripcionOcupacion.Text = this.ctrlOcupacion.ocupacion.strDescripcion;
                    txtDescripcionOcupacion.Focus();
                }
                
            }
            
        }

        public void mostraBusquedaAsentamiento()
        {
            this.ctrlAsentamiento.Paginacion();
            if (this.ctrlAsentamiento.bolResutladoPaginacion)
            {
                ctrlAsentamiento.CargarFrmBusqueda();

                if (ctrlAsentamiento.bolResultadoBusqueda)
                {
                    txtAsentamientoId.Text = this.ctrlAsentamiento.asentamiento.intId.ToString();
                    txtDescripcionAsentamiento.Text = this.ctrlAsentamiento.asentamiento.strDescripcion.ToString();
                    txtCodigoPostal.Text = this.ctrlAsentamiento.asentamiento.strCodigoPostal.ToString();
                    txtCodigoMunicipio.Text = this.ctrlAsentamiento.asentamiento.municipio.strCodigo.ToString();
                    txtDescripcionMunicipio.Text = this.ctrlAsentamiento.asentamiento.municipio.strDescripcion.ToString();
                    txtCodigoEstado.Text = this.ctrlAsentamiento.asentamiento.municipio.estado.strCodigo.ToString();
                    txtDescripcionEstado.Text = this.ctrlAsentamiento.asentamiento.municipio.estado.strDescripcion.ToString();
                    txtDescripcionAsentamiento.Focus();
                }
            }

            
        }


        public void llenar_formulario()
        {

            if (this.ctrlClientes.cliente.strCodigo.Length == 0) return;
            txtCodigo.Enabled = true;
            this.clsView.principal.epMensaje.Clear();
            txtCodigo.Text = this.ctrlClientes.cliente.strCodigo;
            txtClienteId.Text = this.ctrlClientes.cliente.intId.ToString();
            this.intUlitmoCodigo = Int32.Parse(this.ctrlClientes.cliente.strCodigo);
            txtNombre.Text = this.ctrlClientes.cliente.strNombre;
            txtApellidoPaterno.Text = this.ctrlClientes.cliente.strApellidoPaterno;
            txtApellidoMaterno.Text = this.ctrlClientes.cliente.strApellidoMaterno;
            dtpFechaNacimiento.Text = this.ctrlClientes.cliente.dtFechaNacimiento.ToString();
            cmbEstatus.Text = this.ctrlClientes.cliente.strEstatus.ToString();
            cmbGenero.Text = this.ctrlClientes.cliente.strGenero.ToString();            
            txtRFC.Text = this.ctrlClientes.cliente.strRFC;
            txtCURP.Text = this.ctrlClientes.cliente.strCURP;
            txtCurpAntri.Text = this.ctrlClientes.cliente.strCURP;
            txtTelefono01.Text = this.ctrlClientes.cliente.strTelefono01;
            txtTelefono02.Text = this.ctrlClientes.cliente.strTelefono02;
            txtCorreoElectronico.Text = this.ctrlClientes.cliente.strCorreoElectronico;
            txtCalle.Text = this.ctrlClientes.cliente.strCalle;
            txtNoExt.Text = this.ctrlClientes.cliente.strNumeroExterior;
            txtNoInt.Text = this.ctrlClientes.cliente.strNumeroInterior;
            txtNombreCotitular.Text = this.ctrlClientes.cliente.strCotitularNombre;
            txtApellidoPaternoCotitular.Text = this.ctrlClientes.cliente.strCotitularApellidoPaterno;
            txtApellidoMaternoCotitular.Text = this.ctrlClientes.cliente.strCotitularApellidoMaterno;
            txtNombreBeneficario.Text = this.ctrlClientes.cliente.strBeneficiarioNombre;
            txtApellidoPaternoBeneficario.Text = this.ctrlClientes.cliente.strBeneficiarioApellidoPaterno;
            txtApellidoMaternoBeneficario.Text = this.ctrlClientes.cliente.strBeneficiarioApellidoMaterno;
            txtComentario.Text = this.ctrlClientes.cliente.strComentario;
            this.strFoto = this.ctrlClientes.cliente.strFotografiaImagen;
            this.strNombreFrontal = this.ctrlClientes.cliente.strIdentificacionImagenFrente;
            this.strNombreAtras = this.ctrlClientes.cliente.strIdentificacionImagenReverso;
            txtIdOcupacion.Text = this.ctrlClientes.cliente.ocupacion.intId.ToString();
            txtCodigoOcupacion.Text = this.ctrlClientes.cliente.ocupacion.strCodigo;
            txtDescripcionOcupacion.Text = this.ctrlClientes.cliente.ocupacion.strDescripcion;
            txtIdIdentificacion.Text = this.ctrlClientes.cliente.identificacionesTipo.intId.ToString();
            txtCodigoIdentificacion.Text = this.ctrlClientes.cliente.identificacionesTipo.strCodigo;
            txtDescripcionIdentificacion.Text = this.ctrlClientes.cliente.identificacionesTipo.strDescripcion;
            txtAsentamientoId.Text = this.ctrlClientes.cliente.asentamiento.intId.ToString();
            txtDescripcionAsentamiento.Text = this.ctrlClientes.cliente.asentamiento.strDescripcion;
            txtCodigoPostal.Text = this.ctrlClientes.cliente.asentamiento.strCodigoPostal;
            txtCodigoMunicipio.Text = this.ctrlClientes.cliente.asentamiento.municipio.strCodigo;
            txtDescripcionMunicipio.Text = this.ctrlClientes.cliente.asentamiento.municipio.strDescripcion;
            txtCodigoEstado.Text = this.ctrlClientes.cliente.asentamiento.municipio.estado.strCodigo;
            txtDescripcionEstado.Text = this.ctrlClientes.cliente.asentamiento.municipio.estado.strDescripcion;
            txtNumeroIdentificacion.Text = this.ctrlClientes.cliente.strNumeroIdentificacion;

            bool bolEstatus = true;
            if (this.ctrlClientes.cliente.strEstatus == this.ctrlClientes.cliente.dicEstatus["NO DESEADO"])
            {

                string strMsjAdmin = "";
                if (this.clsView.login.usuario.strAutorizar == this.clsView.login.usuario.dicAutorizar["NO"])
                {
                    bolEstatus = false;
                    strMsjAdmin = ", favor de contactar al administrador";

                }
                else
                {
                    bolEstatus = true;
                }
                                
                this.clsView.principal.epMensaje.SetError(txtNombre, this.clsView.mensajeErrorValiForm2("El cliente "+this.ctrlClientes.cliente.strNombre+" "+ this.ctrlClientes.cliente.strApellidoPaterno + " "+this.ctrlClientes.cliente.strApellidoMaterno+ " es un cliente no deseado" + strMsjAdmin + "."));
            }
           
            btnFooterGuardar.Enabled = bolEstatus;
            tsbGuardar.Enabled = bolEstatus;
            
                
        }


        public void agregarDeshacer()
        {
            this.lsDeshacerCliente.RemoveAt(0);
            this.lsDeshacerCliente.Add(this.ctrlClientes.cliente);
        }



        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }



        private void FrmSucursales_Load(object sender, EventArgs e)
        {

        }

        private void txtNombre_Leave(object sender, EventArgs e)
        {

        }

        private void toolStrip2_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void txtCURP_Leave(object sender, EventArgs e)
        {

            if (txtCurpAntri.Text.ToString() == txtCURP.Text.ToString()) return;

            this.ctrlClientes.cliente.strCURP = txtCURP.Text;
            this.ctrlClientes.BuscarCurp();
            if (this.ctrlClientes.mdlClientes.bolResultado) 
            {
                this.clsView.principal.epMensaje.SetError(txtCURP, this.clsView.mensajeErrorValiForm2("La CURP debe ser única, favor de verificar"));
                txtCURP.Text = "";
            }

        }

        private void txtDescripcionAsentamiento_Leave(object sender, EventArgs e)
        {
            if (txtDescripcionAsentamiento.Text == "") {
                txtAsentamientoId.Text = "";
                txtDescripcionAsentamiento.Text = "";
                txtCodigoPostal.Text = "";
                txtCodigoMunicipio.Text = "";
                txtDescripcionMunicipio.Text = "";
                txtCodigoEstado.Text = "";
                txtDescripcionEstado.Text = "";

            }
        }

        private void cmbGenero_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
