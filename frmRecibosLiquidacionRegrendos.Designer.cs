﻿namespace EmpenosEvora
{
    partial class frmRecibosLiquidacionRegrendos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label4 = new System.Windows.Forms.Label();
            this.txtHora = new System.Windows.Forms.TextBox();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFolio = new System.Windows.Forms.TextBox();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.rbtLiquidacion = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.rbtRefrendo = new System.Windows.Forms.RadioButton();
            this.rbtPPI = new System.Windows.Forms.RadioButton();
            this.lblMesesPPI = new System.Windows.Forms.Label();
            this.lblDescripcionBoletaRefrendo = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtAcumCapital = new System.Windows.Forms.TextBox();
            this.txtAcumBonificaciones = new System.Windows.Forms.TextBox();
            this.txtAcumIntereses = new System.Windows.Forms.TextBox();
            this.txtAcumAbonoCap = new System.Windows.Forms.TextBox();
            this.txtAcumSubTotal = new System.Windows.Forms.TextBox();
            this.txtAcumPPI = new System.Windows.Forms.TextBox();
            this.txtAcumPagar = new System.Windows.Forms.TextBox();
            this.txtBonificacion = new System.Windows.Forms.TextBox();
            this.lblBoletaBonificacion = new System.Windows.Forms.Label();
            this.lblBoletaAbonoCapital = new System.Windows.Forms.Label();
            this.txtAbonoCapital = new System.Windows.Forms.TextBox();
            this.txtSemanasPPI = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCodigoCliente = new System.Windows.Forms.TextBox();
            this.btnVerificarHuellaCliente = new System.Windows.Forms.Button();
            this.txtClienteId = new System.Windows.Forms.TextBox();
            this.btnBusquedaCliente = new System.Windows.Forms.Button();
            this.txtNombreCliente = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNombreEmpleado = new System.Windows.Forms.TextBox();
            this.txtDescripcionCaja = new System.Windows.Forms.TextBox();
            this.txtCodigoCaja = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.chkDeselecionarBoleta = new System.Windows.Forms.CheckBox();
            this.tsAcciones = new System.Windows.Forms.ToolStrip();
            this.tsbNuevo = new System.Windows.Forms.ToolStripButton();
            this.tsbGuardar = new System.Windows.Forms.ToolStripButton();
            this.tsbEliminar = new System.Windows.Forms.ToolStripButton();
            this.tsbBuscar = new System.Windows.Forms.ToolStripButton();
            this.btnUndo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrimeroPag = new System.Windows.Forms.ToolStripButton();
            this.btnAtras = new System.Windows.Forms.ToolStripButton();
            this.btnAdelante = new System.Windows.Forms.ToolStripButton();
            this.btnUlitmoPag = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbImprimir = new System.Windows.Forms.ToolStripButton();
            this.btnCierra = new System.Windows.Forms.ToolStripButton();
            this.btnBusqueda = new System.Windows.Forms.Button();
            this.btnFooterCierra = new System.Windows.Forms.Button();
            this.btnFooterGuardar = new System.Windows.Forms.Button();
            this.txtDescripcionFormaPago = new System.Windows.Forms.TextBox();
            this.txtCodigoFormaPago = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnBusquedaFormaPago = new System.Windows.Forms.Button();
            this.txtFormaPaogId = new System.Windows.Forms.TextBox();
            this.dgvDetalles = new System.Windows.Forms.DataGridView();
            this.diaMinimoPromocion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.porcentajeTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.diaPromocion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.diaProcentaje = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cobrosTipoNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.diasMinimo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prendaTipoId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cobroTipoId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.porcentajeCobroTipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.importeCobroTipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.importeCobroTipoBonificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaAutorizacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usuarioAutorizacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.motivoAutorizacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.boleta_empeno_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Boleta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Día = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Capital = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Intereses = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Bonificar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Subtotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sel = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.AbonoCap = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PPIAplica = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pagar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lkbBonifiacion = new System.Windows.Forms.LinkLabel();
            this.lkbAbonoCapital = new System.Windows.Forms.LinkLabel();
            this.lblListaBoletasRefrendadas = new System.Windows.Forms.Label();
            this.btnFotografia = new System.Windows.Forms.Button();
            this.lblBoletaBonifiacionFolio = new System.Windows.Forms.Label();
            this.lblBoletaAbonoCapFolio = new System.Windows.Forms.Label();
            this.btnVerCancelacion = new System.Windows.Forms.Button();
            this.lblEstatus = new System.Windows.Forms.Label();
            this.lblDescEstatus = new System.Windows.Forms.Label();
            this.txtCobroTotal = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtEfectivo = new System.Windows.Forms.TextBox();
            this.txtCambio = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.lkbCliente = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.txtSemanasPPI)).BeginInit();
            this.tsAcciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalles)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(368, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 332;
            this.label4.Text = "Hora";
            // 
            // txtHora
            // 
            this.txtHora.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtHora.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtHora.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHora.Location = new System.Drawing.Point(406, 72);
            this.txtHora.MaxLength = 5;
            this.txtHora.Name = "txtHora";
            this.txtHora.ReadOnly = true;
            this.txtHora.Size = new System.Drawing.Size(62, 20);
            this.txtHora.TabIndex = 4;
            // 
            // dtpFecha
            // 
            this.dtpFecha.Cursor = System.Windows.Forms.Cursors.Default;
            this.dtpFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecha.Location = new System.Drawing.Point(256, 72);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(98, 20);
            this.dtpFecha.TabIndex = 3;
            this.dtpFecha.ValueChanged += new System.EventHandler(this.dtpFecha_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(208, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 329;
            this.label3.Text = "Fecha";
            // 
            // txtFolio
            // 
            this.txtFolio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFolio.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtFolio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFolio.Location = new System.Drawing.Point(55, 72);
            this.txtFolio.MaxLength = 5;
            this.txtFolio.Name = "txtFolio";
            this.txtFolio.Size = new System.Drawing.Size(96, 20);
            this.txtFolio.TabIndex = 1;
            this.txtFolio.Leave += new System.EventHandler(this.txtFolio_Leave);
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(8, 72);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(29, 13);
            this.lblCodigo.TabIndex = 322;
            this.lblCodigo.Text = "Folio";
            // 
            // rbtLiquidacion
            // 
            this.rbtLiquidacion.AutoSize = true;
            this.rbtLiquidacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbtLiquidacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtLiquidacion.Location = new System.Drawing.Point(149, 104);
            this.rbtLiquidacion.Name = "rbtLiquidacion";
            this.rbtLiquidacion.Size = new System.Drawing.Size(79, 17);
            this.rbtLiquidacion.TabIndex = 6;
            this.rbtLiquidacion.TabStop = true;
            this.rbtLiquidacion.Text = "Liquidación";
            this.rbtLiquidacion.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 13);
            this.label2.TabIndex = 352;
            this.label2.Text = "Tipo de recibo a realizar";
            // 
            // rbtRefrendo
            // 
            this.rbtRefrendo.AutoSize = true;
            this.rbtRefrendo.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbtRefrendo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtRefrendo.Location = new System.Drawing.Point(240, 104);
            this.rbtRefrendo.Name = "rbtRefrendo";
            this.rbtRefrendo.Size = new System.Drawing.Size(69, 17);
            this.rbtRefrendo.TabIndex = 7;
            this.rbtRefrendo.TabStop = true;
            this.rbtRefrendo.Text = "Refrendo";
            this.rbtRefrendo.UseVisualStyleBackColor = true;
            this.rbtRefrendo.CheckedChanged += new System.EventHandler(this.rbtRefrendo_CheckedChanged);
            // 
            // rbtPPI
            // 
            this.rbtPPI.AutoSize = true;
            this.rbtPPI.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbtPPI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtPPI.Location = new System.Drawing.Point(320, 104);
            this.rbtPPI.Name = "rbtPPI";
            this.rbtPPI.Size = new System.Drawing.Size(169, 17);
            this.rbtPPI.TabIndex = 8;
            this.rbtPPI.TabStop = true;
            this.rbtPPI.Text = "Pago Parcial de Intereses(PPI)";
            this.rbtPPI.UseVisualStyleBackColor = true;
            // 
            // lblMesesPPI
            // 
            this.lblMesesPPI.AutoSize = true;
            this.lblMesesPPI.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblMesesPPI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMesesPPI.Location = new System.Drawing.Point(528, 168);
            this.lblMesesPPI.Name = "lblMesesPPI";
            this.lblMesesPPI.Size = new System.Drawing.Size(187, 13);
            this.lblMesesPPI.TabIndex = 364;
            this.lblMesesPPI.Text = "Semanas de pago parcial de intereses";
            // 
            // lblDescripcionBoletaRefrendo
            // 
            this.lblDescripcionBoletaRefrendo.AutoSize = true;
            this.lblDescripcionBoletaRefrendo.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblDescripcionBoletaRefrendo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescripcionBoletaRefrendo.Location = new System.Drawing.Point(7, 184);
            this.lblDescripcionBoletaRefrendo.Name = "lblDescripcionBoletaRefrendo";
            this.lblDescripcionBoletaRefrendo.Size = new System.Drawing.Size(228, 13);
            this.lblDescripcionBoletaRefrendo.TabIndex = 366;
            this.lblDescripcionBoletaRefrendo.Text = "El recibo generó la(s) sig. boleta(s) de refrendo:";
            this.lblDescripcionBoletaRefrendo.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Cursor = System.Windows.Forms.Cursors.Default;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(272, 368);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 13);
            this.label11.TabIndex = 369;
            this.label11.Text = "TOTALES";
            // 
            // txtAcumCapital
            // 
            this.txtAcumCapital.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAcumCapital.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAcumCapital.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAcumCapital.Location = new System.Drawing.Point(344, 368);
            this.txtAcumCapital.MaxLength = 50;
            this.txtAcumCapital.Name = "txtAcumCapital";
            this.txtAcumCapital.ReadOnly = true;
            this.txtAcumCapital.Size = new System.Drawing.Size(72, 20);
            this.txtAcumCapital.TabIndex = 22;
            this.txtAcumCapital.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtAcumBonificaciones
            // 
            this.txtAcumBonificaciones.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAcumBonificaciones.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAcumBonificaciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAcumBonificaciones.Location = new System.Drawing.Point(488, 368);
            this.txtAcumBonificaciones.MaxLength = 50;
            this.txtAcumBonificaciones.Name = "txtAcumBonificaciones";
            this.txtAcumBonificaciones.ReadOnly = true;
            this.txtAcumBonificaciones.Size = new System.Drawing.Size(64, 20);
            this.txtAcumBonificaciones.TabIndex = 24;
            this.txtAcumBonificaciones.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtAcumIntereses
            // 
            this.txtAcumIntereses.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAcumIntereses.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAcumIntereses.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAcumIntereses.Location = new System.Drawing.Point(416, 368);
            this.txtAcumIntereses.MaxLength = 50;
            this.txtAcumIntereses.Name = "txtAcumIntereses";
            this.txtAcumIntereses.ReadOnly = true;
            this.txtAcumIntereses.Size = new System.Drawing.Size(72, 20);
            this.txtAcumIntereses.TabIndex = 23;
            this.txtAcumIntereses.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtAcumAbonoCap
            // 
            this.txtAcumAbonoCap.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAcumAbonoCap.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAcumAbonoCap.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAcumAbonoCap.Location = new System.Drawing.Point(664, 368);
            this.txtAcumAbonoCap.MaxLength = 50;
            this.txtAcumAbonoCap.Name = "txtAcumAbonoCap";
            this.txtAcumAbonoCap.ReadOnly = true;
            this.txtAcumAbonoCap.Size = new System.Drawing.Size(72, 20);
            this.txtAcumAbonoCap.TabIndex = 26;
            this.txtAcumAbonoCap.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtAcumSubTotal
            // 
            this.txtAcumSubTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAcumSubTotal.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAcumSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAcumSubTotal.Location = new System.Drawing.Point(552, 368);
            this.txtAcumSubTotal.MaxLength = 50;
            this.txtAcumSubTotal.Name = "txtAcumSubTotal";
            this.txtAcumSubTotal.ReadOnly = true;
            this.txtAcumSubTotal.Size = new System.Drawing.Size(72, 20);
            this.txtAcumSubTotal.TabIndex = 25;
            this.txtAcumSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtAcumPPI
            // 
            this.txtAcumPPI.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAcumPPI.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAcumPPI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAcumPPI.Location = new System.Drawing.Point(736, 368);
            this.txtAcumPPI.MaxLength = 50;
            this.txtAcumPPI.Name = "txtAcumPPI";
            this.txtAcumPPI.ReadOnly = true;
            this.txtAcumPPI.Size = new System.Drawing.Size(72, 20);
            this.txtAcumPPI.TabIndex = 27;
            this.txtAcumPPI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtAcumPagar
            // 
            this.txtAcumPagar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAcumPagar.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAcumPagar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAcumPagar.Location = new System.Drawing.Point(808, 368);
            this.txtAcumPagar.MaxLength = 50;
            this.txtAcumPagar.Name = "txtAcumPagar";
            this.txtAcumPagar.ReadOnly = true;
            this.txtAcumPagar.Size = new System.Drawing.Size(72, 20);
            this.txtAcumPagar.TabIndex = 28;
            this.txtAcumPagar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtBonificacion
            // 
            this.txtBonificacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBonificacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtBonificacion.Enabled = false;
            this.txtBonificacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBonificacion.Location = new System.Drawing.Point(103, 405);
            this.txtBonificacion.MaxLength = 5;
            this.txtBonificacion.Name = "txtBonificacion";
            this.txtBonificacion.Size = new System.Drawing.Size(62, 20);
            this.txtBonificacion.TabIndex = 29;
            this.txtBonificacion.TextChanged += new System.EventHandler(this.txtBonificacion_TextChanged);
            this.txtBonificacion.Leave += new System.EventHandler(this.txtBonificacion_Leave);
            // 
            // lblBoletaBonificacion
            // 
            this.lblBoletaBonificacion.AutoSize = true;
            this.lblBoletaBonificacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblBoletaBonificacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBoletaBonificacion.Location = new System.Drawing.Point(176, 408);
            this.lblBoletaBonificacion.Name = "lblBoletaBonificacion";
            this.lblBoletaBonificacion.Size = new System.Drawing.Size(65, 13);
            this.lblBoletaBonificacion.TabIndex = 380;
            this.lblBoletaBonificacion.Text = "a la boleta:  ";
            // 
            // lblBoletaAbonoCapital
            // 
            this.lblBoletaAbonoCapital.AutoSize = true;
            this.lblBoletaAbonoCapital.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblBoletaAbonoCapital.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBoletaAbonoCapital.Location = new System.Drawing.Point(176, 435);
            this.lblBoletaAbonoCapital.Name = "lblBoletaAbonoCapital";
            this.lblBoletaAbonoCapital.Size = new System.Drawing.Size(65, 13);
            this.lblBoletaAbonoCapital.TabIndex = 383;
            this.lblBoletaAbonoCapital.Text = "a la boleta:  ";
            // 
            // txtAbonoCapital
            // 
            this.txtAbonoCapital.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAbonoCapital.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAbonoCapital.Enabled = false;
            this.txtAbonoCapital.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAbonoCapital.Location = new System.Drawing.Point(104, 432);
            this.txtAbonoCapital.MaxLength = 5;
            this.txtAbonoCapital.Name = "txtAbonoCapital";
            this.txtAbonoCapital.Size = new System.Drawing.Size(62, 20);
            this.txtAbonoCapital.TabIndex = 30;
            this.txtAbonoCapital.Leave += new System.EventHandler(this.txtAbonoCapital_Leave);
            // 
            // txtSemanasPPI
            // 
            this.txtSemanasPPI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSemanasPPI.Location = new System.Drawing.Point(720, 168);
            this.txtSemanasPPI.Name = "txtSemanasPPI";
            this.txtSemanasPPI.Size = new System.Drawing.Size(40, 20);
            this.txtSemanasPPI.TabIndex = 19;
            this.txtSemanasPPI.ValueChanged += new System.EventHandler(this.txtSemanasPPI_ValueChanged);
            this.txtSemanasPPI.Leave += new System.EventHandler(this.txtSemanasPPI_Leave);
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(0, 520);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(920, 2);
            this.label1.TabIndex = 385;
            // 
            // txtCodigoCliente
            // 
            this.txtCodigoCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoCliente.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoCliente.Location = new System.Drawing.Point(56, 136);
            this.txtCodigoCliente.MaxLength = 50;
            this.txtCodigoCliente.Name = "txtCodigoCliente";
            this.txtCodigoCliente.Size = new System.Drawing.Size(48, 20);
            this.txtCodigoCliente.TabIndex = 11;
            this.txtCodigoCliente.Layout += new System.Windows.Forms.LayoutEventHandler(this.txtCodigoCliente_Layout);
            this.txtCodigoCliente.Leave += new System.EventHandler(this.txtCodigoCliente_Leave);
            // 
            // btnVerificarHuellaCliente
            // 
            this.btnVerificarHuellaCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnVerificarHuellaCliente.Location = new System.Drawing.Point(312, 136);
            this.btnVerificarHuellaCliente.Name = "btnVerificarHuellaCliente";
            this.btnVerificarHuellaCliente.Size = new System.Drawing.Size(64, 21);
            this.btnVerificarHuellaCliente.TabIndex = 14;
            this.btnVerificarHuellaCliente.Text = "Verificar ";
            this.btnVerificarHuellaCliente.UseVisualStyleBackColor = true;
            this.btnVerificarHuellaCliente.Click += new System.EventHandler(this.btnVerificarHuellaCliente_Click);
            // 
            // txtClienteId
            // 
            this.txtClienteId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtClienteId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtClienteId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtClienteId.Location = new System.Drawing.Point(128, 136);
            this.txtClienteId.MaxLength = 3;
            this.txtClienteId.Name = "txtClienteId";
            this.txtClienteId.Size = new System.Drawing.Size(44, 20);
            this.txtClienteId.TabIndex = 571;
            this.txtClienteId.Visible = false;
            // 
            // btnBusquedaCliente
            // 
            this.btnBusquedaCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnBusquedaCliente.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusquedaCliente.Location = new System.Drawing.Point(272, 136);
            this.btnBusquedaCliente.Name = "btnBusquedaCliente";
            this.btnBusquedaCliente.Size = new System.Drawing.Size(32, 21);
            this.btnBusquedaCliente.TabIndex = 13;
            this.btnBusquedaCliente.UseVisualStyleBackColor = true;
            this.btnBusquedaCliente.Click += new System.EventHandler(this.btnBusquedaCliente_Click);
            // 
            // txtNombreCliente
            // 
            this.txtNombreCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombreCliente.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNombreCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtNombreCliente.Location = new System.Drawing.Point(112, 136);
            this.txtNombreCliente.MaxLength = 50;
            this.txtNombreCliente.Name = "txtNombreCliente";
            this.txtNombreCliente.ReadOnly = true;
            this.txtNombreCliente.Size = new System.Drawing.Size(152, 20);
            this.txtNombreCliente.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label6.Location = new System.Drawing.Point(528, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 575;
            this.label6.Text = "Empleado";
            // 
            // txtNombreEmpleado
            // 
            this.txtNombreEmpleado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombreEmpleado.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNombreEmpleado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtNombreEmpleado.Location = new System.Drawing.Point(616, 72);
            this.txtNombreEmpleado.MaxLength = 50;
            this.txtNombreEmpleado.Name = "txtNombreEmpleado";
            this.txtNombreEmpleado.ReadOnly = true;
            this.txtNombreEmpleado.Size = new System.Drawing.Size(264, 20);
            this.txtNombreEmpleado.TabIndex = 5;
            // 
            // txtDescripcionCaja
            // 
            this.txtDescripcionCaja.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionCaja.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionCaja.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtDescripcionCaja.Location = new System.Drawing.Point(664, 104);
            this.txtDescripcionCaja.MaxLength = 50;
            this.txtDescripcionCaja.Name = "txtDescripcionCaja";
            this.txtDescripcionCaja.ReadOnly = true;
            this.txtDescripcionCaja.Size = new System.Drawing.Size(216, 20);
            this.txtDescripcionCaja.TabIndex = 10;
            // 
            // txtCodigoCaja
            // 
            this.txtCodigoCaja.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoCaja.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoCaja.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtCodigoCaja.Location = new System.Drawing.Point(616, 104);
            this.txtCodigoCaja.MaxLength = 5;
            this.txtCodigoCaja.Name = "txtCodigoCaja";
            this.txtCodigoCaja.ReadOnly = true;
            this.txtCodigoCaja.Size = new System.Drawing.Size(44, 20);
            this.txtCodigoCaja.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Cursor = System.Windows.Forms.Cursors.Default;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label8.Location = new System.Drawing.Point(528, 104);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 13);
            this.label8.TabIndex = 576;
            this.label8.Text = "Caja";
            // 
            // chkDeselecionarBoleta
            // 
            this.chkDeselecionarBoleta.AutoSize = true;
            this.chkDeselecionarBoleta.Location = new System.Drawing.Point(8, 368);
            this.chkDeselecionarBoleta.Name = "chkDeselecionarBoleta";
            this.chkDeselecionarBoleta.Size = new System.Drawing.Size(66, 17);
            this.chkDeselecionarBoleta.TabIndex = 21;
            this.chkDeselecionarBoleta.Text = "Ninguna";
            this.chkDeselecionarBoleta.UseVisualStyleBackColor = true;
            this.chkDeselecionarBoleta.CheckedChanged += new System.EventHandler(this.chkDeselecionarBoleta_CheckedChanged);
            // 
            // tsAcciones
            // 
            this.tsAcciones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbNuevo,
            this.tsbGuardar,
            this.tsbEliminar,
            this.tsbBuscar,
            this.btnUndo,
            this.toolStripSeparator2,
            this.btnPrimeroPag,
            this.btnAtras,
            this.btnAdelante,
            this.btnUlitmoPag,
            this.toolStripSeparator1,
            this.tsbImprimir,
            this.btnCierra});
            this.tsAcciones.Location = new System.Drawing.Point(0, 0);
            this.tsAcciones.Name = "tsAcciones";
            this.tsAcciones.Size = new System.Drawing.Size(903, 25);
            this.tsAcciones.TabIndex = 581;
            this.tsAcciones.Text = "toolStrip1";
            this.tsAcciones.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tsAcciones_ItemClicked);
            // 
            // tsbNuevo
            // 
            this.tsbNuevo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbNuevo.Image = global::EmpenosEvora.Properties.Resources.AddFile_16x;
            this.tsbNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbNuevo.Name = "tsbNuevo";
            this.tsbNuevo.Size = new System.Drawing.Size(23, 22);
            this.tsbNuevo.Text = "Nuevo";
            this.tsbNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical90;
            this.tsbNuevo.Click += new System.EventHandler(this.tsbNuevo_Click);
            // 
            // tsbGuardar
            // 
            this.tsbGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbGuardar.Image = global::EmpenosEvora.Properties.Resources.Save_16x;
            this.tsbGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGuardar.Name = "tsbGuardar";
            this.tsbGuardar.Size = new System.Drawing.Size(23, 22);
            this.tsbGuardar.Tag = "";
            this.tsbGuardar.Text = "Guardar";
            this.tsbGuardar.Click += new System.EventHandler(this.tsbGuardar_Click);
            // 
            // tsbEliminar
            // 
            this.tsbEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbEliminar.Image = global::EmpenosEvora.Properties.Resources.Cancel_16x;
            this.tsbEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEliminar.Name = "tsbEliminar";
            this.tsbEliminar.Size = new System.Drawing.Size(23, 22);
            this.tsbEliminar.Text = "Desactivar";
            this.tsbEliminar.ToolTipText = "Desactivar";
            this.tsbEliminar.Click += new System.EventHandler(this.tsbEliminar_Click);
            // 
            // tsbBuscar
            // 
            this.tsbBuscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBuscar.Image = global::EmpenosEvora.Properties.Resources.VBSearch_16x;
            this.tsbBuscar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBuscar.Name = "tsbBuscar";
            this.tsbBuscar.Size = new System.Drawing.Size(23, 22);
            this.tsbBuscar.Text = "Buscar";
            this.tsbBuscar.Click += new System.EventHandler(this.tsbBuscar_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUndo.Image = global::EmpenosEvora.Properties.Resources.Undo_16x;
            this.btnUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(23, 22);
            this.btnUndo.Text = "Deshacer";
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrimeroPag
            // 
            this.btnPrimeroPag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrimeroPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeftGroup_16xSM;
            this.btnPrimeroPag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrimeroPag.Name = "btnPrimeroPag";
            this.btnPrimeroPag.Size = new System.Drawing.Size(23, 22);
            this.btnPrimeroPag.Text = "Primero";
            this.btnPrimeroPag.Click += new System.EventHandler(this.btnPrimeroPag_Click);
            // 
            // btnAtras
            // 
            this.btnAtras.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAtras.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeft_16x;
            this.btnAtras.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(23, 22);
            this.btnAtras.Text = "Atras";
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // btnAdelante
            // 
            this.btnAdelante.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdelante.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronRight_16x;
            this.btnAdelante.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdelante.Name = "btnAdelante";
            this.btnAdelante.Size = new System.Drawing.Size(23, 22);
            this.btnAdelante.Text = "Siguente";
            this.btnAdelante.Click += new System.EventHandler(this.btnAdelante_Click);
            // 
            // btnUlitmoPag
            // 
            this.btnUlitmoPag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUlitmoPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronRightGroup_16x;
            this.btnUlitmoPag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUlitmoPag.Name = "btnUlitmoPag";
            this.btnUlitmoPag.Size = new System.Drawing.Size(23, 22);
            this.btnUlitmoPag.Text = "Ultimo";
            this.btnUlitmoPag.Click += new System.EventHandler(this.btnUlitmoPag_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbImprimir
            // 
            this.tsbImprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbImprimir.Image = global::EmpenosEvora.Properties.Resources.Print_16x;
            this.tsbImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbImprimir.Name = "tsbImprimir";
            this.tsbImprimir.Size = new System.Drawing.Size(23, 22);
            this.tsbImprimir.Text = "Imprimir";
            this.tsbImprimir.ToolTipText = "Imprimir";
            // 
            // btnCierra
            // 
            this.btnCierra.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnCierra.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCierra.Name = "btnCierra";
            this.btnCierra.Size = new System.Drawing.Size(23, 22);
            this.btnCierra.Text = "Salir";
            this.btnCierra.Click += new System.EventHandler(this.btnCierra_Click);
            // 
            // btnBusqueda
            // 
            this.btnBusqueda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnBusqueda.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusqueda.Location = new System.Drawing.Point(160, 72);
            this.btnBusqueda.Name = "btnBusqueda";
            this.btnBusqueda.Size = new System.Drawing.Size(32, 21);
            this.btnBusqueda.TabIndex = 2;
            this.btnBusqueda.UseVisualStyleBackColor = true;
            this.btnBusqueda.Click += new System.EventHandler(this.btnBusqueda_Click);
            // 
            // btnFooterCierra
            // 
            this.btnFooterCierra.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnFooterCierra.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterCierra.Location = new System.Drawing.Point(808, 536);
            this.btnFooterCierra.Name = "btnFooterCierra";
            this.btnFooterCierra.Size = new System.Drawing.Size(75, 40);
            this.btnFooterCierra.TabIndex = 36;
            this.btnFooterCierra.Text = "Salir";
            this.btnFooterCierra.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterCierra.UseVisualStyleBackColor = true;
            this.btnFooterCierra.Click += new System.EventHandler(this.btnFooterCierra_Click_1);
            // 
            // btnFooterGuardar
            // 
            this.btnFooterGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterGuardar.Image = global::EmpenosEvora.Properties.Resources.Save_16x;
            this.btnFooterGuardar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterGuardar.Location = new System.Drawing.Point(720, 536);
            this.btnFooterGuardar.Name = "btnFooterGuardar";
            this.btnFooterGuardar.Size = new System.Drawing.Size(75, 40);
            this.btnFooterGuardar.TabIndex = 35;
            this.btnFooterGuardar.Text = "Guardar";
            this.btnFooterGuardar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterGuardar.UseVisualStyleBackColor = true;
            this.btnFooterGuardar.Click += new System.EventHandler(this.btnFooterGuardar_Click);
            // 
            // txtDescripcionFormaPago
            // 
            this.txtDescripcionFormaPago.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionFormaPago.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionFormaPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtDescripcionFormaPago.Location = new System.Drawing.Point(664, 136);
            this.txtDescripcionFormaPago.MaxLength = 50;
            this.txtDescripcionFormaPago.Name = "txtDescripcionFormaPago";
            this.txtDescripcionFormaPago.ReadOnly = true;
            this.txtDescripcionFormaPago.Size = new System.Drawing.Size(176, 20);
            this.txtDescripcionFormaPago.TabIndex = 17;
            // 
            // txtCodigoFormaPago
            // 
            this.txtCodigoFormaPago.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoFormaPago.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoFormaPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtCodigoFormaPago.Location = new System.Drawing.Point(616, 136);
            this.txtCodigoFormaPago.MaxLength = 50;
            this.txtCodigoFormaPago.Name = "txtCodigoFormaPago";
            this.txtCodigoFormaPago.Size = new System.Drawing.Size(44, 20);
            this.txtCodigoFormaPago.TabIndex = 16;
            this.txtCodigoFormaPago.Leave += new System.EventHandler(this.txtFormaPago_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Cursor = System.Windows.Forms.Cursors.Default;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label7.Location = new System.Drawing.Point(528, 136);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 587;
            this.label7.Text = "Forma de pago";
            // 
            // btnBusquedaFormaPago
            // 
            this.btnBusquedaFormaPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnBusquedaFormaPago.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusquedaFormaPago.Location = new System.Drawing.Point(848, 136);
            this.btnBusquedaFormaPago.Name = "btnBusquedaFormaPago";
            this.btnBusquedaFormaPago.Size = new System.Drawing.Size(32, 21);
            this.btnBusquedaFormaPago.TabIndex = 18;
            this.btnBusquedaFormaPago.UseVisualStyleBackColor = true;
            this.btnBusquedaFormaPago.Click += new System.EventHandler(this.btnBusquedaFormaPago_Click);
            // 
            // txtFormaPaogId
            // 
            this.txtFormaPaogId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFormaPaogId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtFormaPaogId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtFormaPaogId.Location = new System.Drawing.Point(696, 136);
            this.txtFormaPaogId.MaxLength = 3;
            this.txtFormaPaogId.Name = "txtFormaPaogId";
            this.txtFormaPaogId.Size = new System.Drawing.Size(44, 20);
            this.txtFormaPaogId.TabIndex = 589;
            this.txtFormaPaogId.Visible = false;
            // 
            // dgvDetalles
            // 
            this.dgvDetalles.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDetalles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetalles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.diaMinimoPromocion,
            this.porcentajeTotal,
            this.diaPromocion,
            this.diaProcentaje,
            this.cobrosTipoNombre,
            this.diasMinimo,
            this.prendaTipoId,
            this.cobroTipoId,
            this.porcentajeCobroTipo,
            this.importeCobroTipo,
            this.importeCobroTipoBonificacion,
            this.fechaAutorizacion,
            this.usuarioAutorizacion,
            this.motivoAutorizacion,
            this.boleta_empeno_id,
            this.Boleta,
            this.Fecha,
            this.Vence,
            this.Sem,
            this.Día,
            this.Capital,
            this.Intereses,
            this.Bonificar,
            this.Subtotal,
            this.Sel,
            this.AbonoCap,
            this.PPIAplica,
            this.Pagar});
            this.dgvDetalles.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgvDetalles.Location = new System.Drawing.Point(8, 203);
            this.dgvDetalles.Name = "dgvDetalles";
            this.dgvDetalles.Size = new System.Drawing.Size(870, 150);
            this.dgvDetalles.TabIndex = 20;
            this.dgvDetalles.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvDetalles_CellBeginEdit);
            this.dgvDetalles.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDetalles_CellClick);
            this.dgvDetalles.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDetalles_CellContentClick);
            this.dgvDetalles.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDetalles_CellValidated);
            this.dgvDetalles.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvDetalles_CellValidating);
            this.dgvDetalles.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDetalles_CellValueChanged);
            // 
            // diaMinimoPromocion
            // 
            this.diaMinimoPromocion.HeaderText = "diaMinimoPromocion";
            this.diaMinimoPromocion.Name = "diaMinimoPromocion";
            this.diaMinimoPromocion.Visible = false;
            // 
            // porcentajeTotal
            // 
            this.porcentajeTotal.HeaderText = "porcentajeTotal";
            this.porcentajeTotal.Name = "porcentajeTotal";
            this.porcentajeTotal.Visible = false;
            // 
            // diaPromocion
            // 
            this.diaPromocion.HeaderText = "diaPromocion";
            this.diaPromocion.Name = "diaPromocion";
            this.diaPromocion.Visible = false;
            // 
            // diaProcentaje
            // 
            this.diaProcentaje.HeaderText = "diaProcentaje";
            this.diaProcentaje.Name = "diaProcentaje";
            this.diaProcentaje.Visible = false;
            // 
            // cobrosTipoNombre
            // 
            this.cobrosTipoNombre.HeaderText = "cobrosTipoNombre";
            this.cobrosTipoNombre.Name = "cobrosTipoNombre";
            this.cobrosTipoNombre.Visible = false;
            // 
            // diasMinimo
            // 
            this.diasMinimo.HeaderText = "diasMinimo";
            this.diasMinimo.Name = "diasMinimo";
            this.diasMinimo.Visible = false;
            // 
            // prendaTipoId
            // 
            this.prendaTipoId.HeaderText = "prendaTipoId";
            this.prendaTipoId.Name = "prendaTipoId";
            this.prendaTipoId.Visible = false;
            // 
            // cobroTipoId
            // 
            this.cobroTipoId.HeaderText = "cobroTipoId";
            this.cobroTipoId.Name = "cobroTipoId";
            this.cobroTipoId.Visible = false;
            // 
            // porcentajeCobroTipo
            // 
            this.porcentajeCobroTipo.HeaderText = "porcentajeCobroTipo";
            this.porcentajeCobroTipo.Name = "porcentajeCobroTipo";
            this.porcentajeCobroTipo.Visible = false;
            // 
            // importeCobroTipo
            // 
            this.importeCobroTipo.HeaderText = "importeCobroTipo";
            this.importeCobroTipo.Name = "importeCobroTipo";
            this.importeCobroTipo.Visible = false;
            // 
            // importeCobroTipoBonificacion
            // 
            this.importeCobroTipoBonificacion.HeaderText = "importeCobroTipoBonificacion";
            this.importeCobroTipoBonificacion.Name = "importeCobroTipoBonificacion";
            this.importeCobroTipoBonificacion.Visible = false;
            // 
            // fechaAutorizacion
            // 
            this.fechaAutorizacion.HeaderText = "fechaAutorizacion";
            this.fechaAutorizacion.Name = "fechaAutorizacion";
            this.fechaAutorizacion.Visible = false;
            // 
            // usuarioAutorizacion
            // 
            this.usuarioAutorizacion.HeaderText = "usuarioAutorizacion";
            this.usuarioAutorizacion.Name = "usuarioAutorizacion";
            this.usuarioAutorizacion.Visible = false;
            // 
            // motivoAutorizacion
            // 
            this.motivoAutorizacion.HeaderText = "motivoAutorizacion";
            this.motivoAutorizacion.Name = "motivoAutorizacion";
            this.motivoAutorizacion.Visible = false;
            // 
            // boleta_empeno_id
            // 
            this.boleta_empeno_id.HeaderText = "boleta_empeno_id";
            this.boleta_empeno_id.Name = "boleta_empeno_id";
            this.boleta_empeno_id.Visible = false;
            // 
            // Boleta
            // 
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Boleta.DefaultCellStyle = dataGridViewCellStyle1;
            this.Boleta.FillWeight = 100.4898F;
            this.Boleta.HeaderText = "Boleta";
            this.Boleta.Name = "Boleta";
            this.Boleta.ReadOnly = true;
            // 
            // Fecha
            // 
            this.Fecha.FillWeight = 100.4898F;
            this.Fecha.HeaderText = "Fecha";
            this.Fecha.Name = "Fecha";
            this.Fecha.ReadOnly = true;
            // 
            // Vence
            // 
            this.Vence.FillWeight = 100.4898F;
            this.Vence.HeaderText = "Vence";
            this.Vence.Name = "Vence";
            this.Vence.ReadOnly = true;
            // 
            // Sem
            // 
            this.Sem.FillWeight = 94.41625F;
            this.Sem.HeaderText = "Sem";
            this.Sem.Name = "Sem";
            this.Sem.ReadOnly = true;
            // 
            // Día
            // 
            this.Día.FillWeight = 100.4898F;
            this.Día.HeaderText = "Día";
            this.Día.Name = "Día";
            this.Día.ReadOnly = true;
            // 
            // Capital
            // 
            this.Capital.FillWeight = 100.4898F;
            this.Capital.HeaderText = "Capital";
            this.Capital.Name = "Capital";
            this.Capital.ReadOnly = true;
            // 
            // Intereses
            // 
            this.Intereses.FillWeight = 100.4898F;
            this.Intereses.HeaderText = "Intereses";
            this.Intereses.Name = "Intereses";
            this.Intereses.ReadOnly = true;
            // 
            // Bonificar
            // 
            this.Bonificar.FillWeight = 100.4898F;
            this.Bonificar.HeaderText = "Bonificar";
            this.Bonificar.Name = "Bonificar";
            this.Bonificar.ReadOnly = true;
            // 
            // Subtotal
            // 
            this.Subtotal.FillWeight = 100.4898F;
            this.Subtotal.HeaderText = "Subtotal";
            this.Subtotal.Name = "Subtotal";
            this.Subtotal.ReadOnly = true;
            // 
            // Sel
            // 
            this.Sel.DataPropertyName = "chkSelecionar";
            this.Sel.FillWeight = 40.19592F;
            this.Sel.HeaderText = "Sel";
            this.Sel.Name = "Sel";
            this.Sel.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Sel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // AbonoCap
            // 
            this.AbonoCap.FillWeight = 100.4898F;
            this.AbonoCap.HeaderText = "Abono Cap";
            this.AbonoCap.Name = "AbonoCap";
            this.AbonoCap.ReadOnly = true;
            // 
            // PPIAplica
            // 
            this.PPIAplica.FillWeight = 100.4898F;
            this.PPIAplica.HeaderText = "PPI x Aplicar";
            this.PPIAplica.Name = "PPIAplica";
            this.PPIAplica.ReadOnly = true;
            // 
            // Pagar
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pagar.DefaultCellStyle = dataGridViewCellStyle2;
            this.Pagar.FillWeight = 100.4898F;
            this.Pagar.HeaderText = "A Pagar";
            this.Pagar.Name = "Pagar";
            this.Pagar.ReadOnly = true;
            // 
            // lkbBonifiacion
            // 
            this.lkbBonifiacion.AutoSize = true;
            this.lkbBonifiacion.Location = new System.Drawing.Point(8, 408);
            this.lkbBonifiacion.Name = "lkbBonifiacion";
            this.lkbBonifiacion.Size = new System.Drawing.Size(65, 13);
            this.lkbBonifiacion.TabIndex = 590;
            this.lkbBonifiacion.TabStop = true;
            this.lkbBonifiacion.Text = "Bonificación";
            this.lkbBonifiacion.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lkbBonifiacion_LinkClicked);
            // 
            // lkbAbonoCapital
            // 
            this.lkbAbonoCapital.AutoSize = true;
            this.lkbAbonoCapital.Location = new System.Drawing.Point(8, 432);
            this.lkbAbonoCapital.Name = "lkbAbonoCapital";
            this.lkbAbonoCapital.Size = new System.Drawing.Size(81, 13);
            this.lkbAbonoCapital.TabIndex = 591;
            this.lkbAbonoCapital.TabStop = true;
            this.lkbAbonoCapital.Text = "Abono a capital";
            this.lkbAbonoCapital.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lkbAbonoCapital_LinkClicked);
            // 
            // lblListaBoletasRefrendadas
            // 
            this.lblListaBoletasRefrendadas.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblListaBoletasRefrendadas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblListaBoletasRefrendadas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblListaBoletasRefrendadas.Location = new System.Drawing.Point(232, 184);
            this.lblListaBoletasRefrendadas.Name = "lblListaBoletasRefrendadas";
            this.lblListaBoletasRefrendadas.Size = new System.Drawing.Size(640, 13);
            this.lblListaBoletasRefrendadas.TabIndex = 592;
            this.lblListaBoletasRefrendadas.Text = "Boletaras Generadas";
            this.lblListaBoletasRefrendadas.Visible = false;
            // 
            // btnFotografia
            // 
            this.btnFotografia.Location = new System.Drawing.Point(384, 136);
            this.btnFotografia.Name = "btnFotografia";
            this.btnFotografia.Size = new System.Drawing.Size(64, 21);
            this.btnFotografia.TabIndex = 15;
            this.btnFotografia.Text = "Fotografía";
            this.btnFotografia.UseVisualStyleBackColor = true;
            this.btnFotografia.Click += new System.EventHandler(this.btnFotografia_Click);
            // 
            // lblBoletaBonifiacionFolio
            // 
            this.lblBoletaBonifiacionFolio.AutoSize = true;
            this.lblBoletaBonifiacionFolio.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblBoletaBonifiacionFolio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBoletaBonifiacionFolio.Location = new System.Drawing.Point(240, 408);
            this.lblBoletaBonifiacionFolio.Name = "lblBoletaBonifiacionFolio";
            this.lblBoletaBonifiacionFolio.Size = new System.Drawing.Size(29, 13);
            this.lblBoletaBonifiacionFolio.TabIndex = 594;
            this.lblBoletaBonifiacionFolio.Text = "Folio";
            this.lblBoletaBonifiacionFolio.Visible = false;
            // 
            // lblBoletaAbonoCapFolio
            // 
            this.lblBoletaAbonoCapFolio.AutoSize = true;
            this.lblBoletaAbonoCapFolio.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblBoletaAbonoCapFolio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBoletaAbonoCapFolio.Location = new System.Drawing.Point(240, 435);
            this.lblBoletaAbonoCapFolio.Name = "lblBoletaAbonoCapFolio";
            this.lblBoletaAbonoCapFolio.Size = new System.Drawing.Size(29, 13);
            this.lblBoletaAbonoCapFolio.TabIndex = 595;
            this.lblBoletaAbonoCapFolio.Text = "Folio";
            this.lblBoletaAbonoCapFolio.Visible = false;
            // 
            // btnVerCancelacion
            // 
            this.btnVerCancelacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnVerCancelacion.Location = new System.Drawing.Point(344, 408);
            this.btnVerCancelacion.Name = "btnVerCancelacion";
            this.btnVerCancelacion.Size = new System.Drawing.Size(144, 21);
            this.btnVerCancelacion.TabIndex = 31;
            this.btnVerCancelacion.Text = "Ver motivo de cancelación";
            this.btnVerCancelacion.UseVisualStyleBackColor = true;
            this.btnVerCancelacion.Visible = false;
            this.btnVerCancelacion.Click += new System.EventHandler(this.btnVerCancelacion_Click);
            // 
            // lblEstatus
            // 
            this.lblEstatus.AutoSize = true;
            this.lblEstatus.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblEstatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstatus.ForeColor = System.Drawing.Color.Red;
            this.lblEstatus.Location = new System.Drawing.Point(55, 40);
            this.lblEstatus.Name = "lblEstatus";
            this.lblEstatus.Size = new System.Drawing.Size(50, 13);
            this.lblEstatus.TabIndex = 599;
            this.lblEstatus.Text = "NUEVO";
            this.lblEstatus.Visible = false;
            // 
            // lblDescEstatus
            // 
            this.lblDescEstatus.AutoSize = true;
            this.lblDescEstatus.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblDescEstatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblDescEstatus.ForeColor = System.Drawing.Color.Black;
            this.lblDescEstatus.Location = new System.Drawing.Point(8, 40);
            this.lblDescEstatus.Name = "lblDescEstatus";
            this.lblDescEstatus.Size = new System.Drawing.Size(42, 13);
            this.lblDescEstatus.TabIndex = 598;
            this.lblDescEstatus.Text = "Estatus";
            this.lblDescEstatus.Visible = false;
            // 
            // txtCobroTotal
            // 
            this.txtCobroTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCobroTotal.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCobroTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCobroTotal.Location = new System.Drawing.Point(744, 416);
            this.txtCobroTotal.MaxLength = 50;
            this.txtCobroTotal.Name = "txtCobroTotal";
            this.txtCobroTotal.ReadOnly = true;
            this.txtCobroTotal.Size = new System.Drawing.Size(136, 29);
            this.txtCobroTotal.TabIndex = 32;
            this.txtCobroTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Cursor = System.Windows.Forms.Cursors.Default;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.DarkGreen;
            this.label9.Location = new System.Drawing.Point(624, 416);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 24);
            this.label9.TabIndex = 601;
            this.label9.Text = "TOTAL";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Cursor = System.Windows.Forms.Cursors.Default;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.DarkGreen;
            this.label10.Location = new System.Drawing.Point(624, 448);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 24);
            this.label10.TabIndex = 602;
            this.label10.Text = "RECIBIDO";
            // 
            // txtEfectivo
            // 
            this.txtEfectivo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEfectivo.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtEfectivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEfectivo.Location = new System.Drawing.Point(744, 448);
            this.txtEfectivo.MaxLength = 50;
            this.txtEfectivo.Name = "txtEfectivo";
            this.txtEfectivo.Size = new System.Drawing.Size(136, 29);
            this.txtEfectivo.TabIndex = 33;
            this.txtEfectivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtEfectivo.Enter += new System.EventHandler(this.txtEfectivo_Enter);
            this.txtEfectivo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEfectivo_KeyPress);
            this.txtEfectivo.Leave += new System.EventHandler(this.txtEfectivo_Leave);
            // 
            // txtCambio
            // 
            this.txtCambio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCambio.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCambio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCambio.Location = new System.Drawing.Point(744, 480);
            this.txtCambio.MaxLength = 50;
            this.txtCambio.Name = "txtCambio";
            this.txtCambio.ReadOnly = true;
            this.txtCambio.Size = new System.Drawing.Size(136, 29);
            this.txtCambio.TabIndex = 34;
            this.txtCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCambio.TextChanged += new System.EventHandler(this.txtCambio_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.DarkGreen;
            this.label12.Location = new System.Drawing.Point(624, 480);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 24);
            this.label12.TabIndex = 604;
            this.label12.Text = "CAMBIO";
            // 
            // lkbCliente
            // 
            this.lkbCliente.AutoSize = true;
            this.lkbCliente.Location = new System.Drawing.Point(8, 136);
            this.lkbCliente.Name = "lkbCliente";
            this.lkbCliente.Size = new System.Drawing.Size(39, 13);
            this.lkbCliente.TabIndex = 606;
            this.lkbCliente.TabStop = true;
            this.lkbCliente.Text = "Cliente";
            this.lkbCliente.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lkbCliente_LinkClicked);
            // 
            // frmRecibosLiquidacionRegrendos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(903, 578);
            this.Controls.Add(this.lkbCliente);
            this.Controls.Add(this.txtCambio);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtEfectivo);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtCobroTotal);
            this.Controls.Add(this.lblEstatus);
            this.Controls.Add(this.lblDescEstatus);
            this.Controls.Add(this.btnVerCancelacion);
            this.Controls.Add(this.lblBoletaAbonoCapFolio);
            this.Controls.Add(this.lblBoletaBonifiacionFolio);
            this.Controls.Add(this.btnFotografia);
            this.Controls.Add(this.lblListaBoletasRefrendadas);
            this.Controls.Add(this.lkbAbonoCapital);
            this.Controls.Add(this.lkbBonifiacion);
            this.Controls.Add(this.txtFormaPaogId);
            this.Controls.Add(this.btnBusquedaFormaPago);
            this.Controls.Add(this.txtDescripcionFormaPago);
            this.Controls.Add(this.txtCodigoFormaPago);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnFooterCierra);
            this.Controls.Add(this.btnFooterGuardar);
            this.Controls.Add(this.btnBusqueda);
            this.Controls.Add(this.tsAcciones);
            this.Controls.Add(this.chkDeselecionarBoleta);
            this.Controls.Add(this.txtDescripcionCaja);
            this.Controls.Add(this.txtCodigoCaja);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtNombreEmpleado);
            this.Controls.Add(this.txtCodigoCliente);
            this.Controls.Add(this.btnVerificarHuellaCliente);
            this.Controls.Add(this.txtClienteId);
            this.Controls.Add(this.btnBusquedaCliente);
            this.Controls.Add(this.txtNombreCliente);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSemanasPPI);
            this.Controls.Add(this.lblBoletaAbonoCapital);
            this.Controls.Add(this.txtAbonoCapital);
            this.Controls.Add(this.lblBoletaBonificacion);
            this.Controls.Add(this.txtBonificacion);
            this.Controls.Add(this.txtAcumPagar);
            this.Controls.Add(this.txtAcumPPI);
            this.Controls.Add(this.txtAcumAbonoCap);
            this.Controls.Add(this.txtAcumSubTotal);
            this.Controls.Add(this.txtAcumBonificaciones);
            this.Controls.Add(this.txtAcumIntereses);
            this.Controls.Add(this.txtAcumCapital);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lblDescripcionBoletaRefrendo);
            this.Controls.Add(this.dgvDetalles);
            this.Controls.Add(this.lblMesesPPI);
            this.Controls.Add(this.rbtPPI);
            this.Controls.Add(this.rbtRefrendo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rbtLiquidacion);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtHora);
            this.Controls.Add(this.dtpFecha);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtFolio);
            this.Controls.Add(this.lblCodigo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRecibosLiquidacionRegrendos";
            this.Text = "Recibos de Liquidación y Refrendos";
            this.Load += new System.EventHandler(this.frmRecibosLiquidacionRegrendos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtSemanasPPI)).EndInit();
            this.tsAcciones.ResumeLayout(false);
            this.tsAcciones.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalles)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtHora;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFolio;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.RadioButton rbtLiquidacion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbtRefrendo;
        private System.Windows.Forms.RadioButton rbtPPI;
        private System.Windows.Forms.Label lblMesesPPI;
        private System.Windows.Forms.Label lblDescripcionBoletaRefrendo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtAcumCapital;
        private System.Windows.Forms.TextBox txtAcumBonificaciones;
        private System.Windows.Forms.TextBox txtAcumIntereses;
        private System.Windows.Forms.TextBox txtAcumAbonoCap;
        private System.Windows.Forms.TextBox txtAcumSubTotal;
        private System.Windows.Forms.TextBox txtAcumPPI;
        private System.Windows.Forms.TextBox txtAcumPagar;
        private System.Windows.Forms.TextBox txtBonificacion;
        private System.Windows.Forms.Label lblBoletaBonificacion;
        private System.Windows.Forms.Label lblBoletaAbonoCapital;
        private System.Windows.Forms.TextBox txtAbonoCapital;
        private System.Windows.Forms.NumericUpDown txtSemanasPPI;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCodigoCliente;
        private System.Windows.Forms.Button btnVerificarHuellaCliente;
        private System.Windows.Forms.TextBox txtClienteId;
        private System.Windows.Forms.Button btnBusquedaCliente;
        private System.Windows.Forms.TextBox txtNombreCliente;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNombreEmpleado;
        private System.Windows.Forms.TextBox txtDescripcionCaja;
        private System.Windows.Forms.TextBox txtCodigoCaja;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chkDeselecionarBoleta;
        private System.Windows.Forms.ToolStrip tsAcciones;
        private System.Windows.Forms.ToolStripButton tsbNuevo;
        private System.Windows.Forms.ToolStripButton tsbGuardar;
        private System.Windows.Forms.ToolStripButton tsbBuscar;
        private System.Windows.Forms.ToolStripButton btnUndo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnPrimeroPag;
        private System.Windows.Forms.ToolStripButton btnAtras;
        private System.Windows.Forms.ToolStripButton btnAdelante;
        private System.Windows.Forms.ToolStripButton btnUlitmoPag;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbImprimir;
        private System.Windows.Forms.ToolStripButton btnCierra;
        private System.Windows.Forms.Button btnBusqueda;
        private System.Windows.Forms.Button btnFooterCierra;
        private System.Windows.Forms.Button btnFooterGuardar;
        private System.Windows.Forms.TextBox txtDescripcionFormaPago;
        private System.Windows.Forms.TextBox txtCodigoFormaPago;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnBusquedaFormaPago;
        private System.Windows.Forms.TextBox txtFormaPaogId;
        private System.Windows.Forms.DataGridView dgvDetalles;
        private System.Windows.Forms.LinkLabel lkbBonifiacion;
        private System.Windows.Forms.LinkLabel lkbAbonoCapital;
        private System.Windows.Forms.Label lblListaBoletasRefrendadas;
        private System.Windows.Forms.Button btnFotografia;
        private System.Windows.Forms.Label lblBoletaBonifiacionFolio;
        private System.Windows.Forms.Label lblBoletaAbonoCapFolio;
        private System.Windows.Forms.ToolStripButton tsbEliminar;
        private System.Windows.Forms.Button btnVerCancelacion;
        private System.Windows.Forms.Label lblEstatus;
        private System.Windows.Forms.Label lblDescEstatus;
        private System.Windows.Forms.TextBox txtCobroTotal;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtEfectivo;
        private System.Windows.Forms.TextBox txtCambio;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.LinkLabel lkbCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn diaMinimoPromocion;
        private System.Windows.Forms.DataGridViewTextBoxColumn porcentajeTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn diaPromocion;
        private System.Windows.Forms.DataGridViewTextBoxColumn diaProcentaje;
        private System.Windows.Forms.DataGridViewTextBoxColumn cobrosTipoNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn diasMinimo;
        private System.Windows.Forms.DataGridViewTextBoxColumn prendaTipoId;
        private System.Windows.Forms.DataGridViewTextBoxColumn cobroTipoId;
        private System.Windows.Forms.DataGridViewTextBoxColumn porcentajeCobroTipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn importeCobroTipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn importeCobroTipoBonificacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaAutorizacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn usuarioAutorizacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn motivoAutorizacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn boleta_empeno_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Boleta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vence;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Día;
        private System.Windows.Forms.DataGridViewTextBoxColumn Capital;
        private System.Windows.Forms.DataGridViewTextBoxColumn Intereses;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bonificar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Subtotal;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Sel;
        private System.Windows.Forms.DataGridViewTextBoxColumn AbonoCap;
        private System.Windows.Forms.DataGridViewTextBoxColumn PPIAplica;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pagar;
    }
}