﻿namespace EmpenosEvora
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.muPrincipal = new System.Windows.Forms.MenuStrip();
            this.tsmCatalogos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmGenerales = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmClientes = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmOcupaciones = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAsentamientos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmMunicipios = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEstados = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmTiposDePrendas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmTipoDeSub = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmGramajes = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmConfiguracion = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEmpresas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSucursales = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmTipoDeIdentificacion = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFormaPago = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmTiposAsentamientos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCajas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmTiposCobros = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmProcesos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmUsuarios = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFolios = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFoliosProceso = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarHuellaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registroDeHuellaDigitalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarHuellaDigitalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmProcesosPadre = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmBoletasDeEmpeno = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRecibosLiqRefrendos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmMovimientosCaja = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAjudicacionBoletas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmLiqInternaBoletas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCorteCaja = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmReportes = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRepContables = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRepCortesCaja = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRepMovCaja = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRepRecibos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRepPrendas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRepBoletas = new System.Windows.Forms.ToolStripMenuItem();
            this.cierrarSessionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ssBarraEstado = new System.Windows.Forms.StatusStrip();
            this.tlsEmpresa = new System.Windows.Forms.ToolStripStatusLabel();
            this.tlsSucursal = new System.Windows.Forms.ToolStripStatusLabel();
            this.tlsUsuario = new System.Windows.Forms.ToolStripStatusLabel();
            this.epMensaje = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.muPrincipal.SuspendLayout();
            this.ssBarraEstado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epMensaje)).BeginInit();
            this.SuspendLayout();
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // muPrincipal
            // 
            this.muPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmCatalogos,
            this.verificarHuellaToolStripMenuItem,
            this.tsmProcesosPadre,
            this.tsmReportes,
            this.cierrarSessionToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.muPrincipal.Location = new System.Drawing.Point(0, 0);
            this.muPrincipal.Name = "muPrincipal";
            this.muPrincipal.Size = new System.Drawing.Size(800, 24);
            this.muPrincipal.TabIndex = 1;
            this.muPrincipal.Text = "menuStrip1";
            // 
            // tsmCatalogos
            // 
            this.tsmCatalogos.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmGenerales,
            this.tsmConfiguracion});
            this.tsmCatalogos.Name = "tsmCatalogos";
            this.tsmCatalogos.Size = new System.Drawing.Size(72, 20);
            this.tsmCatalogos.Text = "Catálogos";
            this.tsmCatalogos.Visible = false;
            this.tsmCatalogos.Click += new System.EventHandler(this.CATALOGOSToolStripMenuItem_Click);
            // 
            // tsmGenerales
            // 
            this.tsmGenerales.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmClientes,
            this.tsmOcupaciones,
            this.tsmAsentamientos,
            this.tsmMunicipios,
            this.tsmEstados,
            this.tsmTiposDePrendas,
            this.tsmTipoDeSub,
            this.tsmGramajes});
            this.tsmGenerales.Name = "tsmGenerales";
            this.tsmGenerales.Size = new System.Drawing.Size(150, 22);
            this.tsmGenerales.Text = "Generales";
            this.tsmGenerales.Visible = false;
            // 
            // tsmClientes
            // 
            this.tsmClientes.Name = "tsmClientes";
            this.tsmClientes.Size = new System.Drawing.Size(181, 22);
            this.tsmClientes.Text = "Clientes";
            this.tsmClientes.Visible = false;
            this.tsmClientes.Click += new System.EventHandler(this.ClientesToolStripMenuItem1_Click);
            // 
            // tsmOcupaciones
            // 
            this.tsmOcupaciones.Name = "tsmOcupaciones";
            this.tsmOcupaciones.Size = new System.Drawing.Size(181, 22);
            this.tsmOcupaciones.Text = "Ocupaciones";
            this.tsmOcupaciones.Visible = false;
            this.tsmOcupaciones.Click += new System.EventHandler(this.OcupacionesToolStripMenuItem_Click);
            // 
            // tsmAsentamientos
            // 
            this.tsmAsentamientos.Name = "tsmAsentamientos";
            this.tsmAsentamientos.Size = new System.Drawing.Size(181, 22);
            this.tsmAsentamientos.Text = "Asentamientos";
            this.tsmAsentamientos.Visible = false;
            this.tsmAsentamientos.Click += new System.EventHandler(this.AsentamientosToolStripMenuItem_Click);
            // 
            // tsmMunicipios
            // 
            this.tsmMunicipios.Name = "tsmMunicipios";
            this.tsmMunicipios.Size = new System.Drawing.Size(181, 22);
            this.tsmMunicipios.Text = "Municipios";
            this.tsmMunicipios.Visible = false;
            this.tsmMunicipios.Click += new System.EventHandler(this.MunicipiosToolStripMenuItem_Click);
            // 
            // tsmEstados
            // 
            this.tsmEstados.Name = "tsmEstados";
            this.tsmEstados.Size = new System.Drawing.Size(181, 22);
            this.tsmEstados.Text = "Estados";
            this.tsmEstados.Visible = false;
            this.tsmEstados.Click += new System.EventHandler(this.EstadosToolStripMenuItem_Click);
            // 
            // tsmTiposDePrendas
            // 
            this.tsmTiposDePrendas.Name = "tsmTiposDePrendas";
            this.tsmTiposDePrendas.Size = new System.Drawing.Size(181, 22);
            this.tsmTiposDePrendas.Text = "Tipos de prendas";
            this.tsmTiposDePrendas.Visible = false;
            this.tsmTiposDePrendas.Click += new System.EventHandler(this.TiposDePrendasToolStripMenuItem_Click);
            // 
            // tsmTipoDeSub
            // 
            this.tsmTipoDeSub.Name = "tsmTipoDeSub";
            this.tsmTipoDeSub.Size = new System.Drawing.Size(181, 22);
            this.tsmTipoDeSub.Text = "Subtipos de prendas";
            this.tsmTipoDeSub.Visible = false;
            this.tsmTipoDeSub.Click += new System.EventHandler(this.TipoDeSubToolStripMenuItem_Click);
            // 
            // tsmGramajes
            // 
            this.tsmGramajes.Name = "tsmGramajes";
            this.tsmGramajes.Size = new System.Drawing.Size(181, 22);
            this.tsmGramajes.Text = "Gramajes";
            this.tsmGramajes.Visible = false;
            this.tsmGramajes.Click += new System.EventHandler(this.TsmGramajes_Click);
            // 
            // tsmConfiguracion
            // 
            this.tsmConfiguracion.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmEmpresas,
            this.tsmSucursales,
            this.tsmTipoDeIdentificacion,
            this.tsmFormaPago,
            this.tsmTiposAsentamientos,
            this.tsmCajas,
            this.tsmTiposCobros,
            this.tsmProcesos,
            this.tsmUsuarios,
            this.tsmFolios,
            this.tsmFoliosProceso});
            this.tsmConfiguracion.Name = "tsmConfiguracion";
            this.tsmConfiguracion.Size = new System.Drawing.Size(150, 22);
            this.tsmConfiguracion.Text = "Configuración";
            // 
            // tsmEmpresas
            // 
            this.tsmEmpresas.Name = "tsmEmpresas";
            this.tsmEmpresas.Size = new System.Drawing.Size(204, 22);
            this.tsmEmpresas.Text = "Empresas";
            this.tsmEmpresas.Visible = false;
            this.tsmEmpresas.Click += new System.EventHandler(this.EmpresasToolStripMenuItem_Click);
            // 
            // tsmSucursales
            // 
            this.tsmSucursales.Name = "tsmSucursales";
            this.tsmSucursales.Size = new System.Drawing.Size(204, 22);
            this.tsmSucursales.Text = "Sucursales";
            this.tsmSucursales.Visible = false;
            this.tsmSucursales.Click += new System.EventHandler(this.SucursalesToolStripMenuItem1_Click);
            // 
            // tsmTipoDeIdentificacion
            // 
            this.tsmTipoDeIdentificacion.Name = "tsmTipoDeIdentificacion";
            this.tsmTipoDeIdentificacion.Size = new System.Drawing.Size(204, 22);
            this.tsmTipoDeIdentificacion.Text = "Tipos de identificaciones";
            this.tsmTipoDeIdentificacion.Visible = false;
            this.tsmTipoDeIdentificacion.Click += new System.EventHandler(this.TipoDeIdentificacionToolStripMenuItem1_Click);
            // 
            // tsmFormaPago
            // 
            this.tsmFormaPago.Name = "tsmFormaPago";
            this.tsmFormaPago.Size = new System.Drawing.Size(204, 22);
            this.tsmFormaPago.Text = "Formas de pago";
            this.tsmFormaPago.Visible = false;
            this.tsmFormaPago.Click += new System.EventHandler(this.TsmFormaPago_Click);
            // 
            // tsmTiposAsentamientos
            // 
            this.tsmTiposAsentamientos.Name = "tsmTiposAsentamientos";
            this.tsmTiposAsentamientos.Size = new System.Drawing.Size(204, 22);
            this.tsmTiposAsentamientos.Text = "Tipos de asentamientos";
            this.tsmTiposAsentamientos.Visible = false;
            this.tsmTiposAsentamientos.Click += new System.EventHandler(this.TsmTiposAsentamientos_Click);
            // 
            // tsmCajas
            // 
            this.tsmCajas.Name = "tsmCajas";
            this.tsmCajas.Size = new System.Drawing.Size(204, 22);
            this.tsmCajas.Text = "Cajas";
            this.tsmCajas.Visible = false;
            this.tsmCajas.Click += new System.EventHandler(this.TsmCajas_Click);
            // 
            // tsmTiposCobros
            // 
            this.tsmTiposCobros.Name = "tsmTiposCobros";
            this.tsmTiposCobros.Size = new System.Drawing.Size(204, 22);
            this.tsmTiposCobros.Text = "Tipos de cobros";
            this.tsmTiposCobros.Visible = false;
            this.tsmTiposCobros.Click += new System.EventHandler(this.TsmTiposCobros_Click);
            // 
            // tsmProcesos
            // 
            this.tsmProcesos.Name = "tsmProcesos";
            this.tsmProcesos.Size = new System.Drawing.Size(204, 22);
            this.tsmProcesos.Text = "Procesos";
            this.tsmProcesos.Visible = false;
            this.tsmProcesos.Click += new System.EventHandler(this.procesosToolStripMenuItem1_Click);
            // 
            // tsmUsuarios
            // 
            this.tsmUsuarios.Name = "tsmUsuarios";
            this.tsmUsuarios.Size = new System.Drawing.Size(204, 22);
            this.tsmUsuarios.Text = "Usuarios";
            this.tsmUsuarios.Visible = false;
            this.tsmUsuarios.Click += new System.EventHandler(this.usuariosToolStripMenuItem_Click);
            // 
            // tsmFolios
            // 
            this.tsmFolios.Name = "tsmFolios";
            this.tsmFolios.Size = new System.Drawing.Size(204, 22);
            this.tsmFolios.Text = "Folios";
            this.tsmFolios.Visible = false;
            this.tsmFolios.Click += new System.EventHandler(this.tsmFolios_Click);
            // 
            // tsmFoliosProceso
            // 
            this.tsmFoliosProceso.Name = "tsmFoliosProceso";
            this.tsmFoliosProceso.Size = new System.Drawing.Size(204, 22);
            this.tsmFoliosProceso.Text = "Folio proceso";
            this.tsmFoliosProceso.Visible = false;
            this.tsmFoliosProceso.Click += new System.EventHandler(this.folioProcesoToolStripMenuItem_Click);
            // 
            // verificarHuellaToolStripMenuItem
            // 
            this.verificarHuellaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registroDeHuellaDigitalToolStripMenuItem,
            this.verificarHuellaDigitalToolStripMenuItem});
            this.verificarHuellaToolStripMenuItem.Name = "verificarHuellaToolStripMenuItem";
            this.verificarHuellaToolStripMenuItem.Size = new System.Drawing.Size(105, 20);
            this.verificarHuellaToolStripMenuItem.Text = "Lector de Huella";
            this.verificarHuellaToolStripMenuItem.Visible = false;
            this.verificarHuellaToolStripMenuItem.Click += new System.EventHandler(this.VerificarHuellaToolStripMenuItem_Click);
            // 
            // registroDeHuellaDigitalToolStripMenuItem
            // 
            this.registroDeHuellaDigitalToolStripMenuItem.Name = "registroDeHuellaDigitalToolStripMenuItem";
            this.registroDeHuellaDigitalToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.registroDeHuellaDigitalToolStripMenuItem.Text = "Registrar huella digital";
            this.registroDeHuellaDigitalToolStripMenuItem.Click += new System.EventHandler(this.registroDeHuellaDigitalToolStripMenuItem_Click);
            // 
            // verificarHuellaDigitalToolStripMenuItem
            // 
            this.verificarHuellaDigitalToolStripMenuItem.Name = "verificarHuellaDigitalToolStripMenuItem";
            this.verificarHuellaDigitalToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.verificarHuellaDigitalToolStripMenuItem.Text = "Verificar huella digital";
            // 
            // tsmProcesosPadre
            // 
            this.tsmProcesosPadre.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmBoletasDeEmpeno,
            this.tsmRecibosLiqRefrendos,
            this.tsmMovimientosCaja,
            this.tsmAjudicacionBoletas,
            this.tsmLiqInternaBoletas,
            this.tsmCorteCaja});
            this.tsmProcesosPadre.Name = "tsmProcesosPadre";
            this.tsmProcesosPadre.Size = new System.Drawing.Size(66, 20);
            this.tsmProcesosPadre.Text = "Procesos";
            this.tsmProcesosPadre.Visible = false;
            // 
            // tsmBoletasDeEmpeno
            // 
            this.tsmBoletasDeEmpeno.Name = "tsmBoletasDeEmpeno";
            this.tsmBoletasDeEmpeno.Size = new System.Drawing.Size(261, 22);
            this.tsmBoletasDeEmpeno.Text = "Boletas de Empeño";
            this.tsmBoletasDeEmpeno.Visible = false;
            this.tsmBoletasDeEmpeno.Click += new System.EventHandler(this.BoletasDeEmpeñoToolStripMenuItem_Click);
            // 
            // tsmRecibosLiqRefrendos
            // 
            this.tsmRecibosLiqRefrendos.Name = "tsmRecibosLiqRefrendos";
            this.tsmRecibosLiqRefrendos.Size = new System.Drawing.Size(261, 22);
            this.tsmRecibosLiqRefrendos.Text = "Recibos de Liquidación y Refrendos";
            this.tsmRecibosLiqRefrendos.Visible = false;
            this.tsmRecibosLiqRefrendos.Click += new System.EventHandler(this.RecibosDeLiquidaciónYRefrendosToolStripMenuItem_Click);
            // 
            // tsmMovimientosCaja
            // 
            this.tsmMovimientosCaja.Name = "tsmMovimientosCaja";
            this.tsmMovimientosCaja.Size = new System.Drawing.Size(261, 22);
            this.tsmMovimientosCaja.Text = "Movimientos de caja";
            this.tsmMovimientosCaja.Visible = false;
            this.tsmMovimientosCaja.Click += new System.EventHandler(this.MovimientosDeCajaToolStripMenuItem_Click);
            // 
            // tsmAjudicacionBoletas
            // 
            this.tsmAjudicacionBoletas.Name = "tsmAjudicacionBoletas";
            this.tsmAjudicacionBoletas.Size = new System.Drawing.Size(261, 22);
            this.tsmAjudicacionBoletas.Text = "Adjudicación de Boletas";
            this.tsmAjudicacionBoletas.Visible = false;
            this.tsmAjudicacionBoletas.Click += new System.EventHandler(this.AdjudicaciónDeBoletasToolStripMenuItem_Click);
            // 
            // tsmLiqInternaBoletas
            // 
            this.tsmLiqInternaBoletas.Name = "tsmLiqInternaBoletas";
            this.tsmLiqInternaBoletas.Size = new System.Drawing.Size(261, 22);
            this.tsmLiqInternaBoletas.Text = "Liquidación Interna de Boletas";
            this.tsmLiqInternaBoletas.Visible = false;
            this.tsmLiqInternaBoletas.Click += new System.EventHandler(this.LiquidaciónInternaDeBoletasToolStripMenuItem_Click);
            // 
            // tsmCorteCaja
            // 
            this.tsmCorteCaja.Name = "tsmCorteCaja";
            this.tsmCorteCaja.Size = new System.Drawing.Size(261, 22);
            this.tsmCorteCaja.Text = "Corte de caja";
            this.tsmCorteCaja.Visible = false;
            this.tsmCorteCaja.Click += new System.EventHandler(this.cierreDeCajaToolStripMenuItem_Click);
            // 
            // tsmReportes
            // 
            this.tsmReportes.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmRepContables,
            this.tsmRepCortesCaja,
            this.tsmRepMovCaja,
            this.tsmRepRecibos,
            this.tsmRepPrendas,
            this.tsmRepBoletas});
            this.tsmReportes.Name = "tsmReportes";
            this.tsmReportes.Size = new System.Drawing.Size(65, 20);
            this.tsmReportes.Text = "Reportes";
            this.tsmReportes.Visible = false;
            // 
            // tsmRepContables
            // 
            this.tsmRepContables.Name = "tsmRepContables";
            this.tsmRepContables.Size = new System.Drawing.Size(250, 22);
            this.tsmRepContables.Text = "Reportes Contables";
            this.tsmRepContables.Visible = false;
            this.tsmRepContables.Click += new System.EventHandler(this.ReportesContablesToolStripMenuItem_Click);
            // 
            // tsmRepCortesCaja
            // 
            this.tsmRepCortesCaja.Name = "tsmRepCortesCaja";
            this.tsmRepCortesCaja.Size = new System.Drawing.Size(250, 22);
            this.tsmRepCortesCaja.Text = "Relación de Cortes de Caja";
            this.tsmRepCortesCaja.Visible = false;
            this.tsmRepCortesCaja.Click += new System.EventHandler(this.RelaciónDeCortesDeCajaToolStripMenuItem_Click);
            // 
            // tsmRepMovCaja
            // 
            this.tsmRepMovCaja.Name = "tsmRepMovCaja";
            this.tsmRepMovCaja.Size = new System.Drawing.Size(250, 22);
            this.tsmRepMovCaja.Text = "Relación de Movimientos de Caja";
            this.tsmRepMovCaja.Visible = false;
            this.tsmRepMovCaja.Click += new System.EventHandler(this.RelaciónDeMovimientosDeCajaToolStripMenuItem_Click);
            // 
            // tsmRepRecibos
            // 
            this.tsmRepRecibos.Name = "tsmRepRecibos";
            this.tsmRepRecibos.Size = new System.Drawing.Size(250, 22);
            this.tsmRepRecibos.Text = "Relación de Recibos";
            this.tsmRepRecibos.Visible = false;
            this.tsmRepRecibos.Click += new System.EventHandler(this.RelaciónDeRecibosToolStripMenuItem_Click);
            // 
            // tsmRepPrendas
            // 
            this.tsmRepPrendas.Name = "tsmRepPrendas";
            this.tsmRepPrendas.Size = new System.Drawing.Size(250, 22);
            this.tsmRepPrendas.Text = "Relación de Prendas";
            this.tsmRepPrendas.Visible = false;
            this.tsmRepPrendas.Click += new System.EventHandler(this.RelaciónDePrendasToolStripMenuItem_Click);
            // 
            // tsmRepBoletas
            // 
            this.tsmRepBoletas.Name = "tsmRepBoletas";
            this.tsmRepBoletas.Size = new System.Drawing.Size(250, 22);
            this.tsmRepBoletas.Text = "Relación de Boletas";
            this.tsmRepBoletas.Visible = false;
            this.tsmRepBoletas.Click += new System.EventHandler(this.RelaciónDeBoletasToolStripMenuItem_Click);
            // 
            // cierrarSessionToolStripMenuItem
            // 
            this.cierrarSessionToolStripMenuItem.Name = "cierrarSessionToolStripMenuItem";
            this.cierrarSessionToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.cierrarSessionToolStripMenuItem.Text = "Cerrar Sesión";
            this.cierrarSessionToolStripMenuItem.Click += new System.EventHandler(this.cierrarSessionToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // ssBarraEstado
            // 
            this.ssBarraEstado.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlsEmpresa,
            this.tlsSucursal,
            this.tlsUsuario});
            this.ssBarraEstado.Location = new System.Drawing.Point(0, 428);
            this.ssBarraEstado.Name = "ssBarraEstado";
            this.ssBarraEstado.Size = new System.Drawing.Size(800, 22);
            this.ssBarraEstado.TabIndex = 5;
            this.ssBarraEstado.Text = "statusStrip1";
            this.ssBarraEstado.Visible = false;
            // 
            // tlsEmpresa
            // 
            this.tlsEmpresa.BackColor = System.Drawing.SystemColors.Control;
            this.tlsEmpresa.Name = "tlsEmpresa";
            this.tlsEmpresa.Size = new System.Drawing.Size(55, 17);
            this.tlsEmpresa.Text = "Empresa:";
            // 
            // tlsSucursal
            // 
            this.tlsSucursal.BackColor = System.Drawing.SystemColors.Control;
            this.tlsSucursal.Name = "tlsSucursal";
            this.tlsSucursal.Size = new System.Drawing.Size(54, 17);
            this.tlsSucursal.Text = "Sucursal:";
            // 
            // tlsUsuario
            // 
            this.tlsUsuario.BackColor = System.Drawing.SystemColors.Control;
            this.tlsUsuario.Name = "tlsUsuario";
            this.tlsUsuario.Size = new System.Drawing.Size(53, 17);
            this.tlsUsuario.Text = "Usuario: ";
            // 
            // epMensaje
            // 
            this.epMensaje.ContainerControl = this;
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ssBarraEstado);
            this.Controls.Add(this.muPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.muPrincipal;
            this.Name = "frmPrincipal";
            this.Text = "Empeños Evora";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmPrincipal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.muPrincipal.ResumeLayout(false);
            this.muPrincipal.PerformLayout();
            this.ssBarraEstado.ResumeLayout(false);
            this.ssBarraEstado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epMensaje)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.MenuStrip muPrincipal;
        private System.Windows.Forms.ToolStripMenuItem verificarHuellaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registroDeHuellaDigitalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verificarHuellaDigitalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmCatalogos;
        private System.Windows.Forms.ToolStripMenuItem tsmConfiguracion;
        private System.Windows.Forms.ToolStripMenuItem tsmEmpresas;
        private System.Windows.Forms.ToolStripMenuItem tsmSucursales;
        private System.Windows.Forms.ToolStripMenuItem tsmGenerales;
        private System.Windows.Forms.ToolStripMenuItem tsmEstados;
        private System.Windows.Forms.ToolStripMenuItem tsmMunicipios;
        private System.Windows.Forms.ToolStripMenuItem tsmTiposDePrendas;
        private System.Windows.Forms.ToolStripMenuItem tsmClientes;
        private System.Windows.Forms.ToolStripMenuItem tsmOcupaciones;
        private System.Windows.Forms.StatusStrip ssBarraEstado;
        private System.Windows.Forms.ToolStripMenuItem tsmProcesosPadre;
        private System.Windows.Forms.ToolStripMenuItem tsmBoletasDeEmpeno;
        private System.Windows.Forms.ToolStripMenuItem tsmRecibosLiqRefrendos;
        private System.Windows.Forms.ToolStripMenuItem tsmMovimientosCaja;
        private System.Windows.Forms.ToolStripMenuItem tsmAjudicacionBoletas;
        private System.Windows.Forms.ToolStripMenuItem tsmLiqInternaBoletas;
        private System.Windows.Forms.ToolStripMenuItem tsmReportes;
        private System.Windows.Forms.ToolStripMenuItem tsmRepContables;
        private System.Windows.Forms.ToolStripMenuItem tsmRepCortesCaja;
        private System.Windows.Forms.ToolStripMenuItem tsmRepMovCaja;
        private System.Windows.Forms.ToolStripMenuItem tsmRepRecibos;
        private System.Windows.Forms.ToolStripMenuItem tsmRepPrendas;
        private System.Windows.Forms.ToolStripMenuItem tsmRepBoletas;
        public System.Windows.Forms.ErrorProvider epMensaje;
        private System.Windows.Forms.ToolStripMenuItem tsmTipoDeSub;
        private System.Windows.Forms.ToolStripMenuItem tsmAsentamientos;
        private System.Windows.Forms.ToolStripMenuItem tsmTipoDeIdentificacion;
        private System.Windows.Forms.ToolStripMenuItem tsmFormaPago;
        private System.Windows.Forms.ToolStripMenuItem tsmTiposAsentamientos;
        private System.Windows.Forms.ToolStripMenuItem tsmCajas;
        private System.Windows.Forms.ToolStripMenuItem tsmTiposCobros;
        private System.Windows.Forms.ToolStripMenuItem tsmGramajes;
        private System.Windows.Forms.ToolStripMenuItem tsmProcesos;
        private System.Windows.Forms.ToolStripMenuItem tsmUsuarios;
        private System.Windows.Forms.ToolStripStatusLabel tlsEmpresa;
        private System.Windows.Forms.ToolStripStatusLabel tlsSucursal;
        private System.Windows.Forms.ToolStripStatusLabel tlsUsuario;
        private System.Windows.Forms.ToolStripMenuItem tsmFolios;
        private System.Windows.Forms.ToolStripMenuItem cierrarSessionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmFoliosProceso;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmCorteCaja;
    }
}

