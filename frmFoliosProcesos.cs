﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmFoliosProcesos : Form
    {
    

        public Controller.clsCtrlFoliosProcesos ctrlFoliosProcesos;        
        public Core.clsCoreView clsView;

        public int _intUlitmoCodigo;
        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }

        public List<Properties_Class.clsFoliosProcesos> lsDeshacerFoliosProcesos;

        public frmFoliosProcesos()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();

         
            
            
        }


        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnBusqueda_Click(object sender, EventArgs e)
        {
            if (this.ctrlFoliosProcesos.bolResutladoPaginacion)
            {
                this.ctrlFoliosProcesos.CargarFrmBusqueda();

                if (this.ctrlFoliosProcesos.bolResultadoBusqueda)
                {
                    this.llenar_formulario();
                    txtDescripcion.Focus();
                }
            }
        }

        private void TsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlFoliosProcesos.foliosProcesos.intId == 0) return;

            if (this.clsView.confirmacionEliminar("Estados"))
            {
                this.ctrlFoliosProcesos.Eliminar();
                if (this.ctrlFoliosProcesos.bolResultado)
                {
                    this.ctrlFoliosProcesos.Paginacion();
                    this.limpiar_textbox();
                    this.clsView.mensajeExitoDB("folio proceso", "elimino");
                }
            }
        }


        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlFoliosProcesos.bolResutladoPaginacion)
            {
                txtDescripcion.Text = this.lsDeshacerFoliosProcesos[0].strDescripcion.ToString();
                txtSeries.Text = this.lsDeshacerFoliosProcesos[0].folio.strSerie.ToString();
                txtConsecutivo.Text = this.lsDeshacerFoliosProcesos[0].folio.intConsecutivo.ToString();
                txtDescripcion.Text = this.lsDeshacerFoliosProcesos[0].strDescripcion.ToString();


            }
        }


        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlFoliosProcesos.PrimeroPaginacion();
            if (!this.ctrlFoliosProcesos.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlFoliosProcesos.SiguentePaginacion();
            if (!this.ctrlFoliosProcesos.bolResultado) return;
            this.llenar_formulario();

        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlFoliosProcesos.AtrasPaginacion();
            if (!this.ctrlFoliosProcesos.bolResultado) return;
            this.llenar_formulario();

        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlFoliosProcesos.UltimoPaginacion();
            if (!this.ctrlFoliosProcesos.bolResultado) return;
            this.llenar_formulario();

        }
        private void TsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();

            this.ctrlFoliosProcesos.foliosProcesos.limpiarProps();
            
           
        }

        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {
        }

        private void TxtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);

        }



        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
        }

        private bool validacion()
        {

            bool bolValidacion = true;
            string strFolioId = txtFolioId.Text;
            string strProcesoId = txtProcesoId.Text;

            if (strFolioId == "")
            {
                txtDescripcion.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("El campo folio es obligatorio, favor de ingresar un folio"));
                bolValidacion = false;
            }

            if (strProcesoId == "")
            {
                txtProcesoDescripcion.Focus();
                this.clsView.principal.epMensaje.SetError(txtProcesoDescripcion, this.clsView.mensajeErrorValiForm2("El campo proceso es obligatorio, favor de ingresar un proceso"));
                bolValidacion = false;
            }
            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();

            if (validacion())
            {
                this.ctrlFoliosProcesos.foliosProcesos.folio.intId = int.Parse(txtFolioId.Text.ToString());
                this.ctrlFoliosProcesos.foliosProcesos.procesos.intId = int.Parse(txtProcesoId.Text.ToString());
                this.ctrlFoliosProcesos.foliosProcesos.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlFoliosProcesos.foliosProcesos.intSucursal = this.clsView.login.intSucursalId;


                this.ctrlFoliosProcesos.BuscarProcesoFolio();
                if (this.ctrlFoliosProcesos.mdlFolioProcesos.bolResultado) 
                {

                    this.ctrlFoliosProcesos.foliosProcesos.intId = 1;
                }

                this.ctrlFoliosProcesos.Guardar();

                if (this.ctrlFoliosProcesos.mdlFolioProcesos.bolResultado)
                {
                    this.limpiar_textbox();
                    this.ctrlFoliosProcesos.Paginacion();
                    this.clsView.mensajeExitoDB("folio proceso", "guardó");

                }
            }
        }

        public void llenar_formulario()
        {

            if (this.ctrlFoliosProcesos.foliosProcesos.intId == 0) return;
            txtDescripcion.Text = this.ctrlFoliosProcesos.foliosProcesos.folio.strDescripcion;
            txtSeries.Text = this.ctrlFoliosProcesos.foliosProcesos.folio.strSerie;
            txtConsecutivo.Text = this.ctrlFoliosProcesos.foliosProcesos.folio.intConsecutivo.ToString();
            txtProcesoDescripcion.Text = this.ctrlFoliosProcesos.foliosProcesos.procesos.strDescripcion;
            txtProcesoId.Text = this.ctrlFoliosProcesos.foliosProcesos.procesos.intId.ToString();
            this.ctrlFoliosProcesos.foliosProcesos.intUsuario = this.clsView.login.usuario.intId;

        }


        public void agregarDeshacer()
        {
            this.lsDeshacerFoliosProcesos.RemoveAt(0);
            this.lsDeshacerFoliosProcesos.Add(this.ctrlFoliosProcesos.foliosProcesos);
        }

        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void TsbBuscar_Click(object sender, EventArgs e)
        {


            Dictionary<int, Properties_Class.clsTextbox> dicFocus = new Dictionary<int, Properties_Class.clsTextbox>()
            {
                { 0, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcion, strBusqueda = "folio"}
                },
                { 1, new Properties_Class.clsTextbox()
                                { txtGenerico = txtSeries, strBusqueda = "folio"}
                },
                { 2, new Properties_Class.clsTextbox()
                                { txtGenerico = txtConsecutivo, strBusqueda = "folio"}
                },
                { 3, new Properties_Class.clsTextbox()
                                { txtGenerico = txtProcesoDescripcion, strBusqueda = "procesos"}
                }
            };
            foreach (KeyValuePair<int, Properties_Class.clsTextbox> data in dicFocus)
            {
                if (data.Value.txtGenerico.Focused)
                {
                    if (data.Value.strBusqueda == "foliosProcesos")
                    {
                        this.mostraBusqueda();
                    }
                    else if (data.Value.strBusqueda == "folio")
                    {
                        this.mostraBusquedaFolio();
                    }
                    else if (data.Value.strBusqueda == "procesos")
                    {
                        this.mostraBusquedaProceso();
                    }
                }

            }


                       
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void txtDescripcion_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblCodigo_Click(object sender, EventArgs e)
        {

        }

        private void btnBusquedaFolio_Click(object sender, EventArgs e)
        {
            this.mostraBusquedaFolio();
        }

        private void btnProcesosBusqueda_Click(object sender, EventArgs e)
        {
            this.mostraBusquedaProceso();
        }

        public void mostraBusqueda() 
        {
            if (this.ctrlFoliosProcesos.bolResutladoPaginacion)
            {
                this.ctrlFoliosProcesos.CargarFrmBusqueda();
                if (this.ctrlFoliosProcesos.bolResultadoBusqueda) 
                {
                    this.llenar_formulario();
                }
                
            }
        }

        public void mostraBusquedaFolio() 
        {
            Controller.clsCtrlFolios ctrlFolios = new Controller.clsCtrlFolios();
            ctrlFolios.Paginacion();
            if (ctrlFolios.bolResutladoPaginacion)
            {
                ctrlFolios.CargarFrmBusqueda();

                if (ctrlFolios.bolResultadoBusqueda)
                {
                    txtDescripcion.Text = ctrlFolios.folio.strDescripcion;
                    txtFolioId.Text = ctrlFolios.folio.intId.ToString();
                    txtSeries.Text = ctrlFolios.folio.strSerie;
                    txtConsecutivo.Text = ctrlFolios.folio.intConsecutivo.ToString();
                }
            }
        }

        public void mostraBusquedaProceso() 
        {
            Controller.clsCtrlProcesos ctrlProcesos = new Controller.clsCtrlProcesos();
            ctrlProcesos.Paginacion();
            if (ctrlProcesos.bolResutladoPaginacion)
            {
                ctrlProcesos.CargarFrmBusqueda();

                if (ctrlProcesos.bolResultadoBusqueda)
                {
                    txtProcesoDescripcion.Text = ctrlProcesos.procesos.strDescripcion;
                    txtProcesoId.Text = ctrlProcesos.procesos.intId.ToString();
                }
            }
        }

        private void frmFoliosProcesos_Load(object sender, EventArgs e)
        {
            this.ctrlFoliosProcesos = new Controller.clsCtrlFoliosProcesos();
            this.ctrlFoliosProcesos.foliosProcesos.intSucursal = this.clsView.login.intSucursalId;
            this.ctrlFoliosProcesos.Paginacion();
            this.lsDeshacerFoliosProcesos = new List<Properties_Class.clsFoliosProcesos>();
            this.lsDeshacerFoliosProcesos.Add(this.ctrlFoliosProcesos.foliosProcesos);

            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);

        }
    }
}
