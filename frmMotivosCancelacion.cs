﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmMotivosCancelacion : Form
    {
        public bool _bolResultado;
        public string _strMotivo;
        public string _strTipo;
        


        public Core.clsCoreView clsView = new Core.clsCoreView();        
        public string strMotivo { get => _strMotivo; set => _strMotivo = value; }
        public bool bolResultado { get => _bolResultado; set => _bolResultado = value; }

        public string strTipo { get => _strTipo; set => _strTipo = value; }

        public frmMotivosCancelacion()
        {
            InitializeComponent();
            this.bolResultado = false;
        }


        private void frmMotivosCancelacion_Load(object sender, EventArgs e)
        {
            if (this.strTipo == "G")//Guardar
            {
                btnFooterGuardar.Visible = true;
                txtMotivo.Focus();
            }
            else  if(this.strTipo == "V")//Ver
            {
                txtMotivo.Text = this.strMotivo;
                btnFooterGuardar.Visible = false;
            }


        }
        private void btnFooterGuardar_Click(object sender, EventArgs e)
        {


            if (txtMotivo.Text == "")             
            {
                txtMotivo.Focus();
                this.clsView.principal.epMensaje.SetError(txtMotivo, this.clsView.mensajeErrorValiForm2("El campo motivo es obligatorio, favor de ingresar un motivo "));                
            }
            else 
            {
                    this.strMotivo = txtMotivo.Text;
                    this.bolResultado = true;
                    this.Close();                                
            }
        }

        private void btnFooterCierra_Click(object sender, EventArgs e)
        {            
            this.Close();
        }


    }
}
