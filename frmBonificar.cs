﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UI_Support;

namespace EmpenosEvora
{
    public partial class frmBonificar : Form
    {
        public Core.clsCoreView clsView;

        private bool _bolSucursal;
        private bool _bolEmpresa;
        private bool _bolResultado;
        public string _strMotivo;
        public DataRow _objDataSetSingle;

        public bool bolSucursal { get => _bolSucursal; set => _bolSucursal = value; }
        public bool bolEmpresa { get => _bolEmpresa; set => _bolEmpresa = value; }
        public bool bolResultado { get => _bolResultado; set => _bolResultado = value; }
        public string strMotivo { get => _strMotivo; set => _strMotivo = value; }


        public DataRow objDataSetSingle { get => _objDataSetSingle; set => _objDataSetSingle = value; }

        public Controller.clsCtrlLogin ctrlLogin;


        public frmBonificar()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();
            this.ctrlLogin = new Controller.clsCtrlLogin();

            //btnVerificarHuella.Enabled = false;
            this.bolResultado = false;
        }

        private void frmAutorizar_Load(object sender, EventArgs e)
        {

        }




        private void btnVerificarHuella_Click(object sender, EventArgs e)
        {
            Controller.clsCtrlUsuarios ctrlUsuarios = new Controller.clsCtrlUsuarios();
            ctrlUsuarios.Paginacion();


            VerificationForm frmVerificarHuella = new VerificationForm();
            frmVerificarHuella.objDataSet = ctrlUsuarios.objDataSet;

            frmVerificarHuella.ShowDialog();

            if (frmVerificarHuella.bolResultado)
            {
                this.objDataSetSingle = frmVerificarHuella.objDataSetSingle;
                ctrlUsuarios.BuscarHuella(frmVerificarHuella.objDataSetSingle);

                Properties_Class.clsConstante clsCon = new Properties_Class.clsConstante();



                if (ctrlUsuarios.usuario.intId > 0)
                {
                    if (ctrlUsuarios.usuario.byteIntentos == clsCon.intIntentos)
                    {
                        MessageBox.Show("Su cuenta se encuentra suspendida, comuníquese con el administrador", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else if (!ctrlUsuarios.usuario.validarAutorizacion())
                    {
                        MessageBox.Show("Su usuario no cuenta con el permiso de autorizar, comuníquese con el administrador", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {

                        this.bolResultado = true;
                        if (txtMotivo.Text == "")
                        {
                            txtMotivo.Focus();
                            this.clsView.principal.epMensaje.SetError(txtMotivo, this.clsView.mensajeErrorValiForm2("El campo motivo es obligatorio, favor de ingresar un motivo "));

                        }
                        else
                        {
                            this.strMotivo = txtMotivo.Text;                            
                            this.Close();
                        }
                        


                        
                    }
                }

            }
        }

        private void btnFooterAcceder_Click(object sender, EventArgs e)
        {
            if (validacion())
            {
                if (txtUsuarioId.Text != "" && this.bolResultado)
                {


                }
                else
                {

                    if (!this.bolResultado)
                    {
                        this.ctrlLogin = new Controller.clsCtrlLogin();
                        ctrlLogin.Login(txtUsuario.Text.TrimStart(), txtContrasena.Text.TrimStart());
                        Properties_Class.clsConstante clsCon = new Properties_Class.clsConstante();

                        if (this.ctrlLogin.bolResultado)
                        {

                            if (this.ctrlLogin.login.usuario.byteIntentos >= clsCon.intIntentos)
                            {
                                MessageBox.Show("Su cuenta se encuentra suspendida ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            else if (!ctrlLogin.login.usuario.validarAutorizacion())
                            {
                                MessageBox.Show("Su usuario no cuenta con el permiso de autorizar, comuníquese con el administrador", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            else
                            {

                                this.objDataSetSingle = this.ctrlLogin.objDataSet.Tables[0].Rows[0];
                                this.strMotivo = txtMotivo.Text;
                                this.bolResultado = true;
                                this.Close();

                            }

                        }
                        else
                        {


                            ctrlLogin.limpiarProps();
                            ctrlLogin.BuscarUsuario(txtUsuario.Text.TrimStart());

                            if (ctrlLogin.bolResultado && ctrlLogin.login.usuario.strEstatus == ctrlLogin.login.dicEstatus["ACTIVO"])
                            {

                                if (!(ctrlLogin.login.usuario.byteIntentos == clsCon.intIntentos))
                                {
                                    MessageBox.Show("Usuario y contraseña son incorrectas, intenta de nuevo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            else
                            {

                                MessageBox.Show("Su cuenta se encuentra suspendida , comuníquese con el administrador ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }


                        }

                    }
                    else 
                    {
                        this.strMotivo = txtMotivo.Text;
                        this.bolResultado = true;
                        this.Close();
                    }
                    

                }

            }

        }


        private bool validacion()
        {

            bool bolValidacion = true;

            string strUsuario = txtUsuario.Text.TrimStart();
            string strContrasena = txtContrasena.Text.TrimStart();
            string strUsuarioId = txtUsuarioId.Text;
            string strMotivo = txtMotivo.Text;

            if (!this.bolResultado) 
            {
                if (strUsuario == "")
                {
                    txtUsuario.Focus();
                    this.clsView.principal.epMensaje.SetError(txtUsuario, this.clsView.mensajeErrorValiForm2("El campo usuario es obligatorio, favor de ingresar un usuario "));
                    bolValidacion = false;
                }

                if (strContrasena == "")
                {
                    txtContrasena.Focus();
                    this.clsView.principal.epMensaje.SetError(txtContrasena, this.clsView.mensajeErrorValiForm2("El campo contraseña es obligatorio, favor de ingresar una contraseña "));
                    bolValidacion = false;
                }
            }

            if (strMotivo == "") 
            {
                txtMotivo.Focus();
                this.clsView.principal.epMensaje.SetError(txtMotivo, this.clsView.mensajeErrorValiForm2("El campo motivo es obligatorio, favor de ingresar un motivo "));
                bolValidacion = false;
            }

            return bolValidacion;
        }

        private void btnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmBonificar_Load(object sender, EventArgs e)
        {

        }

    }
}
