﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UI_Support;

namespace EmpenosEvora
{
    public partial class frmLogin : Form
    {

        public Core.clsCoreView clsView;

        private bool _bolSucursal;
        private bool _bolEmpresa;
        private bool _bolResultado;

        public bool bolSucursal { get => _bolSucursal; set => _bolSucursal = value; }        
        public bool bolEmpresa { get => _bolEmpresa; set => _bolEmpresa = value; }
        public bool bolResultado { get => _bolResultado; set => _bolResultado = value; }

        public Controller.clsCtrlLogin ctrlLogin;


        public frmLogin()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();
            this.ctrlLogin = new Controller.clsCtrlLogin();

            //btnVerificarHuella.Enabled = false;
            this.bolResultado = false;
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }


       /* private void txtDescripcionEmpresas_Leave(object sender, EventArgs e)
        {

            if (txtDescripcionEmpresas.Text == "")
            {

                this.bolEmpresa = false;
                btnVerificarHuella.Enabled = false;
                
            }
            else
            {
                this.bolEmpresa = true;
                if (this.bolSucursal)
                {
                    btnVerificarHuella.Enabled = true;
                }
            }

        }

        private void txtCodigoSucursal_Leave(object sender, EventArgs e)
        {
            Controller.clsCtrlSucursales ctrlSucursales = new Controller.clsCtrlSucursales();
            ctrlSucursales.Buscar(txtCodigoSucursal.Text);
            txtSucursalId.Text = ctrlSucursales.sucursal.intId.ToString();
            txtDescripcionSucursal.Text = ctrlSucursales.sucursal.strNombre.ToString();
            txtCodigoSucursal.Text = ctrlSucursales.sucursal.strCodigo.ToString();


            

            
            if (txtDescripcionSucursal.Text == "")
            {                                               
                this.bolSucursal = false;
                btnVerificarHuella.Enabled = false;
                
            }
            else 
            {
                this.bolSucursal = true;
                if (this.bolEmpresa)
                {
                    btnVerificarHuella.Enabled = true;
                }
                
            }
        }

        private void btnBusquedaEmpresa_Click(object sender, EventArgs e)
        {
            Controller.clsCtrlEmpresas ctrlEmpresas = new Controller.clsCtrlEmpresas();
            ctrlEmpresas.Paginacion();
            ctrlEmpresas.CargarFrmBusqueda();                        
            txtEmpresaId.Text = ctrlEmpresas.empresa.intId.ToString();
            txtDescripcionEmpresas.Text = ctrlEmpresas.empresa.strNombreComercial;

            //Limpiar datos de sucursal
            txtSucursalId.Text = "";
            txtCodigoSucursal.Text = "";
            txtDescripcionSucursal.Text = "";

            this.bolEmpresa = true;

            if (this.bolSucursal)
            {
                btnVerificarHuella.Enabled = true;
            }
            else 
            {
                btnVerificarHuella.Enabled = false;
            }




    }*/

       /* private void btnBusquedaSucursal_Click(object sender, EventArgs e)
        {
            if (txtEmpresaId.Text == "") return;
             Controller.clsCtrlSucursales ctrlSucursales = new Controller.clsCtrlSucursales();
             ctrlSucursales.BuscarSucursalEmpresas(int.Parse(txtEmpresaId.Text));
             txtSucursalId.Text = ctrlSucursales.sucursal.intId.ToString();
             txtCodigoSucursal.Text = ctrlSucursales.sucursal.strCodigo;
             txtDescripcionSucursal.Text = ctrlSucursales.sucursal.strNombre;*/

           /* Controller.clsCtrlSucursales ctrlSucursales = new Controller.clsCtrlSucursales();
            ctrlSucursales.Paginacion();
            ctrlSucursales.CargarFrmBusqueda();
            txtSucursalId.Text = ctrlSucursales.sucursal.intId.ToString();
            txtDescripcionSucursal.Text = ctrlSucursales.sucursal.strNombre;
            txtCodigoSucursal.Text = ctrlSucursales.sucursal.strCodigo;*/

           /* this.bolSucursal = true;

            if (this.bolEmpresa)
            {
                btnVerificarHuella.Enabled = true;
            }
            else
            {
                btnVerificarHuella.Enabled = false;
            }

        }*/

      

        private void btnVerificarHuella_Click(object sender, EventArgs e)
        {
            Controller.clsCtrlUsuarios ctrlUsuarios = new Controller.clsCtrlUsuarios();
            ctrlUsuarios.Paginacion();


            VerificationForm frmVerificarHuella = new VerificationForm();
            frmVerificarHuella.objDataSet = ctrlUsuarios.objDataSet;

            frmVerificarHuella.ShowDialog();

            if (frmVerificarHuella.bolResultado)
            {
                
                ctrlUsuarios.BuscarHuella(frmVerificarHuella.objDataSetSingle);
                txtUsuarioId.Text = ctrlUsuarios.usuario.intId.ToString();
                txtUsuario.Text = ctrlUsuarios.usuario.strUsuario.ToString();
                txtContrasena.Text = ctrlUsuarios.usuario.strContrasena.ToString();

                Properties_Class.clsConstante clsCon = new Properties_Class.clsConstante();                

                this.ctrlLogin.limpiarProps();
                this.ctrlLogin.BuscarUsuario(txtUsuario.Text.TrimStart());

                if (ctrlLogin.bolResultado)
                {
                    if (ctrlLogin.login.usuario.byteIntentos == clsCon.intIntentos)
                    {
                        ctrlLogin.CambioEstatus();
                        MessageBox.Show("Su cuenta ha sido suspendida, comuníquese con el administrador", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtUsuarioId.Text = "";
                    }
                    else 
                    {
                        this.ctrlLogin.login.usuario.intId = int.Parse(ctrlUsuarios.usuario.intId.ToString());                        
                        /*this.ctrlLogin.login.intSucursalId = int.Parse(txtSucursalId.Text.ToString());
                        this.ctrlLogin.login.intEmpresaId = int.Parse(txtEmpresaId.Text.ToString());*/
                        this.ctrlLogin.login.usuario.strUsuario = txtUsuario.Text.ToString();
                        /*this.ctrlLogin.login.strSucursal = txtDescripcionSucursal.Text.ToString();
                        this.ctrlLogin.login.strEmpresa = txtDescripcionEmpresas.Text.ToString();*/
                        this.bolResultado = true;
                        /*this.ctrlLogin.BuscarPermisosAcessoUsuario();                        
                        if (this.ctrlLogin.bolResultado)
                        {
                            this.ctrlLogin.BuscarPermisosAcessoProcessoUsuario();
                            this.ctrlLogin.BuscarPermisosAcessoSubProcessoUsuario();
                        }
                        this.ctrlLogin.BuscarCaja();*/

                        this.Close();
                    }
                }

            }           
        }

        private void btnFooterAcceder_Click(object sender, EventArgs e)
        {
            if (validacion())
            {
                if (txtUsuarioId.Text != "")
                {
                    
                    
                }
                else 
                {

                    this.ctrlLogin = new Controller.clsCtrlLogin();                    
                    ctrlLogin.Login(txtUsuario.Text.TrimStart(), txtContrasena.Text.TrimStart());
                    Properties_Class.clsConstante clsCon = new Properties_Class.clsConstante();

                    if (this.ctrlLogin.bolResultado)
                    {

                        /*this.ctrlLogin.login.intSucursalId = int.Parse(txtSucursalId.Text.ToString());
                        this.ctrlLogin.login.intEmpresaId = int.Parse(txtEmpresaId.Text.ToString());*/

                        if (this.ctrlLogin.login.usuario.byteIntentos >= clsCon.intIntentos)
                        {
                            MessageBox.Show("Su cuenta ha sido suspendida, comuníquese con el administrador ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            ctrlLogin.CambioEstatus();
                        }
                        else
                        {

                            if (ctrlLogin.login.strEstatus == ctrlLogin.login.dicEstatus["ACTIVO"] && ctrlLogin.login.usuario.byteIntentos > 0) 
                            {

                                ctrlLogin.ReiniciarIntentos();
                            }

                            this.bolResultado = true;
                            this.ctrlLogin.login.usuario.strUsuario = txtUsuario.Text.ToString();
                            /*this.ctrlLogin.login.strSucursal = txtDescripcionSucursal.Text.ToString();
                            this.ctrlLogin.login.strEmpresa = txtDescripcionEmpresas.Text.ToString();
                            this.ctrlLogin.BuscarPermisosAcessoUsuario();                            
                            if (this.ctrlLogin.bolResultado) 
                            {
                                this.ctrlLogin.BuscarPermisosAcessoProcessoUsuario();
                                this.ctrlLogin.BuscarPermisosAcessoSubProcessoUsuario();
                            }

                            this.ctrlLogin.BuscarCaja();*/

                            this.Close();

                        }

                    }
                    else
                    {
                        
                       
                        ctrlLogin.limpiarProps();
                        ctrlLogin.BuscarUsuario(txtUsuario.Text.TrimStart());

                        if (ctrlLogin.bolResultado && ctrlLogin.login.usuario.strEstatus == ctrlLogin.login.dicEstatus["ACTIVO"])
                        {


                            ctrlLogin.login.usuario.byteIntentos += 1;


                            if (ctrlLogin.login.usuario.byteIntentos == clsCon.intIntentos)
                            {


                                ctrlLogin.CambioEstatus();
                                ctrlLogin.Incrementar();
                                MessageBox.Show("Se ha superado el número de intentos permitidos, comuníquese con el administrador ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            else
                            {
                                ctrlLogin.Incrementar();
                                MessageBox.Show("Usuario y contraseña son incorrectas, intenta de nuevo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                            


                        }
                        else 
                        {

                            MessageBox.Show("Su cuenta ha sido suspendida, comuníquese con el administrador ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                        
                            
                        
                    }
                                                            
                }

            }
            
        }


        private bool validacion()
        {

            bool bolValidacion = true;
            
            string strUsuario = txtUsuario.Text.TrimStart();
            string strContrasena = txtContrasena.Text.TrimStart();
            string strUsuarioId = txtUsuarioId.Text;
            /*string strEmpresaId = txtEmpresaId.Text; 
            string strSucursal = txtSucursalId.Text;*/

            if (strUsuario == "")
            {
                txtUsuario.Focus();
                this.clsView.principal.epMensaje.SetError(txtUsuario, this.clsView.mensajeErrorValiForm2("El campo usuario es obligatorio, favor de ingresar un usuario "));
                bolValidacion = false;
            }

            if (strContrasena == "")
            {
                txtContrasena.Focus();
                this.clsView.principal.epMensaje.SetError(txtContrasena, this.clsView.mensajeErrorValiForm2("El campo contraseña es obligatorio, favor de ingresar una contraseña "));
                bolValidacion = false;
            }

            /*if (strEmpresaId == "")
            {
                txtDescripcionEmpresas.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcionEmpresas, this.clsView.mensajeErrorValiForm2("El campo empresa es obligatorio, favor de ingresar una empresa "));
                bolValidacion = false;
            }



            if (strSucursal == "")
            {
                txtDescripcionSucursal.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcionSucursal, this.clsView.mensajeErrorValiForm2("El campo sucursal es obligatorio, favor de ingresar una sucursal "));
                bolValidacion = false;
            }*/


            return bolValidacion;
        }

        private void txtCodigoSucursal_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtSucursalId_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtDescripcionSucursal_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
