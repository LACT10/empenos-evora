﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmTiposIdentificacion : Form
    {
        
        public Controller.clsCtrlIdentificacionesTipo ctrlIdentificacionesTipo;
        public Core.clsCoreView clsView;

        public int _intUlitmoCodigo;
        public int _intNumCerosConsecutivo;
        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }

        public int intNumCerosConsecutivo { get => _intNumCerosConsecutivo; set => _intNumCerosConsecutivo = value; }

        public List<Properties_Class.clsIdentificacionesTipo> lsDeshacerIdentificacionesTipo;
        

        public frmTiposIdentificacion()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();
            this.ctrlIdentificacionesTipo = new Controller.clsCtrlIdentificacionesTipo();
            this.ctrlIdentificacionesTipo.Paginacion();
            this.lsDeshacerIdentificacionesTipo = new List<Properties_Class.clsIdentificacionesTipo>();
            this.lsDeshacerIdentificacionesTipo.Add(this.ctrlIdentificacionesTipo.identificacionTipo);
            //Asignar número de ceros pata generar código consecutivo
            this.intNumCerosConsecutivo = 2;
        }
        private void Frmcajas_Load(object sender, EventArgs e)
        {
        }

        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlIdentificacionesTipo.identificacionTipo.intId == 0) return;

            if (this.clsView.confirmacionEliminar("Tipo de identificacion"))
            {
                this.ctrlIdentificacionesTipo.Eliminar();
                if (this.ctrlIdentificacionesTipo.bolResultado)
                {
                    this.ctrlIdentificacionesTipo.Paginacion();
                    this.limpiar_textbox();
                    this.clsView.mensajeExitoDB("tipo de identificacion", "elimino");
                }
            }
        }

        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlIdentificacionesTipo.bolResutladoPaginacion)
            {
                txtCodigo.Text = this.lsDeshacerIdentificacionesTipo[0].strCodigo.ToString();
                txtDescripcion.Text = this.lsDeshacerIdentificacionesTipo[0].strDescripcion.ToString();
                txtDescripcionAnt.Text = this.lsDeshacerIdentificacionesTipo[0].strDescripcion.ToString();
                chkCurp.Checked = (this.lsDeshacerIdentificacionesTipo[0].strValidarCurp == "NO") ? false : true;
            }
        }

        private void tsbBuscar_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();

        }

        private void BtnBusqueda_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();
        }

        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlIdentificacionesTipo.PrimeroPaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlIdentificacionesTipo.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlIdentificacionesTipo.SiguentePaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlIdentificacionesTipo.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlIdentificacionesTipo.AtrasPaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlIdentificacionesTipo.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlIdentificacionesTipo.UltimoPaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlIdentificacionesTipo.bolResultado) return;
            this.llenar_formulario();
        }
        private void TsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();
            this.ctrlIdentificacionesTipo.identificacionTipo.limpiarProps();
            txtCodigo.Enabled = false;
            if (this.ctrlIdentificacionesTipo.bolResutladoPaginacion)
            {
                DataTable dtDatos = this.ctrlIdentificacionesTipo.objDataSet.Tables[0];
                this.intUlitmoCodigo = Int32.Parse(dtDatos.Rows[dtDatos.Rows.Count - 1][1].ToString()) + 1;
                this.agregarDeshacer();
            }
            else
            {
                this.intUlitmoCodigo = 1;
            }
            
            txtCodigo.Text = this.intUlitmoCodigo.ToString().PadLeft(this.intNumCerosConsecutivo, '0');

        }
        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {
            txtDescripcion.Text = "";
        }

        private void TxtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);

        }
        private void TxtCodigo_Leave(object sender, EventArgs e)
        {
            this.ctrlIdentificacionesTipo.Buscar(txtCodigo.Text);
            if (!(this.ctrlIdentificacionesTipo.bolResultado) || txtCodigo.Text.Trim().Length == 0) return;
            this.llenar_formulario();
        }

        private void txtDescripcion_Leave(object sender, EventArgs e)
        {
            if (txtDescripcion.Text != txtDescripcionAnt.Text)
            {
                this.ctrlIdentificacionesTipo.ValidarDescripcion(txtDescripcion.Text);
                if (this.ctrlIdentificacionesTipo.bolResultado)
                {
                    this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("La descripción debe ser única, favor de verificar"));
                    txtDescripcion.Text = "";
                }
            }
        }

        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            txtCodigo.Enabled = true;
            chkCurp.Enabled = true;
            chkCurp.Checked = false;
            txtDescripcionAnt.Text = "";

        }

        private bool validacion()
        {

            bool bolValidacion = true;
            string strCodigo = txtCodigo.Text.TrimStart();
            string strDescripcion = txtDescripcion.Text.TrimStart();

            if (strCodigo == "")
            {
                txtCodigo.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigo, this.clsView.mensajeErrorValiForm2("El campo código es obligatorio, favor de ingresar un código "));
                bolValidacion = false;
            }

            if (strDescripcion == "")
            {
                txtDescripcion.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("El campo descripción es obligatorio, favor de ingresar una descripción "));
                bolValidacion = false;
            }
            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();

            if (validacion())
            {
                this.ctrlIdentificacionesTipo.identificacionTipo.strCodigo = this.intUlitmoCodigo.ToString().PadLeft(this.intNumCerosConsecutivo, '0');
                this.ctrlIdentificacionesTipo.identificacionTipo.strDescripcion = txtDescripcion.Text.ToString();
                this.ctrlIdentificacionesTipo.identificacionTipo.strValidarCurp = (chkCurp.Checked)? "SI": "NO";
                this.ctrlIdentificacionesTipo.identificacionTipo.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlIdentificacionesTipo.Guardar();
                
                if (this.ctrlIdentificacionesTipo.mdlIdentificacionesTipo.bolResultado)
                {
                    this.limpiar_textbox();
                    this.ctrlIdentificacionesTipo.Paginacion();
                    this.clsView.mensajeExitoDB("tipo de identificacion", "guardó");

                }
            }
        }

        public void mostraBusqueda()
        {
            if (this.ctrlIdentificacionesTipo.bolResutladoPaginacion)
            {
                this.ctrlIdentificacionesTipo.CargarFrmBusqueda();

                if (this.ctrlIdentificacionesTipo.bolResultadoBusqueda)
                {
                    this.llenar_formulario();
                }
            }

            txtCodigo.Focus();
        }

        public void llenar_formulario()
        {
            if (this.ctrlIdentificacionesTipo.identificacionTipo.strCodigo.Length == 0) return;
            txtCodigo.Text = this.ctrlIdentificacionesTipo.identificacionTipo.strCodigo;
            txtDescripcion.Text = this.ctrlIdentificacionesTipo.identificacionTipo.strDescripcion;
            txtDescripcionAnt.Text = this.ctrlIdentificacionesTipo.identificacionTipo.strDescripcion;
            chkCurp.Checked = (this.ctrlIdentificacionesTipo.identificacionTipo.strValidarCurp == "NO")? false : true;
            this.intUlitmoCodigo = Int32.Parse(this.ctrlIdentificacionesTipo.identificacionTipo.strCodigo);


        }

        public void agregarDeshacer()
        {
            if (this.ctrlIdentificacionesTipo.bolResutladoPaginacion)
            {
                this.lsDeshacerIdentificacionesTipo.RemoveAt(0);
                this.lsDeshacerIdentificacionesTipo.Add(this.ctrlIdentificacionesTipo.identificacionTipo);
            }
        }

        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void FrmTiposIdentificacion_Load(object sender, EventArgs e)
        {
            txtCodigo.MaxLength = this.intNumCerosConsecutivo;

            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);
        }


    }
}