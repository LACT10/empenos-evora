﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlSubProcesos : Core.clsCoreModel
    {

        public clsMdlSubProcesos()
        {
        }

        public void buscar(string strId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " sp.subproceso_id, " +
                " sp.proceso_id,  " +
                " sp.descripcion, " +
                " sp.nombre_menu, " +
                " sp.descripcion AS `Descripción`, " +
                " sp.nombre_menu AS `Nombre de menú` " +
                " FROM subprocesos AS sp " +
            "WHERE sp.proceso_id = @id";
            this.connexion.mscCmd.Parameters.AddWithValue("@id", strId);
            this.resultadoObtener(this.connexion);
        }

        public void paginacion()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " sp.subproceso_id, " +
                " sp.proceso_id,  " +
                " sp.descripcion, " +
                " sp.nombre_menu, " +
                " sp.descripcion AS `Descripción`, " +
                " sp.nombre_menu AS `Nombre de menú` " +
                " FROM subprocesos AS sp ";                  
            this.resultadoObtener(this.connexion);
        }
    }
}
