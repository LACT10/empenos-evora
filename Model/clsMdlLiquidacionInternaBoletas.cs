﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora.Model
{
    public class clsMdlLiquidacionInternaBoletas : Core.clsCoreModel
    {

        public List<string> _lsFoliosGenerados;

        public List<string> lsFoliosGenerados { get => _lsFoliosGenerados; set => _lsFoliosGenerados = value; }
        public clsMdlLiquidacionInternaBoletas()
        {

        }
        public void guardar(Properties_Class.clsLiquidacionInternaBoletas internaBoletas) 
        {
            connexion.inicializarMSTA();
            try
            {
                string strId = "";
                string strQueryPrincipal = "";
                int intContador = 0;
                Controller.clsCtrlFolios ctrlFolios = new Controller.clsCtrlFolios();
                string strFolioPago = "";
                this.lsFoliosGenerados = new List<string>();
                ctrlFolios.BuscarFolioProceso(internaBoletas.intFolioIdPago.ToString(), internaBoletas.intSucursal.ToString());

                foreach (string row in internaBoletas.lsQuerysTraccionesPagos)
                {

                    strFolioPago = ctrlFolios.folio.regresarFolioConsecutivo(internaBoletas.pagos.intNumCerosConsecutivo);
                    this.lsFoliosGenerados.Add(strFolioPago);
                    strQueryPrincipal = row.Replace("@folio_pago", "'" + strFolioPago + "'" );
                    connexion.mscCmd.CommandText = strQueryPrincipal;

                    this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();

                    if (this.intRowsAffected > 0)
                    {

                        this.bolResultado = true;
                        strId = connexion.mscCmd.LastInsertedId.ToString();
                        this.intLastId = int.Parse(strId);
                    }
                        connexion.mscCmd.CommandText = "UPDATE folios " +
                                                       " SET " +
                                                       " consecutivo = (consecutivo + 1), " +
                                                       " fecha_actualizacion = '" + internaBoletas.dtFechaInterna.ToString("yyyy-MM-dd HH:mm:ss") + "', " +
                                                       " usuario_actualizacion =" + internaBoletas.intUsuario + " " +
                                                       " WHERE folio_id = " + ctrlFolios.folio.intId + "";
                        this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();
                        ctrlFolios.folio.intConsecutivo++;



                        connexion.mscCmd.CommandText = internaBoletas.lsQuerysTraccionesPagosDetalles[intContador].Replace("@pago_id", strId);
                        this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();

                        connexion.mscCmd.CommandText = internaBoletas.lsQuerysTraccionesPagosDetallesCobrosTipos[intContador].Replace("@pago_id", strId);
                        this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();

                        connexion.mscCmd.CommandText = internaBoletas.strQueryBoletaEmpenoEstatus;
                        this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();

                        //Incrementar contador
                        intContador++;

                }
                connexion.msTa.Commit();
                //Si existe filas, asignar resultado exitoso
                if (this.intRowsAffected > 0)
                {
                    this.bolResultado = true;
                }
                //Cerrar connexion                
                this.connexion.close_connection();

            }
            catch (MySqlException e) 
            {
                try
                {
                    connexion.msTa.Rollback();
                    this.intRowsAffected = 0;
                    this.bolResultado = false;
                    //Cerrar connexion         
                    this.connexion.close_connection();
                }
                catch (MySqlException ex)
                {
                    if (connexion.msTa.Connection != null)
                    {
                        /*Console.WriteLine("An exception of type " + ex.GetType() +
                        " was encountered while attempting to roll back the transaction.");*/

                    }
                    this.intRowsAffected = 0;
                    this.bolResultado = false;
                    //Cerrar connexion         
                    this.connexion.close_connection();
                    //Enviar mensaje de error
                    MessageBox.Show(ex.Message);
                }
                //Enviar mensaje de error
                MessageBox.Show(e.Message);
            }


              
        }

        public void paginacionDetallesBoletasActivasAdjudicada(int intSucursalId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT " +
                " be.boleta_empeno_id, " +
                " be.folio, " +
                "CONCAT(c.nombre, ' ', c.apellido_paterno, ' ', c.apellido_materno) AS  cliente_nombre, " +
                " DATE_FORMAT(be.fecha, '%d/%m/%Y') AS fecha," +
                " DATE_FORMAT(be.vencimiento, '%d/%m/%Y') AS fecha_vencimiento," +
                " IFNULL(sp.dias_cobro_minimo, 0) AS dias_cobro_minimo, " +
                " IFNULL(sp.dias_cobro_cuota_diaria, 0) AS dias_cobro_cuota_diaria, " +
                " IFNULL(sp.dias_cobrar_cuota_daria_especial, 0) AS dias_cobrar_cuota_daria_especial, " +
                " IFNULL(sp.dias_liquidar_boletas, 0) AS dias_liquidar_boletas, " +
                " IFNULL(sp.dias_refrendar_boletas, 0) AS dias_refrendar_boletas, " +
                " IFNULL(sp.dias_remate_prendas, 0) AS dias_remate_prendas," +
                " be.importe," +
                " be.prenda_tipo_id "+ 
                " FROM clientes AS c " +
                " INNER JOIN boletas_empeno  AS be" +
                " ON c.cliente_id = be.cliente_id " +               
                " LEFT JOIN sucursales_promociones_detalles  AS spd" +
                " ON be.prenda_tipo_id = spd.prenda_tipo_id " +                
                " LEFT JOIN sucursales_parametros  AS sp" +
                " ON sp.sucursal_id = be.sucursal_id " +
                " WHERE be.estatus = 'ACTIVO' " +
                " OR be.estatus = 'ADJUDICADA' " +
                " AND c.sucursal_id = @sucursal_id" +
                " ORDER BY be.folio ASC";
 
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", intSucursalId);
            this.resultadoObtener(this.connexion);
        }
    }
}
