﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlSucursalesCobrosTipos : Core.clsCoreModel
    {

        public clsMdlSucursalesCobrosTipos()
        {
        }


        public void buscar(string strSucursalId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " sct.sucursal_id," +
                " ct.cobro_tipo_id, " +
                " sct.porcentaje, " +
                " ct.codigo AS `Código tipo de cobro`, " +
                " ct.descripcion AS `Descripción tipo de cobro` " +
                                    "FROM sucursales_cobros_tipos AS sct " +
                                    "RIGHT JOIN cobros_tipos as ct " +
                                    "ON ct.cobro_tipo_id = sct.cobro_tipo_id AND sct.sucursal_id = @id ";
            this.connexion.mscCmd.Parameters.AddWithValue("@id", strSucursalId);
            this.resultadoObtener(this.connexion);
        }


        public void paginacion(int intId)
        {            
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " sct.sucursal_id," +
                " ct.cobro_tipo_id, " +
                " sct.porcentaje, " +
                " ct.descripcion, " +
                " CONCAT(ct.codigo , ' - ' ,  ct.descripcion) AS `Descripción tipo de cobro` " +                
                                    " FROM sucursales_cobros_tipos AS sct " +
                                    " RIGHT JOIN cobros_tipos as ct " +
                                    " ON ct.cobro_tipo_id = sct.cobro_tipo_id AND sct.sucursal_id = @sucursalId " +
                                    " WHERE ct.estatus = 'ACTIVO' ";
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursalId", intId);
            this.resultadoObtener(this.connexion);
        }

    }
}