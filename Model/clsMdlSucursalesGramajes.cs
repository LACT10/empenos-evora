﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlSucursalesGramajes : Core.clsCoreModel
    {

        public clsMdlSucursalesGramajes()
        {
        }


        public void buscar(string strSucursalId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT sg.sucursal_id,g.gramaje_id, sg.precio_tope, g.codigo AS `Código tipo de gramaje`, g.descripcion AS `Descripción tipo de gramaje` " +
                                    "FROM sucursales_gramajes AS sg " +
                                    "RIGHT JOIN gramajes as g " +
                                    "ON g.gramaje_id = sg.gramaje_id AND sg.sucursal_id = @id ";
            this.connexion.mscCmd.Parameters.AddWithValue("@id", strSucursalId);
            this.resultadoObtener(this.connexion);
        }




        public void paginacion(int intId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " sg.sucursal_id," +
                " g.gramaje_id, " +
                " sg.precio_tope, " +
                " CONCAT(g.codigo , ' - ' ,  g.descripcion) AS `Descripción tipo de gramaje` " +
                                    " FROM sucursales_gramajes AS sg " +
                                    " RIGHT JOIN gramajes as g " +
                                    " ON g.gramaje_id = sg.gramaje_id AND sg.sucursal_id = @sucursalId " +
                                    " WHERE g.estatus = 'ACTIVO' ";
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursalId", intId);
            this.resultadoObtener(this.connexion);
        }

        /*public void paginacion(int intId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " sg.sucursal_id," +
                " g.gramaje_id, " +
                " sg.precio_tope, " +
                " g.codigo AS `Código tipo de gramaje`, " +
                " g.descripcion AS `Descripción tipo de gramaje` " +
                                    " FROM sucursales_gramajes AS sg " +
                                    " RIGHT JOIN gramajes as g " +
                                    " ON g.gramaje_id = sg.gramaje_id AND sg.sucursal_id = @sucursalId " +
                                    " WHERE g.estatus = 'ACTIVO' ";
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursalId", intId);
            this.resultadoObtener(this.connexion);
        }*/
    }
}