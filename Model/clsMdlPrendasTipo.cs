﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlPrendasTipo : Core.clsCoreModel
    {
        public clsMdlPrendasTipo()
        {

        }

        public void guardar(Properties_Class.clsPrendasTipo prendasTipo)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (prendasTipo.intId > 0)
            {
                connexion.mscCmd.CommandText = "UPDATE prendas_tipos " +
                    "SET codigo = @codigo,  descripcion = @descripcion, capturar_gramaje = @capturarGramaje, estatus = @estatus, fecha_actualizacion  = @fecha, usuario_actualizacion = @usuario  WHERE prenda_tipo_id = @id";
            }
            else
            {
                connexion.mscCmd.CommandText = "INSERT INTO prendas_tipos(codigo, descripcion, capturar_gramaje, capturar_imei_celular, capturar_serie_vehiculo,estatus,fecha_creacion,usuario_creacion)" +
                    "VALUES(@codigo, @descripcion, @capturarGramaje, @capturarEmailCelular, @capturarSerieVehiculo, @estatus, @fecha, @usuario)";
            }
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", prendasTipo.intId);
            connexion.mscCmd.Parameters.AddWithValue("@codigo", prendasTipo.strCodigo);
            connexion.mscCmd.Parameters.AddWithValue("@descripcion", prendasTipo.strDescripcion);
            connexion.mscCmd.Parameters.AddWithValue("@capturarGramaje", prendasTipo.strCapturarGramaje);
            connexion.mscCmd.Parameters.AddWithValue("@capturarEmailCelular", prendasTipo.strCapturarEmailCelular);
            connexion.mscCmd.Parameters.AddWithValue("@capturarSerieVehiculo", prendasTipo.strCapturarSerieVehiculo);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", prendasTipo.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", prendasTipo.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", prendasTipo.intUsuario);
            //Regesar resultado
            this.resultadoGuardar(this.connexion);
        }

        public void eliminar(Properties_Class.clsPrendasTipo prendasTipo)
        {
            connexion.mscCmd.CommandText = "UPDATE prendas_tipos " +
                               "SET  estatus = @estatus, fecha_eliminacion  = @fecha, usuario_eliminacion = @usuario  WHERE prenda_tipo_id = @id";
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", prendasTipo.intId);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", prendasTipo.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", prendasTipo.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", prendasTipo.intUsuario);
            this.resultadoObtener(this.connexion);
        }


        public void buscar(string strCodigo)
        {
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " pt.prenda_tipo_id, " +
                " pt.codigo, " +
                " pt.descripcion, " +
                " pt.capturar_gramaje, " +
                " pt.capturar_imei_celular, " +
                " pt.capturar_serie_vehiculo, " +
                " pt.estatus FROM prendas_tipos AS pt WHERE pt.codigo = @codigo";
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", strCodigo);
            this.resultadoObtener(this.connexion);
        }

        public void validarDescripcion(string strDescripcion)
        {
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " pt.prenda_tipo_id, " +
                " pt.codigo, " +
                " pt.descripcion, " +
                " pt.capturar_gramaje, " +
                " pt.capturar_imei_celular, " +
                " pt.capturar_serie_vehiculo, " +
                " pt.estatus FROM prendas_tipos AS pt WHERE pt.descripcion = @descripcion";            
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", strDescripcion);
            this.resultadoObtener(this.connexion);
        }


        public void paginacion()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " pt.prenda_tipo_id, " +
                " pt.codigo, " +
                " pt.descripcion, " +
                " pt.capturar_gramaje, " +
                " pt.capturar_imei_celular, " +
                " pt.capturar_serie_vehiculo, " +
                " pt.estatus, " +
                " pt.codigo AS `Código`, " +
                " pt.descripcion AS `Descripción`, " +
                " pt.capturar_gramaje AS Gramaja " +
                " FROM prendas_tipos AS pt " +
                " ORDER BY pt.codigo ASC";
            this.resultadoObtener(this.connexion);
        }
    }
}
