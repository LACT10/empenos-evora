﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlEmpresas : Core.clsCoreModel
    {

        public clsMdlEmpresas()
        {
        }

        public void guardar(Properties_Class.clsEmpresa empresa)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (empresa.intId > 0)
            {
                connexion.mscCmd.CommandText = "UPDATE empresas " +
                    "SET razon_social = @razon_social,  nombre_comercial = @nombre_comercial, rfc = @rfc,  curp = @curp,estatus = @estatus, fecha_actualizacion  = @fecha, usuario_actualizacion = @usuario  WHERE empresa_id = @id";
            }
            else
            {
                connexion.mscCmd.CommandText = "INSERT INTO empresas( razon_social, nombre_comercial, rfc,curp, estatus,fecha_creacion,usuario_creacion)" +
                    "VALUES(@razon_social, @nombre_comercial, @rfc, @curp, @estatus, @fecha, @usuario)";
            }
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", empresa.intId);
            connexion.mscCmd.Parameters.AddWithValue("@razon_social", empresa.strRazonSocial);
            connexion.mscCmd.Parameters.AddWithValue("@nombre_comercial", empresa.strNombreComercial);
            connexion.mscCmd.Parameters.AddWithValue("@rfc", empresa.strRFC);
            connexion.mscCmd.Parameters.AddWithValue("@curp", empresa.strCURP);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", empresa.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", empresa.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", empresa.intUsuario);
            //Regesar resultado
            this.resultadoGuardar(this.connexion);
        }

        public void eliminar(Properties_Class.clsEmpresa empresas)
        {

            connexion.mscCmd.CommandText = "UPDATE empresas " +
                               "SET  estatus = @estatus, fecha_eliminacion  = @fecha, usuario_eliminacion = @usuario  WHERE empresa_id = @id";
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", empresas.intId);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", empresas.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", empresas.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", empresas.intUsuario);
            this.resultadoObtener(this.connexion);
        }


        public void buscar(string strBusqueda)
        {
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT e.empresa_id, e.razon_social, e.nombre_comercial, e.rfc, e.curp FROM empresas AS e WHERE e.codigo = @razonSocial";
            this.connexion.mscCmd.Parameters.AddWithValue("@razonSocial", strBusqueda);
            this.resultadoObtener(this.connexion);
        }

        public void validarComercialRFC(string strComercial, string strRFC)
        {

            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " e.empresa_id, " +
                " e.razon_social, " +
                " e.nombre_comercial, " +
                " e.rfc, " +
                " e.curp " +
                " FROM empresas AS e " +
                " WHERE e.nombre_comercial = @comercial" +
                " AND e.rfc = @rfc";
            this.connexion.mscCmd.Parameters.AddWithValue("@comercial", strComercial);
            this.connexion.mscCmd.Parameters.AddWithValue("@rfc", strRFC);
            this.resultadoObtener(this.connexion);


        }

        public void paginacionSucursal(int intSucursalId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " e.empresa_id, " +
                " e.razon_social, " +
                " e.nombre_comercial, " +
                " e.rfc, " +
                " e.curp," +
                " e.estatus, " +
                " e.razon_social AS `Razón social`, " +
                " e.nombre_comercial AS `Nombre comercial`, " +
                " e.rfc AS `RFC `,  " +
                " e.curp AS `CURP `" +
                " FROM empresas AS e " +
                " INNER JOIN sucursales as s " +
                " ON e.empresa_id = s.empresa_id " +
                " WHERE s.sucursal_id = @sucursalId";
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursalId", intSucursalId);
            this.resultadoObtener(this.connexion);
        }


        public void paginacion()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " e.empresa_id, " +
                " e.razon_social, " +
                " e.nombre_comercial, " +
                " e.rfc, " +
                " e.curp," +
                " e.estatus, " +
                " e.razon_social AS `Razón social`, " +
                " e.nombre_comercial AS `Nombre comercial`, " +
                " e.rfc AS `RFC `,  " +
                " e.curp AS `CURP `" +
                " FROM empresas AS e" +
                " ORDER BY e.nombre_comercial ASC ";
            this.resultadoObtener(this.connexion);
        }
    }
}

