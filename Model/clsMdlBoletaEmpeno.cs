﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlBoletaEmpeno : Core.clsCoreModel
    {

        public clsMdlBoletaEmpeno() {  }

       


        public void guardar(Properties_Class.clsBoletasEmpeno boletasEmpeno)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.lsQuerysTracciones = new List<string>();
            this.lsQuerysTraccionesID = new List<string>();
            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (boletasEmpeno.intId > 0)
            {
                connexion.mscCmd.CommandText = "UPDATE boletas_empeno " +
                    "SET  " +
                    " usuario_id = @usuario_id, " +
                    " fecha = @fechaBoleta, " +
                    " vencimiento = @vencimiento, " +                    
                    " cliente_id = @cliente_id, " +
                    " identificacion_tipo_id = @identificacion_tipo_id, " +
                    " prenda_tipo_id = @prenda_tipo_id, " +
                    " prenda_subtipo_id = @prenda_subtipo_id, " +
                    " gramaje_id = @gramaje_id, " +
                    " kilataje = @kilataje, " +
                    " gramos = @gramos, " +
                    " cantidad = @cantidad, " +
                    " numero_plazos = @numero_plazos, " +
                    " importe = @importe, " +
                    " importe_avaluo = @importeAvaluo, " +
                    " descripcion = @descripcion, " +
                    " comentario = @comentario, " +
                    " fotografia_imagen = @fotografia_imagen,   " +
                    " fecha_actualizacion = @fecha, " +
                    " usuario_actualizacion = @usuario, " +
                    " fecha_autorizacion = @fechaAutorizacion," +
                    " usuario_autorizacion =  @usuarioAutorizacion," +
                    " imei_celular = @imei_celular, " +
                    " tipo_serie_vehiculo = @tipo_serie_vehiculo, " +
                    " numero_serie_vehiculo =@numero_serie_vehiculo" +                     
                    "  WHERE boleta_empeno_id = @id" ;
            }
            else
            {

                //Modificar el consecutivo de folio
                this.lsQuerysTracciones.Add(boletasEmpeno.strQueryConsecutivoFolio);
                connexion.mscCmd.CommandText = " INSERT INTO boletas_empeno(sucursal_id, caja_id, usuario_id, folio, fecha, vencimiento,  cliente_id, identificacion_tipo_id, prenda_tipo_id, prenda_subtipo_id, gramaje_id, kilataje, gramos, cantidad, numero_plazos, importe, importe_avaluo, descripcion, comentario, imei_celular, tipo_serie_vehiculo, numero_serie_vehiculo, fotografia_imagen, estatus, fecha_creacion, usuario_creacion, fecha_autorizacion, usuario_autorizacion) " +
                    " VALUES(@sucursal_id, @caja_id, @usuario_id, @folio, @fechaBoleta,@vencimiento, @cliente_id, @identificacion_tipo_id, @prenda_tipo_id, @prenda_subtipo_id, @gramaje_id, @kilataje, @gramos, @cantidad, @numero_plazos, @importe, @importeAvaluo, @descripcion, @comentario, @imei_celular, @tipo_serie_vehiculo, @numero_serie_vehiculo,  @fotografia_imagen, @estatus, @fecha, @usuario, @fechaAutorizacion, @usuarioAutorizacion ) ";
                this.lsQuerysTraccionesID.Add("UPDATE boletas_empeno " +
                    "SET  " +
                    " boleta_inicial_id = @id " +
                     "  WHERE boleta_empeno_id = @id");
            }

            //Parametros de la tabla principal
            connexion.mscCmd.Parameters.AddWithValue("@id", boletasEmpeno.intId);
            connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", boletasEmpeno.intSucursal);
            connexion.mscCmd.Parameters.AddWithValue("@caja_id", boletasEmpeno.caja.intId);
            connexion.mscCmd.Parameters.AddWithValue("@usuario_id", boletasEmpeno.empleado.intId);
            connexion.mscCmd.Parameters.AddWithValue("@folio", boletasEmpeno.strFolio);
            connexion.mscCmd.Parameters.AddWithValue("@fechaBoleta", boletasEmpeno.dtFechaBoleta);
            connexion.mscCmd.Parameters.AddWithValue("@vencimiento", boletasEmpeno.dtFechaVencimiento);            
            connexion.mscCmd.Parameters.AddWithValue("@cliente_id", boletasEmpeno.cliente.intId);
            connexion.mscCmd.Parameters.AddWithValue("@identificacion_tipo_id", boletasEmpeno.identificacionesTipo.intId);
            connexion.mscCmd.Parameters.AddWithValue("@prenda_tipo_id", boletasEmpeno.prendaTipo.intId);
            connexion.mscCmd.Parameters.AddWithValue("@prenda_subtipo_id", boletasEmpeno.subTipoPrenda.intId);
            connexion.mscCmd.Parameters.AddWithValue("@gramaje_id", boletasEmpeno.gramaje.intId);
            connexion.mscCmd.Parameters.AddWithValue("@kilataje", boletasEmpeno.flKilataje);
            connexion.mscCmd.Parameters.AddWithValue("@gramos", boletasEmpeno.flGramos);
            connexion.mscCmd.Parameters.AddWithValue("@cantidad", boletasEmpeno.intCantidad);
            connexion.mscCmd.Parameters.AddWithValue("@numero_plazos", boletasEmpeno.intPlazo);
            connexion.mscCmd.Parameters.AddWithValue("@importe", boletasEmpeno.decPrestamo);
            connexion.mscCmd.Parameters.AddWithValue("@importeAvaluo", boletasEmpeno.decImporteAvaluo);            
            connexion.mscCmd.Parameters.AddWithValue("@descripcion", boletasEmpeno.strDescripcion);
            connexion.mscCmd.Parameters.AddWithValue("@comentario", boletasEmpeno.strComentario);            
            connexion.mscCmd.Parameters.AddWithValue("@imei_celular", boletasEmpeno.strIMEICelular);
            if (boletasEmpeno.strTipoSerieVehiculo == "")
            {
                connexion.mscCmd.Parameters.AddWithValue("@tipo_serie_vehiculo", null);
            }
            else 
            {
                connexion.mscCmd.Parameters.AddWithValue("@tipo_serie_vehiculo", boletasEmpeno.strTipoSerieVehiculo);
            }
            
            connexion.mscCmd.Parameters.AddWithValue("@numero_serie_vehiculo", boletasEmpeno.strNumeroSerieVehiculo);
            connexion.mscCmd.Parameters.AddWithValue("@fotografia_imagen", boletasEmpeno.strFotografiaImagen);            
            connexion.mscCmd.Parameters.AddWithValue("@estatus", boletasEmpeno.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", boletasEmpeno.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", boletasEmpeno.intUsuario);
            string strUsuarioAutorizacion = null;
         
            if (boletasEmpeno.usuarioAutorizacion.intId > 0)
            {
                strUsuarioAutorizacion = boletasEmpeno.usuarioAutorizacion.intId.ToString();                
                connexion.mscCmd.Parameters.AddWithValue("@fechaAutorizacion", boletasEmpeno.usuarioAutorizacion.dtFecha);
            }
            else 
            {                
                connexion.mscCmd.Parameters.AddWithValue("@fechaAutorizacion", null);
            }            

            connexion.mscCmd.Parameters.AddWithValue("@usuarioAutorizacion", strUsuarioAutorizacion);
            //Parametros para incrementar folio consecutivo de proceso
            connexion.mscCmd.Parameters.AddWithValue("@folio_id", boletasEmpeno.intFolioId);
            //Regesar resultado
            this.resultadoTransaccion(this.connexion);
            
        }
        public void cancelar(Properties_Class.clsBoletasEmpeno boletasEmpeno)
        {
            if (!this.bolResultadoConnexion) return;
            this.lsQuerysTracciones = new List<string>();

        
            this.connexion.mscCmd.CommandText = "UPDATE boletas_empeno " +
                " SET estatus = 'CANCELADA'," +
                " motivo_cancelacion = @motivo, " +
                " fecha_eliminacion = @fecha ," +
                " usuario_eliminacion = @usuario, " +
                " gramos = 0, " +
                " importe = 0," +
                " importe_avaluo = 0" +
                " WHERE boleta_empeno_id = @boleta_id";

            connexion.mscCmd.Parameters.AddWithValue("@motivo", boletasEmpeno.strMotivosCancelacion);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", boletasEmpeno.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", boletasEmpeno.intUsuario);
            connexion.mscCmd.Parameters.AddWithValue("@boleta_id", boletasEmpeno.intId);

            //Regesar resultado
            this.resultadoTransaccion(this.connexion);
        }

        public void reactivar(Properties_Class.clsBoletasEmpeno boletasEmpeno)
        {
            if (!this.bolResultadoConnexion) return;            

            this.connexion.mscCmd.CommandText = "UPDATE boletas_empeno " +
                " SET estatus = 'ACTIVO'," +
                " fecha_actualizacion = @fecha ," +
                " usuario_actualizacion = @usuario, " +
                " fecha_adjudicacion = null," +
                " usuario_adjudicacion = null" +
                " WHERE boleta_empeno_id = @boleta_id";            
            connexion.mscCmd.Parameters.AddWithValue("@fecha", boletasEmpeno.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", boletasEmpeno.intUsuario);
            connexion.mscCmd.Parameters.AddWithValue("@boleta_id", boletasEmpeno.intId);

            //Regesar resultado
            this.resultadoTransaccion(this.connexion);
        }
        public void buscar(string strBoleta)
        {

            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT  " +
                " be.boleta_empeno_id," +
                " be.folio," +
                " '' AS boleta_inicial, " +
                " be.fecha, " +
                "  DATE_FORMAT(be.fecha, '%l:%i:%s') AS hora, " +
                " be.vencimiento, " +
                " be.boleta_refrendo_id, " +
                " c.cliente_id, " +
                " c.codigo AS codigo_cliente, " +
                " CONCAT(c.nombre,  ' ',  c.apellido_paterno,  ' ',  c.apellido_materno) AS nombre_cliente, " +
                " e.usuario_id AS usuario_id, " +
                " CONCAT(e.nombre,  ' ',  e.apellido_paterno,  ' ',  e.apellido_materno) AS nombre_empleado, " +
                " it.identificacion_tipo_id, " +
                " it.codigo as codigo_identificacion, " +
                " it.descripcion as descripcion_identificacion, " +
                " pt.prenda_tipo_id, " +
                " pt.codigo AS codigo_prenda_tipo, " +
                " pt.descripcion AS descripcion_prenda_tipo, " +
                " pt.capturar_gramaje, " +
                " sp.plazo AS plazo_tope, " +
                " ps.prenda_subtipo_id, " +
                " ps.codigo AS codigo_prenda_subtipo, " +
                " ps.descripcion AS descripcion_prenda_subtipo, " +
                " g.gramaje_id, " +
                " g.codigo AS codigo_gramaje, " +
                " g.descripcion AS descripcion_gramaje, " +
                " sg.precio_tope, " +
                " be.kilataje, " +
                " be.gramos, " +
                " be.cantidad, " +
                " be.numero_plazos, " +
                " be.importe, " +
                " be.descripcion, " +
                " be.comentario, " +
                " be.fotografia_imagen, " +
                " be.estatus, " +
                " be.caja_id, " +
                " cj.codigo AS codigo_caja, " +
                " cj.descripcion AS descripcion_caja, " +
                " CONCAT(uc.nombre,  ' ',  uc.apellido_paterno,  ' ',  uc.apellido_materno) AS nombre_usuario_creacion, " +
                " CONCAT(ua.nombre,  ' ',  ua.apellido_paterno,  ' ',  ua.apellido_materno) AS nombre_usuario_actualizacion, " +
                " be.importe_avaluo, " +
                " bei.folio AS boleta_inicial," +
                " be.pago_refrendo_id," +
                " be.boleta_inicial_id," +
                " be.motivo_cancelacion, " +               
                " be.folio AS `Boleta `, " +
                " DATE_FORMAT(be.fecha, '%d/%m/%Y %r') AS `Fecha `, " +
                " CONCAT(c.nombre , ' ',c.apellido_paterno, ' ',c.apellido_materno) AS `Cliente`, " +
                " CONCAT('$', FORMAT(be.importe, 2))  AS `Préstamo`, " +
                " be.estatus AS `Estatus ` ," +
                " DATE_FORMAT(be.fecha, '%Y/%m/%d') AS fecha_busqueda," +
                " Pago.pago_folio ," +
                " Pago.pago_fecha, " +
                " beP.folio AS boleta_refrendo_folio, " +
                " beF.folio AS boleta_refrendada_folio," +
                " CONCAT(uE.nombre,  ' ',  uE.apellido_paterno,  ' ',  uE.apellido_materno) AS nombre_empleado_eliminacion, " +
                " DATE_FORMAT(be.fecha_eliminacion, '%d/%m/%Y %r') AS fecha_eliminacion," +               
                 " IFNULL((SELECT(SUM(pdcPPI.importe) - " +
                        " IFNULL((SELECT SUM(pdcBonif.importe) " +
                            " FROM pagos AS pBonif " +
                            " INNER JOIN pagos_detalles AS pdBonif " +
                            " ON pBonif.pago_id = pdBonif.pago_id " +
                            " INNER JOIN pagos_detalles_cobros_tipos AS pdcBonif " +
                            " ON pdBonif.pago_id = pdcBonif.pago_id " +
                            " WHERE pdBonif.boleta_empeno_id = be.boleta_empeno_id " +
                            " AND pdcBonif.tipo = 'BONIFICACION' AND pBonif.estatus = 'PAGO PARCIAL DE INTERESES' " +
                        " ), 0) " +
                    " ) AS ppi " +
                    " FROM pagos AS pPPI " +
                    " INNER JOIN pagos_detalles AS pdPPI " +
                    " ON pPPI.pago_id = pdPPI.pago_id " +
                    " INNER JOIN pagos_detalles_cobros_tipos AS pdcPPI " +
                    " ON pdPPI.pago_id = pdcPPI.pago_id " +
                    " WHERE pdPPI.boleta_empeno_id = be.boleta_empeno_id  " +
                    " AND pdcPPI.tipo = 'PPI' AND pPPI.estatus = 'PAGO PARCIAL DE INTERESES' ), 0) AS ppi, " +
                " be.imei_celular, " +
                " be.tipo_serie_vehiculo, " +
                " be.numero_serie_vehiculo" +
                " FROM boletas_empeno AS be " +
                " INNER JOIN clientes AS c " +
                " ON be.cliente_id = c.cliente_id" +
                " INNER JOIN identificaciones_tipos AS it " +
                " ON be.identificacion_tipo_id = it.identificacion_tipo_id " +
                " INNER JOIN prendas_tipos AS pt " +
                " ON be.prenda_tipo_id = pt.prenda_tipo_id " +
                " INNER JOIN sucursales_plazos AS sp" +
                " ON be.prenda_tipo_id = sp.prenda_tipo_id  AND sp.sucursal_id = be.sucursal_id" +
                " INNER JOIN prendas_subtipos AS ps " +
                " ON be.prenda_subtipo_id = ps.prenda_subtipo_id " +
                " INNER JOIN usuarios AS e " +
                " ON be.usuario_id = e.usuario_id " +
                " INNER JOIN cajas AS cj " +
                " ON be.caja_id = cj.caja_id " +
                " INNER JOIN usuarios AS uc " +
                " ON be.usuario_creacion = uc.usuario_id " +
                " INNER JOIN boletas_empeno AS bei " +
                " ON be.boleta_inicial_id = bei.boleta_empeno_id " +
                " LEFT JOIN gramajes AS g " +
                " ON be.gramaje_id = g.gramaje_id " +
                " LEFT JOIN sucursales_gramajes AS sg " +
                " ON be.gramaje_id = sg.gramaje_id AND  sg.sucursal_id = be.sucursal_id  " +
                " LEFT JOIN usuarios AS ua " +
                " ON be.usuario_actualizacion = ua.usuario_id " +

                 " LEFT JOIN(SELECT  pd.boleta_empeno_id AS referenciaID, p.folio AS pago_folio, " +
                         "DATE_FORMAT(p.fecha, '%d/%m/%Y') AS pago_fecha " +
                 " FROM  pagos_detalles AS pd " +
                 " INNER JOIN pagos AS p  ON pd.pago_id = p.pago_id " +
                 " WHERE p.estatus = 'ACTIVO'" +
                 " GROUP BY pd.boleta_empeno_id, p.folio, p.fecha) AS Pago ON Pago.referenciaID = be.boleta_empeno_id" +

                " LEFT JOIN boletas_empeno AS beP " +
                " ON be.boleta_refrendo_id = beP.boleta_empeno_id " +
                " LEFT JOIN boletas_empeno AS beF " +
                " ON be.boleta_empeno_id = beF.boleta_refrendo_id AND  beF.estatus = 'ACTIVO'  " +
                " LEFT JOIN usuarios AS uE " +
                " ON be.usuario_eliminacion = uE.usuario_id " +
                 " WHERE be.folio = @folio ";

            this.connexion.mscCmd.Parameters.AddWithValue("@folio", strBoleta);
            this.resultadoObtener(this.connexion);

        }


        public void buscarActivo(string strBoleta, string strSucursalId)
        {

            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT  " +
                " be.boleta_empeno_id," +
                " be.folio," +
                " '' AS boleta_inicial, " +
                " be.fecha, " +
                "  DATE_FORMAT(be.fecha, '%l:%i:%s') AS hora, " +
                " be.vencimiento, " +
                " be.boleta_refrendo_id, " +
                " c.cliente_id, " +
                " c.codigo AS codigo_cliente, " +
                " CONCAT(c.nombre,  ' ',  c.apellido_paterno,  ' ',  c.apellido_materno) AS nombre_cliente, " +
                " e.usuario_id AS usuario_id, " +
                " CONCAT(e.nombre,  ' ',  e.apellido_paterno,  ' ',  e.apellido_materno) AS nombre_empleado, " +
                " it.identificacion_tipo_id, " +
                " it.codigo as codigo_identificacion, " +
                " it.descripcion as descripcion_identificacion, " +
                " pt.prenda_tipo_id, " +
                " pt.codigo AS codigo_prenda_tipo, " +
                " pt.descripcion AS descripcion_prenda_tipo, " +
                " pt.capturar_gramaje, " +
                " sp.plazo AS plazo_tope, " +
                " ps.prenda_subtipo_id, " +
                " ps.codigo AS codigo_prenda_subtipo, " +
                " ps.descripcion AS descripcion_prenda_subtipo, " +
                " g.gramaje_id, " +
                " g.codigo AS codigo_gramaje, " +
                " g.descripcion AS descripcion_gramaje, " +
                " sg.precio_tope, " +
                " be.kilataje, " +
                " be.gramos, " +
                " be.cantidad, " +
                " be.numero_plazos, " +
                " be.importe, " +
                " be.descripcion, " +
                " be.comentario, " +
                " be.fotografia_imagen, " +
                " be.estatus, " +
                " be.caja_id, " +
                " cj.codigo AS codigo_caja, " +
                " cj.descripcion AS descripcion_caja, " +
                " CONCAT(uc.nombre,  ' ',  uc.apellido_paterno,  ' ',  uc.apellido_materno) AS nombre_usuario_creacion, " +
                " CONCAT(ua.nombre,  ' ',  ua.apellido_paterno,  ' ',  ua.apellido_materno) AS nombre_usuario_actualizacion, " +
                " be.importe_avaluo, " +
                " bei.folio AS boleta_inicial," +
                " be.pago_refrendo_id," +
                " be.boleta_inicial_id," +
                " be.motivo_cancelacion, " +
                " be.folio AS `Boleta `, " +
                " DATE_FORMAT(be.fecha, '%d/%m/%Y %r') AS `Fecha `, " +
                " CONCAT(c.nombre , ' ',c.apellido_paterno, ' ',c.apellido_materno) AS `Cliente`, " +
                " CONCAT('$', FORMAT(be.importe, 2))  AS `Préstamo`, " +
                " be.estatus AS `Estatus ` ," +
                " DATE_FORMAT(be.fecha, '%Y/%m/%d') AS fecha_busqueda," +
                " Pago.pago_folio ," +
                " Pago.pago_fecha, " +
                " beP.folio AS boleta_refrendo_folio, " +
                " beF.folio AS boleta_refrendada_folio," +
                " CONCAT(uE.nombre,  ' ',  uE.apellido_paterno,  ' ',  uE.apellido_materno) AS nombre_empleado_eliminacion, " +
                " DATE_FORMAT(be.fecha_eliminacion, '%d/%m/%Y %r') AS fecha_eliminacion," +
                 " IFNULL((SELECT(SUM(pdcPPI.importe) - " +
                        " IFNULL((SELECT SUM(pdcBonif.importe) " +
                            " FROM pagos AS pBonif " +
                            " INNER JOIN pagos_detalles AS pdBonif " +
                            " ON pBonif.pago_id = pdBonif.pago_id " +
                            " INNER JOIN pagos_detalles_cobros_tipos AS pdcBonif " +
                            " ON pdBonif.pago_id = pdcBonif.pago_id " +
                            " WHERE pdBonif.boleta_empeno_id = be.boleta_empeno_id " +
                            " AND pdcBonif.tipo = 'BONIFICACION' AND pBonif.estatus = 'PAGO PARCIAL DE INTERESES' " +
                        " ), 0) " +
                    " ) AS ppi " +
                    " FROM pagos AS pPPI " +
                    " INNER JOIN pagos_detalles AS pdPPI " +
                    " ON pPPI.pago_id = pdPPI.pago_id " +
                    " INNER JOIN pagos_detalles_cobros_tipos AS pdcPPI " +
                    " ON pdPPI.pago_id = pdcPPI.pago_id " +
                    " WHERE pdPPI.boleta_empeno_id = be.boleta_empeno_id  " +
                    " AND pdcPPI.tipo = 'PPI' AND pPPI.estatus = 'PAGO PARCIAL DE INTERESES' ), 0) AS ppi, " +
                " be.imei_celular, " +
                " be.tipo_serie_vehiculo, " +
                " be.numero_serie_vehiculo" +
                " FROM boletas_empeno AS be " +
                " INNER JOIN clientes AS c " +
                " ON be.cliente_id = c.cliente_id" +
                " INNER JOIN identificaciones_tipos AS it " +
                " ON be.identificacion_tipo_id = it.identificacion_tipo_id " +
                " INNER JOIN prendas_tipos AS pt " +
                " ON be.prenda_tipo_id = pt.prenda_tipo_id " +
                " INNER JOIN sucursales_plazos AS sp" +
                " ON be.prenda_tipo_id = sp.prenda_tipo_id  AND sp.sucursal_id = be.sucursal_id" +
                " INNER JOIN prendas_subtipos AS ps " +
                " ON be.prenda_subtipo_id = ps.prenda_subtipo_id " +
                " INNER JOIN usuarios AS e " +
                " ON be.usuario_id = e.usuario_id " +
                " INNER JOIN cajas AS cj " +
                " ON be.caja_id = cj.caja_id " +
                " INNER JOIN usuarios AS uc " +
                " ON be.usuario_creacion = uc.usuario_id " +
                " INNER JOIN boletas_empeno AS bei " +
                " ON be.boleta_inicial_id = bei.boleta_empeno_id " +
                " LEFT JOIN gramajes AS g " +
                " ON be.gramaje_id = g.gramaje_id " +
                " LEFT JOIN sucursales_gramajes AS sg " +
                " ON be.gramaje_id = sg.gramaje_id AND  sg.sucursal_id = be.sucursal_id  " +
                " LEFT JOIN usuarios AS ua " +
                " ON be.usuario_actualizacion = ua.usuario_id " +

                 " LEFT JOIN(SELECT  pd.boleta_empeno_id AS referenciaID, p.folio AS pago_folio, " +
                         "DATE_FORMAT(p.fecha, '%d/%m/%Y') AS pago_fecha " +
                 " FROM  pagos_detalles AS pd " +
                 " INNER JOIN pagos AS p  ON pd.pago_id = p.pago_id " +
                 " WHERE p.estatus = 'ACTIVO'" +
                 " GROUP BY pd.boleta_empeno_id, p.folio, p.fecha) AS Pago ON Pago.referenciaID = be.boleta_empeno_id" +

                " LEFT JOIN boletas_empeno AS beP " +
                " ON be.boleta_refrendo_id = beP.boleta_empeno_id " +
                " LEFT JOIN boletas_empeno AS beF " +
                " ON be.boleta_empeno_id = beF.boleta_refrendo_id AND  beF.estatus = 'ACTIVO'  " +
                " LEFT JOIN usuarios AS uE " +
                " ON be.usuario_eliminacion = uE.usuario_id " +
                " WHERE be.folio = @folio " +
                " AND be.sucursal_id = @sucursal_id" +
                " AND be.estatus = 'ACTIVO' ";

            this.connexion.mscCmd.Parameters.AddWithValue("@folio", strBoleta);
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", strSucursalId);
            this.resultadoObtener(this.connexion);

        }

        /*Metado que se utilzar para obtern boletas estatus con ACTIVO y ADJUDICADA. En caso que tiene un id de cliente entonces se obtner las boletas de dicha cliente. El metado se utlizar en la vista de Liquidadion interna boleta*/
        public void buscarActivoAdjudicada(string strBoleta, string strSucursalId)
        {

            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT  " +
             " be.boleta_empeno_id," +
                " be.folio," +
                " '' AS boleta_inicial, " +
                " be.fecha, " +
                "  DATE_FORMAT(be.fecha, '%l:%i:%s') AS hora, " +
                " be.vencimiento, " +
                " be.boleta_refrendo_id, " +
                " c.cliente_id, " +
                " c.codigo AS codigo_cliente, " +
                " CONCAT(c.nombre,  ' ',  c.apellido_paterno,  ' ',  c.apellido_materno) AS nombre_cliente, " +
                " e.usuario_id AS usuario_id, " +
                " CONCAT(e.nombre,  ' ',  e.apellido_paterno,  ' ',  e.apellido_materno) AS nombre_empleado, " +
                " it.identificacion_tipo_id, " +
                " it.codigo as codigo_identificacion, " +
                " it.descripcion as descripcion_identificacion, " +
                " pt.prenda_tipo_id, " +
                " pt.codigo AS codigo_prenda_tipo, " +
                " pt.descripcion AS descripcion_prenda_tipo, " +
                " pt.capturar_gramaje, " +
                " sp.plazo AS plazo_tope, " +
                " ps.prenda_subtipo_id, " +
                " ps.codigo AS codigo_prenda_subtipo, " +
                " ps.descripcion AS descripcion_prenda_subtipo, " +
                " g.gramaje_id, " +
                " g.codigo AS codigo_gramaje, " +
                " g.descripcion AS descripcion_gramaje, " +
                " sg.precio_tope, " +
                " be.kilataje, " +
                " be.gramos, " +
                " be.cantidad, " +
                " be.numero_plazos, " +
                " be.importe, " +
                " be.descripcion, " +
                " be.comentario, " +
                " be.fotografia_imagen, " +
                " be.estatus, " +
                " be.caja_id, " +
                " cj.codigo AS codigo_caja, " +
                " cj.descripcion AS descripcion_caja, " +
                " CONCAT(uc.nombre,  ' ',  uc.apellido_paterno,  ' ',  uc.apellido_materno) AS nombre_usuario_creacion, " +
                " CONCAT(ua.nombre,  ' ',  ua.apellido_paterno,  ' ',  ua.apellido_materno) AS nombre_usuario_actualizacion, " +
                " be.importe_avaluo, " +
                " bei.folio AS boleta_inicial," +
                " be.pago_refrendo_id," +
                " be.boleta_inicial_id," +
                " be.motivo_cancelacion, " +
                " be.folio AS `Boleta `, " +
                " DATE_FORMAT(be.fecha, '%d/%m/%Y %r') AS `Fecha `, " +
                " CONCAT(c.nombre , ' ',c.apellido_paterno, ' ',c.apellido_materno) AS `Cliente`, " +
                " CONCAT('$', FORMAT(be.importe, 2))  AS `Préstamo`, " +
                " be.estatus AS `Estatus ` ," +
                " DATE_FORMAT(be.fecha, '%Y/%m/%d') AS fecha_busqueda," +
                " Pago.pago_folio ," +
                " Pago.pago_fecha, " +
                " beP.folio AS boleta_refrendo_folio, " +
                " beF.folio AS boleta_refrendada_folio," +
                " CONCAT(uE.nombre,  ' ',  uE.apellido_paterno,  ' ',  uE.apellido_materno) AS nombre_empleado_eliminacion, " +
                " DATE_FORMAT(be.fecha_eliminacion, '%d/%m/%Y %r') AS fecha_eliminacion," +
                " be.imei_celular, " +
                " be.tipo_serie_vehiculo, " +
                " be.numero_serie_vehiculo" +
                " FROM boletas_empeno AS be " +
                " INNER JOIN clientes AS c " +
                " ON be.cliente_id = c.cliente_id" +
                " INNER JOIN identificaciones_tipos AS it " +
                " ON be.identificacion_tipo_id = it.identificacion_tipo_id " +
                " INNER JOIN prendas_tipos AS pt " +
                " ON be.prenda_tipo_id = pt.prenda_tipo_id " +
                " INNER JOIN sucursales_plazos AS sp" +
                " ON be.prenda_tipo_id = sp.prenda_tipo_id  AND sp.sucursal_id = be.sucursal_id" +
                " INNER JOIN prendas_subtipos AS ps " +
                " ON be.prenda_subtipo_id = ps.prenda_subtipo_id " +
                " INNER JOIN usuarios AS e " +
                " ON be.usuario_id = e.usuario_id " +
                " INNER JOIN cajas AS cj " +
                " ON be.caja_id = cj.caja_id " +
                " INNER JOIN usuarios AS uc " +
                " ON be.usuario_creacion = uc.usuario_id " +
                " INNER JOIN boletas_empeno AS bei " +
                " ON be.boleta_inicial_id = bei.boleta_empeno_id " +
                " LEFT JOIN gramajes AS g " +
                " ON be.gramaje_id = g.gramaje_id " +
                " LEFT JOIN sucursales_gramajes AS sg " +
                " ON be.gramaje_id = sg.gramaje_id AND  sg.sucursal_id = be.sucursal_id  " +
                " LEFT JOIN usuarios AS ua " +
                " ON be.usuario_actualizacion = ua.usuario_id " +
                " LEFT JOIN(SELECT  pd.boleta_empeno_id AS referenciaID, p.folio AS pago_folio, " +
                         "DATE_FORMAT(p.fecha, '%d/%m/%Y') AS pago_fecha " +
                 " FROM  pagos_detalles AS pd " +
                 " INNER JOIN pagos AS p  ON pd.pago_id = p.pago_id " +
                 " WHERE p.estatus = 'ACTIVO'" +
                 " GROUP BY pd.boleta_empeno_id, p.folio, p.fecha) AS Pago ON Pago.referenciaID = be.boleta_empeno_id" +
                " LEFT JOIN boletas_empeno AS beP " +
                " ON be.boleta_refrendo_id = beP.boleta_empeno_id " +
                " LEFT JOIN boletas_empeno AS beF " +
                " ON be.boleta_empeno_id = beF.boleta_refrendo_id AND  beF.estatus = 'ACTIVO'  " +
                " LEFT JOIN usuarios AS uE " +
                " ON be.usuario_eliminacion = uE.usuario_id " +
                " WHERE be.folio = @folio " +
                " AND be.sucursal_id = @sucursal_id" +
                " AND( be.estatus = 'ACTIVO' " +
                " OR be.estatus = 'ADJUDICADA')";

            this.connexion.mscCmd.Parameters.AddWithValue("@folio", strBoleta);
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", strSucursalId);
            this.resultadoObtener(this.connexion);

        }

        /*Metado que se utilzar para obtern boletas estatus con ACTIVO y ADJUDICADA. En caso que tiene un id de cliente entonces se obtner las boletas de dicha cliente. El metado se utlizar en la vista de Liquidadion interna boleta*/
        public void buscarActivoAdjudicadaCliente(string strBoleta, string strSucursalId, string strClientId)
        {

            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT  " +
             " be.boleta_empeno_id," +
                " be.folio," +
                " '' AS boleta_inicial, " +
                " be.fecha, " +
                "  DATE_FORMAT(be.fecha, '%l:%i:%s') AS hora, " +
                " be.vencimiento, " +
                " be.boleta_refrendo_id, " +
                " c.cliente_id, " +
                " c.codigo AS codigo_cliente, " +
                " CONCAT(c.nombre,  ' ',  c.apellido_paterno,  ' ',  c.apellido_materno) AS nombre_cliente, " +
                " e.usuario_id AS usuario_id, " +
                " CONCAT(e.nombre,  ' ',  e.apellido_paterno,  ' ',  e.apellido_materno) AS nombre_empleado, " +
                " it.identificacion_tipo_id, " +
                " it.codigo as codigo_identificacion, " +
                " it.descripcion as descripcion_identificacion, " +
                " pt.prenda_tipo_id, " +
                " pt.codigo AS codigo_prenda_tipo, " +
                " pt.descripcion AS descripcion_prenda_tipo, " +
                " pt.capturar_gramaje, " +
                " sp.plazo AS plazo_tope, " +
                " ps.prenda_subtipo_id, " +
                " ps.codigo AS codigo_prenda_subtipo, " +
                " ps.descripcion AS descripcion_prenda_subtipo, " +
                " g.gramaje_id, " +
                " g.codigo AS codigo_gramaje, " +
                " g.descripcion AS descripcion_gramaje, " +
                " sg.precio_tope, " +
                " be.kilataje, " +
                " be.gramos, " +
                " be.cantidad, " +
                " be.numero_plazos, " +
                " be.importe, " +
                " be.descripcion, " +
                " be.comentario, " +
                " be.fotografia_imagen, " +
                " be.estatus, " +
                " be.caja_id, " +
                " cj.codigo AS codigo_caja, " +
                " cj.descripcion AS descripcion_caja, " +
                " CONCAT(uc.nombre,  ' ',  uc.apellido_paterno,  ' ',  uc.apellido_materno) AS nombre_usuario_creacion, " +
                " CONCAT(ua.nombre,  ' ',  ua.apellido_paterno,  ' ',  ua.apellido_materno) AS nombre_usuario_actualizacion, " +
                " be.importe_avaluo, " +
                " bei.folio AS boleta_inicial," +
                " be.pago_refrendo_id," +
                " be.boleta_inicial_id," +
                " be.motivo_cancelacion, " +
                " be.folio AS `Boleta `, " +
                " DATE_FORMAT(be.fecha, '%d/%m/%Y %r') AS `Fecha `, " +
                " CONCAT(c.nombre , ' ',c.apellido_paterno, ' ',c.apellido_materno) AS `Cliente`, " +
                " CONCAT('$', FORMAT(be.importe, 2))  AS `Préstamo`, " +
                " be.estatus AS `Estatus ` ," +
                " DATE_FORMAT(be.fecha, '%Y/%m/%d') AS fecha_busqueda," +
                " Pago.pago_folio ," +
                " Pago.pago_fecha, " +
                " beP.folio AS boleta_refrendo_folio, " +
                " beF.folio AS boleta_refrendada_folio," +
                " CONCAT(uE.nombre,  ' ',  uE.apellido_paterno,  ' ',  uE.apellido_materno) AS nombre_empleado_eliminacion, " +
                " DATE_FORMAT(be.fecha_eliminacion, '%d/%m/%Y %r') AS fecha_eliminacion," +
                " be.imei_celular, " +
                " be.tipo_serie_vehiculo, " +
                " be.numero_serie_vehiculo" +
                " FROM boletas_empeno AS be " +
                " INNER JOIN clientes AS c " +
                " ON be.cliente_id = c.cliente_id" +
                " INNER JOIN identificaciones_tipos AS it " +
                " ON be.identificacion_tipo_id = it.identificacion_tipo_id " +
                " INNER JOIN prendas_tipos AS pt " +
                " ON be.prenda_tipo_id = pt.prenda_tipo_id " +
                " INNER JOIN sucursales_plazos AS sp" +
                " ON be.prenda_tipo_id = sp.prenda_tipo_id  AND sp.sucursal_id = be.sucursal_id" +
                " INNER JOIN prendas_subtipos AS ps " +
                " ON be.prenda_subtipo_id = ps.prenda_subtipo_id " +
                " INNER JOIN usuarios AS e " +
                " ON be.usuario_id = e.usuario_id " +
                " INNER JOIN cajas AS cj " +
                " ON be.caja_id = cj.caja_id " +
                " INNER JOIN usuarios AS uc " +
                " ON be.usuario_creacion = uc.usuario_id " +
                " INNER JOIN boletas_empeno AS bei " +
                " ON be.boleta_inicial_id = bei.boleta_empeno_id " +
                " LEFT JOIN gramajes AS g " +
                " ON be.gramaje_id = g.gramaje_id " +
                " LEFT JOIN sucursales_gramajes AS sg " +
                " ON be.gramaje_id = sg.gramaje_id AND  sg.sucursal_id = be.sucursal_id  " +
                " LEFT JOIN usuarios AS ua " +
                " ON be.usuario_actualizacion = ua.usuario_id " +
                " LEFT JOIN(SELECT  pd.boleta_empeno_id AS referenciaID, p.folio AS pago_folio, " +
                         "DATE_FORMAT(p.fecha, '%d/%m/%Y') AS pago_fecha " +
                 " FROM  pagos_detalles AS pd " +
                 " INNER JOIN pagos AS p  ON pd.pago_id = p.pago_id " +
                 " WHERE p.estatus = 'ACTIVO'" +
                 " GROUP BY pd.boleta_empeno_id, p.folio, p.fecha) AS Pago ON Pago.referenciaID = be.boleta_empeno_id" +
                " LEFT JOIN boletas_empeno AS beP " +
                " ON be.boleta_refrendo_id = beP.boleta_empeno_id " +
                " LEFT JOIN boletas_empeno AS beF " +
                " ON be.boleta_empeno_id = beF.boleta_refrendo_id AND  beF.estatus = 'ACTIVO'  " +
                " LEFT JOIN usuarios AS uE " +
                " ON be.usuario_eliminacion = uE.usuario_id " +
                " WHERE be.folio = @folio " +
                " AND be.sucursal_id = @sucursal_id" +
                " AND( be.estatus = 'ACTIVO' " +
                " OR be.estatus = 'ADJUDICADA')" +
                " AND be.cliente_id = @cliente_id";

            this.connexion.mscCmd.Parameters.AddWithValue("@folio", strBoleta);
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", strSucursalId);
            this.connexion.mscCmd.Parameters.AddWithValue("@cliente_id", strClientId);
            this.resultadoObtener(this.connexion);

        }



        public void getLastId()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT  " +
                " MAX(boleta_empeno_id) AS ultmio_boleta_empeno_id  " +
                " FROM boletas_empeno AS be ";

            this.resultadoObtener(this.connexion);
        }
        public void paginacionCliente(int intClienteId) 
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT " +
                 " be.folio ,  " +
                 " s.nombre ,  " +
                 " DATE_FORMAT(be.fecha, '%Y/%m/%d') AS fecha_busqueda, " +
                 " be.estatus ,  " +
                 " be.folio AS `Boleta `,  " +
                 " be.estatus AS `Estatus `, " +
                 " CONCAT(cj.codigo, ' - ', cj.descripcion) AS `Caja `, " +
                 " CONCAT(uc.nombre, ' ', uc.apellido_paterno, ' ', uc.apellido_materno) AS `Usuario `,  " +
                 " DATE_FORMAT(be.fecha, '%d/%m/%Y') AS `Empeño `,  " +
                 " DATE_FORMAT(be.fecha, '%l:%i:%s') AS `Hora `,  " +
                 " DATE_FORMAT(be.vencimiento, '%d/%m/%Y') AS `Vencimiento `,  " +
                 " CONCAT('$', FORMAT(be.importe, 2))  AS `Importe `,  " +
                 " CONCAT('$', FORMAT(be.importe_avaluo, 2))  AS `Avalúo `,  " +
                 " be.gramos AS `Gramos `, " +
                 " beF.folio AS `Refrendada`, " +
                 " beP.folio AS `Refrendó`, " +
                 " Pago.pago_folio AS `Recibo`, " +
                 " CONCAT('$', FORMAT(Pago.intereses, 2)) AS `Int.Cobrados`  " +
                 " FROM boletas_empeno AS be " +
                 " INNER JOIN sucursales AS s " +
                 " ON be.sucursal_id = s.sucursal_id " +
                 " INNER JOIN usuarios AS uc ON be.usuario_creacion = uc.usuario_id " +
                 " INNER JOIN cajas AS cj " +
                 " ON be.caja_id = cj.caja_id " +
                 " LEFT JOIN boletas_empeno AS beP " +
                 " ON be.boleta_refrendo_id = beP.boleta_empeno_id " +
                 " LEFT JOIN boletas_empeno AS beF " +
                 " ON be.boleta_empeno_id = beF.boleta_refrendo_id AND beF.estatus = 'ACTIVO' " +
                 " LEFT JOIN(SELECT " +
                          " pd.pago_id AS pago_id , " +
                          " pd.boleta_empeno_id AS referenciaID, " +
                          " p.folio AS pago_folio, " +
                          " DATE_FORMAT(p.fecha, '%d/%m/%Y') AS pago_fecha, " +
                          " SUM(pdct.importe) AS intereses " +
                 " FROM pagos_detalles AS pd " +
                 " INNER JOIN pagos AS p " +
                 " ON pd.pago_id = p.pago_id " +
                 " INNER JOIN pagos_detalles_cobros_tipos AS pdct" +
                 " ON p.pago_id = pdct.pago_id " +
                 " WHERE (p.estatus = 'ACTIVO' OR p.estatus = 'LIQUIDACION INTERNA')" +                 
                 " AND pdct.tipo = 'INTERESES' " +
                 " GROUP BY pd.boleta_empeno_id, p.folio, p.fecha, pd.pago_id " +
                " ) AS Pago ON Pago.referenciaID = be.boleta_empeno_id " +
                 " WHERE be.cliente_id = @cliente_id " +
                 " ORDER BY be.folio ASC; ";
            this.connexion.mscCmd.Parameters.AddWithValue("@cliente_id", intClienteId);
            this.resultadoObtener(this.connexion);
        }



        public void paginacion(int intSucursalId) 
        {

            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT  " +
                " be.boleta_empeno_id," +
                " be.folio,"+
                " '' AS boleta_inicial, "+
                " be.fecha, " +                
                "  DATE_FORMAT(be.fecha, '%l:%i:%s') AS hora, " +
                " be.vencimiento, " +
                " be.boleta_refrendo_id, " +
                " c.cliente_id, " +
                " c.codigo AS codigo_cliente, " +
                " CONCAT(c.nombre,  ' ',  c.apellido_paterno,  ' ',  c.apellido_materno) AS nombre_cliente, " +
                " e.usuario_id AS usuario_id, " +
                " CONCAT(e.nombre,  ' ',  e.apellido_paterno,  ' ',  e.apellido_materno) AS nombre_empleado, " +
                " it.identificacion_tipo_id, " +
                " it.codigo as codigo_identificacion, " +
                " it.descripcion as descripcion_identificacion, " +
                " pt.prenda_tipo_id, " +
                " pt.codigo AS codigo_prenda_tipo, " +
                " pt.descripcion AS descripcion_prenda_tipo, " +
                " pt.capturar_gramaje, " +
                " sp.plazo AS plazo_tope, " +
                " ps.prenda_subtipo_id, " +
                " ps.codigo AS codigo_prenda_subtipo, " +
                " ps.descripcion AS descripcion_prenda_subtipo, " +
                " g.gramaje_id, " +
                " g.codigo AS codigo_gramaje, " +
                " g.descripcion AS descripcion_gramaje, " +
                " sg.precio_tope, " +
                " be.kilataje, " +
                " be.gramos, " +
                " be.cantidad, " +
                " be.numero_plazos, " +
                " be.importe, " +
                " be.descripcion, " +
                " be.comentario, " +
                " be.fotografia_imagen, " +
                " be.estatus, " +
                " be.caja_id, " +
                " cj.codigo AS codigo_caja, " +
                " cj.descripcion AS descripcion_caja, " +                
                " CONCAT(uc.nombre,  ' ',  uc.apellido_paterno,  ' ',  uc.apellido_materno) AS nombre_usuario_creacion, " +                
                " CONCAT(ua.nombre,  ' ',  ua.apellido_paterno,  ' ',  ua.apellido_materno) AS nombre_usuario_actualizacion, " +
                " be.importe_avaluo, " +
                " bei.folio AS boleta_inicial," +
                " be.pago_refrendo_id," +
                " be.boleta_inicial_id," +
                " be.motivo_cancelacion, " +
                " be.folio AS `Boleta `, " +                
                " DATE_FORMAT(be.fecha, '%d/%m/%Y %r') AS `Fecha `, " +
                " CONCAT(c.nombre , ' ',c.apellido_paterno, ' ',c.apellido_materno) AS `Cliente`, " +
                " CONCAT('$', FORMAT(be.importe, 2))  AS `Préstamo`, " +
                " be.estatus AS `Estatus ` ," +
                " DATE_FORMAT(be.fecha, '%Y/%m/%d') AS fecha_busqueda," +
                " Pago.pago_folio ," +
                " Pago.pago_fecha, " +
                " beP.folio AS boleta_refrendo_folio, "  +
                " beF.folio AS boleta_refrendada_folio," +
                " CONCAT(uE.nombre,  ' ',  uE.apellido_paterno,  ' ',  uE.apellido_materno) AS nombre_empleado_eliminacion, " +
                " DATE_FORMAT(be.fecha_eliminacion, '%d/%m/%Y %r') AS fecha_eliminacion, " +
                 " IFNULL((SELECT(SUM(pdcPPI.importe) - " +
                        " IFNULL((SELECT SUM(pdcBonif.importe) " +
                            " FROM pagos AS pBonif " +
                            " INNER JOIN pagos_detalles AS pdBonif " +
                            " ON pBonif.pago_id = pdBonif.pago_id " +
                            " INNER JOIN pagos_detalles_cobros_tipos AS pdcBonif " +
                            " ON pdBonif.pago_id = pdcBonif.pago_id " +
                            " WHERE pdBonif.boleta_empeno_id = be.boleta_empeno_id " +
                            " AND pdcBonif.tipo = 'BONIFICACION' AND pBonif.estatus = 'PAGO PARCIAL DE INTERESES' " +
                        " ), 0) " +
                    " ) AS ppi " +
                    " FROM pagos AS pPPI " +
                    " INNER JOIN pagos_detalles AS pdPPI " +
                    " ON pPPI.pago_id = pdPPI.pago_id " +
                    " INNER JOIN pagos_detalles_cobros_tipos AS pdcPPI " +
                    " ON pdPPI.pago_id = pdcPPI.pago_id " +
                    " WHERE pdPPI.boleta_empeno_id = be.boleta_empeno_id  " +
                    " AND pdcPPI.tipo = 'PPI' AND pPPI.estatus = 'PAGO PARCIAL DE INTERESES' ), 0) AS ppi," +
                " be.imei_celular, " +
                " be.tipo_serie_vehiculo, " +
                " be.numero_serie_vehiculo " +
                " FROM boletas_empeno AS be " +
                " INNER JOIN clientes AS c " +
                " ON be.cliente_id = c.cliente_id" +
                " INNER JOIN identificaciones_tipos AS it " +
                " ON be.identificacion_tipo_id = it.identificacion_tipo_id " +
                " INNER JOIN prendas_tipos AS pt " +
                " ON be.prenda_tipo_id = pt.prenda_tipo_id " +
                " INNER JOIN sucursales_plazos AS sp" +
                " ON be.prenda_tipo_id = sp.prenda_tipo_id  AND sp.sucursal_id = be.sucursal_id" +
                " INNER JOIN prendas_subtipos AS ps " +
                " ON be.prenda_subtipo_id = ps.prenda_subtipo_id " +
                " INNER JOIN usuarios AS e " +
                " ON be.usuario_id = e.usuario_id " +
                " INNER JOIN cajas AS cj " +
                " ON be.caja_id = cj.caja_id " +
                " INNER JOIN usuarios AS uc " +
                " ON be.usuario_creacion = uc.usuario_id " +
                " INNER JOIN boletas_empeno AS bei " +
                " ON be.boleta_inicial_id = bei.boleta_empeno_id " +
                " LEFT JOIN gramajes AS g " +
                " ON be.gramaje_id = g.gramaje_id " +
                " LEFT JOIN sucursales_gramajes AS sg " +
                " ON be.gramaje_id = sg.gramaje_id AND  sg.sucursal_id = be.sucursal_id  " +
                " LEFT JOIN usuarios AS ua " +
                " ON be.usuario_actualizacion = ua.usuario_id " +

                 " LEFT JOIN(SELECT  pd.boleta_empeno_id AS referenciaID, p.folio AS pago_folio, " +
                         "DATE_FORMAT(p.fecha, '%d/%m/%Y') AS pago_fecha " +
                 " FROM  pagos_detalles AS pd " +
                 " INNER JOIN pagos AS p  ON pd.pago_id = p.pago_id " +
                 " WHERE p.estatus = 'ACTIVO'" +
                 " GROUP BY pd.boleta_empeno_id, p.folio, p.fecha) AS Pago ON Pago.referenciaID = be.boleta_empeno_id" +

                " LEFT JOIN boletas_empeno AS beP " +
                " ON be.boleta_refrendo_id = beP.boleta_empeno_id " +
                " LEFT JOIN boletas_empeno AS beF " +
                " ON be.boleta_empeno_id = beF.boleta_refrendo_id AND  beF.estatus = 'ACTIVO' " +
                " LEFT JOIN usuarios AS uE " +
                " ON be.usuario_eliminacion = uE.usuario_id " +
                " WHERE be.sucursal_id = @sucursal_id " +
                " ORDER BY be.folio ASC";

            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", intSucursalId);
            this.resultadoObtener(this.connexion);

        }
    }
}