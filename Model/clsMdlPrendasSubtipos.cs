﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlPrendasSubtipos : Core.clsCoreModel
    {

        public clsMdlPrendasSubtipos()
        {
        }

        public void guardar(Properties_Class.clsPrendasSubtipo prendasSubtipo)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (prendasSubtipo.intId > 0)
            {
                connexion.mscCmd.CommandText = "UPDATE prendas_subtipos " +
                    "SET codigo = @codigo,  prenda_tipo_id = @prendaTipoId ,descripcion = @descripcion, estatus = @estatus, fecha_actualizacion  = @fecha, usuario_actualizacion = @usuario  " +
                    "WHERE prenda_subtipo_id = @id";
            }
            else
            {
                connexion.mscCmd.CommandText = "INSERT INTO prendas_subtipos(codigo, prenda_tipo_id,descripcion,estatus,fecha_creacion,usuario_creacion)" +
                    "VALUES(@codigo, @prendaTipoId,@descripcion, @estatus, @fecha, @usuario)";
            }
            //Asignar valores al commando
            this.connexion.mscCmd.Parameters.AddWithValue("@id", prendasSubtipo.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", prendasSubtipo.strCodigo);
            this.connexion.mscCmd.Parameters.AddWithValue("@prendaTipoId", prendasSubtipo.prendasTipo.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", prendasSubtipo.strDescripcion);
            this.connexion.mscCmd.Parameters.AddWithValue("@estatus", prendasSubtipo.strEstatus);
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha", prendasSubtipo.dtFecha);
            this.connexion.mscCmd.Parameters.AddWithValue("@usuario", prendasSubtipo.intUsuario);
            //Regesar resultado
            this.resultadoGuardar(this.connexion);


        }

        public void eliminar(Properties_Class.clsPrendasSubtipo prendasSubtipo)
        {

            this.connexion.mscCmd.CommandText = "UPDATE prendas_subtipos " +
                "SET  estatus = @estatus, fecha_eliminacion  = @fecha, usuario_eliminacion = @usuario  " +
                "WHERE prenda_subtipo_id = @id";
            this.connexion.mscCmd.Parameters.AddWithValue("@id", prendasSubtipo.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@estatus", prendasSubtipo.strEstatus);
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha", prendasSubtipo.dtFecha);
            this.connexion.mscCmd.Parameters.AddWithValue("@usuario", prendasSubtipo.intUsuario);
            this.resultadoGuardar(this.connexion);
        }

        public void buscar(string strCodigo)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT m.prenda_subtipo_id, " +
                "m.codigo, " +
                "m.descripcion, " +
                "m.estatus, " +
                "e.prenda_tipo_id, " +
                "e.codigo AS prendaTipoCodigo, " +
                "e.descripcion AS prendaTipoDescripcion " +
                "FROM prendas_subtipos AS m " +
                "INNER JOIN prendas_tipos as e " +
                "ON m.prenda_tipo_id = e.prenda_tipo_id " +
                "WHERE m.codigo = @codigo";
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", strCodigo);
            this.resultadoObtener(this.connexion);
        }

        public void buscarPrendaTipoId(string strCodigo, string strPrendaTipoId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT " +
                " m.prenda_subtipo_id, " +
                " m.codigo, " +
                " m.descripcion, " +
                " m.estatus, " +
                " e.prenda_tipo_id, " +
                " e.codigo AS prendaTipoCodigo, " +
                " e.descripcion AS prendaTipoDescripcion " +
                " FROM prendas_subtipos AS m " +
                " INNER JOIN prendas_tipos as e " +
                " ON m.prenda_tipo_id = e.prenda_tipo_id " +
                " WHERE m.codigo = @codigo" +
                " AND m.prenda_tipo_id = @prendaTipoId";
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", strCodigo);
            this.connexion.mscCmd.Parameters.AddWithValue("@prendaTipoId", strPrendaTipoId);
            this.resultadoObtener(this.connexion);
        }

        public void paginacionPrendaTipoId(string strPrendaTipoId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " m.prenda_subtipo_id, " +
                " m.codigo, " +
                " m.descripcion, " +
                " m.estatus, " +
                " e.prenda_tipo_id, " +
                " e.codigo AS prendaTipoCodigo, " +
                " e.descripcion AS prendaDescripcion, " +
                " m.codigo AS  `Código`, " +
                " m.descripcion AS `Descripción`, " +
                " e.codigo AS  `Código tipo de prenda`, " +
                " e.descripcion AS `Descripción tipo de prenda`" +
                " FROM prendas_subtipos AS m " +
                " INNER JOIN prendas_tipos as e " +
                " ON m.prenda_tipo_id = e.prenda_tipo_id " +
                " WHERE m.prenda_tipo_id = @prendaTipoId";
            this.connexion.mscCmd.Parameters.AddWithValue("@prendaTipoId", strPrendaTipoId);
            this.resultadoObtener(this.connexion);
        }

        public void paginacion()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                  " m.prenda_subtipo_id, " +
                  " m.codigo, " +
                  " m.descripcion, " +
                  " m.estatus, " +
                  " e.prenda_tipo_id, " +
                  " e.codigo AS prendaTipoCodigo, " +
                  " e.descripcion AS prendaDescripcion, " +
                  " m.codigo AS  `Código`, " +
                  " m.descripcion AS `Descripción`, " +
                  " e.codigo AS  `Código tipo de prenda`, " +
                  " e.descripcion AS `Descripción tipo de prenda`" +
                  "FROM prendas_subtipos AS m " +
                  "INNER JOIN prendas_tipos as e " +
                  "ON m.prenda_tipo_id = e.prenda_tipo_id " +
                  " ORDER BY m.codigo ASC";
            this.resultadoObtener(this.connexion);
        }
    }
}
