﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora.Model
{
   public class clsMdlRecibosLiquidacionRefrendos : Core.clsCoreModel
    {


        public void guardar(Properties_Class.clsRecibosLiquidacionRefrendos refrendos)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.lsQuerysTracciones = new List<string>();
            this.lsQuerysTraccionesID = new List<string>();

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (refrendos.intId > 0)
            {

            }
            else
            {
                //Modificar el consecutivo de folio
                this.lsQuerysTracciones.Add(refrendos.strQueryConsecutivoFolio);
                
                connexion.mscCmd.CommandText = " INSERT INTO pagos( sucursal_id,caja_id, usuario_id, folio, fecha, cliente_id, forma_pago_id, tipo_recibo, fotografia_imagen, total_pagar, importe_pagado, estatus,fecha_creacion,usuario_creacion)" +
                    " VALUES( @sucursal_id, @caja_id, @usuario_id, @folio, @fecha_pago, @cliente_id, @forma_pago_id, @tipo_recibo, @fotografia_imagen, @total_pagar, @importe_pagado, @estatus, @fecha, @usuario) ";
                
                
                if (refrendos.strTipoRecibo == "LIQUIDACION")
                {

                    this.lsQuerysTracciones.Add(refrendos.strQueryBoletaEmpenoEstatus);
                    this.lsQuerysTraccionesID.Add(refrendos.strQueryPagosDetalles);
                    

                }
                else if (refrendos.strTipoRecibo == "REFRENDO")
                {

                    this.lsQuerysTracciones.Add(refrendos.strQueryBoletaEmpenoEstatus);
                    this.lsQuerysTracciones.Add(refrendos.strQueryConsecutivoFolio);
                    
                }
                else if (refrendos.strTipoRecibo == "PARCIAL") 
                {
                    this.lsQuerysTraccionesID.Add(refrendos.strQueryPagosDetalles);
                    this.lsQuerysTracciones.Add(refrendos.strQueryBoletaEmpenoFechaVencimiento);

                }

                this.lsQuerysTraccionesID.Add(refrendos.strQueryPagosDetallesCobrosTipos);
            }


            

            connexion.mscCmd.Parameters.AddWithValue("@id", refrendos.intId);
            connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", refrendos.intSucursal);
            connexion.mscCmd.Parameters.AddWithValue("@caja_id", refrendos.caja.intId);
            connexion.mscCmd.Parameters.AddWithValue("@usuario_id", refrendos.empleado.intId);
            connexion.mscCmd.Parameters.AddWithValue("@folio", refrendos.strFolio);
            connexion.mscCmd.Parameters.AddWithValue("@fecha_pago", refrendos.dtFechaPago);
            connexion.mscCmd.Parameters.AddWithValue("@cliente_id", refrendos.cliente.intId);
            
            connexion.mscCmd.Parameters.AddWithValue("@tipo_recibo", refrendos.strTipoRecibo);
            connexion.mscCmd.Parameters.AddWithValue("@fotografia_imagen", refrendos.strFotografiaImagen);            
            connexion.mscCmd.Parameters.AddWithValue("@total_pagar", refrendos.decTotalPagar);
            connexion.mscCmd.Parameters.AddWithValue("@importe_pagado", refrendos.decImportePagado);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", refrendos.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", refrendos.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", refrendos.intUsuario);

            //Parametros para incrementar folio consecutivo de proceso
            connexion.mscCmd.Parameters.AddWithValue("@folio_id", refrendos.intFolioId);
            

            if (refrendos.formPago.intId > 0)
            {
                connexion.mscCmd.Parameters.AddWithValue("@forma_pago_id", refrendos.formPago.intId);
            }
            else 
            {
                connexion.mscCmd.Parameters.AddWithValue("@forma_pago_id", null);
            }


            if (refrendos.strTipoRecibo == "LIQUIDACION")
            {
                //Regesar resultado
                this.resultadoTransaccion(this.connexion);
            }
            else if (refrendos.strTipoRecibo == "REFRENDO")
            {
                //Regesar resultado
                this.guardarRefrendos(this.connexion, refrendos);
            }
            else if (refrendos.strTipoRecibo == "PARCIAL")
            {
                //Regesar resultado
                this.resultadoTransaccion(this.connexion);
            }
            
            


        }

        public void cancelar(Properties_Class.clsRecibosLiquidacionRefrendos refrendos) 
        {
            if (!this.bolResultadoConnexion) return;
            this.lsQuerysTracciones = new List<string>();

            this.lsQuerysTracciones.Add(refrendos.strQueryActualizarBoletasDetalles);
            this.lsQuerysTracciones.Add(refrendos.strQueryActualizarBoletasGeneradas);
            this.connexion.mscCmd.CommandText = "UPDATE pagos " +
                " SET estatus = 'CANCELADO'," +
                " motivo_cancelacion = @motivo, " +
                " fecha_eliminacion = @fecha ," +
                " usuario_eliminacion = @usuario" +
                " WHERE pago_id = @pago_id ";

            connexion.mscCmd.Parameters.AddWithValue("@motivo", refrendos.strMotivosCancelacion);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", refrendos.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", refrendos.intUsuario);
            connexion.mscCmd.Parameters.AddWithValue("@pago_id", refrendos.intId);

            //Regesar resultado
            this.resultadoTransaccion(this.connexion);
        }

        public void buscar(string strFolio)
        {

            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT  " +
                " p.pago_id, " +
                " p.folio, " +
                " p.fecha, " +
                " DATE_FORMAT(p.fecha, '%l:%i:%s') AS hora, " +
                " c.cliente_id, " +
                " c.codigo AS codigo_cliente, " +
                " CONCAT(c.nombre,  ' ',  c.apellido_paterno,  ' ',  c.apellido_materno) AS nombre_cliente, " +
                " e.usuario_id AS usuario_id, " +
                " CONCAT(e.nombre,  ' ',  e.apellido_paterno,  ' ',  e.apellido_materno) AS nombre_empleado, " +
                " cj.caja_id, " +
                " cj.codigo AS codigo_caja, " +
                " cj.descripcion AS descripcion_caja, " +
                " fp.forma_pago_id, " +
                " fp.codigo AS codigo_forma_pago, " +
                " fp.descripcion AS descripcion_forma_pago, " +
                " p.tipo_recibo," +
                " p.fotografia_imagen, " +
                " p.motivo_cancelacion, " +
                " p.estatus," +
                " DATE_FORMAT(p.fecha, '%Y/%m/%d') AS fecha_busqueda, " +
                " p.folio AS `Folio `, " +
                " DATE_FORMAT(p.fecha, '%d/%m/%Y %r') AS `Fecha `, " +
                " CONCAT(c.nombre , ' ',c.apellido_paterno, ' ',c.apellido_materno) AS `Cliente`, " +
                " p.estatus AS `Estatus ` ," +
                " (SELECT  GROUP_CONCAT(ber.folio ORDER BY ber.folio SEPARATOR ',') AS folios " +
                " FROM pagos_detalles AS pd " +
                " INNER JOIN boletas_empeno AS be " +
                " ON pd.boleta_empeno_id = be.boleta_empeno_id " +
                " INNER JOIN boletas_empeno AS ber " +
                " ON pd.boleta_empeno_id = ber.boleta_refrendo_id  AND ber.pago_refrendo_id = pd.pago_id " +
                " WHERE pd.pago_id = p.pago_id " +
                " GROUP BY pd.pago_id" +
                " ) AS lista_boletas_generadas," +
                 " (SELECT  GROUP_CONCAT(ber.boleta_empeno_id SEPARATOR ',')  " +
                " FROM pagos_detalles AS pd " +
                " INNER JOIN boletas_empeno AS be " +
                " ON pd.boleta_empeno_id = be.boleta_empeno_id " +
                " INNER JOIN boletas_empeno AS ber " +
                " ON pd.boleta_empeno_id = ber.boleta_refrendo_id  AND ber.pago_refrendo_id = pd.pago_id " +
                " WHERE pd.pago_id = p.pago_id " +
                " GROUP BY pd.pago_id) AS ids_boletas_generadas," +
                " (SELECT  GROUP_CONCAT(be.boleta_empeno_id SEPARATOR ',')  " +
                " FROM pagos_detalles AS pd " +
                " INNER JOIN boletas_empeno AS be " +
                " ON pd.boleta_empeno_id = be.boleta_empeno_id " +
                " WHERE pd.pago_id = p.pago_id " +
                " GROUP BY pd.pago_id) AS ids_boletas_detalles," +
                " CONCAT(uE.nombre,  ' ',  uE.apellido_paterno,  ' ',  uE.apellido_materno) AS nombre_empleado_eliminacion, " +
                " DATE_FORMAT(p.fecha_eliminacion, '%d/%m/%Y %r') AS fecha_eliminacion ," +
                " p.total_pagar, " +
                " p.importe_pagado "  +
                " FROM pagos AS p " +
                " INNER JOIN clientes AS c " +
                " ON p.cliente_id = c.cliente_id" +
                " INNER JOIN usuarios AS e " +
                " ON p.usuario_id = e.usuario_id " +
                " INNER JOIN cajas AS cj " +
                " ON p.caja_id = cj.caja_id " +
                " LEFT JOIN formas_pago AS fp " +
                " ON p.forma_pago_id = fp.forma_pago_id " +
                " LEFT JOIN usuarios AS uE " +
                " ON p.usuario_eliminacion = uE.usuario_id " +
                " WHERE p.folio = @folio ";                

            this.connexion.mscCmd.Parameters.AddWithValue("@folio", strFolio);
            this.resultadoObtener(this.connexion);

        }

        public void buscarClientesBoletasActivas(string strCodigo, int intSucursalId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT " +
                " c.cliente_id, " +
                " c.codigo, " +
                " CONCAT(c.nombre, ' ', c.apellido_paterno, ' ', c.apellido_materno) AS  nombre " +
                " FROM clientes AS c " +
                " INNER JOIN boletas_empeno  AS be" +
                " ON c.cliente_id = be.cliente_id " +
                " WHERE be.sucursal_id = @sucursalId " +
                " AND c.codigo = @codigo" +
                " AND be.estatus = 'ACTIVO' " +                
                " LIMIT 1 ";

            this.connexion.mscCmd.Parameters.AddWithValue("@sucursalId", intSucursalId);
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", strCodigo);
            this.resultadoObtener(this.connexion);
        }

        public void buscarPagosDetalles(int intPagosId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT " +
                " pd.pago_id," +
                " pd.renglon ," +
                " pd.boleta_empeno_id ," +                
                " IFNULL(pd.abono_capital, 0) AS abono_capital ," +
                "  IFNULL(pd.dias_intereses, 0) AS dias_intereses  ," +
                " pd.semanas_ppi," +
                " pd.fecha_autorizacion," +
                " pd.usuario_autorizacion," +
                " pd.motivo_autorizacion," +
                " pd.porcentaje_promocion," +
                " pd.dia_promocion," +
                " pd.prenda_tipo_id," +
                " IFNULL((SELECT SUM(pdc.importe) " +
                " FROM pagos_detalles_cobros_tipos AS pdc " +
                " WHERE pdc.pago_id = pd.pago_id " +
                " AND pdc.renglon_detalle = pd.renglon" +
                " AND pdc.tipo = 'INTERESES'), 0) AS intereses," +
                " be.folio AS boleta_folio," +
                " DATE_FORMAT(be.fecha, '%d/%m/%Y') AS fecha," +
                " DATE_FORMAT(be.vencimiento, '%d/%m/%Y') AS fecha_vencimiento," +
                " be.importe, " +
                " IFNULL((SELECT SUM(pdc.importe) " +
                " FROM pagos_detalles_cobros_tipos AS pdc " +
                " WHERE pdc.pago_id = pd.pago_id " +
                " AND pdc.renglon_detalle = pd.renglon" +
                " AND pdc.tipo = 'BONIFICACION'), 0) AS bonificaciones," +
                " IFNULL((SELECT SUM(pdc.importe) " +
                " FROM pagos_detalles_cobros_tipos AS pdc " +
                " WHERE pdc.pago_id = pd.pago_id " +
                " AND pdc.renglon_detalle = pd.renglon" +
                " AND pdc.tipo = 'PPI'), 0) AS intereses_ppi," +
                " IFNULL(pd.meses_intereses, 0) AS meses_intereses" +
                " FROM pagos_detalles AS pd " +
                " INNER JOIN boletas_empeno  AS be" +
                " ON pd.boleta_empeno_id = be.boleta_empeno_id " +
                " WHERE pd.pago_id = @pagosId ";
              
                
                ;

            this.connexion.mscCmd.Parameters.AddWithValue("@pagosId", intPagosId);            
            this.resultadoObtener(this.connexion);
        }


        public void paginacionCliente(int intClienteId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT  " +

                " be.folio , " +
                " s.nombre , " +
                " DATE_FORMAT(be.fecha, '%Y/%m/%d') AS fecha_busqueda, " +
                " be.estatus , " +
                " be.folio AS `Boleta `, " +
                " DATE_FORMAT(be.fecha, '%d/%m/%Y %r') AS `Fecha `, " +
                " CONCAT('$', FORMAT(be.importe, 2))  AS `Préstamo`, " +
                " s.nombre AS `Sucursal`, " +
                " be.estatus AS `Estatus `" +
                " FROM boletas_empeno AS be " +
                " INNER JOIN sucursales AS s" +
                " ON be.sucursal_id = s.sucursal_id " +
                " WHERE be.cliente_id = @cliente_id " +
                " ORDER BY be.folio ASC";

            this.connexion.mscCmd.Parameters.AddWithValue("@cliente_id", intClienteId);
            this.resultadoObtener(this.connexion);
        }

        public void paginacionDetallesBoletasPago(int intPagoId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT " +
                "pd.pago_id," +
                "pd.renglon ," +
                "pd.boleta_empeno_id ," +                                
                "pd.abono_capital ," +
                "pd.dias_intereses ," +
                "pd.semanas_ppi," +
                "pd.fecha_autorizacion," +
                "pd.usuario_autorizacion" +
                " FROM pagos_detalles AS pd " +
                " INNER JOIN boletas_empeno AS be" +
                " ON pd.boleta_empeno_id = be.boleta_empeno_id " +
                " WHERE pd.pago_id = @pagoId" +
                " AND be.estatus = 'ACTIVO' " +
                " ORDER BY be.folio ASC";

            this.connexion.mscCmd.Parameters.AddWithValue("@pagoId", intPagoId);
            this.resultadoObtener(this.connexion);
        }

        public void paginacionDetallesBoletasActivas(int intClienteId, string strDia, int intSucursalId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT " +
                " be.boleta_empeno_id, " +
                " be.folio, " +
                " DATE_FORMAT(be.fecha, '%d/%m/%Y') AS fecha,"+
                " DATE_FORMAT(be.vencimiento, '%d/%m/%Y') AS fecha_vencimiento," +                
                " IFNULL(sp.dias_cobro_minimo, 0) AS dias_cobro_minimo, " +
                " IFNULL(sp.dias_cobro_cuota_diaria, 0) AS dias_cobro_cuota_diaria, " +
                " IFNULL(sp.dias_cobrar_cuota_daria_especial, 0) AS dias_cobrar_cuota_daria_especial, " +
                " IFNULL(sp.dias_liquidar_boletas, 0) AS dias_liquidar_boletas, " +
                " IFNULL(sp.dias_refrendar_boletas, 0) AS dias_refrendar_boletas, " +
                " IFNULL(sp.dias_remate_prendas, 0) AS dias_remate_prendas," +
                " be.importe," +
                " be.prenda_tipo_id, " +
                  " IFNULL((SELECT(SUM(pdcPPI.importe) - " +
                        " IFNULL((SELECT SUM(pdcBonif.importe) " +
                            " FROM pagos AS pBonif " +
                            " INNER JOIN pagos_detalles AS pdBonif " +
                            " ON pBonif.pago_id = pdBonif.pago_id " +
                            " INNER JOIN pagos_detalles_cobros_tipos AS pdcBonif " +
                            " ON pdBonif.pago_id = pdcBonif.pago_id " +
                            " WHERE pdBonif.boleta_empeno_id = be.boleta_empeno_id " +
                            " AND pdcBonif.tipo = 'BONIFICACION' AND pBonif.estatus = 'PAGO PARCIAL DE INTERESES' " +
                        " ), 0) " +
                    " ) AS ppi " +
                    " FROM pagos AS pPPI " +
                    " INNER JOIN pagos_detalles AS pdPPI " +
                    " ON pPPI.pago_id = pdPPI.pago_id " +
                    " INNER JOIN pagos_detalles_cobros_tipos AS pdcPPI " +
                    " ON pdPPI.pago_id = pdcPPI.pago_id " +
                    " WHERE pdPPI.boleta_empeno_id = be.boleta_empeno_id  " +
                    " AND pdcPPI.tipo = 'PPI' AND pPPI.estatus = 'PAGO PARCIAL DE INTERESES' ), 0) AS ppi," +
                    "IFNULL(sprom.porcentaje, 0) AS porcentaje_promocion " +
                " FROM clientes AS c " +
                " INNER JOIN boletas_empeno  AS be" +
                " ON c.cliente_id = be.cliente_id " +
                " LEFT JOIN sucursales_promociones_detalles  AS spd" +
                " ON be.prenda_tipo_id = spd.prenda_tipo_id " +
                " LEFT JOIN sucursales_promociones  AS sprom" +
                " ON spd.sucursal_promocion_id = sprom.sucursal_promocion_id  AND sprom.dia = @dia AND sprom.sucursal_id = @sucursal_id AND sprom.estatus = 'ACTIVO'" +
                " LEFT JOIN sucursales_parametros  AS sp" +
                " ON sp.sucursal_id = be.sucursal_id " +
                " WHERE be.cliente_id = @clienteId" +
                " AND be.estatus = 'ACTIVO' " +
                " AND c.sucursal_id = @sucursal_id" +               
                " ORDER BY be.folio ASC";

            this.connexion.mscCmd.Parameters.AddWithValue("@clienteId", intClienteId);
            this.connexion.mscCmd.Parameters.AddWithValue("@dia", strDia);
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", intSucursalId);
            this.resultadoObtener(this.connexion);
        }

        public void paginacionBoletaActivasCliente(int intSucursalId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT " +
                " be.cliente_id, " +
                " c.codigo," +
                " CONCAT(c.nombre, ' ', c.apellido_paterno, ' ', c.apellido_materno) AS  nombre, " +
                " ch.huella, " +
                " c.codigo AS `Codigo `, " +
                " CONCAT(c.nombre, ' ', c.apellido_paterno, ' ', c.apellido_materno) AS  `Nombre ` " +
                " FROM clientes AS c " +
                " INNER JOIN boletas_empeno  AS be" +
                " ON c.cliente_id = be.cliente_id " +
                " INNER JOIN clientes_huellas  AS ch" +
                " ON ch.cliente_id = c.cliente_id " +
                " WHERE be.sucursal_id = @sucursalId" +
                " AND be.estatus = 'ACTIVO' " +
                " GROUP BY be.cliente_id, c.codigo,c.nombre, c.apellido_paterno, c.apellido_materno, ch.huella" +
                " ORDER BY c.codigo ASC";

            this.connexion.mscCmd.Parameters.AddWithValue("@sucursalId", intSucursalId);
            this.resultadoObtener(this.connexion);
        }

        public void paginacion(int intSucursalId)
        {

            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT  " +
                " p.pago_id, " +                                                
                " p.folio, " +
                " p.fecha, " +
                " DATE_FORMAT(p.fecha, '%l:%i:%s') AS hora, " +                
                " c.cliente_id, " +
                " c.codigo AS codigo_cliente, " +
                " CONCAT(c.nombre,  ' ',  c.apellido_paterno,  ' ',  c.apellido_materno) AS nombre_cliente, " +
                " e.usuario_id AS usuario_id, " +
                " CONCAT(e.nombre,  ' ',  e.apellido_paterno,  ' ',  e.apellido_materno) AS nombre_empleado, " +
                " cj.caja_id, " +
                " cj.codigo AS codigo_caja, " +
                " cj.descripcion AS descripcion_caja, " +
                " fp.forma_pago_id, " +
                " fp.codigo AS codigo_forma_pago, " +
                " fp.descripcion AS descripcion_forma_pago, " +
                " p.tipo_recibo," +
                " p.fotografia_imagen, " +
                " p.motivo_cancelacion, " +
                " p.estatus," +
                " DATE_FORMAT(p.fecha, '%Y/%m/%d') AS fecha_busqueda, " +
                " p.folio AS `Folio `, " +
                " DATE_FORMAT(p.fecha, '%d/%m/%Y %r') AS `Fecha `, " +
                " CONCAT(c.nombre , ' ',c.apellido_paterno, ' ',c.apellido_materno) AS `Cliente`, " +
                " p.estatus AS `Estatus ` ," +
                " (SELECT  GROUP_CONCAT(ber.folio ORDER BY ber.folio SEPARATOR ',') AS folios " +
                " FROM pagos_detalles AS pd " + 
                " INNER JOIN boletas_empeno AS be " +
                " ON pd.boleta_empeno_id = be.boleta_empeno_id " +
                " INNER JOIN boletas_empeno AS ber " +
                " ON pd.boleta_empeno_id = ber.boleta_refrendo_id  AND ber.pago_refrendo_id = pd.pago_id" +
                " WHERE pd.pago_id = p.pago_id " +
                " GROUP BY pd.pago_id" +
                " ) AS lista_boletas_generadas," +
                " (SELECT  GROUP_CONCAT(ber.boleta_empeno_id SEPARATOR ',')  " +
                " FROM pagos_detalles AS pd " +
                " INNER JOIN boletas_empeno AS be " +
                " ON pd.boleta_empeno_id = be.boleta_empeno_id " +
                " INNER JOIN boletas_empeno AS ber " +
                " ON pd.boleta_empeno_id = ber.boleta_refrendo_id  AND ber.pago_refrendo_id = pd.pago_id " +
                " WHERE pd.pago_id = p.pago_id " +
                " GROUP BY pd.pago_id) AS ids_boletas_generadas," +
                " (SELECT  GROUP_CONCAT(be.boleta_empeno_id SEPARATOR ',')  " +
                " FROM pagos_detalles AS pd " +
                " INNER JOIN boletas_empeno AS be " +
                " ON pd.boleta_empeno_id = be.boleta_empeno_id " +                
                " WHERE pd.pago_id = p.pago_id " +
                " GROUP BY pd.pago_id) AS ids_boletas_detalles," +
                " CONCAT(uE.nombre,  ' ',  uE.apellido_paterno,  ' ',  uE.apellido_materno) AS nombre_empleado_eliminacion, " +
                " DATE_FORMAT(p.fecha_eliminacion, '%d/%m/%Y %r') AS fecha_eliminacion ," +
                " p.total_pagar, " +
                " p.importe_pagado " +
                " FROM pagos AS p " +
                " INNER JOIN clientes AS c " +
                " ON p.cliente_id = c.cliente_id" +
                " INNER JOIN usuarios AS e " +
                " ON p.usuario_id = e.usuario_id " +
                " INNER JOIN cajas AS cj " +
                " ON p.caja_id = cj.caja_id " +
                " LEFT JOIN formas_pago AS fp " +
                " ON p.forma_pago_id = fp.forma_pago_id " +
                " LEFT JOIN usuarios AS uE " +
                " ON p.usuario_eliminacion = uE.usuario_id " +
                " WHERE p.sucursal_id = @sucursal_id " +
                " ORDER BY p.folio ASC";

            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", intSucursalId);
            this.resultadoObtener(this.connexion);

        }


        public void guardarRefrendos(db.clsDBConnection connexion, Properties_Class.clsRecibosLiquidacionRefrendos refrendos) 
        {
            connexion.inicializarMSTA();
            try
            {
                this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();

                string strId = "";
                //Si existe filas, asignar resultado exitoso
                if (this.intRowsAffected > 0)
                {
                    this.bolResultado = true;
                    strId = connexion.mscCmd.LastInsertedId.ToString();
                    this.intLastId = int.Parse(strId);

                }

                Controller.clsCtrlFolios ctrlFolios = new Controller.clsCtrlFolios();
                ctrlFolios.BuscarFolioProceso(refrendos.intFolioIdBoleta.ToString(), refrendos.intSucursal.ToString());
                ctrlFolios.folio.regresarFolioConsecutivo(10);
                for (int i = 0; i < refrendos.intCountFolioBoleta; i++) 
                {

                    refrendos.strQueryCrearBoletaEmpeno = refrendos.strQueryCrearBoletaEmpeno.Replace("@folio" + i, ctrlFolios.folio.regresarFolioConsecutivo(10)).Replace("@id", strId);                    
                    connexion.mscCmd.CommandText = "UPDATE folios " +
                    " SET " +
                    " consecutivo = (consecutivo + 1), " +
                    " fecha_actualizacion = '"+refrendos.dtFecha.ToString("yyyy-MM-dd HH:mm:ss") +"', " +
                    " usuario_actualizacion ="+refrendos.intUsuario + " " +
                    " WHERE folio_id = "+ ctrlFolios.folio.intId + "";
                    this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();

                    ctrlFolios.folio.intConsecutivo++;
                }

                connexion.mscCmd.CommandText = refrendos.strQueryCrearBoletaEmpeno;
                this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();
                string strIdBoleta = connexion.mscCmd.LastInsertedId.ToString();

                connexion.mscCmd.CommandText = refrendos.strQueryPagosDetalles.Replace("@id", strId).Replace("@boleta_empeno_id", strIdBoleta);
                this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();

                if (!(this.lsQuerysTraccionesID == null))
                {
                    this.lsQuerysTraccionesID.ForEach(delegate (string strQuery)
                    {
                        connexion.mscCmd.CommandText = strQuery.Replace("@id", strId);
                        if (!(strQuery == "")) this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();
                    });
                }

                if (!(this.lsQuerysTracciones == null))
                {
                    //Funcione que instancia la transaccion                
                    this.lsQuerysTracciones.ForEach(delegate (string strQuery)
                    {
                        connexion.mscCmd.CommandText = strQuery;
                        if (!(strQuery == "")) this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();
                    });
                }
              
                connexion.msTa.Commit();
                //Si existe filas, asignar resultado exitoso
                if (this.intRowsAffected > 0)
                {
                    this.bolResultado = true;
                }
                //Cerrar connexion                
                this.connexion.close_connection();

            }
            catch (MySqlException e)
            {
                try
                {
                    connexion.msTa.Rollback();
                    this.intRowsAffected = 0;
                    this.bolResultado = false;
                    //Cerrar connexion         
                    this.connexion.close_connection();
                }
                catch (MySqlException ex)
                {
                    if (connexion.msTa.Connection != null)
                    {
                        /*Console.WriteLine("An exception of type " + ex.GetType() +
                        " was encountered while attempting to roll back the transaction.");*/

                    }
                    this.intRowsAffected = 0;
                    this.bolResultado = false;
                    //Cerrar connexion         
                    this.connexion.close_connection();
                    //Enviar mensaje de error
                    MessageBox.Show(ex.Message);
                }
                //Enviar mensaje de error
                MessageBox.Show(e.Message);
            }
        }
    }
}
