﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
   public class clsMdlCajasUsuario : Core.clsCoreModel
    {

        public clsMdlCajasUsuario() 
        { 
        }


        public void buscarCodigo(int intUsuarioId, int intSucursalId, string strCodigo) 
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT " +                
                " c.caja_id, " +
                " c.descripcion ," +
                " c.codigo " +                
                " FROM usuarios_cajas AS uc " +
                " INNER JOIN cajas as c" +
                " ON c.caja_id = uc.caja_id AND uc.usuario_id = @usuarioId  AND uc.sucursal_id = @sucursalId" +
                " WHERE c.estatus = 'ACTIVO' AND c.codigo = @codigo";
            this.connexion.mscCmd.Parameters.AddWithValue("@usuarioId", intUsuarioId);
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursalId", intSucursalId);
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", strCodigo);

            this.resultadoObtener(this.connexion);
        }

        public void paginacionSucursalUsario(int intId, int intSucursalId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT " +
                " uc.usuario_id, " +
                " c.caja_id, " +
                " c.descripcion ," +
                " c.codigo," +
                " c.codigo AS `Código`, " +
                " c.descripcion AS `Descripción`" +
                " FROM usuarios_cajas AS uc " +
                " RIGHT JOIN cajas as c" +
                " ON c.caja_id = uc.caja_id AND uc.usuario_id = @usuarioId  AND uc.sucursal_id = @sucursalId" +
                " WHERE c.estatus = 'ACTIVO'";
            this.connexion.mscCmd.Parameters.AddWithValue("@usuarioId", intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursalId", intSucursalId);

            this.resultadoObtener(this.connexion);
        }
        public void paginacion(int intId, int intSucursalId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT " +                
                " c.caja_id, " +
                " c.descripcion ," +
                " c.codigo," +
                " c.codigo AS `Código`, " +
                " c.descripcion AS `Descripción`" +
                " FROM usuarios_cajas AS uc " +
                " INNER JOIN cajas as c" +
                " ON c.caja_id = uc.caja_id AND uc.usuario_id = @usuarioId  AND uc.sucursal_id = @sucursalId" +
                " WHERE c.estatus = 'ACTIVO'" +
                " ORDER BY c.codigo ASC";
            this.connexion.mscCmd.Parameters.AddWithValue("@usuarioId", intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursalId", intSucursalId);
            
            this.resultadoObtener(this.connexion);
        }
    }
}
