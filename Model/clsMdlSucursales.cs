﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlSucursales : Core.clsCoreModel
    {

        public clsMdlSucursales()
        {
        }

        public void guardar(Properties_Class.clsSucursal sucursal)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            this.lsQuerysTracciones = new List<string>();
            this.lsQuerysTraccionesID = new List<string>();
            this.lsQuerysTraccionesElminacionID = new List<string>();

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (sucursal.intId > 0)
            {
                this.connexion.mscCmd.CommandText = "UPDATE sucursales " +
                    "SET empresa_id = @empresaId, codigo = @codigo, nombre = @nombre, representante_legal =  @representanteLegal, calle = @calle, numero_exterior = @numeroExterior, numero_interior = @numeroInterior, asentamiento_id = @asentamientoId, telefono_01 = @telefono1, telefono_02 = @telefono2, correo_electronico = @correoElectronico, inicio_operaciones = @inicioOperaciones, estatus = @estatus, fecha_creacion = @fecha, usuario_creacion =  @usuario  " +
                    "WHERE sucursal_id = @id";
                this.lsQuerysTraccionesElminacionID.Add("DELETE FROM sucursales_cobros_tipos WHERE sucursal_id = @id;");                
                this.lsQuerysTraccionesElminacionID.Add("DELETE FROM sucursales_gramajes WHERE sucursal_id = @id;");
                this.lsQuerysTraccionesElminacionID.Add("DELETE FROM sucursales_plazos WHERE sucursal_id = @id;");
                this.lsQuerysTraccionesElminacionID.Add("DELETE FROM sucursales_parametros WHERE sucursal_id = @id;");
                this.lsQuerysTraccionesID.Add(sucursal.strQueryCobrosTipos);
                this.lsQuerysTraccionesID.Add(sucursal.strQueryGramajes);
                this.lsQuerysTraccionesID.Add(sucursal.strQueryPlazos);
                this.lsQuerysTraccionesID.Add(sucursal.strQueryParametros);
            }
            else
            {
                this.connexion.mscCmd.CommandText = "INSERT INTO sucursales(empresa_id,codigo,nombre,representante_legal,calle,numero_exterior,numero_interior,asentamiento_id,telefono_01,telefono_02,correo_electronico,inicio_operaciones,estatus,fecha_creacion,usuario_creacion) " +
                    " VALUES(@empresaId, @codigo, @nombre, @representanteLegal, @calle, @numeroExterior, @numeroInterior, @asentamientoId, @telefono1, @telefono2, @correoElectronico, @inicioOperaciones, @estatus, @fecha, @usuario); ";
                
                this.lsQuerysTraccionesID.Add(sucursal.strQueryCobrosTipos);
                this.lsQuerysTraccionesID.Add(sucursal.strQueryGramajes);
                this.lsQuerysTraccionesID.Add(sucursal.strQueryPlazos);
                this.lsQuerysTraccionesID.Add(sucursal.strQueryParametros);
            }

            //Asignar valores al commando
            this.connexion.mscCmd.Parameters.AddWithValue("@id", sucursal.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", sucursal.strCodigo);
            this.connexion.mscCmd.Parameters.AddWithValue("@empresaId", sucursal.empresa.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@nombre", sucursal.strNombre);
            this.connexion.mscCmd.Parameters.AddWithValue("@asentamientoId", sucursal.asentamiento.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@representanteLegal", sucursal.strRepresentanteLegal);
            this.connexion.mscCmd.Parameters.AddWithValue("@inicioOperaciones", sucursal.dtInicioOperaciones);
            this.connexion.mscCmd.Parameters.AddWithValue("@telefono1", sucursal.strTelefono1);
            this.connexion.mscCmd.Parameters.AddWithValue("@telefono2", sucursal.strTelefono2);
            this.connexion.mscCmd.Parameters.AddWithValue("@correoElectronico", sucursal.strCorreoElectronico);
            this.connexion.mscCmd.Parameters.AddWithValue("@calle", sucursal.strCalle);
            this.connexion.mscCmd.Parameters.AddWithValue("@numeroInterior", sucursal.strNumeroInterior);
            this.connexion.mscCmd.Parameters.AddWithValue("@numeroExterior", sucursal.strNumeroExterior);
            this.connexion.mscCmd.Parameters.AddWithValue("@estatus", sucursal.strEstatus);
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha", DateTime.Now);
            this.connexion.mscCmd.Parameters.AddWithValue("@usuario", sucursal.intUsuario);

            //Regesar resultado
            this.resultadoTransaccion(this.connexion);
        }

        public void eliminar(Properties_Class.clsSucursal sucursal)
        {

            this.connexion.mscCmd.CommandText = "UPDATE sucursales " +
                "SET  estatus = @estatus, fecha_eliminacion  = @fecha, usuario_eliminacion = @usuario  " +
                "WHERE sucursal_id = @id";
            this.connexion.mscCmd.Parameters.AddWithValue("@id", sucursal.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@estatus", sucursal.strEstatus);
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha", DateTime.Now);
            this.connexion.mscCmd.Parameters.AddWithValue("@usuario", sucursal.intUsuario);
            this.resultadoGuardar(this.connexion);
        }

        public void reactivar(Properties_Class.clsSucursal sucursal)
        {

            connexion.mscCmd.CommandText = "UPDATE sucursales " +
                               " SET  " +
                               " estatus = @estatus, " +
                               " fecha_actualizacion  = @fecha, " +
                               " usuario_actualizacion = @usuario, " +
                               " fecha_eliminacion  = null, " +
                               " usuario_eliminacion = null  " +
                               " WHERE sucursal_id = @id";
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", sucursal.intId);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", sucursal.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", DateTime.Now);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", sucursal.intUsuario);
            this.resultadoGuardar(this.connexion);

        }

        public void buscar(string strCodigo)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT  " +
                " s.sucursal_id ," +
                " s.codigo," +
                " s.empresa_id," +                
                " s.nombre," +
                " s.representante_legal," +
                " s.calle," +
                " s.numero_exterior," +
                " s.numero_interior," +
                " s.asentamiento_id," +
                " s.telefono_01," +
                " s.telefono_02," +
                " s.correo_electronico," +
                " s.inicio_operaciones," +
                " e.razon_social," +
                " e.nombre_comercial," +
                " e.rfc," +
                " e.curp," +
                " a.descripcion," +
                " a.ciudad," +
                " a.codigo_postal ," +
                " m.codigo AS municipioCodigo, " +
                " m.descripcion AS municipioDescripcion,  " +
                " es.codigo AS estadoCodigo, " +
                " es.descripcion AS estadoDescripcion, " +
                " s.estatus " +
                " FROM sucursales AS s " +
                " INNER JOIN empresas as e " +
                " ON s.empresa_id = e.empresa_id " +
                " INNER JOIN asentamientos as a " +
                " ON s.asentamiento_id = a.asentamiento_id " +
                " INNER JOIN municipios as m " +
                " ON a.municipio_id = m.municipio_id " +
                " INNER JOIN estados as es " +
                " ON m.estado_id = es.estado_id " +
            " WHERE s.codigo = @codigo";
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", strCodigo);
            this.resultadoObtener(this.connexion);
        }

        public void buscarSucursalEmprsea(int intEmpresaId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT  " +
                " s.sucursal_id ," +
                " s.codigo," +
                " s.empresa_id," +                
                " s.nombre," +
                " s.representante_legal," +
                " s.calle," +
                " s.numero_exterior," +
                " s.numero_interior," +
                " s.asentamiento_id," +
                " s.telefono_01," +
                " s.telefono_02," +
                " s.correo_electronico," +
                " s.inicio_operaciones," +
                " s.codigo AS `Código sucursal`, " +
                " s.nombre AS `Nombre sucursal` " +                
                " FROM sucursales AS s " +               
            "WHERE s.empresa_id = @empresaId";
            this.connexion.mscCmd.Parameters.AddWithValue("@empresaId", intEmpresaId);
            this.resultadoObtener(this.connexion);
        }


        public void buscarSucursalUsuario(int intUsuarioId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT  " +
                " s.sucursal_id ," +
                " s.codigo," +
                " s.empresa_id," +
                " s.nombre," +
                " s.representante_legal," +
                " s.calle," +
                " s.numero_exterior," +
                " s.numero_interior," +
                " s.asentamiento_id," +
                " s.telefono_01," +
                " s.telefono_02," +
                " s.correo_electronico," +
                " s.inicio_operaciones," +
                " s.codigo AS `Código sucursal`, " +
                " s.nombre AS `Nombre sucursal`" +
                " FROM sucursales AS s " +
                " INNER JOIN usuarios_permisos as up " +
                " ON s.sucursal_id = up.sucursal_id " +
                " WHERE up.usuario_id = @usuarioId" +
                " GROUP BY s.sucursal_id";
            this.connexion.mscCmd.Parameters.AddWithValue("@usuarioId", intUsuarioId);
            this.resultadoObtener(this.connexion);
        }



        public void paginacion()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT  " +
                " s.sucursal_id ," +
                " s.codigo," +
                " s.empresa_id," +                
                " s.nombre," +
                " s.representante_legal," +
                " s.calle," +
                " s.numero_exterior," +
                " s.numero_interior," +
                " s.asentamiento_id," +
                " s.telefono_01," +
                " s.telefono_02," +
                " s.correo_electronico," +
                " s.inicio_operaciones," +
                " e.razon_social," +
                " e.nombre_comercial," +
                " e.rfc," +
                " e.curp," +
                " a.descripcion," +
                " a.ciudad," +
                " a.codigo_postal ," +
                " m.codigo AS municipioCodigo, " +
                " m.descripcion AS municipioDescripcion,  " +
                " es.codigo AS estadoCodigo, " +
                " es.descripcion AS estadoDescripcion, " +
                " s.estatus, " +
                " s.codigo AS `Código sucursal`, " +
                " s.nombre AS `Nombre sucursal`," +
                " e.nombre_comercial AS  `Nombre comercial`" +
                " FROM sucursales AS s " +
                " INNER JOIN empresas as e " +
                " ON s.empresa_id = e.empresa_id "+
                " INNER JOIN asentamientos as a " +
                " ON s.asentamiento_id = a.asentamiento_id " +
                " INNER JOIN municipios as m " +
                " ON a.municipio_id = m.municipio_id " +
                " INNER JOIN estados as es " +
                " ON m.estado_id = es.estado_id " +
                " ORDER BY s.codigo ASC";
            this.resultadoObtener(this.connexion);
        }

        //Obtener sucursales que es diferenet al sucursal que esta logeado el usuario del sistema
        public void paginacionOtrasSuc(int intSucursalId, int intEmpresaId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT  " +
                " s.sucursal_id ," +
                " s.nombre" +
                " FROM sucursales AS s " +
                " WHERE s.sucursal_id NOT IN (@id) AND" +
                " s.empresa_id = @empresa_id";
            this.connexion.mscCmd.Parameters.AddWithValue("@id", intSucursalId);
            this.connexion.mscCmd.Parameters.AddWithValue("@empresa_id", intEmpresaId);
            this.resultadoObtener(this.connexion);
        }


        public void paginacionPorcentajesCobro(int intId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT sct.sucursal_id,ct.cobro_tipo_id, sct.porcentaje " +
                                    "FROM sucursales_cobros_tipos AS sct " +
                                    "RIGHT JOIN cobros_tipos as ct " +
                                    "ON ct.cobro_tipo_id = sct.cobro_tipo_id AND sct.sucursal_id = @sucursalId ";
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursalId", intId);
            this.resultadoObtener(this.connexion);
        }

    }
}
