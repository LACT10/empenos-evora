﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace EmpenosEvora.Model
{
    public class clsMdlMunicipio : Core.clsCoreModel
    {

        public clsMdlMunicipio()
        {
        }

        public void guardar(Properties_Class.clsMunicipio municipio)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (municipio.intId > 0)
            {
                connexion.mscCmd.CommandText = "UPDATE municipios " +
                    "SET codigo = @codigo,  estado_id = @estadoId ,descripcion = @descripcion, estatus = @estatus, fecha_actualizacion  = @fecha, usuario_actualizacion = @usuario  " +
                    "WHERE municipio_id = @id";
            }
            else
            {
                connexion.mscCmd.CommandText = "INSERT INTO municipios(codigo, estado_id,descripcion,estatus,fecha_creacion,usuario_creacion)" +
                    "VALUES(@codigo, @estadoId,@descripcion, @estatus, @fecha, @usuario)";
            }
            //Asignar valores al commando
            this.connexion.mscCmd.Parameters.AddWithValue("@id", municipio.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", municipio.strCodigo);
            this.connexion.mscCmd.Parameters.AddWithValue("@estadoId", municipio.estado.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", municipio.strDescripcion);
            this.connexion.mscCmd.Parameters.AddWithValue("@estatus", municipio.strEstatus);
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha", municipio.dtFecha);
            this.connexion.mscCmd.Parameters.AddWithValue("@usuario", municipio.intUsuario);
            //Regesar resultado
            this.resultadoGuardar(this.connexion);


        }

        public void eliminar(Properties_Class.clsMunicipio municipio)
        {
            
            this.connexion.mscCmd.CommandText = "UPDATE municipios " +
                "SET  estatus = @estatus, fecha_eliminacion  = @fecha, usuario_eliminacion = @usuario  " +
                "WHERE municipio_id = @id";
            this.connexion.mscCmd.Parameters.AddWithValue("@id", municipio.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@estatus", municipio.strEstatus);
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha", municipio.dtFecha);
            this.connexion.mscCmd.Parameters.AddWithValue("@usuario", municipio.intUsuario);
            this.resultadoGuardar(this.connexion);
        }

        public void buscar(string strCodigo)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;            
            this.connexion.mscCmd.CommandText = "SELECT " +
                " m.municipio_id, " +
                " m.codigo, " +
                " m.descripcion, " +
                " m.estatus, " +
                " e.estado_id, " +
                " e.codigo AS estadoCodigo, " +
                " e.descripcion AS estadoDescripcion " +
                "FROM municipios AS m " +
                "INNER JOIN estados as e " +
                "ON m.estado_id = e.estado_id " +
                "WHERE m.codigo = @codigo";
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", strCodigo);            
            this.resultadoObtener(this.connexion);
        }

        public void validarDescripcion(string strDescripcion)
        {

            if (!this.bolResultadoConnexion) return;
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT m.municipio_id, m.codigo, m.descripcion, m.estatus " +
                "FROM municipios AS m " +            
                "WHERE m.descripcion = @descripcion";
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", strDescripcion);
            this.resultadoObtener(this.connexion);


        }
        public void paginacion()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;            
            this.connexion.mscCmd.CommandText = "SELECT m.municipio_id, " +
                "m.codigo, " +
                "m.descripcion, " +
                "m.estatus, " +
                "e.estado_id, " +
                "e.codigo AS estadoCodigo, " +
                "e.descripcion AS estadoDescripcion," +                
                "m.codigo AS  `Código`, " +
                "m.descripcion AS `Descripción`, " +
                "e.codigo AS  `Código Estado`, " +
                "e.descripcion AS `Descripción estado` " +
                "FROM municipios AS m " +
                "INNER JOIN estados as e " +
                "ON m.estado_id = e.estado_id " +
                " ORDER BY m.codigo ASC";                                           
            this.resultadoObtener(this.connexion);
        }
    }
}
