﻿
  using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlAsentamientosTipos : Core.clsCoreModel
    {
        public clsMdlAsentamientosTipos()
        {

        }

        public void guardar(Properties_Class.clsAsentamientosTipos  asentamientosTipo)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if ( asentamientosTipo.intId > 0)
            {
                connexion.mscCmd.CommandText = "UPDATE asentamientos_tipos " +
                    "SET codigo = @codigo,  descripcion = @descripcion, estatus = @estatus, fecha_actualizacion  = @fecha, usuario_actualizacion = @usuario  WHERE  asentamiento_tipo_id = @id";
            }
            else
            {
                connexion.mscCmd.CommandText = "INSERT INTO asentamientos_tipos(codigo, descripcion,estatus,fecha_creacion,usuario_creacion)" +
                    "VALUES(@codigo, @descripcion, @estatus, @fecha, @usuario)";
            }
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id",  asentamientosTipo.intId);
            connexion.mscCmd.Parameters.AddWithValue("@codigo",  asentamientosTipo.strCodigo);
            connexion.mscCmd.Parameters.AddWithValue("@descripcion",  asentamientosTipo.strDescripcion);
            connexion.mscCmd.Parameters.AddWithValue("@estatus",  asentamientosTipo.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha",  asentamientosTipo.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario",  asentamientosTipo.intUsuario);
            //Regesar resultado
            this.resultadoGuardar(this.connexion);
        }

        public void eliminar(Properties_Class.clsAsentamientosTipos  asentamientosTipo)
        {

            connexion.mscCmd.CommandText = "UPDATE asentamientos_tipos " +
                               "SET  estatus = @estatus, fecha_eliminacion  = @fecha, usuario_eliminacion = @usuario  WHERE  asentamiento_tipo_id = @id";
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id",  asentamientosTipo.intId);
            connexion.mscCmd.Parameters.AddWithValue("@estatus",  asentamientosTipo.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha",  asentamientosTipo.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario",  asentamientosTipo.intUsuario);
            this.resultadoObtener(this.connexion);
        }


        public void buscar(string strCodigo)
        {

            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT e. asentamiento_tipo_id, e.codigo, e.descripcion, e.estatus FROM asentamientos_tipos AS e WHERE e.codigo = @codigo";
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", strCodigo);
            this.resultadoObtener(this.connexion);


        }


        public void validarDescripcion(string strDescripcion)
        {

            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT e. asentamiento_tipo_id, e.codigo, e.descripcion FROM asentamientos_tipos AS e WHERE e.descripcion = @descripcion";
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", strDescripcion);
            this.resultadoObtener(this.connexion);


        }

        public void paginacion()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT e.asentamiento_tipo_id, " +
                "e.codigo, " +
                "e.descripcion," +
                " e.estatus, " +
                "e.codigo AS `Código`," +
                " e.descripcion AS `Descripción`" +
                " FROM asentamientos_tipos AS e " +
                " ORDER BY e.codigo ASC";
            this.resultadoObtener(this.connexion);
        }
    }
}