﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlIdentificacionesTipo : Core.clsCoreModel
    {
        public clsMdlIdentificacionesTipo()
        {

        }

        public void guardar(Properties_Class.clsIdentificacionesTipo identificaciones_tipo)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (identificaciones_tipo.intId > 0)
            {
                connexion.mscCmd.CommandText = "UPDATE identificaciones_tipos " +
                    "SET codigo = @codigo,  descripcion = @descripcion,  @validar_curp = valida_curp, estatus = @estatus, fecha_actualizacion  = @fecha, usuario_actualizacion = @usuario  WHERE identificacion_tipo_id = @id";
            }
            else
            {
                connexion.mscCmd.CommandText = "INSERT INTO identificaciones_tipos(codigo, descripcion, valida_curp, estatus,fecha_creacion,usuario_creacion)" +
                    "VALUES(@codigo, @descripcion, @validar_curp,@estatus, @fecha, @usuario)";
            }
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", identificaciones_tipo.intId);
            connexion.mscCmd.Parameters.AddWithValue("@codigo", identificaciones_tipo.strCodigo);
            connexion.mscCmd.Parameters.AddWithValue("@descripcion", identificaciones_tipo.strDescripcion);
            connexion.mscCmd.Parameters.AddWithValue("@validar_curp", identificaciones_tipo.strValidarCurp);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", identificaciones_tipo.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", identificaciones_tipo.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", identificaciones_tipo.intUsuario);
            //Regesar resultado
            this.resultadoGuardar(this.connexion);
        }

        public void eliminar(Properties_Class.clsIdentificacionesTipo identificaciones_tipo)
        {

            connexion.mscCmd.CommandText = "UPDATE identificaciones_tipos " +
                               "SET  estatus = @estatus, fecha_eliminacion  = @fecha, usuario_eliminacion = @usuario  WHERE identificacion_tipo_id = @id";
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", identificaciones_tipo.intId);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", identificaciones_tipo.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", identificaciones_tipo.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", identificaciones_tipo.intUsuario);
            this.resultadoObtener(this.connexion);
        }


        public void buscar(string strCodigo)
        {

            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT it.identificacion_tipo_id, it.codigo, it.descripcion , it.valida_curp FROM identificaciones_tipos AS it WHERE it.codigo = @codigo";
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", strCodigo);
            this.resultadoObtener(this.connexion);


        }

        public void validarDescripcion(string strDescripcion)
        {

            if (!this.bolResultadoConnexion) return;
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT it.identificacion_tipo_id, it.codigo, it.descripcion , it.valida_curp FROM identificaciones_tipos AS it WHERE it.descripcion = @descripcion";
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", strDescripcion);
            this.resultadoObtener(this.connexion);


        }

        public void paginacion()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT it.identificacion_tipo_id, " +
                "it.codigo, " +
                "it.descripcion, " +
                "it.valida_curp, " +
                "it.estatus, " +
                "it.codigo AS `Código`, " +
                "it.descripcion AS `Descripción`, " +
                "it.valida_curp AS CURP" +
                " FROM identificaciones_tipos AS it " +
               " ORDER BY it.codigo ASC";
            this.resultadoObtener(this.connexion);
        }
    }
}