﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlMovimientosCajaTipos : Core.clsCoreModel
    {

        public clsMdlMovimientosCajaTipos()
        {

        }
        public void guardar(Properties_Class.clsMovimientosCajaTipos movimientosCajaTipo)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (movimientosCajaTipo.intId > 0)
            {
                connexion.mscCmd.CommandText = "UPDATE movimientos_caja_tipos " +
                    "SET tipo = @tipo,  descripcion = @descripcion, estatus = @estatus, fecha_actualizacion  = @fecha, usuario_actualizacion = @usuario  WHERE movimiento_caja_tipo_id = @id";
            }
            else
            {
                connexion.mscCmd.CommandText = "INSERT INTO movimientos_caja_tipos( descripcion, tipo ,estatus,fecha_creacion,usuario_creacion)" +
                    "VALUES( @descripcion, @tipo, @estatus, @fecha, @usuario)";
            }
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", movimientosCajaTipo.intId);
            connexion.mscCmd.Parameters.AddWithValue("@descripcion", movimientosCajaTipo.strDescripcion);
            connexion.mscCmd.Parameters.AddWithValue("@tipo", movimientosCajaTipo.strTipo);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", movimientosCajaTipo.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", movimientosCajaTipo.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", movimientosCajaTipo.intUsuario);
            //Regesar resultado
            this.resultadoGuardar(this.connexion);
        }

        public void eliminar(Properties_Class.clsMovimientosCajaTipos movimientosCajaTipo)
        {

            connexion.mscCmd.CommandText = "UPDATE movimientos_caja_tipos " +
                               "SET  estatus = @estatus, fecha_eliminacion  = @fecha, usuario_eliminacion = @usuario  WHERE movimiento_caja_tipo_id = @id";
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", movimientosCajaTipo.intId);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", movimientosCajaTipo.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", movimientosCajaTipo.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", movimientosCajaTipo.intUsuario);
            this.resultadoObtener(this.connexion);
        }


        public void buscar(string strDescripcion)
        {

            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT e.movimiento_caja_tipo_id,e.descripcion,  e.tipo " +
                "FROM movimientos_caja_tipos AS e " +
                "WHERE e.descripcion = @descripcion";
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", strDescripcion);
            this.resultadoObtener(this.connexion);
        }

        public void buscarDuplicacion(string strTipo, string strDescripcion)
        {

            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT e.movimiento_caja_tipo_id,e.descripcion,  e.tipo " +
                " FROM movimientos_caja_tipos AS e " +
                " WHERE e.descripcion = @descripcion" +
                " AND e.tipo = @tipo ";
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", strDescripcion);
            this.connexion.mscCmd.Parameters.AddWithValue("@tipo", strTipo);
            this.resultadoObtener(this.connexion);
        }

        public void paginacion()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT e.movimiento_caja_tipo_id, e.descripcion, e.tipo, e.estatus , e.tipo AS `Tipo `, e.descripcion AS `Descripción`" +
                " FROM movimientos_caja_tipos AS e ";
            this.resultadoObtener(this.connexion);
        }

        public void paginacionActivo()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " e.movimiento_caja_tipo_id, " +
                " e.descripcion, " +
                " e.tipo, " +
                " e.estatus ," +
                "  e.tipo AS `Tipo `, " +
                " e.descripcion AS `Descripción`" +
                " FROM movimientos_caja_tipos AS e " +
                " WHERE e.estatus =  'ACTIVO'";
            this.resultadoObtener(this.connexion);
        }

    }
}
