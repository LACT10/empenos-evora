﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlMovimientosCaja : Core.clsCoreModel
    {

       public clsMdlMovimientosCaja() { }
        public void guardar(Properties_Class.clsMovimientosCaja movimientosCaja)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.lsQuerysTracciones = new List<string>();
            this.lsQuerysTraccionesID = new List<string>();
            this.lsQuerysTraccionesElminacionID = new List<string>();
            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (movimientosCaja.intId > 0)
            {
                connexion.mscCmd.CommandText = "UPDATE movimientos_caja " +
                    "SET " +
                    " sucursal_id = @sucursal_id,  " +
                    " caja_id = @caja_id, " +
                    " usuario_id = @usuario_id, " +
                    " folio = @folio, " +
                    " fecha = @fechaMovimientos, " +
                    " concepto = @concepto, " +
                    " estatus = @estatus, " +
                    " fecha_actualizacion  = @fecha, " +
                    " usuario_actualizacion = @usuario  " +
                    " WHERE movimiento_caja_id = @id";
                this.lsQuerysTraccionesElminacionID.Add("DELETE FROM movimientos_caja_detalles WHERE movimiento_caja_id = @id;");
                this.lsQuerysTraccionesID.Add(movimientosCaja.strQueryMovimientos);
            }
            else
            {                
                connexion.mscCmd.CommandText = "INSERT INTO movimientos_caja(sucursal_id, caja_id, usuario_id, folio, fecha, concepto, estatus, fecha_creacion, usuario_creacion)" +
                    "VALUES(  @sucursal_id, @caja_id, @usuario_id, @folio, @fechaMovimientos, @concepto, @estatus, @fecha, @usuario)";
                this.lsQuerysTracciones.Add(movimientosCaja.strQueryConsecutivoFolio);
                this.lsQuerysTraccionesID.Add(movimientosCaja.strQueryMovimientos);
            }
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", movimientosCaja.intId);
            connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", movimientosCaja.intSucursal);
            connexion.mscCmd.Parameters.AddWithValue("@caja_id", movimientosCaja.caja.intId);
            connexion.mscCmd.Parameters.AddWithValue("@usuario_id", movimientosCaja.empleado.intId);
            connexion.mscCmd.Parameters.AddWithValue("@folio", movimientosCaja.strFolio);
            connexion.mscCmd.Parameters.AddWithValue("@fechaMovimientos", movimientosCaja.dtFechaMovimiento);
            connexion.mscCmd.Parameters.AddWithValue("@concepto", movimientosCaja.strConcepto);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", movimientosCaja.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", movimientosCaja.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", movimientosCaja.intUsuario);
            //Parametros para incrementar folio consecutivo de proceso
            connexion.mscCmd.Parameters.AddWithValue("@folio_id", movimientosCaja.intFolioId);
            //Regesar resultado
            this.resultadoTransaccion(this.connexion);
        }

        public void eliminar(Properties_Class.clsMovimientosCaja movimientosCaja)
        {

            connexion.mscCmd.CommandText = "UPDATE movimientos_caja " +
                               " SET  " +
                               " estatus = @estatus, " +
                               " motivo_cancelacion = @motivo, " +
                               " fecha_eliminacion  = @fecha, " +
                               " usuario_eliminacion = @usuario  " +
                               " WHERE movimiento_caja_id = @id";
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@motivo", movimientosCaja.strMotivosCancelacion);
            connexion.mscCmd.Parameters.AddWithValue("@id", movimientosCaja.intId);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", movimientosCaja.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", DateTime.Now);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", movimientosCaja.intUsuario);
            this.resultadoGuardar(this.connexion);
        }

       /*public void reactivar(Properties_Class.clsMovimientosCaja movimientosCaja)
        {

            connexion.mscCmd.CommandText = "UPDATE movimientos_caja " +
                               "SET  " +
                               " estatus = @estatus, " +
                               " fecha_actualizacion  = @fecha, " +
                               " usuario_actualizacion = @usuario, " +
                               " fecha_eliminacion  = null, " +
                               " usuario_eliminacion = null  " +
                               " WHERE movimiento_caja_id = @id";
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", movimientosCaja.intId);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", movimientosCaja.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", DateTime.Now);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", movimientosCaja.intUsuario);
            this.resultadoGuardar(this.connexion);
        }*/

        public void buscar(string strFolio)
        {
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText =
                " SELECT " +
                " mc.movimiento_caja_id, " +
                " mc.folio, " +
                " mc.fecha, " +
                " mc.concepto," +
                " CONCAT(e.nombre,  ' ',  e.apellido_paterno,  ' ',  e.apellido_materno) AS nombre_empleado, " +
                " e.usuario_id AS usuario_id, " +
                " cj.caja_id, " +
                " cj.codigo AS codigo_caja, " +
                " cj.descripcion AS descripcion_caja, " +
                " mc.estatus, " +
                " mc.motivo_cancelacion, " +
                " CONCAT(uE.nombre,  ' ',  uE.apellido_paterno,  ' ',  uE.apellido_materno) AS nombre_empleado_eliminacion, " +
                " DATE_FORMAT(mc.fecha_eliminacion, '%d/%m/%Y %r') AS fecha_eliminacion, " +
                " mc.folio as `Folio `, " +
                " mc.concepto as `Concepto `, " +
                " DATE_FORMAT(mc.fecha, '%d/%m/%Y %r') AS `Fecha ` " +
                " FROM movimientos_caja AS mc " +
                " INNER JOIN usuarios AS e " +
                " ON mc.usuario_id = e.usuario_id " +
                " INNER JOIN cajas AS cj " +
                " ON mc.caja_id = cj.caja_id" +
                " LEFT JOIN usuarios AS uE " +
                " ON mc.usuario_eliminacion = uE.usuario_id " +
                " WHERE  mc.folio = @folio";
                
            this.connexion.mscCmd.Parameters.AddWithValue("@folio", strFolio);
            this.resultadoObtener(this.connexion);
        }
        
        public void paginacion(int intSucursalId, int intCajaId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            //Los alias por ejemplos `Estatus ` se utiliza para sincronizar en el datagridview.
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText =
                " SELECT " +
                " mc.movimiento_caja_id, " +
                " mc.folio, " +
                " mc.fecha, " +
                " mc.concepto," +
                " CONCAT(e.nombre,  ' ',  e.apellido_paterno,  ' ',  e.apellido_materno) AS nombre_empleado, " +
                " e.usuario_id AS usuario_id, " +
                " cj.caja_id, " +
                " cj.codigo AS codigo_caja, " +
                " cj.descripcion AS descripcion_caja, " +
                " mc.estatus, " +
                " mc.motivo_cancelacion, " +
                " CONCAT(uE.nombre,  ' ',  uE.apellido_paterno,  ' ',  uE.apellido_materno) AS nombre_empleado_eliminacion, " +
                " DATE_FORMAT(mc.fecha_eliminacion, '%d/%m/%Y %r') AS fecha_eliminacion, " +
                " DATE_FORMAT(mc.fecha, '%Y/%m/%d') AS fecha_busqueda ," +
                " mc.folio as `Folio `, " +
                " mc.concepto as `Concepto `, " +
                " DATE_FORMAT(mc.fecha, '%d/%m/%Y %r') AS `Fecha `, " +
                " mc.estatus AS `Estatus ` " +
                " FROM movimientos_caja AS mc " +
                " INNER JOIN usuarios AS e " +
                " ON mc.usuario_id = e.usuario_id " +
                " INNER JOIN cajas AS cj " +
                " ON mc.caja_id = cj.caja_id" +
                " LEFT JOIN usuarios AS uE " +
                " ON mc.usuario_eliminacion = uE.usuario_id " +
                " WHERE mc.caja_id = @caja_id AND " +
                " mc.sucursal_id = @sucursal_id" +
                " ORDER BY mc.folio ASC ";
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", intSucursalId);
            this.connexion.mscCmd.Parameters.AddWithValue("@caja_id", intCajaId);
            this.resultadoObtener(this.connexion);
        }
    }
}
