﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlSucursalesPromocion : Core.clsCoreModel
    {

        public clsMdlSucursalesPromocion()
        {
        }

        public void guardar(Properties_Class.clsSucursalesPromocion sucursalesPromocion) 
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            this.lsQuerysTracciones = new List<string>();
            this.lsQuerysTraccionesID = new List<string>();
            this.lsQuerysTraccionesElminacionID = new List<string>();
            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (sucursalesPromocion.intId > 0)
            {
                connexion.mscCmd.CommandText = " UPDATE sucursales_promociones " +
                               " SET " +
                               " sucursal_id = @sucursal_id,  " +
                               " dia = @dia, " +
                               " dias_minimo = @dias_minimo, " +
                               " porcentaje = @porcentaje, " +
                               " estatus = @estatus " +                               
                               " WHERE sucursal_promocion_id = @id";

                this.lsQuerysTraccionesElminacionID.Add("DELETE FROM sucursales_promociones_detalles WHERE sucursal_promocion_id = @id;");
                this.lsQuerysTracciones.Add(sucursalesPromocion.strQueryDetalles);
            }
            else
            {
                connexion.mscCmd.CommandText = "INSERT INTO sucursales_promociones(sucursal_id, dia, dias_minimo, porcentaje, estatus)" +
                    "VALUES(@sucursal_id, @dia, @dias_minimo, @porcentaje,@estatus)";
                
                this.lsQuerysTraccionesID.Add(sucursalesPromocion.strQueryDetalles);
            }
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", sucursalesPromocion.intId);
            connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", sucursalesPromocion.intSucursal);
            connexion.mscCmd.Parameters.AddWithValue("@dia", sucursalesPromocion.strDia);
            connexion.mscCmd.Parameters.AddWithValue("@dias_minimo", sucursalesPromocion.decDiaMin);
            connexion.mscCmd.Parameters.AddWithValue("@porcentaje", sucursalesPromocion.decPorcentaje);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", sucursalesPromocion.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", sucursalesPromocion.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", sucursalesPromocion.intUsuario);
            //Regesar resultado
            this.resultadoTransaccion(this.connexion);
        }

        public void eliminar(int intSucursalPromocionId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo         
            connexion.mscCmd.CommandText = "DELETE FROM sucursales_promociones WHERE sucursal_promocion_id = @id";
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", intSucursalPromocionId);
            
            //Regesar resultado
            this.resultadoTransaccion(this.connexion);
        }
        public void buscar(int intSucursalPromocionId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT  " +
                " sucursal_promocion_id," +
                " dia, " +
                " dias_minimo, " +
                " porcentaje, " +
                " estatus " +
                " FROM sucursales_promociones AS sp " +
                " WHERE sucursal_promocion_id = @sucursal_promocion_id";                
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_promocion_id", intSucursalPromocionId);
            this.resultadoObtener(this.connexion);
        }


        public void buscarPromocionDia(int intSucusrsalId, string strDia) 
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT  " +                              
                " sp.porcentaje, " +
                " spd.prenda_tipo_id ," +
                " sp.dias_minimo" +
                " FROM sucursales_promociones AS sp " +
                " INNER JOIN sucursales_promociones_detalles AS spd " +
                " ON sp.sucursal_promocion_id = spd.sucursal_promocion_id " +
                " WHERE sp.sucursal_id = @sucursal_id " +                
                " AND sp.dia = @dia" +
                " AND sp.estatus = 'ACTIVO'";
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", intSucusrsalId);
            this.connexion.mscCmd.Parameters.AddWithValue("@dia", strDia);
            this.resultadoObtener(this.connexion);
        }
        public void buscarTipoPrenda(int intSucusrsalId,string strTipoPrenda, string strDia)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT  " +
                " sp.sucursal_promocion_id," +
                " sp.dia, " +
                " sp.dias_minimo, " +
                " sp.porcentaje, " +
                " sp.estatus " +
                " FROM sucursales_promociones AS sp " +
                " INNER JOIN sucursales_promociones_detalles AS spd " +
                " ON sp.sucursal_promocion_id = spd.sucursal_promocion_id " +
                " WHERE sp.sucursal_id = @sucursal_id " +
                " AND spd.prenda_tipo_id = @prenda_tipo_id" +
                " AND sp.dia = @dia " +
                " AND sp.estatus = 'ACTIVO'";
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", intSucusrsalId);
            this.connexion.mscCmd.Parameters.AddWithValue("@prenda_tipo_id", strTipoPrenda);
            this.connexion.mscCmd.Parameters.AddWithValue("@dia", strDia);
            this.resultadoObtener(this.connexion);
        }

        public void paginacion_detalles(int strSucursald, int intSucursalPromocionId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT  " +
                " IFNULL(spd.renglon, 0) AS renglon," +
                " pt.prenda_tipo_id, " +
                " pt.descripcion" +                
                " FROM sucursales_promociones AS sp " +
                " RIGHT JOIN sucursales_promociones_detalles AS spd " +
                " ON sp.sucursal_promocion_id = spd.sucursal_promocion_id AND spd.sucursal_promocion_id = @sucursal_promocion_id " +
                " RIGHT JOIN prendas_tipos AS pt " +
                " ON spd.prenda_tipo_id = pt.prenda_tipo_id AND sp.sucursal_id = @sucursal_id  " +                                
                " WHERE pt.estatus = 'ACTIVO'";
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", strSucursald);
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_promocion_id", intSucursalPromocionId);
            this.resultadoObtener(this.connexion);
        }

        public void paginacion(int strSucursald)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT  " +
                " sp.sucursal_promocion_id, " +
                " sp.dia, " +
                " sp.dias_minimo, " +
                " CONCAT(sp.porcentaje, '%') AS porcentaje, " +
                " (SELECT  GROUP_CONCAT(pt.descripcion SEPARATOR ',') AS descripcion" +
                " FROM prendas_tipos AS pt " +
                " INNER JOIN sucursales_promociones_detalles AS spd" +
                " ON pt.prenda_tipo_id = spd.prenda_tipo_id" +
                " WHERE spd.sucursal_promocion_id = sp.sucursal_promocion_id " +
                " AND pt.estatus = 'ACTIVO'" +
                " GROUP BY spd.sucursal_promocion_id) AS lista_tipos_prenda," +
                " sp.estatus " +
                " FROM sucursales_promociones AS sp " +
                " WHERE sp.sucursal_id = @sucursal_id";                               
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", strSucursald);            
            this.resultadoObtener(this.connexion);
        }


    }
}
