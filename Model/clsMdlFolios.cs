﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlFolios : Core.clsCoreModel
    {

        public clsMdlFolios() { }

        public void guardar(Properties_Class.clsFolios folio)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (folio.intId > 0)
            {
                connexion.mscCmd.CommandText = "UPDATE folios " +
                    "SET   descripcion = @descripcion, serie = @serie, consecutivo = @consecutivo, estatus = @estatus, fecha_actualizacion  = @fecha, usuario_actualizacion = @usuario  WHERE folio_id = @id";
            }
            else
            {
                connexion.mscCmd.CommandText = "INSERT INTO folios(descripcion,serie,consecutivo ,estatus,fecha_creacion,usuario_creacion)" +
                    "VALUES( @descripcion, @serie, @consecutivo, @estatus, @fecha, @usuario)";
            }
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", folio.intId);            
            connexion.mscCmd.Parameters.AddWithValue("@descripcion", folio.strDescripcion);
            connexion.mscCmd.Parameters.AddWithValue("@serie", folio.strSerie);
            connexion.mscCmd.Parameters.AddWithValue("@consecutivo", folio.intConsecutivo.ToString());
            connexion.mscCmd.Parameters.AddWithValue("@estatus", folio.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", folio.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", folio.intUsuario);
            //Regesar resultado
            this.resultadoGuardar(this.connexion);
        }

        public void eliminar(Properties_Class.clsFolios estado)
        {

            connexion.mscCmd.CommandText = "UPDATE folios " +
                               "SET  estatus = @estatus, fecha_eliminacion  = @fecha, usuario_eliminacion = @usuario  WHERE folio_id = @id";
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", estado.intId);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", estado.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", estado.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", estado.intUsuario);
            this.resultadoObtener(this.connexion);
        }


        /*public void buscar(string strCodigo)
        {

            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT e.estado_id, e.codigo, e.descripcion " +
                " FROM estados AS e WHERE e.codigo = @codigo";
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", strCodigo);
            this.resultadoObtener(this.connexion);


        }*/

        public void buscarDescripcion(Properties_Class.clsFolios folios) 
        {
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +                
                " f.descripcion " +
                " FROM folios AS f " +
                " WHERE f.descripcion = @descripcion ";
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", folios.strDescripcion);
            this.resultadoObtener(this.connexion);
        }

        public void buscarFolioProceso(string strProcesoId, string strSucursalId)
        {
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " f.serie," +
                " f.consecutivo, " +
                " f.folio_id " +
                " FROM folios AS f " +
                " INNER JOIN folios_procesos AS fp" +
                " ON f.folio_id = fp.folio_id" +
                " WHERE fp.sucursal_id = @sucursalId " +
                " AND fp.proceso_id = @procesoId ";
            this.connexion.mscCmd.Parameters.AddWithValue("@procesoId", strProcesoId);
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursalId", strSucursalId);
            this.resultadoObtener(this.connexion);
        }
    
        public void paginacion()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " f.folio_id, " +
                " f.descripcion, " +
                " f.serie, " +
                " f.consecutivo," +
                " f.estatus," +              
                " f.descripcion AS `Descripción`," +
                " f.serie AS `Serie `," +
                " f.consecutivo AS `Consecutivo `" +
                " FROM folios AS f " +
                " ORDER BY f.descripcion ASC";
            this.resultadoObtener(this.connexion);
        }


    }
}
