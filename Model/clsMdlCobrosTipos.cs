﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlCobrosTipos : Core.clsCoreModel
    {
        public clsMdlCobrosTipos()
        {

        }

        public void guardar(Properties_Class.clsCobrosTipos cobrosTipos)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (cobrosTipos.intId > 0)
            {
                connexion.mscCmd.CommandText = "UPDATE cobros_tipos " +
                    "SET codigo = @codigo,  descripcion = @descripcion, estatus = @estatus, fecha_actualizacion  = @fecha, usuario_actualizacion = @usuario  WHERE cobro_tipo_id = @id";
            }
            else
            {
                connexion.mscCmd.CommandText = "INSERT INTO cobros_tipos(codigo, descripcion,estatus,fecha_creacion,usuario_creacion)" +
                    "VALUES(@codigo, @descripcion, @estatus, @fecha, @usuario)";
            }
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", cobrosTipos.intId);
            connexion.mscCmd.Parameters.AddWithValue("@codigo", cobrosTipos.strCodigo);
            connexion.mscCmd.Parameters.AddWithValue("@descripcion", cobrosTipos.strDescripcion);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", cobrosTipos.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", cobrosTipos.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", cobrosTipos.intUsuario);
            //Regesar resultado
            this.resultadoGuardar(this.connexion);
        }

        public void eliminar(Properties_Class.clsCobrosTipos cobrosTipos)
        {

            connexion.mscCmd.CommandText = "UPDATE cobros_tipos " +
                               "SET  estatus = @estatus, fecha_eliminacion  = @fecha, usuario_eliminacion = @usuario  WHERE cobro_tipo_id = @id";
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", cobrosTipos.intId);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", cobrosTipos.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", cobrosTipos.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", cobrosTipos.intUsuario);
            this.resultadoObtener(this.connexion);
        }


        public void buscar(string strCodigo)
        {

            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT ct.cobro_tipo_id, ct.codigo, ct.descripcion FROM cobros_tipos AS ct WHERE ct.codigo = @codigo";
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", strCodigo);
            this.resultadoObtener(this.connexion);


        }
        public void validarDescripcion(string strDescripcion)
        {
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT ct.cobro_tipo_id, ct.codigo, ct.descripcion FROM cobros_tipos AS ct WHERE ct.descripcion = @descripcion";
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", strDescripcion);
            this.resultadoObtener(this.connexion);
        }

        public void paginacion()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT ct.cobro_tipo_id," +
                " ct.codigo, " +
                "ct.descripcion, " +
                "ct.estatus,  " +
                "ct.codigo AS `Código`, " +
                "ct.descripcion AS `Descripción`" +
                " FROM cobros_tipos AS ct " +
                " ORDER BY ct.codigo ASC";
            this.resultadoObtener(this.connexion);
        }
    }
}
