﻿namespace EmpenosEvora.Model
{
    public class clsMdlSucursalPlazo : Core.clsCoreModel
    {

        public clsMdlSucursalPlazo()
        {
        }


        public void buscar(string strSucursalId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " sp.sucursal_id, " +
                " pt.prenda_tipo_id, " +
                " sp.plazo, " +
                " pt.codigo AS `Código tipo de prendas`, " +
                " pt.descripcion AS `Descripción tipo de prendas` " +
                                    "FROM sucursales_plazos AS sp " +
                                    "RIGHT JOIN prendas_tipos as pt " +
                                    "ON pt.prenda_tipo_id = sp.prenda_tipo_id AND sp.sucursal_id = @id ";
            this.connexion.mscCmd.Parameters.AddWithValue("@id", strSucursalId);
            this.resultadoObtener(this.connexion);
        }


        public void buscarSucTipoPrenda(Properties_Class.clsSucursalPlazo sucursalPlazo)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " sp.sucursal_id, " +
                " pt.prenda_tipo_id, " +
                " sp.plazo, " +
                " pt.codigo, " +
                " pt.descripcion, " +
                " pt.capturar_gramaje, " +
                 " pt.capturar_imei_celular," +
                " pt.capturar_serie_vehiculo, " +
                " sp.plazo AS `Plazo `, " +
                " pt.codigo AS `Código tipo de prendas`, " +
                " pt.capturar_gramaje AS `Gramaje`, " +
                " pt.descripcion AS `Descripción tipo de prendas` " +
                                    " FROM sucursales_plazos AS sp " +
                                    " INNER JOIN prendas_tipos as pt " +
                                    " ON pt.prenda_tipo_id = sp.prenda_tipo_id AND sp.sucursal_id = @sucursalId " +
                                    " WHERE pt.estatus = 'ACTIVO'" +
                                    " AND pt.codigo = @prendasTipoCodigo";
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursalId", sucursalPlazo.sucursal.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@prendasTipoCodigo", sucursalPlazo.prendasTipo.strCodigo);
            this.resultadoObtener(this.connexion);
        }


        public void paginacionSucursalInner(int intId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " sp.sucursal_id, " +
                " pt.prenda_tipo_id, " +
                " sp.plazo, " +
                " pt.codigo, " +
                " pt.descripcion, " +
                " pt.capturar_gramaje," +
                " pt.capturar_imei_celular," +
                " pt.capturar_serie_vehiculo, " +
                " pt.codigo AS `Código tipo de prendas`, " +
                " pt.capturar_gramaje AS `Gramaje`, " +
                " sp.plazo AS `Plazo `, " +
                " pt.descripcion AS `Descripción tipo de prendas` " +
                                    " FROM sucursales_plazos AS sp " +
                                    " Inner JOIN prendas_tipos as pt " +
                                    " ON pt.prenda_tipo_id = sp.prenda_tipo_id AND sp.sucursal_id = @sucursalId " +
                                    " WHERE pt.estatus = 'ACTIVO'";
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursalId", intId);
            this.resultadoObtener(this.connexion);
        }

        public void paginacion(int intId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " sp.sucursal_id, " +
                " pt.prenda_tipo_id, " +
                " sp.plazo, " +
                " pt.codigo, " +
                " pt.descripcion, " +
                " pt.capturar_gramaje, " +
                " pt.capturar_imei_celular," +
                " pt.capturar_serie_vehiculo, " +
                " CONCAT(pt.codigo , ' - ' ,  pt.descripcion) AS `Descripción tipo de prendas` " +
                                    " FROM sucursales_plazos AS sp " +
                                    " RIGHT JOIN prendas_tipos as pt " +
                                    " ON pt.prenda_tipo_id = sp.prenda_tipo_id AND sp.sucursal_id = @sucursalId " +
                                    " WHERE pt.estatus = 'ACTIVO'";
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursalId", intId);
            this.resultadoObtener(this.connexion);
        }

    }
}