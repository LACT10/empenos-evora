﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlGramajes : Core.clsCoreModel
    {

        public clsMdlGramajes()
        {
        }

        public void guardar(Properties_Class.clsGramaje gramaje)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (gramaje.intId > 0)
            {
                connexion.mscCmd.CommandText = "UPDATE gramajes " +
                    "SET codigo = @codigo,  prenda_tipo_id = @prendaTipoId ,descripcion = @descripcion, estatus = @estatus, fecha_actualizacion  = @fecha, usuario_actualizacion = @usuario  " +
                    "WHERE gramaje_id = @id";
            }
            else
            {
                connexion.mscCmd.CommandText = "INSERT INTO gramajes(codigo, prenda_tipo_id,descripcion,estatus,fecha_creacion,usuario_creacion)" +
                    "VALUES(@codigo, @prendaTipoId,@descripcion, @estatus, @fecha, @usuario)";
            }
            //Asignar valores al commando
            this.connexion.mscCmd.Parameters.AddWithValue("@id", gramaje.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", gramaje.strCodigo);
            this.connexion.mscCmd.Parameters.AddWithValue("@prendaTipoId", gramaje.prendasTipo.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", gramaje.strDescripcion);
            this.connexion.mscCmd.Parameters.AddWithValue("@estatus", gramaje.strEstatus);
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha", gramaje.dtFecha);
            this.connexion.mscCmd.Parameters.AddWithValue("@usuario", gramaje.intUsuario);
            //Regesar resultado
            this.resultadoGuardar(this.connexion);


        }

        public void eliminar(Properties_Class.clsGramaje gramaje)
        {

            this.connexion.mscCmd.CommandText = "UPDATE gramajes " +
                "SET  estatus = @estatus, fecha_eliminacion  = @fecha, usuario_eliminacion = @usuario  " +
                "WHERE gramaje_id = @id";
            this.connexion.mscCmd.Parameters.AddWithValue("@id", gramaje.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@estatus", gramaje.strEstatus);
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha", gramaje.dtFecha);
            this.connexion.mscCmd.Parameters.AddWithValue("@usuario", gramaje.intUsuario);
            this.resultadoGuardar(this.connexion);
        }

        public void buscar(string strCodigo)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT m.gramaje_id, m.codigo, m.descripcion, m.estatus, e.prenda_tipo_id, e.codigo AS prendaTipoCodigo, e.descripcion AS estadoDescripcion " +
                "FROM gramajes AS m " +
                "INNER JOIN prendas_tipos as e " +
                "ON m.prenda_tipo_id = e.prenda_tipo_id " +
                "WHERE m.codigo = @codigo";
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", strCodigo);
            this.resultadoObtener(this.connexion);
        }

        

        public void buscarPrendaSucursal(string strCodigo, string strTipoPrendaId, string strSucursalId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " m.gramaje_id, " +
                " m.codigo, " +
                " m.descripcion, " +
                " sg.precio_tope " +
                " FROM gramajes as m " +
                " INNER JOIN sucursales_gramajes as sg " +
                " on m.gramaje_id = sg.gramaje_id " +
                " WHERE sg.sucursal_id = @sucursal_id " +
                " AND m.prenda_tipo_id = @tipoPrendaId " +
                " AND m.estatus = 'ACTIVO'" +
                " AND m.codigo = @codigo";
            this.connexion.mscCmd.Parameters.AddWithValue("@tipoPrendaId", strTipoPrendaId);
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", strSucursalId);
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", strCodigo);
            this.resultadoObtener(this.connexion);
        }

        public void validarDescripcion(string strDescripcion)
        {
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT m.gramaje_id, m.codigo, m.descripcion, m.estatus " +
               "FROM gramajes AS m " +

               "WHERE m.descripcion = @descripcion";
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", strDescripcion);
            this.resultadoObtener(this.connexion);
        }

        public void paginacionPrendaSucursal(string strTipoPrendaId, string strSucursalId) 
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " m.gramaje_id, " +
                " m.codigo, " +
                " m.descripcion, " +
                " sg.precio_tope, "+
                " m.codigo AS  `Código`, " +
                " m.descripcion AS `Descripción` " +                
                " FROM gramajes as m " +                
                " INNER JOIN sucursales_gramajes as sg " +
                " on m.gramaje_id = sg.gramaje_id " +
                " WHERE sg.sucursal_id = @sucursal_id " +
                " AND m.prenda_tipo_id = @tipoPrendaId " +
                " AND m.estatus = 'ACTIVO'";
            this.connexion.mscCmd.Parameters.AddWithValue("@tipoPrendaId", strTipoPrendaId);
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", strSucursalId);
            this.resultadoObtener(this.connexion);
        }
        public void paginacion()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " m.gramaje_id, " +
                " m.codigo, " +
                " m.descripcion, " +
                " m.estatus, " +
                " e.prenda_tipo_id, " +
                " e.codigo AS prendaTipoCodigo, " +
                " e.descripcion AS prendaTipoDescripcion, " +
                " m.codigo AS  `Código`, " +
                " m.descripcion AS `Descripción`, " +
                " e.codigo AS  `Código tipo de prenda`, " +
                " e.descripcion AS `Descripción tipo de prenda`  " +
                " FROM gramajes as m " +
                " INNER JOIN prendas_tipos as e " +
                " ON m.prenda_tipo_id = e.prenda_tipo_id " +
                " ORDER BY m.codigo ASC";
            this.resultadoObtener(this.connexion);
        }
    }
}
