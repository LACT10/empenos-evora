﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlMovimientosCajaDetalles : Core.clsCoreModel
    {

        public clsMdlMovimientosCajaDetalles() { }

        public void buscar(int intMovimientosCajaId)
        {
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText =
                " SELECT " +
                " mcd.movimiento_caja_id, " +
                " mcd.sucursal_id, " +
                " renglon, " +
                " IF(mcd.tipo = 'ENTRADA', 'E', 'S') AS `Tipo`, " +
                " mcd.concepto AS `Concepto`, " +
                " CONCAT('$',FORMAT(importe, 2)) AS Importe " +
                " FROM movimientos_caja_detalles AS mcd " +                
                " WHERE mcd.movimiento_caja_id = @id";
            this.connexion.mscCmd.Parameters.AddWithValue("@id", intMovimientosCajaId);
            this.resultadoObtener(this.connexion);
        }

        public void paginacionCajaId(int intMovimientosCajaId)
        {
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText =
               " SELECT " +
                " mcd.movimiento_caja_id, " +
                " mcd.sucursal_id, " +
                " renglon, " +
                " IF(mcd.tipo = 'ENTRADA', 'E', 'S') AS `Tipo`, " +
                " mcd.concepto AS `Concepto`, " +
                " CONCAT('$',FORMAT(importe, 2)) AS Importe " +
                " FROM movimientos_caja_detalles AS mcd " +                
                " WHERE mcd.movimiento_caja_id = @id";
            this.connexion.mscCmd.Parameters.AddWithValue("@id", intMovimientosCajaId);
            this.resultadoObtener(this.connexion);
        }
    }
}
