﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlUsuarios : Core.clsCoreModel
    {

        public clsMdlUsuarios()
        {

        }

        public void guardar(Properties_Class.clsUsuario usuario)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            this.lsQuerysTraccionesID = new List<string>();
            this.lsQuerysTracciones = new List<string>();
            this.lsQuerysTraccionesElminacionID = new List<string>();

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (usuario.intId > 0)
            {
                connexion.mscCmd.CommandText = "UPDATE usuarios " +
                    "SET  nombre = @nombre ,  apellido_paterno = @apellido_paterno, apellido_materno =  @apellido_materno, telefono_01 =  @telefono_01,telefono_02 =  @telefono_02, correo_electronico = @correo_electronico, usuario = @usuario, contrasena = @contrasena, intentos =  @intentos, autorizar = @autorizar ,modificar_movimientos =  @modificar_movimientos, estatus = @estatus, fecha_actualizacion  = @fecha, usuario_actualizacion = @usuarioCreacion  WHERE usuario_id   = @id";
                if (usuario.bolHuella) 
                {
                    this.lsQuerysTracciones.Add(" UPDATE usuarios_huellas " +
                   " SET huella = @huella " +
                   " WHERE usuario_id = @id ");
                }
                
                this.lsQuerysTraccionesElminacionID.Add(usuario.strQueryCajaUsuarioDelete);
                this.lsQuerysTraccionesElminacionID.Add(usuario.strQueryPermisosUsuarioDelete);
                this.lsQuerysTracciones.Add(usuario.strQueryCajaUsuario);
                this.lsQuerysTracciones.Add(usuario.strQueryPermisosUsuario);
            }
            else
            {
                connexion.mscCmd.CommandText = "INSERT INTO usuarios(nombre, apellido_paterno, apellido_materno, telefono_01, telefono_02, correo_electronico, usuario, contrasena, intentos, autorizar, modificar_movimientos, estatus,fecha_creacion,usuario_creacion)" +
                    "VALUES(@nombre, @apellido_paterno, @apellido_materno, @telefono_01, @telefono_02, @correo_electronico, @usuario, @contrasena, @intentos, @autorizar, @modificar_movimientos,@estatus, @fecha, @usuarioCreacion)";

                this.lsQuerysTraccionesID.Add(" INSERT INTO usuarios_huellas(usuario_id,huella) " +
                   "VALUES(@id, @huella) ");
                this.lsQuerysTraccionesID.Add(usuario.strQueryCajaUsuario);
                this.lsQuerysTraccionesID.Add(usuario.strQueryPermisosUsuario);
            }
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", usuario.intId);
            connexion.mscCmd.Parameters.AddWithValue("@nombre", usuario.strNombre);
            connexion.mscCmd.Parameters.AddWithValue("@apellido_paterno", usuario.strApellidoPaterno);
            connexion.mscCmd.Parameters.AddWithValue("@apellido_materno", usuario.strApellidoMaterno);
            connexion.mscCmd.Parameters.AddWithValue("@telefono_01", usuario.strTelefono01);
            connexion.mscCmd.Parameters.AddWithValue("@telefono_02", usuario.strTelefono02);
            connexion.mscCmd.Parameters.AddWithValue("@correo_electronico", usuario.strCorreoElectronico);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", usuario.strUsuario);
            connexion.mscCmd.Parameters.AddWithValue("@contrasena", usuario.strContrasena);
            connexion.mscCmd.Parameters.AddWithValue("@intentos", usuario.byteIntentos);
            connexion.mscCmd.Parameters.AddWithValue("@autorizar", usuario.strAutorizar);
            connexion.mscCmd.Parameters.AddWithValue("@modificar_movimientos", usuario.strModificarMovimientos);            
            connexion.mscCmd.Parameters.AddWithValue("@estatus", usuario.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", usuario.dtFecha);
            if (usuario.bolHuella) 
            {
                connexion.mscCmd.Parameters.AddWithValue("@huella", usuario.msHuella.ToArray());
            }
            

            if (usuario.intUsuario == 0) 
            {
                connexion.mscCmd.Parameters.AddWithValue("@usuarioCreacion", null);
            } 
            else 
            {
                connexion.mscCmd.Parameters.AddWithValue("@usuarioCreacion", usuario.intUsuario);
            }
           
            //Regesar resultado
            this.resultadoTransaccion(this.connexion); 
        }

        public void eliminar(Properties_Class.clsUsuario usuario)
        {

            connexion.mscCmd.CommandText = "UPDATE usuarios " +
                               "SET  estatus = @estatus, fecha_eliminacion  = @fecha, usuario_eliminacion = @usuario  WHERE usuario_id = @id";
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", usuario.intId);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", usuario.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", usuario.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", usuario.intUsuario);
            this.resultadoObtener(this.connexion);
        }


        /* public void buscar(string strCodigo)
         {

             if (!this.bolResultadoConnexion) return;
             this.connexion.mscCmd.CommandText = "SELECT e.estado_id, e.codigo, e.descripcion FROM usuarios AS e WHERE e.codigo = @codigo";
             this.connexion.mscCmd.Parameters.AddWithValue("@codigo", strCodigo);
             this.resultadoObtener(this.connexion);


         }*/


        public void buscarUsuario(string strUsuario)
        {

            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT u.usuario" +
                " FROM usuarios AS u" +
                " WHERE u.usuario = @usuario";
            this.connexion.mscCmd.Parameters.AddWithValue("@usuario", strUsuario);
            this.resultadoObtener(this.connexion);


        }




        public void paginacion()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " u.usuario_id," +
                " u.nombre, " +
                " u.apellido_paterno, " +
                " u.apellido_materno, " +
                " u.telefono_01, " +
                " u.telefono_02," +
                " u.correo_electronico, " +
                " u.usuario, " +
                " u.contrasena, " +
                " u.intentos, " +
                " u.autorizar, " +
                " u.modificar_movimientos, " +                
                " u.estatus, " +
                " uh.huella, " +
                " u.nombre AS  `Nombre `, " +
                " u.apellido_paterno AS  `Apellido Paterno`, " +
                " u.apellido_materno AS  `Apellido Materno`, " +
                " u.usuario AS  `Usuario ` " +
                " FROM usuarios AS u " +
                " INNER JOIN usuarios_huellas as uh " +
                " ON u.usuario_id = uh.usuario_id  ";
            this.resultadoObtener(this.connexion);
        }
    }
}
