﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlCaja : Core.clsCoreModel
    {

        public clsMdlCaja()
        {
        }


        public void guardar(Properties_Class.clsCaja caja)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (caja.intId > 0)
            {
                connexion.mscCmd.CommandText = "UPDATE cajas " +
                    "SET codigo = @codigo,  descripcion = @descripcion, estatus = @estatus, fecha_actualizacion  = @fecha, usuario_actualizacion = @usuario  WHERE caja_id = @id";
            }
            else
            {
                connexion.mscCmd.CommandText = "INSERT INTO cajas(codigo, descripcion,estatus,fecha_creacion,usuario_creacion)" +
                    "VALUES(@codigo, @descripcion, @estatus, @fecha, @usuario)";
            }
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", caja.intId);
            connexion.mscCmd.Parameters.AddWithValue("@codigo", caja.strCodigo);
            connexion.mscCmd.Parameters.AddWithValue("@descripcion", caja.strDescripcion);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", caja.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", caja.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", caja.intUsuario);
            //Regesar resultado
            this.resultadoGuardar(this.connexion);
        }

        public void eliminar(Properties_Class.clsCaja caja)
        {

            connexion.mscCmd.CommandText = "UPDATE cajas " +
                               "SET  estatus = @estatus, fecha_eliminacion  = @fecha, usuario_eliminacion = @usuario  WHERE caja_id = @id";
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", caja.intId);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", caja.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", caja.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", caja.intUsuario);
            this.resultadoObtener(this.connexion);
        }


        public void buscar(string strCodigo)
        {
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT c.caja_id, c.codigo, c.descripcion, c.estatus FROM cajas AS c WHERE c.codigo = @codigo";
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", strCodigo);
            this.resultadoObtener(this.connexion);
        }

        public void validarDescripcion(string strDescripcion)
        {

            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT c.caja_id, c.codigo, c.descripcion FROM cajas AS c WHERE c.descripcion = @descripcion";
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", strDescripcion);
            this.resultadoObtener(this.connexion);

        }

        public void paginacion()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT c.caja_id, " +
                "c.codigo, " +
                "c.descripcion," +
                " c.estatus, " +
                "c.codigo AS `Código`," +
                " c.descripcion AS `Descripción`" +
                " FROM cajas AS c " +
                " ORDER BY c.codigo ASC";
            this.resultadoObtener(this.connexion);
        }      
    }
}

