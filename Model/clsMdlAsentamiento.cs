﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
   public class clsMdlAsentamiento : Core.clsCoreModel
    {

        public clsMdlAsentamiento()
        {
        }

        public void guardar(Properties_Class.clsAsentamiento asentamiento)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (asentamiento.intId > 0)
            {
                connexion.mscCmd.CommandText = "UPDATE asentamientos " +
                    "SET  municipio_id = @municipiooId , asentamiento_tipo_id = @asentamientosTipos ,descripcion = @descripcion,ciudad = @ciudad, codigo_postal = @codigo_postal, estatus = @estatus, fecha_actualizacion  = @fecha, usuario_actualizacion = @usuario  " +
                    "WHERE asentamiento_id = @id";
            }
            else
            {
                connexion.mscCmd.CommandText = "INSERT INTO asentamientos( municipio_id,asentamiento_tipo_id,descripcion,ciudad,codigo_postal,estatus,fecha_creacion,usuario_creacion)" +
                    "VALUES( @municipiooId,@asentamientosTipos, @descripcion, @ciudad,@codigo_postal,@estatus, @fecha, @usuario)";
            }
            //Asignar valores al commando
            this.connexion.mscCmd.Parameters.AddWithValue("@id", asentamiento.intId);            
            this.connexion.mscCmd.Parameters.AddWithValue("@municipiooId", asentamiento.municipio.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@asentamientosTipos", asentamiento.asentamientosTipos.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", asentamiento.strDescripcion);
            this.connexion.mscCmd.Parameters.AddWithValue("@ciudad", asentamiento.strCiudad);
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo_postal", asentamiento.strCodigoPostal);
            this.connexion.mscCmd.Parameters.AddWithValue("@estatus", asentamiento.strEstatus);
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha", asentamiento.dtFecha);
            this.connexion.mscCmd.Parameters.AddWithValue("@usuario", asentamiento.intUsuario);

         
            //Regesar resultado
            this.resultadoGuardar(this.connexion);


        }

        public void eliminar(Properties_Class.clsAsentamiento asentamiento)
        {

            this.connexion.mscCmd.CommandText = "UPDATE asentamientos " +
                "SET  estatus = @estatus, fecha_eliminacion  = @fecha, usuario_eliminacion = @usuario  " +
                "WHERE asentamiento_id = @id";
            this.connexion.mscCmd.Parameters.AddWithValue("@id", asentamiento.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@estatus", asentamiento.strEstatus);
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha", asentamiento.dtFecha);
            this.connexion.mscCmd.Parameters.AddWithValue("@usuario", asentamiento.intUsuario);
            this.resultadoGuardar(this.connexion);
        }

        public void buscar(string strDescripcion)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " a.asentamiento_id, " +
                " a.descripcion, " +
                " a.estatus, " +
                " m.municipio_id, " +
                " m.codigo AS municipioCodigo, " +
                " m.descripcion AS municipioDescripcion, " +
                " at.asentamiento_tipo_id, " +
                " at.codigo AS asentamientoTipoCodigo, " +
                " at.descripcion AS asentamientoTipoDescripcion, " +
                " a.ciudad, " +
                " a.codigo_postal , " +
                " e.estado_id, " +
                " e.codigo AS estadoCodigo, " +
                " e.descripcion AS estadoDescripcion " +
                "FROM asentamientos AS a " +
                "INNER JOIN municipios as m " +
                "ON a.municipio_id = m.municipio_id " +
                "INNER JOIN asentamientos_tipos as at " +
                "ON a.asentamiento_tipo_id = at.asentamiento_tipo_id " +
                "INNER JOIN estados as e " +
                "ON m.estado_id = e.estado_id" +
                "WHERE a.descripcion = @codigo";
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", strDescripcion);
            this.resultadoObtener(this.connexion);
        }

        public void validarDescripcionMunicipio(string strDescripcion, string strMunicpioId)
        {

            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT " +
                " a.descripcion " +
                " FROM asentamientos AS a " +
                " WHERE a.descripcion = @descripcion" +
                " AND a.municipio_id = @municipioId";
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", strDescripcion);
            this.connexion.mscCmd.Parameters.AddWithValue("@municipioId", strMunicpioId);
            this.resultadoObtener(this.connexion);


        }


        public void paginacion()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " a.asentamiento_id, " +
                " a.descripcion, " +
                " a.estatus, " +
                " m.municipio_id, " +
                " m.codigo AS municipioCodigo, " +
                " m.descripcion AS municipioDescripcion, " +
                " at.asentamiento_tipo_id, " +
                " at.codigo AS asentamientoTipoCodigo, " +
                " at.descripcion AS asentamientoTipoDescripcion,  " +
                " a.ciudad, " +
                " a.codigo_postal, " +
                " e.estado_id, " +
                " e.codigo AS estadoCodigo, " +
                " e.descripcion AS estadoDescripcion , " +
                " a.descripcion AS `Descripción`,   " +
                " e.descripcion AS `Estado`,  " +
                " m.descripcion AS `Municipio`,  " +
                " a.codigo_postal AS `Código postal` ,   " +
                " a.ciudad AS `Ciudad ` , " +                                                
                " m.codigo AS `Código municipio`, " +                
                " at.codigo AS `Código tipo de asentamiento`, " +
                " at.descripcion AS `Tipo de asentamiento`  " +                
               "FROM asentamientos AS a " +
                "INNER JOIN municipios as m " +
                "ON a.municipio_id = m.municipio_id " +
                "INNER JOIN asentamientos_tipos as at " +
                "ON a.asentamiento_tipo_id = at.asentamiento_tipo_id "+
                "INNER JOIN estados as e " +
                "ON m.estado_id = e.estado_id " +
                "ORDER BY a.municipio_id, a.descripcion";
            this.resultadoObtener(this.connexion);
        }
    }
}
