﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace EmpenosEvora.Model
{
    public class clsMdlEstado : Core.clsCoreModel 
    {

        public clsMdlEstado()
        {

        }

        public void guardar(Properties_Class.clsEstado estado)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (estado.intId > 0)
            {
                connexion.mscCmd.CommandText = "UPDATE estados " +
                    "SET codigo = @codigo,  descripcion = @descripcion, estatus = @estatus, fecha_actualizacion  = @fecha, usuario_actualizacion = @usuario  WHERE estado_id = @id";
            }
            else
            {
                connexion.mscCmd.CommandText = "INSERT INTO estados(codigo, descripcion,estatus,fecha_creacion,usuario_creacion)" +
                    "VALUES(@codigo, @descripcion, @estatus, @fecha, @usuario)";
            }
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", estado.intId);
            connexion.mscCmd.Parameters.AddWithValue("@codigo", estado.strCodigo);
            connexion.mscCmd.Parameters.AddWithValue("@descripcion", estado.strDescripcion);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", estado.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", DateTime.Now);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", estado.intUsuario);
            //Regesar resultado
            this.resultadoGuardar(this.connexion);
        }

        public void eliminar(Properties_Class.clsEstado estado)
        {

            connexion.mscCmd.CommandText = "UPDATE estados " +
                               "SET  estatus = @estatus, fecha_eliminacion  = @fecha, usuario_eliminacion = @usuario  WHERE estado_id = @id";
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", estado.intId);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", estado.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", DateTime.Now);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", estado.intUsuario);
            this.resultadoGuardar(this.connexion);
        }

        public void reactivar(Properties_Class.clsEstado estado)
        {

            connexion.mscCmd.CommandText = "UPDATE estados " +
                               "SET  " +
                               " estatus = @estatus, " +
                               " fecha_actualizacion  = @fecha, " +
                               " usuario_actualizacion = @usuario, " +
                               " fecha_eliminacion  = null, " +
                               " usuario_eliminacion = null  " +
                               " WHERE estado_id = @id";
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", estado.intId);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", estado.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", DateTime.Now);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", estado.intUsuario);
            this.resultadoGuardar(this.connexion);
        }


        public void buscar(string strCodigo)
        {

            if (!this.bolResultadoConnexion) return;            
            this.connexion.mscCmd.CommandText = "SELECT e.estado_id, e.codigo, e.descripcion, e.estatus FROM estados AS e WHERE e.codigo = @codigo";
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", strCodigo);                                    
            this.resultadoObtener(this.connexion);
            
                         
        }

        public void validarDescripcion(string strDescripcion)
        {
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT e.estado_id, e.codigo, e.descripcion FROM estados AS e WHERE e.descripcion = @descripcion";
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", strDescripcion);
            this.resultadoObtener(this.connexion);
        }

        public void paginacion()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT e.estado_id, " +
                "e.codigo, " +
                "e.descripcion, " +
                "e.estatus , " +
                "e.codigo AS `Código`, " +
                "e.descripcion AS `Descripción`" +
                " FROM estados AS e " +
                " ORDER BY e.codigo ASC";
            this.resultadoObtener(this.connexion);
        }
    }  
}
