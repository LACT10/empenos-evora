﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlLogin : Core.clsCoreModel
    {

        public clsMdlLogin() 
        { 
        
        }

        public void IncrementarIntentos(Properties_Class.clsLogin clsLogin)
        {
           
            connexion.mscCmd.CommandText = "UPDATE usuarios " +
                  " SET intentos = @intentos, " +
                  " fecha_actualizacion  = @fecha  " +
                  " WHERE usuario_id = @id";
         
            this.connexion.mscCmd.Parameters.AddWithValue("@id", clsLogin.usuario.intId.ToString());
            this.connexion.mscCmd.Parameters.AddWithValue("@intentos", clsLogin.usuario.byteIntentos.ToString());
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha", clsLogin.dtFecha);
            this.resultadoObtener(this.connexion);
        }



        public void reiniciarIntentos(Properties_Class.clsLogin clsLogin)
        {

            connexion.mscCmd.CommandText = "UPDATE usuarios " +
                  " SET intentos = @intentos, " +
                  " fecha_actualizacion  = @fecha  " +
                  " WHERE usuario_id = @id";

            this.connexion.mscCmd.Parameters.AddWithValue("@id", clsLogin.usuario.intId.ToString());
            this.connexion.mscCmd.Parameters.AddWithValue("@intentos", clsLogin.usuario.byteIntentos.ToString());
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha", clsLogin.dtFecha);
            this.resultadoObtener(this.connexion);
        }


        public void cambairEstatus(Properties_Class.clsLogin clsLogin) 
        {


            connexion.mscCmd.CommandText = "UPDATE usuarios " +
                  " SET estatus = @estatus, " +
                  " fecha_actualizacion  = @fecha  " +
                  " WHERE usuario_id = @id";

            this.connexion.mscCmd.Parameters.AddWithValue("@id", clsLogin.usuario.intId.ToString());
            this.connexion.mscCmd.Parameters.AddWithValue("@estatus", clsLogin.usuario.strEstatus.ToString());
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha", clsLogin.dtFecha);
            this.resultadoObtener(this.connexion);
        }
        public void buscarUsuario(string strUsuario)
        {
            connexion.mscCmd.CommandText = "SELECT " +
                               " u.usuario_id," +
                                " u.nombre, " +
                                " u.apellido_paterno, " +
                                " u.apellido_materno, " +
                                " u.telefono_01, " +
                                " u.telefono_02," +
                                " u.correo_electronico, " +
                                " u.usuario, " +
                                " u.contrasena, " +
                                " u.intentos, " +
                                " u.autorizar, " +
                                " u.modificar_movimientos, " +
                                " u.estatus, " +                                
                                " u.nombre AS  `Nombre `, " +
                                " u.apellido_paterno AS  `Apellido Paterno`, " +
                                " u.apellido_materno AS  `Apellido Materno`, " +
                                " u.usuario AS  `Usuario ` " +
                               " FROM usuarios AS u " +
                               " WHERE u.usuario = @usuario";

            this.connexion.mscCmd.Parameters.AddWithValue("@usuario", strUsuario);
            this.resultadoObtener(this.connexion);
        }

        public void buscarPermisosAcessoUsuario(Properties_Class.clsLogin login) 
        {

            connexion.mscCmd.CommandText = " SELECT " +
                                   " up.permisos " +                                 
                                   " FROM usuarios_permisos AS up " +
                                   " WHERE up.usuario_id = @usuarioId " +
                                   " AND up.sucursal_id = @sucursalId ";

            this.connexion.mscCmd.Parameters.AddWithValue("@usuarioId", login.usuario.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursalId", login.intSucursalId);
            this.resultadoObtener(this.connexion);
        }

        public void buscarCajaUsuario(Properties_Class.clsLogin login)
        {

            connexion.mscCmd.CommandText = "SELECT " +
                                   " uc.caja_id," +
                                   " c.descripcion, " +
                                   " c.codigo, " +
                                   " c.descripcion AS  `Cajas ` " +
                                   " FROM usuarios_cajas AS uc " +
                                   " INNER JOIN cajas as c " +
                                   " ON uc.caja_id = c.caja_id " +
                                   " WHERE uc.usuario_id = @usuarioId" +
                                   " AND uc.sucursal_id = @sucursalId" +
                                    " ORDER BY c.codigo ASC";

            this.connexion.mscCmd.Parameters.AddWithValue("@usuarioId", login.usuario.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursalId", login.intSucursalId);
            this.resultadoObtener(this.connexion);
        }

        public void login(string strUsuario, string strContrasena)
        {

            connexion.mscCmd.CommandText = "SELECT " +
                              " u.usuario_id," +
                                " u.nombre, " +
                                " u.apellido_paterno, " +
                                " u.apellido_materno, " +
                                " u.telefono_01, " +
                                " u.telefono_02," +
                                " u.correo_electronico, " +
                                " u.usuario, " +
                                " u.contrasena, " +
                                " u.intentos, " +
                                " u.autorizar, " +
                                " u.modificar_movimientos, " +
                                " u.estatus, " +                                
                                " u.nombre AS  `Nombre `, " +
                                " u.apellido_paterno AS  `Apellido Paterno`, " +
                                " u.apellido_materno AS  `Apellido Materno`, " +
                                " u.usuario AS  `Usuario ` " +
                               " FROM usuarios AS u" +
                               " WHERE u.usuario = @usuario" +
                               " AND u.contrasena = @contrasena" +
                               " AND u.estatus = 'ACTIVO'";
            
            this.connexion.mscCmd.Parameters.AddWithValue("@usuario", strUsuario);
            this.connexion.mscCmd.Parameters.AddWithValue("@contrasena", strContrasena);
            this.resultadoObtener(this.connexion);
        }


        public void BuscarPermisosAcessoProcessoUsuario(Properties_Class.clsLogin login) 
        {
            connexion.mscCmd.CommandText = login.strQueryPermisosAccessoProcessoUsuario;            
            this.resultadoObtener(this.connexion);
        }
    }
}
