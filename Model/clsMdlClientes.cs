﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlClientes : Core.clsCoreModel
    {

        public clsMdlClientes()
        {
        }

        public void guardar(Properties_Class.clsCliente cliente)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            this.lsQuerysTraccionesID = new List<string>();
            this.lsQuerysTracciones = new List<string>();
            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (cliente.intId > 0)
            {
                connexion.mscCmd.CommandText = " UPDATE clientes " +
                    " SET  " +                                        
                    " nombre = @nombre , " +
                    " apellido_paterno = @apellido_paterno , " +
                    " apellido_materno = @apellido_materno , " +
                    " fecha_nacimiento = @fecha_nacimiento , " +
                    " genero = @genero , " +
                    " rfc = @rfc, " +
                    " curp = @curp, " +
                    " telefono_01 = @telefono_01, " +
                    " telefono_02 = @telefono_02, " +
                    " correo_electronico = @correo_electronico, " +
                    " ocupacion_id = @ocupacion_id, " +
                    " identificacion_tipo_id = @identificacion_tipo_id, " +
                    " numero_identificacion = @numero_identificacion, " +
                    " calle = @calle, " +
                    " numero_exterior = @numero_exterior, " +
                    " numero_interior = @numero_interior, " +
                    " asentamiento_id = @asentamiento_id , " +
                    " cotitular_nombre = @cotitular_nombre, " +
                    " cotitular_apellido_paterno = @cotitular_apellido_paterno , " +
                    " cotitular_apellido_materno = @cotitular_apellido_materno , " +
                    " beneficiario_nombre = @beneficiario_nombre, " +
                    " beneficiario_apellido_paterno = @beneficiario_apellido_paterno, " +
                    " beneficiario_apellido_materno = @beneficiario_apellido_materno, " +
                    " comentarios = @comentarios, " +
                    " fotografia_imagen = @fotografia_imagen, " +
                    " identificacion_imagen_frente = @identificacion_imagen_frente, " +
                    " identificacion_imagen_reverso = @identificacion_imagen_reverso,  " +
                    " estatus = @estatus, " +
                    " fecha_actualizacion  = @fecha, usuario_actualizacion = @usuario  " +
                    " WHERE cliente_id = @id ";                
                this.lsQuerysTracciones.Add(" UPDATE clientes_huellas " +
                    " SET huella = @huella " +
                    " WHERE cliente_id = @id " +
                    " AND sucursal_id = @sucursalId ");
            }
            else
            {
                connexion.mscCmd.CommandText = " INSERT INTO clientes( sucursal_id , codigo , nombre , apellido_paterno , apellido_materno ,fecha_nacimiento , genero , rfc, curp, telefono_01, telefono_02, correo_electronico, ocupacion_id, identificacion_tipo_id, calle, numero_exterior, numero_interior, asentamiento_id , cotitular_nombre, cotitular_apellido_paterno , cotitular_apellido_materno , beneficiario_nombre, beneficiario_apellido_paterno, beneficiario_apellido_materno, comentarios, fotografia_imagen, identificacion_imagen_frente, identificacion_imagen_reverso, numero_identificacion,estatus, fecha_creacion, usuario_creacion)" +
                    "VALUES( @sucursalId , @codigo , @nombre , @apellido_paterno , @apellido_materno , @fecha_nacimiento , @genero , @rfc, @curp, @telefono_01, @telefono_02, @correo_electronico, @ocupacion_id, @identificacion_tipo_id, @calle, @numero_exterior, @numero_interior, @asentamiento_id , @cotitular_nombre, @cotitular_apellido_paterno , @cotitular_apellido_materno , @beneficiario_nombre, @beneficiario_apellido_paterno, @beneficiario_apellido_materno, @comentarios, @fotografia_imagen, @identificacion_imagen_frente, @identificacion_imagen_reverso, @numero_identificacion, @estatus, @fecha, @usuario) ";
                this.lsQuerysTraccionesID.Add(" INSERT INTO clientes_huellas(cliente_id, sucursal_id, huella) " +
                    "VALUES(@id, @sucursalId, @huella) ");
            }

            //Asignar valores al commando
            this.connexion.mscCmd.Parameters.AddWithValue("@id", cliente.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursalId", cliente.intSucursal);
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", cliente.strCodigo);
            this.connexion.mscCmd.Parameters.AddWithValue("@nombre", cliente.strNombre);
            this.connexion.mscCmd.Parameters.AddWithValue("@apellido_paterno", cliente.strApellidoPaterno);
            this.connexion.mscCmd.Parameters.AddWithValue("@apellido_materno", cliente.strApellidoMaterno);            
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha_nacimiento", cliente.dtFechaNacimiento);
            this.connexion.mscCmd.Parameters.AddWithValue("@genero", cliente.strGenero);
            this.connexion.mscCmd.Parameters.AddWithValue("@rfc", cliente.strRFC);
            this.connexion.mscCmd.Parameters.AddWithValue("@curp", cliente.strCURP);
            this.connexion.mscCmd.Parameters.AddWithValue("@telefono_01", cliente.strTelefono01);
            this.connexion.mscCmd.Parameters.AddWithValue("@telefono_02", cliente.strTelefono02);
            this.connexion.mscCmd.Parameters.AddWithValue("@correo_electronico", cliente.strCorreoElectronico);
            if (cliente.ocupacion.intId > 1) 
            {
                this.connexion.mscCmd.Parameters.AddWithValue("@ocupacion_id", cliente.ocupacion.intId);
            } 
            else 
            {
                this.connexion.mscCmd.Parameters.AddWithValue("@ocupacion_id", null);
            }
            
            this.connexion.mscCmd.Parameters.AddWithValue("@identificacion_tipo_id", cliente.identificacionesTipo.intId);            
            this.connexion.mscCmd.Parameters.AddWithValue("@numero_identificacion", cliente.strNumeroIdentificacion);
            this.connexion.mscCmd.Parameters.AddWithValue("@calle", cliente.strCalle);            
            this.connexion.mscCmd.Parameters.AddWithValue("@numero_exterior", cliente.strNumeroExterior);
            this.connexion.mscCmd.Parameters.AddWithValue("@numero_interior", cliente.strNumeroInterior);
            this.connexion.mscCmd.Parameters.AddWithValue("@asentamiento_id", cliente.asentamiento.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@cotitular_nombre", cliente.strCotitularNombre);
            this.connexion.mscCmd.Parameters.AddWithValue("@cotitular_apellido_paterno", cliente.strCotitularApellidoPaterno);
            this.connexion.mscCmd.Parameters.AddWithValue("@cotitular_apellido_materno", cliente.strCotitularApellidoMaterno);
            this.connexion.mscCmd.Parameters.AddWithValue("@beneficiario_nombre", cliente.strBeneficiarioNombre);
            this.connexion.mscCmd.Parameters.AddWithValue("@beneficiario_apellido_paterno", cliente.strBeneficiarioApellidoPaterno);
            this.connexion.mscCmd.Parameters.AddWithValue("@beneficiario_apellido_materno", cliente.strBeneficiarioApellidoMaterno);
            this.connexion.mscCmd.Parameters.AddWithValue("@comentarios", cliente.strComentario);
            this.connexion.mscCmd.Parameters.AddWithValue("@fotografia_imagen", cliente.strFotografiaImagen);
            this.connexion.mscCmd.Parameters.AddWithValue("@identificacion_imagen_frente", cliente.strIdentificacionImagenFrente);
            this.connexion.mscCmd.Parameters.AddWithValue("@identificacion_imagen_reverso", cliente.strIdentificacionImagenReverso);
            this.connexion.mscCmd.Parameters.AddWithValue("@estatus", cliente.strEstatus);
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha", cliente.dtFecha);
            this.connexion.mscCmd.Parameters.AddWithValue("@usuario", cliente.intUsuario);
            
            this.connexion.mscCmd.Parameters.AddWithValue("@huella", cliente.msHuella.ToArray());

            this.resultadoTransaccion(this.connexion);

        }

        public void eliminar(Properties_Class.clsCliente cliente)
        {

            this.connexion.mscCmd.CommandText = " UPDATE clientes " +
                " SET  estatus = @estatus, fecha_eliminacion  = @fecha, usuario_eliminacion = @usuario  " +
                " WHERE cliente_id = @id ";
            this.connexion.mscCmd.Parameters.AddWithValue("@id", cliente.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@estatus", cliente.strEstatus);
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha", cliente.dtFecha);
            this.connexion.mscCmd.Parameters.AddWithValue("@usuario", cliente.intUsuario);
            this.resultadoGuardar(this.connexion);
        }

        public void buscar(string strCodigo)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT " +
                " c.cliente_id, " +                
                " c.codigo, " +
                " c.sucursal_id, " +
                " c.nombre, " +
                " c.apellido_paterno, " +
                " c.apellido_materno, " +
                " c.fecha_nacimiento, " +
                " c.genero, " +
                " c.rfc, " +
                " c.curp, " +
                " c.telefono_01, " +
                " c.telefono_02, " +
                " c.correo_electronico," +
                " c.calle, " +
                " c.numero_exterior, " +
                " c.numero_interior, " +
                " c.cotitular_nombre, " +
                " c.cotitular_apellido_paterno, " +
                " c.cotitular_apellido_materno, " +
                " c.beneficiario_nombre, " +
                " c.beneficiario_apellido_paterno, " +
                " c.beneficiario_apellido_materno, " +
                " c.comentarios, " +
                " c.fotografia_imagen, " +
                " c.identificacion_imagen_frente, " +
                " c.identificacion_imagen_reverso, " +
                " o.ocupacion_id, " +
                " o.codigo AS ocupacionCodigo, " +
                " o.descripcion AS ocupacionDescripcion, " +
                " it.identificacion_tipo_id, " +
                " it.codigo AS identificacionesTiposCodigo, " +
                " it.descripcion AS identificacionesTiposDescripcion, " +
                " a.asentamiento_id," +
                " a.descripcion, " +
                " a.ciudad, " +
                " a.codigo_postal ,  " +
                " m.municipio_id, " +
                " m.codigo AS municipioCodigo, " +
                " m.descripcion AS municipioDescripcion,  " +                
                " e.estado_id, " +
                " e.codigo AS estadoCodigo, " +
                " e.descripcion AS estadoDescripcion, "+
                " c.estatus, " +
                " c.numero_identificacion, " +
                " ch.huella" +                
                " FROM clientes AS c " +               
                " INNER JOIN identificaciones_tipos as it " +
                " ON c.identificacion_tipo_id = it.identificacion_tipo_id " +
                " INNER JOIN asentamientos as a " +
                " ON c.asentamiento_id = a.asentamiento_id " +
                " INNER JOIN municipios as m " +
                " ON a.municipio_id = m.municipio_id " +
                " INNER JOIN asentamientos_tipos as at " +
                " ON a.asentamiento_tipo_id = at.asentamiento_tipo_id " +
                " INNER JOIN estados as e " +
                " ON m.estado_id = e.estado_id " +
                " LEFT JOIN clientes_huellas as ch " +
                " ON c.cliente_id = ch.cliente_id " +
                " LEFT JOIN ocupaciones as o " +
                " ON c.ocupacion_id = o.ocupacion_id " +
                " WHERE c.codigo = @codigo ";
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", strCodigo);
            this.resultadoObtener(this.connexion);
        }

        public void buscarCurp(Properties_Class.clsCliente cliente) 
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT " +                
                " c.curp " +                           
                " FROM clientes AS c " +
                " WHERE c.curp = @curp ";
            this.connexion.mscCmd.Parameters.AddWithValue("@curp", cliente.strCURP);
            this.resultadoObtener(this.connexion);
        }
        public void paginacion()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT c.cliente_id, " +
                " c.codigo, " +
                " c.sucursal_id, " +
                " c.nombre, " +
                " c.apellido_paterno, " +
                " c.apellido_materno, " +
                " c.fecha_nacimiento, " +
                " c.genero, " +
                " c.rfc, " +
                " c.curp, " +
                " c.telefono_01, " +
                " c.telefono_02, " +
                " c.correo_electronico," +
                " c.calle, " +
                " c.numero_exterior, " +
                " c.numero_interior, " +
                " c.cotitular_nombre, " +
                " c.cotitular_apellido_paterno, " +
                " c.cotitular_apellido_materno, " +
                " c.beneficiario_nombre, " +
                " c.beneficiario_apellido_paterno, " +
                " c.beneficiario_apellido_materno, " +
                " c.comentarios, " +
                " c.fotografia_imagen, " +
                " c.identificacion_imagen_frente, " +
                " c.identificacion_imagen_reverso, " +
                " o.ocupacion_id, " +
                " o.codigo AS ocupacionCodigo, " +
                " o.descripcion AS ocupacionDescripcion, " +
                " it.identificacion_tipo_id, " +
                " it.codigo AS identificacionesTiposCodigo, " +
                " it.descripcion AS identificacionesTiposDescripcion, " +
                " a.asentamiento_id," +
                " a.descripcion AS asentamientoDescripcion, " +
                " a.ciudad, " +
                " a.codigo_postal,  " +
                " m.municipio_id, " +
                " m.codigo AS municipioCodigo, " +
                " m.descripcion AS municipioDescripcion,  " +
                " e.estado_id, " +
                " e.codigo AS estadoCodigo, " +
                " e.descripcion AS estadoDescripcion, " +
                " c.estatus, " +
                " c.numero_identificacion, " +
                " ch.huella, " +                
                " c.codigo AS  `Código `, " +
                " CONCAT(c.nombre, ' ', c.apellido_paterno, ' ', c.apellido_materno) AS  `Nombre `, " +
                " c.estatus AS `Estatus ` " +
                " FROM clientes AS c " +               
                " INNER JOIN identificaciones_tipos as it " +
                " ON c.identificacion_tipo_id = it.identificacion_tipo_id " +
                " INNER JOIN asentamientos as a " +
                " ON c.asentamiento_id = a.asentamiento_id " +
                " INNER JOIN municipios as m " +
                " ON a.municipio_id = m.municipio_id " +
                " INNER JOIN asentamientos_tipos as at " +
                " ON a.asentamiento_tipo_id = at.asentamiento_tipo_id " +
                " INNER JOIN estados as e " +
                " ON m.estado_id = e.estado_id " +
                " INNER JOIN clientes_huellas as ch " +
                " ON c.cliente_id = ch.cliente_id " +                
                " LEFT JOIN ocupaciones as o " +
                " ON c.ocupacion_id = o.ocupacion_id  " +
                " ORDER BY c.codigo ASC";
            this.resultadoObtener(this.connexion);
        }
    }
}
