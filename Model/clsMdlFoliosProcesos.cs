﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlFoliosProcesos : Core.clsCoreModel
    {

        public clsMdlFoliosProcesos() { }



        public void guardar(Properties_Class.clsFoliosProcesos foliosProcesos)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (foliosProcesos.intId > 0)
            {
                connexion.mscCmd.CommandText = "UPDATE folios_procesos " +
                    " SET sucursal_id = @sucursal_id,  folio_id = @folio_id, proceso_id = @proceso_id , fecha_actualizacion  = @fecha, usuario_actualizacion = @usuario  " +
                    " WHERE sucursal_id = @sucursal_id AND folio_id = @folio_id AND proceso_id = @proceso_id ";
            }
            else
            {
                connexion.mscCmd.CommandText = "INSERT INTO folios_procesos(sucursal_id, folio_id, proceso_id, fecha_creacion,usuario_creacion)" +
                    "VALUES(@sucursal_id, @folio_id, @proceso_id, @fecha, @usuario)";
            }


            //Asignar valores al commando
            
            connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", foliosProcesos.intSucursal);
            connexion.mscCmd.Parameters.AddWithValue("@folio_id", foliosProcesos.folio.intId);
            connexion.mscCmd.Parameters.AddWithValue("@proceso_id", foliosProcesos.procesos.intId);                        
            connexion.mscCmd.Parameters.AddWithValue("@fecha", foliosProcesos.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", foliosProcesos.intUsuario);
            //Regesar resultado
            this.resultadoGuardar(this.connexion);
        }

        public void eliminar(Properties_Class.clsFoliosProcesos foliosProcesos)
        {

            connexion.mscCmd.CommandText = "UPDATE folios_procesos " +
                               "SET   fecha_eliminacion  = @fecha, usuario_eliminacion = @usuario  "+
                                " WHERE sucursal_id = @sucursal_id AND folio_id = @folio_id AND proceso_id = @proceso_id "; 
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", foliosProcesos.intSucursal);
            connexion.mscCmd.Parameters.AddWithValue("@folio_id", foliosProcesos.folio.intId);
            connexion.mscCmd.Parameters.AddWithValue("@proceso_id", foliosProcesos.procesos.intId);            
            connexion.mscCmd.Parameters.AddWithValue("@fecha", foliosProcesos.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", foliosProcesos.intUsuario);
            this.resultadoObtener(this.connexion);
        }


        


        public void buscarProcesoFolio(Properties_Class.clsFoliosProcesos foliosProcesos)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " fp.sucursal_id, " +
                " fp.folio_id, " +
                " fp.proceso_id " +
                " FROM folios_procesos AS fp " +
                " WHERE fp.folio_id = @folio_id AND fp.proceso_id = @proceso_id ";
            // " WHERE fp.sucursal_id = @sucursal_id AND fp.folio_id = @folio_id AND fp.proceso_id = @proceso_id ";
            connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", foliosProcesos.intSucursal);
            connexion.mscCmd.Parameters.AddWithValue("@folio_id", foliosProcesos.folio.intId);
            connexion.mscCmd.Parameters.AddWithValue("@proceso_id", foliosProcesos.procesos.intId);

            this.resultadoObtener(this.connexion);
        }


        public void buscarFolioProcesoMenu(int intProcesoId, int intSucursalId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +                
                " fp.folio_id " +                
                " FROM folios_procesos AS fp " +
                " WHERE fp.sucursal_id = @sucursal_id  AND fp.proceso_id = @proceso_id ";            
            connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", intSucursalId);
            
            connexion.mscCmd.Parameters.AddWithValue("@proceso_id", intProcesoId);

            this.resultadoObtener(this.connexion);
        }


        public void incrementarConsecutivo(int intFolioId) 
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " UPDATE " +
                " folios " +
                " SET consecutivo = (consecutivo +1) WHERE folio_id = @folio_id";            

            connexion.mscCmd.Parameters.AddWithValue("@folio_id", intFolioId);

            this.resultadoObtener(this.connexion);
        }

        public void paginacion(Properties_Class.clsFoliosProcesos foliosProcesos)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " fp.sucursal_id, " +
                " fp.folio_id, " +
                " f.descripcion, " +
                " f.serie, " +
                " f.consecutivo," +
                " fp.proceso_id," +
                " p.descripcion AS proceso, " +                
                " f.descripcion AS `Folio`," +
                " f.serie AS `Serie `," +
                " f.consecutivo AS `Consecutivo`, " +
                " p.descripcion AS `Proceso ` " +
                " FROM folios_procesos AS fp " +
                " INNER JOIN folios AS f " +
                " ON fp.folio_id = f.folio_id " +
                " INNER JOIN procesos AS p " +
                " ON fp.proceso_id = p.proceso_id "+
                " WHERE fp.sucursal_id = @sucursal_id";
                // " WHERE fp.sucursal_id = @sucursal_id AND fp.folio_id = @folio_id AND fp.proceso_id = @proceso_id ";
                connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", foliosProcesos.intSucursal);
                connexion.mscCmd.Parameters.AddWithValue("@folio_id", foliosProcesos.folio.intId);
                connexion.mscCmd.Parameters.AddWithValue("@proceso_id", foliosProcesos.procesos.intId);

            this.resultadoObtener(this.connexion);
        }

        
    }
}
