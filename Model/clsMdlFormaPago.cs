﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlFormaPago : Core.clsCoreModel
    {
        public clsMdlFormaPago()
        {

        }

        public void guardar(Properties_Class.clsFormPago formaPago)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (formaPago.intId > 0)
            {
                connexion.mscCmd.CommandText = "UPDATE formas_pago " +
                    "SET codigo = @codigo,  descripcion = @descripcion, estatus = @estatus, fecha_actualizacion  = @fecha, usuario_actualizacion = @usuario  WHERE forma_pago_id = @id";
            }
            else
            {
                connexion.mscCmd.CommandText = "INSERT INTO formas_pago (codigo, descripcion,estatus,fecha_creacion,usuario_creacion)" +
                    "VALUES(@codigo, @descripcion, @estatus, @fecha, @usuario)";
            }
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", formaPago.intId);
            connexion.mscCmd.Parameters.AddWithValue("@codigo", formaPago.strCodigo);
            connexion.mscCmd.Parameters.AddWithValue("@descripcion", formaPago.strDescripcion);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", formaPago.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", formaPago.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", formaPago.intUsuario);
            //Regesar resultado
            this.resultadoGuardar(this.connexion);
        }

        public void eliminar(Properties_Class.clsFormPago formaPago)
        {

            connexion.mscCmd.CommandText = "UPDATE formas_pago " +
                               "SET  estatus = @estatus, fecha_eliminacion  = @fecha, usuario_eliminacion = @usuario  WHERE forma_pago_id = @id";
            //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", formaPago.intId);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", formaPago.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", formaPago.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", formaPago.intUsuario);
            this.resultadoObtener(this.connexion);
        }


        public void buscar(string strCodigo)
        {

            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT fp.forma_pago_id, fp.codigo, fp.descripcion FROM formas_pago AS fp WHERE fp.codigo = @codigo";
            this.connexion.mscCmd.Parameters.AddWithValue("@codigo", strCodigo);
            this.resultadoObtener(this.connexion);


        }

        public void validarDescripcion(string strDescripcion)
        {
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT fp.forma_pago_id, fp.codigo, fp.descripcion FROM formas_pago AS fp WHERE fp.descripcion = @descripcion";
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", strDescripcion);
            this.resultadoObtener(this.connexion);
        }

        public void paginacion()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT fp.forma_pago_id, " +
                "fp.codigo, " +
                "fp.descripcion, " +
                "fp.estatus, " +
                "fp.codigo AS `Código`, " +
                "fp.descripcion AS `Descripción`" +
                " FROM formas_pago AS fp " +
                " ORDER BY fp.codigo ASC";
            this.resultadoObtener(this.connexion);
        }
    }
}
