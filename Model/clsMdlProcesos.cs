﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlProcesos : Core.clsCoreModel
    {

        public clsMdlProcesos()
        {
        }

        public void guardar(Properties_Class.clsProcesos procesos)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;

            this.lsQuerysTracciones = new List<string>();
            this.lsQuerysTraccionesID = new List<string>();
            this.lsQuerysTraccionesElminacionID = new List<string>();

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
            if (procesos.intId > 0)
            {
                this.connexion.mscCmd.CommandText = "UPDATE procesos " +
                    " SET " +
                    " proceso_padre_id = @proceso_padre_id, " +
                    " descripcion = @descripcion, " +
                    " nombre_menu = @nombre_menu,   " +
                    " estatus = @estatus, " +
                    " fecha_actualizacion  = @fecha, " +
                    " usuario_actualizacion = @usuario  " +
                    " WHERE proceso_id = @id";
                /*this.lsQuerysTraccionesElminacionID.Add("DELETE FROM subprocesos " +
                    " WHERE proceso_id = @id;");                */
                this.lsQuerysTracciones.Add(procesos.strQuerySubProcesos);
                this.lsQuerysTracciones.Add(procesos.strQuerySubProcesosActualizar);
                this.lsQuerysTraccionesElminacionID.Add(procesos.strQuerySubProcesosEliminar);
                
            }
            else
            {
                this.connexion.mscCmd.CommandText = " INSERT INTO  procesos(proceso_padre_id, descripcion, nombre_menu,  estatus, fecha_creacion, usuario_creacion) " +
                                                    " VALUES(@proceso_padre_id, @descripcion, @nombre_menu, @estatus, @fecha, @usuario) "; 
                this.lsQuerysTraccionesID.Add(procesos.strQuerySubProcesos); 
                
            }

            //Asignar valores al commando
            this.connexion.mscCmd.Parameters.AddWithValue("@id", procesos.intId);
            if (procesos.intProcesoPadreId == 0)
            {
                this.connexion.mscCmd.Parameters.AddWithValue("@proceso_padre_id", null);
            }
            else 
            {
                this.connexion.mscCmd.Parameters.AddWithValue("@proceso_padre_id", procesos.intProcesoPadreId);
            }
            
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", procesos.strDescripcion);
            this.connexion.mscCmd.Parameters.AddWithValue("@nombre_menu", procesos.strNombreMenu);
            this.connexion.mscCmd.Parameters.AddWithValue("@estatus", procesos.strEstatus);
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha", procesos.dtFecha);
            this.connexion.mscCmd.Parameters.AddWithValue("@usuario", procesos.intUsuario);            
            
            this.resultadoTransaccion(this.connexion);
        }

        public void eliminar(Properties_Class.clsProcesos procesos)
        {

            this.connexion.mscCmd.CommandText = "UPDATE procesos " +
                "SET  estatus = @estatus, fecha_eliminacion  = @fecha, usuario_eliminacion = @usuario  " +
                "WHERE proceso_id = @id";
            this.connexion.mscCmd.Parameters.AddWithValue("@id", procesos.intId);
            this.connexion.mscCmd.Parameters.AddWithValue("@estatus", procesos.strEstatus);
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha", procesos.dtFecha);
            this.connexion.mscCmd.Parameters.AddWithValue("@usuario", procesos.intUsuario);
            this.resultadoGuardar(this.connexion);
        }

        public void buscar(string strId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " p.proceso_id, " +
                " IFNULL(p.proceso_padre_id, 0) AS proceso_padre_id, " +
                " p.descripcion, " +
                " p.nombre_menu " +
                " FROM procesos AS p " +
               " LEFT JOIN procesos as pp " +
                " ON p.proceso_padre_id = pp.proceso_id " +
            "WHERE p.proceso_id = @id";
            this.connexion.mscCmd.Parameters.AddWithValue("@id", strId);
            this.resultadoObtener(this.connexion);
        }

        public void buscarDescripcion(string strDescripcion)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " p.proceso_id, " +                             
                " p.nombre_menu " +
                " FROM procesos AS p " +              
                " WHERE p.descripcion = @descripcion";
            this.connexion.mscCmd.Parameters.AddWithValue("@descripcion", strDescripcion);
            this.resultadoObtener(this.connexion);
        }

        

        public void proceosos()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " p.proceso_id," +
                " IFNULL(p.proceso_padre_id, 0) AS proceso_padre_id, " +
                " p.descripcion " +
                " FROM procesos AS p ";
            this.resultadoObtener(this.connexion);
        }
        public void proceososUsuario(int intUsuarioId, int intSucursalesId)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " up.usuario_id," +
                " up.sucursal_id," +
                " up.permisos " +
                " FROM usuarios_permisos AS up " +
                " WHERE up.usuario_id = @id " +
                " AND up.sucursal_id = @sucursalId";

            this.connexion.mscCmd.Parameters.AddWithValue("@id", intUsuarioId);
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursalId", intSucursalesId);
            this.resultadoObtener(this.connexion);
        }

        public void paginacion()
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT " +
                " p.proceso_id," +
                " IFNULL(p.proceso_padre_id, 0) AS proceso_padre_id, " +
                " p.descripcion, " +
                " p.nombre_menu, " +
                " pp.descripcion AS procesosDescripcion, " +
                " p.descripcion AS  `Descripción`, " +
                " p.nombre_menu AS  `Nombre de menu` " +
                " FROM procesos AS p " +
                " LEFT JOIN procesos as pp " +
                " ON p.proceso_padre_id = pp.proceso_id "  ;
            this.resultadoObtener(this.connexion);
        }


     

    }
}
