﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlCorteCaja : Core.clsCoreModel
    {

        public clsMdlCorteCaja() { }

        public void guardar(Properties_Class.clsCorteCaja corteCaja)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.lsQuerysTracciones = new List<string>();
            this.lsQuerysTraccionesID = new List<string>();
            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo
                       
            connexion.mscCmd.CommandText = " INSERT INTO cajas_corte(  sucursal_id, caja_id , fecha , usuario_realiza , tipo , saldo_inicial , saldo_final, mil , quinientos, doscientos, cien, cincuenta, veinte, diez, cinco, dos, uno, cincuenta_centavos, veinte_centavos, diez_centavos, cinco_centavos, estatus, fecha_creacion, usuario_creacion) " +
                " VALUES( @sucursal, @caja_id , @fecha_corte , @usuario_realiza , @tipo , @saldo_inicial , @saldo_final, @mil , @quinientos, @doscientos, @cien, @cincuenta, @veinte, @diez, @cinco, @dos, @uno, @cincuenta_centavos, @veinte_centavos, @diez_centavos, @cinco_centavos, @estatus, @fecha, @usuario) ";
            this.lsQuerysTraccionesID.Add(corteCaja.strQueryBoletaEmpeno);
            this.lsQuerysTraccionesID.Add(corteCaja.strQueryBoletaEmpenoRefrendos);
            this.lsQuerysTraccionesID.Add(corteCaja.strQueryBoletaEmpenoSinPago);
            this.lsQuerysTraccionesID.Add(corteCaja.strQueryPago);
            this.lsQuerysTraccionesID.Add(corteCaja.strQueryMovimientoEntradadCaja);
            this.lsQuerysTraccionesID.Add(corteCaja.strQueryMovimientoSalidadCaja);

            this.lsQuerysTraccionesID.Add(corteCaja.strQueryCajaCorteDetallesBoletasId);
            this.lsQuerysTraccionesID.Add(corteCaja.strQueryCajaCorteDetallesBoletasRefrendosId);
            this.lsQuerysTraccionesID.Add(corteCaja.strQueryCajaCorteDetallesBoletasSinPagoId);
            this.lsQuerysTraccionesID.Add(corteCaja.strQueryCajaCorteDetallesPagoId);
            this.lsQuerysTraccionesID.Add(corteCaja.strQueryCajaCorteDetallesMovimientoEntradadId);
            this.lsQuerysTraccionesID.Add(corteCaja.strQueryCajaCorteDetallesMovimientoSalidadId);




            //Parametros de la tabla principal
            connexion.mscCmd.Parameters.AddWithValue("@caja_id", corteCaja.caja.intId);
            connexion.mscCmd.Parameters.AddWithValue("@fecha_corte", corteCaja.dtFechaCorte);
            connexion.mscCmd.Parameters.AddWithValue("@usuario_realiza", corteCaja.intUsuario);
            connexion.mscCmd.Parameters.AddWithValue("@tipo", corteCaja.strTipo);
            connexion.mscCmd.Parameters.AddWithValue("@saldo_inicial", corteCaja.decSaldoInicial);
            connexion.mscCmd.Parameters.AddWithValue("@saldo_final", corteCaja.decSaldoFinal);
            connexion.mscCmd.Parameters.AddWithValue("@mil", corteCaja.intBilletes1000 );
            connexion.mscCmd.Parameters.AddWithValue("@quinientos", corteCaja.intBilletes500 );
            connexion.mscCmd.Parameters.AddWithValue("@doscientos", corteCaja.intBilletes200);
            connexion.mscCmd.Parameters.AddWithValue("@cien", corteCaja.intBilletes100);
            connexion.mscCmd.Parameters.AddWithValue("@cincuenta", corteCaja.intBilletes50);
            connexion.mscCmd.Parameters.AddWithValue("@veinte", corteCaja.intBilletes20);
            connexion.mscCmd.Parameters.AddWithValue("@diez", corteCaja.intMonedas10);
            connexion.mscCmd.Parameters.AddWithValue("@cinco", corteCaja.intMonedas5);
            connexion.mscCmd.Parameters.AddWithValue("@dos", corteCaja.intMonedas2);
            connexion.mscCmd.Parameters.AddWithValue("@uno", corteCaja.intMonedas1);
            connexion.mscCmd.Parameters.AddWithValue("@cincuenta_centavos", corteCaja.intMonedas50c);
            connexion.mscCmd.Parameters.AddWithValue("@veinte_centavos", corteCaja.intMonedas20c);
            connexion.mscCmd.Parameters.AddWithValue("@diez_centavos", corteCaja.intMonedas10c);
            connexion.mscCmd.Parameters.AddWithValue("@cinco_centavos", corteCaja.intMonedas5c);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", corteCaja.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", DateTime.Now);
            connexion.mscCmd.Parameters.AddWithValue("@sucursal", corteCaja.intSucursal);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", corteCaja.intUsuario);

    
            //connexion.mscCmd.Parameters.AddWithValue("@usuarioAutorizacion", strUsuarioAutorizacion);
          
            //Regesar resultado
            this.resultadoTransaccion(this.connexion);

        }
        public void cancelar(Properties_Class.clsCorteCaja corteCaja)
        {
            if (!this.bolResultadoConnexion) return;
            this.lsQuerysTracciones = new List<string>();


            this.connexion.mscCmd.CommandText = "UPDATE boletas_empeno " +
                " SET estatus = 'CANCELADA'," +
                " motivo_cancelacion = @motivo, " +
                " fecha_eliminacion = @fecha ," +
                " usuario_eliminacion = @usuario, " +
                " gramos = 0, " +
                " importe = 0," +
                " importe_avaluo = 0" +
                " WHERE boleta_empeno_id = @boleta_id";

            //connexion.mscCmd.Parameters.AddWithValue("@motivo", corteCaja.strMotivosCancelacion);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", corteCaja.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", corteCaja.intUsuario);
            connexion.mscCmd.Parameters.AddWithValue("@boleta_id", corteCaja.intId);

            //Regesar resultado
            this.resultadoTransaccion(this.connexion);
        }

        public void reactivar(Properties_Class.clsCorteCaja corteCaja)
        {
            if (!this.bolResultadoConnexion) return;

            this.connexion.mscCmd.CommandText = "UPDATE boletas_empeno " +
                " SET estatus = 'ACTIVO'," +
                " fecha_actualizacion = @fecha ," +
                " usuario_actualizacion = @usuario, " +
                " fecha_adjudicacion = null," +
                " usuario_adjudicacion = null" +
                " WHERE boleta_empeno_id = @boleta_id";
            connexion.mscCmd.Parameters.AddWithValue("@fecha", corteCaja.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", corteCaja.intUsuario);
            connexion.mscCmd.Parameters.AddWithValue("@boleta_id", corteCaja.intId);

            //Regesar resultado
            this.resultadoTransaccion(this.connexion);
        }

        public void ultimoCorteCierre(int intSucursalId) 
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT " +
                         " cc.fecha AS ultimoFecha, " +
                         " cc.saldo_final " +
                         " FROM cajas_corte AS cc " +
                         " WHERE cc.sucursal_id = @sucursal_id " +
                         " AND cc.tipo = 'CIERRE' " +
                         " AND cc.estatus = 'ACTIVO' " +
                         " AND cc.fecha = (SELECT MAX(ccM.fecha) " +
                                " FROM cajas_corte AS ccM " +
                         " WHERE ccM.sucursal_id = @sucursal_id " +
                         " AND ccM.tipo = 'CIERRE' " +
                         " AND ccM.estatus = 'ACTIVO') ";
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", intSucursalId);
            this.resultadoObtener(this.connexion);
        }

        public void buscarCaja(int intSucursalId, int intCajaId, DateTime dtFechaCorte)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT  " +
                         " IFNULL((SUM(be.importe)), 0) AS capital, " +
                         " GROUP_CONCAT(be.boleta_empeno_id ORDER BY be.boleta_empeno_id SEPARATOR ',') AS lista_boletas_id ," +                         
                          " IFNULL((SELECT " +
                                " SUM(pd.abono_capital) AS abono_capital_total " +
                                " FROM pagos  AS p " +
                                " INNER JOIN pagos_detalles AS pd " +
                                " ON p.pago_id = pd.pago_id " +
                                " INNER JOIN boletas_empeno AS be " +
                                " on pd.boleta_empeno_id = be.boleta_empeno_id" +
                                " WHERE p.estatus = 'ACTIVO' " +
                                " AND p.tipo_recibo = 'REFRENDO'" +
                                " AND p.sucursal_id = @sucursal_id " +
                                " AND p.caja_id = @caja_id " +
                                " AND  DATE_FORMAT(p.fecha, '%Y-%m-%d') = @fecha_corte " +
                                " AND be.caja_corte_id IS null ),0) AS abono_capital_total, " +
                         " IFNULL((SELECT " +
                                " SUM(pdct.importe) AS intereses_total " +
                                " FROM pagos  AS p " +
                                " INNER JOIN pagos_detalles AS pd " +
                                " ON p.pago_id = pd.pago_id " +
                                " INNER JOIN pagos_detalles_cobros_tipos AS pdct " +
                                " ON pd.renglon = pdct.renglon_detalle AND pd.pago_id = pdct.pago_id " +
                                " INNER JOIN boletas_empeno AS b " +
                                " on pd.boleta_empeno_id = b.boleta_empeno_id " +
                              " WHERE p.estatus = 'ACTIVO' " +
                              " AND (p.tipo_recibo = 'LIQUIDACION' OR  p.tipo_recibo = 'REFRENDO')" +
                              " AND p.sucursal_id = @sucursal_id " +
                              " AND p.caja_id = @caja_id " +
                              " AND pdct.tipo = 'INTERESES' " +
                              " AND b.caja_corte_id IS null " +
                              " AND  DATE_FORMAT(p.fecha, '%Y-%m-%d') = @fecha_corte " +
                              " AND pdct.cobro_tipo_id = 1),0) AS intereses, " +
                        " IFNULL((SELECT " +
                               " SUM(pdct.importe) AS almacenaje_total " +
                               " FROM pagos  AS p " +
                               " INNER JOIN pagos_detalles AS pd " +
                               " ON p.pago_id = pd.pago_id " +
                               " INNER JOIN pagos_detalles_cobros_tipos AS pdct " +
                               " ON pd.renglon = pdct.renglon_detalle AND pd.pago_id = pdct.pago_id " +
                               " INNER JOIN boletas_empeno AS b " +
                               " on pd.boleta_empeno_id = b.boleta_empeno_id " +
                            " WHERE p.estatus = 'ACTIVO' " +
                           " AND (p.tipo_recibo = 'LIQUIDACION' OR  p.tipo_recibo = 'REFRENDO')" +
                            " AND p.sucursal_id = @sucursal_id " +
                            " AND p.caja_id = @caja_id " +
                            " AND pdct.tipo = 'INTERESES' " +
                            " AND b.caja_corte_id IS null " +
                            " AND  DATE_FORMAT(p.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " AND pdct.cobro_tipo_id = 2),0) AS almacenaje, " +
                       " IFNULL((SELECT " +
                            " SUM(pdct.importe) AS manejo_total " +
                            " FROM pagos  AS p " +
                              " INNER JOIN pagos_detalles AS pd " +
                              " ON p.pago_id = pd.pago_id " +
                              " INNER JOIN pagos_detalles_cobros_tipos AS pdct " +
                              " ON pd.renglon = pdct.renglon_detalle AND pd.pago_id = pdct.pago_id " +
                              " INNER JOIN boletas_empeno AS b " +
                              " on pd.boleta_empeno_id = b.boleta_empeno_id " +
                           " WHERE p.estatus = 'ACTIVO' " +
                           " AND (p.tipo_recibo = 'LIQUIDACION' OR  p.tipo_recibo = 'REFRENDO')" +
                           " AND p.sucursal_id = @sucursal_id " +
                           " AND p.caja_id = @caja_id " +
                           " AND pdct.tipo = 'INTERESES' " +
                           " AND b.caja_corte_id IS null " +
                           " AND  DATE_FORMAT(p.fecha, '%Y-%m-%d') = @fecha_corte " +
                           " AND pdct.cobro_tipo_id = 3),0) AS manejo, " +
                       " IFNULL((SELECT " +
                            " SUM(pdct.importe) AS bonificacion_total " +
                            " FROM pagos  AS p " +
                            " INNER JOIN pagos_detalles AS pd " +
                            " ON p.pago_id = pd.pago_id " +
                            " INNER JOIN pagos_detalles_cobros_tipos AS pdct " +
                            " ON pd.renglon = pdct.renglon_detalle AND pd.pago_id = pdct.pago_id " +
                            " INNER JOIN boletas_empeno AS b " +
                            " on pd.boleta_empeno_id = b.boleta_empeno_id " +
                            " WHERE p.estatus = 'ACTIVO' " +
                            " AND (p.tipo_recibo = 'LIQUIDACION' OR  p.tipo_recibo = 'REFRENDO')" +
                            " AND p.sucursal_id = @sucursal_id " +
                            " AND p.caja_id = @caja_id " +
                            " AND b.caja_corte_id IS null " +
                            " AND  DATE_FORMAT(p.fecha, '%Y-%m-%d') = @fecha_corte" +
                            " AND pdct.tipo = 'BONIFICACION'),0) AS bonificacion, " +
                     " IFNULL((SELECT " +
                            " SUM(mcd.importe) AS importe_total " +
                            " FROM movimientos_caja  AS mc " +
                            " INNER JOIN movimientos_caja_detalles AS mcd " +
                            " ON mc.movimiento_caja_id = mcd.movimiento_caja_id AND mcd.tipo = 'ENTRADA' " +
                            " WHERE mc.estatus = 'ACTIVO' " +
                            " AND mc.sucursal_id = @sucursal_id " +
                            " AND mc.caja_id = @caja_id " +
                            " AND mc.caja_corte_id IS null " +
                            " AND  DATE_FORMAT(mc.fecha, '%Y-%m-%d') = @fecha_corte),0) AS entrada_total, " +
                     " IFNULL((SELECT " +
                            " SUM(mcd.importe) AS importe_total " +
                            " FROM movimientos_caja  AS mc " +
                            " INNER JOIN movimientos_caja_detalles AS mcd " +
                            " ON mc.movimiento_caja_id = mcd.movimiento_caja_id AND mcd.tipo = 'SALIDA' " +
                            " WHERE mc.estatus = 'ACTIVO' " +
                            " AND mc.sucursal_id = @sucursal_id " +
                            " AND mc.caja_id = @caja_id " +
                            " AND mc.caja_corte_id IS null " +
                            " AND  DATE_FORMAT(mc.fecha, '%Y-%m-%d') = @fecha_corte),0) AS salida_total, " +
                    " IFNULL((SELECT " +
                            " SUM(be.importe) AS importe_total " +
                            " FROM boletas_empeno  AS be " +
                            " WHERE be.estatus = 'ACTIVO' " +
                            " AND be.sucursal_id = @sucursal_id " +
                            " AND be.caja_id = @caja_id " +
                            " AND be.caja_corte_id IS null " +
                            " AND  DATE_FORMAT(be.fecha, '%Y-%m-%d') = @fecha_corte),0) AS boletas_empenadas, " +
                    " IFNULL((SELECT  " +
                            " GROUP_CONCAT(DISTINCT mc.movimiento_caja_id ORDER BY mc.movimiento_caja_id SEPARATOR ',') AS lista_movimiento_entradad_caja_id" +
                            " FROM movimientos_caja  AS mc " +
                             " INNER JOIN movimientos_caja_detalles AS mcd " +
                            " ON mc.movimiento_caja_id = mcd.movimiento_caja_id AND mcd.tipo = 'ENTRADA' " +
                            " WHERE mc.estatus = 'ACTIVO' " +
                            " AND mc.sucursal_id = @sucursal_id " +
                            " AND mc.caja_id = @caja_id " +
                            " AND mc.caja_corte_id IS null " +
                            " AND  DATE_FORMAT(mc.fecha, '%Y-%m-%d') = @fecha_corte), 0) AS lista_movimiento_entradad_caja_id," +
                    " IFNULL((SELECT  " +
                            " GROUP_CONCAT(DISTINCT mc.movimiento_caja_id ORDER BY mc.movimiento_caja_id SEPARATOR ',') AS lista_movimiento_salidad_caja_id" +
                            " FROM movimientos_caja  AS mc " +
                             " INNER JOIN movimientos_caja_detalles AS mcd " +
                            " ON mc.movimiento_caja_id = mcd.movimiento_caja_id AND mcd.tipo = 'SALIDA' " +
                            " WHERE mc.estatus = 'ACTIVO' " +
                            " AND mc.sucursal_id = @sucursal_id " +
                            " AND mc.caja_id = @caja_id " +
                            " AND mc.caja_corte_id IS null " +
                            " AND  DATE_FORMAT(mc.fecha, '%Y-%m-%d') = @fecha_corte), 0) AS lista_movimiento_salidad_caja_id," +
                    " IFNULL((SELECT " +
                             " GROUP_CONCAT(be.boleta_empeno_id ORDER BY be.boleta_empeno_id SEPARATOR ',') AS lista_boletas_sin_pago_id " +
                            " FROM boletas_empeno  AS be " +
                            " WHERE be.estatus = 'ACTIVO' " +
                            " AND be.sucursal_id = @sucursal_id " +
                            " AND be.caja_id = @caja_id " +
                            " AND be.caja_corte_id IS null " +
                            " AND  DATE_FORMAT(be.fecha, '%Y-%m-%d') = @fecha_corte),0) AS lista_boletas_sin_pago_id, " +
                    " IFNULL((SELECT " +
                            " GROUP_CONCAT(be.boleta_empeno_id ORDER BY be.boleta_empeno_id SEPARATOR ',') AS lista_boletas_refrendos_id " +
                            " FROM pagos  AS p " +
                            " INNER JOIN pagos_detalles AS pd " +
                            " ON p.pago_id = pd.pago_id " +
                            " INNER JOIN boletas_empeno AS be " +
                            " on pd.boleta_empeno_id = be.boleta_empeno_id" +
                            " WHERE p.estatus = 'ACTIVO' " +
                            " AND p.tipo_recibo = 'REFRENDO'" +
                            " AND p.sucursal_id = @sucursal_id " +
                            " AND p.caja_id = @caja_id " +
                            " AND  DATE_FORMAT(p.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " AND be.caja_corte_id IS null ),0) AS lista_boletas_refrendos_id, " +
                     " IFNULL((SELECT " +
                                " GROUP_CONCAT(DISTINCT p.pago_id ORDER BY p.pago_id SEPARATOR ',')  AS lista_pago_id " +
                                " FROM pagos  AS p " +
                                " INNER JOIN pagos_detalles AS pd " +
                                " ON p.pago_id = pd.pago_id " +
                                " INNER JOIN boletas_empeno AS be " +
                                " on pd.boleta_empeno_id = be.boleta_empeno_id" +
                                " WHERE p.estatus = 'ACTIVO' " +
                                " AND (p.tipo_recibo = 'LIQUIDACION' OR p.tipo_recibo = 'REFRENDO') " +
                                " AND p.sucursal_id = @sucursal_id  " +
                                " AND p.caja_id = @caja_id " +
                                " AND  DATE_FORMAT(p.fecha, '%Y-%m-%d') = @fecha_corte " +
                                " AND be.caja_corte_id IS null),0) AS lista_pago_id " +
                       " FROM pagos  AS p " +
                       " INNER JOIN pagos_detalles AS pd " +
                       " ON p.pago_id = pd.pago_id " +
                       " INNER JOIN boletas_empeno AS be " + 
                       " on pd.boleta_empeno_id = be.boleta_empeno_id" +
                       " WHERE p.estatus = 'ACTIVO' " +
                       " AND p.tipo_recibo = 'LIQUIDACION'" +
                       " AND p.sucursal_id = @sucursal_id " +
                       " AND p.caja_id = @caja_id " +
                       " AND  DATE_FORMAT(p.fecha, '%Y-%m-%d') = @fecha_corte " +
                       " AND be.caja_corte_id IS null " ;
            string fecha = dtFechaCorte.ToString("yyyy-MM-dd");
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", intSucursalId);
            this.connexion.mscCmd.Parameters.AddWithValue("@caja_id", intCajaId);
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha_corte", fecha);
            this.resultadoObtener(this.connexion);

        }

        public void buscarCajaIntegradora(int intSucursalId,  DateTime dtFechaCorte)
        {

            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT  " +
                         " IFNULL((SUM(be.importe)), 0) AS capital, " +
                         " GROUP_CONCAT(be.boleta_empeno_id ORDER BY be.boleta_empeno_id SEPARATOR ',') AS lista_boletas_id ," +
                          " IFNULL((SELECT " +
                                " SUM(pd.abono_capital) AS abono_capital_total " +
                                " FROM pagos  AS p " +
                                " INNER JOIN pagos_detalles AS pd " +
                                " ON p.pago_id = pd.pago_id " +
                                " INNER JOIN boletas_empeno AS be " +
                                " on pd.boleta_empeno_id = be.boleta_empeno_id" +
                                " WHERE p.estatus = 'ACTIVO' " +
                                " AND p.tipo_recibo = 'REFRENDO'" +
                                " AND p.sucursal_id = @sucursal_id " +                                
                                " AND  DATE_FORMAT(p.fecha, '%Y-%m-%d') = @fecha_corte " +
                                " AND be.caja_corte_id IS null ),0) AS abono_capital_total, " +
                         " IFNULL((SELECT " +
                                " SUM(pdct.importe) AS intereses_total " +
                                " FROM pagos  AS p " +
                                " INNER JOIN pagos_detalles AS pd " +
                                " ON p.pago_id = pd.pago_id " +
                                " INNER JOIN pagos_detalles_cobros_tipos AS pdct " +
                                " ON pd.renglon = pdct.renglon_detalle AND pd.pago_id = pdct.pago_id " +
                                " INNER JOIN boletas_empeno AS b " +
                                " on pd.boleta_empeno_id = b.boleta_empeno_id " +
                              " WHERE p.estatus = 'ACTIVO' " +
                              " AND (p.tipo_recibo = 'LIQUIDACION' OR  p.tipo_recibo = 'REFRENDO')" +
                              " AND p.sucursal_id = @sucursal_id " +                              
                              " AND pdct.tipo = 'INTERESES' " +
                              " AND b.caja_corte_id IS null " +
                              " AND  DATE_FORMAT(p.fecha, '%Y-%m-%d') = @fecha_corte " +
                              " AND pdct.cobro_tipo_id = 1),0) AS intereses, " +
                        " IFNULL((SELECT " +
                               " SUM(pdct.importe) AS almacenaje_total " +
                               " FROM pagos  AS p " +
                               " INNER JOIN pagos_detalles AS pd " +
                               " ON p.pago_id = pd.pago_id " +
                               " INNER JOIN pagos_detalles_cobros_tipos AS pdct " +
                               " ON pd.renglon = pdct.renglon_detalle AND pd.pago_id = pdct.pago_id " +
                               " INNER JOIN boletas_empeno AS b " +
                               " on pd.boleta_empeno_id = b.boleta_empeno_id " +
                            " WHERE p.estatus = 'ACTIVO' " +
                           " AND (p.tipo_recibo = 'LIQUIDACION' OR  p.tipo_recibo = 'REFRENDO')" +
                            " AND p.sucursal_id = @sucursal_id " +                            
                            " AND pdct.tipo = 'INTERESES' " +
                            " AND b.caja_corte_id IS null " +
                            " AND  DATE_FORMAT(p.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " AND pdct.cobro_tipo_id = 2),0) AS almacenaje, " +
                       " IFNULL((SELECT " +
                            " SUM(pdct.importe) AS manejo_total " +
                            " FROM pagos  AS p " +
                              " INNER JOIN pagos_detalles AS pd " +
                              " ON p.pago_id = pd.pago_id " +
                              " INNER JOIN pagos_detalles_cobros_tipos AS pdct " +
                              " ON pd.renglon = pdct.renglon_detalle AND pd.pago_id = pdct.pago_id " +
                              " INNER JOIN boletas_empeno AS b " +
                              " on pd.boleta_empeno_id = b.boleta_empeno_id " +
                           " WHERE p.estatus = 'ACTIVO' " +
                           " AND (p.tipo_recibo = 'LIQUIDACION' OR  p.tipo_recibo = 'REFRENDO')" +
                           " AND p.sucursal_id = @sucursal_id " +                           
                           " AND pdct.tipo = 'INTERESES' " +
                           " AND b.caja_corte_id IS null " +
                           " AND  DATE_FORMAT(p.fecha, '%Y-%m-%d') = @fecha_corte " +
                           " AND pdct.cobro_tipo_id = 3),0) AS manejo, " +
                       " IFNULL((SELECT " +
                            " SUM(pdct.importe) AS bonificacion_total " +
                            " FROM pagos  AS p " +
                            " INNER JOIN pagos_detalles AS pd " +
                            " ON p.pago_id = pd.pago_id " +
                            " INNER JOIN pagos_detalles_cobros_tipos AS pdct " +
                            " ON pd.renglon = pdct.renglon_detalle AND pd.pago_id = pdct.pago_id " +
                            " INNER JOIN boletas_empeno AS b " +
                            " on pd.boleta_empeno_id = b.boleta_empeno_id " +
                            " WHERE p.estatus = 'ACTIVO' " +
                            " AND (p.tipo_recibo = 'LIQUIDACION' OR  p.tipo_recibo = 'REFRENDO')" +
                            " AND p.sucursal_id = @sucursal_id " +                            
                            " AND b.caja_corte_id IS null " +
                            " AND  DATE_FORMAT(p.fecha, '%Y-%m-%d') = @fecha_corte" +
                            " AND pdct.tipo = 'BONIFICACION'),0) AS bonificacion, " +
                     " IFNULL((SELECT " +
                            " SUM(mcd.importe) AS importe_total " +
                            " FROM movimientos_caja  AS mc " +
                            " INNER JOIN movimientos_caja_detalles AS mcd " +
                            " ON mc.movimiento_caja_id = mcd.movimiento_caja_id AND mcd.tipo = 'ENTRADA' " +
                            " WHERE mc.estatus = 'ACTIVO' " +
                            " AND mc.sucursal_id = @sucursal_id " +                            
                            " AND mc.caja_corte_id IS null " +
                            " AND  DATE_FORMAT(mc.fecha, '%Y-%m-%d') = @fecha_corte),0) AS entrada_total, " +
                     " IFNULL((SELECT " +
                            " SUM(mcd.importe) AS importe_total " +
                            " FROM movimientos_caja  AS mc " +
                            " INNER JOIN movimientos_caja_detalles AS mcd " +
                            " ON mc.movimiento_caja_id = mcd.movimiento_caja_id AND mcd.tipo = 'SALIDA' " +
                            " WHERE mc.estatus = 'ACTIVO' " +
                            " AND mc.sucursal_id = @sucursal_id " +                            
                            " AND mc.caja_corte_id IS null " +
                            " AND  DATE_FORMAT(mc.fecha, '%Y-%m-%d') = @fecha_corte),0) AS salida_total, " +
                    " IFNULL((SELECT " +
                            " SUM(be.importe) AS importe_total " +
                            " FROM boletas_empeno  AS be " +
                            " WHERE be.estatus = 'ACTIVO' " +
                            " AND be.sucursal_id = @sucursal_id " +                            
                            " AND be.caja_corte_id IS null " +
                            " AND  DATE_FORMAT(be.fecha, '%Y-%m-%d') = @fecha_corte),0) AS boletas_empenadas, " +
                    " IFNULL((SELECT  " +
                            " GROUP_CONCAT(DISTINCT mc.movimiento_caja_id ORDER BY mc.movimiento_caja_id SEPARATOR ',') AS lista_movimiento_entradad_caja_id" +
                            " FROM movimientos_caja  AS mc " +
                             " INNER JOIN movimientos_caja_detalles AS mcd " +
                            " ON mc.movimiento_caja_id = mcd.movimiento_caja_id AND mcd.tipo = 'ENTRADA' " +
                            " WHERE mc.estatus = 'ACTIVO' " +
                            " AND mc.sucursal_id = @sucursal_id " +                            
                            " AND mc.caja_corte_id IS null " +
                            " AND  DATE_FORMAT(mc.fecha, '%Y-%m-%d') = @fecha_corte), 0) AS lista_movimiento_entradad_caja_id," +
                    " IFNULL((SELECT  " +
                            " GROUP_CONCAT(DISTINCT mc.movimiento_caja_id ORDER BY mc.movimiento_caja_id SEPARATOR ',') AS lista_movimiento_salidad_caja_id" +
                            " FROM movimientos_caja  AS mc " +
                             " INNER JOIN movimientos_caja_detalles AS mcd " +
                            " ON mc.movimiento_caja_id = mcd.movimiento_caja_id AND mcd.tipo = 'SALIDA' " +
                            " WHERE mc.estatus = 'ACTIVO' " +
                            " AND mc.sucursal_id = @sucursal_id " +                            
                            " AND mc.caja_corte_id IS null " +
                            " AND  DATE_FORMAT(mc.fecha, '%Y-%m-%d') = @fecha_corte), 0) AS lista_movimiento_salidad_caja_id," +
                    " IFNULL((SELECT " +
                             " GROUP_CONCAT(be.boleta_empeno_id ORDER BY be.boleta_empeno_id SEPARATOR ',') AS lista_boletas_sin_pago_id " +
                            " FROM boletas_empeno  AS be " +
                            " WHERE be.estatus = 'ACTIVO' " +
                            " AND be.sucursal_id = @sucursal_id " +                            
                            " AND be.caja_corte_id IS null " +
                            " AND  DATE_FORMAT(be.fecha, '%Y-%m-%d') = @fecha_corte),0) AS lista_boletas_sin_pago_id, " +
                    " IFNULL((SELECT " +
                            " GROUP_CONCAT(be.boleta_empeno_id ORDER BY be.boleta_empeno_id SEPARATOR ',') AS lista_boletas_refrendos_id " +
                            " FROM pagos  AS p " +
                            " INNER JOIN pagos_detalles AS pd " +
                            " ON p.pago_id = pd.pago_id " +
                            " INNER JOIN boletas_empeno AS be " +
                            " on pd.boleta_empeno_id = be.boleta_empeno_id" +
                            " WHERE p.estatus = 'ACTIVO' " +
                            " AND p.tipo_recibo = 'REFRENDO'" +
                            " AND p.sucursal_id = @sucursal_id " +                            
                            " AND  DATE_FORMAT(p.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " AND be.caja_corte_id IS null ),0) AS lista_boletas_refrendos_id, " +
                     " IFNULL((SELECT " +
                                " GROUP_CONCAT(DISTINCT p.pago_id ORDER BY p.pago_id SEPARATOR ',')  AS lista_pago_id " +
                                " FROM pagos  AS p " +
                                " INNER JOIN pagos_detalles AS pd " +
                                " ON p.pago_id = pd.pago_id " +
                                " INNER JOIN boletas_empeno AS be " +
                                " on pd.boleta_empeno_id = be.boleta_empeno_id" +
                                " WHERE p.estatus = 'ACTIVO' " +
                                " AND (p.tipo_recibo = 'LIQUIDACION' OR p.tipo_recibo = 'REFRENDO') " +
                                " AND p.sucursal_id = @sucursal_id  " +                                
                                " AND  DATE_FORMAT(p.fecha, '%Y-%m-%d') = @fecha_corte " +
                                " AND be.caja_corte_id IS null),0) AS lista_pago_id " +
                       " FROM pagos  AS p " +
                       " INNER JOIN pagos_detalles AS pd " +
                       " ON p.pago_id = pd.pago_id " +
                       " INNER JOIN boletas_empeno AS be " +
                       " on pd.boleta_empeno_id = be.boleta_empeno_id" +
                       " WHERE p.estatus = 'ACTIVO' " +
                       " AND p.tipo_recibo = 'LIQUIDACION'" +
                       " AND p.sucursal_id = @sucursal_id " +                       
                       " AND  DATE_FORMAT(p.fecha, '%Y-%m-%d') = @fecha_corte " +
                       " AND be.caja_corte_id IS null ";
            string fecha = dtFechaCorte.ToString("yyyy-MM-dd");
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", intSucursalId);            
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha_corte", fecha);
            this.resultadoObtener(this.connexion);



        }

        public void buscarCorteCaja(int intCorteCajaId,int intSucursalId, int intCajaId, DateTime dtFechaCorte)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT   " +
                        " cc.caja_corte_id, " +
                        " cc.sucursal_id, " +
                        " cc.caja_id,   " +
                        " cc.usuario_realiza, " +
                        " cc.tipo, " +
                        " cc.saldo_inicial, " +
                        " cc.saldo_final, " +
                        " cc.mil, " +
                        " cc.quinientos, " +
                        " cc.doscientos, " +
                        " cc.cien, " +
                        " cc.cincuenta, " +
                        " cc.veinte, " +
                        " cc.diez, " +
                        " cc.cinco, " +
                        " cc.dos, " +
                        " cc.uno, " +
                        " cc.cincuenta_centavos, " +
                        " cc.veinte_centavos, " +
                        " cc.diez_centavos, " +
                        " cc.cinco_centavos, " +
                        " IFNULL((SELECT SUM(be.importe) " +
                            " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                " INNER JOIN  pagos AS p " +
                                " ON ccd.referencia_id = p.pago_id AND ccd.tipo_referencia = 'PAGO' AND ccd.tipo = 'INGRESO' " +
                            " INNER JOIN pagos_detalles AS pd  " +
                            " ON p.pago_id = pd.pago_id " +
                            " INNER JOIN boletas_empeno AS be  " +
                            " on pd.boleta_empeno_id = be.boleta_empeno_id " +
                            " WHERE cc.sucursal_id = @sucursal_id " +
                            " AND  cc.caja_id = @caja_id " +
                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'CIERRE'      " +
                                " AND p.tipo_recibo = 'LIQUIDACION' " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS capital, " +
                            " IFNULL((SELECT SUM(pd.abono_capital) " +
                                " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                " INNER JOIN  pagos AS p " +
                                " ON ccd.referencia_id = p.pago_id AND ccd.tipo_referencia = 'PAGO' AND ccd.tipo = 'INGRESO' " +
                            " INNER JOIN pagos_detalles AS pd  " +
                            " ON p.pago_id = pd.pago_id " +
                            " INNER JOIN boletas_empeno AS be  " +
                            " on pd.boleta_empeno_id = be.boleta_empeno_id " +
                            " WHERE cc.sucursal_id = @sucursal_id " +
                            " AND  cc.caja_id = @caja_id " +
                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'CIERRE'         " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS abono_capital_total, " +
                            " IFNULL((SELECT SUM(pdct.importe) AS intereses_total  " +
                            " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                " INNER JOIN  pagos AS p " +
                                " ON ccd.referencia_id = p.pago_id AND ccd.tipo_referencia = 'PAGO' AND ccd.tipo = 'INGRESO' " +
                            " INNER JOIN pagos_detalles AS pd  " +
                            " ON p.pago_id = pd.pago_id " +
                            " INNER JOIN pagos_detalles_cobros_tipos AS pdct  " +
                            " ON pd.renglon = pdct.renglon_detalle AND pd.pago_id = pdct.pago_id  " +
                            " WHERE cc.sucursal_id = @sucursal_id " +
                            " AND  cc.caja_id = @caja_id " +
                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'CIERRE' " +
                            " AND pdct.tipo = 'INTERESES'  " +
                                " AND pdct.cobro_tipo_id = 1 " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS intereses, " +
                            " IFNULL((SELECT SUM(pdct.importe) AS almacenaje_total  " +
                            " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                " INNER JOIN  pagos AS p " +
                                " ON ccd.referencia_id = p.pago_id AND ccd.tipo_referencia = 'PAGO' AND ccd.tipo = 'INGRESO' " +
                            " INNER JOIN pagos_detalles AS pd  " +
                            " ON p.pago_id = pd.pago_id " +
                            " INNER JOIN pagos_detalles_cobros_tipos AS pdct  " +
                            " ON pd.renglon = pdct.renglon_detalle AND pd.pago_id = pdct.pago_id  " +
                            " WHERE cc.sucursal_id = @sucursal_id " +
                            " AND  cc.caja_id = @caja_id " +
                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'CIERRE' " +
                            " AND pdct.tipo = 'INTERESES'  " +
                                " AND pdct.cobro_tipo_id = 2 " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS almacenaje, " +
                            " IFNULL((SELECT SUM(pdct.importe) AS manejo_total  " +
                            " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                " INNER JOIN  pagos AS p " +
                                " ON ccd.referencia_id = p.pago_id AND ccd.tipo_referencia = 'PAGO' AND ccd.tipo = 'INGRESO' " +
                            " INNER JOIN pagos_detalles AS pd  " +
                            " ON p.pago_id = pd.pago_id " +
                            " INNER JOIN pagos_detalles_cobros_tipos AS pdct  " +
                            " ON pd.renglon = pdct.renglon_detalle AND pd.pago_id = pdct.pago_id  " +
                            " WHERE cc.sucursal_id = @sucursal_id " +
                            " AND  cc.caja_id = @caja_id " +
                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'CIERRE' " +
                            " AND pdct.tipo = 'INTERESES'  " +
                                " AND pdct.cobro_tipo_id = 3 " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS manejo, " +
                            " IFNULL((SELECT SUM(pdct.importe) AS intereses_total  " +
                            " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                " INNER JOIN  pagos AS p " +
                                " ON ccd.referencia_id = p.pago_id AND ccd.tipo_referencia = 'PAGO' AND ccd.tipo = 'INGRESO' " +
                            " INNER JOIN pagos_detalles AS pd  " +
                            " ON p.pago_id = pd.pago_id " +
                            " INNER JOIN pagos_detalles_cobros_tipos AS pdct  " +
                            " ON pd.renglon = pdct.renglon_detalle AND pd.pago_id = pdct.pago_id  " +
                            " WHERE cc.sucursal_id = @sucursal_id " +
                            " AND  cc.caja_id = @caja_id " +
                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'CIERRE' " +
                            " AND pdct.tipo = 'BONIFICACION'          " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS bonificacion, " +
                            " IFNULL((SELECT  " +
                            " SUM(mcd.importe) AS importe_total  " +
                            " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                " INNER JOIN  movimientos_caja AS mc " +
                                " ON ccd.referencia_id = mc.movimiento_caja_id AND ccd.tipo_referencia = 'MOVIMIENTO CAJA' AND ccd.tipo = 'INGRESO' " +
                            " INNER JOIN movimientos_caja_detalles AS mcd  " +
                            " ON mc.movimiento_caja_id = mcd.movimiento_caja_id AND mcd.tipo = 'ENTRADA'  " +
                            " WHERE cc.sucursal_id = @sucursal_id " +
                            " AND  cc.caja_id = @caja_id " +
                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'CIERRE' " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS entrada_total, " +
                            " IFNULL((SELECT  " +
                            " SUM(mcd.importe) AS importe_total  " +
                            " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                " INNER JOIN  movimientos_caja AS mc " +
                                " ON ccd.referencia_id = mc.movimiento_caja_id AND ccd.tipo_referencia = 'MOVIMIENTO CAJA' AND ccd.tipo = 'EGRESO' " +
                            " INNER JOIN movimientos_caja_detalles AS mcd  " +
                            " ON mc.movimiento_caja_id = mcd.movimiento_caja_id AND mcd.tipo = 'SALIDA'  " +
                            " WHERE cc.sucursal_id = @sucursal_id " +
                            " AND  cc.caja_id = @caja_id " +
                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'CIERRE' " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS salida_total, " +
                            " IFNULL((SELECT SUM(be.importe) " +
                                " FROM  cajas_corte AS cc " +
                                    " INNER JOIN cajas_corte_detalles AS ccd " +
                                    " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                    " INNER JOIN  boletas_empeno AS be " +
                                    " ON ccd.referencia_id = be.boleta_empeno_id AND ccd.tipo_referencia = 'BOLETA' AND ccd.tipo = 'EGRESO' " +
                                " WHERE cc.sucursal_id = @sucursal_id " +
                                " AND  cc.caja_id = @caja_id " +
                                " AND cc.tipo = 'CIERRE' " +
                                " AND cc.estatus = 'ACTIVO'      " +
                                " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                                " ),0) AS boletas_empenadas, " +
                            " IFNULL((SELECT  " +
                            " GROUP_CONCAT(be.boleta_empeno_id ORDER BY be.boleta_empeno_id SEPARATOR ',') AS lista_boletas_id  " +
                            " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id AND ccd.tipo_referencia = 'BOLETA' " +
                            " INNER JOIN boletas_empeno AS be " +
                                " ON ccd.referencia_id = be.boleta_empeno_id       " +
                            " WHERE cc.sucursal_id = @sucursal_id " +
                            " AND  cc.caja_id = @caja_id " +
                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'CIERRE' " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS lista_boletas_id, " +
                            " IFNULL((SELECT  " +
                            " GROUP_CONCAT(p.pago_id ORDER BY p.pago_id SEPARATOR ',') AS lista_pago_id  " +
                            " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                " INNER JOIN  pagos AS p " +
                                " ON ccd.referencia_id = p.pago_id AND ccd.tipo_referencia = 'PAGO' AND ccd.tipo = 'INGRESO' " +
                            " WHERE cc.sucursal_id = @sucursal_id  " +
                            " AND  cc.caja_id = @caja_id " +
                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'ACTIVO' " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS lista_pago_id " +
                        " FROM  cajas_corte AS cc " +
                        " WHERE cc.sucursal_id = @sucursal_id " +
                        " AND  cc.caja_id = @caja_id " +
                        " AND cc.tipo = 'CIERRE' " +
                        " AND cc.estatus = 'ACTIVO' " +
                        " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte ";
            string fecha = dtFechaCorte.ToString("yyyy-MM-dd");
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", intSucursalId);
            this.connexion.mscCmd.Parameters.AddWithValue("@caja_id", intCajaId);
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha_corte", fecha);
            this.resultadoObtener(this.connexion);

        }

        public void buscarCorteCajaIntegrador(int intCorteCajaId, int intSucursalId, DateTime dtFechaCorte)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT   " +
                        " cc.caja_corte_id, " +
                        " cc.sucursal_id, " +
                        " cc.caja_id,   " +
                        " cc.usuario_realiza, " +
                        " cc.tipo, " +
                        " cc.saldo_inicial, " +
                        " IFNULL( SUM(cc.saldo_final), 0)  AS importeTeorico," +
                        " IFNULL( SUM(cc.mil), 0)  AS billete1000, " +
                        " IFNULL( SUM(cc.quinientos), 0)  AS billete500, " +
                        " IFNULL( SUM(cc.doscientos), 0)  AS billete200, " +
                        " IFNULL( SUM(cc.cien), 0)  AS billete100, " +
                        " IFNULL( SUM(cc.cincuenta), 0)  AS billete50, " +
                        " IFNULL( SUM(cc.veinte), 0)  AS billete20, " +
                        " IFNULL( SUM(cc.diez), 0)  AS moneda10, " +
                        " IFNULL( SUM(cc.cinco), 0)  AS moneda5, " +
                        " IFNULL( SUM(cc.dos), 0)  AS moneda2, " +
                        " IFNULL( SUM(cc.uno), 0)  AS moneda1, " +
                        " IFNULL( SUM(cc.cincuenta_centavos), 0)  AS monedaCentavos50, " +
                        " IFNULL( SUM(cc.veinte_centavos), 0)  AS monedaCentavos20, " +
                        " IFNULL( SUM(cc.diez_centavos), 0)  AS monedaCentavos10, " +
                        " IFNULL( SUM(cc.cinco_centavos), 0)  AS monedaCentavos5, " +
                        " IFNULL((SELECT SUM(be.importe) " +
                            " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                " INNER JOIN  pagos AS p " +
                                " ON ccd.referencia_id = p.pago_id AND ccd.tipo_referencia = 'PAGO' AND ccd.tipo = 'INGRESO' " +
                            " INNER JOIN pagos_detalles AS pd  " +
                            " ON p.pago_id = pd.pago_id " +
                            " INNER JOIN boletas_empeno AS be  " +
                            " on pd.boleta_empeno_id = be.boleta_empeno_id " +
                            " WHERE cc.sucursal_id = @sucursal_id " +

                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'CIERRE'      " +
                                " AND p.tipo_recibo = 'LIQUIDACION' " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS capital, " +
                            " IFNULL((SELECT SUM(pd.abono_capital) " +
                                " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                " INNER JOIN  pagos AS p " +
                                " ON ccd.referencia_id = p.pago_id AND ccd.tipo_referencia = 'PAGO' AND ccd.tipo = 'INGRESO' " +
                            " INNER JOIN pagos_detalles AS pd  " +
                            " ON p.pago_id = pd.pago_id " +
                            " INNER JOIN boletas_empeno AS be  " +
                            " on pd.boleta_empeno_id = be.boleta_empeno_id " +
                            " WHERE cc.sucursal_id = @sucursal_id " +
                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'CIERRE'         " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS abono_capital_total, " +
                            " IFNULL((SELECT SUM(pdct.importe) AS intereses_total  " +
                            " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                " INNER JOIN  pagos AS p " +
                                " ON ccd.referencia_id = p.pago_id AND ccd.tipo_referencia = 'PAGO' AND ccd.tipo = 'INGRESO' " +
                            " INNER JOIN pagos_detalles AS pd  " +
                            " ON p.pago_id = pd.pago_id " +
                            " INNER JOIN pagos_detalles_cobros_tipos AS pdct  " +
                            " ON pd.renglon = pdct.renglon_detalle AND pd.pago_id = pdct.pago_id  " +
                            " WHERE cc.sucursal_id = @sucursal_id " +
                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'CIERRE' " +
                            " AND pdct.tipo = 'INTERESES'  " +
                                " AND pdct.cobro_tipo_id = 1 " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS intereses, " +
                            " IFNULL((SELECT SUM(pdct.importe) AS almacenaje_total  " +
                            " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                " INNER JOIN  pagos AS p " +
                                " ON ccd.referencia_id = p.pago_id AND ccd.tipo_referencia = 'PAGO' AND ccd.tipo = 'INGRESO' " +
                            " INNER JOIN pagos_detalles AS pd  " +
                            " ON p.pago_id = pd.pago_id " +
                            " INNER JOIN pagos_detalles_cobros_tipos AS pdct  " +
                            " ON pd.renglon = pdct.renglon_detalle AND pd.pago_id = pdct.pago_id  " +
                            " WHERE cc.sucursal_id = @sucursal_id " +
                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'CIERRE' " +
                            " AND pdct.tipo = 'INTERESES'  " +
                                " AND pdct.cobro_tipo_id = 2 " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS almacenaje, " +
                            " IFNULL((SELECT SUM(pdct.importe) AS manejo_total  " +
                            " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                " INNER JOIN  pagos AS p " +
                                " ON ccd.referencia_id = p.pago_id AND ccd.tipo_referencia = 'PAGO' AND ccd.tipo = 'INGRESO' " +
                            " INNER JOIN pagos_detalles AS pd  " +
                            " ON p.pago_id = pd.pago_id " +
                            " INNER JOIN pagos_detalles_cobros_tipos AS pdct  " +
                            " ON pd.renglon = pdct.renglon_detalle AND pd.pago_id = pdct.pago_id  " +
                            " WHERE cc.sucursal_id = @sucursal_id " +
                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'CIERRE' " +
                            " AND pdct.tipo = 'INTERESES'  " +
                                " AND pdct.cobro_tipo_id = 3 " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS manejo, " +
                            " IFNULL((SELECT SUM(pdct.importe) AS intereses_total  " +
                            " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                " INNER JOIN  pagos AS p " +
                                " ON ccd.referencia_id = p.pago_id AND ccd.tipo_referencia = 'PAGO' AND ccd.tipo = 'INGRESO' " +
                            " INNER JOIN pagos_detalles AS pd  " +
                            " ON p.pago_id = pd.pago_id " +
                            " INNER JOIN pagos_detalles_cobros_tipos AS pdct  " +
                            " ON pd.renglon = pdct.renglon_detalle AND pd.pago_id = pdct.pago_id  " +
                            " WHERE cc.sucursal_id = @sucursal_id " +
                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'CIERRE' " +
                            " AND pdct.tipo = 'BONIFICACION'          " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS bonificacion, " +
                            " IFNULL((SELECT  " +
                            " SUM(mcd.importe) AS importe_total  " +
                            " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                " INNER JOIN  movimientos_caja AS mc " +
                                " ON ccd.referencia_id = mc.movimiento_caja_id AND ccd.tipo_referencia = 'MOVIMIENTO CAJA' AND ccd.tipo = 'INGRESO' " +
                            " INNER JOIN movimientos_caja_detalles AS mcd  " +
                            " ON mc.movimiento_caja_id = mcd.movimiento_caja_id AND mcd.tipo = 'ENTRADA'  " +
                            " WHERE cc.sucursal_id = @sucursal_id " +

                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'CIERRE' " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS entrada_total, " +
                            " IFNULL((SELECT  " +
                            " SUM(mcd.importe) AS importe_total  " +
                            " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                " INNER JOIN  movimientos_caja AS mc " +
                                " ON ccd.referencia_id = mc.movimiento_caja_id AND ccd.tipo_referencia = 'MOVIMIENTO CAJA' AND ccd.tipo = 'EGRESO' " +
                            " INNER JOIN movimientos_caja_detalles AS mcd  " +
                            " ON mc.movimiento_caja_id = mcd.movimiento_caja_id AND mcd.tipo = 'SALIDA'  " +
                            " WHERE cc.sucursal_id = @sucursal_id " +
                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'CIERRE' " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS salida_total, " +
                            " IFNULL((SELECT SUM(be.importe) " +
                                " FROM  cajas_corte AS cc " +
                                    " INNER JOIN cajas_corte_detalles AS ccd " +
                                    " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                    " INNER JOIN  boletas_empeno AS be " +
                                    " ON ccd.referencia_id = be.boleta_empeno_id AND ccd.tipo_referencia = 'BOLETA' AND ccd.tipo = 'EGRESO' " +
                                " WHERE cc.sucursal_id = @sucursal_id " +
                                " AND  cc.caja_id = @caja_id " +
                                " AND cc.tipo = 'CIERRE' " +
                                " AND cc.estatus = 'ACTIVO'      " +
                                " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                                " ),0) AS boletas_empenadas, " +
                            " IFNULL((SELECT  " +
                            " GROUP_CONCAT(be.boleta_empeno_id ORDER BY be.boleta_empeno_id SEPARATOR ',') AS lista_boletas_id  " +
                            " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id AND ccd.tipo_referencia = 'BOLETA' " +
                            " INNER JOIN boletas_empeno AS be " +
                                " ON ccd.referencia_id = be.boleta_empeno_id       " +
                            " WHERE cc.sucursal_id = @sucursal_id " +
                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'CIERRE' " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS lista_boletas_id, " +
                            " IFNULL((SELECT  " +
                            " GROUP_CONCAT(p.pago_id ORDER BY p.pago_id SEPARATOR ',') AS lista_pago_id  " +
                            " FROM  cajas_corte AS cc " +
                                " INNER JOIN cajas_corte_detalles AS ccd " +
                                " ON cc.caja_corte_id = ccd.caja_corte_id " +
                                " INNER JOIN  pagos AS p " +
                                " ON ccd.referencia_id = p.pago_id AND ccd.tipo_referencia = 'PAGO' AND ccd.tipo = 'INGRESO' " +
                            " WHERE cc.sucursal_id = @sucursal_id  " +
                            " AND cc.tipo = 'CIERRE' " +
                            " AND cc.estatus = 'ACTIVO' " +
                            " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte " +
                            " ),0) AS lista_pago_id " +
                        " FROM  cajas_corte AS cc " +
                        " WHERE cc.sucursal_id = @sucursal_id " +
                        " AND cc.tipo = 'CIERRE' " +
                        " AND cc.estatus = 'ACTIVO' " +
                        " AND DATE_FORMAT(cc.fecha, '%Y-%m-%d') = @fecha_corte ";
            string fecha = dtFechaCorte.ToString("yyyy-MM-dd");
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", intSucursalId);            
            this.connexion.mscCmd.Parameters.AddWithValue("@fecha_corte", fecha);
            this.resultadoObtener(this.connexion);

        }


        public void paginacion(int  intSucursalId) 
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT  " +
                " cc.fecha, " +
                " cc.tipo, " +
                " cc.saldo_inicial, " +
                " cc.saldo_final, " +
                " cc.estatus, " +
                " cj.caja_id, " +
                " cj.codigo AS codigo_caja, " +
                " cj.descripcion AS descripcion_caja, " +

                " cj.descripcion AS `Tipo `" +
                " DATE_FORMAT(cc.fecha, '%d/%m/%Y %r') AS `Fecha `, " +
                " cc.tipo AS `Caja `, " +
                " cc.saldo_inicial AS `Saldo inicial`, " +
                " cc.saldo_final AS `Importe teórico`, " +
                " cc.estatus AS `Estatus `, " +
                " FROM  cajas_corte AS cc " +
                " INNER JOIN cajas AS cj " +
                " ON cc.caja_id = cj.caja_id " +
                " WHERE cc.sucursal_id = @sucursal_id " +
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", intSucursalId);
            this.resultadoObtener(this.connexion);
        }

        public void buscar(string strBoleta)
        {

            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = " SELECT  " +
                " be.boleta_empeno_id," +
                " be.folio," +
                " '' AS boleta_inicial, " +
                " be.fecha, " +
                "  DATE_FORMAT(be.fecha, '%l:%i:%s') AS hora, " +
                " be.vencimiento, " +
                " be.boleta_refrendo_id, " +
                " c.cliente_id, " +
                " c.codigo AS codigo_cliente, " +
                " CONCAT(c.nombre,  ' ',  c.apellido_paterno,  ' ',  c.apellido_materno) AS nombre_cliente, " +
                " e.usuario_id AS usuario_id, " +
                " CONCAT(e.nombre,  ' ',  e.apellido_paterno,  ' ',  e.apellido_materno) AS nombre_empleado, " +
                " it.identificacion_tipo_id, " +
                " it.codigo as codigo_identificacion, " +
                " it.descripcion as descripcion_identificacion, " +
                " pt.prenda_tipo_id, " +
                " pt.codigo AS codigo_prenda_tipo, " +
                " pt.descripcion AS descripcion_prenda_tipo, " +
                " pt.capturar_gramaje, " +
                " sp.plazo AS plazo_tope, " +
                " ps.prenda_subtipo_id, " +
                " ps.codigo AS codigo_prenda_subtipo, " +
                " ps.descripcion AS descripcion_prenda_subtipo, " +
                " g.gramaje_id, " +
                " g.codigo AS codigo_gramaje, " +
                " g.descripcion AS descripcion_gramaje, " +
                " sg.precio_tope, " +
                " be.kilataje, " +
                " be.gramos, " +
                " be.cantidad, " +
                " be.numero_plazos, " +
                " be.importe, " +
                " be.descripcion, " +
                " be.comentario, " +
                " be.fotografia_imagen, " +
                " be.estatus, " +
                " be.caja_id, " +
                " cj.codigo AS codigo_caja, " +
                " cj.descripcion AS descripcion_caja, " +
                " CONCAT(uc.nombre,  ' ',  uc.apellido_paterno,  ' ',  uc.apellido_materno) AS nombre_usuario_creacion, " +
                " CONCAT(ua.nombre,  ' ',  ua.apellido_paterno,  ' ',  ua.apellido_materno) AS nombre_usuario_actualizacion, " +
                " be.importe_avaluo, " +
                " bei.folio AS boleta_inicial," +
                " be.pago_refrendo_id," +
                " be.boleta_inicial_id," +
                " be.motivo_cancelacion, " +
                " be.folio AS `Boleta `, " +
                " DATE_FORMAT(be.fecha, '%d/%m/%Y %r') AS `Fecha `, " +
                " CONCAT(c.nombre , ' ',c.apellido_paterno, ' ',c.apellido_materno) AS `Cliente`, " +
                " CONCAT('$', FORMAT(be.importe, 2))  AS `Préstamo`, " +
                " be.estatus AS `Estatus ` ," +
                " DATE_FORMAT(be.fecha, '%Y/%m/%d') AS fecha_busqueda," +
                " Pago.pago_folio ," +
                " Pago.pago_fecha, " +
                " beP.folio AS boleta_refrendo_folio, " +
                " beF.folio AS boleta_refrendada_folio," +
                " CONCAT(uE.nombre,  ' ',  uE.apellido_paterno,  ' ',  uE.apellido_materno) AS nombre_empleado_eliminacion, " +
                " DATE_FORMAT(be.fecha_eliminacion, '%d/%m/%Y %r') AS fecha_eliminacion," +
                 " IFNULL((SELECT(SUM(pdcPPI.importe) - " +
                        " IFNULL((SELECT SUM(pdcBonif.importe) " +
                            " FROM pagos AS pBonif " +
                            " INNER JOIN pagos_detalles AS pdBonif " +
                            " ON pBonif.pago_id = pdBonif.pago_id " +
                            " INNER JOIN pagos_detalles_cobros_tipos AS pdcBonif " +
                            " ON pdBonif.pago_id = pdcBonif.pago_id " +
                            " WHERE pdBonif.boleta_empeno_id = be.boleta_empeno_id " +
                            " AND pdcBonif.tipo = 'BONIFICACION' AND pBonif.estatus = 'PAGO PARCIAL DE INTERESES' " +
                        " ), 0) " +
                    " ) AS ppi " +
                    " FROM pagos AS pPPI " +
                    " INNER JOIN pagos_detalles AS pdPPI " +
                    " ON pPPI.pago_id = pdPPI.pago_id " +
                    " INNER JOIN pagos_detalles_cobros_tipos AS pdcPPI " +
                    " ON pdPPI.pago_id = pdcPPI.pago_id " +
                    " WHERE pdPPI.boleta_empeno_id = be.boleta_empeno_id  " +
                    " AND pdcPPI.tipo = 'PPI' AND pPPI.estatus = 'PAGO PARCIAL DE INTERESES' ), 0) AS ppi, " +
                " be.imei_celular, " +
                " be.tipo_serie_vehiculo, " +
                " be.numero_serie_vehiculo" +
                " FROM boletas_empeno AS be " +
                " INNER JOIN clientes AS c " +
                " ON be.cliente_id = c.cliente_id" +
                " INNER JOIN identificaciones_tipos AS it " +
                " ON be.identificacion_tipo_id = it.identificacion_tipo_id " +
                " INNER JOIN prendas_tipos AS pt " +
                " ON be.prenda_tipo_id = pt.prenda_tipo_id " +
                " INNER JOIN sucursales_plazos AS sp" +
                " ON be.prenda_tipo_id = sp.prenda_tipo_id  AND sp.sucursal_id = be.sucursal_id" +
                " INNER JOIN prendas_subtipos AS ps " +
                " ON be.prenda_subtipo_id = ps.prenda_subtipo_id " +
                " INNER JOIN usuarios AS e " +
                " ON be.usuario_id = e.usuario_id " +
                " INNER JOIN cajas AS cj " +
                " ON be.caja_id = cj.caja_id " +
                " INNER JOIN usuarios AS uc " +
                " ON be.usuario_creacion = uc.usuario_id " +
                " INNER JOIN boletas_empeno AS bei " +
                " ON be.boleta_inicial_id = bei.boleta_empeno_id " +
                " LEFT JOIN gramajes AS g " +
                " ON be.gramaje_id = g.gramaje_id " +
                " LEFT JOIN sucursales_gramajes AS sg " +
                " ON be.gramaje_id = sg.gramaje_id AND  sg.sucursal_id = be.sucursal_id  " +
                " LEFT JOIN usuarios AS ua " +
                " ON be.usuario_actualizacion = ua.usuario_id " +

                 " LEFT JOIN(SELECT  pd.boleta_empeno_id AS referenciaID, p.folio AS pago_folio, " +
                         "DATE_FORMAT(p.fecha, '%d/%m/%Y') AS pago_fecha " +
                 " FROM  pagos_detalles AS pd " +
                 " INNER JOIN pagos AS p  ON pd.pago_id = p.pago_id " +
                 " WHERE p.estatus = 'ACTIVO'" +
                 " GROUP BY pd.boleta_empeno_id, p.folio, p.fecha) AS Pago ON Pago.referenciaID = be.boleta_empeno_id" +

                " LEFT JOIN boletas_empeno AS beP " +
                " ON be.boleta_refrendo_id = beP.boleta_empeno_id " +
                " LEFT JOIN boletas_empeno AS beF " +
                " ON be.boleta_empeno_id = beF.boleta_refrendo_id AND  beF.estatus = 'ACTIVO'  " +
                " LEFT JOIN usuarios AS uE " +
                " ON be.usuario_eliminacion = uE.usuario_id " +
                 " WHERE be.folio = @folio ";

            this.connexion.mscCmd.Parameters.AddWithValue("@folio", strBoleta);
            this.resultadoObtener(this.connexion);

        }

    
    }
}
