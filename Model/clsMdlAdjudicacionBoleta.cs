﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora.Model
{
    public class clsMdlAdjudicacionBoleta : Core.clsCoreModel
    {
        public clsMdlAdjudicacionBoleta()
        {

        }

        public void guardar(Properties_Class.clsAdjudicacionBoletas adjudicacionBoletas)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.lsQuerysTracciones = new List<string>();
            this.lsQuerysTraccionesID = new List<string>();

            //Si existe id del registro se modifica, de lo contrario agregar uno nuevo           
            this.lsQuerysTraccionesID.Add(adjudicacionBoletas.strQueryDetalles);
            this.lsQuerysTracciones.Add(adjudicacionBoletas.strQueryBoletaEmpenoEstatus);
            

           //Asignar valores al commando
            connexion.mscCmd.Parameters.AddWithValue("@id", adjudicacionBoletas.intId);
            connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", adjudicacionBoletas.intSucursal);
            connexion.mscCmd.Parameters.AddWithValue("@codigo", adjudicacionBoletas.strCodigo);
            connexion.mscCmd.Parameters.AddWithValue("@fechaAdjudicacion", adjudicacionBoletas.dtAjudicacionFecha);
            connexion.mscCmd.Parameters.AddWithValue("@estatus", adjudicacionBoletas.strEstatus);
            connexion.mscCmd.Parameters.AddWithValue("@fecha", adjudicacionBoletas.dtFecha);
            connexion.mscCmd.Parameters.AddWithValue("@usuario", adjudicacionBoletas.intUsuario);
            //Regesar resultado
            this.resultadoTransaccionSinPrincipal(this.connexion);
            
        }



        public void resultadoTransaccionSinPrincipal(db.clsDBConnection connexion)
        {

            connexion.inicializarMSTA();
            try
            {                
               

                if (!(this.lsQuerysTracciones == null))
                {
                    //Funcione que instancia la transaccion                
                    this.lsQuerysTracciones.ForEach(delegate (string strQuery)
                    {
                        connexion.mscCmd.CommandText = strQuery;
                        if (!(strQuery == "")) this.intRowsAffected = connexion.mscCmd.ExecuteNonQuery();
                    });
                }

                connexion.msTa.Commit();
                //Si existe filas, asignar resultado exitoso
                if (this.intRowsAffected > 0)
                {
                    this.bolResultado = true;
                }
                //Cerrar connexion                
                this.connexion.close_connection();

            }
            catch (MySqlException e)
            {
                try
                {
                    connexion.msTa.Rollback();
                    this.intRowsAffected = 0;
                    this.bolResultado = false;
                    //Cerrar connexion         
                    this.connexion.close_connection();
                }
                catch (MySqlException ex)
                {
                    if (connexion.msTa.Connection != null)
                    {
                        /*Console.WriteLine("An exception of type " + ex.GetType() +
                        " was encountered while attempting to roll back the transaction.");*/

                    }
                    this.intRowsAffected = 0;
                    this.bolResultado = false;
                    //Cerrar connexion         
                    this.connexion.close_connection();
                    //Enviar mensaje de error
                    MessageBox.Show(ex.Message);
                }
                //Enviar mensaje de error
                MessageBox.Show(e.Message);
            }
        }
    }
}
