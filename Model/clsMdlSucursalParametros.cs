﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Model
{
    public class clsMdlSucursalParametros : Core.clsCoreModel
    {
        public clsMdlSucursalParametros()
        {
        }

        public void buscar(string strSucursald)
        {
            //En caso que no existe connexion se regresa al metodo anterior
            if (!this.bolResultadoConnexion) return;
            this.connexion.mscCmd.CommandText = "SELECT  " +
                " dias_cobro_minimo, " +
                " dias_cobro_cuota_diaria, "+
                " dias_cobrar_cuota_daria_especial,  " +
                " dias_liquidar_boletas, " +
                " dias_refrendar_boletas, " +
                " dias_remate_prendas," +
                " avaluo_calculo, " +
                " avaluo_impresion" +  
                " FROM sucursales_parametros  " +
                " WHERE sucursal_id = @sucursal_id";
            this.connexion.mscCmd.Parameters.AddWithValue("@sucursal_id", strSucursald);
            this.resultadoObtener(this.connexion);
        }
    }
}
