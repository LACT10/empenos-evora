﻿namespace EmpenosEvora
{
    partial class frmBusquedaRangoFechaBoleta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvResultado = new System.Windows.Forms.DataGridView();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtNombreCotitular = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtNombreBeneficario = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAsentamiento = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCurp = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtRfc = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTelefono01 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtCodigoPostal = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtIdentificacion = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtComentario = new System.Windows.Forms.TextBox();
            this.lblDescripcionBoletaRefrendo = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtTotalEmpenado = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtInteresesCobrados = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtTotalActivo = new System.Windows.Forms.TextBox();
            this.btnFooterCierra = new System.Windows.Forms.Button();
            this.txtTelefono02 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNoIdentificacion = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResultado)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvResultado
            // 
            this.dgvResultado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResultado.Location = new System.Drawing.Point(8, 320);
            this.dgvResultado.Name = "dgvResultado";
            this.dgvResultado.ReadOnly = true;
            this.dgvResultado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dgvResultado.Size = new System.Drawing.Size(616, 150);
            this.dgvResultado.TabIndex = 275;
            // 
            // txtNombre
            // 
            this.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombre.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.Location = new System.Drawing.Point(112, 8);
            this.txtNombre.MaxLength = 100;
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(512, 20);
            this.txtNombre.TabIndex = 280;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Cursor = System.Windows.Forms.Cursors.Default;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(8, 8);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 13);
            this.label14.TabIndex = 281;
            this.label14.Text = "Nombre";
            // 
            // txtNombreCotitular
            // 
            this.txtNombreCotitular.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombreCotitular.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNombreCotitular.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreCotitular.Location = new System.Drawing.Point(112, 40);
            this.txtNombreCotitular.MaxLength = 100;
            this.txtNombreCotitular.Name = "txtNombreCotitular";
            this.txtNombreCotitular.Size = new System.Drawing.Size(512, 20);
            this.txtNombreCotitular.TabIndex = 332;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 333;
            this.label4.Text = "Cotitular";
            this.label4.UseWaitCursor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(8, 71);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(60, 13);
            this.label23.TabIndex = 337;
            this.label23.Text = "Beneficario";
            this.label23.UseWaitCursor = true;
            // 
            // txtNombreBeneficario
            // 
            this.txtNombreBeneficario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombreBeneficario.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNombreBeneficario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreBeneficario.Location = new System.Drawing.Point(112, 72);
            this.txtNombreBeneficario.MaxLength = 100;
            this.txtNombreBeneficario.Name = "txtNombreBeneficario";
            this.txtNombreBeneficario.Size = new System.Drawing.Size(512, 20);
            this.txtNombreBeneficario.TabIndex = 336;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 339;
            this.label5.Text = "Dirección";
            this.label5.UseWaitCursor = true;
            // 
            // txtDireccion
            // 
            this.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDireccion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDireccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDireccion.Location = new System.Drawing.Point(112, 138);
            this.txtDireccion.MaxLength = 100;
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.Size = new System.Drawing.Size(512, 20);
            this.txtDireccion.TabIndex = 338;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 341;
            this.label6.Text = "Asentamiento";
            this.label6.UseWaitCursor = true;
            // 
            // txtAsentamiento
            // 
            this.txtAsentamiento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAsentamiento.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAsentamiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsentamiento.Location = new System.Drawing.Point(112, 105);
            this.txtAsentamiento.MaxLength = 100;
            this.txtAsentamiento.Name = "txtAsentamiento";
            this.txtAsentamiento.Size = new System.Drawing.Size(512, 20);
            this.txtAsentamiento.TabIndex = 340;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(328, 232);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 347;
            this.label8.Text = "CURP";
            this.label8.UseWaitCursor = true;
            // 
            // txtCurp
            // 
            this.txtCurp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurp.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCurp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurp.Location = new System.Drawing.Point(424, 234);
            this.txtCurp.MaxLength = 100;
            this.txtCurp.Name = "txtCurp";
            this.txtCurp.Size = new System.Drawing.Size(201, 20);
            this.txtCurp.TabIndex = 346;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(9, 233);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 345;
            this.label9.Text = "R.F.C";
            this.label9.UseWaitCursor = true;
            // 
            // txtRfc
            // 
            this.txtRfc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRfc.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtRfc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRfc.Location = new System.Drawing.Point(112, 234);
            this.txtRfc.MaxLength = 100;
            this.txtRfc.Name = "txtRfc";
            this.txtRfc.Size = new System.Drawing.Size(208, 20);
            this.txtRfc.TabIndex = 344;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(328, 168);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 351;
            this.label10.Text = "Teléfono(s)";
            this.label10.UseWaitCursor = true;
            // 
            // txtTelefono01
            // 
            this.txtTelefono01.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTelefono01.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtTelefono01.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefono01.Location = new System.Drawing.Point(424, 169);
            this.txtTelefono01.MaxLength = 100;
            this.txtTelefono01.Name = "txtTelefono01";
            this.txtTelefono01.Size = new System.Drawing.Size(96, 20);
            this.txtTelefono01.TabIndex = 350;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(9, 168);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 13);
            this.label11.TabIndex = 349;
            this.label11.Text = "Código postal";
            this.label11.UseWaitCursor = true;
            // 
            // txtCodigoPostal
            // 
            this.txtCodigoPostal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoPostal.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoPostal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoPostal.Location = new System.Drawing.Point(112, 169);
            this.txtCodigoPostal.MaxLength = 100;
            this.txtCodigoPostal.Name = "txtCodigoPostal";
            this.txtCodigoPostal.Size = new System.Drawing.Size(208, 20);
            this.txtCodigoPostal.TabIndex = 348;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(8, 199);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 13);
            this.label12.TabIndex = 353;
            this.label12.Text = "Identificación";
            this.label12.UseWaitCursor = true;
            // 
            // txtIdentificacion
            // 
            this.txtIdentificacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIdentificacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtIdentificacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdentificacion.Location = new System.Drawing.Point(112, 200);
            this.txtIdentificacion.MaxLength = 100;
            this.txtIdentificacion.Name = "txtIdentificacion";
            this.txtIdentificacion.Size = new System.Drawing.Size(208, 20);
            this.txtIdentificacion.TabIndex = 352;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(8, 264);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 13);
            this.label13.TabIndex = 355;
            this.label13.Text = "Comentario";
            this.label13.UseWaitCursor = true;
            // 
            // txtComentario
            // 
            this.txtComentario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtComentario.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtComentario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComentario.Location = new System.Drawing.Point(112, 265);
            this.txtComentario.MaxLength = 100;
            this.txtComentario.Name = "txtComentario";
            this.txtComentario.Size = new System.Drawing.Size(512, 20);
            this.txtComentario.TabIndex = 354;
            // 
            // lblDescripcionBoletaRefrendo
            // 
            this.lblDescripcionBoletaRefrendo.AutoSize = true;
            this.lblDescripcionBoletaRefrendo.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblDescripcionBoletaRefrendo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescripcionBoletaRefrendo.Location = new System.Drawing.Point(8, 296);
            this.lblDescripcionBoletaRefrendo.Name = "lblDescripcionBoletaRefrendo";
            this.lblDescripcionBoletaRefrendo.Size = new System.Drawing.Size(116, 13);
            this.lblDescripcionBoletaRefrendo.TabIndex = 367;
            this.lblDescripcionBoletaRefrendo.Text = "Boletas del cliente:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Cursor = System.Windows.Forms.Cursors.Default;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(392, 480);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(98, 13);
            this.label15.TabIndex = 445;
            this.label15.Text = "Total empeñado";
            // 
            // txtTotalEmpenado
            // 
            this.txtTotalEmpenado.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtTotalEmpenado.Enabled = false;
            this.txtTotalEmpenado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtTotalEmpenado.Location = new System.Drawing.Point(520, 480);
            this.txtTotalEmpenado.MaxLength = 5;
            this.txtTotalEmpenado.Name = "txtTotalEmpenado";
            this.txtTotalEmpenado.Size = new System.Drawing.Size(105, 20);
            this.txtTotalEmpenado.TabIndex = 444;
            this.txtTotalEmpenado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Cursor = System.Windows.Forms.Cursors.Default;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(392, 512);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(115, 13);
            this.label16.TabIndex = 447;
            this.label16.Text = "Intereses cobrados";
            // 
            // txtInteresesCobrados
            // 
            this.txtInteresesCobrados.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtInteresesCobrados.Enabled = false;
            this.txtInteresesCobrados.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtInteresesCobrados.Location = new System.Drawing.Point(520, 512);
            this.txtInteresesCobrados.MaxLength = 5;
            this.txtInteresesCobrados.Name = "txtInteresesCobrados";
            this.txtInteresesCobrados.Size = new System.Drawing.Size(105, 20);
            this.txtInteresesCobrados.TabIndex = 446;
            this.txtInteresesCobrados.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Cursor = System.Windows.Forms.Cursors.Default;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(392, 544);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(75, 13);
            this.label17.TabIndex = 449;
            this.label17.Text = "Total activo";
            // 
            // txtTotalActivo
            // 
            this.txtTotalActivo.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtTotalActivo.Enabled = false;
            this.txtTotalActivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtTotalActivo.Location = new System.Drawing.Point(520, 544);
            this.txtTotalActivo.MaxLength = 5;
            this.txtTotalActivo.Name = "txtTotalActivo";
            this.txtTotalActivo.Size = new System.Drawing.Size(105, 20);
            this.txtTotalActivo.TabIndex = 448;
            this.txtTotalActivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnFooterCierra
            // 
            this.btnFooterCierra.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnFooterCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnFooterCierra.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterCierra.Location = new System.Drawing.Point(8, 528);
            this.btnFooterCierra.Name = "btnFooterCierra";
            this.btnFooterCierra.Size = new System.Drawing.Size(75, 40);
            this.btnFooterCierra.TabIndex = 450;
            this.btnFooterCierra.Text = "Salir";
            this.btnFooterCierra.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterCierra.UseVisualStyleBackColor = true;
            this.btnFooterCierra.Click += new System.EventHandler(this.btnFooterCierra_Click);
            // 
            // txtTelefono02
            // 
            this.txtTelefono02.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTelefono02.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtTelefono02.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefono02.Location = new System.Drawing.Point(528, 168);
            this.txtTelefono02.MaxLength = 100;
            this.txtTelefono02.Name = "txtTelefono02";
            this.txtTelefono02.Size = new System.Drawing.Size(96, 20);
            this.txtTelefono02.TabIndex = 451;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(328, 200);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 453;
            this.label1.Text = "No. Identificación";
            this.label1.UseWaitCursor = true;
            // 
            // txtNoIdentificacion
            // 
            this.txtNoIdentificacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNoIdentificacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNoIdentificacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoIdentificacion.Location = new System.Drawing.Point(424, 200);
            this.txtNoIdentificacion.MaxLength = 100;
            this.txtNoIdentificacion.Name = "txtNoIdentificacion";
            this.txtNoIdentificacion.Size = new System.Drawing.Size(200, 20);
            this.txtNoIdentificacion.TabIndex = 452;
            // 
            // frmBusquedaRangoFechaBoleta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(635, 573);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNoIdentificacion);
            this.Controls.Add(this.txtTelefono02);
            this.Controls.Add(this.btnFooterCierra);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtTotalActivo);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtInteresesCobrados);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtTotalEmpenado);
            this.Controls.Add(this.lblDescripcionBoletaRefrendo);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtComentario);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtIdentificacion);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtTelefono01);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtCodigoPostal);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtCurp);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtRfc);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtAsentamiento);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtDireccion);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.txtNombreBeneficario);
            this.Controls.Add(this.txtNombreCotitular);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.dgvResultado);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBusquedaRangoFechaBoleta";
            this.Text = "Historial de cliente";
            this.Load += new System.EventHandler(this.frmBusquedaRangoFechaBoleta_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvResultado)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvResultado;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtNombreCotitular;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtNombreBeneficario;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDireccion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAsentamiento;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCurp;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtRfc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtTelefono01;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtCodigoPostal;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtIdentificacion;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtComentario;
        private System.Windows.Forms.Label lblDescripcionBoletaRefrendo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtTotalEmpenado;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtInteresesCobrados;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtTotalActivo;
        private System.Windows.Forms.Button btnFooterCierra;
        private System.Windows.Forms.TextBox txtTelefono02;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNoIdentificacion;
    }
}