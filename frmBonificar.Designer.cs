﻿namespace EmpenosEvora
{
    partial class frmBonificar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFooterCierra = new System.Windows.Forms.Button();
            this.txtUsuarioId = new System.Windows.Forms.TextBox();
            this.btnVerificarHuella = new System.Windows.Forms.Button();
            this.btnFooterAcceder = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtContrasena = new System.Windows.Forms.TextBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMotivo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnFooterCierra
            // 
            this.btnFooterCierra.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnFooterCierra.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterCierra.Location = new System.Drawing.Point(320, 232);
            this.btnFooterCierra.Name = "btnFooterCierra";
            this.btnFooterCierra.Size = new System.Drawing.Size(75, 40);
            this.btnFooterCierra.TabIndex = 6;
            this.btnFooterCierra.Text = "Salir";
            this.btnFooterCierra.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterCierra.UseVisualStyleBackColor = true;
            this.btnFooterCierra.Click += new System.EventHandler(this.btnFooterCierra_Click);
            // 
            // txtUsuarioId
            // 
            this.txtUsuarioId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtUsuarioId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuarioId.Location = new System.Drawing.Point(248, 8);
            this.txtUsuarioId.MaxLength = 3;
            this.txtUsuarioId.Name = "txtUsuarioId";
            this.txtUsuarioId.Size = new System.Drawing.Size(44, 20);
            this.txtUsuarioId.TabIndex = 582;
            this.txtUsuarioId.Visible = false;
            // 
            // btnVerificarHuella
            // 
            this.btnVerificarHuella.Location = new System.Drawing.Point(264, 184);
            this.btnVerificarHuella.Name = "btnVerificarHuella";
            this.btnVerificarHuella.Size = new System.Drawing.Size(128, 21);
            this.btnVerificarHuella.TabIndex = 4;
            this.btnVerificarHuella.Text = "Verificar con huella";
            this.btnVerificarHuella.UseVisualStyleBackColor = true;
            this.btnVerificarHuella.Click += new System.EventHandler(this.btnVerificarHuella_Click);
            // 
            // btnFooterAcceder
            // 
            this.btnFooterAcceder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterAcceder.Image = global::EmpenosEvora.Properties.Resources.Compliant_16x;
            this.btnFooterAcceder.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterAcceder.Location = new System.Drawing.Point(240, 232);
            this.btnFooterAcceder.Name = "btnFooterAcceder";
            this.btnFooterAcceder.Size = new System.Drawing.Size(75, 40);
            this.btnFooterAcceder.TabIndex = 5;
            this.btnFooterAcceder.Text = "Autorizar";
            this.btnFooterAcceder.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterAcceder.UseVisualStyleBackColor = true;
            this.btnFooterAcceder.Click += new System.EventHandler(this.btnFooterAcceder_Click);
            // 
            // label15
            // 
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(-218, 216);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(974, 2);
            this.label15.TabIndex = 581;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 580;
            this.label1.Text = "Contraseña";
            // 
            // txtContrasena
            // 
            this.txtContrasena.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtContrasena.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtContrasena.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContrasena.Location = new System.Drawing.Point(72, 40);
            this.txtContrasena.MaxLength = 200;
            this.txtContrasena.Name = "txtContrasena";
            this.txtContrasena.PasswordChar = '*';
            this.txtContrasena.Size = new System.Drawing.Size(320, 20);
            this.txtContrasena.TabIndex = 2;
            // 
            // txtUsuario
            // 
            this.txtUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUsuario.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuario.Location = new System.Drawing.Point(72, 8);
            this.txtUsuario.MaxLength = 200;
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(320, 20);
            this.txtUsuario.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 584;
            this.label2.Text = "Usuario";
            // 
            // txtMotivo
            // 
            this.txtMotivo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMotivo.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtMotivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtMotivo.Location = new System.Drawing.Point(72, 72);
            this.txtMotivo.MaxLength = 250;
            this.txtMotivo.Multiline = true;
            this.txtMotivo.Name = "txtMotivo";
            this.txtMotivo.Size = new System.Drawing.Size(320, 104);
            this.txtMotivo.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 586;
            this.label3.Text = "Motivo";
            // 
            // frmBonificar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(412, 282);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtMotivo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnFooterCierra);
            this.Controls.Add(this.txtUsuarioId);
            this.Controls.Add(this.btnVerificarHuella);
            this.Controls.Add(this.btnFooterAcceder);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtContrasena);
            this.Controls.Add(this.txtUsuario);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBonificar";
            this.Text = "Bonificar";
            this.Load += new System.EventHandler(this.frmBonificar_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnFooterCierra;
        private System.Windows.Forms.TextBox txtUsuarioId;
        private System.Windows.Forms.Button btnVerificarHuella;
        private System.Windows.Forms.Button btnFooterAcceder;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtContrasena;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMotivo;
        private System.Windows.Forms.Label label3;
    }
}