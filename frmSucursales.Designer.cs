﻿namespace EmpenosEvora
{
    partial class frmSucursales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.dtpInicioOperaciones = new System.Windows.Forms.DateTimePicker();
            this.txtNoExt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCodigoMunicipio = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtTelefono1 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtRepresentanteLegal = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtCalle = new System.Windows.Forms.TextBox();
            this.txtDescripcionMunicipio = new System.Windows.Forms.TextBox();
            this.txtDescripcionEstado = new System.Windows.Forms.TextBox();
            this.txtCodigoEstado = new System.Windows.Forms.TextBox();
            this.txtTelefono2 = new System.Windows.Forms.TextBox();
            this.txtNoInt = new System.Windows.Forms.TextBox();
            this.btnFooterCierra = new System.Windows.Forms.Button();
            this.btnFooterGuardar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tsAcciones = new System.Windows.Forms.ToolStrip();
            this.tsbNuevo = new System.Windows.Forms.ToolStripButton();
            this.tsbGuardar = new System.Windows.Forms.ToolStripButton();
            this.tsbEliminar = new System.Windows.Forms.ToolStripButton();
            this.tsbReactivar = new System.Windows.Forms.ToolStripButton();
            this.tsbBuscar = new System.Windows.Forms.ToolStripButton();
            this.btnUndo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrimeroPag = new System.Windows.Forms.ToolStripButton();
            this.btnAtras = new System.Windows.Forms.ToolStripButton();
            this.btnAdelante = new System.Windows.Forms.ToolStripButton();
            this.btnUlitmoPag = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.btnCierra = new System.Windows.Forms.ToolStripButton();
            this.btnBusqueda = new System.Windows.Forms.Button();
            this.btnBusquedaEmpresas = new System.Windows.Forms.Button();
            this.txtDescripcionEmpresas = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtCodigoPostal = new System.Windows.Forms.TextBox();
            this.btnBusquedaAsentamiento = new System.Windows.Forms.Button();
            this.txtDescripcionAsentamiento = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbsControlSucursal = new System.Windows.Forms.TabControl();
            this.tpInformacionGeneral = new System.Windows.Forms.TabPage();
            this.lblDescEstatus = new System.Windows.Forms.Label();
            this.lblEstatus = new System.Windows.Forms.Label();
            this.txtAsentamientoId = new System.Windows.Forms.TextBox();
            this.txtEmpresaId = new System.Windows.Forms.TextBox();
            this.txtScursalId = new System.Windows.Forms.TextBox();
            this.tpConfiguracion = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.dgvParametos = new System.Windows.Forms.DataGridView();
            this.label11 = new System.Windows.Forms.Label();
            this.dgvSucursalesGramajes = new System.Windows.Forms.DataGridView();
            this.label10 = new System.Windows.Forms.Label();
            this.dgvSucursalesPlazo = new System.Windows.Forms.DataGridView();
            this.label9 = new System.Windows.Forms.Label();
            this.dgvSucursalesCobrosTipos = new System.Windows.Forms.DataGridView();
            this.tpPromociones = new System.Windows.Forms.TabPage();
            this.dgvDetallesPromociones = new System.Windows.Forms.DataGridView();
            this.sucursal_promocion_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.porcentaje_pro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TiposDePrendas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acciones = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnAgregarPromociones = new System.Windows.Forms.Button();
            this.tsAcciones.SuspendLayout();
            this.tbsControlSucursal.SuspendLayout();
            this.tpInformacionGeneral.SuspendLayout();
            this.tpConfiguracion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvParametos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSucursalesGramajes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSucursalesPlazo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSucursalesCobrosTipos)).BeginInit();
            this.tpPromociones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetallesPromociones)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 55;
            this.label3.Text = "Inicio operaciones";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 13);
            this.label2.TabIndex = 52;
            this.label2.Text = "Representante legal";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 49;
            this.label1.Text = "Empresa";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcion.Location = new System.Drawing.Point(232, 40);
            this.txtDescripcion.MaxLength = 50;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(776, 20);
            this.txtDescripcion.TabIndex = 2;
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(16, 40);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(48, 13);
            this.lblCodigo.TabIndex = 43;
            this.lblCodigo.Text = "Sucursal";
            // 
            // txtCodigo
            // 
            this.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigo.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigo.Location = new System.Drawing.Point(128, 40);
            this.txtCodigo.MaxLength = 5;
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(96, 20);
            this.txtCodigo.TabIndex = 1;
            this.txtCodigo.TextChanged += new System.EventHandler(this.TxtCodigo_TextChanged);
            this.txtCodigo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtCodigo_KeyPress);
            this.txtCodigo.Leave += new System.EventHandler(this.TxtCodigo_Leave);
            // 
            // dtpInicioOperaciones
            // 
            this.dtpInicioOperaciones.Cursor = System.Windows.Forms.Cursors.Default;
            this.dtpInicioOperaciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpInicioOperaciones.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpInicioOperaciones.Location = new System.Drawing.Point(128, 136);
            this.dtpInicioOperaciones.Name = "dtpInicioOperaciones";
            this.dtpInicioOperaciones.Size = new System.Drawing.Size(104, 20);
            this.dtpInicioOperaciones.TabIndex = 6;
            // 
            // txtNoExt
            // 
            this.txtNoExt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNoExt.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNoExt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoExt.Location = new System.Drawing.Point(464, 168);
            this.txtNoExt.MaxLength = 15;
            this.txtNoExt.Name = "txtNoExt";
            this.txtNoExt.Size = new System.Drawing.Size(50, 20);
            this.txtNoExt.TabIndex = 11;
            this.txtNoExt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNoExt_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(584, 200);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 282;
            this.label5.Text = "Estado";
            // 
            // txtCodigoMunicipio
            // 
            this.txtCodigoMunicipio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoMunicipio.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoMunicipio.Enabled = false;
            this.txtCodigoMunicipio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoMunicipio.Location = new System.Drawing.Point(264, 200);
            this.txtCodigoMunicipio.MaxLength = 50;
            this.txtCodigoMunicipio.Name = "txtCodigoMunicipio";
            this.txtCodigoMunicipio.ReadOnly = true;
            this.txtCodigoMunicipio.Size = new System.Drawing.Size(44, 20);
            this.txtCodigoMunicipio.TabIndex = 279;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(200, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 278;
            this.label6.Text = "Municipio";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Cursor = System.Windows.Forms.Cursors.Default;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(384, 168);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(68, 13);
            this.label37.TabIndex = 271;
            this.label37.Text = "No. Ext. y Int";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Cursor = System.Windows.Forms.Cursors.Default;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(16, 168);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(30, 13);
            this.label38.TabIndex = 286;
            this.label38.Text = "Calle";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Cursor = System.Windows.Forms.Cursors.Default;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(248, 136);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(60, 13);
            this.label24.TabIndex = 291;
            this.label24.Text = "Teléfono(s)";
            // 
            // txtTelefono1
            // 
            this.txtTelefono1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTelefono1.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtTelefono1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefono1.Location = new System.Drawing.Point(328, 136);
            this.txtTelefono1.MaxLength = 10;
            this.txtTelefono1.Name = "txtTelefono1";
            this.txtTelefono1.Size = new System.Drawing.Size(72, 20);
            this.txtTelefono1.TabIndex = 7;
            this.txtTelefono1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTelefono1_KeyPress);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Cursor = System.Windows.Forms.Cursors.Default;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(584, 136);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(93, 13);
            this.label22.TabIndex = 288;
            this.label22.Text = "Correo electrónico";
            // 
            // txtRepresentanteLegal
            // 
            this.txtRepresentanteLegal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRepresentanteLegal.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtRepresentanteLegal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRepresentanteLegal.Location = new System.Drawing.Point(128, 104);
            this.txtRepresentanteLegal.MaxLength = 250;
            this.txtRepresentanteLegal.Name = "txtRepresentanteLegal";
            this.txtRepresentanteLegal.Size = new System.Drawing.Size(920, 20);
            this.txtRepresentanteLegal.TabIndex = 5;
            // 
            // txtEmail
            // 
            this.txtEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEmail.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Location = new System.Drawing.Point(688, 136);
            this.txtEmail.MaxLength = 50;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(360, 20);
            this.txtEmail.TabIndex = 9;
            this.txtEmail.Leave += new System.EventHandler(this.txtEmail_Leave);
            // 
            // txtCalle
            // 
            this.txtCalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCalle.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCalle.Location = new System.Drawing.Point(128, 168);
            this.txtCalle.MaxLength = 250;
            this.txtCalle.Name = "txtCalle";
            this.txtCalle.Size = new System.Drawing.Size(240, 20);
            this.txtCalle.TabIndex = 10;
            // 
            // txtDescripcionMunicipio
            // 
            this.txtDescripcionMunicipio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionMunicipio.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionMunicipio.Enabled = false;
            this.txtDescripcionMunicipio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcionMunicipio.Location = new System.Drawing.Point(312, 200);
            this.txtDescripcionMunicipio.MaxLength = 250;
            this.txtDescripcionMunicipio.Name = "txtDescripcionMunicipio";
            this.txtDescripcionMunicipio.ReadOnly = true;
            this.txtDescripcionMunicipio.Size = new System.Drawing.Size(256, 20);
            this.txtDescripcionMunicipio.TabIndex = 299;
            // 
            // txtDescripcionEstado
            // 
            this.txtDescripcionEstado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionEstado.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionEstado.Enabled = false;
            this.txtDescripcionEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcionEstado.Location = new System.Drawing.Point(736, 200);
            this.txtDescripcionEstado.MaxLength = 250;
            this.txtDescripcionEstado.Name = "txtDescripcionEstado";
            this.txtDescripcionEstado.ReadOnly = true;
            this.txtDescripcionEstado.Size = new System.Drawing.Size(312, 20);
            this.txtDescripcionEstado.TabIndex = 300;
            // 
            // txtCodigoEstado
            // 
            this.txtCodigoEstado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoEstado.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoEstado.Enabled = false;
            this.txtCodigoEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoEstado.Location = new System.Drawing.Point(688, 200);
            this.txtCodigoEstado.MaxLength = 50;
            this.txtCodigoEstado.Name = "txtCodigoEstado";
            this.txtCodigoEstado.ReadOnly = true;
            this.txtCodigoEstado.Size = new System.Drawing.Size(44, 20);
            this.txtCodigoEstado.TabIndex = 301;
            // 
            // txtTelefono2
            // 
            this.txtTelefono2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTelefono2.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtTelefono2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefono2.Location = new System.Drawing.Point(408, 136);
            this.txtTelefono2.MaxLength = 10;
            this.txtTelefono2.Name = "txtTelefono2";
            this.txtTelefono2.Size = new System.Drawing.Size(72, 20);
            this.txtTelefono2.TabIndex = 8;
            this.txtTelefono2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTelefono2_KeyPress);
            // 
            // txtNoInt
            // 
            this.txtNoInt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNoInt.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNoInt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoInt.Location = new System.Drawing.Point(520, 168);
            this.txtNoInt.MaxLength = 15;
            this.txtNoInt.Name = "txtNoInt";
            this.txtNoInt.Size = new System.Drawing.Size(50, 20);
            this.txtNoInt.TabIndex = 12;
            // 
            // btnFooterCierra
            // 
            this.btnFooterCierra.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnFooterCierra.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterCierra.Location = new System.Drawing.Point(1000, 368);
            this.btnFooterCierra.Name = "btnFooterCierra";
            this.btnFooterCierra.Size = new System.Drawing.Size(75, 40);
            this.btnFooterCierra.TabIndex = 16;
            this.btnFooterCierra.Text = "Salir";
            this.btnFooterCierra.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterCierra.UseVisualStyleBackColor = true;
            this.btnFooterCierra.Click += new System.EventHandler(this.BtnFooterCierra_Click);
            // 
            // btnFooterGuardar
            // 
            this.btnFooterGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterGuardar.Image = global::EmpenosEvora.Properties.Resources.Save_16x;
            this.btnFooterGuardar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterGuardar.Location = new System.Drawing.Point(912, 368);
            this.btnFooterGuardar.Name = "btnFooterGuardar";
            this.btnFooterGuardar.Size = new System.Drawing.Size(75, 40);
            this.btnFooterGuardar.TabIndex = 15;
            this.btnFooterGuardar.Text = "Guardar";
            this.btnFooterGuardar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterGuardar.UseVisualStyleBackColor = true;
            this.btnFooterGuardar.Click += new System.EventHandler(this.BtnFooterGuardar_Click);
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(-48, 352);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(1128, 2);
            this.label4.TabIndex = 497;
            // 
            // tsAcciones
            // 
            this.tsAcciones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbNuevo,
            this.tsbGuardar,
            this.tsbEliminar,
            this.tsbReactivar,
            this.tsbBuscar,
            this.btnUndo,
            this.toolStripSeparator2,
            this.btnPrimeroPag,
            this.btnAtras,
            this.btnAdelante,
            this.btnUlitmoPag,
            this.toolStripSeparator1,
            this.toolStripButton3,
            this.btnCierra});
            this.tsAcciones.Location = new System.Drawing.Point(0, 0);
            this.tsAcciones.Name = "tsAcciones";
            this.tsAcciones.Size = new System.Drawing.Size(1088, 25);
            this.tsAcciones.TabIndex = 500;
            this.tsAcciones.Text = "toolStrip2";
            // 
            // tsbNuevo
            // 
            this.tsbNuevo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbNuevo.Image = global::EmpenosEvora.Properties.Resources.AddFile_16x;
            this.tsbNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbNuevo.Name = "tsbNuevo";
            this.tsbNuevo.Size = new System.Drawing.Size(23, 22);
            this.tsbNuevo.Text = "Nuevo";
            this.tsbNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical90;
            this.tsbNuevo.Click += new System.EventHandler(this.TsbNuevo_Click);
            // 
            // tsbGuardar
            // 
            this.tsbGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbGuardar.Image = global::EmpenosEvora.Properties.Resources.Save_16x;
            this.tsbGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGuardar.Name = "tsbGuardar";
            this.tsbGuardar.Size = new System.Drawing.Size(23, 22);
            this.tsbGuardar.Tag = "";
            this.tsbGuardar.Text = "Guardar";
            this.tsbGuardar.Click += new System.EventHandler(this.TsbGuardar_Click);
            // 
            // tsbEliminar
            // 
            this.tsbEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbEliminar.Image = global::EmpenosEvora.Properties.Resources.Cancel_16x;
            this.tsbEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEliminar.Name = "tsbEliminar";
            this.tsbEliminar.Size = new System.Drawing.Size(23, 22);
            this.tsbEliminar.Text = "Cancelar";
            this.tsbEliminar.ToolTipText = "Desactivar";
            this.tsbEliminar.Click += new System.EventHandler(this.TsbEliminar_Click);
            // 
            // tsbReactivar
            // 
            this.tsbReactivar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbReactivar.Image = global::EmpenosEvora.Properties.Resources.CompareValidator_16x;
            this.tsbReactivar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbReactivar.Name = "tsbReactivar";
            this.tsbReactivar.Size = new System.Drawing.Size(23, 22);
            this.tsbReactivar.Text = "Reactivar";
            this.tsbReactivar.ToolTipText = "Reactivar";
            this.tsbReactivar.Visible = false;
            this.tsbReactivar.Click += new System.EventHandler(this.tsbReactivar_Click);
            // 
            // tsbBuscar
            // 
            this.tsbBuscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBuscar.Image = global::EmpenosEvora.Properties.Resources.VBSearch_16x;
            this.tsbBuscar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBuscar.Name = "tsbBuscar";
            this.tsbBuscar.Size = new System.Drawing.Size(23, 22);
            this.tsbBuscar.Text = "Buscar";
            this.tsbBuscar.Click += new System.EventHandler(this.TsbBuscar_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUndo.Image = global::EmpenosEvora.Properties.Resources.Undo_16x;
            this.btnUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(23, 22);
            this.btnUndo.Text = "Deshacer";
            this.btnUndo.Click += new System.EventHandler(this.BtnUndo_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrimeroPag
            // 
            this.btnPrimeroPag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrimeroPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeftGroup_16xSM;
            this.btnPrimeroPag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrimeroPag.Name = "btnPrimeroPag";
            this.btnPrimeroPag.Size = new System.Drawing.Size(23, 22);
            this.btnPrimeroPag.Text = "Primero";
            this.btnPrimeroPag.Click += new System.EventHandler(this.BtnPrimeroPag_Click);
            // 
            // btnAtras
            // 
            this.btnAtras.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAtras.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeft_16x;
            this.btnAtras.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(23, 22);
            this.btnAtras.Text = "Atras";
            this.btnAtras.Click += new System.EventHandler(this.BtnAtras_Click);
            // 
            // btnAdelante
            // 
            this.btnAdelante.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdelante.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronRight_16x;
            this.btnAdelante.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdelante.Name = "btnAdelante";
            this.btnAdelante.Size = new System.Drawing.Size(23, 22);
            this.btnAdelante.Text = "Siguente";
            this.btnAdelante.Click += new System.EventHandler(this.BtnAdelante_Click);
            // 
            // btnUlitmoPag
            // 
            this.btnUlitmoPag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUlitmoPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronRightGroup_16x;
            this.btnUlitmoPag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUlitmoPag.Name = "btnUlitmoPag";
            this.btnUlitmoPag.Size = new System.Drawing.Size(23, 22);
            this.btnUlitmoPag.Text = "Ultimo";
            this.btnUlitmoPag.Click += new System.EventHandler(this.BtnUlitmoPag_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::EmpenosEvora.Properties.Resources.Print_16x;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "Imprimir";
            this.toolStripButton3.ToolTipText = "Imprimir";
            // 
            // btnCierra
            // 
            this.btnCierra.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnCierra.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCierra.Name = "btnCierra";
            this.btnCierra.Size = new System.Drawing.Size(23, 22);
            this.btnCierra.Text = "Salir";
            this.btnCierra.Click += new System.EventHandler(this.BtnCierra_Click);
            // 
            // btnBusqueda
            // 
            this.btnBusqueda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBusqueda.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusqueda.Location = new System.Drawing.Point(1016, 40);
            this.btnBusqueda.Name = "btnBusqueda";
            this.btnBusqueda.Size = new System.Drawing.Size(32, 23);
            this.btnBusqueda.TabIndex = 3;
            this.btnBusqueda.UseVisualStyleBackColor = true;
            this.btnBusqueda.Click += new System.EventHandler(this.BtnBusqueda_Click);
            // 
            // btnBusquedaEmpresas
            // 
            this.btnBusquedaEmpresas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBusquedaEmpresas.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusquedaEmpresas.Location = new System.Drawing.Point(1016, 72);
            this.btnBusquedaEmpresas.Name = "btnBusquedaEmpresas";
            this.btnBusquedaEmpresas.Size = new System.Drawing.Size(32, 23);
            this.btnBusquedaEmpresas.TabIndex = 4;
            this.btnBusquedaEmpresas.UseVisualStyleBackColor = true;
            this.btnBusquedaEmpresas.Click += new System.EventHandler(this.BtnBusquedaEmpresas_Click);
            // 
            // txtDescripcionEmpresas
            // 
            this.txtDescripcionEmpresas.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionEmpresas.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionEmpresas.Enabled = false;
            this.txtDescripcionEmpresas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcionEmpresas.Location = new System.Drawing.Point(128, 72);
            this.txtDescripcionEmpresas.MaxLength = 250;
            this.txtDescripcionEmpresas.Name = "txtDescripcionEmpresas";
            this.txtDescripcionEmpresas.ReadOnly = true;
            this.txtDescripcionEmpresas.Size = new System.Drawing.Size(880, 20);
            this.txtDescripcionEmpresas.TabIndex = 503;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Cursor = System.Windows.Forms.Cursors.Default;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(16, 200);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(71, 13);
            this.label35.TabIndex = 273;
            this.label35.Text = "Código postal";
            // 
            // txtCodigoPostal
            // 
            this.txtCodigoPostal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoPostal.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoPostal.Enabled = false;
            this.txtCodigoPostal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoPostal.Location = new System.Drawing.Point(128, 200);
            this.txtCodigoPostal.MaxLength = 5;
            this.txtCodigoPostal.Name = "txtCodigoPostal";
            this.txtCodigoPostal.ReadOnly = true;
            this.txtCodigoPostal.Size = new System.Drawing.Size(48, 20);
            this.txtCodigoPostal.TabIndex = 298;
            // 
            // btnBusquedaAsentamiento
            // 
            this.btnBusquedaAsentamiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBusquedaAsentamiento.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusquedaAsentamiento.Location = new System.Drawing.Point(1016, 168);
            this.btnBusquedaAsentamiento.Name = "btnBusquedaAsentamiento";
            this.btnBusquedaAsentamiento.Size = new System.Drawing.Size(32, 23);
            this.btnBusquedaAsentamiento.TabIndex = 14;
            this.btnBusquedaAsentamiento.UseVisualStyleBackColor = true;
            this.btnBusquedaAsentamiento.Click += new System.EventHandler(this.BtnBusquedaAsentamiento_Click);
            // 
            // txtDescripcionAsentamiento
            // 
            this.txtDescripcionAsentamiento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionAsentamiento.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionAsentamiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcionAsentamiento.Location = new System.Drawing.Point(688, 168);
            this.txtDescripcionAsentamiento.MaxLength = 250;
            this.txtDescripcionAsentamiento.Name = "txtDescripcionAsentamiento";
            this.txtDescripcionAsentamiento.Size = new System.Drawing.Size(320, 20);
            this.txtDescripcionAsentamiento.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(584, 168);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 508;
            this.label7.Text = "Asentamiento";
            // 
            // tbsControlSucursal
            // 
            this.tbsControlSucursal.Controls.Add(this.tpInformacionGeneral);
            this.tbsControlSucursal.Controls.Add(this.tpConfiguracion);
            this.tbsControlSucursal.Controls.Add(this.tpPromociones);
            this.tbsControlSucursal.Location = new System.Drawing.Point(8, 40);
            this.tbsControlSucursal.Name = "tbsControlSucursal";
            this.tbsControlSucursal.SelectedIndex = 0;
            this.tbsControlSucursal.Size = new System.Drawing.Size(1072, 288);
            this.tbsControlSucursal.TabIndex = 511;
            this.tbsControlSucursal.SelectedIndexChanged += new System.EventHandler(this.tbsControlSucursal_SelectedIndexChanged);
            this.tbsControlSucursal.Selected += new System.Windows.Forms.TabControlEventHandler(this.tbsControlSucursal_Selected);
            // 
            // tpInformacionGeneral
            // 
            this.tpInformacionGeneral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(245)))));
            this.tpInformacionGeneral.Controls.Add(this.lblDescEstatus);
            this.tpInformacionGeneral.Controls.Add(this.lblEstatus);
            this.tpInformacionGeneral.Controls.Add(this.txtAsentamientoId);
            this.tpInformacionGeneral.Controls.Add(this.txtEmpresaId);
            this.tpInformacionGeneral.Controls.Add(this.txtScursalId);
            this.tpInformacionGeneral.Controls.Add(this.txtDescripcion);
            this.tpInformacionGeneral.Controls.Add(this.btnBusquedaAsentamiento);
            this.tpInformacionGeneral.Controls.Add(this.txtCodigo);
            this.tpInformacionGeneral.Controls.Add(this.txtDescripcionAsentamiento);
            this.tpInformacionGeneral.Controls.Add(this.lblCodigo);
            this.tpInformacionGeneral.Controls.Add(this.label7);
            this.tpInformacionGeneral.Controls.Add(this.label1);
            this.tpInformacionGeneral.Controls.Add(this.label2);
            this.tpInformacionGeneral.Controls.Add(this.label3);
            this.tpInformacionGeneral.Controls.Add(this.btnBusquedaEmpresas);
            this.tpInformacionGeneral.Controls.Add(this.dtpInicioOperaciones);
            this.tpInformacionGeneral.Controls.Add(this.txtDescripcionEmpresas);
            this.tpInformacionGeneral.Controls.Add(this.label37);
            this.tpInformacionGeneral.Controls.Add(this.label35);
            this.tpInformacionGeneral.Controls.Add(this.btnBusqueda);
            this.tpInformacionGeneral.Controls.Add(this.label6);
            this.tpInformacionGeneral.Controls.Add(this.txtCodigoMunicipio);
            this.tpInformacionGeneral.Controls.Add(this.label5);
            this.tpInformacionGeneral.Controls.Add(this.txtNoExt);
            this.tpInformacionGeneral.Controls.Add(this.label38);
            this.tpInformacionGeneral.Controls.Add(this.txtNoInt);
            this.tpInformacionGeneral.Controls.Add(this.label22);
            this.tpInformacionGeneral.Controls.Add(this.txtTelefono2);
            this.tpInformacionGeneral.Controls.Add(this.txtTelefono1);
            this.tpInformacionGeneral.Controls.Add(this.txtCodigoEstado);
            this.tpInformacionGeneral.Controls.Add(this.label24);
            this.tpInformacionGeneral.Controls.Add(this.txtDescripcionEstado);
            this.tpInformacionGeneral.Controls.Add(this.txtRepresentanteLegal);
            this.tpInformacionGeneral.Controls.Add(this.txtDescripcionMunicipio);
            this.tpInformacionGeneral.Controls.Add(this.txtEmail);
            this.tpInformacionGeneral.Controls.Add(this.txtCodigoPostal);
            this.tpInformacionGeneral.Controls.Add(this.txtCalle);
            this.tpInformacionGeneral.Location = new System.Drawing.Point(4, 22);
            this.tpInformacionGeneral.Name = "tpInformacionGeneral";
            this.tpInformacionGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.tpInformacionGeneral.Size = new System.Drawing.Size(1064, 262);
            this.tpInformacionGeneral.TabIndex = 0;
            this.tpInformacionGeneral.Text = "Información General";
            this.tpInformacionGeneral.Click += new System.EventHandler(this.tpInformacionGeneral_Click);
            // 
            // lblDescEstatus
            // 
            this.lblDescEstatus.AutoSize = true;
            this.lblDescEstatus.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblDescEstatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblDescEstatus.ForeColor = System.Drawing.Color.Black;
            this.lblDescEstatus.Location = new System.Drawing.Point(16, 16);
            this.lblDescEstatus.Name = "lblDescEstatus";
            this.lblDescEstatus.Size = new System.Drawing.Size(42, 13);
            this.lblDescEstatus.TabIndex = 601;
            this.lblDescEstatus.Text = "Estatus";
            this.lblDescEstatus.Visible = false;
            // 
            // lblEstatus
            // 
            this.lblEstatus.AutoSize = true;
            this.lblEstatus.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblEstatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstatus.ForeColor = System.Drawing.Color.Red;
            this.lblEstatus.Location = new System.Drawing.Point(128, 16);
            this.lblEstatus.Name = "lblEstatus";
            this.lblEstatus.Size = new System.Drawing.Size(50, 13);
            this.lblEstatus.TabIndex = 600;
            this.lblEstatus.Text = "NUEVO";
            this.lblEstatus.Visible = false;
            // 
            // txtAsentamientoId
            // 
            this.txtAsentamientoId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAsentamientoId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsentamientoId.Location = new System.Drawing.Point(792, 168);
            this.txtAsentamientoId.MaxLength = 3;
            this.txtAsentamientoId.Name = "txtAsentamientoId";
            this.txtAsentamientoId.Size = new System.Drawing.Size(44, 20);
            this.txtAsentamientoId.TabIndex = 513;
            this.txtAsentamientoId.Visible = false;
            // 
            // txtEmpresaId
            // 
            this.txtEmpresaId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtEmpresaId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmpresaId.Location = new System.Drawing.Point(792, 72);
            this.txtEmpresaId.MaxLength = 3;
            this.txtEmpresaId.Name = "txtEmpresaId";
            this.txtEmpresaId.Size = new System.Drawing.Size(44, 20);
            this.txtEmpresaId.TabIndex = 512;
            this.txtEmpresaId.Visible = false;
            // 
            // txtScursalId
            // 
            this.txtScursalId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtScursalId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtScursalId.Location = new System.Drawing.Point(792, 40);
            this.txtScursalId.MaxLength = 3;
            this.txtScursalId.Name = "txtScursalId";
            this.txtScursalId.Size = new System.Drawing.Size(44, 20);
            this.txtScursalId.TabIndex = 511;
            this.txtScursalId.Visible = false;
            // 
            // tpConfiguracion
            // 
            this.tpConfiguracion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(245)))));
            this.tpConfiguracion.Controls.Add(this.label8);
            this.tpConfiguracion.Controls.Add(this.dgvParametos);
            this.tpConfiguracion.Controls.Add(this.label11);
            this.tpConfiguracion.Controls.Add(this.dgvSucursalesGramajes);
            this.tpConfiguracion.Controls.Add(this.label10);
            this.tpConfiguracion.Controls.Add(this.dgvSucursalesPlazo);
            this.tpConfiguracion.Controls.Add(this.label9);
            this.tpConfiguracion.Controls.Add(this.dgvSucursalesCobrosTipos);
            this.tpConfiguracion.Location = new System.Drawing.Point(4, 22);
            this.tpConfiguracion.Name = "tpConfiguracion";
            this.tpConfiguracion.Padding = new System.Windows.Forms.Padding(3);
            this.tpConfiguracion.Size = new System.Drawing.Size(1064, 262);
            this.tpConfiguracion.TabIndex = 1;
            this.tpConfiguracion.Text = "Configuración";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(720, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 144;
            this.label8.Text = "Parámetros";
            // 
            // dgvParametos
            // 
            this.dgvParametos.AllowUserToDeleteRows = false;
            this.dgvParametos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvParametos.Location = new System.Drawing.Point(720, 40);
            this.dgvParametos.Name = "dgvParametos";
            this.dgvParametos.Size = new System.Drawing.Size(328, 168);
            this.dgvParametos.TabIndex = 143;
            this.dgvParametos.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvParametos_CellValidating);
            this.dgvParametos.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvParametos_EditingControlShowing);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(456, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 13);
            this.label11.TabIndex = 142;
            this.label11.Text = "Precios tope";
            // 
            // dgvSucursalesGramajes
            // 
            this.dgvSucursalesGramajes.AllowUserToDeleteRows = false;
            this.dgvSucursalesGramajes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSucursalesGramajes.Location = new System.Drawing.Point(456, 40);
            this.dgvSucursalesGramajes.Name = "dgvSucursalesGramajes";
            this.dgvSucursalesGramajes.Size = new System.Drawing.Size(256, 168);
            this.dgvSucursalesGramajes.TabIndex = 141;
            this.dgvSucursalesGramajes.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvSucursalesGramajes_CellValidating);
            this.dgvSucursalesGramajes.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvSucursalesGramajes_EditingControlShowing);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(232, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 140;
            this.label10.Text = " Plazos";
            // 
            // dgvSucursalesPlazo
            // 
            this.dgvSucursalesPlazo.AllowUserToDeleteRows = false;
            this.dgvSucursalesPlazo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSucursalesPlazo.Location = new System.Drawing.Point(232, 40);
            this.dgvSucursalesPlazo.Name = "dgvSucursalesPlazo";
            this.dgvSucursalesPlazo.Size = new System.Drawing.Size(216, 168);
            this.dgvSucursalesPlazo.TabIndex = 139;
            this.dgvSucursalesPlazo.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvSucursalesPlazo_CellValidating);
            this.dgvSucursalesPlazo.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvSucursalesPlazo_EditingControlShowing);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(108, 13);
            this.label9.TabIndex = 138;
            this.label9.Text = "Porcentajes de cobro";
            // 
            // dgvSucursalesCobrosTipos
            // 
            this.dgvSucursalesCobrosTipos.AllowUserToDeleteRows = false;
            this.dgvSucursalesCobrosTipos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSucursalesCobrosTipos.Location = new System.Drawing.Point(8, 40);
            this.dgvSucursalesCobrosTipos.Name = "dgvSucursalesCobrosTipos";
            this.dgvSucursalesCobrosTipos.Size = new System.Drawing.Size(216, 168);
            this.dgvSucursalesCobrosTipos.TabIndex = 137;
            this.dgvSucursalesCobrosTipos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSucursalesCobrosTipos_CellContentClick);
            this.dgvSucursalesCobrosTipos.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvSucursalesCobrosTipos_CellValidating);
            this.dgvSucursalesCobrosTipos.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvSucursalesCobrosTipos_EditingControlShowing);
            // 
            // tpPromociones
            // 
            this.tpPromociones.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(245)))));
            this.tpPromociones.Controls.Add(this.dgvDetallesPromociones);
            this.tpPromociones.Controls.Add(this.btnAgregarPromociones);
            this.tpPromociones.Location = new System.Drawing.Point(4, 22);
            this.tpPromociones.Name = "tpPromociones";
            this.tpPromociones.Padding = new System.Windows.Forms.Padding(3);
            this.tpPromociones.Size = new System.Drawing.Size(1064, 262);
            this.tpPromociones.TabIndex = 2;
            this.tpPromociones.Text = "Promociones";
            this.tpPromociones.Click += new System.EventHandler(this.tpPromociones_Click);
            // 
            // dgvDetallesPromociones
            // 
            this.dgvDetallesPromociones.AllowUserToAddRows = false;
            this.dgvDetallesPromociones.AllowUserToDeleteRows = false;
            this.dgvDetallesPromociones.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDetallesPromociones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetallesPromociones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sucursal_promocion_id,
            this.dia,
            this.porcentaje_pro,
            this.TiposDePrendas,
            this.estatus,
            this.acciones});
            this.dgvDetallesPromociones.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgvDetallesPromociones.Location = new System.Drawing.Point(16, 48);
            this.dgvDetallesPromociones.Name = "dgvDetallesPromociones";
            this.dgvDetallesPromociones.Size = new System.Drawing.Size(1032, 150);
            this.dgvDetallesPromociones.TabIndex = 595;
            this.dgvDetallesPromociones.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDetallesPromociones_CellContentClick);
            this.dgvDetallesPromociones.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvDetallesPromociones_KeyDown);
            // 
            // sucursal_promocion_id
            // 
            this.sucursal_promocion_id.HeaderText = "sucursal_promocion_id";
            this.sucursal_promocion_id.Name = "sucursal_promocion_id";
            this.sucursal_promocion_id.Visible = false;
            // 
            // dia
            // 
            this.dia.FillWeight = 93.8651F;
            this.dia.HeaderText = "Día";
            this.dia.Name = "dia";
            // 
            // porcentaje_pro
            // 
            this.porcentaje_pro.FillWeight = 81.00505F;
            this.porcentaje_pro.HeaderText = "Porcentaje";
            this.porcentaje_pro.Name = "porcentaje_pro";
            // 
            // TiposDePrendas
            // 
            this.TiposDePrendas.FillWeight = 207.6766F;
            this.TiposDePrendas.HeaderText = "Tipos de prendas";
            this.TiposDePrendas.Name = "TiposDePrendas";
            // 
            // estatus
            // 
            this.estatus.FillWeight = 66.69189F;
            this.estatus.HeaderText = "Estatus";
            this.estatus.Name = "estatus";
            // 
            // acciones
            // 
            this.acciones.FillWeight = 50.76143F;
            this.acciones.HeaderText = "Accion";
            this.acciones.Name = "acciones";
            this.acciones.Text = "Modificar";
            this.acciones.ToolTipText = "Modificar";
            this.acciones.UseColumnTextForButtonValue = true;
            this.acciones.Visible = false;
            // 
            // btnAgregarPromociones
            // 
            this.btnAgregarPromociones.Location = new System.Drawing.Point(16, 16);
            this.btnAgregarPromociones.Name = "btnAgregarPromociones";
            this.btnAgregarPromociones.Size = new System.Drawing.Size(64, 21);
            this.btnAgregarPromociones.TabIndex = 594;
            this.btnAgregarPromociones.Text = "Agregar";
            this.btnAgregarPromociones.UseVisualStyleBackColor = true;
            this.btnAgregarPromociones.Click += new System.EventHandler(this.btnAgregarPromociones_Click);
            // 
            // frmSucursales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(1088, 417);
            this.Controls.Add(this.tbsControlSucursal);
            this.Controls.Add(this.tsAcciones);
            this.Controls.Add(this.btnFooterCierra);
            this.Controls.Add(this.btnFooterGuardar);
            this.Controls.Add(this.label4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSucursales";
            this.Text = "Sucursales ";
            this.Load += new System.EventHandler(this.FrmSucursales_Load);
            this.Shown += new System.EventHandler(this.frmSucursales_Shown);
            this.tsAcciones.ResumeLayout(false);
            this.tsAcciones.PerformLayout();
            this.tbsControlSucursal.ResumeLayout(false);
            this.tpInformacionGeneral.ResumeLayout(false);
            this.tpInformacionGeneral.PerformLayout();
            this.tpConfiguracion.ResumeLayout(false);
            this.tpConfiguracion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvParametos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSucursalesGramajes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSucursalesPlazo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSucursalesCobrosTipos)).EndInit();
            this.tpPromociones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetallesPromociones)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.DateTimePicker dtpInicioOperaciones;
        private System.Windows.Forms.TextBox txtNoExt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCodigoMunicipio;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtTelefono1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtRepresentanteLegal;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtCalle;
        private System.Windows.Forms.TextBox txtDescripcionMunicipio;
        private System.Windows.Forms.TextBox txtDescripcionEstado;
        private System.Windows.Forms.TextBox txtCodigoEstado;
        private System.Windows.Forms.TextBox txtTelefono2;
        private System.Windows.Forms.TextBox txtNoInt;
        private System.Windows.Forms.Button btnFooterCierra;
        private System.Windows.Forms.Button btnFooterGuardar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStrip tsAcciones;
        private System.Windows.Forms.ToolStripButton tsbNuevo;
        private System.Windows.Forms.ToolStripButton tsbGuardar;
        private System.Windows.Forms.ToolStripButton tsbEliminar;
        private System.Windows.Forms.ToolStripButton btnUndo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnPrimeroPag;
        private System.Windows.Forms.ToolStripButton btnAtras;
        private System.Windows.Forms.ToolStripButton btnAdelante;
        private System.Windows.Forms.ToolStripButton btnUlitmoPag;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton btnCierra;
        private System.Windows.Forms.ToolStripButton tsbBuscar;
        private System.Windows.Forms.Button btnBusqueda;
        private System.Windows.Forms.Button btnBusquedaEmpresas;
        private System.Windows.Forms.TextBox txtDescripcionEmpresas;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtCodigoPostal;
        private System.Windows.Forms.Button btnBusquedaAsentamiento;
        private System.Windows.Forms.TextBox txtDescripcionAsentamiento;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabControl tbsControlSucursal;
        private System.Windows.Forms.TabPage tpInformacionGeneral;
        private System.Windows.Forms.TabPage tpConfiguracion;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView dgvSucursalesGramajes;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dgvSucursalesPlazo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dgvSucursalesCobrosTipos;
        private System.Windows.Forms.TextBox txtAsentamientoId;
        private System.Windows.Forms.TextBox txtEmpresaId;
        private System.Windows.Forms.TextBox txtScursalId;
        private System.Windows.Forms.DataGridView dgvParametos;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabPage tpPromociones;
        private System.Windows.Forms.Button btnAgregarPromociones;
        private System.Windows.Forms.DataGridView dgvDetallesPromociones;
        private System.Windows.Forms.DataGridViewTextBoxColumn sucursal_promocion_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn dia;
        private System.Windows.Forms.DataGridViewTextBoxColumn porcentaje_pro;
        private System.Windows.Forms.DataGridViewTextBoxColumn TiposDePrendas;
        private System.Windows.Forms.DataGridViewTextBoxColumn estatus;
        private System.Windows.Forms.DataGridViewButtonColumn acciones;
        private System.Windows.Forms.Label lblDescEstatus;
        private System.Windows.Forms.Label lblEstatus;
        private System.Windows.Forms.ToolStripButton tsbReactivar;
    }
}