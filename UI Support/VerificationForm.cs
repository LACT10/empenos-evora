using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace UI_Support {
    /* public partial class VerificationForm : Form {
       public VerificationForm(AppData data) {
         InitializeComponent();
         Data = data;
       }

       public void OnComplete(object Control, DPFP.FeatureSet FeatureSet, ref DPFP.Gui.EventHandlerStatus Status) {
         DPFP.Verification.Verification ver = new DPFP.Verification.Verification();
         DPFP.Verification.Verification.Result res = new DPFP.Verification.Verification.Result();

         // Compare feature set with all stored templates.
         foreach (DPFP.Template template in Data.Templates) {
           // Get template from storage.
           if (template != null) {
             // Compare feature set with particular template.
             ver.Verify(FeatureSet, template, ref res);
             Data.IsFeatureSetMatched = res.Verified;
             Data.FalseAcceptRate = res.FARAchieved;
             if (res.Verified)
               break; // success
           }
         }

         if (!res.Verified)
           Status = DPFP.Gui.EventHandlerStatus.Failure;

         Data.Update();
       }

       private AppData Data;

           private void lblPrompt_Click(object sender, EventArgs e)
           {

           }

           private void CloseButton_Click(object sender, EventArgs e)
           {

           }

           private void VerificationForm_Load(object sender, EventArgs e)
           {

           }
       }*/


    public partial class VerificationForm : Form
    {
        public DataSet _objDataSet;
        public DataRow _objDataSetSingle;
        public bool _bolResultado;
        public DataSet objDataSet { get => _objDataSet; set => _objDataSet = value; }
        public DataRow objDataSetSingle { get => _objDataSetSingle; set => _objDataSetSingle = value; }
        public bool bolResultado { get => _bolResultado; set => _bolResultado = value; }
        public VerificationForm()
        {
            InitializeComponent();
            this.bolResultado = false;
        }

        public void OnComplete(object Control, DPFP.FeatureSet FeatureSet, ref DPFP.Gui.EventHandlerStatus Status)
        {
            DPFP.Verification.Verification ver = new DPFP.Verification.Verification();
            DPFP.Verification.Verification.Result res = new DPFP.Verification.Verification.Result();
            DPFP.Template template = new DPFP.Template();

            this.bolResultado = false;
            // Compare feature set with all stored templates.
            foreach (DataRow row in this.objDataSet.Tables[0].Rows)
            {
                byte[] huella = (byte[])row["huella"];
                //Obtener secuencia de bytes de la huella del registro
                MemoryStream memoria = new MemoryStream(huella);
                //Variable que se utiliza para asignar resultado de la verificación de huella digital
                template = new DPFP.Template(memoria);
                //Deserializar plantilla para comparar caracteristicas de la huella 
                template.DeSerialize(memoria.ToArray());
                //Comparar el conjunto de características con nuestra plantilla
                ver.Verify(FeatureSet, template, ref res);

                if (res.Verified)
                {
                    this.bolResultado = true;
                    this.objDataSetSingle = row;
                    break;
                }
            }


            if (this.bolResultado)
            {                
                
                this.Close();
            }
            else 
            {
                Status = DPFP.Gui.EventHandlerStatus.Failure;
                MessageBox.Show("No se encontró ningún registro", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            
        }

        

        private void lblPrompt_Click(object sender, EventArgs e)
        {

        }

        private void CloseButton_Click(object sender, EventArgs e)
        {

        }

        private void VerificationForm_Load(object sender, EventArgs e)
        {

        }
    }
}