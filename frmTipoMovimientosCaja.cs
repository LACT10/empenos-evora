﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmTipoMovimientosCaja : Form
    {
        
        public Controller.clsCtrlMovimientosCajaTipos ctrlMovimientosCajaTipos;
        public Core.clsCoreView clsView;

        public int _intUlitmoCodigo;
        
        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }

        

        public List<Properties_Class.clsMovimientosCajaTipos> lsDeshacerMovimientosCajaTipos;        
        public frmTipoMovimientosCaja()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();
            this.ctrlMovimientosCajaTipos = new Controller.clsCtrlMovimientosCajaTipos();
            this.ctrlMovimientosCajaTipos.Paginacion();
            this.lsDeshacerMovimientosCajaTipos = new List<Properties_Class.clsMovimientosCajaTipos>();
            this.lsDeshacerMovimientosCajaTipos.Add(this.ctrlMovimientosCajaTipos.movimientosCajaTipos);
        }
        private void FrmTipoMovimientosCaja_Load(object sender, EventArgs e)
        {
            cmbTipo.SelectedIndex = 0;

            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);
        }


        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void TsbEliminar_Click(object sender, EventArgs e)
        {

            if (this.ctrlMovimientosCajaTipos.movimientosCajaTipos.intId == 0) return;
            if (this.clsView.confirmacionEliminar("Tipo de movimeinto caja"))
            {
                this.ctrlMovimientosCajaTipos.Eliminar();
                if (this.ctrlMovimientosCajaTipos.bolResultado)
                {
                    this.ctrlMovimientosCajaTipos.Paginacion();
                    this.limpiar_textbox();
                    this.clsView.mensajeExitoDB("tipo de movimeinto caja", "elimino");
                }
            }
        }


        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlMovimientosCajaTipos.bolResutladoPaginacion)
            {
                
                txtDescripcion.Text = this.lsDeshacerMovimientosCajaTipos[0].strDescripcion.ToString();
                cmbTipo.SelectedItem = this.lsDeshacerMovimientosCajaTipos[0].strTipo.ToString();
            }
        }


        private void tsbBuscar_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();

        }

        private void BtnBusqueda_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();
        }

       
        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlMovimientosCajaTipos.PrimeroPaginacion();            
            if (!this.ctrlMovimientosCajaTipos.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlMovimientosCajaTipos.SiguentePaginacion();            
            if (!this.ctrlMovimientosCajaTipos.bolResultado) return;
            this.llenar_formulario();

        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlMovimientosCajaTipos.AtrasPaginacion();            
            if (!this.ctrlMovimientosCajaTipos.bolResultado) return;
            this.llenar_formulario();

        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlMovimientosCajaTipos.UltimoPaginacion();            
            if (!this.ctrlMovimientosCajaTipos.bolResultado) return;
            this.llenar_formulario();

        }
        private void TsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();
            this.ctrlMovimientosCajaTipos.movimientosCajaTipos.limpiarProps();            
            //txtCodigo.Enabled = false;
            if (this.ctrlMovimientosCajaTipos.bolResutladoPaginacion)
            {
                /*DataTable dtDatos = this.ctrlMovimientosCajaTipos.objDataSet.Tables[0];
                this.intUlitmoCodigo = Int32.Parse(dtDatos.Rows[dtDatos.Rows.Count - 1][1].ToString()) + 1;
                this.agregarDeshacer();*/
            }
            else
            {
                this.intUlitmoCodigo = 1;
            }

            //txtCodigo.Text = this.intUlitmoCodigo.ToString();

        }

        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {
        }

        private void TxtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);

        }


        /*private void TxtCodigo_Leave(object sender, EventArgs e)
        {
            this.ctrlMovimientosCajaTipos.Buscar(txtCodigo.Text);
            this.llenar_formulario();
        }*/

        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            //this.clsView.soloTextoVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            cmbTipo.SelectedIndex = 0;
        }

        private bool validacion()
        {

            bool bolValidacion = true;
            //string strCodigo = txtCodigo.Text.TrimStart();
            string strDescripcion = txtDescripcion.Text.TrimStart();

            /*if (strCodigo == "")
            {
                txtCodigo.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigo, this.clsView.mensajeErrorValiForm("código", "un"));
                bolValidacion = false;
            }*/

            if (strDescripcion == "")
            {
                txtDescripcion.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("El campo descripción es obligatorio, favor de ingresar una descripción "));
                bolValidacion = false;
            }
            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();

            if (validacion())
            {
                
                this.ctrlMovimientosCajaTipos.movimientosCajaTipos.strDescripcion = txtDescripcion.Text.ToString();
                this.ctrlMovimientosCajaTipos.movimientosCajaTipos.strTipo = cmbTipo.SelectedItem.ToString();
                this.ctrlMovimientosCajaTipos.movimientosCajaTipos.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlMovimientosCajaTipos.Guardar();

                if (this.ctrlMovimientosCajaTipos.mdlMovimientosCajaTipos.bolResultado)
                {
                    this.limpiar_textbox();
                    this.ctrlMovimientosCajaTipos.Paginacion();
                    this.clsView.mensajeExitoDB("tipo de movimeinto caja", "guardó");

                }
            }
        }

        public void mostraBusqueda()
        {
            if (this.ctrlMovimientosCajaTipos.bolResutladoPaginacion)
            {
                this.ctrlMovimientosCajaTipos.CargarFrmBusqueda();
                if (this.ctrlMovimientosCajaTipos.bolResultadoBusqueda) 
                {
                    this.llenar_formulario();
                }
                
                
            }

            cmbTipo.Focus();
        }
        public void llenar_formulario()
        {
            if (this.ctrlMovimientosCajaTipos.movimientosCajaTipos.strDescripcion.Length == 0) return;
            txtDescripcion.Text = this.ctrlMovimientosCajaTipos.movimientosCajaTipos.strDescripcion;
            cmbTipo.SelectedItem = this.ctrlMovimientosCajaTipos.movimientosCajaTipos.strTipo;
            txtDescripcionAnt.Text = this.ctrlMovimientosCajaTipos.movimientosCajaTipos.strDescripcion;
            txtTipoAnt.Text = this.ctrlMovimientosCajaTipos.movimientosCajaTipos.strTipo; 
        }


        public void agregarDeshacer()
        {
            this.lsDeshacerMovimientosCajaTipos.RemoveAt(0);
            this.lsDeshacerMovimientosCajaTipos.Add(this.ctrlMovimientosCajaTipos.movimientosCajaTipos);
        }

        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void txtDescripcion_Leave(object sender, EventArgs e)
        {
            this.validarDuplicar();
        }

        public void validarDuplicar() 
        {
            if(txtDescripcion.Text.Trim() != txtDescripcionAnt.Text)
            { 
                       
                if (txtDescripcion.Text.Trim() != "" && cmbTipo.SelectedIndex > 0) 
                {
                    this.ctrlMovimientosCajaTipos.BuscarDuplicacion(cmbTipo.SelectedItem.ToString(), txtDescripcion.Text);
                    if (this.ctrlMovimientosCajaTipos.bolResultado) 
                    {
                        this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("El campo descripción debe ser única, favor de verificar"));
                        txtDescripcion.Text = "";
                    }
                }
            }
        }

        private void cmbTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //this.validarDuplicar();
            if (cmbTipo.SelectedIndex > 0) 
            {
                txtDescripcionAnt.Text = "";
            }
            

        }
    }
}