﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using UI_Support;

namespace EmpenosEvora
{
    public partial class frmUsuarios : Form
    {
        public Controller.clsCtrlUsuarios ctrlUsuario;
        public Controller.clsCtrlCajasUsuario ctrlCajasUsuario;
        public Controller.clsCtrlProcesos ctrlProcesos;
        public Controller.clsCtrlSubProcesos ctrlSubProcesos;
        public Core.clsCoreView clsView;

        public int _intUlitmoCodigo;
        public string _strPermisosUsuario;

        private DPFP.Template template;
        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }

        public string strPermisosUsuario { get => _strPermisosUsuario; set => _strPermisosUsuario = value; }

        public List<Properties_Class.clsUsuario> lsDeshacerUsuario;

        public List<string> lsTreeNodesCheck;

        public frmUsuarios()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();

            this.ctrlUsuario = new Controller.clsCtrlUsuarios();
            this.ctrlUsuario.Paginacion();

            this.lsDeshacerUsuario = new List<Properties_Class.clsUsuario>();
            this.lsDeshacerUsuario.Add(this.ctrlUsuario.usuario);

            this.lsTreeNodesCheck = new List<string>();

            this.ctrlCajasUsuario = new Controller.clsCtrlCajasUsuario();

            this.ctrlProcesos = new Controller.clsCtrlProcesos();

            this.ctrlSubProcesos = new Controller.clsCtrlSubProcesos();
            this.ctrlSubProcesos.Paginacion();

            //this.usuarioCajas(0, 0);
            //this.usuarioPermisos(0, 0);


        }


        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlUsuario.usuario.intId == 0) return;
            if (this.clsView.confirmacionEliminar("Usuarios"))
            {
                this.ctrlUsuario.Eliminar();
                if (this.ctrlUsuario.bolResultado)
                {
                    this.ctrlUsuario.Paginacion();
                    this.limpiar_textbox();
                    this.clsView.mensajeExitoDB("usuario", "elimino");
                }
            }
        }


        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlUsuario.bolResutladoPaginacion)
            {
                /*txtCodigo.Text = this.lsDeshacerUsuario[0].strCodigo.ToString();
                txtDescripcion.Text = this.lsDeshacerUsuario[0].strDescripcion.ToString();*/
                txtUsuarioId.Text = this.lsDeshacerUsuario[0].intId.ToString();
                txtNombre.Text = this.lsDeshacerUsuario[0].strNombre;
                txtApellidoPaterno.Text = this.lsDeshacerUsuario[0].strApellidoPaterno;
                txtApellidoMaterno.Text = this.lsDeshacerUsuario[0].strApellidoMaterno;
                txtTelefono01.Text = this.lsDeshacerUsuario[0].strTelefono01;
                txtTelefono02.Text = this.lsDeshacerUsuario[0].strTelefono02;

                txtCorreoElectronico.Text = this.lsDeshacerUsuario[0].strCorreoElectronico.ToString();
                txtUsuario.Text = this.lsDeshacerUsuario[0].strUsuario.ToString();
                txtUsuarioAntri.Text = this.lsDeshacerUsuario[0].strUsuario.ToString();
                txtContrasena.Text = this.lsDeshacerUsuario[0].strContrasena.ToString();
                cmbAutorizar.Text = this.lsDeshacerUsuario[0].strAutorizar.ToString();
                cmbModificarMovimientos.Text = this.lsDeshacerUsuario[0].strModificarMovimientos.ToString();



            }
        }


        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlUsuario.PrimeroPaginacion();
            if (!this.ctrlUsuario.bolResultado) return;

            this.limpiarSucursal();
            this.llenar_formulario();
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlUsuario.SiguentePaginacion();
            if (!this.ctrlUsuario.bolResultado) return;
            this.limpiarSucursal();
            this.llenar_formulario();


        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlUsuario.AtrasPaginacion();
            if (!this.ctrlUsuario.bolResultado) return;
            this.limpiarSucursal();
            this.llenar_formulario();

        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlUsuario.UltimoPaginacion();
            if (!this.ctrlUsuario.bolResultado) return;
            this.limpiarSucursal();
            this.llenar_formulario();


        }
 

        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void txtTelefono01_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtTelefono02_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void txtApellidoPaterno_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void txtApellidoMaterno_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void txtCorreoElectronico_Leave(object sender, EventArgs e)
        {
            if (!(txtCorreoElectronico.Text == "") && !(this.clsView.validarCorreoElectronico(txtCorreoElectronico.Text)))
            {
                txtCorreoElectronico.Focus();
                this.clsView.principal.epMensaje.SetError(txtCorreoElectronico, this.clsView.mensajeErrorValiForm2("El campo correo electrónico no es válido, favor de ingresar un correo electrónico "));
            }
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            cmbAutorizar.SelectedIndex = 0;
            cmbModificarMovimientos.SelectedIndex = 0;
            this.lsTreeNodesCheck.Clear();
            txtSucursalId.Text = "";
            txtDescripcionSucursal.Text = "";
            txtCodigoSucursal.Text = "";


        }

        private bool validacion()
        {

            bool bolValidacion = true;
            string strNombre = txtNombre.Text.TrimStart();
            string strApellidoPaterno = txtApellidoPaterno.Text.TrimStart();
            string strApellidoMaterno = txtApellidoMaterno.Text.TrimStart();
            string strTelefono01 = txtTelefono01.Text.TrimStart();
            string strTelefono02 = txtTelefono02.Text.TrimStart();
            string strCorreoElectronico = txtCorreoElectronico.Text.TrimStart();
            string strUsuario = txtUsuario.Text.TrimStart();
            string strContrasena = txtContrasena.Text.TrimStart();
            int intAutorizar = cmbAutorizar.SelectedIndex;
            int intModificarMovimientos = cmbModificarMovimientos.SelectedIndex;


            if (strNombre == "")
            {
                txtNombre.Focus();
                this.clsView.principal.epMensaje.SetError(txtNombre, this.clsView.mensajeErrorValiForm2("El campo nombre es obligatorio, favor de ingresar un nombre"));
                bolValidacion = false;
            }

            if (strApellidoMaterno == "" && strApellidoPaterno == "")
            {
                txtApellidoPaterno.Focus();
                this.clsView.principal.epMensaje.SetError(txtApellidoPaterno, this.clsView.mensajeErrorValiForm2("El campo apellido  es obligatorio, favor de ingresar un apellido "));
                bolValidacion = false;
            }

            if (strApellidoMaterno == "" && strApellidoPaterno == "")
            {
                txtApellidoMaterno.Focus();
                this.clsView.principal.epMensaje.SetError(txtApellidoMaterno, this.clsView.mensajeErrorValiForm2("El campo apellido  es obligatorio, favor de ingresar un apellido "));
                bolValidacion = false;
            }

            if (strTelefono01 == "")
            {
                txtTelefono01.Focus();
                this.clsView.principal.epMensaje.SetError(txtTelefono01, this.clsView.mensajeErrorValiForm2("El campo telefono es obligatorio, favor de ingresar un telefono"));
                bolValidacion = false;
            }

            /*if (strCorreoElectronico == "")
            {
                txtTelefono01.Focus();
                this.clsView.principal.epMensaje.SetError(txtCorreoElectronico, this.clsView.mensajeErrorValiForm2("El campo correo electrónico es obligatorio, favor de ingresar un correo electrónico"));

                bolValidacion = false;
            }*/
            if (strUsuario == "")
            {
                txtUsuario.Focus();
                this.clsView.principal.epMensaje.SetError(txtUsuario, this.clsView.mensajeErrorValiForm2("El campo usuario es obligatorio, favor de ingresar un usuario"));
                bolValidacion = false;
            }

            if (strContrasena == "")
            {
                txtContrasena.Focus();
                this.clsView.principal.epMensaje.SetError(txtContrasena, this.clsView.mensajeErrorValiForm2("El campo contraseña es obligatorio, favor de ingresar una contraseña"));
                bolValidacion = false;
            }

            if (intAutorizar == 0)
            {
                cmbAutorizar.Focus();
                this.clsView.principal.epMensaje.SetError(cmbAutorizar, this.clsView.mensajeErrorValiForm2("El campo autorizar empeños es obligatorio, favor de selecionar una opción "));
                bolValidacion = false;
            }

            if (intModificarMovimientos == 0)
            {
                cmbAutorizar.Focus();
                this.clsView.principal.epMensaje.SetError(cmbModificarMovimientos, this.clsView.mensajeErrorValiForm2("El campo modificar movimientos  es obligatorio, favor de selecionar una opción "));
                bolValidacion = false;
            }

            

            if (this.ctrlUsuario.usuario.intId == 0 && this.template == null)
            {
                this.clsView.principal.epMensaje.SetError(txtNombre, this.clsView.mensajeErrorValiForm2("La huella es obligatoria, favor de capturarla "));
                bolValidacion = false;
            }

            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();

            if (validacion())
            {


                this.ctrlUsuario.usuario.strNombre = txtNombre.Text.ToString();
                this.ctrlUsuario.usuario.strApellidoPaterno = txtApellidoPaterno.Text.ToString();
                this.ctrlUsuario.usuario.strApellidoMaterno = txtApellidoMaterno.Text.ToString();
                this.ctrlUsuario.usuario.strTelefono01 = txtTelefono01.Text.ToString();
                this.ctrlUsuario.usuario.strTelefono02 = txtTelefono02.Text.ToString();
                this.ctrlUsuario.usuario.strUsuario = txtUsuario.Text.ToString();
                this.ctrlUsuario.usuario.strCorreoElectronico = txtCorreoElectronico.Text.ToString();
                this.ctrlUsuario.usuario.strContrasena = txtContrasena.Text.ToString();
                this.ctrlUsuario.usuario.strAutorizar = cmbAutorizar.SelectedItem.ToString();
                this.ctrlUsuario.usuario.strModificarMovimientos = cmbModificarMovimientos.SelectedItem.ToString();
                this.ctrlUsuario.usuario.intUsuario = this.clsView.login.usuario.intId;

                if (!(this.template == null))
                {

                    this.ctrlUsuario.usuario.msHuella = new MemoryStream(this.template.Bytes);
                    this.ctrlUsuario.usuario.bolHuella = true;                                        
                }


                if (txtSucursalId.Text.ToString() != "") 
                {
                    this.guardarCajas();
                    this.guardarPermisos();
                }
               
                

                this.ctrlUsuario.Guardar();

                if (this.ctrlUsuario.mdlUsuario.bolResultado)
                {
                    this.limpiar_textbox();
                    this.ctrlUsuario.Paginacion();
                    tvPermisos.Nodes.Clear();
                    tvCajas.Nodes.Clear();

                    this.clsView.mensajeExitoDB("usuario", "guardó");

                }
            }
        }

        public void llenar_formulario()
        {

            if (this.ctrlUsuario.usuario.intId == 0) return;

            txtUsuarioId.Text = this.ctrlUsuario.usuario.intId.ToString();
            txtNombre.Text = this.ctrlUsuario.usuario.strNombre.ToString();
            txtApellidoPaterno.Text = this.ctrlUsuario.usuario.strApellidoPaterno.ToString();
            txtApellidoMaterno.Text = this.ctrlUsuario.usuario.strApellidoMaterno.ToString();
            txtTelefono01.Text = this.ctrlUsuario.usuario.strTelefono01.ToString();
            txtTelefono02.Text = this.ctrlUsuario.usuario.strTelefono02.ToString();
            txtCorreoElectronico.Text = this.ctrlUsuario.usuario.strCorreoElectronico.ToString();
            txtUsuario.Text = this.ctrlUsuario.usuario.strUsuario.ToString();
            txtUsuarioAntri.Text = this.ctrlUsuario.usuario.strUsuario.ToString();
            txtContrasena.Text = this.ctrlUsuario.usuario.strContrasena.ToString();
            cmbAutorizar.Text = this.ctrlUsuario.usuario.strAutorizar.ToString();
            cmbModificarMovimientos.Text = this.ctrlUsuario.usuario.strModificarMovimientos.ToString();

        }


        public void agregarDeshacer()
        {
            this.lsDeshacerUsuario.RemoveAt(0);
            this.lsDeshacerUsuario.Add(this.ctrlUsuario.usuario);
        }

        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void TsbBuscar_Click(object sender, EventArgs e)
        {
            
            
            cmbAutorizar.Text = this.ctrlUsuario.usuario.strAutorizar.ToString();
            cmbModificarMovimientos.Text = this.ctrlUsuario.usuario.strModificarMovimientos.ToString();
            Dictionary<int, Properties_Class.clsTextbox> dicFocus = new Dictionary<int, Properties_Class.clsTextbox>()
            {
                { 0, new Properties_Class.clsTextbox()
                                { txtGenerico = txtNombre, strBusqueda = "usuario"}
                },
                { 1, new Properties_Class.clsTextbox()
                                { txtGenerico = txtApellidoPaterno, strBusqueda = "usuario"}
                },
                { 2, new Properties_Class.clsTextbox()
                                { txtGenerico = txtApellidoMaterno, strBusqueda = "usuario"}
                },
                { 3, new Properties_Class.clsTextbox()
                                { txtGenerico = txtTelefono01, strBusqueda = "usuario"}
                },
                { 4, new Properties_Class.clsTextbox()
                                { txtGenerico = txtTelefono02, strBusqueda = "usuario"}
                },
                { 5, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCorreoElectronico, strBusqueda = "usuario"}
                },
                { 6, new Properties_Class.clsTextbox()
                                { txtGenerico = txtUsuario, strBusqueda = "usuario"}
                },
                { 7, new Properties_Class.clsTextbox()
                                { txtGenerico = txtContrasena, strBusqueda = "usuario"}
                },
                { 8, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoSucursal, strBusqueda = "sucursal"}
                },
                { 9, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcionSucursal, strBusqueda = "sucursal"}
                }

            };
            foreach (KeyValuePair<int, Properties_Class.clsTextbox> data in dicFocus)
            {
                if (data.Value.txtGenerico.Focused)
                {
                    if (data.Value.strBusqueda == "usuario")
                    {
                        this.mostraBusqueda();
                    }
                    else if (data.Value.strBusqueda == "sucursal")
                    {
                        this.mostraSucrsal();
                    }
                }

            }

            if (cmbAutorizar.Focused || cmbModificarMovimientos.Focused)
            {
                this.mostraBusqueda();
            }
        }

        private void frmUsuarios_Load(object sender, EventArgs e)
        {
            cmbAutorizar.SelectedIndex = 0;
            cmbModificarMovimientos.SelectedIndex = 0;

            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);

        }

        private void tsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();
            this.limpiarSucursal();
            
            if (this.ctrlUsuario.bolResutladoPaginacion)
            {

                this.agregarDeshacer();
            }
        }

        private void btnHuella_Click(object sender, EventArgs e)
        {

            frmHuella frmHuella = new frmHuella();
            frmHuella.ShowDialog();
            this.template = frmHuella.template;

        }

        private void btnVerificarHuella_Click(object sender, EventArgs e)
        {
            VerificationForm frmVerificarHuella = new VerificationForm();
            frmVerificarHuella.objDataSet = this.ctrlUsuario.objDataSet;

            frmVerificarHuella.ShowDialog();

            if (frmVerificarHuella.bolResultado)
            {
                this.ctrlUsuario.BuscarHuella(frmVerificarHuella.objDataSetSingle);
                this.llenar_formulario();
                this.limpiarSucursal();
            }
        }

        public void mostraCajas()
        {

        }

        private void tvPermisos_AfterSelect(object sender, TreeViewEventArgs e)
        {


            //MessageBox.Show(tvPermisos.SelectedNode.Text);
        }

        private void tvPermisos_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {

        }

        private void tvPermisos_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Parent != null)
            {
                if (e.Node.Parent.Checked)
                {
                    if (!(e.Node.Checked))
                    {
                        bool bolHermanos = true;
                        foreach (TreeNode row in e.Node.Parent.Nodes)
                        {

                            if (row.Checked)
                            {
                                bolHermanos = false;
                            }


                        }

                        if (bolHermanos)
                        {
                            e.Node.Parent.Checked = false;
                        }
                    }
                }

            }

            bool bolCondicion = false;

            if (e.Node.Checked)
            {
                bolCondicion = true;
                this.hijosProcesosCheked(e.Node, bolCondicion);
            }
            else
            {
                this.hijosProcesosCheked(e.Node, bolCondicion);

            }


            if (e.Node.Nodes.Count == 0)
            {

                this.lsTreeNodesCheck = this.lsTreeNodesCheck.Distinct().ToList();
                if (bolCondicion)
                {


                    this.lsTreeNodesCheck.Add(e.Node.Tag.ToString());

                }
                else
                {
                    this.lsTreeNodesCheck.Remove(e.Node.Tag.ToString());

                }
            }



        }


        public void hijosProcesosCheked(TreeNode node, bool condicion)
        {
            int j = 0;
            if (node.Nodes.Count > 0)
            {
                foreach (TreeNode row in node.Nodes)
                {

                    row.Checked = condicion;


                    this.hijosProcesosCheked(node.Nodes[j], condicion);
                    j++;

                }
            }



        }


        public void usuarioCajas(int intUsuarioId, int intSucursalId)
        {

            this.ctrlCajasUsuario = new Controller.clsCtrlCajasUsuario();
            //ctrlCajasUsuario.Paginacion(intUsuarioId, intSucursalId);
            ctrlCajasUsuario.PaginacionSucursalUsario(intUsuarioId, intSucursalId);
            tvCajas.Nodes.Clear();
            if (ctrlCajasUsuario.bolResultado)
            {
                foreach (DataRow row in ctrlCajasUsuario.objDataSet.Tables[0].Rows)
                {
                    //Agregar el valor en zero en el campo porcentaje
                    TreeNode nod = new TreeNode();
                    nod.Name = row["descripcion"].ToString();
                    nod.Text = row["descripcion"].ToString();
                    nod.Tag = row["caja_id"].ToString();
                    if (row["usuario_id"].ToString() != "")
                    {
                        nod.Checked = true;
                    }
                    tvCajas.Nodes.Add(nod);


                }
            }
        }

        public void usuarioPermisos(int intUsuarioId, int intSucursalId)
        {
            this.ctrlProcesos = new Controller.clsCtrlProcesos();
            this.ctrlProcesos.Proceosos();
            tvPermisos.Nodes.Clear();
            this.lsTreeNodesCheck.Clear();
            if (this.ctrlProcesos.bolResultado)
            {
                Dictionary<int, Properties_Class.clsProcesos> dicPadre = new Dictionary<int, Properties_Class.clsProcesos>();
                Dictionary<int, Properties_Class.clsProcesos> dicHijos = new Dictionary<int, Properties_Class.clsProcesos>();

                int i = 0;
                int j = 0;

                foreach (DataRow row in this.ctrlProcesos.objDataSet.Tables[0].Rows)
                {

                    if (row["proceso_padre_id"].ToString() == "0")
                    {
                        dicPadre.Add(i, new Properties_Class.clsProcesos()
                        {
                            strDescripcion = row["descripcion"].ToString(),
                            intId = int.Parse(row["proceso_id"].ToString()),
                            intProcesoPadreId = int.Parse(row["proceso_padre_id"].ToString())
                        }
                                    );
                        i++;
                    }
                    else
                    {
                        if (row["proceso_id"].ToString() == "61") 
                        {
                            string luis = "0";
                        }
                        dicHijos.Add(j, new Properties_Class.clsProcesos()
                        {
                            strDescripcion = row["descripcion"].ToString(),
                            intId = int.Parse(row["proceso_id"].ToString()),
                            intProcesoPadreId = int.Parse(row["proceso_padre_id"].ToString())
                        }
                                    );
                        j++;
                    }

                }
                string strPermisosUsuario = "";
                if (intSucursalId > 0 && this.ctrlUsuario.usuario.intId > 0)
                {
                    Controller.clsCtrlProcesos clsProcesos = new Controller.clsCtrlProcesos();
                    clsProcesos.ProceososUsuario(intUsuarioId, intSucursalId);

                    if (clsProcesos.bolResultado)
                    {
                        strPermisosUsuario = clsProcesos.objDataSet.Tables[0].Rows[0][2].ToString();
                    }


                }
                this.padreProcesos(dicPadre, dicHijos, strPermisosUsuario);


            }

        }

        public void padreProcesos(Dictionary<int, Properties_Class.clsProcesos> dicPadre, Dictionary<int, Properties_Class.clsProcesos> dicHijos, string strPermisosUsuario)
        {

            int i = 0;

            foreach (KeyValuePair<int, Properties_Class.clsProcesos> row in dicPadre)
            {

                TreeNode node = new TreeNode()
                {
                    Text = row.Value.strDescripcion.ToString(),
                    Tag = row.Value.intId.ToString()
                };
                tvPermisos.Nodes.Add(node);
                this.hijosProcesos(dicHijos, row.Value.intId.ToString(), tvPermisos.Nodes[i], strPermisosUsuario);
                i++;
            }

        }

        public void hijosProcesos(Dictionary<int, Properties_Class.clsProcesos> dicHijos, string strProcesoId, TreeNode node, string strPermisosUsuario)
        {
            int j = 0;
            bool bolSinHijo = true;
            foreach (KeyValuePair<int, Properties_Class.clsProcesos> row in dicHijos)
            {
                if (row.Value.intProcesoPadreId.ToString() == "33") 
                {
                    string lusi = "";
                }

                if (row.Value.intProcesoPadreId.ToString() == strProcesoId)
                {
                    bolSinHijo = false;
                    TreeNode nuevoNode = new TreeNode()
                    {
                        Text = row.Value.strDescripcion.ToString(),
                        Tag = row.Value.intId.ToString()
                    };

                    node.Nodes.Add(nuevoNode);


                    this.hijosProcesos(dicHijos, row.Value.intId.ToString(), node.Nodes[j], strPermisosUsuario);

                    j++;
                }


            }

            if (bolSinHijo)
            {
                //Contador para controlar los nodes 
                int e = 0;
                //bool bolUnaVezNodo = true;
                foreach (DataRow row in this.ctrlSubProcesos.objDataSet.Tables[0].Rows)
                {

                    if (row["proceso_id"].ToString() == strProcesoId)
                    {
                        //Agregar el valor en zero en el campo porcentaje
                        TreeNode nodeSubprocesos = new TreeNode()
                        {
                            Name = row["descripcion"].ToString(),
                            Text = row["descripcion"].ToString(),
                            Tag = row["subproceso_id"].ToString()
                        };

                        node.Nodes.Add(nodeSubprocesos);

                        if (strPermisosUsuario != "")
                        {
                            //Si el tag de node (usuario_id) esta en el string de strPermisosUsuario entonces se poner en checked el node
                            if (strPermisosUsuario.IndexOf(" "+node.Nodes[e].Tag.ToString()) >= 0)
                            {

                                node.Nodes[e].Checked = true;
                                /*if (bolUnaVezNodo) 
                                {

                                    this.checkPardesNodes(node.Nodes[e]);
                                    bolUnaVezNodo = false;
                                }*/
                                
                            }
                        }

                        e++;

                    }

                }
            }

        }


        /*public void checkPardesNodes(TreeNode node) 
        {
            if (node.Parent == null) return;

            node.Parent.Checked = true;
            this.checkPardesNodes(node.Parent);
        }*/


        private void btnBusquedaSucursal_Click(object sender, EventArgs e)
        {
            this.mostraSucrsal();
        }


        public void mostraSucrsal() 
        {

            Controller.clsCtrlSucursales ctrlSucursales = new Controller.clsCtrlSucursales();
            ctrlSucursales.Paginacion();
            ctrlSucursales.CargarFrmBusqueda();


            if (ctrlSucursales.bolResultadoBusqueda)
            {
                txtSucursalId.Text = ctrlSucursales.sucursal.intId.ToString();
                txtCodigoSucursal.Text = ctrlSucursales.sucursal.strCodigo;
                txtDescripcionSucursal.Text = ctrlSucursales.sucursal.strNombre;
            

                if (this.ctrlUsuario.usuario.intId > 0)
                {

                    this.usuarioCajas(this.ctrlUsuario.usuario.intId, ctrlSucursales.sucursal.intId);
                    this.usuarioPermisos(this.ctrlUsuario.usuario.intId, ctrlSucursales.sucursal.intId);
                }
                else
                {
                    this.usuarioCajas(0, 0);
                    this.usuarioPermisos(0, 0);
                }
            }

            //this.usuarioCajas(int.Parse(txtUsuarioId.Text.ToString()), int.Parse(txtSucursalId.Text.ToString()));
        }
        private void btnBusqueda_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();
            this.limpiarSucursal();
        }

        public void mostraBusqueda() 
        {
            if (this.ctrlUsuario.bolResutladoPaginacion)
            {
                this.ctrlUsuario.CargarFrmBusqueda();

                if (this.ctrlUsuario.bolResultadoBusqueda)
                {
                    this.llenar_formulario();
                    txtNombre.Focus();
                }

                
            }
            
        }

        private void txtCodigoSucursal_Leave(object sender, EventArgs e)
        {
            Controller.clsCtrlSucursales ctrlSucursales = new Controller.clsCtrlSucursales();
            ctrlSucursales.Buscar(txtCodigoSucursal.Text);
            txtSucursalId.Text = ctrlSucursales.sucursal.intId.ToString();
            txtDescripcionSucursal.Text = ctrlSucursales.sucursal.strNombre.ToString();
            txtCodigoSucursal.Text = ctrlSucursales.sucursal.strCodigo.ToString();
            
            if (this.ctrlUsuario.usuario.intId > 0)
            {
                this.usuarioCajas(this.ctrlUsuario.usuario.intId, ctrlSucursales.sucursal.intId);
                this.usuarioPermisos(this.ctrlUsuario.usuario.intId, ctrlSucursales.sucursal.intId);
            }
            else 
            {
                if (ctrlSucursales.sucursal.intId > 0)
                {
                    this.usuarioCajas(0, 0);
                    this.usuarioPermisos(0, 0);
                }
                else 
                {
                    tvCajas.Nodes.Clear();
                    tvPermisos.Nodes.Clear();
                }
                
            }

                
        }

        public void guardarPermisos() 
        {
            string intUsuarioId = "";
            string intSucuarsalId = txtSucursalId.Text;
            if (this.ctrlUsuario.usuario.intId == 0)
            {
                intUsuarioId = "@id";
                
            }
            else
            {
                intUsuarioId = this.ctrlUsuario.usuario.intId.ToString();
                this.ctrlUsuario.usuario.strQueryPermisosUsuarioDelete = "DELETE FROM usuarios_permisos " +
                    " WHERE sucursal_id = " + txtSucursalId.Text.ToString() + " " +
                    " AND  usuario_id = " + intUsuarioId + " ; ";
            }
            int countPermisosUsuario = this.lsTreeNodesCheck.Count();


            this.ctrlUsuario.usuario.strQueryPermisosUsuario = "INSERT INTO usuarios_permisos(usuario_id,sucursal_id,permisos)VALUES ( {0}, {1}, {2} ) ";
            string strPermisos = "'";

            
            int i = 0;
            bool bolNiguanDato = false;
            this.lsTreeNodesCheck = this.lsTreeNodesCheck.Distinct().ToList();

            this.lsTreeNodesCheck.ForEach(delegate (String strId)
           {
               if (i == countPermisosUsuario) return;

               if (i < countPermisosUsuario )
               {
                   strPermisos += strId + " | ";
               }

               bolNiguanDato = true;

               i++;
           });
            /*foreach (TreeNode tNode in tvPermisos.Nodes)
            {
                if (i == countCajasUsuario) break;

                if (tNode.Checked)
                {

                    if (tNode.Nodes.Count == 0)
                    {
                        if (i == countCajasUsuario - 1)
                        {
                            strPermisos += tNode.Tag + " ; ";

                            break;
                        }
                        else
                        {
                            strPermisos += tNode.Tag + " | ";
                        }
                        bolNiguanDato = true;
                    }
                    
                }
                i++;
            }*/

            if (!bolNiguanDato)
            {
                this.ctrlUsuario.usuario.strQueryPermisosUsuario = "";
            }
            else 
            {
                int intStrIndex = strPermisos.LastIndexOf("|");
                strPermisos = strPermisos.Remove(intStrIndex).Insert(intStrIndex, "'");                                     
                this.ctrlUsuario.usuario.strQueryPermisosUsuario = String.Format(this.ctrlUsuario.usuario.strQueryPermisosUsuario, intUsuarioId, intSucuarsalId, strPermisos);
            }

        }

        public void guardarCajas() 
        {


            string intUsuarioId = "";
            if (this.ctrlUsuario.usuario.intId == 0)
            {
                intUsuarioId = "@id";
            }
            else
            {
                intUsuarioId = this.ctrlUsuario.usuario.intId.ToString();
                this.ctrlUsuario.usuario.strQueryCajaUsuarioDelete = "DELETE FROM usuarios_cajas " +
                    " WHERE sucursal_id = "+txtSucursalId.Text.ToString()+" " +
                    " AND  usuario_id = "+ intUsuarioId + " ; ";
            }
            int countCajasUsuario = this.ctrlCajasUsuario.objDataSet.Tables[0].Rows.Count;

            this.ctrlUsuario.usuario.strQueryCajaUsuario = "INSERT INTO usuarios_cajas(usuario_id,sucursal_id,caja_id)VALUES ";
            
            int i = 0;
            bool bolNiguanDato = false;
            foreach (TreeNode tNode in tvCajas.Nodes)
            {
                if (i == countCajasUsuario) break;
                if (tNode.Checked)
                {                 
                    
                   this.ctrlUsuario.usuario.strQueryCajaUsuario += " (" + intUsuarioId + "," + txtSucursalId.Text + "," + tNode.Tag.ToString() + " ), ";                    
                    bolNiguanDato = true;
                }
                i++;
            }

            if (!bolNiguanDato)
            {
                this.ctrlUsuario.usuario.strQueryCajaUsuario = "";
            }
            else 
            {
                 int intStrIndex = this.ctrlUsuario.usuario.strQueryCajaUsuario.LastIndexOf(",");
                this.ctrlUsuario.usuario.strQueryCajaUsuario = this.ctrlUsuario.usuario.strQueryCajaUsuario.Remove(intStrIndex).Insert(intStrIndex, ";");              
            }
        }

        public void limpiarSucursal() 
        {
            txtSucursalId.Text = "";
            txtDescripcionSucursal.Text = "";
            txtCodigoSucursal.Text = "";
            tvCajas.Nodes.Clear();
            tvPermisos.Nodes.Clear();

        
        }
        private void button1_Click(object sender, EventArgs e)
        {

            
                

        }

        private void txtUsuario_Leave(object sender, EventArgs e)
        {

            if (txtUsuarioAntri.Text.ToString() == txtUsuario.Text.ToString()) return;

            this.ctrlUsuario.BuscarUsuario(txtUsuario.Text.ToString());
            if (this.ctrlUsuario.mdlUsuario.bolResultado)
            {
                this.clsView.principal.epMensaje.SetError(txtUsuario, this.clsView.mensajeErrorValiForm2("El usuario debe ser único, favor de verificar"));
                txtUsuario.Text = "";
            }
            
        }
    }
}
