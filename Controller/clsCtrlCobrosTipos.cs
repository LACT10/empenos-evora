﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
   public class clsCtrlCobrosTipos : Core.clsCore
    {
        public Model.clsMdlCobrosTipos mdlCobrosTipos;
        public Properties_Class.clsCobrosTipos cobrosTipos;
        


        public clsCtrlCobrosTipos()
        {
            this.cobrosTipos = new Properties_Class.clsCobrosTipos();
        }

        public void Guardar()
        {
            this.mdlCobrosTipos = new Model.clsMdlCobrosTipos();
            this.cobrosTipos.agregarCero();
            this.mdlCobrosTipos.guardar(this.cobrosTipos);
        }

        public void Eliminar()
        {
            if (this.cobrosTipos.intId == 0) return;
            this.mdlCobrosTipos = new Model.clsMdlCobrosTipos();
            //Hacer un llamado a la funcion para desactivar el registro
            this.cobrosTipos.cambioEstatus(this.cobrosTipos.dicEstatus["INACTIVO"]);
            this.mdlCobrosTipos.eliminar(this.cobrosTipos);
        }

        public void Buscar(string strCodigo)
        {
            if (strCodigo.Length > 0)
            {
                this.mdlCobrosTipos = new Model.clsMdlCobrosTipos();
                this.mdlCobrosTipos.buscar(strCodigo);

                if (this.mdlCobrosTipos.bolResultado)
                {
                    this.cobrosTipos.intId = Int32.Parse(this.mdlCobrosTipos.objResultado.Tables[0].Rows[0][0].ToString());
                    this.cobrosTipos.strCodigo = this.mdlCobrosTipos.objResultado.Tables[0].Rows[0][1].ToString();
                    this.cobrosTipos.strDescripcion = this.mdlCobrosTipos.objResultado.Tables[0].Rows[0][2].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }
        }

        public void ValidarDescripcion(string strDescripcion)
        {
            this.mdlCobrosTipos = new Model.clsMdlCobrosTipos();
            this.mdlCobrosTipos.validarDescripcion(strDescripcion);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlCobrosTipos.bolResultado;
        }

        public void Paginacion()
        {
            this.mdlCobrosTipos = new Model.clsMdlCobrosTipos();
            this.mdlCobrosTipos.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlCobrosTipos.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlCobrosTipos.bolResultado; ;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlCobrosTipos.objResultado;

        }
        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de tipo de cobros";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = "descripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                    "  codigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' ";    
            frmBusqueda.lsBloquearCol = new List<string>()
            {
                    "cobro_tipo_id",
                    "codigo",
                    "descripcion",
                    "estatus"
            };
            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.cobrosTipos.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.cobrosTipos.strCodigo = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.cobrosTipos.strDescripcion = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.cobrosTipos.strEstatus = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                    this.intCountIndex = int.Parse(this.cobrosTipos.strCodigo) - 1;
                }
                catch (Exception e) { }
            }
        }

        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.cobrosTipos.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.cobrosTipos.strCodigo = this.objDataSetSingle.ItemArray[1].ToString();
                this.cobrosTipos.strDescripcion = this.objDataSetSingle.ItemArray[2].ToString();
                this.cobrosTipos.strEstatus = this.objDataSetSingle.ItemArray[3].ToString();
            }

        }

        public void limpiarProps()
        {
            this.cobrosTipos.limpiarProps();
        }
    }
}