﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlFoliosProcesos : Core.clsCore
    {
        public Model.clsMdlFoliosProcesos mdlFolioProcesos;

        public Properties_Class.clsFoliosProcesos foliosProcesos;

        public int luis;

        public clsCtrlFoliosProcesos() 
        {
            this.foliosProcesos = new Properties_Class.clsFoliosProcesos();
        }



        public void Guardar()
        {
            this.mdlFolioProcesos = new Model.clsMdlFoliosProcesos();            
            this.mdlFolioProcesos.guardar(this.foliosProcesos);
        }

        public void Eliminar()
        {
            if (foliosProcesos.intId == 0) return;
            this.mdlFolioProcesos = new Model.clsMdlFoliosProcesos();
            //Hacer un llamado a la funcion para desactivar el registro
            this.foliosProcesos.cambioEstatus(this.foliosProcesos.dicEstatus["INACTIVO"]);
            this.mdlFolioProcesos.eliminar(this.foliosProcesos);
        }

        public void BuscarProcesoFolio()
        {
            this.mdlFolioProcesos = new Model.clsMdlFoliosProcesos();
            this.mdlFolioProcesos.buscarProcesoFolio(this.foliosProcesos);                    
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlFolioProcesos.objResultado;

        }

        public void BuscarFolioProcesoMenu(int intProcesoId, int intSucursalId )
        {
            this.mdlFolioProcesos = new Model.clsMdlFoliosProcesos();
            this.mdlFolioProcesos.buscarFolioProcesoMenu(intProcesoId, intSucursalId);
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlFolioProcesos.objResultado;
            
           
           this.foliosProcesos.folio.intId = int.Parse(this.objDataSet.Tables[0].Rows[0].ItemArray[0].ToString());
           //this.mdlFolioProcesos.incrementarConsecutivo(intFolioId);                


            


        }


        public void Paginacion()
        {
            this.mdlFolioProcesos = new Model.clsMdlFoliosProcesos();
            this.mdlFolioProcesos.paginacion(this.foliosProcesos);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlFolioProcesos.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlFolioProcesos.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlFolioProcesos.objResultado;

        }

        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de folios procesos";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = " descripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                   " serie Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                   " Proceso  Like '%" + frmBusqueda.dicReemplazo["texto"] + "%'";
            frmBusqueda.lsBloquearCol = new List<string>()
            {               
                "sucursal_id" ,
                "folio_id" ,
                "descripcion" ,
                "serie" ,
                "consecutivo" ,
                "proceso" ,
                "proceso_id"                
            };

            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {

                    this.foliosProcesos.intId = 1;
                    this.foliosProcesos.intSucursal = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.foliosProcesos.folio.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[1].Value.ToString());
                    this.foliosProcesos.folio.strDescripcion = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.foliosProcesos.folio.strSerie = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                    this.foliosProcesos.folio.intConsecutivo = Int32.Parse(frmBusqueda.rowsIndex.Cells[4].Value.ToString());
                    this.foliosProcesos.procesos.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[5].Value.ToString());
                    this.foliosProcesos.procesos.strDescripcion = frmBusqueda.rowsIndex.Cells[6].Value.ToString();
                }
                catch (Exception e) { }
            }
        }


        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {

                this.foliosProcesos.intId = 1;
                this.foliosProcesos.intSucursal = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.foliosProcesos.folio.intId = Int32.Parse(this.objDataSetSingle.ItemArray[1].ToString());
                this.foliosProcesos.folio.strDescripcion = this.objDataSetSingle.ItemArray[2].ToString();
                this.foliosProcesos.folio.strSerie = this.objDataSetSingle.ItemArray[3].ToString();
                this.foliosProcesos.folio.intConsecutivo = Int32.Parse(this.objDataSetSingle.ItemArray[4].ToString());
                this.foliosProcesos.procesos.intId = Int32.Parse(this.objDataSetSingle.ItemArray[5].ToString());
                this.foliosProcesos.procesos.strDescripcion = this.objDataSetSingle.ItemArray[6].ToString();
                this.foliosProcesos.strEstatus = this.objDataSetSingle.ItemArray[7].ToString();                
            }
            

        }

        public void limpiarProps()
        {
            this.foliosProcesos.limpiarProps();
        }



    }
}
