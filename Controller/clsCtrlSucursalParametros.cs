﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlSucursalParametros : Core.clsCore
    {

        public Model.clsMdlSucursalParametros mdlSucursalesParametro;
        public Properties_Class.clsSucursalParametro sucursalParametro;

        public clsCtrlSucursalParametros()
        {
            this.sucursalParametro = new Properties_Class.clsSucursalParametro();
        }


        public void Buscar(string strSucursalId) 
        {
            if (strSucursalId.Length > 0)
            {
                this.mdlSucursalesParametro = new Model.clsMdlSucursalParametros();
                this.mdlSucursalesParametro.buscar(strSucursalId);
                //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
                this.bolResultado = this.mdlSucursalesParametro.bolResultado;
                //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
                this.objDataSet = this.mdlSucursalesParametro.objResultado;
                if (this.bolResultado)
                {
                    try
                    {                        
                        this.sucursalParametro.intDiasCobroMinimo = int.Parse(this.mdlSucursalesParametro.objResultado.Tables[0].Rows[0][0].ToString());
                        this.sucursalParametro.intDiasCobroCuotaDiaria = int.Parse(this.mdlSucursalesParametro.objResultado.Tables[0].Rows[0][1].ToString());
                        this.sucursalParametro.intDiasCobroCuotaDiariaEspecial = int.Parse(this.mdlSucursalesParametro.objResultado.Tables[0].Rows[0][2].ToString());
                        this.sucursalParametro.intDiasLiquidarBoletas = int.Parse(this.mdlSucursalesParametro.objResultado.Tables[0].Rows[0][3].ToString());
                        this.sucursalParametro.intDiasRefrendarBoletas = int.Parse(this.mdlSucursalesParametro.objResultado.Tables[0].Rows[0][4].ToString());
                        this.sucursalParametro.intDiasRematePrendas = int.Parse(this.mdlSucursalesParametro.objResultado.Tables[0].Rows[0][5].ToString());
                        this.sucursalParametro.decAvaluo = decimal.Parse(this.mdlSucursalesParametro.objResultado.Tables[0].Rows[0][6].ToString());
                        this.sucursalParametro.decAvaluoImpresion = decimal.Parse(this.mdlSucursalesParametro.objResultado.Tables[0].Rows[0][7].ToString());
                    }
                    catch (Exception e) { }
                    
                }
                else
                {
                    this.limpiarProps();
                }
            }
        }

    
        public void limpiarProps()
        {
            this.sucursalParametro.limpiarProps();
        }

    }
}
