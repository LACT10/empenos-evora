﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlLogin : Core.clsCore
    {

        public Properties_Class.clsLogin login;

        public clsCtrlLogin()
        {
            this.login = new Properties_Class.clsLogin();
        }

        public void BuscarUsuario(string strUsuario) 
        {
            Model.clsMdlLogin mdlLogin = new Model.clsMdlLogin();
            mdlLogin.buscarUsuario(strUsuario);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = mdlLogin.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = mdlLogin.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = mdlLogin.objResultado;
            //Llenar 
            lLenarObjecto();
        }

        public void Incrementar() 
        {
            Model.clsMdlLogin mdlLogin = new Model.clsMdlLogin();

            mdlLogin.IncrementarIntentos(this.login);
        }

        public void CambioEstatus() 
        {
            Model.clsMdlLogin mdlLogin = new Model.clsMdlLogin();
            this.login.usuario.strEstatus = this.login.dicEstatus["SUSPENDIDO"];
            mdlLogin.cambairEstatus(this.login);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = mdlLogin.bolResultado;
        }

        public void ReiniciarIntentos()
        {
            Model.clsMdlLogin mdlLogin = new Model.clsMdlLogin();
            this.login.usuario.byteIntentos = 0;
            mdlLogin.reiniciarIntentos(this.login);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = mdlLogin.bolResultado;
        }

        public void Login(string strUsuario, string strContrasena) 
        {
            Model.clsMdlLogin mdlLogin = new Model.clsMdlLogin();
            mdlLogin.login(strUsuario, strContrasena);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = mdlLogin.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = mdlLogin.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = mdlLogin.objResultado;

            this.lLenarObjecto();

        }

        public void BuscarPermisosAcessoUsuario() 
        {
            Model.clsMdlLogin mdlLogin = new Model.clsMdlLogin();
            mdlLogin.buscarPermisosAcessoUsuario(this.login);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = mdlLogin.bolResultado;            
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = mdlLogin.objResultado;


            if (this.bolResultado) 
            {
                DataRow objDataSetSingle = this.objDataSet.Tables[0].Rows[0];
                this.login.strPermisos = objDataSetSingle.ItemArray[0].ToString();
            }
            
        }

        public void BuscarPermisosAcessoProcessoUsuario()
        {
            Model.clsMdlLogin mdlLogin = new Model.clsMdlLogin();

            this.login.arrStrIdsSubProcesos = this.login.strPermisos.Split('|');

            this.login.strQueryPermisosAccessoProcessoUsuario = "SELECT " +
                                   " sp.proceso_id, " +
                                   " p.nombre_menu " + 
                                   " FROM subprocesos AS sp" +
                                   " INNER JOIN procesos AS p" +
                                   " ON sp.proceso_id = p.proceso_id" +                                   
                                   " WHERE sp.subproceso_id IN ({0})" +
                                   " GROUP BY sp.proceso_id;";
            string strSubPermisosId = "";


            int i = 0;
            bool bolNiguanDato = false;

            int countPermisosUsuario = this.login.arrStrIdsSubProcesos.Length;

            foreach (string row in this.login.arrStrIdsSubProcesos)
            {
                if (i == countPermisosUsuario) return;

                if (i < countPermisosUsuario)
                {
                    strSubPermisosId += row + " , ";
                }

                bolNiguanDato = true;

                i++;
            }


            if (!bolNiguanDato)
            {
                this.login.strQueryPermisosAccessoProcessoUsuario = "";
            }
            else
            {
                int intStrIndex = strSubPermisosId.LastIndexOf(",");
                strSubPermisosId = strSubPermisosId.Remove(intStrIndex).Insert(intStrIndex, "");
                this.login.strQueryPermisosAccessoProcessoUsuario = String.Format(this.login.strQueryPermisosAccessoProcessoUsuario, strSubPermisosId);
            }

            mdlLogin.BuscarPermisosAcessoProcessoUsuario(this.login);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = mdlLogin.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = mdlLogin.objResultado;

            this.login.objProcesos = this.objDataSet;
            
        }


        public void BuscarPermisosAcessoSubProcessoUsuario()
        {
            Model.clsMdlLogin mdlLogin = new Model.clsMdlLogin();

            this.login.arrStrIdsSubProcesos = this.login.strPermisos.Split('|');

            this.login.strQueryPermisosAccessoProcessoUsuario = "SELECT " +
                                   " sp.proceso_id, " +
                                   " sp.nombre_menu " +
                                   " FROM subprocesos AS sp" +
                                   " INNER JOIN procesos AS p" +
                                   " ON sp.proceso_id = p.proceso_id" +
                                   " WHERE sp.subproceso_id IN ({0})";                                  
            string strSubPermisosId = "";


            int i = 0;
            bool bolNiguanDato = false;

            int countPermisosUsuario = this.login.arrStrIdsSubProcesos.Length;

            foreach (string row in this.login.arrStrIdsSubProcesos)
            {
                if (i == countPermisosUsuario) return;

                if (i < countPermisosUsuario)
                {
                    strSubPermisosId += row + " , ";
                }

                bolNiguanDato = true;

                i++;
            }


            if (!bolNiguanDato)
            {
                this.login.strQueryPermisosAccessoProcessoUsuario = "";
            }
            else
            {
                int intStrIndex = strSubPermisosId.LastIndexOf(",");
                strSubPermisosId = strSubPermisosId.Remove(intStrIndex).Insert(intStrIndex, "");
                this.login.strQueryPermisosAccessoProcessoUsuario = String.Format(this.login.strQueryPermisosAccessoProcessoUsuario, strSubPermisosId);
            }

            mdlLogin.BuscarPermisosAcessoProcessoUsuario(this.login);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = mdlLogin.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = mdlLogin.objResultado;

            this.login.objSubProcesos = this.objDataSet;

        }

        public void BuscarCaja() 
        {

            Model.clsMdlLogin mdlLogin = new Model.clsMdlLogin();
            mdlLogin.buscarCajaUsuario(this.login);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = mdlLogin.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = mdlLogin.objResultado;

            this.login.objCajas = this.objDataSet;            
        }

        public void lLenarObjecto()
        {

            if (this.bolResultado)
            {
                DataRow objDataSetSingle = this.objDataSet.Tables[0].Rows[0];
                this.login.usuario.intId = Int32.Parse(objDataSetSingle.ItemArray[0].ToString());
                this.login.usuario.strNombre = objDataSetSingle.ItemArray[1].ToString();
                this.login.usuario.strApellidoPaterno = objDataSetSingle.ItemArray[2].ToString();
                this.login.usuario.strApellidoMaterno = objDataSetSingle.ItemArray[3].ToString();
                this.login.usuario.strTelefono01 = objDataSetSingle.ItemArray[4].ToString();
                this.login.usuario.strTelefono02 = objDataSetSingle.ItemArray[5].ToString();
                this.login.usuario.strCorreoElectronico = objDataSetSingle.ItemArray[6].ToString();
                this.login.usuario.strUsuario = objDataSetSingle.ItemArray[7].ToString();
                this.login.usuario.strContrasena = objDataSetSingle.ItemArray[8].ToString();
                this.login.usuario.byteIntentos = Byte.Parse(objDataSetSingle.ItemArray[9].ToString());
                this.login.usuario.strAutorizar = objDataSetSingle.ItemArray[10].ToString();
                this.login.usuario.strModificarMovimientos = objDataSetSingle.ItemArray[11].ToString();
                this.login.usuario.strEstatus = objDataSetSingle.ItemArray[12].ToString();
            }
        }

        public void limpiarProps()
        {
            this.login.limpiarProps();
        }


    }
}