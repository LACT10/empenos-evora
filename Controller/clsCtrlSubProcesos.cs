﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlSubProcesos : Core.clsCore
    {

        public Model.clsMdlSubProcesos mdlSubProcesos;

        public void Buscar(string strSubProcesoslId)
        {
            this.mdlSubProcesos = new Model.clsMdlSubProcesos();
            this.mdlSubProcesos.buscar(strSubProcesoslId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlSubProcesos.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlSubProcesos.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlSubProcesos.objResultado;

        }

        public void Paginacion() 
        {
            this.mdlSubProcesos = new Model.clsMdlSubProcesos();
            this.mdlSubProcesos.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlSubProcesos.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlSubProcesos.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlSubProcesos.objResultado;
        }

        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                /*this.sucursalCobrosTipos.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.sucursalCobrosTipos.strCodigo = this.objDataSetSingle.ItemArray[1].ToString();
                this.sucursalCobrosTipos.strDescripcion = this.objDataSetSingle.ItemArray[2].ToString();
                this.sucursalCobrosTipos.strEstatus = this.objDataSetSingle.ItemArray[3].ToString();*/
            }

        }
    }
}
