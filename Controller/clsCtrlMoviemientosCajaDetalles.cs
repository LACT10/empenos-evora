﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
   public class clsCtrlMoviemientosCajaDetalles : Core.clsCore
    {
        public Model.clsMdlMovimientosCajaDetalles mdlMovimientosCajaDetalles;

        public Properties_Class.clsMoviemientosCajaDetalles movimientosCajaDetalles;

        public clsCtrlMoviemientosCajaDetalles()
        {
            this.movimientosCajaDetalles = new Properties_Class.clsMoviemientosCajaDetalles();
        }

        public void Buscar(int intMovimientoCajaId) 
        {
            this.mdlMovimientosCajaDetalles = new Model.clsMdlMovimientosCajaDetalles();
            this.mdlMovimientosCajaDetalles.buscar(intMovimientoCajaId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlMovimientosCajaDetalles.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlMovimientosCajaDetalles.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlMovimientosCajaDetalles.objResultado;
        }

        public void PaginacionCajaId(int intMovimientoCajaId)
        {
            this.mdlMovimientosCajaDetalles = new Model.clsMdlMovimientosCajaDetalles();
            this.mdlMovimientosCajaDetalles.paginacionCajaId(intMovimientoCajaId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlMovimientosCajaDetalles.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlMovimientosCajaDetalles.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlMovimientosCajaDetalles.objResultado;
        }

    }
}
