﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlSucursalesPromocion : Core.clsCore
    {

        public Model.clsMdlSucursalesPromocion mdlSucursalesPromocion;

        public Properties_Class.clsSucursalesPromocion sucursalesPromocion;

        public clsCtrlSucursalesPromocion()
        {
            this.sucursalesPromocion = new Properties_Class.clsSucursalesPromocion();
        }
        public void Guardar()
        {
            this.mdlSucursalesPromocion = new Model.clsMdlSucursalesPromocion();            
            this.mdlSucursalesPromocion.guardar(this.sucursalesPromocion);
            this.bolResultado = this.mdlSucursalesPromocion.bolResultado;
        }

        public void Eliminar(int intSucursalPromcionId)
        {
            if (intSucursalPromcionId == 0) return;
            this.mdlSucursalesPromocion = new Model.clsMdlSucursalesPromocion();
            //Hacer un llamado a la funcion para desactivar el registro
            this.mdlSucursalesPromocion.eliminar(intSucursalPromcionId);
            this.bolResultado = this.mdlSucursalesPromocion.bolResultado;
        }
        public void Buscar(int intSucursalPromocionId)
        {
            if (intSucursalPromocionId > 0)
            {
                this.mdlSucursalesPromocion = new Model.clsMdlSucursalesPromocion();
                this.mdlSucursalesPromocion.buscar(intSucursalPromocionId);
                //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
                this.bolResultado = this.mdlSucursalesPromocion.bolResultado;
                if (this.mdlSucursalesPromocion.bolResultado)
                {

                    this.sucursalesPromocion.intId = Int32.Parse(this.mdlSucursalesPromocion.objResultado.Tables[0].Rows[0][0].ToString());
                    this.sucursalesPromocion.strDia = this.mdlSucursalesPromocion.objResultado.Tables[0].Rows[0][1].ToString();
                    this.sucursalesPromocion.decDiaMin = decimal.Parse(this.mdlSucursalesPromocion.objResultado.Tables[0].Rows[0][2].ToString());
                    this.sucursalesPromocion.decPorcentaje = decimal.Parse(this.mdlSucursalesPromocion.objResultado.Tables[0].Rows[0][3].ToString());
                    this.sucursalesPromocion.strEstatus = this.mdlSucursalesPromocion.objResultado.Tables[0].Rows[0][4].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
        }

        public void BuscarPromocionDia(int intSucusrsalId, string strDia) 
        {
            this.mdlSucursalesPromocion = new Model.clsMdlSucursalesPromocion();
            this.mdlSucursalesPromocion.buscarPromocionDia(intSucusrsalId, strDia);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlSucursalesPromocion.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlSucursalesPromocion.objResultado;
        }
        public void BuscarTipoPrenda(int intSucusrsalId, string strTipoPrenda, string strDia)
        {
            if (intSucusrsalId > 0)
            {
                this.mdlSucursalesPromocion = new Model.clsMdlSucursalesPromocion();
                this.mdlSucursalesPromocion.buscarTipoPrenda(intSucusrsalId, strTipoPrenda, strDia);
                //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
                this.bolResultado = this.mdlSucursalesPromocion.bolResultado;
                if (this.mdlSucursalesPromocion.bolResultado)
                {                    
                    this.sucursalesPromocion.intId = Int32.Parse(this.mdlSucursalesPromocion.objResultado.Tables[0].Rows[0][0].ToString());
                    this.sucursalesPromocion.strDia = this.mdlSucursalesPromocion.objResultado.Tables[0].Rows[0][1].ToString();
                    this.sucursalesPromocion.decDiaMin = decimal.Parse(this.mdlSucursalesPromocion.objResultado.Tables[0].Rows[0][2].ToString());
                    this.sucursalesPromocion.decPorcentaje = decimal.Parse(this.mdlSucursalesPromocion.objResultado.Tables[0].Rows[0][3].ToString());
                    this.sucursalesPromocion.strEstatus = this.mdlSucursalesPromocion.objResultado.Tables[0].Rows[0][4].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
        }

        public void Paginacion_detalles(int intSucursald, int intSucursalPromocionId)
        {
            this.mdlSucursalesPromocion = new Model.clsMdlSucursalesPromocion();
            this.mdlSucursalesPromocion.paginacion_detalles(intSucursald, intSucursalPromocionId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlSucursalesPromocion.bolResultado;            
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlSucursalesPromocion.objResultado;

        }

        public void Paginacion(int intSucursald)
        {
            this.mdlSucursalesPromocion = new Model.clsMdlSucursalesPromocion();
            this.mdlSucursalesPromocion.paginacion(intSucursald);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlSucursalesPromocion.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlSucursalesPromocion.objResultado;

        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                /*this.sucursalPlazo.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.sucursalPlazo.strCodigo = this.objDataSetSingle.ItemArray[1].ToString();
                this.sucursalPlazo.strDescripcion = this.objDataSetSingle.ItemArray[2].ToString();
                this.sucursalPlazo.strEstatus = this.objDataSetSingle.ItemArray[3].ToString();*/
            }

        }

        public void limpiarProps()
        {
            this.sucursalesPromocion.limpiarProps();
        }
    }
}
