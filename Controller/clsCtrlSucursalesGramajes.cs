﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
   public class clsCtrlSucursalesGramajes : Core.clsCore
    {   
        public Model.clsMdlSucursalesGramajes mdlSucursalesGramajes;

        public Properties_Class.clsSucursalGramaje sucursalGramaje;

        public clsCtrlSucursalesGramajes()
        {
            this.sucursalGramaje = new Properties_Class.clsSucursalGramaje();
        }

        public void Buscar(string strSucursalId)
        {
            if (strSucursalId.Length > 0)
            {
                this.mdlSucursalesGramajes = new Model.clsMdlSucursalesGramajes();
                this.mdlSucursalesGramajes.buscar(strSucursalId);
                //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
                this.bolResultado = this.mdlSucursalesGramajes.bolResultado;
                //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
                this.bolResutladoPaginacion = this.mdlSucursalesGramajes.bolResultado;
                //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
                this.objDataSet = this.mdlSucursalesGramajes.objResultado;
            }
        }

        public void Paginacion(int intSucursalId)
        {
            this.mdlSucursalesGramajes = new Model.clsMdlSucursalesGramajes();
            this.mdlSucursalesGramajes.paginacion(intSucursalId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlSucursalesGramajes.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlSucursalesGramajes.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlSucursalesGramajes.objResultado;

        }

        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()

        {
            if (this.bolResultado)
            {
                /*this.sucursalGramaje.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.sucursalGramaje.strCodigo = this.objDataSetSingle.ItemArray[1].ToString();
                this.sucursalGramaje.strDescripcion = this.objDataSetSingle.ItemArray[2].ToString();
                this.sucursalGramaje.strEstatus = this.objDataSetSingle.ItemArray[3].ToString();*/
            } 

        }

        public void limpiarProps()
        {
            this.sucursalGramaje.limpiarProps();
        }
    }
}
