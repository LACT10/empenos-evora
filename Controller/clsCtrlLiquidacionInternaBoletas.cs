﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlLiquidacionInternaBoletas : Core.clsCore
    {


        public Properties_Class.clsLiquidacionInternaBoletas internaBoletas = new Properties_Class.clsLiquidacionInternaBoletas();
        public Model.clsMdlLiquidacionInternaBoletas mdlInternaBoletas;

        public void Guardar()
        {
            this.mdlInternaBoletas = new Model.clsMdlLiquidacionInternaBoletas();
            this.mdlInternaBoletas.guardar(this.internaBoletas);
            this.bolResultado = this.mdlInternaBoletas.bolResultado;
        }


        public void PaginacionDetallesBoletasActivasAdjudicada(int intPagoId)
        {

            this.mdlInternaBoletas = new Model.clsMdlLiquidacionInternaBoletas();
            this.mdlInternaBoletas.paginacionDetallesBoletasActivasAdjudicada(intPagoId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlInternaBoletas.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSetSecundario = this.mdlInternaBoletas.objResultado;
        }


        public void limpiarProps()
        {
            this.internaBoletas.limpiarProps();
        }

    }
}
