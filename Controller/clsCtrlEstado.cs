﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data;


namespace EmpenosEvora.Controller 
{
    public class clsCtrlEstado : Core.clsCore
    {

        public Model.clsMdlEstado mdlEstado;

        public Properties_Class.clsEstado estado;
       
        public clsCtrlEstado()
        {            
            this.estado = new Properties_Class.clsEstado();
        }

        public void Guardar()
        {
            this.mdlEstado = new Model.clsMdlEstado();
            this.estado.agregarCero();
            this.mdlEstado.guardar(this.estado);                     
        }

        public void Eliminar()
        {                        
            if (estado.intId == 0) return;
            this.mdlEstado = new Model.clsMdlEstado();
            //Hacer un llamado a la funcion para desactivar el registro
            this.estado.cambioEstatus(this.estado.dicEstatus["INACTIVO"]);
            this.mdlEstado.eliminar(this.estado);
        }

        public void Reactivar()
        {
            if (estado.intId == 0) return;
            this.mdlEstado = new Model.clsMdlEstado();
            //Hacer un llamado a la funcion para desactivar el registro
            this.estado.cambioEstatus(this.estado.dicEstatus["ACTIVO"]);
            this.mdlEstado.reactivar(this.estado);
            this.bolResultado = this.mdlEstado.bolResultado;
        }

        public void Buscar(string strCodigo)
        {
            if (strCodigo.Length > 0)
            {
                this.mdlEstado = new Model.clsMdlEstado();
                this.mdlEstado.buscar(strCodigo);

                if (this.mdlEstado.bolResultado)
                {
                    this.estado.intId = Int32.Parse(this.mdlEstado.objResultado.Tables[0].Rows[0][0].ToString());
                    this.estado.strCodigo = this.mdlEstado.objResultado.Tables[0].Rows[0][1].ToString();
                    this.estado.strDescripcion = this.mdlEstado.objResultado.Tables[0].Rows[0][2].ToString();  
                    this.estado.strEstatus = this.mdlEstado.objResultado.Tables[0].Rows[0][3].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }

            
        }

        public void ValidarDescripcion(string strDescripcion)
        {
            this.mdlEstado = new Model.clsMdlEstado();
            this.mdlEstado.validarDescripcion(strDescripcion);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlEstado.bolResultado;            
        }

        public void Paginacion()
        {
            this.mdlEstado = new Model.clsMdlEstado();
            this.mdlEstado.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlEstado.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlEstado.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlEstado.objResultado;
            
        }

        public void CargarFrmBusqueda()
        {            
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de estados";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = "descripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   "  codigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%'";   
            frmBusqueda.lsBloquearCol = new List<string>()
            {
                    "estado_id",
                    "codigo",
                    "descripcion",
                    "estatus"
            };

            frmBusqueda.objDataSet = this.objDataSet;            
            frmBusqueda.ShowDialog();

            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.estado.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.estado.strCodigo = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.estado.strDescripcion = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.estado.strEstatus = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                    this.intCountIndex = int.Parse(this.estado.strCodigo) - 1;
                }
                catch (Exception e) { }
            }
        }



        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.estado.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.estado.strCodigo = this.objDataSetSingle.ItemArray[1].ToString();
                this.estado.strDescripcion = this.objDataSetSingle.ItemArray[2].ToString();
                this.estado.strEstatus = this.objDataSetSingle.ItemArray[3].ToString();
            }  
            
        }

        public void limpiarProps()
        {
            this.estado.limpiarProps();
        }
    }
}
