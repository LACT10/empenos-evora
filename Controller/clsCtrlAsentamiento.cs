﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlAsentamiento : Core.clsCore
    {       
        public Model.clsMdlAsentamiento mdlAsentamiento;
        public Properties_Class.clsAsentamiento asentamiento;


        public clsCtrlAsentamiento()
        {
            this.asentamiento = new Properties_Class.clsAsentamiento();
            
        }

        public void Guardar()
        {
            this.mdlAsentamiento = new Model.clsMdlAsentamiento();            
            this.mdlAsentamiento.guardar(this.asentamiento);
        }

        public void Eliminar()
        {
            if (this.asentamiento.intId == 0) return;
            this.mdlAsentamiento = new Model.clsMdlAsentamiento();
            //Hacer un llamado a la funcion para desactivar el registro
            this.asentamiento.cambioEstatus(this.asentamiento.dicEstatus["INACTIVO"]);
            this.mdlAsentamiento.eliminar(this.asentamiento);
        }

        public void Buscar(string strCodigo)
        {
            if (strCodigo.Length > 0)
            {
                this.mdlAsentamiento = new Model.clsMdlAsentamiento();
                this.mdlAsentamiento.buscar(strCodigo);

                if (this.mdlAsentamiento.bolResultado)
                {
                    this.asentamiento.intId = Int32.Parse(this.mdlAsentamiento.objResultado.Tables[0].Rows[0][0].ToString());                    
                    this.asentamiento.strDescripcion = this.mdlAsentamiento.objResultado.Tables[0].Rows[0][1].ToString();
                    this.asentamiento.strEstatus = this.mdlAsentamiento.objResultado.Tables[0].Rows[0][2].ToString();
                    this.asentamiento.municipio.intId = Int32.Parse(this.mdlAsentamiento.objResultado.Tables[0].Rows[0][3].ToString());
                    this.asentamiento.municipio.strCodigo = this.mdlAsentamiento.objResultado.Tables[0].Rows[0][4].ToString();
                    this.asentamiento.municipio.strDescripcion = this.mdlAsentamiento.objResultado.Tables[0].Rows[0][5].ToString();
                    this.asentamiento.asentamientosTipos.intId = Int32.Parse(this.mdlAsentamiento.objResultado.Tables[0].Rows[0][6].ToString());
                    this.asentamiento.asentamientosTipos.strCodigo = this.mdlAsentamiento.objResultado.Tables[0].Rows[0][7].ToString();
                    this.asentamiento.asentamientosTipos.strDescripcion = this.mdlAsentamiento.objResultado.Tables[0].Rows[0][8].ToString();
                    this.asentamiento.strCiudad = this.mdlAsentamiento.objResultado.Tables[0].Rows[0][9].ToString();
                    this.asentamiento.strCodigoPostal = this.mdlAsentamiento.objResultado.Tables[0].Rows[0][10].ToString();
                    this.asentamiento.municipio.estado.intId = Int32.Parse(this.mdlAsentamiento.objResultado.Tables[0].Rows[0][11].ToString());
                    this.asentamiento.municipio.estado.strCodigo = this.mdlAsentamiento.objResultado.Tables[0].Rows[0][12].ToString();
                    this.asentamiento.municipio.estado.strDescripcion = this.mdlAsentamiento.objResultado.Tables[0].Rows[0][13].ToString();
                   
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }
        }

        public void validarDescripcionMunicipio(string strDescripcion, string strMunicpioId)
        {
            this.mdlAsentamiento = new Model.clsMdlAsentamiento();
            this.mdlAsentamiento.validarDescripcionMunicipio(strDescripcion, strMunicpioId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlAsentamiento.bolResultado;
        }
        public void Paginacion()
        {
            this.mdlAsentamiento = new Model.clsMdlAsentamiento();
            this.mdlAsentamiento.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlAsentamiento.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlAsentamiento.bolResultado; 
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlAsentamiento.objResultado;

        }
        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de asentamientos ";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = "descripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
            "  municipioCodigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
            "  municipioDescripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
            "  asentamientoTipoCodigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
            "  asentamientoTipoDescripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' ";
            
            frmBusqueda.lsBloquearCol = new List<string>()
            {
                 
                    "asentamiento_id",                    
                    "descripcion",
                    "estatus",
                    "municipio_id",
                    "asentamiento_tipo_id",
                    "ciudad",
                    "estado_id",
                    "codigo_postal",
                    "municipioCodigo",
                    "municipioDescripcion",
                    "asentamientoTipoCodigo",
                    "asentamientoTipoDescripcion",
                    "estadoCodigo",
                    "estadoDescripcion",
                    "Ciudad ",
                    "Código tipo de asentamiento",
                    "Código municipio",
                    "Tipo de asentamiento"


            };
            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.asentamiento.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.asentamiento.strDescripcion = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.asentamiento.strEstatus = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.asentamiento.municipio.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[3].Value.ToString());
                    this.asentamiento.municipio.strCodigo = frmBusqueda.rowsIndex.Cells[4].Value.ToString();
                    this.asentamiento.municipio.strDescripcion = frmBusqueda.rowsIndex.Cells[5].Value.ToString();
                    this.asentamiento.asentamientosTipos.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[6].Value.ToString());
                    this.asentamiento.asentamientosTipos.strCodigo = frmBusqueda.rowsIndex.Cells[7].Value.ToString();
                    this.asentamiento.asentamientosTipos.strDescripcion = frmBusqueda.rowsIndex.Cells[8].Value.ToString();
                    this.asentamiento.strCiudad = frmBusqueda.rowsIndex.Cells[9].Value.ToString();
                    this.asentamiento.strCodigoPostal = frmBusqueda.rowsIndex.Cells[10].Value.ToString();
                    this.asentamiento.municipio.estado.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[11].Value.ToString());
                    this.asentamiento.municipio.estado.strCodigo = frmBusqueda.rowsIndex.Cells[12].Value.ToString();
                    this.asentamiento.municipio.estado.strDescripcion = frmBusqueda.rowsIndex.Cells[13].Value.ToString();
                    //this.intCountIndex = this.asentamiento.intId - 1;
                }
                catch (Exception e) { }

            }
        }

        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.asentamiento.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());                
                this.asentamiento.strDescripcion = this.objDataSetSingle.ItemArray[1].ToString();
                this.asentamiento.strEstatus = this.objDataSetSingle.ItemArray[2].ToString();
                this.asentamiento.municipio.intId = Int32.Parse(this.objDataSetSingle.ItemArray[3].ToString());
                this.asentamiento.municipio.strCodigo = this.objDataSetSingle.ItemArray[4].ToString();
                this.asentamiento.municipio.strDescripcion = this.objDataSetSingle.ItemArray[5].ToString();
                this.asentamiento.asentamientosTipos.intId = Int32.Parse(this.objDataSetSingle.ItemArray[6].ToString());
                this.asentamiento.asentamientosTipos.strCodigo = this.objDataSetSingle.ItemArray[7].ToString();
                this.asentamiento.asentamientosTipos.strDescripcion = this.objDataSetSingle.ItemArray[8].ToString();
                this.asentamiento.strCiudad = this.objDataSetSingle.ItemArray[9].ToString();
                this.asentamiento.strCodigoPostal = this.objDataSetSingle.ItemArray[10].ToString();
                this.asentamiento.municipio.estado.intId = Int32.Parse(this.objDataSetSingle.ItemArray[11].ToString());
                this.asentamiento.municipio.estado.strCodigo = this.objDataSetSingle.ItemArray[12].ToString();
                this.asentamiento.municipio.estado.strDescripcion = this.objDataSetSingle.ItemArray[13].ToString();

            }

        }

        public void limpiarProps()
        {
            this.asentamiento.limpiarProps();
        }
    }
}
