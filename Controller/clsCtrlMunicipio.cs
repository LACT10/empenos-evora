﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlMunicipio : Core.clsCore
    {

        public Model.clsMdlMunicipio mdlMunicipio;
        public Properties_Class.clsMunicipio municipio;
        
           
        public clsCtrlMunicipio()
        {
            this.municipio = new Properties_Class.clsMunicipio();
        }

        public void Guardar()
        {
            this.mdlMunicipio = new Model.clsMdlMunicipio();
            this.municipio.agregarCero();
            this.mdlMunicipio.guardar(this.municipio);
        }

        public void Eliminar()
        {
            if (this.municipio.intId == 0) return;
            this.mdlMunicipio = new Model.clsMdlMunicipio();
            //Hacer un llamado a la funcion para desactivar el registro
            this.municipio.cambioEstatus(this.municipio.dicEstatus["INACTIVO"]);
            this.mdlMunicipio.eliminar(this.municipio);
        }

        public void Buscar(string strCodigo)
        {
            if (strCodigo.Length > 0)
            {
                this.mdlMunicipio = new Model.clsMdlMunicipio();
                this.mdlMunicipio.buscar(strCodigo);

                if (this.mdlMunicipio.bolResultado)
                {
                    this.municipio.intId = Int32.Parse(this.mdlMunicipio.objResultado.Tables[0].Rows[0][0].ToString());
                    this.municipio.strCodigo = this.mdlMunicipio.objResultado.Tables[0].Rows[0][1].ToString();
                    this.municipio.strDescripcion = this.mdlMunicipio.objResultado.Tables[0].Rows[0][2].ToString();
                    this.municipio.strEstatus = this.mdlMunicipio.objResultado.Tables[0].Rows[0][3].ToString();
                    this.municipio.estado.intId = Int32.Parse(this.mdlMunicipio.objResultado.Tables[0].Rows[0][4].ToString());
                    this.municipio.estado.strCodigo = this.mdlMunicipio.objResultado.Tables[0].Rows[0][5].ToString();
                    this.municipio.estado.strDescripcion = this.mdlMunicipio.objResultado.Tables[0].Rows[0][6].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }
        }

        public void ValidarDescripcion(string strDescripcion)
        {
            this.mdlMunicipio = new Model.clsMdlMunicipio();
            this.mdlMunicipio.validarDescripcion(strDescripcion);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlMunicipio.bolResultado;
        }

        public void Paginacion()
        {
            this.mdlMunicipio = new Model.clsMdlMunicipio();
            this.mdlMunicipio.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlMunicipio.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlMunicipio.bolResultado; ;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlMunicipio.objResultado;

        }
        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de municipios";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
                        
            frmBusqueda.strQuery = "descripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
               "  codigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
               "  estadoDescripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
               " estadoCodigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%'";

            frmBusqueda.lsBloquearCol = new List<string>()
            {
                    "municipio_id",
                    "codigo",
                    "descripcion",
                    "estatus",
                    "estado_id",
                    "estadoCodigo",
                    "estadoDescripcion"
            };
            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {

                    this.municipio.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.municipio.strCodigo = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.municipio.strDescripcion = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.municipio.strEstatus = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                    this.municipio.estado.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[4].Value.ToString());
                    this.municipio.estado.strCodigo = frmBusqueda.rowsIndex.Cells[5].Value.ToString();
                    this.municipio.estado.strDescripcion = frmBusqueda.rowsIndex.Cells[6].Value.ToString();
                }
                catch (Exception e) { }
            }
        }

        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.municipio.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.municipio.strCodigo = this.objDataSetSingle.ItemArray[1].ToString();
                this.municipio.strDescripcion = this.objDataSetSingle.ItemArray[2].ToString();                
                this.municipio.strEstatus = this.objDataSetSingle.ItemArray[3].ToString();
                this.municipio.estado.intId = Int32.Parse(this.objDataSetSingle.ItemArray[4].ToString());
                this.municipio.estado.strCodigo = this.objDataSetSingle.ItemArray[5].ToString();
                this.municipio.estado.strDescripcion = this.objDataSetSingle.ItemArray[6].ToString();
            }

        }

        public void limpiarProps()
        {
            this.municipio.limpiarProps();
        }
    }
}
