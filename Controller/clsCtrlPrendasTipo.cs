﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlPrendasTipo : Core.clsCore
    {
        
        public Model.clsMdlPrendasTipo mdlPrendasTipo;
        public Properties_Class.clsPrendasTipo prendasTipo;
        
        public clsCtrlPrendasTipo()
        {
            this.prendasTipo = new Properties_Class.clsPrendasTipo();
        }

        public void Guardar()
        {
            this.mdlPrendasTipo = new Model.clsMdlPrendasTipo();            
            this.mdlPrendasTipo.guardar(this.prendasTipo);
        }

        public void Eliminar()
        {
            if (this.prendasTipo.intId == 0) return;
            this.mdlPrendasTipo = new Model.clsMdlPrendasTipo();
            //Hacer un llamado a la funcion para desactivar el registro
            this.prendasTipo.cambioEstatus(this.prendasTipo.dicEstatus["INACTIVO"]);
            this.mdlPrendasTipo.eliminar(this.prendasTipo);
        }

        public void Buscar(string strCodigo)
        {
            if (strCodigo.Length > 0)
            {
                this.mdlPrendasTipo = new Model.clsMdlPrendasTipo();
                this.mdlPrendasTipo.buscar(strCodigo);                
                if (this.mdlPrendasTipo.bolResultado)
                {
                    this.prendasTipo.intId = Int32.Parse(this.mdlPrendasTipo.objResultado.Tables[0].Rows[0][0].ToString());
                    this.prendasTipo.strCodigo = this.mdlPrendasTipo.objResultado.Tables[0].Rows[0][1].ToString();
                    this.prendasTipo.strDescripcion = this.mdlPrendasTipo.objResultado.Tables[0].Rows[0][2].ToString();
                    this.prendasTipo.strCapturarGramaje = this.mdlPrendasTipo.objResultado.Tables[0].Rows[0][3].ToString();
                    this.prendasTipo.strCapturarEmailCelular = this.mdlPrendasTipo.objResultado.Tables[0].Rows[0][4].ToString();
                    this.prendasTipo.strCapturarSerieVehiculo = this.mdlPrendasTipo.objResultado.Tables[0].Rows[0][5].ToString();
                    this.prendasTipo.strEstatus = this.mdlPrendasTipo.objResultado.Tables[0].Rows[0][6].ToString();
                }
                else
                {
                    this.limpiarProps();
                    
                }
            }
            else
            {
                this.limpiarProps();                
            }
        }

        public void ValidarDescripcion(string strDescripcion)
        {
            this.mdlPrendasTipo = new Model.clsMdlPrendasTipo();
            this.mdlPrendasTipo.validarDescripcion(strDescripcion);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlPrendasTipo.bolResultado;
        }

        public void Paginacion()
        {
            this.mdlPrendasTipo = new Model.clsMdlPrendasTipo();
            this.mdlPrendasTipo.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlPrendasTipo.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlPrendasTipo.bolResultado; ;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlPrendasTipo.objResultado;

        }
        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de tipos de prendas";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = "descripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   "  codigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                   "  capturar_gramaje Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' ";
            frmBusqueda.lsBloquearCol = new List<string>()
            {
                    "prenda_tipo_id",
                    "codigo",
                    "descripcion",
                    "capturar_gramaje",
                    "capturar_imei_celular",
                    "capturar_serie_vehiculo",
                    "estatus"
            };
            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.prendasTipo.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.prendasTipo.strCodigo = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.prendasTipo.strDescripcion = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.prendasTipo.strCapturarGramaje = frmBusqueda.rowsIndex.Cells[3].Value.ToString();                    
                    this.prendasTipo.strCapturarEmailCelular = frmBusqueda.rowsIndex.Cells[4].Value.ToString();
                    this.prendasTipo.strCapturarSerieVehiculo = frmBusqueda.rowsIndex.Cells[5].Value.ToString();
                    this.prendasTipo.strEstatus = frmBusqueda.rowsIndex.Cells[6].Value.ToString();
                }
                catch (Exception e) { }
            }
        }

        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.prendasTipo.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.prendasTipo.strCodigo = this.objDataSetSingle.ItemArray[1].ToString();
                this.prendasTipo.strDescripcion = this.objDataSetSingle.ItemArray[2].ToString();
                this.prendasTipo.strCapturarGramaje = this.objDataSetSingle.ItemArray[3].ToString();
                this.prendasTipo.strCapturarEmailCelular = this.objDataSetSingle.ItemArray[4].ToString();
                this.prendasTipo.strCapturarSerieVehiculo = this.objDataSetSingle.ItemArray[5].ToString();
                this.prendasTipo.strEstatus = this.objDataSetSingle.ItemArray[6].ToString();
            }

        }

        public void limpiarProps()
        {
            this.prendasTipo.limpiarProps();
        }
    }
}