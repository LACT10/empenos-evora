﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlIdentificacionesTipo : Core.clsCore
    {
        public Model.clsMdlIdentificacionesTipo mdlIdentificacionesTipo;
        public Properties_Class.clsIdentificacionesTipo identificacionTipo;
        
        public clsCtrlIdentificacionesTipo()
        {
            this.identificacionTipo = new Properties_Class.clsIdentificacionesTipo();
        }

        public void Guardar()
        {
            this.mdlIdentificacionesTipo = new Model.clsMdlIdentificacionesTipo();
            this.identificacionTipo.agregarCero();
            this.mdlIdentificacionesTipo.guardar(this.identificacionTipo);
        }

        public void Eliminar()
        {
            if (this.identificacionTipo.intId == 0) return;
            this.mdlIdentificacionesTipo = new Model.clsMdlIdentificacionesTipo();
            //Hacer un llamado a la funcion para desactivar el registro
            this.identificacionTipo.cambioEstatus(this.identificacionTipo.dicEstatus["INACTIVO"]);
            this.mdlIdentificacionesTipo.eliminar(this.identificacionTipo);
        }

        public void Buscar(string strCodigo)
        {
            if (strCodigo.Length > 0)
            {
                this.mdlIdentificacionesTipo = new Model.clsMdlIdentificacionesTipo();
                this.mdlIdentificacionesTipo.buscar(strCodigo);

                if (this.mdlIdentificacionesTipo.bolResultado)
                {
                    this.identificacionTipo.intId = Int32.Parse(this.mdlIdentificacionesTipo.objResultado.Tables[0].Rows[0][0].ToString());
                    this.identificacionTipo.strCodigo = this.mdlIdentificacionesTipo.objResultado.Tables[0].Rows[0][1].ToString();
                    this.identificacionTipo.strDescripcion = this.mdlIdentificacionesTipo.objResultado.Tables[0].Rows[0][2].ToString();
                    this.identificacionTipo.strValidarCurp = this.mdlIdentificacionesTipo.objResultado.Tables[0].Rows[0][3].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }
        }

        public void ValidarDescripcion(string strDescripcion)
        {
            this.mdlIdentificacionesTipo = new Model.clsMdlIdentificacionesTipo();
            this.mdlIdentificacionesTipo.validarDescripcion(strDescripcion);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlIdentificacionesTipo.bolResultado;
        }
        public void Paginacion()
        {
            this.mdlIdentificacionesTipo = new Model.clsMdlIdentificacionesTipo();
            this.mdlIdentificacionesTipo.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlIdentificacionesTipo.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlIdentificacionesTipo.bolResultado; ;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlIdentificacionesTipo.objResultado;

        }
        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda tipos de identificaciones";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = "descripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   "  codigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                   "  valida_curp Like '%" + frmBusqueda.dicReemplazo["texto"] + "%'  ";
            frmBusqueda.lsBloquearCol = new List<string>()
            {
                    "identificacion_tipo_id",
                    "codigo",
                    "descripcion",
                    "valida_curp",
                    "estatus"
            };
            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.identificacionTipo.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.identificacionTipo.strCodigo = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.identificacionTipo.strDescripcion = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.identificacionTipo.strValidarCurp = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                    this.identificacionTipo.strEstatus = frmBusqueda.rowsIndex.Cells[4].Value.ToString();
                }
                catch (Exception e) { }
            }
        }

        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.identificacionTipo.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.identificacionTipo.strCodigo = this.objDataSetSingle.ItemArray[1].ToString();
                this.identificacionTipo.strDescripcion = this.objDataSetSingle.ItemArray[2].ToString();
                this.identificacionTipo.strValidarCurp = this.objDataSetSingle.ItemArray[3].ToString();
                this.identificacionTipo.strEstatus = this.objDataSetSingle.ItemArray[4].ToString();
            }

        }

        public void limpiarProps()
        {
            this.identificacionTipo.limpiarProps();
        }
    }
}