﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
   public class clsCtrlCajasUsuario : Core.clsCore
    {

        public Model.clsMdlCajasUsuario mdlCajasUsuario;
        public Properties_Class.clsCaja caja = new Properties_Class.clsCaja();
        
        public clsCtrlCajasUsuario() 
        { 
        
        }


        public void BuscarCodigo(int intUsuarioId, int intSucursal, string strCodigo)
        {
            if (!(strCodigo == ""))
            {
                this.mdlCajasUsuario = new Model.clsMdlCajasUsuario();


                this.mdlCajasUsuario.buscarCodigo(intUsuarioId, intSucursal, strCodigo);
                this.bolResultado = this.mdlCajasUsuario.bolResultado;
                if (this.mdlCajasUsuario.bolResultado)
                {
                    
                    this.caja.intId = Int32.Parse(this.mdlCajasUsuario.objResultado.Tables[0].Rows[0][0].ToString());
                    this.caja.strDescripcion = this.mdlCajasUsuario.objResultado.Tables[0].Rows[0][1].ToString();
                    this.caja.strCodigo = this.mdlCajasUsuario.objResultado.Tables[0].Rows[0][2].ToString();
                    
                }
                else
                {
                    //this.limpiarProps();
                }
            }
            else
            {
                //this.limpiarProps();

            }
        }
        
        public void PaginacionSucursalUsario(int intUsuarioId, int intSucursalId)
        {

            this.mdlCajasUsuario = new Model.clsMdlCajasUsuario();
            mdlCajasUsuario.paginacionSucursalUsario(intUsuarioId, intSucursalId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlCajasUsuario.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlCajasUsuario.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlCajasUsuario.objResultado;
        }
        public void Paginacion(int intUsuarioId, int intSucursalId) 
        {
            
            this.mdlCajasUsuario = new Model.clsMdlCajasUsuario();
            mdlCajasUsuario.paginacion(intUsuarioId, intSucursalId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlCajasUsuario.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlCajasUsuario.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlCajasUsuario.objResultado;
        }

        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de cajas";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = " descripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                   "  codigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' ";

            frmBusqueda.lsBloquearCol = new List<string>()
            {
                    "caja_id",
                    "codigo",
                    "descripcion"
                    

            };
            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {


                    this.caja.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.caja.strDescripcion = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.caja.strCodigo = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.caja.strEstatus = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                }
                catch (Exception e) { }
            }
        }



    }
}
