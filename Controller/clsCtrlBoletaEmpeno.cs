﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlBoletaEmpeno : Core.clsCore
    {

        public Properties_Class.clsBoletasEmpeno boletasEmpeno = new Properties_Class.clsBoletasEmpeno();
        public Model.clsMdlBoletaEmpeno mdlBoletaEmpeno;

        public clsCtrlBoletaEmpeno() {
            this.boletasEmpeno = new Properties_Class.clsBoletasEmpeno();

        
        }

        public void Guardar()
        {
            this.mdlBoletaEmpeno = new Model.clsMdlBoletaEmpeno();                        
            this.mdlBoletaEmpeno.guardar(this.boletasEmpeno);

        }

        public void Cancelar()
        {
            if (boletasEmpeno.intId == 0) return;
            this.mdlBoletaEmpeno = new Model.clsMdlBoletaEmpeno();
            //Hacer un llamado a la funcion para desactivar el registro
            this.boletasEmpeno.cambioEstatus(this.boletasEmpeno.dicEstatus["CANCELADO"]);
            this.mdlBoletaEmpeno.cancelar(this.boletasEmpeno);
            this.bolResultado = this.mdlBoletaEmpeno.bolResultado;
        }

        public void Reactivar()
        {
            if (boletasEmpeno.intId == 0) return;
            this.mdlBoletaEmpeno = new Model.clsMdlBoletaEmpeno();
            //Hacer un llamado a la funcion para desactivar el registro
            this.boletasEmpeno.cambioEstatus(this.boletasEmpeno.dicEstatus["ACTIVO"]);
            this.mdlBoletaEmpeno.reactivar(this.boletasEmpeno);
            this.bolResultado = this.mdlBoletaEmpeno.bolResultado;
        }


        public void Buscar(string strBoleta)
        {
            if (strBoleta.Length > 0)
            {
                this.mdlBoletaEmpeno = new Model.clsMdlBoletaEmpeno();
                this.mdlBoletaEmpeno.buscar(strBoleta);

                if (this.mdlBoletaEmpeno.bolResultado)
                {
                    this.boletasEmpeno.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][0].ToString());
                    this.boletasEmpeno.strFolio = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][1].ToString();
                    this.boletasEmpeno.strBoletaInicial = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][2].ToString();
                    this.boletasEmpeno.dtFechaBoleta = Convert.ToDateTime(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][3].ToString());
                    this.boletasEmpeno.strHora = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][4].ToString();
                    this.boletasEmpeno.dtFechaVencimiento = Convert.ToDateTime(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][5].ToString());
                    if (this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][6].ToString() != "") 
                    {
                        this.boletasEmpeno.intReferenciaId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][6].ToString());
                    }                    
                    this.boletasEmpeno.cliente.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][7].ToString());
                    this.boletasEmpeno.cliente.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][8].ToString();
                    this.boletasEmpeno.cliente.strNombre = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][9].ToString();
                    this.boletasEmpeno.empleado.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][10].ToString());
                    this.boletasEmpeno.empleado.strNombre = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][11].ToString();
                    this.boletasEmpeno.identificacionesTipo.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][12].ToString());
                    this.boletasEmpeno.identificacionesTipo.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][13].ToString();
                    this.boletasEmpeno.identificacionesTipo.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][14].ToString();
                    this.boletasEmpeno.prendaTipo.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][15].ToString());
                    this.boletasEmpeno.prendaTipo.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][16].ToString();
                    this.boletasEmpeno.prendaTipo.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][17].ToString();
                    this.boletasEmpeno.prendaTipo.strCapturarGramaje = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][18].ToString();
                    this.boletasEmpeno.sucursalPlazo.ftPlazo = float.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][19].ToString());
                    this.boletasEmpeno.subTipoPrenda.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][20].ToString());
                    this.boletasEmpeno.subTipoPrenda.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][21].ToString();
                    this.boletasEmpeno.subTipoPrenda.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][22].ToString();
                    if (this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][23].ToString() != "") 
                    {
                        this.boletasEmpeno.gramaje.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][23].ToString());
                        this.boletasEmpeno.gramaje.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][24].ToString();
                        this.boletasEmpeno.gramaje.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][25].ToString();
                        this.boletasEmpeno.sucursalGramaje.ftPreciotope = float.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][26].ToString());
                        this.boletasEmpeno.flKilataje = float.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][27].ToString());
                        this.boletasEmpeno.flGramos = float.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][28].ToString());
                    }
                    
                    this.boletasEmpeno.intCantidad = int.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][29].ToString());
                    this.boletasEmpeno.intPlazo = int.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][30].ToString());
                    this.boletasEmpeno.decPrestamo = decimal.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][31].ToString());                    
                    this.boletasEmpeno.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][32].ToString();
                    this.boletasEmpeno.strComentario = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][33].ToString();
                    this.boletasEmpeno.strFotografiaImagen = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][34].ToString();
                    this.boletasEmpeno.strEstatus = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][35].ToString();
                    this.boletasEmpeno.caja.intId = int.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][36].ToString());
                    this.boletasEmpeno.caja.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][37].ToString();
                    this.boletasEmpeno.caja.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][38].ToString();
                    this.boletasEmpeno.usuarioCreacion.strNombre = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][39].ToString();
                    this.boletasEmpeno.usuarioActulizacion.strNombre = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][40].ToString();
                    this.boletasEmpeno.decImporteAvaluo = decimal.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][41].ToString());
                    this.boletasEmpeno.strBoletaInicial = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][42].ToString();
                    this.boletasEmpeno.intBoletaInicialId = int.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][44].ToString());
                    this.boletasEmpeno.strMotivosCancelacion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][45].ToString();
                    this.boletasEmpeno.strPagoFolio = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][52].ToString();
                    this.boletasEmpeno.strPagoFecha = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][53].ToString(); 
                    this.boletasEmpeno.strBoletaRefrendo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][54].ToString();
                    this.boletasEmpeno.strBoletaRefrendada = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][55].ToString();
                    this.boletasEmpeno.strUsuarioEliminacion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][56].ToString();
                    this.boletasEmpeno.strFechaEliminacion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][57].ToString();
                    this.boletasEmpeno.decPPI = decimal.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][58].ToString());
                    this.boletasEmpeno.strIMEICelular = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][59].ToString();
                    this.boletasEmpeno.strTipoSerieVehiculo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][60].ToString();
                    this.boletasEmpeno.strNumeroSerieVehiculo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][61].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }


        }


        public void BuscarActivo(string strBoleta, string strSucursalId)
        {
            if (strBoleta.Length > 0)
            {
                this.mdlBoletaEmpeno = new Model.clsMdlBoletaEmpeno();
                this.mdlBoletaEmpeno.buscarActivo(strBoleta, strSucursalId);

                if (this.mdlBoletaEmpeno.bolResultado)
                {
                    this.boletasEmpeno.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][0].ToString());
                    this.boletasEmpeno.strFolio = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][1].ToString();
                    this.boletasEmpeno.strBoletaInicial = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][2].ToString();
                    this.boletasEmpeno.dtFechaBoleta = Convert.ToDateTime(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][3].ToString());
                    this.boletasEmpeno.strHora = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][4].ToString();
                    this.boletasEmpeno.dtFechaVencimiento = Convert.ToDateTime(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][5].ToString());
                    if (this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][6].ToString() != "")
                    {
                        this.boletasEmpeno.intReferenciaId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][6].ToString());
                    }
                    this.boletasEmpeno.cliente.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][7].ToString());
                    this.boletasEmpeno.cliente.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][8].ToString();
                    this.boletasEmpeno.cliente.strNombre = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][9].ToString();
                    this.boletasEmpeno.empleado.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][10].ToString());
                    this.boletasEmpeno.empleado.strNombre = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][11].ToString();
                    this.boletasEmpeno.identificacionesTipo.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][12].ToString());
                    this.boletasEmpeno.identificacionesTipo.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][13].ToString();
                    this.boletasEmpeno.identificacionesTipo.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][14].ToString();
                    this.boletasEmpeno.prendaTipo.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][15].ToString());
                    this.boletasEmpeno.prendaTipo.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][16].ToString();
                    this.boletasEmpeno.prendaTipo.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][17].ToString();
                    this.boletasEmpeno.prendaTipo.strCapturarGramaje = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][18].ToString();
                    this.boletasEmpeno.sucursalPlazo.ftPlazo = float.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][19].ToString());
                    this.boletasEmpeno.subTipoPrenda.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][20].ToString());
                    this.boletasEmpeno.subTipoPrenda.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][21].ToString();
                    this.boletasEmpeno.subTipoPrenda.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][22].ToString();
                    if (this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][23].ToString() != "")
                    {
                        this.boletasEmpeno.gramaje.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][23].ToString());
                        this.boletasEmpeno.gramaje.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][24].ToString();
                        this.boletasEmpeno.gramaje.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][25].ToString();
                        this.boletasEmpeno.sucursalGramaje.ftPreciotope = float.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][26].ToString());
                        this.boletasEmpeno.flKilataje = float.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][27].ToString());
                        this.boletasEmpeno.flGramos = float.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][28].ToString());
                    }

                    this.boletasEmpeno.intCantidad = int.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][29].ToString());
                    this.boletasEmpeno.intPlazo = int.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][30].ToString());
                    this.boletasEmpeno.decPrestamo = decimal.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][31].ToString());
                    this.boletasEmpeno.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][32].ToString();
                    this.boletasEmpeno.strComentario = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][33].ToString();
                    this.boletasEmpeno.strFotografiaImagen = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][34].ToString();
                    this.boletasEmpeno.strEstatus = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][35].ToString();
                    this.boletasEmpeno.caja.intId = int.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][36].ToString());
                    this.boletasEmpeno.caja.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][37].ToString();
                    this.boletasEmpeno.caja.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][38].ToString();
                    this.boletasEmpeno.usuarioCreacion.strNombre = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][39].ToString();
                    this.boletasEmpeno.usuarioActulizacion.strNombre = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][40].ToString();
                    this.boletasEmpeno.decImporteAvaluo = decimal.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][41].ToString());
                    this.boletasEmpeno.strBoletaInicial = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][42].ToString();
                    this.boletasEmpeno.intBoletaInicialId = int.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][44].ToString());
                    this.boletasEmpeno.strMotivosCancelacion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][45].ToString();
                    this.boletasEmpeno.strPagoFolio = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][52].ToString();
                    this.boletasEmpeno.strPagoFecha = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][53].ToString();
                    this.boletasEmpeno.strBoletaRefrendo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][54].ToString();
                    this.boletasEmpeno.strBoletaRefrendada = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][55].ToString();
                    this.boletasEmpeno.strUsuarioEliminacion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][56].ToString();
                    this.boletasEmpeno.strFechaEliminacion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][57].ToString();
                    this.boletasEmpeno.decPPI = decimal.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][58].ToString());
                    this.boletasEmpeno.strIMEICelular = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][59].ToString();
                    this.boletasEmpeno.strTipoSerieVehiculo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][60].ToString();
                    this.boletasEmpeno.strNumeroSerieVehiculo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][61].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }


        }

        /*Metado que se utilzar para obtern boletas estatus con ACTIVO y ADJUDICADA.  El metado se utlizar en la vista de Liquidadion interna boleta*/
        public void BuscarActivoAdjudicada(string strBoleta, string strSucursalId)
        {
            if (strBoleta.Length > 0)
            {
                this.mdlBoletaEmpeno = new Model.clsMdlBoletaEmpeno();
                this.mdlBoletaEmpeno.buscarActivoAdjudicada(strBoleta, strSucursalId);

                if (this.mdlBoletaEmpeno.bolResultado)
                {
                    this.boletasEmpeno.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][0].ToString());
                    this.boletasEmpeno.strFolio = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][1].ToString();
                    this.boletasEmpeno.strBoletaInicial = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][2].ToString();
                    this.boletasEmpeno.dtFechaBoleta = Convert.ToDateTime(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][3].ToString());
                    this.boletasEmpeno.strHora = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][4].ToString();
                    this.boletasEmpeno.dtFechaVencimiento = Convert.ToDateTime(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][5].ToString());
                    if (this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][6].ToString() != "")
                    {
                        this.boletasEmpeno.intReferenciaId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][6].ToString());
                    }
                    this.boletasEmpeno.cliente.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][7].ToString());
                    this.boletasEmpeno.cliente.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][8].ToString();
                    this.boletasEmpeno.cliente.strNombre = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][9].ToString();
                    this.boletasEmpeno.empleado.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][10].ToString());
                    this.boletasEmpeno.empleado.strNombre = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][11].ToString();
                    this.boletasEmpeno.identificacionesTipo.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][12].ToString());
                    this.boletasEmpeno.identificacionesTipo.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][13].ToString();
                    this.boletasEmpeno.identificacionesTipo.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][14].ToString();
                    this.boletasEmpeno.prendaTipo.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][15].ToString());
                    this.boletasEmpeno.prendaTipo.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][16].ToString();
                    this.boletasEmpeno.prendaTipo.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][17].ToString();
                    this.boletasEmpeno.prendaTipo.strCapturarGramaje = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][18].ToString();
                    this.boletasEmpeno.sucursalPlazo.ftPlazo = float.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][19].ToString());
                    this.boletasEmpeno.subTipoPrenda.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][20].ToString());
                    this.boletasEmpeno.subTipoPrenda.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][21].ToString();
                    this.boletasEmpeno.subTipoPrenda.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][22].ToString();
                    if (this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][23].ToString() != "")
                    {
                        this.boletasEmpeno.gramaje.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][23].ToString());
                        this.boletasEmpeno.gramaje.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][24].ToString();
                        this.boletasEmpeno.gramaje.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][25].ToString();
                        this.boletasEmpeno.sucursalGramaje.ftPreciotope = float.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][26].ToString());
                        this.boletasEmpeno.flKilataje = float.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][27].ToString());
                        this.boletasEmpeno.flGramos = float.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][28].ToString());
                    }

                    this.boletasEmpeno.intCantidad = int.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][29].ToString());
                    this.boletasEmpeno.intPlazo = int.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][30].ToString());
                    this.boletasEmpeno.decPrestamo = decimal.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][31].ToString());
                    this.boletasEmpeno.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][32].ToString();
                    this.boletasEmpeno.strComentario = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][33].ToString();
                    this.boletasEmpeno.strFotografiaImagen = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][34].ToString();
                    this.boletasEmpeno.strEstatus = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][35].ToString();
                    this.boletasEmpeno.caja.intId = int.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][36].ToString());
                    this.boletasEmpeno.caja.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][37].ToString();
                    this.boletasEmpeno.caja.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][38].ToString();
                    this.boletasEmpeno.usuarioCreacion.strNombre = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][39].ToString();
                    this.boletasEmpeno.usuarioActulizacion.strNombre = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][40].ToString();
                    this.boletasEmpeno.decImporteAvaluo = decimal.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][41].ToString());
                    this.boletasEmpeno.strBoletaInicial = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][42].ToString();
                    this.boletasEmpeno.intBoletaInicialId = int.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][44].ToString());
                    this.boletasEmpeno.strMotivosCancelacion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][45].ToString();
                    this.boletasEmpeno.strPagoFolio = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][52].ToString();
                    this.boletasEmpeno.strPagoFecha = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][53].ToString();
                    this.boletasEmpeno.strBoletaRefrendo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][54].ToString();
                    this.boletasEmpeno.strBoletaRefrendada = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][55].ToString();
                    this.boletasEmpeno.strUsuarioEliminacion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][56].ToString();
                    this.boletasEmpeno.strFechaEliminacion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][57].ToString();                   
                    this.boletasEmpeno.strIMEICelular = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][58].ToString();
                    this.boletasEmpeno.strTipoSerieVehiculo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][59].ToString();
                    this.boletasEmpeno.strNumeroSerieVehiculo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][60].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }


        }

        /*Metado que se utilzar para obtern boletas estatus con ACTIVO y ADJUDICADA. En caso que tiene un id de cliente entonces se obtner las boletas de dicha cliente. El metado se utlizar en la vista de Liquidadion interna boleta*/
        public void BuscarActivoAdjudicadaCliente(string strBoleta, string strSucursalId, string strClientId)
        {
            if (strBoleta.Length > 0)
            {
                this.mdlBoletaEmpeno = new Model.clsMdlBoletaEmpeno();
                this.mdlBoletaEmpeno.buscarActivoAdjudicadaCliente(strBoleta, strSucursalId, strClientId);

                if (this.mdlBoletaEmpeno.bolResultado)
                {
                    this.boletasEmpeno.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][0].ToString());
                    this.boletasEmpeno.strFolio = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][1].ToString();
                    this.boletasEmpeno.strBoletaInicial = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][2].ToString();
                    this.boletasEmpeno.dtFechaBoleta = Convert.ToDateTime(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][3].ToString());
                    this.boletasEmpeno.strHora = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][4].ToString();
                    this.boletasEmpeno.dtFechaVencimiento = Convert.ToDateTime(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][5].ToString());
                    if (this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][6].ToString() != "")
                    {
                        this.boletasEmpeno.intReferenciaId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][6].ToString());
                    }
                    this.boletasEmpeno.cliente.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][7].ToString());
                    this.boletasEmpeno.cliente.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][8].ToString();
                    this.boletasEmpeno.cliente.strNombre = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][9].ToString();
                    this.boletasEmpeno.empleado.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][10].ToString());
                    this.boletasEmpeno.empleado.strNombre = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][11].ToString();
                    this.boletasEmpeno.identificacionesTipo.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][12].ToString());
                    this.boletasEmpeno.identificacionesTipo.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][13].ToString();
                    this.boletasEmpeno.identificacionesTipo.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][14].ToString();
                    this.boletasEmpeno.prendaTipo.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][15].ToString());
                    this.boletasEmpeno.prendaTipo.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][16].ToString();
                    this.boletasEmpeno.prendaTipo.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][17].ToString();
                    this.boletasEmpeno.prendaTipo.strCapturarGramaje = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][18].ToString();
                    this.boletasEmpeno.sucursalPlazo.ftPlazo = float.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][19].ToString());
                    this.boletasEmpeno.subTipoPrenda.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][20].ToString());
                    this.boletasEmpeno.subTipoPrenda.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][21].ToString();
                    this.boletasEmpeno.subTipoPrenda.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][22].ToString();
                    if (this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][23].ToString() != "")
                    {
                        this.boletasEmpeno.gramaje.intId = Int32.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][23].ToString());
                        this.boletasEmpeno.gramaje.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][24].ToString();
                        this.boletasEmpeno.gramaje.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][25].ToString();
                        this.boletasEmpeno.sucursalGramaje.ftPreciotope = float.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][26].ToString());
                        this.boletasEmpeno.flKilataje = float.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][27].ToString());
                        this.boletasEmpeno.flGramos = float.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][28].ToString());
                    }

                    this.boletasEmpeno.intCantidad = int.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][29].ToString());
                    this.boletasEmpeno.intPlazo = int.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][30].ToString());
                    this.boletasEmpeno.decPrestamo = decimal.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][31].ToString());
                    this.boletasEmpeno.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][32].ToString();
                    this.boletasEmpeno.strComentario = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][33].ToString();
                    this.boletasEmpeno.strFotografiaImagen = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][34].ToString();
                    this.boletasEmpeno.strEstatus = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][35].ToString();
                    this.boletasEmpeno.caja.intId = int.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][36].ToString());
                    this.boletasEmpeno.caja.strCodigo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][37].ToString();
                    this.boletasEmpeno.caja.strDescripcion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][38].ToString();
                    this.boletasEmpeno.usuarioCreacion.strNombre = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][39].ToString();
                    this.boletasEmpeno.usuarioActulizacion.strNombre = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][40].ToString();
                    this.boletasEmpeno.decImporteAvaluo = decimal.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][41].ToString());
                    this.boletasEmpeno.strBoletaInicial = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][42].ToString();
                    this.boletasEmpeno.intBoletaInicialId = int.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][44].ToString());
                    this.boletasEmpeno.strMotivosCancelacion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][45].ToString();
                    this.boletasEmpeno.strPagoFolio = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][52].ToString();
                    this.boletasEmpeno.strPagoFecha = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][53].ToString();
                    this.boletasEmpeno.strBoletaRefrendo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][54].ToString();
                    this.boletasEmpeno.strBoletaRefrendada = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][55].ToString();
                    this.boletasEmpeno.strUsuarioEliminacion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][56].ToString();
                    this.boletasEmpeno.strFechaEliminacion = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][57].ToString();
                    this.boletasEmpeno.strIMEICelular = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][58].ToString();
                    this.boletasEmpeno.strTipoSerieVehiculo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][59].ToString();
                    this.boletasEmpeno.strNumeroSerieVehiculo = this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][60].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }


        }



        public void GetLastId() 
        {
            this.mdlBoletaEmpeno = new Model.clsMdlBoletaEmpeno();
            this.mdlBoletaEmpeno.getLastId();

            if (this.mdlBoletaEmpeno.bolResultado)
            {
                this.boletasEmpeno.intId = int.Parse(this.mdlBoletaEmpeno.objResultado.Tables[0].Rows[0][0].ToString());
            }
        }



        public void PaginacionCliente(int intClienteId)
        {
            this.mdlBoletaEmpeno = new Model.clsMdlBoletaEmpeno();
            this.mdlBoletaEmpeno.paginacionCliente(intClienteId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlBoletaEmpeno.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlBoletaEmpeno.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlBoletaEmpeno.objResultado;

        }



        public void Paginacion(int intSucursalId)
        {
            this.mdlBoletaEmpeno = new Model.clsMdlBoletaEmpeno();
            this.mdlBoletaEmpeno.paginacion(intSucursalId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlBoletaEmpeno.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlBoletaEmpeno.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlBoletaEmpeno.objResultado;

        }

        public void CargarFrmBusquedaRangoFecha()
        {
            frmBusquedaRangoFecha frmBusquedaRangoFecha = new frmBusquedaRangoFecha();
            frmBusquedaRangoFecha.Text = "Historial de boletas de empeño";
            frmBusquedaRangoFecha.strTipoFormulario = frmBusquedaRangoFecha.dicTipoFormulario["Simple"];
            frmBusquedaRangoFecha.strQuery = "( folio Like '%" + frmBusquedaRangoFecha.dicReemplazo["texto"] + "%' OR" +
                                    "  nombre Like '%" + frmBusquedaRangoFecha.dicReemplazo["texto"] + "%' OR" +
                                    "  estatus Like '%" + frmBusquedaRangoFecha.dicReemplazo["texto"] + "%') AND (" +
                                    "  fecha_busqueda  >= #" + frmBusquedaRangoFecha.dicReemplazo["fecha"] + "# AND " +
                                    "  fecha_busqueda <= #" + frmBusquedaRangoFecha.dicReemplazo["fecha2"] + "#)";
           


            frmBusquedaRangoFecha.lsBloquearCol = new List<string>()
            {
                  "folio" ,
                  "nombre" ,
                  "fecha_busqueda",
                  "estatus" 

            };

            frmBusquedaRangoFecha.objDataSet = this.objDataSet;
            frmBusquedaRangoFecha.ShowDialog();

            
        }

        public void CargarFrmBusquedaRangoFechaBoleta(Properties_Class.clsCliente cliente)
        {
            frmBusquedaRangoFechaBoleta frmBusquedaRangoFecha = new frmBusquedaRangoFechaBoleta();
            frmBusquedaRangoFecha.Text = "Historial de boletas de empeño";
            frmBusquedaRangoFecha.strTipoFormulario = frmBusquedaRangoFecha.dicTipoFormulario["Simple"];
            frmBusquedaRangoFecha.strQuery = "( folio Like '%" + frmBusquedaRangoFecha.dicReemplazo["texto"] + "%' OR" +
                                    "  nombre Like '%" + frmBusquedaRangoFecha.dicReemplazo["texto"] + "%' OR" +
                                    "  estatus Like '%" + frmBusquedaRangoFecha.dicReemplazo["texto"] + "%') AND (" +
                                    "  fecha_busqueda  >= #" + frmBusquedaRangoFecha.dicReemplazo["fecha"] + "# AND " +
                                    "  fecha_busqueda <= #" + frmBusquedaRangoFecha.dicReemplazo["fecha2"] + "#)";



            frmBusquedaRangoFecha.lsBloquearCol = new List<string>()
            {
                  "folio" ,
                  "nombre" ,
                  "fecha_busqueda",
                  "estatus"

            };

            frmBusquedaRangoFecha.objDataSet = this.objDataSet;
            frmBusquedaRangoFecha.cliente = cliente;
            frmBusquedaRangoFecha.ShowDialog();


        }

        public void CargarFrmBusqueda()
        {
            frmBusquedaRangoFecha frmBusqueda = new frmBusquedaRangoFecha();
            frmBusqueda.Text = "Búsqueda de boletas de empeño";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = " (folio Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   "  nombre_cliente Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   "  estatus Like '%" + frmBusqueda.dicReemplazo["texto"] + "%') AND (" +
                                    "  fecha_busqueda  >= #" + frmBusqueda.dicReemplazo["fecha"] + "# AND " +
                                    "  fecha_busqueda <= #" + frmBusqueda.dicReemplazo["fecha2"] + "#)";
            frmBusqueda.lsBloquearCol = new List<string>()
            {
               "boleta_empeno_id",
                "folio",
                "fecha_busqueda",
                "boleta_inicial",
                "boleta_inicial1",
                "boleta_inicial_id",
                "fecha",
                "hora",
                "vencimiento",
                "boleta_refrendo_id",
                "pago_refrendo_id",
                "cliente_id",
                "usuario_id",
                "codigo_cliente",
                "nombre_cliente",
                "nombre_empleado",
                "identificacion_tipo_id",
                "codigo_identificacion",
                "descripcion_identificacion",
                "prenda_tipo_id",
                "codigo_prenda_tipo",
                "descripcion_prenda_tipo",
                "capturar_gramaje",
                "plazo_tope",
                "prenda_subtipo_id",
                "codigo_prenda_subtipo",
                "descripcion_prenda_subtipo",
                "gramaje_id",
                "codigo_gramaje",
                "descripcion_gramaje",
                "precio_tope",
                "kilataje",
                "gramos",
                "cantidad",
                "numero_plazos",
                "importe",
                "descripcion",
                "comentario",
                "fotografia_imagen",
                "estatus",
                "caja_id",
                "codigo_caja",
                "nombre_usuario_creacion",
                "nombre_usuario_actualizacion",
                "descripcion_caja",
                "importe_avaluo",
                "pago_folio",
                "pago_fecha",
                "boleta_refrendo_folio",
                "boleta_refrendada_folio",
                "motivo_cancelacion",
                "nombre_empleado_eliminacion",
                "fecha_eliminacion",
                "ppi",
                "imei_celular", 
                "tipo_serie_vehiculo", 
                "numero_serie_vehiculo"

            };

            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.boletasEmpeno.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.boletasEmpeno.strFolio = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.boletasEmpeno.strBoletaInicial = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.boletasEmpeno.dtFechaBoleta = Convert.ToDateTime(frmBusqueda.rowsIndex.Cells[3].Value.ToString());
                    this.boletasEmpeno.strHora = frmBusqueda.rowsIndex.Cells[4].Value.ToString();
                    this.boletasEmpeno.dtFechaVencimiento = Convert.ToDateTime(frmBusqueda.rowsIndex.Cells[5].Value.ToString());
                    if (frmBusqueda.rowsIndex.Cells[6].Value.ToString() != "")
                    {
                        this.boletasEmpeno.intReferenciaId = Int32.Parse(frmBusqueda.rowsIndex.Cells[6].Value.ToString());
                    }                    
                    this.boletasEmpeno.cliente.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[7].Value.ToString());
                    this.boletasEmpeno.cliente.strCodigo = frmBusqueda.rowsIndex.Cells[8].Value.ToString();
                    this.boletasEmpeno.cliente.strNombre = frmBusqueda.rowsIndex.Cells[9].Value.ToString();
                    this.boletasEmpeno.empleado.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[10].Value.ToString());
                    this.boletasEmpeno.empleado.strNombre = frmBusqueda.rowsIndex.Cells[11].Value.ToString();
                    this.boletasEmpeno.identificacionesTipo.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[12].Value.ToString());
                    this.boletasEmpeno.identificacionesTipo.strCodigo = frmBusqueda.rowsIndex.Cells[13].Value.ToString();
                    this.boletasEmpeno.identificacionesTipo.strDescripcion = frmBusqueda.rowsIndex.Cells[14].Value.ToString();
                    this.boletasEmpeno.prendaTipo.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[15].Value.ToString());
                    this.boletasEmpeno.prendaTipo.strCodigo = frmBusqueda.rowsIndex.Cells[16].Value.ToString();
                    this.boletasEmpeno.prendaTipo.strDescripcion = frmBusqueda.rowsIndex.Cells[17].Value.ToString();
                    this.boletasEmpeno.prendaTipo.strCapturarGramaje = frmBusqueda.rowsIndex.Cells[18].Value.ToString();
                    this.boletasEmpeno.sucursalPlazo.ftPlazo = float.Parse(frmBusqueda.rowsIndex.Cells[19].Value.ToString());
                    this.boletasEmpeno.subTipoPrenda.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[20].Value.ToString());
                    this.boletasEmpeno.subTipoPrenda.strCodigo = frmBusqueda.rowsIndex.Cells[21].Value.ToString();
                    this.boletasEmpeno.subTipoPrenda.strDescripcion = frmBusqueda.rowsIndex.Cells[22].Value.ToString();
                    //Si existe id de gramaje 
                    if (frmBusqueda.rowsIndex.Cells[23].Value.ToString() != "") 
                    {
                        this.boletasEmpeno.gramaje.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[23].Value.ToString());
                        this.boletasEmpeno.gramaje.strCodigo = frmBusqueda.rowsIndex.Cells[24].Value.ToString();
                        this.boletasEmpeno.gramaje.strDescripcion = frmBusqueda.rowsIndex.Cells[25].Value.ToString();
                        this.boletasEmpeno.sucursalGramaje.ftPreciotope = float.Parse(frmBusqueda.rowsIndex.Cells[26].Value.ToString());
                        this.boletasEmpeno.flKilataje = float.Parse(frmBusqueda.rowsIndex.Cells[27].Value.ToString());
                        this.boletasEmpeno.flGramos = float.Parse(frmBusqueda.rowsIndex.Cells[28].Value.ToString());
                    }
                   
                    this.boletasEmpeno.intCantidad = int.Parse(frmBusqueda.rowsIndex.Cells[29].Value.ToString());
                    this.boletasEmpeno.intPlazo = int.Parse(frmBusqueda.rowsIndex.Cells[30].Value.ToString());
                    this.boletasEmpeno.decPrestamo = decimal.Parse(frmBusqueda.rowsIndex.Cells[31].Value.ToString());
                    this.boletasEmpeno.strDescripcion = frmBusqueda.rowsIndex.Cells[32].Value.ToString();
                    this.boletasEmpeno.strComentario = frmBusqueda.rowsIndex.Cells[33].Value.ToString();
                    this.boletasEmpeno.strFotografiaImagen = frmBusqueda.rowsIndex.Cells[34].Value.ToString();
                    this.boletasEmpeno.strEstatus = frmBusqueda.rowsIndex.Cells[35].Value.ToString();
                    this.boletasEmpeno.caja.intId = int.Parse(frmBusqueda.rowsIndex.Cells[36].Value.ToString());
                    this.boletasEmpeno.caja.strCodigo = frmBusqueda.rowsIndex.Cells[37].Value.ToString();
                    this.boletasEmpeno.caja.strDescripcion = frmBusqueda.rowsIndex.Cells[38].Value.ToString();
                    this.boletasEmpeno.usuarioCreacion.strNombre = frmBusqueda.rowsIndex.Cells[39].Value.ToString();
                    this.boletasEmpeno.usuarioActulizacion.strNombre = frmBusqueda.rowsIndex.Cells[40].Value.ToString();
                    this.boletasEmpeno.decImporteAvaluo = decimal.Parse(frmBusqueda.rowsIndex.Cells[41].Value.ToString());
                    this.boletasEmpeno.strBoletaInicial = frmBusqueda.rowsIndex.Cells[42].Value.ToString();

                    this.boletasEmpeno.strMotivosCancelacion = frmBusqueda.rowsIndex.Cells[45].Value.ToString();
                    this.boletasEmpeno.strPagoFolio = frmBusqueda.rowsIndex.Cells[52].Value.ToString();
                    this.boletasEmpeno.strPagoFecha = frmBusqueda.rowsIndex.Cells[53].Value.ToString();
                    this.boletasEmpeno.strBoletaRefrendo = frmBusqueda.rowsIndex.Cells[54].Value.ToString();
                    this.boletasEmpeno.strBoletaRefrendada = frmBusqueda.rowsIndex.Cells[55].Value.ToString();
                    this.boletasEmpeno.strUsuarioEliminacion = frmBusqueda.rowsIndex.Cells[56].Value.ToString();
                    this.boletasEmpeno.strFechaEliminacion = frmBusqueda.rowsIndex.Cells[57].Value.ToString();
                    this.boletasEmpeno.decPPI = decimal.Parse(frmBusqueda.rowsIndex.Cells[58].Value.ToString());
                    this.boletasEmpeno.strIMEICelular = frmBusqueda.rowsIndex.Cells[59].Value.ToString();
                    this.boletasEmpeno.strTipoSerieVehiculo = frmBusqueda.rowsIndex.Cells[60].Value.ToString();
                    this.boletasEmpeno.strNumeroSerieVehiculo = frmBusqueda.rowsIndex.Cells[61].Value.ToString();
                }
                catch (Exception e) { }
            }
        }



        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.boletasEmpeno.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.boletasEmpeno.strFolio = this.objDataSetSingle.ItemArray[1].ToString();
                this.boletasEmpeno.strBoletaInicial = this.objDataSetSingle.ItemArray[2].ToString();
                this.boletasEmpeno.dtFechaBoleta = Convert.ToDateTime(this.objDataSetSingle.ItemArray[3].ToString());
                this.boletasEmpeno.strHora = this.objDataSetSingle.ItemArray[4].ToString();
                this.boletasEmpeno.dtFechaVencimiento = Convert.ToDateTime(this.objDataSetSingle.ItemArray[5].ToString());
                if (this.objDataSetSingle.ItemArray[6].ToString() != "")
                {
                    this.boletasEmpeno.intReferenciaId = Int32.Parse(this.objDataSetSingle.ItemArray[6].ToString());
                }                
                this.boletasEmpeno.cliente.intId = Int32.Parse(this.objDataSetSingle.ItemArray[7].ToString());
                this.boletasEmpeno.cliente.strCodigo = this.objDataSetSingle.ItemArray[8].ToString();
                this.boletasEmpeno.cliente.strNombre = this.objDataSetSingle.ItemArray[9].ToString();
                this.boletasEmpeno.empleado.intId = Int32.Parse(this.objDataSetSingle.ItemArray[10].ToString());
                this.boletasEmpeno.empleado.strNombre = this.objDataSetSingle.ItemArray[11].ToString();
                this.boletasEmpeno.identificacionesTipo.intId = Int32.Parse(this.objDataSetSingle.ItemArray[12].ToString());
                this.boletasEmpeno.identificacionesTipo.strCodigo = this.objDataSetSingle.ItemArray[13].ToString();
                this.boletasEmpeno.identificacionesTipo.strDescripcion = this.objDataSetSingle.ItemArray[14].ToString();
                this.boletasEmpeno.prendaTipo.intId = Int32.Parse(this.objDataSetSingle.ItemArray[15].ToString());
                this.boletasEmpeno.prendaTipo.strCodigo = this.objDataSetSingle.ItemArray[16].ToString();
                this.boletasEmpeno.prendaTipo.strDescripcion = this.objDataSetSingle.ItemArray[17].ToString();
                this.boletasEmpeno.prendaTipo.strCapturarGramaje = this.objDataSetSingle.ItemArray[18].ToString();
                this.boletasEmpeno.sucursalPlazo.ftPlazo = float.Parse(this.objDataSetSingle.ItemArray[19].ToString());
                this.boletasEmpeno.subTipoPrenda.intId = Int32.Parse(this.objDataSetSingle.ItemArray[20].ToString());
                this.boletasEmpeno.subTipoPrenda.strCodigo = this.objDataSetSingle.ItemArray[21].ToString();
                this.boletasEmpeno.subTipoPrenda.strDescripcion = this.objDataSetSingle.ItemArray[22].ToString();
                //Si existe id de gramaje 
                if (this.objDataSetSingle.ItemArray[23].ToString() != "") 
                {
                    this.boletasEmpeno.gramaje.intId = Int32.Parse(this.objDataSetSingle.ItemArray[23].ToString());
                    this.boletasEmpeno.gramaje.strCodigo = this.objDataSetSingle.ItemArray[24].ToString();
                    this.boletasEmpeno.gramaje.strDescripcion = this.objDataSetSingle.ItemArray[25].ToString();
                    this.boletasEmpeno.sucursalGramaje.ftPreciotope = float.Parse(this.objDataSetSingle.ItemArray[26].ToString());
                    this.boletasEmpeno.flKilataje = float.Parse(this.objDataSetSingle.ItemArray[27].ToString());
                    this.boletasEmpeno.flGramos = float.Parse(this.objDataSetSingle.ItemArray[28].ToString());
                }
                
                this.boletasEmpeno.intCantidad = int.Parse(this.objDataSetSingle.ItemArray[29].ToString());
                this.boletasEmpeno.intPlazo = int.Parse(this.objDataSetSingle.ItemArray[30].ToString());
                this.boletasEmpeno.decPrestamo = decimal.Parse(this.objDataSetSingle.ItemArray[31].ToString());
                this.boletasEmpeno.strDescripcion = this.objDataSetSingle.ItemArray[32].ToString();
                this.boletasEmpeno.strComentario = this.objDataSetSingle.ItemArray[33].ToString();
                this.boletasEmpeno.strFotografiaImagen = this.objDataSetSingle.ItemArray[34].ToString();
                this.boletasEmpeno.strEstatus = this.objDataSetSingle.ItemArray[35].ToString();
                this.boletasEmpeno.caja.intId = int.Parse(this.objDataSetSingle.ItemArray[36].ToString());
                this.boletasEmpeno.caja.strCodigo = this.objDataSetSingle.ItemArray[37].ToString();
                this.boletasEmpeno.caja.strDescripcion = this.objDataSetSingle.ItemArray[38].ToString();
                this.boletasEmpeno.usuarioCreacion.strNombre = this.objDataSetSingle.ItemArray[39].ToString();
                this.boletasEmpeno.usuarioActulizacion.strNombre = this.objDataSetSingle.ItemArray[40].ToString();
                this.boletasEmpeno.decImporteAvaluo = decimal.Parse(this.objDataSetSingle.ItemArray[41].ToString());
                this.boletasEmpeno.strBoletaInicial = this.objDataSetSingle.ItemArray[42].ToString();
                this.boletasEmpeno.intBoletaInicialId = int.Parse(this.objDataSetSingle.ItemArray[44].ToString());
                this.boletasEmpeno.strMotivosCancelacion = this.objDataSetSingle.ItemArray[45].ToString();
                this.boletasEmpeno.strPagoFolio = this.objDataSetSingle.ItemArray[52].ToString();
                this.boletasEmpeno.strPagoFecha = this.objDataSetSingle.ItemArray[53].ToString();
                this.boletasEmpeno.strBoletaRefrendo = this.objDataSetSingle.ItemArray[54].ToString();
                this.boletasEmpeno.strBoletaRefrendada = this.objDataSetSingle.ItemArray[55].ToString();
                this.boletasEmpeno.strUsuarioEliminacion = this.objDataSetSingle.ItemArray[56].ToString();
                this.boletasEmpeno.strFechaEliminacion = this.objDataSetSingle.ItemArray[57].ToString();
                this.boletasEmpeno.decPPI = decimal.Parse(this.objDataSetSingle.ItemArray[58].ToString());
                this.boletasEmpeno.strIMEICelular = this.objDataSetSingle.ItemArray[59].ToString();
                this.boletasEmpeno.strTipoSerieVehiculo = this.objDataSetSingle.ItemArray[60].ToString();
                this.boletasEmpeno.strNumeroSerieVehiculo = this.objDataSetSingle.ItemArray[61].ToString();



            }            

        }

        public void limpiarProps()
        {
            this.boletasEmpeno.limpiarProps();
        }




    }
}
