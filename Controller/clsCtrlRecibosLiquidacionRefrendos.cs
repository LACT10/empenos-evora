﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlRecibosLiquidacionRefrendos : Core.clsCore
    {
        public Properties_Class.clsRecibosLiquidacionRefrendos refrendos = new Properties_Class.clsRecibosLiquidacionRefrendos();
        public Model.clsMdlRecibosLiquidacionRefrendos mdlRefrendos;

        public clsCtrlRecibosLiquidacionRefrendos()
        {
            this.refrendos = new Properties_Class.clsRecibosLiquidacionRefrendos();


        }

        public void Guardar()
        {
            this.mdlRefrendos = new Model.clsMdlRecibosLiquidacionRefrendos();
            this.mdlRefrendos.guardar(this.refrendos);
            this.bolResultado = this.mdlRefrendos.bolResultado;
        }

        public void Cancelar()
        {
            if (refrendos.intId == 0) return;
            this.mdlRefrendos = new Model.clsMdlRecibosLiquidacionRefrendos();
            //Hacer un llamado a la funcion para desactivar el registro
            this.refrendos.cambioEstatus(this.refrendos.dicEstatus["CANCELADO"]);
            this.mdlRefrendos.cancelar(this.refrendos);
            this.bolResultado = this.mdlRefrendos.bolResultado;
        }


        public void Buscar(string strFolio)
        {
            if (strFolio.Length > 0)
            {
                this.mdlRefrendos = new Model.clsMdlRecibosLiquidacionRefrendos();
                this.mdlRefrendos.buscar(strFolio);

                if (this.mdlRefrendos.bolResultado)
                {
                    this.refrendos.intId = Int32.Parse(this.mdlRefrendos.objResultado.Tables[0].Rows[0][0].ToString());
                    this.refrendos.strFolio = this.mdlRefrendos.objResultado.Tables[0].Rows[0][1].ToString();
                    this.refrendos.dtFechaPago = Convert.ToDateTime(this.mdlRefrendos.objResultado.Tables[0].Rows[0][2].ToString());
                    this.refrendos.strHora = this.mdlRefrendos.objResultado.Tables[0].Rows[0][3].ToString();
                    this.refrendos.cliente.intId = Int32.Parse(this.mdlRefrendos.objResultado.Tables[0].Rows[0][4].ToString());
                    this.refrendos.cliente.strCodigo = this.mdlRefrendos.objResultado.Tables[0].Rows[0][5].ToString();
                    this.refrendos.cliente.strNombre = this.mdlRefrendos.objResultado.Tables[0].Rows[0][6].ToString();
                    this.refrendos.empleado.intId = Int32.Parse(this.mdlRefrendos.objResultado.Tables[0].Rows[0][7].ToString());
                    this.refrendos.empleado.strNombre = this.mdlRefrendos.objResultado.Tables[0].Rows[0][8].ToString();
                    this.refrendos.caja.intId = int.Parse(this.mdlRefrendos.objResultado.Tables[0].Rows[0][9].ToString());
                    this.refrendos.caja.strCodigo = this.mdlRefrendos.objResultado.Tables[0].Rows[0][10].ToString();
                    this.refrendos.caja.strDescripcion = this.mdlRefrendos.objResultado.Tables[0].Rows[0][11].ToString();
                    //Si existe id de forma de pago
                    if (this.mdlRefrendos.objResultado.Tables[0].Rows[0][12].ToString() != "")
                    {
                        this.refrendos.formPago.intId = Int32.Parse(this.mdlRefrendos.objResultado.Tables[0].Rows[0][12].ToString());
                        this.refrendos.formPago.strCodigo = this.mdlRefrendos.objResultado.Tables[0].Rows[0][13].ToString();
                        this.refrendos.formPago.strDescripcion = this.mdlRefrendos.objResultado.Tables[0].Rows[0][14].ToString();
                    }
                    this.refrendos.strTipoRecibo = this.mdlRefrendos.objResultado.Tables[0].Rows[0][15].ToString();
                    this.refrendos.strFotografiaImagen = this.mdlRefrendos.objResultado.Tables[0].Rows[0][16].ToString();
                    this.refrendos.strMotivosCancelacion = this.mdlRefrendos.objResultado.Tables[0].Rows[0][17].ToString();
                    this.refrendos.strEstatus = this.mdlRefrendos.objResultado.Tables[0].Rows[0][18].ToString();
                    this.refrendos.strListaBoletasGeneradas = this.mdlRefrendos.objResultado.Tables[0].Rows[0][24].ToString();
                    this.refrendos.strIdsBoletasGeneradas = this.mdlRefrendos.objResultado.Tables[0].Rows[0][25].ToString();
                    this.refrendos.strIdsBoletasDetalles = this.mdlRefrendos.objResultado.Tables[0].Rows[0][26].ToString();
                    this.refrendos.strUsuarioEliminacion = this.mdlRefrendos.objResultado.Tables[0].Rows[0][27].ToString();
                    this.refrendos.strFechaEliminacion = this.mdlRefrendos.objResultado.Tables[0].Rows[0][28].ToString();
                    this.refrendos.decTotalPagar = decimal.Parse(this.mdlRefrendos.objResultado.Tables[0].Rows[0][29].ToString());
                    this.refrendos.decImportePagado = decimal.Parse(this.mdlRefrendos.objResultado.Tables[0].Rows[0][30].ToString());
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }
            

        }


        public void BuscarClientesBoletasActivas(string strCodigoCliente, int intSucursalId)
        {
            if (strCodigoCliente.Length > 0)
            {
                this.mdlRefrendos = new Model.clsMdlRecibosLiquidacionRefrendos();
                this.mdlRefrendos.buscarClientesBoletasActivas(strCodigoCliente, intSucursalId);

                if (this.mdlRefrendos.bolResultado)
                {
                    this.refrendos.cliente.intId = Int32.Parse(this.mdlRefrendos.objResultado.Tables[0].Rows[0][0].ToString());
                    this.refrendos.cliente.strCodigo = this.mdlRefrendos.objResultado.Tables[0].Rows[0][1].ToString();
                    this.refrendos.cliente.strNombre = this.mdlRefrendos.objResultado.Tables[0].Rows[0][2].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }


        }

        public void BuscarPagosDetalles(int intPagoId) 
        {

            this.mdlRefrendos = new Model.clsMdlRecibosLiquidacionRefrendos();
            this.mdlRefrendos.buscarPagosDetalles(intPagoId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlRefrendos.bolResultado;            
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSetSecundario = this.mdlRefrendos.objResultado;
        }

        public void PaginacionCliente(int intClienteId)
        {
           /* this.mdlBoletaEmpeno = new Model.clsMdlBoletaEmpeno();
            this.mdlBoletaEmpeno.paginacionCliente(intClienteId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlBoletaEmpeno.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlBoletaEmpeno.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlBoletaEmpeno.objResultado;*/

        }

        public void PaginacionBoletaActivasCliente(int intSucursalId)
        {
            this.mdlRefrendos = new Model.clsMdlRecibosLiquidacionRefrendos();
            this.mdlRefrendos.paginacionBoletaActivasCliente(intSucursalId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlRefrendos.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlRefrendos.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSetSecundario = this.mdlRefrendos.objResultado;

        }

        public void PaginacionDetallesBoletasPago(int intPagoId)
        {
            this.mdlRefrendos = new Model.clsMdlRecibosLiquidacionRefrendos();
            this.mdlRefrendos.paginacionDetallesBoletasPago(intPagoId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlRefrendos.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlRefrendos.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSetSecundario = this.mdlRefrendos.objResultado;

        }

        public void PaginacionDetallesBoletasActivas(int intClienteId, string strDia, int intSucursalId)
        {
            this.mdlRefrendos = new Model.clsMdlRecibosLiquidacionRefrendos();
            this.mdlRefrendos.paginacionDetallesBoletasActivas(intClienteId, strDia, intSucursalId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlRefrendos.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlRefrendos.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSetSecundario = this.mdlRefrendos.objResultado;

        }

        public void BuscarHuella(DataRow row)
        {

            this.refrendos.cliente.intId = int.Parse(row.ItemArray[0].ToString());
            this.refrendos.cliente.strCodigo = row.ItemArray[1].ToString();
            this.refrendos.cliente.strNombre = row.ItemArray[2].ToString();

        }


        public void Paginacion(int intSucursalId)
        {
            this.mdlRefrendos = new Model.clsMdlRecibosLiquidacionRefrendos();
            this.mdlRefrendos.paginacion(intSucursalId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlRefrendos.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlRefrendos.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlRefrendos.objResultado;

        }


        public void CargarFrmBusquedaClientesBoletasActivas()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de clientes con boletas activas";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = " codigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   " nombre Like '%" + frmBusqueda.dicReemplazo["texto"] + "%'";

            frmBusqueda.lsBloquearCol = new List<string>()
             {
                "cliente_id",
                "codigo",
                "nombre",
                "huella"

            };

           frmBusqueda.objDataSet = this.objDataSetSecundario;
           frmBusqueda.ShowDialog();

           this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.refrendos.cliente.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.refrendos.cliente.strCodigo = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.refrendos.cliente.strNombre = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                }
                catch (Exception e) { }
            }

       }


        public void CargarFrmBusquedaRangoFecha()
        {
            frmBusquedaRangoFecha frmBusqueda = new frmBusquedaRangoFecha();
            frmBusqueda.Text = "Búsqueda de pagos";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = " (folio Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   "  nombre_cliente Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   "  estatus Like '%" + frmBusqueda.dicReemplazo["texto"] + "%') AND (" +
                                    "  fecha_busqueda  >= #" + frmBusqueda.dicReemplazo["fecha"] + "# AND " +
                                    "  fecha_busqueda <= #" + frmBusqueda.dicReemplazo["fecha2"] + "#)";
            frmBusqueda.lsBloquearCol = new List<string>()
             {
               "pago_id",
                "folio",
                "fecha",
                "hora",
                "cliente_id",
                "codigo_cliente",
                "nombre_cliente",
                "usuario_id",
                "nombre_empleado",
                "caja_id",
                "codigo_caja",
                "descripcion_caja",
                "forma_pago_id",
                "codigo_forma_pago",
                "descripcion_forma_pago",
                "tipo_recibo",
                "fotografia_imagen",
                "motivo_cancelacion",
                "estatus",
                "lista_boletas_generadas",
                "fecha_busqueda"   ,            
                "ids_boletas_generadas",
                "ids_boletas_detalles",
                "nombre_empleado_eliminacion",
                "fecha_eliminacion",
                "total_pagar" ,
                "importe_pagado"  

             };

            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.refrendos.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.refrendos.strFolio = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.refrendos.dtFechaPago = Convert.ToDateTime(frmBusqueda.rowsIndex.Cells[2].Value.ToString());
                    this.refrendos.strHora = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                    this.refrendos.cliente.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[4].Value.ToString());
                    this.refrendos.cliente.strCodigo = frmBusqueda.rowsIndex.Cells[5].Value.ToString();
                    this.refrendos.cliente.strNombre = frmBusqueda.rowsIndex.Cells[6].Value.ToString();
                    this.refrendos.empleado.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[7].Value.ToString());
                    this.refrendos.empleado.strNombre = frmBusqueda.rowsIndex.Cells[8].Value.ToString();
                    this.refrendos.caja.intId = int.Parse(frmBusqueda.rowsIndex.Cells[9].Value.ToString());
                    this.refrendos.caja.strCodigo = frmBusqueda.rowsIndex.Cells[10].Value.ToString();
                    this.refrendos.caja.strDescripcion = frmBusqueda.rowsIndex.Cells[11].Value.ToString();
                    //Si existe id de forma de pago
                    if (frmBusqueda.rowsIndex.Cells[12].Value.ToString() != "")
                    {
                        this.refrendos.formPago.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[12].Value.ToString());
                        this.refrendos.formPago.strCodigo = frmBusqueda.rowsIndex.Cells[13].Value.ToString();
                        this.refrendos.formPago.strDescripcion = frmBusqueda.rowsIndex.Cells[14].Value.ToString();
                    }
                    this.refrendos.strTipoRecibo = frmBusqueda.rowsIndex.Cells[15].Value.ToString();
                    this.refrendos.strFotografiaImagen = frmBusqueda.rowsIndex.Cells[16].Value.ToString();
                    this.refrendos.strMotivosCancelacion = frmBusqueda.rowsIndex.Cells[17].Value.ToString();
                    this.refrendos.strEstatus = frmBusqueda.rowsIndex.Cells[18].Value.ToString();
                    this.refrendos.strListaBoletasGeneradas = frmBusqueda.rowsIndex.Cells[24].Value.ToString();
                    this.refrendos.strIdsBoletasGeneradas = frmBusqueda.rowsIndex.Cells[25].Value.ToString();
                    this.refrendos.strIdsBoletasDetalles = frmBusqueda.rowsIndex.Cells[26].Value.ToString();
                    this.refrendos.strUsuarioEliminacion = frmBusqueda.rowsIndex.Cells[27].Value.ToString();
                    this.refrendos.strFechaEliminacion = frmBusqueda.rowsIndex.Cells[28].Value.ToString();
                    this.refrendos.decTotalPagar = decimal.Parse(frmBusqueda.rowsIndex.Cells[29].Value.ToString());
                    this.refrendos.decImportePagado = decimal.Parse(frmBusqueda.rowsIndex.Cells[30].Value.ToString());





                }
                catch (Exception e) { }
            }
        }



        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.refrendos.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.refrendos.strFolio = this.objDataSetSingle.ItemArray[1].ToString();
                this.refrendos.dtFechaPago = Convert.ToDateTime(this.objDataSetSingle.ItemArray[2].ToString());
                this.refrendos.strHora = this.objDataSetSingle.ItemArray[3].ToString();
                this.refrendos.cliente.intId = Int32.Parse(this.objDataSetSingle.ItemArray[4].ToString());
                this.refrendos.cliente.strCodigo = this.objDataSetSingle.ItemArray[5].ToString();
                this.refrendos.cliente.strNombre = this.objDataSetSingle.ItemArray[6].ToString();
                this.refrendos.empleado.intId = Int32.Parse(this.objDataSetSingle.ItemArray[7].ToString());
                this.refrendos.empleado.strNombre = this.objDataSetSingle.ItemArray[8].ToString();
                this.refrendos.caja.intId = int.Parse(this.objDataSetSingle.ItemArray[9].ToString());
                this.refrendos.caja.strCodigo = this.objDataSetSingle.ItemArray[10].ToString();
                this.refrendos.caja.strDescripcion = this.objDataSetSingle.ItemArray[11].ToString();
                //Si existe id de forma de pago
                if (this.objDataSetSingle.ItemArray[12].ToString() != "")
                {
                    this.refrendos.formPago.intId = Int32.Parse(this.objDataSetSingle.ItemArray[12].ToString());
                    this.refrendos.formPago.strCodigo = this.objDataSetSingle.ItemArray[13].ToString();
                    this.refrendos.formPago.strDescripcion = this.objDataSetSingle.ItemArray[14].ToString();
                }
                this.refrendos.strTipoRecibo = this.objDataSetSingle.ItemArray[15].ToString();
                this.refrendos.strFotografiaImagen = this.objDataSetSingle.ItemArray[16].ToString();
                this.refrendos.strMotivosCancelacion = this.objDataSetSingle.ItemArray[17].ToString();
                this.refrendos.strEstatus = this.objDataSetSingle.ItemArray[18].ToString();
                this.refrendos.strListaBoletasGeneradas = this.objDataSetSingle.ItemArray[24].ToString();
                this.refrendos.strIdsBoletasGeneradas = this.objDataSetSingle.ItemArray[25].ToString();
                this.refrendos.strIdsBoletasDetalles = this.objDataSetSingle.ItemArray[26].ToString();
                this.refrendos.strUsuarioEliminacion = this.objDataSetSingle.ItemArray[27].ToString();
                this.refrendos.strFechaEliminacion = this.objDataSetSingle.ItemArray[28].ToString();
                this.refrendos.decTotalPagar = decimal.Parse(this.objDataSetSingle.ItemArray[29].ToString());
                this.refrendos.decImportePagado = decimal.Parse(this.objDataSetSingle.ItemArray[30].ToString());


            }
            
        }

        public void limpiarProps()
        {
            this.refrendos.limpiarProps();
        }

    }
}
