﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlCaja : Core.clsCore
    {
        public Model.clsMdlCaja mdlCaja;        
        public Properties_Class.clsCaja caja;


        public clsCtrlCaja()
        {
            this.caja = new Properties_Class.clsCaja();
        }

        public void Guardar()
        {
            this.mdlCaja = new Model.clsMdlCaja();
            this.caja.agregarCero();
            this.mdlCaja.guardar(this.caja);
        }

        public void Eliminar()
        {
            if (this.caja.intId == 0) return;
            this.mdlCaja = new Model.clsMdlCaja();
            //Hacer un llamado a la funcion para desactivar el registro
            this.caja.cambioEstatus(this.caja.dicEstatus["INACTIVO"]);
            this.mdlCaja.eliminar(this.caja);
        }

        public void Buscar(string strCodigo)
        {
            if (strCodigo.Length > 0)
            {
                this.mdlCaja = new Model.clsMdlCaja();
                this.mdlCaja.buscar(strCodigo);

                if (this.mdlCaja.bolResultado)
                {
                    this.caja.intId = Int32.Parse(this.mdlCaja.objResultado.Tables[0].Rows[0][0].ToString());
                    this.caja.strCodigo = this.mdlCaja.objResultado.Tables[0].Rows[0][1].ToString();
                    this.caja.strDescripcion = this.mdlCaja.objResultado.Tables[0].Rows[0][2].ToString();
                    this.caja.strEstatus = this.mdlCaja.objResultado.Tables[0].Rows[0][3].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }
        }

        public void ValidarDescripcion(string strDescripcion)
        {
            this.mdlCaja = new Model.clsMdlCaja();
            this.mdlCaja.validarDescripcion(strDescripcion);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlCaja.bolResultado;
        }
        public void Paginacion()
        {
            this.mdlCaja = new Model.clsMdlCaja();
            this.mdlCaja.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlCaja.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlCaja.bolResultado; ;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlCaja.objResultado;

        }
        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de cajas";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = "descripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   "  codigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' ";
                                 
            frmBusqueda.lsBloquearCol = new List<string>()
            {
                    "caja_id",
                    "codigo",
                    "descripcion",
                    "estatus"
            };
            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.caja.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.caja.strCodigo = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.caja.strDescripcion = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.caja.strEstatus = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                    this.intCountIndex = int.Parse(this.caja.strCodigo) - 1;
                }
                catch (Exception e) { }
            }
        }

        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.caja.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.caja.strCodigo = this.objDataSetSingle.ItemArray[1].ToString();
                this.caja.strDescripcion = this.objDataSetSingle.ItemArray[2].ToString();
                this.caja.strEstatus = this.objDataSetSingle.ItemArray[3].ToString();
            }

        }

        public void limpiarProps()
        {
            this.caja.limpiarProps();
        }
    }
}