﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlSucursales : Core.clsCore
    {
     
        public Model.clsMdlSucursales mdlSucursales;
        public Properties_Class.clsSucursal sucursal;        

        public clsCtrlSucursales()
        {
            this.sucursal = new Properties_Class.clsSucursal();
        }
         
        public void Guardar()
        {
            this.mdlSucursales = new Model.clsMdlSucursales();
            this.sucursal.agregarCero();
            this.mdlSucursales.guardar(this.sucursal);

        }

        public void Eliminar()
        {
            if (this.sucursal.intId == 0) return;
            this.mdlSucursales = new Model.clsMdlSucursales();
            //Hacer un llamado a la funcion para desactivar el registro
            this.sucursal.cambioEstatus(this.sucursal.dicEstatus["INACTIVO"]);
            this.mdlSucursales.eliminar(this.sucursal);
        }

        public void Reactivar()
        {
            if (sucursal.intId == 0) return;
            this.mdlSucursales = new Model.clsMdlSucursales();
            //Hacer un llamado a la funcion para desactivar el registro
            this.sucursal.cambioEstatus(this.sucursal.dicEstatus["ACTIVO"]);
            this.mdlSucursales.reactivar(this.sucursal);
            this.bolResultado = this.mdlSucursales.bolResultado;
        }

        public void Buscar(string strCodigo)
        {
            if (strCodigo.Length > 0)
            {
                this.mdlSucursales = new Model.clsMdlSucursales();
                this.mdlSucursales.buscar(strCodigo);

                if (this.mdlSucursales.bolResultado)
                {

                    this.sucursal.intId = Int32.Parse(this.mdlSucursales.objResultado.Tables[0].Rows[0][0].ToString());
                    this.sucursal.strCodigo = this.mdlSucursales.objResultado.Tables[0].Rows[0][1].ToString();
                    this.sucursal.empresa.intId = Int32.Parse(this.mdlSucursales.objResultado.Tables[0].Rows[0][2].ToString());                    
                    this.sucursal.strNombre = this.mdlSucursales.objResultado.Tables[0].Rows[0][3].ToString();
                    this.sucursal.strRepresentanteLegal = this.mdlSucursales.objResultado.Tables[0].Rows[0][4].ToString();
                    this.sucursal.strCalle = this.mdlSucursales.objResultado.Tables[0].Rows[0][5].ToString();
                    this.sucursal.strNumeroExterior = this.mdlSucursales.objResultado.Tables[0].Rows[0][6].ToString();
                    this.sucursal.strNumeroInterior = this.mdlSucursales.objResultado.Tables[0].Rows[0][7].ToString();
                    this.sucursal.asentamiento.intId = Int32.Parse(this.mdlSucursales.objResultado.Tables[0].Rows[0][8].ToString());
                    this.sucursal.strTelefono1 = this.mdlSucursales.objResultado.Tables[0].Rows[0][9].ToString();
                    this.sucursal.strTelefono2 = this.mdlSucursales.objResultado.Tables[0].Rows[0][10].ToString();
                    this.sucursal.strCorreoElectronico = this.mdlSucursales.objResultado.Tables[0].Rows[0][11].ToString();
                    this.sucursal.dtInicioOperaciones = Convert.ToDateTime(this.mdlSucursales.objResultado.Tables[0].Rows[0][12].ToString());
                    this.sucursal.empresa.strNombreComercial = this.mdlSucursales.objResultado.Tables[0].Rows[0][14].ToString();
                    this.sucursal.asentamiento.strDescripcion = this.mdlSucursales.objResultado.Tables[0].Rows[0][17].ToString();
                    this.sucursal.asentamiento.strCodigoPostal = this.mdlSucursales.objResultado.Tables[0].Rows[0][19].ToString();
                    this.sucursal.asentamiento.municipio.strCodigo = this.mdlSucursales.objResultado.Tables[0].Rows[0][20].ToString();
                    this.sucursal.asentamiento.municipio.strDescripcion = this.mdlSucursales.objResultado.Tables[0].Rows[0][21].ToString();
                    this.sucursal.asentamiento.municipio.estado.strCodigo = this.mdlSucursales.objResultado.Tables[0].Rows[0][22].ToString();
                    this.sucursal.asentamiento.municipio.estado.strDescripcion = this.mdlSucursales.objResultado.Tables[0].Rows[0][23].ToString();
                    this.sucursal.strEstatus = this.mdlSucursales.objResultado.Tables[0].Rows[0][24].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }
        }

        public void BuscarSucursalEmpresas(int intEmpresaId)
        {
            if (intEmpresaId > 0)
            {
                this.mdlSucursales = new Model.clsMdlSucursales();
                this.mdlSucursales.buscarSucursalEmprsea(intEmpresaId);
                //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
                this.bolResultado = this.mdlSucursales.bolResultado;
                //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
                this.objDataSet = this.mdlSucursales.objResultado;

                if (!(this.bolResultado)) return;

                frmBusqueda frmBusqueda = new frmBusqueda();
                frmBusqueda.Text = "Búsqueda de sucursales";
                frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
                frmBusqueda.strQuery = "nombre Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                       "  codigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' ";
                                     
                frmBusqueda.lsBloquearCol = new List<string>()
                {
                        "sucursal_id" ,
                        "codigo" ,
                        "empresa_id" ,
                        "nombre" ,
                        "representante_legal" ,
                        "calle" ,
                        "numero_exterior" ,
                        "numero_interior" ,
                        "asentamiento_id" ,
                        "telefono_01" ,
                        "telefono_02" ,
                        "correo_electronico" ,
                        "inicio_operaciones"
                };
                frmBusqueda.objDataSet = this.objDataSet;
                frmBusqueda.ShowDialog();
                this.bolResultadoBusqueda = frmBusqueda.bolCierra;
                if (frmBusqueda.bolCierra)
                {

                    try
                    {
                        this.sucursal.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                        this.sucursal.strCodigo = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                        this.sucursal.empresa.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[2].Value.ToString());
                        this.sucursal.strNombre = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                        this.sucursal.strRepresentanteLegal = frmBusqueda.rowsIndex.Cells[4].Value.ToString();
                        this.sucursal.strCalle = frmBusqueda.rowsIndex.Cells[5].Value.ToString();
                        this.sucursal.strNumeroExterior = frmBusqueda.rowsIndex.Cells[6].Value.ToString();
                        this.sucursal.strNumeroInterior = frmBusqueda.rowsIndex.Cells[7].Value.ToString();
                        this.sucursal.asentamiento.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[8].Value.ToString());
                        this.sucursal.strTelefono1 = frmBusqueda.rowsIndex.Cells[9].Value.ToString();
                        this.sucursal.strTelefono2 = frmBusqueda.rowsIndex.Cells[10].Value.ToString();
                        this.sucursal.strCorreoElectronico = frmBusqueda.rowsIndex.Cells[11].Value.ToString();
                        this.sucursal.dtInicioOperaciones = Convert.ToDateTime(frmBusqueda.rowsIndex.Cells[12].Value.ToString());                        
                    }
                    catch (Exception e) { }
                }

            }
        }

        public void BuscarSucursalUsuario(int intUsuarioId)
        {
            if (intUsuarioId > 0)
            {
                this.mdlSucursales = new Model.clsMdlSucursales();
                this.mdlSucursales.buscarSucursalUsuario(intUsuarioId);
                //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
                this.bolResultado = this.mdlSucursales.bolResultado;
                //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
                this.objDataSet = this.mdlSucursales.objResultado;

                if (!(this.bolResultado)) return;

                frmBusqueda frmBusqueda = new frmBusqueda();
                frmBusqueda.Text = "Búsqueda de sucursales";
                frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
                frmBusqueda.strQuery = "nombre Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                       "  codigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' ";

                frmBusqueda.lsBloquearCol = new List<string>()
                {
                        "sucursal_id" ,
                        "codigo" ,
                        "empresa_id" ,
                        "nombre" ,
                        "representante_legal" ,
                        "calle" ,
                        "numero_exterior" ,
                        "numero_interior" ,
                        "asentamiento_id" ,
                        "telefono_01" ,
                        "telefono_02" ,
                        "correo_electronico" ,
                        "inicio_operaciones"
                };
                frmBusqueda.objDataSet = this.objDataSet;
                frmBusqueda.ShowDialog();
                this.bolResultadoBusqueda = frmBusqueda.bolCierra;
                if (frmBusqueda.bolCierra)
                {

                    try
                    {

                        this.sucursal.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                        this.sucursal.strCodigo = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                        this.sucursal.empresa.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[2].Value.ToString());
                        this.sucursal.strNombre = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                        this.sucursal.strRepresentanteLegal = frmBusqueda.rowsIndex.Cells[4].Value.ToString();
                        this.sucursal.strCalle = frmBusqueda.rowsIndex.Cells[5].Value.ToString();
                        this.sucursal.strNumeroExterior = frmBusqueda.rowsIndex.Cells[6].Value.ToString();
                        this.sucursal.strNumeroInterior = frmBusqueda.rowsIndex.Cells[7].Value.ToString();
                        this.sucursal.asentamiento.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[8].Value.ToString());
                        this.sucursal.strTelefono1 = frmBusqueda.rowsIndex.Cells[9].Value.ToString();
                        this.sucursal.strTelefono2 = frmBusqueda.rowsIndex.Cells[10].Value.ToString();
                        this.sucursal.strCorreoElectronico = frmBusqueda.rowsIndex.Cells[11].Value.ToString();
                        this.sucursal.dtInicioOperaciones = Convert.ToDateTime(frmBusqueda.rowsIndex.Cells[12].Value.ToString());
                    }
                    catch (Exception e) { 
                    }

                }

            }
        }
        public void Paginacion()
        {
            this.mdlSucursales = new Model.clsMdlSucursales();
            this.mdlSucursales.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlSucursales.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlSucursales.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlSucursales.objResultado;

        }
        

        //Obtener sucursales que es diferenet al sucursal que esta logeado el usuario del sistema
        public void PaginacionOtrasSuc(int intSucursal, int intEmpresaId)
        {
            this.mdlSucursales = new Model.clsMdlSucursales();
            this.mdlSucursales.paginacionOtrasSuc(intSucursal, intEmpresaId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlSucursales.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlSucursales.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlSucursales.objResultado;

        }

        public void PaginacionPorcentajesCobro()
        {
            this.mdlSucursales = new Model.clsMdlSucursales();
            //this.mdlSucursales.paginacionPorcentajesCobro();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlSucursales.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlSucursales.bolResultado; 
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlSucursales.objResultado;

        }

        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de sucursales";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = "nombre Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   "  codigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                   "  estadoDescripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                   " estadoCodigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%'";
            frmBusqueda.lsBloquearCol = new List<string>()
            {
                    "sucursal_id",
                    "codigo",
                    "nombre",
                    "empresa_id",
                    "asentamiento_id",
                    "representante_legal",
                    "calle",
                    "numero_exterior",
                    "numero_interior",
                    "telefono_01",
                    "telefono_02",
                    "correo_electronico",
                    "inicio_operaciones",
                    "razon_social",
                    "nombre_comercial",
                    "rfc",
                    "curp",
                    "descripcion",
                    "ciudad",
                    "codigo_postal" ,
                    "municipioCodigo",
                    "municipioDescripcion", 
                    "estadoCodigo", 
                   "estadoDescripcion",
                   "estatus"
            };
            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {

                try
                {
                    this.sucursal.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.sucursal.strCodigo = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.sucursal.empresa.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[2].Value.ToString());
                    this.sucursal.strNombre = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                    this.sucursal.strRepresentanteLegal = frmBusqueda.rowsIndex.Cells[4].Value.ToString();
                    this.sucursal.strCalle = frmBusqueda.rowsIndex.Cells[5].Value.ToString();
                    this.sucursal.strNumeroExterior = frmBusqueda.rowsIndex.Cells[6].Value.ToString();
                    this.sucursal.strNumeroInterior = frmBusqueda.rowsIndex.Cells[7].Value.ToString();
                    this.sucursal.asentamiento.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[8].Value.ToString());
                    this.sucursal.strTelefono1 = frmBusqueda.rowsIndex.Cells[9].Value.ToString();
                    this.sucursal.strTelefono2 = frmBusqueda.rowsIndex.Cells[10].Value.ToString();
                    this.sucursal.strCorreoElectronico = frmBusqueda.rowsIndex.Cells[11].Value.ToString();
                    this.sucursal.dtInicioOperaciones = Convert.ToDateTime(frmBusqueda.rowsIndex.Cells[12].Value.ToString());
                    this.sucursal.empresa.strNombreComercial = frmBusqueda.rowsIndex.Cells[14].Value.ToString();
                    this.sucursal.asentamiento.strDescripcion = frmBusqueda.rowsIndex.Cells[17].Value.ToString();
                    this.sucursal.asentamiento.strCodigoPostal = frmBusqueda.rowsIndex.Cells[19].Value.ToString();
                    this.sucursal.asentamiento.municipio.strCodigo = frmBusqueda.rowsIndex.Cells[20].Value.ToString();
                    this.sucursal.asentamiento.municipio.strDescripcion = frmBusqueda.rowsIndex.Cells[21].Value.ToString();
                    this.sucursal.asentamiento.municipio.estado.strCodigo = frmBusqueda.rowsIndex.Cells[22].Value.ToString();
                    this.sucursal.asentamiento.municipio.estado.strDescripcion = frmBusqueda.rowsIndex.Cells[23].Value.ToString();
                    this.sucursal.strEstatus = frmBusqueda.rowsIndex.Cells[24].Value.ToString();
                }
                catch (Exception e) { }

            }
        }

        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {               
                this.sucursal.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.sucursal.strCodigo = this.objDataSetSingle.ItemArray[1].ToString();
                this.sucursal.empresa.intId = Int32.Parse(this.objDataSetSingle.ItemArray[2].ToString());                
                this.sucursal.strNombre = this.objDataSetSingle.ItemArray[3].ToString();
                this.sucursal.strRepresentanteLegal = this.objDataSetSingle.ItemArray[4].ToString();
                this.sucursal.strCalle = this.objDataSetSingle.ItemArray[5].ToString();
                this.sucursal.strNumeroExterior = this.objDataSetSingle.ItemArray[6].ToString();
                this.sucursal.strNumeroInterior = this.objDataSetSingle.ItemArray[7].ToString();
                this.sucursal.asentamiento.intId = Int32.Parse(this.objDataSetSingle.ItemArray[8].ToString());
                this.sucursal.strTelefono1 = this.objDataSetSingle.ItemArray[9].ToString();
                this.sucursal.strTelefono2 = this.objDataSetSingle.ItemArray[10].ToString();
                this.sucursal.strCorreoElectronico = this.objDataSetSingle.ItemArray[11].ToString();
                this.sucursal.dtInicioOperaciones = Convert.ToDateTime(this.objDataSetSingle.ItemArray[12].ToString());
                this.sucursal.empresa.strNombreComercial = this.objDataSetSingle.ItemArray[14].ToString();
                this.sucursal.asentamiento.strDescripcion = this.objDataSetSingle.ItemArray[17].ToString();
                this.sucursal.asentamiento.strCodigoPostal = this.objDataSetSingle.ItemArray[19].ToString();
                this.sucursal.asentamiento.municipio.strCodigo = this.objDataSetSingle.ItemArray[20].ToString();
                this.sucursal.asentamiento.municipio.strDescripcion = this.objDataSetSingle.ItemArray[21].ToString();
                this.sucursal.asentamiento.municipio.estado.strCodigo = this.objDataSetSingle.ItemArray[22].ToString();
                this.sucursal.asentamiento.municipio.estado.strDescripcion = this.objDataSetSingle.ItemArray[23].ToString();
                this.sucursal.strEstatus = this.objDataSetSingle.ItemArray[24].ToString();                
            }

        }

        public void limpiarProps()
        {
            this.sucursal.limpiarProps();
        }
    }
}
