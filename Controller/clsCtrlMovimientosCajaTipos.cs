﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlMovimientosCajaTipos : Core.clsCore
    {
        
        public Model.clsMdlMovimientosCajaTipos mdlMovimientosCajaTipos;

        
        public Properties_Class.clsMovimientosCajaTipos movimientosCajaTipos;

        public clsCtrlMovimientosCajaTipos()
        {
            this.movimientosCajaTipos = new Properties_Class.clsMovimientosCajaTipos();
        }

        public void Guardar()
        {
            this.mdlMovimientosCajaTipos = new Model.clsMdlMovimientosCajaTipos();            
            this.mdlMovimientosCajaTipos.guardar(this.movimientosCajaTipos);
        }

        public void Eliminar()
        {
            if (movimientosCajaTipos.intId == 0) return;
            this.mdlMovimientosCajaTipos = new Model.clsMdlMovimientosCajaTipos();
            //Hacer un llamado a la funcion para desactivar el registro
            this.movimientosCajaTipos.cambioEstatus(this.movimientosCajaTipos.dicEstatus["INACTIVO"]);
            this.mdlMovimientosCajaTipos.eliminar(this.movimientosCajaTipos);
        }

        public void Buscar(string strCodigo)
        {
            if (strCodigo.Length > 0)
            {
                this.mdlMovimientosCajaTipos = new Model.clsMdlMovimientosCajaTipos();
                this.mdlMovimientosCajaTipos.buscar(strCodigo);

                if (this.mdlMovimientosCajaTipos.bolResultado)
                {
                    this.movimientosCajaTipos.intId = Int32.Parse(this.mdlMovimientosCajaTipos.objResultado.Tables[0].Rows[0][0].ToString());                    
                    this.movimientosCajaTipos.strDescripcion = this.mdlMovimientosCajaTipos.objResultado.Tables[0].Rows[0][1].ToString();
                    this.movimientosCajaTipos.strTipo = this.mdlMovimientosCajaTipos.objResultado.Tables[0].Rows[0][2].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }
        }

        public void BuscarDuplicacion(string strTipo, string strDescripcion) 
        {
            this.mdlMovimientosCajaTipos = new Model.clsMdlMovimientosCajaTipos();
            this.mdlMovimientosCajaTipos.buscarDuplicacion(strTipo, strDescripcion);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlMovimientosCajaTipos.bolResultado;
            
        }
        public void Paginacion()
        {
            this.mdlMovimientosCajaTipos = new Model.clsMdlMovimientosCajaTipos();
            this.mdlMovimientosCajaTipos.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlMovimientosCajaTipos.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlMovimientosCajaTipos.bolResultado; 
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlMovimientosCajaTipos.objResultado;

        }

        public void PaginacionActivo()
        {
            this.mdlMovimientosCajaTipos = new Model.clsMdlMovimientosCajaTipos();
            this.mdlMovimientosCajaTipos.paginacionActivo();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlMovimientosCajaTipos.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlMovimientosCajaTipos.bolResultado; ;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlMovimientosCajaTipos.objResultado;

        }

        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de tipo de movimientos de caja";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = "descripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                               "  tipo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' ";                               
            frmBusqueda.lsBloquearCol = new List<string>()
            {
                    "movimiento_caja_tipo_id",
                    "tipo",
                    "descripcion",
                    "estatus"
            };            

           frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.movimientosCajaTipos.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.movimientosCajaTipos.strDescripcion = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.movimientosCajaTipos.strTipo = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.movimientosCajaTipos.strEstatus = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                }
                catch (Exception e) { }
            }
        }



        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.movimientosCajaTipos.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());                
                this.movimientosCajaTipos.strDescripcion = this.objDataSetSingle.ItemArray[1].ToString();
                this.movimientosCajaTipos.strTipo = this.objDataSetSingle.ItemArray[2].ToString();
                this.movimientosCajaTipos.strEstatus = this.objDataSetSingle.ItemArray[3].ToString();
            }
            
        }

        public void limpiarProps()
        {
            this.movimientosCajaTipos.limpiarProps();
        }
    }
}
