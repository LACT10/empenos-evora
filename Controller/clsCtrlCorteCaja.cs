﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
   public class clsCtrlCorteCaja : Core.clsCore
    {
        public Properties_Class.clsCorteCaja corteCaja;
        public Model.clsMdlCorteCaja mdlCorteCaja;

        public clsCtrlCorteCaja()
        {
            this.corteCaja = new Properties_Class.clsCorteCaja();


        }

        public void Guardar()
        {
            this.mdlCorteCaja = new Model.clsMdlCorteCaja();
            this.mdlCorteCaja.guardar(this.corteCaja);

        }

        public void Cancelar()
        {
            if (corteCaja.intId == 0) return;
            this.mdlCorteCaja = new Model.clsMdlCorteCaja();
            //Hacer un llamado a la funcion para desactivar el registro
            this.corteCaja.cambioEstatus(this.corteCaja.dicEstatus["CANCELADO"]);
            this.mdlCorteCaja.cancelar(this.corteCaja);
            this.bolResultado = this.mdlCorteCaja.bolResultado;
        }

       


   

        public void UltimoCorteCierre(int intSucursalId) 
        {
            this.mdlCorteCaja = new Model.clsMdlCorteCaja();
            this.mdlCorteCaja.ultimoCorteCierre(intSucursalId);
            this.mdlCorteCaja.bolResultado = (this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["ultimoFecha"].ToString() == "") ? false : true;
            if (this.mdlCorteCaja.bolResultado) 
            {                
                 this.corteCaja.dtFechaUlimoCorteCierre = Convert.ToDateTime(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["ultimoFecha"].ToString());
                this.corteCaja.decSaldoInicial = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["saldo_final"].ToString());
            }
        }

        /*Metodo que buscar  el capital, intereses, almacenaje, manejo, bonificaciones, siempre cuando no hay un corte de la fecha que se envia en el parametro
          dtFechaCorte y dependiendo del sucursal y caja*/
        public void BuscarCaja(int intSucursalId, int intCajaId, DateTime dtFechaCorte)
        {
            this.mdlCorteCaja = new Model.clsMdlCorteCaja();
            this.mdlCorteCaja.buscarCaja(intSucursalId, intCajaId, dtFechaCorte);                        

            if (this.mdlCorteCaja.bolResultado)
            {
                this.corteCaja.strListaBoletasId = this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["lista_boletas_id"].ToString();
                this.corteCaja.strListaBoletasRefrendosId = this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["lista_boletas_refrendos_id"].ToString();
                this.corteCaja.strListaBoletasSinPagoId = this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["lista_boletas_sin_pago_id"].ToString();                
                this.corteCaja.strListaPagoId = this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["lista_pago_id"].ToString();
                this.corteCaja.strListaMovimientoEntradaId = this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["lista_movimiento_entradad_caja_id"].ToString();
                this.corteCaja.strListaMovimientoSalidadId = this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["lista_movimiento_salidad_caja_id"].ToString();
                this.corteCaja.decCapital = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["capital"].ToString());
                this.corteCaja.decAbonoCapital = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["abono_capital_total"].ToString());
                this.corteCaja.decCapital += this.corteCaja.decAbonoCapital;
                this.corteCaja.decInteres = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["intereses"].ToString());
                this.corteCaja.decAlmacenaje = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["almacenaje"].ToString());
                this.corteCaja.decManejo = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["manejo"].ToString());
                this.corteCaja.decBonificacinones = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["bonificacion"].ToString());
                this.corteCaja.decLiqBoletas = this.corteCaja.calcularLiqBoletas();
                this.corteCaja.decEntradasCaja = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["entrada_total"].ToString());
                this.corteCaja.decSalidadCaja = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["salida_total"].ToString());
                this.corteCaja.decTotalIngresos = this.corteCaja.calcularTotalIngresos();
                this.corteCaja.decBoletasEmpenadas = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["boletas_empenadas"].ToString());
                this.corteCaja.decTotalEgresos = this.corteCaja.calcularTotalEgresos();
                this.corteCaja.decSaldoFinal = this.corteCaja.calcularSaldoFinal();


            }
            else 
            {
                this.limpiarProps();
            }

        }

        /*Metodo que buscar  el capital, intereses, almacenaje, manejo, bonificaciones, siempre cuando no hay un corte de la fecha que se envia en el parametro
        dtFechaCorte y dependiendo del sucursal*/
        public void BuscarCajaIntegradora(int intSucursalId, DateTime dtFechaCorte)
        {
            this.mdlCorteCaja = new Model.clsMdlCorteCaja();
            this.mdlCorteCaja.buscarCajaIntegradora(intSucursalId, dtFechaCorte);

            if (this.mdlCorteCaja.bolResultado)
            {

                this.corteCaja.strListaBoletasId = this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["lista_boletas_id"].ToString();
                this.corteCaja.strListaBoletasRefrendosId = this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["lista_boletas_refrendos_id"].ToString();
                this.corteCaja.strListaBoletasSinPagoId = this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["lista_boletas_sin_pago_id"].ToString();
                this.corteCaja.strListaPagoId = this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["lista_pago_id"].ToString();
                this.corteCaja.strListaMovimientoEntradaId = this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["lista_movimiento_entradad_caja_id"].ToString();
                this.corteCaja.strListaMovimientoSalidadId = this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["lista_movimiento_salidad_caja_id"].ToString();
                this.corteCaja.decCapital = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["capital"].ToString());
                this.corteCaja.decAbonoCapital = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["abono_capital_total"].ToString());
                this.corteCaja.decCapital += this.corteCaja.decAbonoCapital;
                this.corteCaja.decInteres = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["intereses"].ToString());
                this.corteCaja.decAlmacenaje = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["almacenaje"].ToString());
                this.corteCaja.decManejo = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["manejo"].ToString());
                this.corteCaja.decBonificacinones = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["bonificacion"].ToString());
                this.corteCaja.decLiqBoletas = this.corteCaja.calcularLiqBoletas();
                this.corteCaja.decEntradasCaja = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["entrada_total"].ToString());
                this.corteCaja.decSalidadCaja = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["salida_total"].ToString());
                this.corteCaja.decTotalIngresos = this.corteCaja.calcularTotalIngresos();
                this.corteCaja.decBoletasEmpenadas = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["boletas_empenadas"].ToString());
                this.corteCaja.decTotalEgresos = this.corteCaja.calcularTotalEgresos();
                this.corteCaja.decSaldoFinal = this.corteCaja.calcularSaldoFinal();



            }
            else
            {
                this.limpiarProps();
            }

        }

        public void BuscarCorteCaja(int intCorteCajaId, int intSucursalId, int intCajaId, DateTime dtFechaCorte)
        {
            this.mdlCorteCaja = new Model.clsMdlCorteCaja();
            this.mdlCorteCaja.buscarCorteCaja(intCorteCajaId, intSucursalId, intCajaId, dtFechaCorte);

            if (this.mdlCorteCaja.bolResultado)
            {
                /*this.corteCaja.decCapital = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["capital"].ToString());
                this.corteCaja.decInteres = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["intereses"].ToString());
                this.corteCaja.decAlmacenaje = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["almacenaje"].ToString());
                this.corteCaja.decManejo = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["manejo"].ToString());
                this.corteCaja.decBonificacinones = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["bonificacion"].ToString());
                this.corteCaja.decLiqBoletas = this.corteCaja.calcularLiqBoletas();
                this.corteCaja.decEntradasCaja = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["entrada_total"].ToString());
                this.corteCaja.decSalidadCaja = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["salida_total"].ToString());
                this.corteCaja.decTotalIngresos = this.corteCaja.calcularTotalIngresos();
                this.corteCaja.decBoletasEmpenadas = decimal.Parse(this.mdlCorteCaja.objResultado.Tables[0].Rows[0]["boletas_empenadas"].ToString());
                this.corteCaja.decTotalEgresos = this.corteCaja.calcularTotalEgresos();*/

            }
            else
            {
                this.limpiarProps();
            }

        }



        public void Paginacion(int intSucursalId)
        {
            this.mdlCorteCaja = new Model.clsMdlCorteCaja();
            this.mdlCorteCaja.paginacion(intSucursalId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlCorteCaja.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlCorteCaja.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlCorteCaja.objResultado;

        }

      



        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                /*this.corteCaja.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.corteCaja.strFolio = this.objDataSetSingle.ItemArray[1].ToString();
                this.corteCaja.strBoletaInicial = this.objDataSetSingle.ItemArray[2].ToString();*/

                this.corteCaja.caja.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.corteCaja.caja.strDescripcion = this.objDataSetSingle.ItemArray[1].ToString();

            }

        }

        public void limpiarProps()
        {
            this.corteCaja.limpiarProps();
        }
    }
}
