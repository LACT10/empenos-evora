﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clCtrlFormaPago : Core.clsCore
    {

        public Model.clsMdlFormaPago mdlFormaPago;
        public Properties_Class.clsFormPago formaPago;
        

        public clCtrlFormaPago()
        {
            this.formaPago = new Properties_Class.clsFormPago();
        }

        public void Guardar()
        {
            this.mdlFormaPago = new Model.clsMdlFormaPago();
            this.formaPago.agregarCero();            
            this.mdlFormaPago.guardar(this.formaPago);
            
        }

        public void Eliminar()
        {
            if (this.formaPago.intId == 0) return;
            this.mdlFormaPago = new Model.clsMdlFormaPago();
            //Hacer un llamado a la funcion para desactivar el registro
            this.formaPago.cambioEstatus(this.formaPago.dicEstatus["INACTIVO"]);
            this.mdlFormaPago.eliminar(this.formaPago);
        }

        public void Buscar(string strCodigo)
        {
            this.mdlFormaPago = new Model.clsMdlFormaPago();
            if (strCodigo.Length > 0)
            {
                
                this.mdlFormaPago.buscar(strCodigo);

                if (this.mdlFormaPago.bolResultado)
                {
                    this.formaPago.intId = Int32.Parse(this.mdlFormaPago.objResultado.Tables[0].Rows[0][0].ToString());
                    this.formaPago.strCodigo = this.mdlFormaPago.objResultado.Tables[0].Rows[0][1].ToString();
                    this.formaPago.strDescripcion = this.mdlFormaPago.objResultado.Tables[0].Rows[0][2].ToString();                    
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.mdlFormaPago.bolResultado = false;
                this.limpiarProps();
            }
        }

        public void ValidarDescripcion(string strDescripcion)
        {
            this.mdlFormaPago = new Model.clsMdlFormaPago();
            this.mdlFormaPago.validarDescripcion(strDescripcion);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlFormaPago.bolResultado;
        }
        public void Paginacion()
        {
            this.mdlFormaPago = new Model.clsMdlFormaPago();
            this.mdlFormaPago.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlFormaPago.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlFormaPago.bolResultado; ;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlFormaPago.objResultado;

        }
        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de formas de pago";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = "descripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   "  codigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%'";
            frmBusqueda.lsBloquearCol = new List<string>()
            {
                    "forma_pago_id",
                    "codigo",
                    "descripcion",
                    "estatus"
            };
            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.formaPago.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.formaPago.strCodigo = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.formaPago.strDescripcion = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.formaPago.strEstatus = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                    //this.intCountIndex = int.Parse(frmBusqueda.rowsIndex.Index.ToString());
                }
                catch (Exception e) { }
            }
        }

        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.formaPago.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.formaPago.strCodigo = this.objDataSetSingle.ItemArray[1].ToString();
                this.formaPago.strDescripcion = this.objDataSetSingle.ItemArray[2].ToString();
                this.formaPago.strEstatus = this.objDataSetSingle.ItemArray[3].ToString();
            }

        }

        public void limpiarProps()
        {
            this.formaPago.limpiarProps();
        }
    }
}