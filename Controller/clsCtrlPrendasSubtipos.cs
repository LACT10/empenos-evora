﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
   public class clsCtrlPrendasSubtipos : Core.clsCore
    {
        
        public Model.clsMdlPrendasSubtipos mdlPrendasSubtipos;
        public Properties_Class.clsPrendasSubtipo prendasSubtipos;


        public clsCtrlPrendasSubtipos()
        {
            this.prendasSubtipos = new Properties_Class.clsPrendasSubtipo();
        }

        public void Guardar()
        {
            this.mdlPrendasSubtipos = new Model.clsMdlPrendasSubtipos();
            this.prendasSubtipos.agregarCero();
            this.mdlPrendasSubtipos.guardar(this.prendasSubtipos);
        }

        public void Eliminar()
        {
            if (this.prendasSubtipos.intId == 0) return;
            this.mdlPrendasSubtipos = new Model.clsMdlPrendasSubtipos();
            //Hacer un llamado a la funcion para desactivar el registro
            this.prendasSubtipos.cambioEstatus(this.prendasSubtipos.dicEstatus["INACTIVO"]);
            this.mdlPrendasSubtipos.eliminar(this.prendasSubtipos);
        }

        public void Buscar(string strCodigo)
        {
            if (strCodigo.Length > 0)
            {
                this.mdlPrendasSubtipos = new Model.clsMdlPrendasSubtipos();
                this.mdlPrendasSubtipos.buscar(strCodigo);

                if (this.mdlPrendasSubtipos.bolResultado)
                {
                    this.prendasSubtipos.intId = Int32.Parse(this.mdlPrendasSubtipos.objResultado.Tables[0].Rows[0][0].ToString());
                    this.prendasSubtipos.strCodigo = this.mdlPrendasSubtipos.objResultado.Tables[0].Rows[0][1].ToString();
                    this.prendasSubtipos.strDescripcion = this.mdlPrendasSubtipos.objResultado.Tables[0].Rows[0][2].ToString();
                    this.prendasSubtipos.strEstatus = this.mdlPrendasSubtipos.objResultado.Tables[0].Rows[0][3].ToString();
                    this.prendasSubtipos.prendasTipo.intId = Int32.Parse(this.mdlPrendasSubtipos.objResultado.Tables[0].Rows[0][4].ToString());
                    this.prendasSubtipos.prendasTipo.strCodigo = this.mdlPrendasSubtipos.objResultado.Tables[0].Rows[0][5].ToString();
                    this.prendasSubtipos.prendasTipo.strDescripcion = this.mdlPrendasSubtipos.objResultado.Tables[0].Rows[0][6].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }
        }

        public void BuscarPrendaTipoId(string strCodigo, string strPrendaTipoId)
        {
            if (strCodigo.Length > 0)
            {
                this.mdlPrendasSubtipos = new Model.clsMdlPrendasSubtipos();
                this.mdlPrendasSubtipos.buscarPrendaTipoId(strCodigo, strPrendaTipoId);
                this.bolResultado = this.mdlPrendasSubtipos.bolResultado;
                if (this.mdlPrendasSubtipos.bolResultado)
                {
                    this.prendasSubtipos.intId = Int32.Parse(this.mdlPrendasSubtipos.objResultado.Tables[0].Rows[0][0].ToString());
                    this.prendasSubtipos.strCodigo = this.mdlPrendasSubtipos.objResultado.Tables[0].Rows[0][1].ToString();
                    this.prendasSubtipos.strDescripcion = this.mdlPrendasSubtipos.objResultado.Tables[0].Rows[0][2].ToString();
                    this.prendasSubtipos.strEstatus = this.mdlPrendasSubtipos.objResultado.Tables[0].Rows[0][3].ToString();
                    this.prendasSubtipos.prendasTipo.intId = Int32.Parse(this.mdlPrendasSubtipos.objResultado.Tables[0].Rows[0][4].ToString());
                    this.prendasSubtipos.prendasTipo.strCodigo = this.mdlPrendasSubtipos.objResultado.Tables[0].Rows[0][5].ToString();
                    this.prendasSubtipos.prendasTipo.strDescripcion = this.mdlPrendasSubtipos.objResultado.Tables[0].Rows[0][6].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }
        }


        
        public void Paginacion()
        {
            this.mdlPrendasSubtipos = new Model.clsMdlPrendasSubtipos();
            this.mdlPrendasSubtipos.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlPrendasSubtipos.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlPrendasSubtipos.bolResultado; ;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlPrendasSubtipos.objResultado;

        }

        public void PaginacionPrendaTipoId(string strPrendaTipoId)
        {
            this.mdlPrendasSubtipos = new Model.clsMdlPrendasSubtipos();
            this.mdlPrendasSubtipos.paginacionPrendaTipoId(strPrendaTipoId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlPrendasSubtipos.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlPrendasSubtipos.bolResultado; ;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlPrendasSubtipos.objResultado;

        }

        
        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de subtipos de prendas";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = "descripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   "  codigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                   "  prendaDescripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                   "  prendaTipoCodigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%'";
            frmBusqueda.lsBloquearCol = new List<string>()
            {
                    "prenda_subtipo_id",
                    "codigo",
                    "descripcion",
                    "estatus",
                    "prenda_tipo_id",
                    "prendaTipoCodigo",
                    "prendaDescripcion"
            };
            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.prendasSubtipos.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.prendasSubtipos.strCodigo = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.prendasSubtipos.strDescripcion = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.prendasSubtipos.strEstatus = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                    this.prendasSubtipos.prendasTipo.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[4].Value.ToString());
                    this.prendasSubtipos.prendasTipo.strCodigo = frmBusqueda.rowsIndex.Cells[5].Value.ToString();
                    this.prendasSubtipos.prendasTipo.strDescripcion = frmBusqueda.rowsIndex.Cells[6].Value.ToString();
                }
                catch (Exception e) { }
            }
        }

        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.prendasSubtipos.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.prendasSubtipos.strCodigo = this.objDataSetSingle.ItemArray[1].ToString();
                this.prendasSubtipos.strDescripcion = this.objDataSetSingle.ItemArray[2].ToString();
                this.prendasSubtipos.strEstatus = this.objDataSetSingle.ItemArray[3].ToString();
                this.prendasSubtipos.prendasTipo.intId = Int32.Parse(this.objDataSetSingle.ItemArray[4].ToString());
                this.prendasSubtipos.prendasTipo.strCodigo = this.objDataSetSingle.ItemArray[5].ToString();
                this.prendasSubtipos.prendasTipo.strDescripcion = this.objDataSetSingle.ItemArray[6].ToString();
            }

        }

        public void limpiarProps()
        {
            this.prendasSubtipos.limpiarProps();
        }
    }
}
