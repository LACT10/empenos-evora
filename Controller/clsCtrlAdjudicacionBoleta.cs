﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlAdjudicacionBoleta : Core.clsCore
    {
        

        public Model.clsMdlAdjudicacionBoleta mdlAdjudicacionBoleta; 
        
        public Properties_Class.clsAdjudicacionBoletas adjudicacionBoletas;


        public clsCtrlAdjudicacionBoleta()
        {
            this.adjudicacionBoletas = new Properties_Class.clsAdjudicacionBoletas();
        }

        public void Guardar()
        {
            this.mdlAdjudicacionBoleta = new Model.clsMdlAdjudicacionBoleta();
            this.mdlAdjudicacionBoleta.guardar(this.adjudicacionBoletas);
        }


        public void limpiarProps()
        {
            this.adjudicacionBoletas.limpiarProps();
        }
    }
}
