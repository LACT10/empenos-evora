﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlClientes : Core.clsCore
    {
        public Model.clsMdlClientes mdlClientes;
        public Properties_Class.clsCliente cliente;

        public clsCtrlClientes()
        {
            this.cliente = new Properties_Class.clsCliente();
        }

        public void Guardar()
        {
            this.mdlClientes = new Model.clsMdlClientes();
            this.cliente.agregarCero();
            this.mdlClientes.guardar(this.cliente);

        }

        public void Eliminar()
        {
            if (this.cliente.intId == 0) return;
            this.mdlClientes = new Model.clsMdlClientes();
            //Hacer un llamado a la funcion para desactivar el registro
            this.cliente.cambioEstatus(this.cliente.dicEstatus["INACTIVO"]);
            this.mdlClientes.eliminar(this.cliente);
        }

        public void Buscar(string strCodigo)
        {
            this.mdlClientes = new Model.clsMdlClientes();

            if (strCodigo.Length > 0)
            {
               
                this.mdlClientes.buscar(strCodigo);

                if (this.mdlClientes.bolResultado)
                {

                    this.cliente.intId = Int32.Parse(this.mdlClientes.objResultado.Tables[0].Rows[0][0].ToString());                    
                    this.cliente.strCodigo = this.mdlClientes.objResultado.Tables[0].Rows[0][1].ToString();
                    this.cliente.intSucursal = Int32.Parse(this.mdlClientes.objResultado.Tables[0].Rows[0][2].ToString());
                    this.cliente.strNombre = this.mdlClientes.objResultado.Tables[0].Rows[0][3].ToString();
                    this.cliente.strApellidoPaterno = this.mdlClientes.objResultado.Tables[0].Rows[0][4].ToString();
                    this.cliente.strApellidoMaterno = this.mdlClientes.objResultado.Tables[0].Rows[0][5].ToString();
                    this.cliente.dtFechaNacimiento = Convert.ToDateTime(this.mdlClientes.objResultado.Tables[0].Rows[0][6].ToString());
                    this.cliente.strGenero = this.mdlClientes.objResultado.Tables[0].Rows[0][7].ToString();
                    this.cliente.strRFC = this.mdlClientes.objResultado.Tables[0].Rows[0][8].ToString();
                    this.cliente.strCURP = this.mdlClientes.objResultado.Tables[0].Rows[0][9].ToString();
                    this.cliente.strTelefono01 = this.mdlClientes.objResultado.Tables[0].Rows[0][10].ToString();
                    this.cliente.strTelefono02 = this.mdlClientes.objResultado.Tables[0].Rows[0][11].ToString();
                    this.cliente.strCorreoElectronico = this.mdlClientes.objResultado.Tables[0].Rows[0][12].ToString();
                    this.cliente.strCalle = this.mdlClientes.objResultado.Tables[0].Rows[0][13].ToString();
                    this.cliente.strNumeroExterior = this.mdlClientes.objResultado.Tables[0].Rows[0][14].ToString();
                    this.cliente.strNumeroInterior = this.mdlClientes.objResultado.Tables[0].Rows[0][15].ToString();
                    this.cliente.strCotitularNombre = this.mdlClientes.objResultado.Tables[0].Rows[0][16].ToString();
                    this.cliente.strCotitularApellidoPaterno = this.mdlClientes.objResultado.Tables[0].Rows[0][17].ToString();
                    this.cliente.strCotitularApellidoMaterno = this.mdlClientes.objResultado.Tables[0].Rows[0][18].ToString();
                    this.cliente.strBeneficiarioNombre = this.mdlClientes.objResultado.Tables[0].Rows[0][19].ToString();
                    this.cliente.strBeneficiarioApellidoPaterno = this.mdlClientes.objResultado.Tables[0].Rows[0][20].ToString();
                    this.cliente.strBeneficiarioApellidoMaterno = this.mdlClientes.objResultado.Tables[0].Rows[0][21].ToString();
                    this.cliente.strComentario = this.mdlClientes.objResultado.Tables[0].Rows[0][22].ToString();
                    this.cliente.strFotografiaImagen = this.mdlClientes.objResultado.Tables[0].Rows[0][23].ToString();
                    this.cliente.strIdentificacionImagenFrente = this.mdlClientes.objResultado.Tables[0].Rows[0][24].ToString();
                    this.cliente.strIdentificacionImagenReverso = this.mdlClientes.objResultado.Tables[0].Rows[0][25].ToString();
                    if (this.mdlClientes.objResultado.Tables[0].Rows[0][26].ToString() == "")
                    {
                        this.cliente.ocupacion.intId = 0;
                    }
                    else 
                    {
                        this.cliente.ocupacion.intId = Int32.Parse(this.mdlClientes.objResultado.Tables[0].Rows[0][26].ToString());
                    }
                    
                    this.cliente.ocupacion.strCodigo = this.mdlClientes.objResultado.Tables[0].Rows[0][27].ToString();
                    this.cliente.ocupacion.strDescripcion = this.mdlClientes.objResultado.Tables[0].Rows[0][28].ToString();
                    this.cliente.identificacionesTipo.intId = Int32.Parse(this.mdlClientes.objResultado.Tables[0].Rows[0][29].ToString());
                    this.cliente.identificacionesTipo.strCodigo = this.mdlClientes.objResultado.Tables[0].Rows[0][30].ToString();
                    this.cliente.identificacionesTipo.strDescripcion = this.mdlClientes.objResultado.Tables[0].Rows[0][31].ToString();
                    this.cliente.asentamiento.intId = Int32.Parse(this.mdlClientes.objResultado.Tables[0].Rows[0][32].ToString());
                    this.cliente.asentamiento.strDescripcion = this.mdlClientes.objResultado.Tables[0].Rows[0][33].ToString();
                    this.cliente.asentamiento.strCiudad = this.mdlClientes.objResultado.Tables[0].Rows[0][34].ToString();
                    this.cliente.asentamiento.strCodigoPostal = this.mdlClientes.objResultado.Tables[0].Rows[0][35].ToString();
                    this.cliente.asentamiento.municipio.intId = Int32.Parse(this.mdlClientes.objResultado.Tables[0].Rows[0][36].ToString());
                    this.cliente.asentamiento.municipio.strCodigo = this.mdlClientes.objResultado.Tables[0].Rows[0][37].ToString();
                    this.cliente.asentamiento.municipio.strDescripcion = this.mdlClientes.objResultado.Tables[0].Rows[0][38].ToString();
                    this.cliente.asentamiento.municipio.estado.intId = Int32.Parse(this.mdlClientes.objResultado.Tables[0].Rows[0][39].ToString());
                    this.cliente.asentamiento.municipio.estado.strCodigo = this.mdlClientes.objResultado.Tables[0].Rows[0][40].ToString();
                    this.cliente.asentamiento.municipio.estado.strDescripcion = this.mdlClientes.objResultado.Tables[0].Rows[0][41].ToString();
                    this.cliente.strEstatus = this.mdlClientes.objResultado.Tables[0].Rows[0][42].ToString();
                    this.cliente.strNumeroIdentificacion = this.mdlClientes.objResultado.Tables[0].Rows[0][43].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.mdlClientes.bolResultado = false;
                this.limpiarProps();
            }
        }

        public void BuscarCurp() 
        { 
            this.mdlClientes = new Model.clsMdlClientes();
            this.mdlClientes.buscarCurp(this.cliente);
            if (this.mdlClientes.bolResultado)
            {

                this.cliente.strCURP = this.mdlClientes.objResultado.Tables[0].Rows[0][0].ToString();
            }
            else
            {
                this.limpiarProps();
            }

        }


        /*public bool BuscarHuella(DPFP.Verification.Verification verificador, DPFP.Template template, DPFP.FeatureSet caracteristicas) 
        {

            bool bolResultado = false;
            if (this.bolResultado)
            {

                //Variable que se utiliza para asignar resultado de la verificación de huella digital
                DPFP.Verification.Verification.Result resultado = new DPFP.Verification.Verification.Result();
                foreach (DataRow row in this.objDataSet.Tables[0].Rows)
                {
                    byte[] huella = (byte[])row["huella"];
                    //Obtener secuencia de bytes de la huella del registro
                    MemoryStream memoria = new MemoryStream(huella);
                    //Deserializar plantilla para comparar caracteristicas de la huella 
                    template.DeSerialize(memoria.ToArray());
                    //Comparar el conjunto de características con nuestra plantilla
                    verificador.Verify(caracteristicas, template, ref resultado);
                    //Si huella es verificada
                    if (resultado.Verified)
                    {
                        bolResultado = true;                        
                        this.objDataSetSingle = row;
                        this.lLenarObjecto();
                        break;
                    }


                }
            }
            return bolResultado;


        }*/

        public void BuscarHuella(DataRow row)
        {

          
            this.objDataSetSingle = row;
            this.lLenarObjecto();
                 
        }



        public void Paginacion()
        {
            this.mdlClientes = new Model.clsMdlClientes();
            this.mdlClientes.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlClientes.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlClientes.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlClientes.objResultado;

        }

        //public void PaginacionHuella


        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de clientes";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = "nombre Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   "  codigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                   "  estatus Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                   "  estadoDescripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                   " estadoCodigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%'";

            frmBusqueda.lsBloquearCol = new List<string>()
            {
                "cliente_id",
                "codigo",
                "sucursal_id",
                "nombre",
                "apellido_paterno",
                "apellido_materno",
                "fecha_nacimiento",
                "genero",
                "rfc",
                "curp",
                "telefono_01",
                "telefono_02",
                "correo_electronico",
                "calle",
                "numero_exterior",
                "numero_interior",
                "cotitular_nombre",
                "cotitular_apellido_paterno",
                "cotitular_apellido_materno",
                "beneficiario_nombre",
                "beneficiario_apellido_paterno",
                "beneficiario_apellido_materno",
                "comentarios",
                "fotografia_imagen",
                "identificacion_imagen_frente",
                "identificacion_imagen_reverso",
                "ocupacion_id",
                "ocupacionCodigo",
                "ocupacionDescripcion",
                "identificacion_tipo_id",
                "identificacionesTiposCodigo",
                "identificacionesTiposDescripcion",
                "asentamiento_id",
                "asentamientoDescripcion",
                "ciudad",
                "codigo_postal",
                "municipio_id",
                "municipioCodigo",
                "municipioDescripcion",
                "estado_id",
                "estadoCodigo",
                "estadoDescripcion",
                "huella",           
                "estatus",
                "numero_identificacion"
             };
            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.cliente.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.cliente.strCodigo = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.cliente.intSucursal = Int32.Parse(frmBusqueda.rowsIndex.Cells[2].Value.ToString());
                    this.cliente.strNombre = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                    this.cliente.strApellidoPaterno = frmBusqueda.rowsIndex.Cells[4].Value.ToString();
                    this.cliente.strApellidoMaterno = frmBusqueda.rowsIndex.Cells[5].Value.ToString();
                    this.cliente.dtFechaNacimiento = Convert.ToDateTime(frmBusqueda.rowsIndex.Cells[6].Value.ToString());
                    this.cliente.strGenero = frmBusqueda.rowsIndex.Cells[7].Value.ToString();
                    this.cliente.strRFC = frmBusqueda.rowsIndex.Cells[8].Value.ToString();
                    this.cliente.strCURP = frmBusqueda.rowsIndex.Cells[9].Value.ToString();
                    this.cliente.strTelefono01 = frmBusqueda.rowsIndex.Cells[10].Value.ToString();
                    this.cliente.strTelefono02 = frmBusqueda.rowsIndex.Cells[11].Value.ToString();
                    this.cliente.strCorreoElectronico = frmBusqueda.rowsIndex.Cells[12].Value.ToString();
                    this.cliente.strCalle = frmBusqueda.rowsIndex.Cells[13].Value.ToString();
                    this.cliente.strNumeroExterior = frmBusqueda.rowsIndex.Cells[14].Value.ToString();
                    this.cliente.strNumeroInterior = frmBusqueda.rowsIndex.Cells[15].Value.ToString();
                    this.cliente.strCotitularNombre = frmBusqueda.rowsIndex.Cells[16].Value.ToString();
                    this.cliente.strCotitularApellidoPaterno = frmBusqueda.rowsIndex.Cells[17].Value.ToString();
                    this.cliente.strCotitularApellidoMaterno = frmBusqueda.rowsIndex.Cells[18].Value.ToString();
                    this.cliente.strBeneficiarioNombre = frmBusqueda.rowsIndex.Cells[19].Value.ToString();
                    this.cliente.strBeneficiarioApellidoPaterno = frmBusqueda.rowsIndex.Cells[20].Value.ToString();
                    this.cliente.strBeneficiarioApellidoMaterno = frmBusqueda.rowsIndex.Cells[21].Value.ToString();
                    this.cliente.strComentario = frmBusqueda.rowsIndex.Cells[22].Value.ToString();
                    this.cliente.strFotografiaImagen = frmBusqueda.rowsIndex.Cells[23].Value.ToString();
                    this.cliente.strIdentificacionImagenFrente = frmBusqueda.rowsIndex.Cells[24].Value.ToString();
                    this.cliente.strIdentificacionImagenReverso = frmBusqueda.rowsIndex.Cells[25].Value.ToString();
                    if (frmBusqueda.rowsIndex.Cells[26].Value.ToString() != "")
                    {
                        this.cliente.ocupacion.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[26].Value.ToString());
                    }

                    this.cliente.ocupacion.strCodigo = frmBusqueda.rowsIndex.Cells[27].Value.ToString();
                    this.cliente.ocupacion.strDescripcion = frmBusqueda.rowsIndex.Cells[28].Value.ToString();
                    this.cliente.identificacionesTipo.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[29].Value.ToString());
                    this.cliente.identificacionesTipo.strCodigo = frmBusqueda.rowsIndex.Cells[30].Value.ToString();
                    this.cliente.identificacionesTipo.strDescripcion = frmBusqueda.rowsIndex.Cells[31].Value.ToString();
                    this.cliente.asentamiento.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[32].Value.ToString());
                    this.cliente.asentamiento.strDescripcion = frmBusqueda.rowsIndex.Cells[33].Value.ToString();
                    this.cliente.asentamiento.strCiudad = frmBusqueda.rowsIndex.Cells[34].Value.ToString();
                    this.cliente.asentamiento.strCodigoPostal = frmBusqueda.rowsIndex.Cells[35].Value.ToString();
                    this.cliente.asentamiento.municipio.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[36].Value.ToString());
                    this.cliente.asentamiento.municipio.strCodigo = frmBusqueda.rowsIndex.Cells[37].Value.ToString();
                    this.cliente.asentamiento.municipio.strDescripcion = frmBusqueda.rowsIndex.Cells[38].Value.ToString();
                    this.cliente.asentamiento.municipio.estado.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[39].Value.ToString());
                    this.cliente.asentamiento.municipio.estado.strCodigo = frmBusqueda.rowsIndex.Cells[40].Value.ToString();
                    this.cliente.asentamiento.municipio.estado.strDescripcion = frmBusqueda.rowsIndex.Cells[41].Value.ToString();
                    this.cliente.strEstatus = frmBusqueda.rowsIndex.Cells[42].Value.ToString();
                    this.cliente.strNumeroIdentificacion = frmBusqueda.rowsIndex.Cells[43].Value.ToString();
                    this.intCountIndex = int.Parse(this.cliente.strCodigo) - 1;
                }
                catch (Exception e) { }

            }
        }

        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.cliente.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.cliente.strCodigo = this.objDataSetSingle.ItemArray[1].ToString();
                this.cliente.intSucursal = Int32.Parse(this.objDataSetSingle.ItemArray[2].ToString());
                this.cliente.strNombre = this.objDataSetSingle.ItemArray[3].ToString();
                this.cliente.strApellidoPaterno = this.objDataSetSingle.ItemArray[4].ToString();
                this.cliente.strApellidoMaterno = this.objDataSetSingle.ItemArray[5].ToString();
                this.cliente.dtFechaNacimiento = Convert.ToDateTime(this.objDataSetSingle.ItemArray[6].ToString());
                this.cliente.strGenero = this.objDataSetSingle.ItemArray[7].ToString();
                this.cliente.strRFC = this.objDataSetSingle.ItemArray[8].ToString();
                this.cliente.strCURP = this.objDataSetSingle.ItemArray[9].ToString();
                this.cliente.strTelefono01 = this.objDataSetSingle.ItemArray[10].ToString();
                this.cliente.strTelefono02 = this.objDataSetSingle.ItemArray[11].ToString();
                this.cliente.strCorreoElectronico = this.objDataSetSingle.ItemArray[12].ToString();
                this.cliente.strCalle = this.objDataSetSingle.ItemArray[13].ToString();
                this.cliente.strNumeroExterior = this.objDataSetSingle.ItemArray[14].ToString();
                this.cliente.strNumeroInterior = this.objDataSetSingle.ItemArray[15].ToString();
                this.cliente.strCotitularNombre = this.objDataSetSingle.ItemArray[16].ToString();
                this.cliente.strCotitularApellidoPaterno = this.objDataSetSingle.ItemArray[17].ToString();
                this.cliente.strCotitularApellidoMaterno = this.objDataSetSingle.ItemArray[18].ToString();
                this.cliente.strBeneficiarioNombre = this.objDataSetSingle.ItemArray[19].ToString();
                this.cliente.strBeneficiarioApellidoPaterno = this.objDataSetSingle.ItemArray[20].ToString();
                this.cliente.strBeneficiarioApellidoMaterno = this.objDataSetSingle.ItemArray[21].ToString();
                this.cliente.strComentario = this.objDataSetSingle.ItemArray[22].ToString();
                this.cliente.strFotografiaImagen = this.objDataSetSingle.ItemArray[23].ToString();
                this.cliente.strIdentificacionImagenFrente = this.objDataSetSingle.ItemArray[24].ToString();
                this.cliente.strIdentificacionImagenReverso = this.objDataSetSingle.ItemArray[25].ToString();
                if (this.objDataSetSingle.ItemArray[26].ToString() == "")
                {
                    this.cliente.ocupacion.intId = 0;
                }
                else 
                {
                    this.cliente.ocupacion.intId = Int32.Parse(this.objDataSetSingle.ItemArray[26].ToString());
                }
                
                this.cliente.ocupacion.strCodigo = this.objDataSetSingle.ItemArray[27].ToString();
                this.cliente.ocupacion.strDescripcion = this.objDataSetSingle.ItemArray[28].ToString();
                this.cliente.identificacionesTipo.intId = Int32.Parse(this.objDataSetSingle.ItemArray[29].ToString());
                this.cliente.identificacionesTipo.strCodigo = this.objDataSetSingle.ItemArray[30].ToString();
                this.cliente.identificacionesTipo.strDescripcion = this.objDataSetSingle.ItemArray[31].ToString();
                this.cliente.asentamiento.intId = Int32.Parse(this.objDataSetSingle.ItemArray[32].ToString());
                this.cliente.asentamiento.strDescripcion = this.objDataSetSingle.ItemArray[33].ToString();
                this.cliente.asentamiento.strCiudad = this.objDataSetSingle.ItemArray[34].ToString();
                this.cliente.asentamiento.strCodigoPostal = this.objDataSetSingle.ItemArray[35].ToString();
                this.cliente.asentamiento.municipio.intId = Int32.Parse(this.objDataSetSingle.ItemArray[36].ToString());
                this.cliente.asentamiento.municipio.strCodigo = this.objDataSetSingle.ItemArray[37].ToString();
                this.cliente.asentamiento.municipio.strDescripcion = this.objDataSetSingle.ItemArray[38].ToString();
                this.cliente.asentamiento.municipio.estado.intId = Int32.Parse(this.objDataSetSingle.ItemArray[39].ToString());
                this.cliente.asentamiento.municipio.estado.strCodigo = this.objDataSetSingle.ItemArray[40].ToString();
                this.cliente.asentamiento.municipio.estado.strDescripcion = this.objDataSetSingle.ItemArray[41].ToString();
                this.cliente.strEstatus = this.objDataSetSingle.ItemArray[42].ToString();
                this.cliente.strNumeroIdentificacion = this.objDataSetSingle.ItemArray[43].ToString();
            }

        }

        public void limpiarProps()
        {
            this.cliente.limpiarProps();
        }
    }
}
