﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlEmpresas : Core.clsCore
    {

        public Model.clsMdlEmpresas mdlEmpresas;

        public Properties_Class.clsEmpresa empresa;

        public clsCtrlEmpresas()
        {
            this.empresa = new Properties_Class.clsEmpresa();
        }
        public void Guardar()
        {
            this.mdlEmpresas = new Model.clsMdlEmpresas();
            this.mdlEmpresas.guardar(this.empresa);
        }

        public void Eliminar()
        {
            if (empresa.intId == 0) return;
            this.mdlEmpresas = new Model.clsMdlEmpresas();
            //Hacer un llamado a la funcion para desactivar el registro
            this.empresa.cambioEstatus(this.empresa.dicEstatus["INACTIVO"]);
            this.mdlEmpresas.eliminar(this.empresa);
        }

        public void Buscar(string strCodigo)
        {
            if (strCodigo.Length > 0)
            {
                this.mdlEmpresas = new Model.clsMdlEmpresas();
                this.mdlEmpresas.buscar(strCodigo);

                if (this.mdlEmpresas.bolResultado)
                {
                    this.empresa.intId = Int32.Parse(this.mdlEmpresas.objResultado.Tables[0].Rows[0][0].ToString());
                    this.empresa.strRazonSocial = this.mdlEmpresas.objResultado.Tables[0].Rows[0][1].ToString();
                    this.empresa.strNombreComercial = this.mdlEmpresas.objResultado.Tables[0].Rows[0][2].ToString();
                    this.empresa.strRFC = this.mdlEmpresas.objResultado.Tables[0].Rows[0][3].ToString();
                    this.empresa.strCURP = this.mdlEmpresas.objResultado.Tables[0].Rows[0][4].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
        }

        public void ValidarComercialRFC(string strComercial, string strRFC)
        {
            this.mdlEmpresas = new Model.clsMdlEmpresas();
            this.mdlEmpresas.validarComercialRFC(strComercial, strRFC);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlEmpresas.bolResultado;
        }
        public void PaginacionSucursal(int intSucursalId) 
        {

            if (intSucursalId > 0) 
            {
                this.mdlEmpresas = new Model.clsMdlEmpresas();
                this.mdlEmpresas.paginacionSucursal(intSucursalId);
                //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
                this.bolResultado = this.mdlEmpresas.bolResultado;
                //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
                this.bolResutladoPaginacion = this.mdlEmpresas.bolResultado;
                //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
                this.objDataSet = this.mdlEmpresas.objResultado;
            }
        
        }

        public void Paginacion()
        {
            this.mdlEmpresas = new Model.clsMdlEmpresas();
            this.mdlEmpresas.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlEmpresas.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlEmpresas.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlEmpresas.objResultado;
        }


        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de empresas";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = "razon_social  Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   "  nombre_comercial Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                   "  rfc Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                   " curp Like '%" + frmBusqueda.dicReemplazo["texto"] + "%'";
            frmBusqueda.lsBloquearCol = new List<string>()
            {
                    "empresa_id",
                    "razon_social",
                    "nombre_comercial",
                    "rfc",
                    "curp",
                    "estatus"
            };

            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try {

                    this.empresa.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.empresa.strRazonSocial = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.empresa.strNombreComercial = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.empresa.strRFC = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                    this.empresa.strCURP = frmBusqueda.rowsIndex.Cells[4].Value.ToString();
                }
                catch (Exception e) { 
                }

                

            }
        }

        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {

                //e.empresa_id, e.razon_social, e.nombre_comercial, e.rfc
                this.empresa.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.empresa.strRazonSocial = this.objDataSetSingle.ItemArray[1].ToString();
                this.empresa.strNombreComercial = this.objDataSetSingle.ItemArray[2].ToString();
                this.empresa.strRFC = this.objDataSetSingle.ItemArray[3].ToString();
                this.empresa.strCURP = this.objDataSetSingle.ItemArray[4].ToString();
                this.empresa.strEstatus = this.objDataSetSingle.ItemArray[5].ToString();
            }
            
        }

        public void limpiarProps()
        {
            this.empresa.limpiarProps();
        }
    }
}
