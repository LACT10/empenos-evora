﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlSucursalesCobrosTipos : Core.clsCore
    {        
        public Model.clsMdlSucursalesCobrosTipos mdlSucursalesCobrosTipos;
        
        public Properties_Class.clsSucursalCobrosTipos sucursalCobrosTipos;
        
        public clsCtrlSucursalesCobrosTipos()
        {
            this.sucursalCobrosTipos = new Properties_Class.clsSucursalCobrosTipos();
        }

        public void Buscar(string strSucursalId)
        {
            if (strSucursalId.Length > 0)
            {
                this.mdlSucursalesCobrosTipos = new Model.clsMdlSucursalesCobrosTipos();
                this.mdlSucursalesCobrosTipos.buscar(strSucursalId);
                //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
                this.bolResultado = this.mdlSucursalesCobrosTipos.bolResultado;
                //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
                this.bolResutladoPaginacion = this.mdlSucursalesCobrosTipos.bolResultado;
                //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
                this.objDataSet = this.mdlSucursalesCobrosTipos.objResultado;
            }
        }

        public void Paginacion(int intSucursalId)
        {
            this.mdlSucursalesCobrosTipos = new Model.clsMdlSucursalesCobrosTipos();
            this.mdlSucursalesCobrosTipos.paginacion(intSucursalId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlSucursalesCobrosTipos.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlSucursalesCobrosTipos.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlSucursalesCobrosTipos.objResultado;

        }

        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                /*this.sucursalCobrosTipos.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.sucursalCobrosTipos.strCodigo = this.objDataSetSingle.ItemArray[1].ToString();
                this.sucursalCobrosTipos.strDescripcion = this.objDataSetSingle.ItemArray[2].ToString();
                this.sucursalCobrosTipos.strEstatus = this.objDataSetSingle.ItemArray[3].ToString();*/
            }
            
        }

        public void limpiarProps()
        {
            this.sucursalCobrosTipos.limpiarProps();
        }
    }
}
