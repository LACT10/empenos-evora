﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlOcupacion : Core.clsCore
    {

        public Model.clsMdlOcupacion mdlOcupacion;
        public Properties_Class.clsOcupacion ocupacion;


        public clsCtrlOcupacion()
        {
            this.ocupacion = new Properties_Class.clsOcupacion();
        }

        public void Guardar()
        {
            this.mdlOcupacion = new Model.clsMdlOcupacion();
            this.ocupacion.agregarCero();
            this.mdlOcupacion.guardar(this.ocupacion);
        }

        public void Eliminar()
        {
            if (this.ocupacion.intId == 0) return;
            this.mdlOcupacion = new Model.clsMdlOcupacion();
            //Hacer un llamado a la funcion para desactivar el registro
            this.ocupacion.cambioEstatus(this.ocupacion.dicEstatus["INACTIVO"]);
            this.mdlOcupacion.eliminar(this.ocupacion);
        }

        public void Buscar(string strCodigo)
        {
            if (strCodigo.Length > 0)
            {
                this.mdlOcupacion = new Model.clsMdlOcupacion();
                this.mdlOcupacion.buscar(strCodigo);

                if (this.mdlOcupacion.bolResultado)
                {
                    this.ocupacion.intId = Int32.Parse(this.mdlOcupacion.objResultado.Tables[0].Rows[0][0].ToString());
                    this.ocupacion.strCodigo = this.mdlOcupacion.objResultado.Tables[0].Rows[0][1].ToString();
                    this.ocupacion.strDescripcion = this.mdlOcupacion.objResultado.Tables[0].Rows[0][2].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }
        }

        public void ValidarDescripcion(string strDescripcion)
        {
            this.mdlOcupacion = new Model.clsMdlOcupacion();
            this.mdlOcupacion.validarDescripcion(strDescripcion);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlOcupacion.bolResultado;
        }
        public void Paginacion()
        {
            this.mdlOcupacion = new Model.clsMdlOcupacion();
            this.mdlOcupacion.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlOcupacion.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlOcupacion.bolResultado; ;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlOcupacion.objResultado;

        }
        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de ocupaciones";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = "descripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                    "  codigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' ";
            frmBusqueda.lsBloquearCol = new List<string>()
            {
                    "ocupacion_id",
                    "codigo",
                    "descripcion",
                    "estatus"
            };
            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.ocupacion.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.ocupacion.strCodigo = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.ocupacion.strDescripcion = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.ocupacion.strEstatus = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                }
                catch (Exception e) { }
            }
        }

        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.ocupacion.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.ocupacion.strCodigo = this.objDataSetSingle.ItemArray[1].ToString();
                this.ocupacion.strDescripcion = this.objDataSetSingle.ItemArray[2].ToString();
                this.ocupacion.strEstatus = this.objDataSetSingle.ItemArray[3].ToString();
            }

        }

        public void limpiarProps()
        {
            this.ocupacion.limpiarProps();
        }
    }
}