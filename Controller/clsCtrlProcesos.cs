﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlProcesos : Core.clsCore
    {

        public Model.clsMdlProcesos mdlProcesos;
        public Properties_Class.clsProcesos procesos;

        public clsCtrlProcesos()
        {
            this.procesos = new Properties_Class.clsProcesos();
        }

        public void Guardar()
        {
            this.mdlProcesos = new Model.clsMdlProcesos();            
            this.mdlProcesos.guardar(this.procesos);

        }

        public void Eliminar()
        {
            if (this.procesos.intId == 0) return;
            this.mdlProcesos = new Model.clsMdlProcesos();
            //Hacer un llamado a la funcion para desactivar el registro
            this.procesos.cambioEstatus(this.procesos.dicEstatus["INACTIVO"]);
            this.mdlProcesos.eliminar(this.procesos);
        }

        public void Buscar(string strCodigo)
        {
            if (strCodigo.Length > 0)
            {
                this.mdlProcesos = new Model.clsMdlProcesos();
                this.mdlProcesos.buscar(strCodigo);

                if (this.mdlProcesos.bolResultado)
                {

                   
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }
        }

        public void BuscarDescripcion(string strDescripcion)
        {
                       
            this.mdlProcesos = new Model.clsMdlProcesos();
            this.mdlProcesos.buscarDescripcion(strDescripcion);

            if (this.mdlProcesos.bolResultado)
            {
                this.procesos.intId = Int32.Parse(this.mdlProcesos.objResultado.Tables[0].Rows[0][0].ToString());

            }
            else
            {
                this.limpiarProps();
            }
            
        }

        public void Proceosos() 
        {
            this.mdlProcesos = new Model.clsMdlProcesos();
            this.mdlProcesos.proceosos();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlProcesos.bolResultado;            
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlProcesos.objResultado;
        }

        public void ProceososUsuario(int intUsuarioId, int intSucursalId)
        {
            this.mdlProcesos = new Model.clsMdlProcesos();
            this.mdlProcesos.proceososUsuario(intUsuarioId, intSucursalId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlProcesos.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlProcesos.objResultado;
        }

        public void Paginacion()
        {
            this.mdlProcesos = new Model.clsMdlProcesos();
            this.mdlProcesos.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlProcesos.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlProcesos.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlProcesos.objResultado;

        }


        public void PaginacionPorcentajesCobro()
        {
            this.mdlProcesos = new Model.clsMdlProcesos();
            //this.mdlProcesos.paginacionPorcentajesCobro();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlProcesos.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlProcesos.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlProcesos.objResultado;

        }

        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de Procesos";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = 
                                   "  descripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                   "  nombre_menu Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' ";                                 
            frmBusqueda.lsBloquearCol = new List<string>()
            {
                "proceso_id",
                "proceso_padre_id", 
                "descripcion", 
                "nombre_menu",
                "procesosDescripcion"
            };
            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.procesos.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.procesos.intProcesoPadreId = Int32.Parse(frmBusqueda.rowsIndex.Cells[1].Value.ToString());
                    this.procesos.strDescripcion = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.procesos.strNombreMenu = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                    this.procesos.strProcesoPadreDescripcion = frmBusqueda.rowsIndex.Cells[4].Value.ToString();
                }
                catch (Exception e) { }
            }
        }

        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.procesos.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.procesos.intProcesoPadreId = Int32.Parse(this.objDataSetSingle.ItemArray[1].ToString());
                this.procesos.strDescripcion = this.objDataSetSingle.ItemArray[2].ToString();
                this.procesos.strNombreMenu = this.objDataSetSingle.ItemArray[3].ToString(); 
                this.procesos.strProcesoPadreDescripcion = this.objDataSetSingle.ItemArray[4].ToString();
            }

        }

        public void limpiarProps()
        {
            this.procesos.limpiarProps();            
        }
    }
}
