﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlMovimientosCaja : Core.clsCore
    {

        public Model.clsMdlMovimientosCaja mdlMovimientosCaja;

        public Properties_Class.clsMovimientosCaja movimientosCaja;

        public clsCtrlMovimientosCaja()
        {
            this.movimientosCaja = new Properties_Class.clsMovimientosCaja();
        }

        public void Guardar()
        {
            this.mdlMovimientosCaja = new Model.clsMdlMovimientosCaja();            
            this.mdlMovimientosCaja.guardar(this.movimientosCaja);
        }

        public void Eliminar()
        {
            if (movimientosCaja.intId == 0) return;
            this.mdlMovimientosCaja = new Model.clsMdlMovimientosCaja();
            //Hacer un llamado a la funcion para desactivar el registro
            this.movimientosCaja.cambioEstatus(this.movimientosCaja.dicEstatus["CANCELADO"]);
            this.mdlMovimientosCaja.eliminar(this.movimientosCaja);
            this.bolResultado = this.mdlMovimientosCaja.bolResultado;
        }

        /*public void Reactivar()
        {
            if (movimientosCaja.intId == 0) return;
            this.mdlMovimientosCaja = new Model.clsMdlMovimientosCaja();
            //Hacer un llamado a la funcion para desactivar el registro
            this.movimientosCaja.cambioEstatus(this.movimientosCaja.dicEstatus["ACTIVO"]);
            this.mdlMovimientosCaja.reactivar(this.movimientosCaja);
            this.bolResultado = this.mdlMovimientosCaja.bolResultado;
        }*/

        public void Buscar(string strCodigo)
        {
            if (strCodigo.Length > 0)
            {
                this.mdlMovimientosCaja = new Model.clsMdlMovimientosCaja();
                this.mdlMovimientosCaja.buscar(strCodigo);

                if (this.mdlMovimientosCaja.bolResultado)
                {
                    this.movimientosCaja.intId = Int32.Parse(this.mdlMovimientosCaja.objResultado.Tables[0].Rows[0][0].ToString());
                    this.movimientosCaja.strFolio = this.mdlMovimientosCaja.objResultado.Tables[0].Rows[0][1].ToString();
                    this.movimientosCaja.dtFechaMovimiento = Convert.ToDateTime(this.mdlMovimientosCaja.objResultado.Tables[0].Rows[0][2].ToString());
                    this.movimientosCaja.strHora = Convert.ToDateTime(this.mdlMovimientosCaja.objResultado.Tables[0].Rows[0][2].ToString()).ToString("HH:mm:ss");
                    this.movimientosCaja.strConcepto = this.mdlMovimientosCaja.objResultado.Tables[0].Rows[0][3].ToString();
                    this.movimientosCaja.empleado.strNombre = this.mdlMovimientosCaja.objResultado.Tables[0].Rows[0][4].ToString();
                    this.movimientosCaja.empleado.intId = Int32.Parse(this.mdlMovimientosCaja.objResultado.Tables[0].Rows[0][5].ToString());
                    this.movimientosCaja.caja.intId = Int32.Parse(this.mdlMovimientosCaja.objResultado.Tables[0].Rows[0][6].ToString());
                    this.movimientosCaja.caja.strCodigo = this.mdlMovimientosCaja.objResultado.Tables[0].Rows[0][7].ToString();
                    this.movimientosCaja.caja.strDescripcion = this.mdlMovimientosCaja.objResultado.Tables[0].Rows[0][8].ToString();
                    this.movimientosCaja.strEstatus = this.mdlMovimientosCaja.objResultado.Tables[0].Rows[0][9].ToString();
                    this.movimientosCaja.strMotivosCancelacion = this.mdlMovimientosCaja.objResultado.Tables[0].Rows[0][10].ToString();
                    this.movimientosCaja.strUsuarioEliminacion = this.mdlMovimientosCaja.objResultado.Tables[0].Rows[0][11].ToString();
                    this.movimientosCaja.strFechaEliminacion = this.mdlMovimientosCaja.objResultado.Tables[0].Rows[0][12].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }


        }
        public void Paginacion(int intSucursalId, int intCajaId)
        {
            this.mdlMovimientosCaja = new Model.clsMdlMovimientosCaja();
            this.mdlMovimientosCaja.paginacion(intSucursalId, intCajaId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlMovimientosCaja.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlMovimientosCaja.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlMovimientosCaja.objResultado;

        }

        public void CargarFrmBusqueda()
        {
            frmBusquedaRangoFecha frmBusqueda = new frmBusquedaRangoFecha();
            frmBusqueda.Text = "Búsqueda de movimientos de caja";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = "(folio Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   "  concepto Like '%" + frmBusqueda.dicReemplazo["texto"] + "%') AND (" +
                                    "  fecha_busqueda  >= #" + frmBusqueda.dicReemplazo["fecha"] + "# AND " +
                                    "  fecha_busqueda <= #" + frmBusqueda.dicReemplazo["fecha2"] + "#)";
            frmBusqueda.lsBloquearCol = new List<string>()
            {
                "movimiento_caja_id" ,
                "folio" ,
                "fecha",
                "concepto",
                "nombre_empleado",
                "usuario_id",
                "caja_id",
                "codigo_caja",
                "descripcion_caja",
                "fecha_busqueda",
                "motivo_cancelacion",
                "nombre_empleado_eliminacion",
                "fecha_eliminacion",
                "estatus" 
            };

            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {                
                this.movimientosCaja.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                this.movimientosCaja.strFolio = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                this.movimientosCaja.dtFechaMovimiento = Convert.ToDateTime(frmBusqueda.rowsIndex.Cells[2].Value.ToString());
                this.movimientosCaja.strHora = Convert.ToDateTime(frmBusqueda.rowsIndex.Cells[2].Value.ToString()).ToString("HH:mm:ss");
                this.movimientosCaja.strConcepto = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                this.movimientosCaja.empleado.strNombre = frmBusqueda.rowsIndex.Cells[4].Value.ToString();
                this.movimientosCaja.empleado.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[5].Value.ToString());
                this.movimientosCaja.caja.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[6].Value.ToString());
                this.movimientosCaja.caja.strCodigo = frmBusqueda.rowsIndex.Cells[7].Value.ToString();
                this.movimientosCaja.caja.strDescripcion = frmBusqueda.rowsIndex.Cells[8].Value.ToString();
                this.movimientosCaja.strEstatus = frmBusqueda.rowsIndex.Cells[9].Value.ToString();
                this.movimientosCaja.strMotivosCancelacion = frmBusqueda.rowsIndex.Cells[10].Value.ToString();
                this.movimientosCaja.strUsuarioEliminacion = frmBusqueda.rowsIndex.Cells[11].Value.ToString();
                this.movimientosCaja.strFechaEliminacion = frmBusqueda.rowsIndex.Cells[12].Value.ToString();
                // this.movimientosCaja.intRowIndex = int.Parse(frmBusqueda.rowsIndex.ToString());
            }
        }



        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.movimientosCaja.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.movimientosCaja.strFolio = this.objDataSetSingle.ItemArray[1].ToString();
                this.movimientosCaja.dtFechaMovimiento = Convert.ToDateTime(this.objDataSetSingle.ItemArray[2].ToString());
                this.movimientosCaja.strHora = Convert.ToDateTime(this.objDataSetSingle.ItemArray[2].ToString()).ToString("HH:mm:ss");
                this.movimientosCaja.strConcepto = this.objDataSetSingle.ItemArray[3].ToString();
                this.movimientosCaja.empleado.strNombre = this.objDataSetSingle.ItemArray[4].ToString();
                this.movimientosCaja.empleado.intId = Int32.Parse(this.objDataSetSingle.ItemArray[5].ToString());
                this.movimientosCaja.caja.intId = Int32.Parse(this.objDataSetSingle.ItemArray[6].ToString());
                this.movimientosCaja.caja.strCodigo = this.objDataSetSingle.ItemArray[7].ToString();
                this.movimientosCaja.caja.strDescripcion = this.objDataSetSingle.ItemArray[8].ToString();
                this.movimientosCaja.strEstatus = this.objDataSetSingle.ItemArray[9].ToString();
                this.movimientosCaja.strMotivosCancelacion = this.objDataSetSingle.ItemArray[10].ToString();
                this.movimientosCaja.strUsuarioEliminacion = this.objDataSetSingle.ItemArray[11].ToString();
                this.movimientosCaja.strFechaEliminacion = this.objDataSetSingle.ItemArray[12].ToString();
            }
            

        }

        public void limpiarProps()
        {
            this.movimientosCaja.limpiarProps();
        }
    }
}
