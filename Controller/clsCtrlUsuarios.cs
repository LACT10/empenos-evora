﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlUsuarios : Core.clsCore
    {

        public Model.clsMdlUsuarios mdlUsuario;
        

        public Properties_Class.clsUsuario usuario;

        public clsCtrlUsuarios()
        {
            this.usuario = new Properties_Class.clsUsuario();
        }

        public void Guardar()
        {
            this.mdlUsuario = new Model.clsMdlUsuarios();
            //this.usuario.agregarCero();
            this.mdlUsuario.guardar(this.usuario);
        }

        public void Eliminar()
        {
            if (usuario.intId == 0) return;
            this.mdlUsuario = new Model.clsMdlUsuarios();
            //Hacer un llamado a la funcion para desactivar el registro
            this.usuario.cambioEstatus(this.usuario.dicEstatus["INACTIVO"]);
            this.mdlUsuario.eliminar(this.usuario);
        }

        /*public void Buscar(string strCodigo)
        {
            if (strCodigo.Length > 0)
            {
                this.mdlUsuario = new Model.clsMdlUsuarios();
                this.mdlUsuario.buscar(strCodigo);

                if (this.mdlUsuario.bolResultado)
                {
                    this.usuario.intId = Int32.Parse(this.mdlUsuario.objResultado.Tables[0].Rows[0][0].ToString());
                    this.usuario.strCodigo = this.mdlUsuario.objResultado.Tables[0].Rows[0][1].ToString();
                    this.usuario.strDescripcion = this.mdlUsuario.objResultado.Tables[0].Rows[0][2].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }


        }*/


        public void BuscarHuella(DataRow row)
        {


            this.objDataSetSingle = row;
            this.lLenarObjecto();

        }
        public void Paginacion()
        {
            this.mdlUsuario = new Model.clsMdlUsuarios();
            this.mdlUsuario.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlUsuario.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlUsuario.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlUsuario.objResultado;

        }

        public void BuscarUsuario(string strUsuario) 
        {
            this.mdlUsuario = new Model.clsMdlUsuarios();
            this.mdlUsuario.buscarUsuario(strUsuario);

        }

        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de usuarios";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = " nombre Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   " apellido_paterno Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                   " apellido_materno Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR  " +
                                   " usuario Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR  " +
                                   " correo_electronico Like '%" + frmBusqueda.dicReemplazo["texto"] + "%'  ";
            frmBusqueda.lsBloquearCol = new List<string>()
            {
                    "usuario_id",
                    "nombre",
                    "apellido_paterno",
                    "apellido_materno",
                    "autorizar",
                    "modificar_movimientos",
                    "telefono_01",
                    "telefono_02",
                    "correo_electronico",
                    "usuario",
                    "contrasena",
                    "intentos",
                    "autorizar",
                    "estatus",
                    "huella"
            };

            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {

                    this.usuario.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.usuario.strNombre = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.usuario.strApellidoPaterno = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.usuario.strApellidoMaterno = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                    this.usuario.strTelefono01 = frmBusqueda.rowsIndex.Cells[4].Value.ToString();
                    this.usuario.strTelefono02 = frmBusqueda.rowsIndex.Cells[5].Value.ToString();
                    this.usuario.strCorreoElectronico = frmBusqueda.rowsIndex.Cells[6].Value.ToString();
                    this.usuario.strUsuario = frmBusqueda.rowsIndex.Cells[7].Value.ToString();
                    this.usuario.strContrasena = frmBusqueda.rowsIndex.Cells[8].Value.ToString();
                    //this.usuario.byteIntentos = Byte.Parse(frmBusqueda.rowsIndex.Cells[9].Value.ToString());
                    this.usuario.strAutorizar = frmBusqueda.rowsIndex.Cells[10].Value.ToString();
                    this.usuario.strModificarMovimientos = frmBusqueda.rowsIndex.Cells[11].Value.ToString();
                    //this.usuario.strEstatus = frmBusqueda.rowsIndex.Cells[12].Value.ToString();
                }
                catch (Exception e) { }
            }
        }



        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.usuario.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.usuario.strNombre = this.objDataSetSingle.ItemArray[1].ToString();
                this.usuario.strApellidoPaterno = this.objDataSetSingle.ItemArray[2].ToString();
                this.usuario.strApellidoMaterno = this.objDataSetSingle.ItemArray[3].ToString();
                this.usuario.strTelefono01 = this.objDataSetSingle.ItemArray[4].ToString();
                this.usuario.strTelefono02 = this.objDataSetSingle.ItemArray[5].ToString();
                this.usuario.strCorreoElectronico = this.objDataSetSingle.ItemArray[6].ToString();
                this.usuario.strUsuario = this.objDataSetSingle.ItemArray[7].ToString();
                this.usuario.strContrasena = this.objDataSetSingle.ItemArray[8].ToString();
                this.usuario.byteIntentos = Byte.Parse(this.objDataSetSingle.ItemArray[9].ToString());
                this.usuario.strAutorizar = this.objDataSetSingle.ItemArray[10].ToString();
                this.usuario.strModificarMovimientos = this.objDataSetSingle.ItemArray[11].ToString();
                this.usuario.strEstatus = this.objDataSetSingle.ItemArray[12].ToString();

            }
            

        }

        public void limpiarProps()
        {
            this.usuario.limpiarProps();
        }
    }
}