﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlSucursalPlazo : Core.clsCore
    { 

        public Model.clsMdlSucursalPlazo mdlSucursalesPlazo;

        public Properties_Class.clsSucursalPlazo sucursalPlazo;

        public clsCtrlSucursalPlazo()
        {
            this.sucursalPlazo = new Properties_Class.clsSucursalPlazo();
        }

        public void Buscar(string strSucursalId)
        {
            if (strSucursalId.Length > 0)
            {
                this.mdlSucursalesPlazo = new Model.clsMdlSucursalPlazo();
                this.mdlSucursalesPlazo.buscar(strSucursalId);
                //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
                this.bolResultado = this.mdlSucursalesPlazo.bolResultado;
                //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
                this.bolResutladoPaginacion = this.mdlSucursalesPlazo.bolResultado;
                //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
                this.objDataSet = this.mdlSucursalesPlazo.objResultado;
            }
        }

        public void BuscarSucTipoPrenda()
        {
            if (this.sucursalPlazo.prendasTipo.strCodigo != "")
            {
                this.mdlSucursalesPlazo = new Model.clsMdlSucursalPlazo();
                this.mdlSucursalesPlazo.buscarSucTipoPrenda(this.sucursalPlazo);                                 
                //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
                this.bolResultado = this.mdlSucursalesPlazo.bolResultado;
                //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
                this.bolResutladoPaginacion = this.mdlSucursalesPlazo.bolResultado;
                //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
                this.objDataSet = this.mdlSucursalesPlazo.objResultado;

                if (this.bolResultado)
                {


                    this.objDataSetSingle = this.objDataSet.Tables[0].Rows[0];
                    this.sucursalPlazo.sucursal.intId = int.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                    this.sucursalPlazo.prendasTipo.intId = int.Parse(this.objDataSetSingle.ItemArray[1].ToString());
                    this.sucursalPlazo.ftPlazo = float.Parse(this.objDataSetSingle.ItemArray[2].ToString());
                    this.sucursalPlazo.prendasTipo.strCodigo = this.objDataSetSingle.ItemArray[3].ToString();
                    this.sucursalPlazo.prendasTipo.strDescripcion = this.objDataSetSingle.ItemArray[4].ToString();
                    this.sucursalPlazo.prendasTipo.strCapturarGramaje = this.objDataSetSingle.ItemArray[5].ToString();
                    this.sucursalPlazo.prendasTipo.strCapturarEmailCelular = this.objDataSetSingle.ItemArray[6].ToString();
                    this.sucursalPlazo.prendasTipo.strCapturarSerieVehiculo = this.objDataSetSingle.ItemArray[7].ToString();


                }
                else 
                {
                    this.sucursalPlazo.limpiarProps();
                }

            }
        }


        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de tipos de prendas";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = "descripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   "  codigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                   "  plazo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' ";
            
           frmBusqueda.lsBloquearCol = new List<string>()
            {
                    "sucursal_id",
                    "prenda_tipo_id",
                    "codigo",
                    "capturar_gramaje",
                    "descripcion",
                    "plazo",
                    "capturar_imei_celular",
                    "capturar_serie_vehiculo"


            };
            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();

            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.sucursalPlazo.sucursal.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.sucursalPlazo.prendasTipo.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[1].Value.ToString());
                    this.sucursalPlazo.ftPlazo = float.Parse(frmBusqueda.rowsIndex.Cells[2].Value.ToString());
                    this.sucursalPlazo.prendasTipo.strCodigo = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                    this.sucursalPlazo.prendasTipo.strDescripcion = frmBusqueda.rowsIndex.Cells[4].Value.ToString();
                    this.sucursalPlazo.prendasTipo.strCapturarGramaje = frmBusqueda.rowsIndex.Cells[5].Value.ToString();
                    this.sucursalPlazo.prendasTipo.strCapturarEmailCelular = frmBusqueda.rowsIndex.Cells[6].Value.ToString();
                    this.sucursalPlazo.prendasTipo.strCapturarSerieVehiculo = frmBusqueda.rowsIndex.Cells[7].Value.ToString();
                }
                catch (Exception e) { }


            }
        }

        public void PaginacionSucursalInner(int intSucursalId)
        {
            this.mdlSucursalesPlazo = new Model.clsMdlSucursalPlazo();
            this.mdlSucursalesPlazo.paginacionSucursalInner(intSucursalId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlSucursalesPlazo.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlSucursalesPlazo.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlSucursalesPlazo.objResultado;

        }

        public void Paginacion(int intSucursalId)
        {
            this.mdlSucursalesPlazo = new Model.clsMdlSucursalPlazo();
            this.mdlSucursalesPlazo.paginacion(intSucursalId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlSucursalesPlazo.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlSucursalesPlazo.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlSucursalesPlazo.objResultado;

        }

        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                /*this.sucursalPlazo.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.sucursalPlazo.strCodigo = this.objDataSetSingle.ItemArray[1].ToString();
                this.sucursalPlazo.strDescripcion = this.objDataSetSingle.ItemArray[2].ToString();
                this.sucursalPlazo.strEstatus = this.objDataSetSingle.ItemArray[3].ToString();*/
            }

        }

        public void limpiarProps()
        {
            this.sucursalPlazo.limpiarProps();
        }
    }
}
