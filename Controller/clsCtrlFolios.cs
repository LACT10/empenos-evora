﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlFolios : Core.clsCore
    {

        public Model.clsMdlFolios mdlFolios;

        public Properties_Class.clsFolios folio;

        public clsCtrlFolios()
        {
            this.folio = new Properties_Class.clsFolios();
        }

        public void Guardar()
        {
            this.mdlFolios = new Model.clsMdlFolios();
            //this.folio.agregarCero();
            this.mdlFolios.guardar(this.folio);
        }

        public void Eliminar()
        {
            if (folio.intId == 0) return;
            this.mdlFolios = new Model.clsMdlFolios();
            //Hacer un llamado a la funcion para desactivar el registro
            this.folio.cambioEstatus(this.folio.dicEstatus["INACTIVO"]);
            this.mdlFolios.eliminar(this.folio);
        }
        public void BuscarDescripcion() 
        {
            this.mdlFolios = new Model.clsMdlFolios();
            this.mdlFolios.buscarDescripcion(this.folio);
            if (this.mdlFolios.bolResultado)
            {

                this.folio.strDescripcion = this.mdlFolios.objResultado.Tables[0].Rows[0][0].ToString();
            }
            else
            {
                this.limpiarProps();
            }
        }

        public void BuscarFolioProceso(string strProcesoId, string strSucursalId)
        {
            this.mdlFolios = new Model.clsMdlFolios();
            this.mdlFolios.buscarFolioProceso(strProcesoId, strSucursalId);
            if (this.mdlFolios.bolResultado)
            {


                this.folio.strSerie = this.mdlFolios.objResultado.Tables[0].Rows[0][0].ToString();
                this.folio.intConsecutivo = int.Parse(this.mdlFolios.objResultado.Tables[0].Rows[0][1].ToString());
                this.folio.intId = int.Parse(this.mdlFolios.objResultado.Tables[0].Rows[0][2].ToString());
            }
            else
            {
                this.limpiarProps();
            }
        }
        public void Paginacion()
        {
            this.mdlFolios = new Model.clsMdlFolios();
            this.mdlFolios.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlFolios.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlFolios.bolResultado;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlFolios.objResultado;

        }

        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de estados";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = "descripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   "  serie Like '%" + frmBusqueda.dicReemplazo["texto"] + "%'";
            frmBusqueda.lsBloquearCol = new List<string>()
            {

                    "folio_id",
                    "descripcion",
                    "serie",
                    "consecutivo",
                    "estatus"

            };

            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.folio.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.folio.strDescripcion = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.folio.strSerie = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.folio.intConsecutivo = int.Parse(frmBusqueda.rowsIndex.Cells[3].Value.ToString());
                    //this.folio.strEstatus = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                }
                catch (Exception e) { }
            }
        }



        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.folio.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());                
                this.folio.strDescripcion = this.objDataSetSingle.ItemArray[1].ToString();
                this.folio.strSerie = this.objDataSetSingle.ItemArray[2].ToString();
                this.folio.intConsecutivo = int.Parse(this.objDataSetSingle.ItemArray[3].ToString());
                this.folio.strEstatus = this.objDataSetSingle.ItemArray[4].ToString();

            }
            

        }

        public void limpiarProps()
        {
            this.folio.limpiarProps();
        }
    }
}
