﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlAsentamientosTipos : Core.clsCore
    {

        public Model.clsMdlAsentamientosTipos mdlAsentamientosTipos;

        public Properties_Class.clsAsentamientosTipos asentamientosTipo;

        public clsCtrlAsentamientosTipos()
        {
            this.asentamientosTipo = new Properties_Class.clsAsentamientosTipos();
        }

        public void Guardar()
        {
            this.mdlAsentamientosTipos = new Model.clsMdlAsentamientosTipos();
            this.asentamientosTipo.agregarCero();
            this.mdlAsentamientosTipos.guardar(this.asentamientosTipo);
        }

        public void Eliminar()
        {
            if (this.asentamientosTipo.intId == 0) return;
            this.mdlAsentamientosTipos = new Model.clsMdlAsentamientosTipos();
            //Hacer un llamado a la funcion para desactivar el registro
            this.asentamientosTipo.cambioEstatus(this.asentamientosTipo.dicEstatus["INACTIVO"]);
            this.mdlAsentamientosTipos.eliminar(this.asentamientosTipo);
        }

        public void Buscar(string strCodigo)
        {
            if (strCodigo.Length > 0)
            {
                this.mdlAsentamientosTipos = new Model.clsMdlAsentamientosTipos();
                this.mdlAsentamientosTipos.buscar(strCodigo);

                if (this.mdlAsentamientosTipos.bolResultado)
                {
                    this.asentamientosTipo.intId = Int32.Parse(this.mdlAsentamientosTipos.objResultado.Tables[0].Rows[0][0].ToString());
                    this.asentamientosTipo.strCodigo = this.mdlAsentamientosTipos.objResultado.Tables[0].Rows[0][1].ToString();
                    this.asentamientosTipo.strDescripcion = this.mdlAsentamientosTipos.objResultado.Tables[0].Rows[0][2].ToString();
                    this.asentamientosTipo.strEstatus = this.mdlAsentamientosTipos.objResultado.Tables[0].Rows[0][3].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }
        }

        public void ValidarDescripcion(string strDescripcion)
        {
            this.mdlAsentamientosTipos = new Model.clsMdlAsentamientosTipos();
            this.mdlAsentamientosTipos.validarDescripcion(strDescripcion);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlAsentamientosTipos.bolResultado;
        }

        public void Paginacion()
        {
            this.mdlAsentamientosTipos = new Model.clsMdlAsentamientosTipos();
            this.mdlAsentamientosTipos.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlAsentamientosTipos.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlAsentamientosTipos.bolResultado; ;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlAsentamientosTipos.objResultado;
            
        }
        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda tipos asentamientos";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = "descripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
               "  codigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' ";               
            frmBusqueda.lsBloquearCol = new List<string>()
            {
                    "asentamiento_tipo_id",
                    "codigo",
                    "descripcion",
                    "estatus"
            };
            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.asentamientosTipo.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.asentamientosTipo.strCodigo = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.asentamientosTipo.strDescripcion = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.asentamientosTipo.strEstatus = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                    this.intCountIndex = int.Parse(this.asentamientosTipo.strCodigo) - 1;
                    
                }
                catch (Exception e) { }
            }
        }

        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.asentamientosTipo.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.asentamientosTipo.strCodigo = this.objDataSetSingle.ItemArray[1].ToString();
                this.asentamientosTipo.strDescripcion = this.objDataSetSingle.ItemArray[2].ToString();
                this.asentamientosTipo.strEstatus = this.objDataSetSingle.ItemArray[3].ToString();
            }
            
        }

        public void limpiarProps()
        {
            this.asentamientosTipo.limpiarProps();
        }
    }
}