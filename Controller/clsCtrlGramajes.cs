﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Controller
{
    public class clsCtrlGramajes : Core.clsCore
    {

        public Model.clsMdlGramajes mdlGramajes;
        public Properties_Class.clsGramaje gramaje;


        public clsCtrlGramajes()
        {
            this.gramaje = new Properties_Class.clsGramaje();
        }

        public void Guardar()
        {
            this.mdlGramajes = new Model.clsMdlGramajes();
            this.gramaje.agregarCero();
            this.mdlGramajes.guardar(this.gramaje);
        }

        public void Eliminar()
        {
            if (this.gramaje.intId == 0) return;
            this.mdlGramajes = new Model.clsMdlGramajes();
            //Hacer un llamado a la funcion para desactivar el registro
            this.gramaje.cambioEstatus(this.gramaje.dicEstatus["INACTIVO"]);
            this.mdlGramajes.eliminar(this.gramaje);
        }

        public void Buscar(string strCodigo)
        {
            if (strCodigo.Length > 0)
            {
                this.mdlGramajes = new Model.clsMdlGramajes();
                this.mdlGramajes.buscar(strCodigo);

                if (this.mdlGramajes.bolResultado)
                {
                    this.gramaje.intId = Int32.Parse(this.mdlGramajes.objResultado.Tables[0].Rows[0][0].ToString());
                    this.gramaje.strCodigo = this.mdlGramajes.objResultado.Tables[0].Rows[0][1].ToString();
                    this.gramaje.strDescripcion = this.mdlGramajes.objResultado.Tables[0].Rows[0][2].ToString();
                    this.gramaje.strEstatus = this.mdlGramajes.objResultado.Tables[0].Rows[0][3].ToString();
                    this.gramaje.prendasTipo.intId = Int32.Parse(this.mdlGramajes.objResultado.Tables[0].Rows[0][4].ToString());
                    this.gramaje.prendasTipo.strCodigo = this.mdlGramajes.objResultado.Tables[0].Rows[0][5].ToString();
                    this.gramaje.prendasTipo.strDescripcion = this.mdlGramajes.objResultado.Tables[0].Rows[0][6].ToString();
                }
                else
                {
                    this.limpiarProps();
                }
            }
            else
            {
                this.limpiarProps();
            }
        }

        public void ValidarDescripcion(string strDescripcion)
        {
            this.mdlGramajes = new Model.clsMdlGramajes();
            this.mdlGramajes.validarDescripcion(strDescripcion);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlGramajes.bolResultado;
        }


        public void BuscarPrendaSucursal(string strCodigo, string strTipoPrendaId, string strSucursalId)
        {
            
            
                this.mdlGramajes = new Model.clsMdlGramajes();
                this.mdlGramajes.buscarPrendaSucursal(strCodigo, strTipoPrendaId, strSucursalId);

                if (this.mdlGramajes.bolResultado)
                {                
                    this.gramaje.intId = Int32.Parse(this.mdlGramajes.objResultado.Tables[0].Rows[0][0].ToString());
                    this.gramaje.strCodigo = this.mdlGramajes.objResultado.Tables[0].Rows[0][1].ToString();
                    this.gramaje.strDescripcion = this.mdlGramajes.objResultado.Tables[0].Rows[0][2].ToString();
                    this.gramaje.decPrecioTope = decimal.Parse(this.mdlGramajes.objResultado.Tables[0].Rows[0][3].ToString());
                
                }
                else
                {
                    this.limpiarProps();
                }
            
            
        }

        public void PaginacionPrendaSucursal(string strTipoPrendaId, string strSucursalId) 
        {
            this.mdlGramajes = new Model.clsMdlGramajes();
            this.mdlGramajes.paginacionPrendaSucursal(strTipoPrendaId, strSucursalId);
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlGramajes.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlGramajes.bolResultado; ;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlGramajes.objResultado;
        }
        public void Paginacion()
        {
            this.mdlGramajes = new Model.clsMdlGramajes();
            this.mdlGramajes.paginacion();
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la paginacion.
            this.bolResultado = this.mdlGramajes.bolResultado;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.bolResutladoPaginacion = this.mdlGramajes.bolResultado; ;
            //El dataset es el resultado que tiene valores de la base de datos. Se asignar a otro dataset para controlar la paginacion.
            this.objDataSet = this.mdlGramajes.objResultado;

        }

        public void CargarFrmBusquedaTipoPrendaSuc()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de gramajes";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = "descripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   "  codigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' ";
                                   
                                  
            frmBusqueda.lsBloquearCol = new List<string>()
            {
                    "gramaje_id",
                    "codigo",
                    "descripcion",
                    "precio_tope"
            };
            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.gramaje.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.gramaje.strCodigo = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.gramaje.strDescripcion = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.gramaje.decPrecioTope = decimal.Parse(frmBusqueda.rowsIndex.Cells[3].Value.ToString());                                                           
                }
                catch (Exception e) { }
            }
        }
        public void CargarFrmBusqueda()
        {
            frmBusqueda frmBusqueda = new frmBusqueda();
            frmBusqueda.Text = "Búsqueda de gramajes";
            frmBusqueda.strTipoFormulario = frmBusqueda.dicTipoFormulario["Simple"];
            frmBusqueda.strQuery = "descripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR" +
                                   "  codigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                   "  prendaTipoDescripcion Like '%" + frmBusqueda.dicReemplazo["texto"] + "%' OR " +
                                   " prendaTipoCodigo Like '%" + frmBusqueda.dicReemplazo["texto"] + "%'";
            frmBusqueda.lsBloquearCol = new List<string>()
            {
                    "gramaje_id",
                    "codigo",
                    "descripcion",
                    "estatus",
                    "prenda_tipo_id",
                    "prendaTipoCodigo",
                    "prendaTipoDescripcion"
            };
            frmBusqueda.objDataSet = this.objDataSet;
            frmBusqueda.ShowDialog();
            this.bolResultadoBusqueda = frmBusqueda.bolCierra;
            if (frmBusqueda.bolCierra)
            {
                try
                {
                    this.gramaje.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[0].Value.ToString());
                    this.gramaje.strCodigo = frmBusqueda.rowsIndex.Cells[1].Value.ToString();
                    this.gramaje.strDescripcion = frmBusqueda.rowsIndex.Cells[2].Value.ToString();
                    this.gramaje.strEstatus = frmBusqueda.rowsIndex.Cells[3].Value.ToString();
                    this.gramaje.prendasTipo.intId = Int32.Parse(frmBusqueda.rowsIndex.Cells[4].Value.ToString());
                    this.gramaje.prendasTipo.strCodigo = frmBusqueda.rowsIndex.Cells[5].Value.ToString();
                    this.gramaje.prendasTipo.strDescripcion = frmBusqueda.rowsIndex.Cells[6].Value.ToString();
                }
                catch (Exception e) { }
            }
        }

        public void PrimeroPaginacion()
        {
            this.primerPagina();
            this.lLenarObjecto();
        }

        public void AtrasPaginacion()
        {
            this.atrasPagina();
            this.lLenarObjecto();
        }

        public void SiguentePaginacion()
        {
            this.siguentePagina();
            this.lLenarObjecto();
        }

        public void UltimoPaginacion()
        {
            this.ultimoPagina();
            this.lLenarObjecto();
        }

        public void lLenarObjecto()
        {
            if (this.bolResultado)
            {
                this.gramaje.intId = Int32.Parse(this.objDataSetSingle.ItemArray[0].ToString());
                this.gramaje.strCodigo = this.objDataSetSingle.ItemArray[1].ToString();
                this.gramaje.strDescripcion = this.objDataSetSingle.ItemArray[2].ToString();
                this.gramaje.strEstatus = this.objDataSetSingle.ItemArray[3].ToString();
                this.gramaje.prendasTipo.intId = Int32.Parse(this.objDataSetSingle.ItemArray[4].ToString());
                this.gramaje.prendasTipo.strCodigo = this.objDataSetSingle.ItemArray[5].ToString();
                this.gramaje.prendasTipo.strDescripcion = this.objDataSetSingle.ItemArray[6].ToString();
            }

        }

        public void limpiarProps()
        {
            this.gramaje.limpiarProps();
        }
    }
}
