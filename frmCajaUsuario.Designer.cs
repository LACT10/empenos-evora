﻿namespace EmpenosEvora
{
    partial class frmCajaUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCodigoSucursal = new System.Windows.Forms.TextBox();
            this.btnBusquedaSucursal = new System.Windows.Forms.Button();
            this.txtSucursalId = new System.Windows.Forms.TextBox();
            this.txtEmpresaId = new System.Windows.Forms.TextBox();
            this.btnBusquedaEmpresa = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDescripcionSucursal = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDescripcionEmpresas = new System.Windows.Forms.TextBox();
            this.btnFooterAcceder = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.txtCodigoCajas = new System.Windows.Forms.TextBox();
            this.btnBusquedaCajas = new System.Windows.Forms.Button();
            this.txtCajasId = new System.Windows.Forms.TextBox();
            this.lblCajas = new System.Windows.Forms.Label();
            this.txtDescripcionCajas = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtCodigoSucursal
            // 
            this.txtCodigoSucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoSucursal.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoSucursal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoSucursal.Location = new System.Drawing.Point(72, 40);
            this.txtCodigoSucursal.MaxLength = 5;
            this.txtCodigoSucursal.Name = "txtCodigoSucursal";
            this.txtCodigoSucursal.Size = new System.Drawing.Size(44, 20);
            this.txtCodigoSucursal.TabIndex = 3;
            this.txtCodigoSucursal.Leave += new System.EventHandler(this.txtCodigoSucursal_Leave);
            // 
            // btnBusquedaSucursal
            // 
            this.btnBusquedaSucursal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBusquedaSucursal.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusquedaSucursal.Location = new System.Drawing.Point(264, 40);
            this.btnBusquedaSucursal.Name = "btnBusquedaSucursal";
            this.btnBusquedaSucursal.Size = new System.Drawing.Size(32, 23);
            this.btnBusquedaSucursal.TabIndex = 5;
            this.btnBusquedaSucursal.UseVisualStyleBackColor = true;
            this.btnBusquedaSucursal.Click += new System.EventHandler(this.btnBusquedaSucursal_Click);
            // 
            // txtSucursalId
            // 
            this.txtSucursalId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtSucursalId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSucursalId.Location = new System.Drawing.Point(216, 40);
            this.txtSucursalId.MaxLength = 3;
            this.txtSucursalId.Name = "txtSucursalId";
            this.txtSucursalId.Size = new System.Drawing.Size(44, 20);
            this.txtSucursalId.TabIndex = 571;
            this.txtSucursalId.Visible = false;
            // 
            // txtEmpresaId
            // 
            this.txtEmpresaId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtEmpresaId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmpresaId.Location = new System.Drawing.Point(216, 8);
            this.txtEmpresaId.MaxLength = 3;
            this.txtEmpresaId.Name = "txtEmpresaId";
            this.txtEmpresaId.Size = new System.Drawing.Size(44, 20);
            this.txtEmpresaId.TabIndex = 570;
            this.txtEmpresaId.Visible = false;
            // 
            // btnBusquedaEmpresa
            // 
            this.btnBusquedaEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBusquedaEmpresa.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusquedaEmpresa.Location = new System.Drawing.Point(264, 8);
            this.btnBusquedaEmpresa.Name = "btnBusquedaEmpresa";
            this.btnBusquedaEmpresa.Size = new System.Drawing.Size(32, 23);
            this.btnBusquedaEmpresa.TabIndex = 2;
            this.btnBusquedaEmpresa.UseVisualStyleBackColor = true;
            this.btnBusquedaEmpresa.Click += new System.EventHandler(this.btnBusquedaEmpresa_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 569;
            this.label4.Text = "Sucursal";
            // 
            // txtDescripcionSucursal
            // 
            this.txtDescripcionSucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionSucursal.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionSucursal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcionSucursal.Location = new System.Drawing.Point(120, 40);
            this.txtDescripcionSucursal.MaxLength = 5;
            this.txtDescripcionSucursal.Name = "txtDescripcionSucursal";
            this.txtDescripcionSucursal.Size = new System.Drawing.Size(136, 20);
            this.txtDescripcionSucursal.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 568;
            this.label3.Text = "Empresa";
            // 
            // txtDescripcionEmpresas
            // 
            this.txtDescripcionEmpresas.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionEmpresas.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionEmpresas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcionEmpresas.Location = new System.Drawing.Point(72, 8);
            this.txtDescripcionEmpresas.MaxLength = 5;
            this.txtDescripcionEmpresas.Name = "txtDescripcionEmpresas";
            this.txtDescripcionEmpresas.Size = new System.Drawing.Size(184, 20);
            this.txtDescripcionEmpresas.TabIndex = 1;
            // 
            // btnFooterAcceder
            // 
            this.btnFooterAcceder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterAcceder.Image = global::EmpenosEvora.Properties.Resources.Entry_16x;
            this.btnFooterAcceder.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterAcceder.Location = new System.Drawing.Point(224, 120);
            this.btnFooterAcceder.Name = "btnFooterAcceder";
            this.btnFooterAcceder.Size = new System.Drawing.Size(75, 40);
            this.btnFooterAcceder.TabIndex = 9;
            this.btnFooterAcceder.Text = "Acceder";
            this.btnFooterAcceder.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterAcceder.UseVisualStyleBackColor = true;
            this.btnFooterAcceder.Click += new System.EventHandler(this.btnFooterAcceder_Click);
            // 
            // label15
            // 
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(-329, 104);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(974, 2);
            this.label15.TabIndex = 573;
            // 
            // txtCodigoCajas
            // 
            this.txtCodigoCajas.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigoCajas.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoCajas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoCajas.Location = new System.Drawing.Point(72, 72);
            this.txtCodigoCajas.MaxLength = 5;
            this.txtCodigoCajas.Name = "txtCodigoCajas";
            this.txtCodigoCajas.Size = new System.Drawing.Size(44, 20);
            this.txtCodigoCajas.TabIndex = 6;
            this.txtCodigoCajas.Leave += new System.EventHandler(this.txtCodigoCajas_Leave);
            // 
            // btnBusquedaCajas
            // 
            this.btnBusquedaCajas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBusquedaCajas.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusquedaCajas.Location = new System.Drawing.Point(264, 72);
            this.btnBusquedaCajas.Name = "btnBusquedaCajas";
            this.btnBusquedaCajas.Size = new System.Drawing.Size(32, 23);
            this.btnBusquedaCajas.TabIndex = 8;
            this.btnBusquedaCajas.UseVisualStyleBackColor = true;
            this.btnBusquedaCajas.Click += new System.EventHandler(this.btnBusquedaCajas_Click);
            // 
            // txtCajasId
            // 
            this.txtCajasId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCajasId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCajasId.Location = new System.Drawing.Point(216, 72);
            this.txtCajasId.MaxLength = 3;
            this.txtCajasId.Name = "txtCajasId";
            this.txtCajasId.Size = new System.Drawing.Size(44, 20);
            this.txtCajasId.TabIndex = 578;
            this.txtCajasId.Visible = false;
            // 
            // lblCajas
            // 
            this.lblCajas.AutoSize = true;
            this.lblCajas.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCajas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCajas.Location = new System.Drawing.Point(8, 71);
            this.lblCajas.Name = "lblCajas";
            this.lblCajas.Size = new System.Drawing.Size(33, 13);
            this.lblCajas.TabIndex = 577;
            this.lblCajas.Text = "Cajas";
            // 
            // txtDescripcionCajas
            // 
            this.txtDescripcionCajas.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionCajas.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionCajas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcionCajas.Location = new System.Drawing.Point(120, 72);
            this.txtDescripcionCajas.MaxLength = 5;
            this.txtDescripcionCajas.Name = "txtDescripcionCajas";
            this.txtDescripcionCajas.Size = new System.Drawing.Size(136, 20);
            this.txtDescripcionCajas.TabIndex = 7;
            // 
            // frmCajaUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(316, 168);
            this.Controls.Add(this.txtCodigoCajas);
            this.Controls.Add(this.btnBusquedaCajas);
            this.Controls.Add(this.txtCajasId);
            this.Controls.Add(this.lblCajas);
            this.Controls.Add(this.txtDescripcionCajas);
            this.Controls.Add(this.btnFooterAcceder);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtCodigoSucursal);
            this.Controls.Add(this.btnBusquedaSucursal);
            this.Controls.Add(this.txtSucursalId);
            this.Controls.Add(this.txtEmpresaId);
            this.Controls.Add(this.btnBusquedaEmpresa);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDescripcionSucursal);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtDescripcionEmpresas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCajaUsuario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cajas";
            this.Load += new System.EventHandler(this.frmCajaUsuario_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtCodigoSucursal;
        private System.Windows.Forms.Button btnBusquedaSucursal;
        private System.Windows.Forms.TextBox txtSucursalId;
        private System.Windows.Forms.TextBox txtEmpresaId;
        private System.Windows.Forms.Button btnBusquedaEmpresa;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDescripcionSucursal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDescripcionEmpresas;
        private System.Windows.Forms.Button btnFooterAcceder;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtCodigoCajas;
        private System.Windows.Forms.Button btnBusquedaCajas;
        private System.Windows.Forms.TextBox txtCajasId;
        private System.Windows.Forms.Label lblCajas;
        private System.Windows.Forms.TextBox txtDescripcionCajas;
    }
}