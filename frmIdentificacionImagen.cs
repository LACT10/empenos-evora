﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebCam_Capture;

namespace EmpenosEvora
{
    public partial class frmIdentificacionImagen : Form
    {

        public PictureBox _pbFrontal;
        public PictureBox _pbTrasera;
        Library.clsWebCam webCam;
        public PictureBox pbFrontal { get => _pbFrontal; set => _pbFrontal = value; }
        public PictureBox pbTrasera { get => _pbTrasera; set => _pbTrasera = value; }

        public frmIdentificacionImagen()
        {
            InitializeComponent();
            this.webCam = new Library.clsWebCam();
            this.webCam.InitializeWebCam(ref imgVideoFrontal);
            this.webCam.Start();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void frmIdentificacionImagen_Load(object sender, EventArgs e)
        {

        }

        private void bntCaptureFrontal_Click(object sender, EventArgs e)
        {
            imgCaptureFrontal.Image = imgVideoFrontal.Image;
            this.pbFrontal = imgCaptureFrontal;
            this.webCam.Stop();
        }

        private void bntCaptureTrasera_Click(object sender, EventArgs e)
        {
            imgCaptureTrasera.Image = imgVideoTrasera.Image;
            this.pbTrasera = imgCaptureTrasera;
        }

        private void frmIdentificacionImagen_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.webCam.Stop();
        }

        private void btnIniciarFrontal_Click(object sender, EventArgs e)
        {
            this.webCam.Stop();
            this.webCam.InitializeWebCam(ref imgVideoFrontal);
            this.webCam.Start();
        }

        private void btnIniciarTrasera_Click(object sender, EventArgs e)
        {
            this.webCam.Stop();
            this.webCam.InitializeWebCam(ref imgVideoTrasera);
            this.webCam.Start();
        }

        private void btnGuardarTrasera_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
