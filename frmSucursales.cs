﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmSucursales : Form
    {    
        public Controller.clsCtrlSucursales ctrlSucursales;
        public Controller.clsCtrlSucursalesCobrosTipos ctrlSucursalesCobrosTipos;
        public Controller.clsCtrlSucursalesGramajes ctrlSucursalesGramajes;
        public Controller.clsCtrlSucursalPlazo ctrlSucursalPlazo;
        public Controller.clsCtrlSucursalParametros clsCtrlSucursalParametros;
        public Controller.clsCtrlSucursalesPromocion ctrlSucursalesPromocion;
        public Controller.clsCtrlEmpresas ctrlEmpresas;
        public Controller.clsCtrlAsentamiento ctrlAsentamiento;
        Properties_Class.clsConstante clsCon;
        public Core.clsCoreView clsView;

        public int _intUlitmoCodigo;
        public int _intNumCerosConsecutivo;
        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }

        public int intNumCerosConsecutivo { get => _intNumCerosConsecutivo; set => _intNumCerosConsecutivo = value; }

        public List<Properties_Class.clsSucursal> lsDeshacerSucursal;

        public frmSucursales()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();

            this.ctrlSucursales = new Controller.clsCtrlSucursales();
            this.ctrlSucursales.Paginacion();

            this.ctrlSucursalesPromocion = new Controller.clsCtrlSucursalesPromocion();

            this.ctrlEmpresas = new Controller.clsCtrlEmpresas();
            this.ctrlAsentamiento =  new Controller.clsCtrlAsentamiento();
            this.clsCon =  new Properties_Class.clsConstante();

            this.lsDeshacerSucursal = new List<Properties_Class.clsSucursal>();
            this.lsDeshacerSucursal.Add(this.ctrlSucursales.sucursal);
            //Asignar número de ceros pata generar código consecutivo
            this.intNumCerosConsecutivo = 3;

            btnAgregarPromociones.Visible = false;
            dgvDetallesPromociones.Visible = false;

            this.sucursalesCobrosTipos(0);
            this.sucursalesGramajes(0);
            this.sucursalesPlazo(0);
            this.sucursalParametros(0);                        
        }


        private void FrmSucursales_Load(object sender, EventArgs e)
        {
            tbsControlSucursal.SelectTab(0);
            txtCodigo.MaxLength = this.intNumCerosConsecutivo;
            
            


            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);

            txtCodigo.Focus();
        }

        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void TsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlSucursales.sucursal.intId == 0) return;

            if (this.clsView.confirmacionEliminar(this.Text))
            {
                this.ctrlSucursales.Eliminar();
                if (this.ctrlSucursales.bolResultado)
                {
                    //Hacer un llamado a la funcion para obtener los datos del registro(modificado)
                    this.regresarRegistro("eliminó");
                }
            }
        }

        private void tsbReactivar_Click(object sender, EventArgs e)
        {
            if (this.ctrlSucursales.sucursal.intId == 0) return;

            if (this.clsView.confirmacionAccion(this.Text, "¿Desea reactivar este registro?"))
            {
                this.ctrlSucursales.Reactivar();
                if (this.ctrlSucursales.bolResultado)
                {
                    //Hacer un llamado a la funcion para obtener los datos del registro(modificado)
                    this.regresarRegistro("activó");
                }
            }
        }



        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlSucursales.sucursal.intId > 0)
            {
                llenar_formulario();
            }
            else
            {
                txtCodigo.Enabled = true;
                this.limpiar_textbox();

            }

            txtCodigo.Focus();

       
        }


        private void TsbBuscar_Click(object sender, EventArgs e)
        {
            Dictionary<int, Properties_Class.clsTextbox> dicFocus = new Dictionary<int, Properties_Class.clsTextbox>()
            {
                { 0, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcion, strBusqueda = "sucursal"}
                },
                { 1, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigo, strBusqueda = "sucursal"}
                },
                { 2, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcionEmpresas, strBusqueda = "empresa"}
                },
                { 3, new Properties_Class.clsTextbox()
                                { txtGenerico = txtRepresentanteLegal, strBusqueda = "sucursal"}
                },
                { 4, new Properties_Class.clsTextbox()
                                { txtGenerico = txtTelefono1, strBusqueda = "sucursal"}
                },
                { 5, new Properties_Class.clsTextbox()
                                { txtGenerico = txtTelefono2, strBusqueda = "sucursal"}
                },
                { 6, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcionAsentamiento, strBusqueda = "asentamiento"}
                },
                { 7, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoPostal, strBusqueda = "asentamiento"}
                },
                { 8, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoMunicipio, strBusqueda = "asentamiento"}
                },
                { 9, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcionMunicipio, strBusqueda = "asentamiento"}
                },
                { 10, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoEstado, strBusqueda = "asentamiento"}
                },
                { 11, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcionEstado, strBusqueda = "asentamiento"}
                }

            };
            foreach (KeyValuePair<int, Properties_Class.clsTextbox> data in dicFocus)
            {
                if (data.Value.txtGenerico.Focused)
                {
                    if (data.Value.strBusqueda == "sucursal")
                    {
                        this.mostraBusqueda();
                    }
                    else if (data.Value.strBusqueda == "empresa")
                    {
                        this.mostraBusquedaEmpresas();
                    }
                    else if (data.Value.strBusqueda == "asentamiento")
                    {
                        this.mostraBusquedaAsentamiento();
                    }
                }               
            }

            if (dtpInicioOperaciones.Focused) 
            {
                this.mostraBusqueda();
            }


            
        }


        private void BtnBusqueda_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();
        }



        private void BtnBusquedaEmpresas_Click(object sender, EventArgs e)
        {
            this.mostraBusquedaEmpresas();
        }

        private void BtnBusquedaAsentamiento_Click(object sender, EventArgs e)
        {
            this.mostraBusquedaAsentamiento();
        }



        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {            
            this.ctrlSucursales.PrimeroPaginacion();            
            if (!this.ctrlSucursales.bolResultado) return;

            this.llenar_formulario();
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {            
            this.ctrlSucursales.SiguentePaginacion();
            if (!this.ctrlSucursales.bolResultado) return;
            
            this.llenar_formulario();

        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {            
            this.ctrlSucursales.AtrasPaginacion();
            if (!this.ctrlSucursales.bolResultado) return;
            
            this.llenar_formulario();

        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {            
            this.ctrlSucursales.UltimoPaginacion();
            if (!this.ctrlSucursales.bolResultado) return;
            
            this.llenar_formulario();

        }
        private void TsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();
            this.ctrlSucursales.sucursal.limpiarProps();            
            lblDescEstatus.Visible = true;
            lblEstatus.Text = "NUEVO";
            lblEstatus.Visible = true;

            txtCodigo.Enabled = false;
            txtCodigo.Text = "AUTOGENERADO";
            txtDescripcion.Focus();

           


        }

        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);

        }

        private void txtTelefono1_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtTelefono2_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtNoExt_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void dgvSucursalesCobrosTipos_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            this.editarNum(dgvSucursalesCobrosTipos, e);
        }



        private void dgvSucursalesPlazo_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            this.editarNum(dgvSucursalesPlazo, e);
        }

        private void dgvSucursalesGramajes_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            this.editarNum(dgvSucursalesGramajes, e);
        }


        private void dgvSucursalesCobrosTipos_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {


            this.cellValidatingNum(dgvSucursalesCobrosTipos, e);
        }

        private void dgvSucursalesPlazo_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            this.cellValidatingNum(dgvSucursalesPlazo, e);
        }

        private void dgvSucursalesGramajes_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            this.cellValidatingNum(dgvSucursalesGramajes, e);
        }

        private void dgvParametos_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
                          
        }

        private void dgvParametos_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            string header = dgvParametos.Columns[dgvParametos.CurrentCell.ColumnIndex].HeaderText;



            TextBox txt = e.Control as TextBox;
            if (txt != null)
            {
                txt.AutoCompleteMode = AutoCompleteMode.Suggest;
                txt.AutoCompleteSource = AutoCompleteSource.CustomSource;
                AutoCompleteStringCollection DataCollection = new AutoCompleteStringCollection();

                if (header == "Valor")
                {
                    string strNombreColumna  = dgvParametos.CurrentRow.Cells["Descripción"].Value.ToString();
                    if (strNombreColumna == this.clsCon.dicParametrosSucursal["avaluo"] )
                    {
                        this.editarNum(dgvParametos, e);
                    }
                    
                    else
                    {
                        this.editarNumInt(dgvParametos, e);
                        
                    }
                    
                }


            }
        }

        private void Porcentaje_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && !(e.KeyChar == '.'))
            {
                e.Handled = true;
            }
        
        }

        private void Int_KeyPress(object sender, KeyPressEventArgs e)
        {
           
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

        }


        private void TxtCodigo_Leave(object sender, EventArgs e)
        {
            if (txtCodigo.Text == "") return;
            string strCodigo = this.ctrlSucursales.sucursal.generaCodigoConsecutivo(txtCodigo.Text);
            this.ctrlSucursales.Buscar(strCodigo);
            if (!(this.ctrlSucursales.mdlSucursales.bolResultado)) 
            {
                txtCodigo.Text = "";
                txtCodigo.Focus();
                this.limpiar_textbox();
                return;
            }

            this.clsView.bloquearColumnasGrid();
            this.clsView.configuracionDataGridView();
            this.llenar_formulario();
        }

        private void txtEmail_Leave(object sender, EventArgs e)
        {
            if (!(this.clsView.validarCorreoElectronico(txtEmail.Text)))
            {
                txtEmail.Focus();
                this.clsView.principal.epMensaje.SetError(txtEmail, this.clsView.mensajeErrorValiForm2("El campo correo electrónico no es válido, favor de ingresar un correo electrónico "));
            }
        }

        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            //this.clsView.soloTextoVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);

            tbsControlSucursal.SelectTab(0);            
            this.sucursalesCobrosTipos(0);
            this.sucursalesGramajes(0);
            this.sucursalesPlazo(0);
            this.sucursalParametros(0);

            btnAgregarPromociones.Visible = false;
            dgvDetallesPromociones.Visible = false;

            txtCodigo.Text = "";
            txtDescripcion.Text = "";
            txtEmpresaId.Text = "";
            txtDescripcionEmpresas.Text = "";
            txtAsentamientoId.Text = "";
            txtDescripcionAsentamiento.Text = "";
            txtRepresentanteLegal.Text = "";
            dtpInicioOperaciones.Text = "";
            txtTelefono1.Text = "";
            txtTelefono2.Text = "";
            txtEmail.Text = "";
            txtCalle.Text = "";
            txtNoInt.Text = "";
            txtNoExt.Text = "";
            txtCodigoPostal.Text = "";
            txtCodigoMunicipio.Text = "";
            txtDescripcionMunicipio.Text = "";
            txtCodigoEstado.Text = "";
            txtDescripcionEstado.Text = "";

            lblDescEstatus.Visible = false;
            lblEstatus.Text = "";
            lblEstatus.Visible = false;

            txtCodigo.Enabled = true;
            

            this.clsView.principal.epMensaje.Clear();
        }

        private bool validacion()
        {

            bool bolValidacion = true;
            string strCodigo = txtCodigo.Text.TrimStart();
            string strDescripcion = txtDescripcion.Text.TrimStart();
            string strDescripcionEmpresas = txtDescripcionEmpresas.Text.TrimStart();
            string strDescripcionAsentamiento = txtDescripcionAsentamiento.Text.TrimStart();
            string strRepresentanteLegal = txtRepresentanteLegal.Text.TrimStart();

            
            string dtInicioOperaciones = dtpInicioOperaciones.Text.TrimStart();
            string strTelefono1 = txtTelefono1.Text.TrimStart();            
            string strCorreoElectronico = txtEmail.Text.TrimStart();
            string strCalle = txtCalle.Text.TrimStart();
            string strNumeroExterior = txtNoExt.Text.TrimStart();

            if (strCodigo == "")
            {
                txtCodigo.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigo, this.clsView.mensajeErrorValiForm2("El campo código es obligatorio, favor de ingresar un código "));
                bolValidacion = false;
            }

            if (strDescripcion == "")
            {
                txtDescripcion.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("El campo descripción es obligatorio, favor de ingresar una descripción "));
                bolValidacion = false;
            }

            if (strDescripcionEmpresas == "")
            {
                txtDescripcionEmpresas.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcionEmpresas, this.clsView.mensajeErrorValiForm2("El campo empresa es obligatorio, favor de ingresar una empresa "));                
                bolValidacion = false;
            }

            if (strDescripcionAsentamiento == "")
            {
                txtDescripcionAsentamiento.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcionAsentamiento, this.clsView.mensajeErrorValiForm2("El campo asentamiento es obligatorio, favor de ingresar un asentamiento "));                
                bolValidacion = false;
            }

            if (strRepresentanteLegal == "")
            {
                txtRepresentanteLegal.Focus();
                this.clsView.principal.epMensaje.SetError(txtRepresentanteLegal, this.clsView.mensajeErrorValiForm2("El campo representante legal es obligatorio, favor de ingresar un representante legal "));                
                bolValidacion = false;
            }

            if (dtInicioOperaciones == "")
            {
                dtpInicioOperaciones.Focus();
                this.clsView.principal.epMensaje.SetError(dtpInicioOperaciones, this.clsView.mensajeErrorValiForm2("El campo inicio operaciones es obligatorio, favor de ingresar un inicio operaciones "));                
                bolValidacion = false;
            }

            if (strTelefono1 == "")
            {
                txtTelefono1.Focus();
                this.clsView.principal.epMensaje.SetError(txtTelefono1, this.clsView.mensajeErrorValiForm2("El campo telefono es obligatorio y debe ser de 10 caracteres, favor de ingresar un telefono "));                
                bolValidacion = false;
            }

        
            if (strCorreoElectronico == "" && !(this.clsView.validarCorreoElectronico(txtEmail.Text)))
            {
                txtEmail.Focus();
                this.clsView.principal.epMensaje.SetError(txtEmail, this.clsView.mensajeErrorValiForm2("El campo correo electrónico es obligatorio  y debe de ser formato válido, favor de ingresar un correo electrónico "));                
                bolValidacion = false;
            }

            if (strCalle == "")
            {
                txtCalle.Focus();
                this.clsView.principal.epMensaje.SetError(txtCalle, this.clsView.mensajeErrorValiForm2("El campo calle es obligatorio, favor de ingresar un calle "));                
                bolValidacion = false;
            }



            if (strNumeroExterior == "")
            {
                txtNoExt.Focus();
                this.clsView.principal.epMensaje.SetError(txtNoExt, this.clsView.mensajeErrorValiForm2("El campo numero exterior es obligatorio, favor de ingresar un numero exterior "));                
                bolValidacion = false;
            }

            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();
            if (validacion())
            {

                if (!(this.ctrlSucursales.sucursal.intId > 0)) 
                {
                    if (this.ctrlSucursales.bolResutladoPaginacion)
                    {
                        DataTable dtDatos = this.ctrlSucursales.objDataSet.Tables[0];
                        this.intUlitmoCodigo = Int32.Parse(dtDatos.Rows[dtDatos.Rows.Count - 1][1].ToString()) + 1;                        
                    }
                    else
                    {
                        this.intUlitmoCodigo = 1;
                    }

                    txtCodigo.Text = this.intUlitmoCodigo.ToString().PadLeft(this.intNumCerosConsecutivo, '0');                    
                }

                this.ctrlSucursales.sucursal.intId = Int32.Parse((txtScursalId.Text == "") ? "0" : txtScursalId.Text);
                this.ctrlSucursales.sucursal.strCodigo = this.intUlitmoCodigo.ToString().PadLeft(this.intNumCerosConsecutivo, '0'); ;
                this.ctrlSucursales.sucursal.strNombre = txtDescripcion.Text.ToString();
                this.ctrlSucursales.sucursal.empresa.intId = Int32.Parse((txtEmpresaId.Text == "") ? "0" : txtEmpresaId.Text);
                this.ctrlSucursales.sucursal.asentamiento.intId = Int32.Parse((txtAsentamientoId.Text == "") ? "0" : txtAsentamientoId.Text);
                this.ctrlSucursales.sucursal.strRepresentanteLegal = txtRepresentanteLegal.Text.ToString();
                this.ctrlSucursales.sucursal.dtInicioOperaciones = Convert.ToDateTime(dtpInicioOperaciones.Text.ToString());
                this.ctrlSucursales.sucursal.strTelefono1 = txtTelefono1.Text.ToString();
                this.ctrlSucursales.sucursal.strTelefono2 = txtTelefono2.Text.ToString();
                this.ctrlSucursales.sucursal.strCorreoElectronico = txtEmail.Text.ToString();
                this.ctrlSucursales.sucursal.strCalle = txtCalle.Text.ToString();
                this.ctrlSucursales.sucursal.strNumeroInterior = txtNoInt.Text.ToString();
                this.ctrlSucursales.sucursal.strNumeroExterior = txtNoExt.Text.ToString();
                this.ctrlSucursales.sucursal.intUsuario = this.clsView.login.usuario.intId;

                this.guardarSucursalesCobrosTipos();
                this.guardarSucursalesGramajes();
                this.guardarSucursalesPlazo();
                this.guardarSucursalesParametros();
                 

                this.ctrlSucursales.Guardar();

                 if (this.ctrlSucursales.mdlSucursales.bolResultado)
                 {
                    //Hacer un llamado a la funcion para obtener los datos del registro(modificado)
                    this.regresarRegistro("guardó");
                 }                
            }
        }

        public void mostraBusqueda()
        {
            if (this.ctrlSucursales.bolResutladoPaginacion)
            {
                this.ctrlSucursales.CargarFrmBusqueda();

                if (this.ctrlSucursales.bolResultadoBusqueda)
                {
                   
                    this.llenar_formulario();
                    txtCodigo.Focus();
                }
            }
            
        }


        public void mostraBusquedaEmpresas()
        {
            this.ctrlEmpresas.Paginacion();
            if (this.ctrlEmpresas.bolResutladoPaginacion)
            {
                ctrlEmpresas.CargarFrmBusqueda();
                if (this.ctrlEmpresas.bolResultadoBusqueda)
                {
                    txtEmpresaId.Text = this.ctrlEmpresas.empresa.intId.ToString();
                    txtDescripcionEmpresas.Text = this.ctrlEmpresas.empresa.strNombreComercial;
                    txtDescripcionEmpresas.Focus();
                }
            }
            
        }

        public void mostraBusquedaAsentamiento()
        {
            this.ctrlAsentamiento.Paginacion();
            if (this.ctrlAsentamiento.bolResutladoPaginacion)
            {
                this.ctrlAsentamiento.CargarFrmBusqueda();
                if (this.ctrlAsentamiento.bolResultadoBusqueda)
                {
                    txtAsentamientoId.Text = this.ctrlAsentamiento.asentamiento.intId.ToString();
                    txtDescripcionAsentamiento.Text = this.ctrlAsentamiento.asentamiento.strDescripcion.ToString();
                    txtCodigoPostal.Text = this.ctrlAsentamiento.asentamiento.strCodigoPostal.ToString();
                    txtCodigoMunicipio.Text = this.ctrlAsentamiento.asentamiento.municipio.strCodigo.ToString();
                    txtDescripcionMunicipio.Text = this.ctrlAsentamiento.asentamiento.municipio.strDescripcion.ToString();
                    txtCodigoEstado.Text = this.ctrlAsentamiento.asentamiento.municipio.estado.strCodigo.ToString();
                    txtDescripcionEstado.Text = this.ctrlAsentamiento.asentamiento.municipio.estado.strDescripcion.ToString();
                    txtDescripcionAsentamiento.Focus();
                }
            }

            
        }


        public void llenar_formulario()
        {
            this.limpiar_textbox();
            if (this.ctrlSucursales.sucursal.strCodigo.Length == 0) return;
                        
            txtCodigo.Enabled = true;
            txtCodigo.Text = this.ctrlSucursales.sucursal.strCodigo;
            this.intUlitmoCodigo = Int32.Parse(this.ctrlSucursales.sucursal.strCodigo);
            txtDescripcion.Text = this.ctrlSucursales.sucursal.strDescripcion;
            txtScursalId.Text = this.ctrlSucursales.sucursal.intId.ToString();             
            txtCodigo.Text = this.ctrlSucursales.sucursal.strCodigo.ToString();
            txtDescripcion.Text = this.ctrlSucursales.sucursal.strNombre.ToString();
            txtEmpresaId.Text = this.ctrlSucursales.sucursal.empresa.intId.ToString();
            txtDescripcionEmpresas.Text = this.ctrlSucursales.sucursal.empresa.strNombreComercial;
            txtAsentamientoId.Text = this.ctrlSucursales.sucursal.asentamiento.intId.ToString();
            txtDescripcionAsentamiento.Text = this.ctrlSucursales.sucursal.asentamiento.strDescripcion.ToString();
            txtRepresentanteLegal.Text = this.ctrlSucursales.sucursal.strRepresentanteLegal.ToString();
            dtpInicioOperaciones.Text = this.ctrlSucursales.sucursal.dtInicioOperaciones.ToString();
            txtTelefono1.Text = this.ctrlSucursales.sucursal.strTelefono1.ToString();
            txtTelefono2.Text = this.ctrlSucursales.sucursal.strTelefono2.ToString();
            txtEmail.Text = this.ctrlSucursales.sucursal.strCorreoElectronico.ToString();
            txtCalle.Text = this.ctrlSucursales.sucursal.strCalle.ToString();
            txtNoInt.Text = this.ctrlSucursales.sucursal.strNumeroInterior.ToString();
            txtNoExt.Text = this.ctrlSucursales.sucursal.strNumeroExterior.ToString();
            txtCodigoPostal.Text = this.ctrlSucursales.sucursal.asentamiento.strCodigoPostal.ToString();
            txtCodigoMunicipio.Text = this.ctrlSucursales.sucursal.asentamiento.municipio.strCodigo.ToString();
            txtDescripcionMunicipio.Text = this.ctrlSucursales.sucursal.asentamiento.municipio.strDescripcion.ToString();
            txtCodigoEstado.Text = this.ctrlSucursales.sucursal.asentamiento.municipio.estado.strCodigo.ToString();
            txtDescripcionEstado.Text = this.ctrlSucursales.sucursal.asentamiento.municipio.estado.strDescripcion.ToString();

            //Solo usuario con el privilegio de modificar movimientos puede realizar ciertas acciones
            if (this.clsView.login.usuario.validarMovimientos())
            {
                btnAgregarPromociones.Visible = true;
                dgvDetallesPromociones.Columns["acciones"].Visible = true;
            }
            else 
            {
                btnAgregarPromociones.Visible = false;
                dgvDetallesPromociones.Columns["acciones"].Visible = false;
            }
            
            dgvDetallesPromociones.Visible = true;

            lblDescEstatus.Visible = true;
            lblEstatus.Text = this.ctrlSucursales.sucursal.strEstatus;
            lblEstatus.Visible = true;

            this.sucursalesCobrosTipos(this.ctrlSucursales.sucursal.intId);
            this.sucursalesGramajes(this.ctrlSucursales.sucursal.intId);
            this.sucursalesPlazo(this.ctrlSucursales.sucursal.intId);
            this.sucursalParametros(this.ctrlSucursales.sucursal.intId);
            this.sucursalesPromocion(this.ctrlSucursales.sucursal.intId);

            this.clsView.permisosMovimientoMenu(tsAcciones, this.ctrlSucursales.sucursal.strEstatus);
        }

        public void guardarSucursalesCobrosTipos() 
        {
            string intSucursalId = "";
            if (this.ctrlSucursales.sucursal.intId == 0)
            {
                intSucursalId = "@id";
            }
            else
            {
                intSucursalId = this.ctrlSucursales.sucursal.intId.ToString();
            }
            int countCobrosTipos = this.ctrlSucursalesCobrosTipos.objDataSet.Tables[0].Rows.Count;
            this.ctrlSucursales.sucursal.strQueryCobrosTipos = "INSERT INTO sucursales_cobros_tipos(sucursal_id,cobro_tipo_id,porcentaje)VALUES ";
            int i = 0;
            bool bolUltimoDato = false;
            bool bolNiguanDato = false;
            foreach (DataGridViewRow row in dgvSucursalesCobrosTipos.Rows)
            {
                if (i == countCobrosTipos) break;
                if (!(float.Parse(row.Cells["Porcentaje "].Value.ToString()) == 0))
                {
                    bolNiguanDato = true;
                    
                    if (i == countCobrosTipos - 1)
                    {
                        this.ctrlSucursales.sucursal.strQueryCobrosTipos += " (" + intSucursalId + "," + row.Cells["cobro_tipo_id"].Value.ToString() + "," + row.Cells["Porcentaje "].Value.ToString() + " ); ";
                        //this.ctrlSucursales.sucursal.strQueryCobrosTipos += " (" + intSucursalId + "," + row.Cells[1].Value.ToString() + "," + row.Cells[5].Value.ToString() + " ); ";
                        bolUltimoDato = true;
                        break;
                    }
                    else
                    {
                        this.ctrlSucursales.sucursal.strQueryCobrosTipos += " (" + intSucursalId + "," + row.Cells["cobro_tipo_id"].Value.ToString() + "," + row.Cells["Porcentaje "].Value.ToString() + " )|| ";
                        //this.ctrlSucursales.sucursal.strQueryCobrosTipos += " (" + intSucursalId + "," + row.Cells[1].Value.ToString() + "," + row.Cells[5].Value.ToString() + " )|| ";
                        //this.ctrlSucursales.sucursal.strQueryCobrosTipos += " (" + intSucursalId + "," + row.Cells[1].Value.ToString() + "," + row.Cells[5].Value.ToString() + " ), ";
                    }

                    
                }

                i++;
            }
            if (!bolNiguanDato)
            {
                this.ctrlSucursales.sucursal.strQueryCobrosTipos = "";
            }
            else
            {
                if (bolUltimoDato)
                {
                    this.ctrlSucursales.sucursal.strQueryCobrosTipos = this.ctrlSucursales.sucursal.strQueryCobrosTipos.Replace("||", ",");
                }
                else
                {
                    int intStrIndex = this.ctrlSucursales.sucursal.strQueryCobrosTipos.LastIndexOf("||");
                    this.ctrlSucursales.sucursal.strQueryCobrosTipos = this.ctrlSucursales.sucursal.strQueryCobrosTipos.Remove(intStrIndex).Insert(intStrIndex, ";").Replace("||", ",");
                }
            }


        }


        public void guardarSucursalesGramajes()
        {
            string intSucursalId = "";
            if (this.ctrlSucursales.sucursal.intId == 0)
            {
                intSucursalId = "@id";
            }
            else
            {
                intSucursalId = this.ctrlSucursales.sucursal.intId.ToString();
            }
            int countGramajes = this.ctrlSucursalesGramajes.objDataSet.Tables[0].Rows.Count;
            this.ctrlSucursales.sucursal.strQueryGramajes = "INSERT INTO sucursales_gramajes(sucursal_id,gramaje_id,precio_tope)VALUES ";
            int i = 0;
            bool bolUltimoDato = false;
            bool bolNiguanDato = false;
            foreach (DataGridViewRow row in dgvSucursalesGramajes.Rows)
            {
                if (i == countGramajes) break;
                if (!(float.Parse(row.Cells["Precio tope"].Value.ToString()) == 0))
                {
                    bolNiguanDato = true;

                    if (i == countGramajes - 1)
                    {                        
                        this.ctrlSucursales.sucursal.strQueryGramajes += " (" + intSucursalId + "," + row.Cells["gramaje_id"].Value.ToString() + "," + row.Cells["Precio tope"].Value.ToString() + " ); ";
                        //this.ctrlSucursales.sucursal.strQueryGramajes += " (" + intSucursalId + "," + row.Cells[1].Value.ToString() + "," + row.Cells[5].Value.ToString() + " ); ";
                        bolUltimoDato = true;
                        break;
                    }
                    else
                    {
                        this.ctrlSucursales.sucursal.strQueryGramajes += " (" + intSucursalId + "," + row.Cells["gramaje_id"].Value.ToString() + "," + row.Cells["Precio tope"].Value.ToString() + " )|| ";
                        //this.ctrlSucursales.sucursal.strQueryGramajes += " (" + intSucursalId + "," + row.Cells[1].Value.ToString() + "," + row.Cells[5].Value.ToString() + " ), ";
                    }

                    
                }

                i++;
            }
            if (!bolNiguanDato)
            {
                this.ctrlSucursales.sucursal.strQueryGramajes = "";
            }
            else
            {
                if (bolUltimoDato)
                {
                    this.ctrlSucursales.sucursal.strQueryGramajes = this.ctrlSucursales.sucursal.strQueryGramajes.Replace("||", ",");
                }
                else
                {
                     int intStrIndex = this.ctrlSucursales.sucursal.strQueryGramajes.LastIndexOf("||");
                     this.ctrlSucursales.sucursal.strQueryGramajes = this.ctrlSucursales.sucursal.strQueryGramajes.Remove(intStrIndex).Insert(intStrIndex, ";").Replace("||", ",");
                    //this.ctrlSucursales.sucursal.strQueryGramajes = this.ctrlSucursales.sucursal.strQueryGramajes.Remove(intStrIndex).Insert(intStrIndex, ";").Replace("||", ",");
                }
            }


        }


        public void guardarSucursalesPlazo()
        {
            string intSucursalId = "";
            if (this.ctrlSucursales.sucursal.intId == 0)
            {
                intSucursalId = "@id";
            }
            else
            {
                intSucursalId = this.ctrlSucursales.sucursal.intId.ToString();
            }
            int countGramajes = this.ctrlSucursalPlazo.objDataSet.Tables[0].Rows.Count;
            this.ctrlSucursales.sucursal.strQueryPlazos = "INSERT INTO sucursales_plazos(sucursal_id,prenda_tipo_id, plazo)VALUES ";
            int i = 0;
            bool bolUltimoDato = false;
            bool bolNiguanDato = false;
            foreach (DataGridViewRow row in dgvSucursalesPlazo.Rows)
            {
                if (i == countGramajes) break;
                
                if (!(float.Parse(row.Cells["Plazo  "].Value.ToString()) == 0))
                {
                    bolNiguanDato = true;
                    if (i == countGramajes - 1)
                    {
                        this.ctrlSucursales.sucursal.strQueryPlazos += " (" + intSucursalId + "," + row.Cells["prenda_tipo_id"].Value.ToString() + "," + row.Cells["Plazo  "].Value.ToString() + " ); ";
                        bolUltimoDato = true;
                        break;
                    }
                    else
                    {
                        this.ctrlSucursales.sucursal.strQueryPlazos += " (" + intSucursalId + "," + row.Cells["prenda_tipo_id"].Value.ToString() + "," + row.Cells["Plazo  "].Value.ToString() + " )|| ";
                        //this.ctrlSucursales.sucursal.strQueryPlazos += " (" + intSucursalId + "," + row.Cells[1].Value.ToString() + "," + row.Cells[5].Value.ToString() + " ), ";
                    }
                    
                }

                i++;
            }
            if (!bolNiguanDato)
            {
                this.ctrlSucursales.sucursal.strQueryPlazos = "";
            }
            else
            {
                if (bolUltimoDato)
                {
                    this.ctrlSucursales.sucursal.strQueryPlazos = this.ctrlSucursales.sucursal.strQueryPlazos.Replace("||", ",");
                }
                else
                {
                    int intStrIndex = this.ctrlSucursales.sucursal.strQueryPlazos.LastIndexOf("||");
                    this.ctrlSucursales.sucursal.strQueryPlazos = this.ctrlSucursales.sucursal.strQueryPlazos.Remove(intStrIndex).Insert(intStrIndex, ";").Replace("||", ",");
                }
            }


        }

        public void guardarSucursalesParametros()
        {
            string intSucursalId = "";
            if (this.ctrlSucursales.sucursal.intId == 0)
            {
                intSucursalId = "@id";
            }
            else
            {
                intSucursalId = this.ctrlSucursales.sucursal.intId.ToString();
            }
            
            this.ctrlSucursales.sucursal.strQueryParametros = "INSERT INTO sucursales_parametros(sucursal_id, dias_cobro_minimo, dias_cobro_cuota_diaria, dias_cobrar_cuota_daria_especial,  dias_liquidar_boletas, dias_refrendar_boletas, dias_remate_prendas, avaluo_calculo, avaluo_impresion)VALUES ";

            int intDiasCobroMinimo = int.Parse((dgvParametos.Rows[0].Cells[1].Value.ToString() != "") ? dgvParametos.Rows[0].Cells[1].Value.ToString() : "0");          
            int intDiasCobroCuotaDiaria = int.Parse((dgvParametos.Rows[1].Cells[1].Value.ToString() != "") ? dgvParametos.Rows[1].Cells[1].Value.ToString() : "0");            
            int intDiasCobroCuotaDiariaEsp = int.Parse((dgvParametos.Rows[2].Cells[1].Value.ToString() != "") ? dgvParametos.Rows[2].Cells[1].Value.ToString() : "0");            
            int intDiasLiquidarBoletas = int.Parse((dgvParametos.Rows[3].Cells[1].Value.ToString() != "") ? dgvParametos.Rows[3].Cells[1].Value.ToString() : "0");
            int intDiasRefrendarBoletas = int.Parse((dgvParametos.Rows[4].Cells[1].Value.ToString() != "") ? dgvParametos.Rows[4].Cells[1].Value.ToString() : "0");
            int intDiasRemateBoletas = int.Parse((dgvParametos.Rows[5].Cells[1].Value.ToString() != "") ? dgvParametos.Rows[5].Cells[1].Value.ToString() : "0");
            decimal decAvaluo = decimal.Parse((dgvParametos.Rows[6].Cells[1].Value.ToString() != "") ? dgvParametos.Rows[6].Cells[1].Value.ToString() : "0");
            decimal decAvaluoImpresion = decimal.Parse((dgvParametos.Rows[7].Cells[1].Value.ToString() != "") ? dgvParametos.Rows[7].Cells[1].Value.ToString() : "0");

            this.ctrlSucursales.sucursal.strQueryParametros += String.Format("({0},{1},{2},{3},{4},{5},{6},{7},{8});", intSucursalId, intDiasCobroMinimo, intDiasCobroCuotaDiaria,  intDiasCobroCuotaDiariaEsp, intDiasLiquidarBoletas, intDiasRefrendarBoletas, intDiasRemateBoletas, decAvaluo, decAvaluoImpresion);

        }

        public void sucursalesCobrosTipos(int intSucursalCobrosTipos)
        {
            this.ctrlSucursalesCobrosTipos = new Controller.clsCtrlSucursalesCobrosTipos();
            this.ctrlSucursalesCobrosTipos.Paginacion(intSucursalCobrosTipos);
            if (this.ctrlSucursalesCobrosTipos.bolResultado)
            {
                dgvSucursalesCobrosTipos.DataSource = this.ctrlSucursalesCobrosTipos.objDataSet.Tables[0].DefaultView; // dataset          
                ctrlSucursalesCobrosTipos.objDataSet.Tables[0].Columns.Add("Porcentaje ", typeof(float));

                //Desablilitar las siguentes columnas
                dgvSucursalesCobrosTipos.Columns["Descripción tipo de cobro"].ReadOnly = true;
                
                


                foreach (DataRow row in ctrlSucursalesCobrosTipos.objDataSet.Tables[0].Rows)
                {
                    if (row["porcentaje"].ToString() == "")
                    {
                        row["Porcentaje "] = 0;
                    }
                    else {
                        row["Porcentaje "] = row["porcentaje"];                         
                        
                    }
                }

                //Alinear el contentido de la celda a la derecha 
                dgvSucursalesCobrosTipos.Columns["Porcentaje "].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;                

                //Son los nombre de campos que no quiero que se muestran en el datagridview
                this.clsView.lsBloquearCol = new List<string>()
                {
                        "sucursal_id",
                        "cobro_tipo_id",
                        "porcentaje",
                        "descripcion"
                };
                //Pasar datagridview para que se puede usar en core
                this.clsView.dgvGenrico = dgvSucursalesCobrosTipos;
                //Quitar ultima fila de la tabla                
                dgvSucursalesCobrosTipos.AllowUserToAddRows = false;
            }
          
            this.clsView.bloquearColumnasGrid();            
            dgvSucursalesCobrosTipos.Columns["Descripción tipo de cobro"].Width = 103;
            dgvSucursalesCobrosTipos.Columns["Porcentaje "].Width =  70;
        }

        public void sucursalesGramajes(int intSucursalGramajes)
        {
            this.ctrlSucursalesGramajes = new Controller.clsCtrlSucursalesGramajes();
            this.ctrlSucursalesGramajes.Paginacion(intSucursalGramajes);
            if (this.ctrlSucursalesGramajes.bolResultado)
            {
                dgvSucursalesGramajes.DataSource = this.ctrlSucursalesGramajes.objDataSet.Tables[0].DefaultView; // dataset          
                ctrlSucursalesGramajes.objDataSet.Tables[0].Columns.Add("Precio tope", typeof(float));

                //Desablilitar las siguentes columnas
                dgvSucursalesGramajes.Columns["Descripción tipo de gramaje"].ReadOnly = true;
                /*dgvSucursalesGramajes.Columns["Código tipo de gramaje"].ReadOnly = true;
                dgvSucursalesGramajes.Columns["Descripción tipo de gramaje"].ReadOnly = true;*/

                foreach (DataRow row in ctrlSucursalesGramajes.objDataSet.Tables[0].Rows)
                {
                    if (row["precio_tope"].ToString() == "")
                    {
                        row["Precio tope"] = 0;
                    }
                    else
                    {
                        row["Precio tope"] = row["precio_tope"];
                    }
                    //Agregar el valor en zero en el campo porcentaje

                }

                //Alinear el contentido de la celda a la derecha 
                dgvSucursalesGramajes.Columns["Precio tope"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                //Son los nombre de campos que no quiero que se muestran en el datagridview
                this.clsView.lsBloquearCol = new List<string>()
                {
                        "sucursal_id",
                        "gramaje_id",
                        "precio_tope"
                };
                //Pasar datagridview para que se puede usar en core
                this.clsView.dgvGenrico = dgvSucursalesGramajes;
                dgvSucursalesGramajes.AllowUserToAddRows = false;
            }

            this.clsView.bloquearColumnasGrid();
            //this.clsView.configuracionDataGridView();
            dgvSucursalesGramajes.Columns["Descripción tipo de gramaje"].Width = 140;
            dgvSucursalesGramajes.Columns["Precio tope"].Width = 70;
        }


        public void sucursalesPlazo(int intSucursalPlazo)
        {            
            this.ctrlSucursalPlazo = new Controller.clsCtrlSucursalPlazo();
            this.ctrlSucursalPlazo.Paginacion(intSucursalPlazo);
            if (this.ctrlSucursalPlazo.bolResultado)
            {
                dgvSucursalesPlazo.DataSource = this.ctrlSucursalPlazo.objDataSet.Tables[0].DefaultView; // dataset          
                ctrlSucursalPlazo.objDataSet.Tables[0].Columns.Add("Plazo  ", typeof(float));

                //Desablilitar las siguentes columnas
                dgvSucursalesPlazo.Columns["Descripción tipo de prendas"].ReadOnly = true;
                /*dgvSucursalesPlazo.Columns["Código tipo de prendas"].ReadOnly = true;
                dgvSucursalesPlazo.Columns["Descripción tipo de prendas"].ReadOnly = true;*/

                foreach (DataRow row in ctrlSucursalPlazo.objDataSet.Tables[0].Rows)
                {
                    if (row["plazo"].ToString() == "")
                    {
                        row["Plazo  "] = 0;
                    }
                    else
                    {
                        row["Plazo  "] = row["plazo"];
                    }

                    
                    //Agregar el valor en zero en el campo porcentaje

                }

                //Alinear el contentido de la celda a la derecha                 
                dgvSucursalesPlazo.Columns["Plazo  "].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                //Son los nombre de campos que no quiero que se muestran en el datagridview
                this.clsView.lsBloquearCol = new List<string>()
                {
                        "sucursal_id",
                        "prenda_tipo_id",
                        "codigo",
                        "descripcion",                        
                        "capturar_gramaje",
                        "capturar_imei_celular",
                        "capturar_serie_vehiculo",
                        "plazo"
                };
                //Pasar datagridview para que se puede usar en core
                this.clsView.dgvGenrico = dgvSucursalesPlazo;
            }

            dgvSucursalesPlazo.AllowUserToAddRows = false;
            this.clsView.bloquearColumnasGrid();
            //this.clsView.configuracionDataGridView();
            //dgvSucursalesPlazo.Columns[6].Width = 103;
           // dgvSucursalesPlazo.Columns["Plazo  "].Width = 50;
        }

       

        public void sucursalParametros(int intSucursalId)
        {

            this.clsCtrlSucursalParametros = new Controller.clsCtrlSucursalParametros();
            this.clsCtrlSucursalParametros.Buscar(intSucursalId.ToString());
            //this.ctrlSU
            dgvParametos.AutoGenerateColumns = true;

            DataTable dtParametros = new DataTable();

            dtParametros.Columns.Add(new DataColumn("Descripción", typeof(string)));
            dtParametros.Columns.Add(new DataColumn("Valor", typeof(string)));
                                        
            dgvParametos.DataSource = dtParametros.DefaultView;
            
           
            
            
            string[] row = new string[] { this.clsCon.dicParametrosSucursal["dias_cobro_minimo"], clsCtrlSucursalParametros.sucursalParametro.intDiasCobroMinimo.ToString() };
            dtParametros.Rows.Add(row);            
            string[] row1 = new string[] { this.clsCon.dicParametrosSucursal["dias_cobro_cuota_diaria"], clsCtrlSucursalParametros.sucursalParametro.intDiasCobroCuotaDiaria.ToString() };
            dtParametros.Rows.Add(row1);            
            string[] row2 = new string[] { this.clsCon.dicParametrosSucursal["dias_cobrar_cuota_daria_especial"], clsCtrlSucursalParametros.sucursalParametro.intDiasCobroCuotaDiariaEspecial.ToString() };
            dtParametros.Rows.Add(row2);            
            string[] row3 = new string[] { this.clsCon.dicParametrosSucursal["dias_liquidar_boletas"], clsCtrlSucursalParametros.sucursalParametro.intDiasLiquidarBoletas.ToString() };
            dtParametros.Rows.Add(row3);
            string[] row4 = new string[] { this.clsCon.dicParametrosSucursal["dias_refrendar_boletas"], clsCtrlSucursalParametros.sucursalParametro.intDiasRefrendarBoletas.ToString() };
            dtParametros.Rows.Add(row4);
            string[] row5 = new string[] { this.clsCon.dicParametrosSucursal["dias_remate_prendas"], clsCtrlSucursalParametros.sucursalParametro.intDiasRematePrendas.ToString() };
            dtParametros.Rows.Add(row5);
            string[] row6 = new string[] { this.clsCon.dicParametrosSucursal["avaluo"], clsCtrlSucursalParametros.sucursalParametro.decAvaluo.ToString() };
            dtParametros.Rows.Add(row6);
            string[] row7 = new string[] { this.clsCon.dicParametrosSucursal["avaluo_impresion"], clsCtrlSucursalParametros.sucursalParametro.decAvaluoImpresion.ToString() };
            dtParametros.Rows.Add(row7);
            dgvParametos.Columns["Descripción"].ReadOnly = true;
            

            //Alinear el contentido de la celda a la derecha 
            dgvParametos.Columns["Valor"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvParametos.Columns["Descripción"].Width = 220;
            dgvParametos.Columns["Valor"].Width = 45;

            
            
            //Quitar ultima fila de la tabla
            dgvParametos.AllowUserToAddRows = false;

        }


        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void TsbNuevo_Click_1(object sender, EventArgs e)
        {

        }

        public void cellValidatingNum(DataGridView dgvGenerico, DataGridViewCellValidatingEventArgs e) 
        {
            if (e.ColumnIndex == dgvGenerico.ColumnCount - 1) // 1 should be your column index 
            {
                float i;

                if (!float.TryParse(Convert.ToString(e.FormattedValue), out i))
                {
                    e.Cancel = true;
                    this.clsView.principal.epMensaje.SetError(txtCodigo, this.clsView.mensajeErrorValiForm2("Favor de proporcionar un valor numérico"));
                }

            }
        }

        public void editarNum(DataGridView dgvGenerico, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= new KeyPressEventHandler(Int_KeyPress);
            if (dgvGenerico.CurrentCell.ColumnIndex == dgvGenerico.ColumnCount - 1) //Desired Column
            {
                TextBox tb = e.Control as TextBox;
                if (tb != null)
                {
                    tb.KeyPress += new KeyPressEventHandler(Porcentaje_KeyPress);
                }
            }
        }

        public void editarNumInt(DataGridView dgvGenerico, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= new KeyPressEventHandler(Porcentaje_KeyPress);
            if (dgvGenerico.CurrentCell.ColumnIndex == dgvGenerico.ColumnCount - 1) //Desired Column
            {
                TextBox tb = e.Control as TextBox;
                if (tb != null)
                {
                    tb.KeyPress += new KeyPressEventHandler(Int_KeyPress);
                }
            }
        }

        private void dgvSucursalesCobrosTipos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnAgregarPromociones_Click(object sender, EventArgs e)
        {

            frmSucursalesPromocionDetalles frmSucursalesPromocion = new frmSucursalesPromocionDetalles();
            frmSucursalesPromocion.intSucursalId = this.ctrlSucursales.sucursal.intId;
            frmSucursalesPromocion.intSucursalPromocionId = 0;
          
            frmSucursalesPromocion.ShowDialog();
            if (frmSucursalesPromocion.bolResultado)
            {
                sucursalesPromocion(this.ctrlSucursales.sucursal.intId);
            }           
        }


        public void sucursalesPromocion(int intSucursalId) 
        {
            dgvDetallesPromociones.Rows.Clear();
            this.ctrlSucursalesPromocion.Paginacion(intSucursalId);
            dgvDetallesPromociones.Columns["porcentaje_pro"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvDetallesPromociones.Columns["estatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            if (this.ctrlSucursalesPromocion.bolResultado) 
            {
                foreach (DataRow row in this.ctrlSucursalesPromocion.objDataSet.Tables[0].Rows) 
                {
                    dgvDetallesPromociones.Rows.Add(
                         row["sucursal_promocion_id"],
                         row["dia"],
                         row["porcentaje"],
                         row["lista_tipos_prenda"],
                         row["estatus"]
                        );
                }
            }
        }
        private void tpPromociones_Click(object sender, EventArgs e)
        {

        }

        private void dgvDetallesPromociones_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvDetallesPromociones.Columns[e.ColumnIndex].Name == "acciones") 
            {

                frmSucursalesPromocionDetalles frmSucursalesPromocion = new frmSucursalesPromocionDetalles();
                frmSucursalesPromocion.intSucursalId = this.ctrlSucursales.sucursal.intId;
                frmSucursalesPromocion.intSucursalPromocionId = int.Parse(dgvDetallesPromociones.Rows[e.RowIndex].Cells["sucursal_promocion_id"].Value.ToString());
                frmSucursalesPromocion.ShowDialog();
                if (frmSucursalesPromocion.bolResultado)
                {
                    sucursalesPromocion(this.ctrlSucursales.sucursal.intId);
                }                
            }
        }

        private void dgvDetallesPromociones_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete && this.clsView.login.usuario.validarMovimientos())
            {

                try
                {
                    dgvDetallesPromociones.CurrentRow.Selected = true;
                    int dgvIndex = dgvDetallesPromociones.CurrentRow.Index;

                    if (this.clsView.confirmacionAccion("Promociones", "¿Desea eliminar este registro?"))
                    {
                        this.ctrlSucursalesPromocion.Eliminar(int.Parse(dgvDetallesPromociones.CurrentRow.Cells["sucursal_promocion_id"].Value.ToString()));
                        if (this.ctrlSucursalesPromocion.bolResultado) 
                        {
                            dgvDetallesPromociones.Rows.RemoveAt(dgvIndex);
                        }
                            
                    }
                    

                    e.Handled = true;

                }
                catch (Exception ex) { }

            }
        }

        /*Metodo para regresar los datos de un registro*/        
        public void regresarRegistro(string stMsj)
        {
            this.limpiar_textbox();
            this.ctrlSucursales.Paginacion();
            this.clsView.mensajeExitoDB(this.Text.ToLower(), stMsj);

            this.ctrlSucursales.Buscar(this.ctrlSucursales.sucursal.strCodigo);
            llenar_formulario();
            txtCodigo.Enabled = true;
            txtCodigo.Focus();
        }

        private void tbsControlSucursal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tbsControlSucursal.SelectedIndex == 0) 
            {
                txtCodigo.Focus();
            }
        }

        private void tbsControlSucursal_Selected(object sender, TabControlEventArgs e)
        {            

        }

        private void frmSucursales_Shown(object sender, EventArgs e)
        {
            txtCodigo.Focus();
        }

        private void tpInformacionGeneral_Click(object sender, EventArgs e)
        {

        }
    }
}
