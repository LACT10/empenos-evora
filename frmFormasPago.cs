﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmFormasPago : Form
    {
        
        public Controller.clCtrlFormaPago ctrlFormaPago;
        public Core.clsCoreView clsView;

        public int _intUlitmoCodigo;
        public int _intNumCerosConsecutivo;
        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }

        public int intNumCerosConsecutivo { get => _intNumCerosConsecutivo; set => _intNumCerosConsecutivo = value; }
        
        public List<Properties_Class.clsFormPago> lsDeshacerFormasPago;

        public frmFormasPago()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();
            this.ctrlFormaPago = new Controller.clCtrlFormaPago();
            this.ctrlFormaPago.Paginacion();
            this.lsDeshacerFormasPago = new List<Properties_Class.clsFormPago>();
            this.lsDeshacerFormasPago.Add(this.ctrlFormaPago.formaPago);
            //Asignar número de ceros pata generar código consecutivo
            this.intNumCerosConsecutivo = 2;

        }

        private void FrmformaPagos_Load(object sender, EventArgs e)
        {
        }

        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlFormaPago.formaPago.intId == 0) return;

            if (this.clsView.confirmacionEliminar("Forma de pago"))
            {
                this.ctrlFormaPago.Eliminar();
                if (this.ctrlFormaPago.bolResultado)
                {
                    this.ctrlFormaPago.Paginacion();
                    this.limpiar_textbox();
                    this.clsView.mensajeExitoDB("forma de pago", "elimino");
                }
            }
        }

        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlFormaPago.bolResutladoPaginacion)
            {
                this.clsView.principal.epMensaje.Clear();
                txtCodigo.Text = this.lsDeshacerFormasPago[0].strCodigo.ToString();
                txtDescripcion.Text = this.lsDeshacerFormasPago[0].strDescripcion.ToString();
                txtDescripcionAnt.Text = this.lsDeshacerFormasPago[0].strDescripcion.ToString();
                txtCodigo.Enabled = true;
                txtCodigo.Focus();
            }
        }

        private void tsbBuscar_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();

        }

        private void BtnBusqueda_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();
        }

        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlFormaPago.PrimeroPaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlFormaPago.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlFormaPago.SiguentePaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlFormaPago.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlFormaPago.AtrasPaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlFormaPago.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlFormaPago.UltimoPaginacion();
            txtCodigo.Enabled = true;
            if (!this.ctrlFormaPago.bolResultado) return;
            this.llenar_formulario();
        }
        private void TsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();
            this.ctrlFormaPago.formaPago.limpiarProps();
            txtCodigo.Enabled = false;
            if (this.ctrlFormaPago.bolResutladoPaginacion)
            {
                DataTable dtDatos = this.ctrlFormaPago.objDataSet.Tables[0];
                this.intUlitmoCodigo = Int32.Parse(dtDatos.Rows[dtDatos.Rows.Count - 1][1].ToString()) + 1;
                this.agregarDeshacer();
            }
            else
            {
                this.intUlitmoCodigo = 1;
            }

            txtCodigo.Text = this.intUlitmoCodigo.ToString().PadLeft(this.intNumCerosConsecutivo, '0');

        }
        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {
            txtDescripcion.Text = "";
        }

        private void TxtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);

        }
        private void TxtCodigo_Leave(object sender, EventArgs e)
        {
            this.ctrlFormaPago.Buscar(txtCodigo.Text);
            if (!(this.ctrlFormaPago.bolResultado) || txtCodigo.Text.Trim().Length == 0) return;
            this.llenar_formulario();
        }
        private void txtDescripcion_Leave(object sender, EventArgs e)
        {
            if (txtDescripcion.Text != txtDescripcionAnt.Text)
            {
                this.ctrlFormaPago.ValidarDescripcion(txtDescripcion.Text);
                if (this.ctrlFormaPago.bolResultado)
                {
                    this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("La descripción debe ser única, favor de verificar"));
                    txtDescripcion.Text = "";
                    txtDescripcion.Focus();
                }
            }
        }

        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            txtCodigo.Enabled = true;
            txtDescripcionAnt.Text = "";
            this.clsView.principal.epMensaje.Clear();
        }

        private bool validacion()
        {

            bool bolValidacion = true;
            string strCodigo = txtCodigo.Text.TrimStart();
            string strDescripcion = txtDescripcion.Text.TrimStart();

            if (strCodigo == "")
            {
                txtCodigo.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigo, this.clsView.mensajeErrorValiForm2("El campo código es obligatorio, favor de ingresar un código "));
                bolValidacion = false;
            }

            if (strDescripcion == "")
            {
                txtDescripcion.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("El campo descripción es obligatorio, favor de ingresar una descripción "));
                bolValidacion = false;
            }
            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();

            if (validacion())
            {
                
                this.ctrlFormaPago.formaPago.strCodigo = this.intUlitmoCodigo.ToString().PadLeft(this.intNumCerosConsecutivo, '0');
                this.ctrlFormaPago.formaPago.strDescripcion = txtDescripcion.Text.ToString();
                this.ctrlFormaPago.formaPago.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlFormaPago.Guardar();
                
                if (this.ctrlFormaPago.mdlFormaPago.bolResultado)
                {
                    this.limpiar_textbox();
                    this.ctrlFormaPago.Paginacion();
                    this.clsView.mensajeExitoDB("forma de pago", "guardó");
                }
            }
        }

        public void mostraBusqueda()
        {
            if (this.ctrlFormaPago.bolResutladoPaginacion)
            {
                this.ctrlFormaPago.CargarFrmBusqueda();

                if (this.ctrlFormaPago.bolResultadoBusqueda)
                {
                    this.llenar_formulario();
                    txtCodigo.Focus();
                }
            }
            
        }

        public void llenar_formulario()
        {

            if (this.ctrlFormaPago.formaPago.strCodigo.Length == 0) return;
            txtCodigo.Enabled = true;
            this.clsView.principal.epMensaje.Clear();
            txtCodigo.Text = this.ctrlFormaPago.formaPago.strCodigo;
            this.intUlitmoCodigo = Int32.Parse(this.ctrlFormaPago.formaPago.strCodigo);
            txtDescripcion.Text = this.ctrlFormaPago.formaPago.strDescripcion;
            txtDescripcionAnt.Text = this.ctrlFormaPago.formaPago.strDescripcion;
            this.ctrlFormaPago.formaPago.intUsuario = this.clsView.login.usuario.intId;

        }

        public void agregarDeshacer()
        {
            if (this.ctrlFormaPago.bolResutladoPaginacion)
            {
                this.lsDeshacerFormasPago.RemoveAt(0);
                this.lsDeshacerFormasPago.Add(this.ctrlFormaPago.formaPago);
            }
        }

        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void FrmFormasPago_Load(object sender, EventArgs e)
        {
            txtCodigo.MaxLength = this.intNumCerosConsecutivo;

            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);
        }


    }
}