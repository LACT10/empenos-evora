-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: empenos_evora
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `asentamientos`
--

DROP TABLE IF EXISTS `asentamientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `asentamientos` (
  `asentamiento_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `municipio_id` int(11) unsigned NOT NULL,
  `asentamiento_tipo_id` tinyint(3) unsigned NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `ciudad` varchar(250) NOT NULL,
  `codigo_postal` char(5) NOT NULL,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`asentamiento_id`),
  UNIQUE KEY `UNIQUE` (`municipio_id`,`descripcion`),
  KEY `asentamiento_tipo_id_asentamientos` (`asentamiento_tipo_id`),
  CONSTRAINT `asentamiento_tipo_id_asentamientos` FOREIGN KEY (`asentamiento_tipo_id`) REFERENCES `asentamientos_tipos` (`asentamiento_tipo_id`),
  CONSTRAINT `municipio_id_asentamientos` FOREIGN KEY (`municipio_id`) REFERENCES `municipios` (`municipio_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asentamientos`
--

LOCK TABLES `asentamientos` WRITE;
/*!40000 ALTER TABLE `asentamientos` DISABLE KEYS */;
INSERT INTO `asentamientos` VALUES (5,25,2,'URBANO','CULIACAN','88045','ACTIVO','2019-09-28 09:22:07',0,'2020-01-31 16:56:35',27,NULL,NULL),(6,25,2,'OTRA','CULIACAN','80200','ACTIVO','2020-02-07 15:48:39',28,'2020-02-08 09:19:30',27,NULL,NULL),(7,26,2,'URBANO','MAZATLAN PUERTO','88045','ACTIVO','2020-02-08 09:13:54',27,'2020-02-08 09:23:03',27,NULL,NULL),(8,25,2,'VARIOS','CULIACAN','48545','ACTIVO','2020-02-08 10:07:44',27,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `asentamientos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asentamientos_tipos`
--

DROP TABLE IF EXISTS `asentamientos_tipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `asentamientos_tipos` (
  `asentamiento_tipo_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` char(2) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`asentamiento_tipo_id`),
  UNIQUE KEY `UNIQUE_codigo` (`codigo`),
  UNIQUE KEY `UNIQUE_descripcion` (`descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asentamientos_tipos`
--

LOCK TABLES `asentamientos_tipos` WRITE;
/*!40000 ALTER TABLE `asentamientos_tipos` DISABLE KEYS */;
INSERT INTO `asentamientos_tipos` VALUES (2,'01','PESCA','ACTIVO','2019-09-22 12:34:40',0,'2020-02-08 09:49:11',27,NULL,NULL),(3,'02','AGRICULTURA','INACTIVO','2019-09-22 12:34:46',0,'2020-01-31 17:03:37',27,'2019-09-22 12:34:46',0),(4,'03','COMERCIO','ACTIVO','2019-09-22 13:54:50',0,'2020-02-08 09:25:31',27,NULL,NULL),(5,'04','COMERCIO2','ACTIVO','2020-02-08 09:43:55',27,NULL,NULL,NULL,NULL),(6,'05','VARIOS','ACTIVO','2020-02-08 09:45:38',27,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `asentamientos_tipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `boletas_empeno`
--

DROP TABLE IF EXISTS `boletas_empeno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `boletas_empeno` (
  `boleta_empeno_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sucursal_id` tinyint(3) unsigned NOT NULL,
  `caja_id` tinyint(3) unsigned NOT NULL,
  `usuario_id` int(11) unsigned NOT NULL,
  `folio` varchar(10) NOT NULL,
  `fecha` datetime NOT NULL,
  `vencimiento` date NOT NULL,
  `boleta_inicial_id` int(11) DEFAULT NULL,
  `boleta_refrendo_id` int(11) DEFAULT NULL,
  `pago_refrendo_id` int(11) DEFAULT NULL,
  `cliente_id` int(11) unsigned NOT NULL,
  `identificacion_tipo_id` tinyint(3) unsigned DEFAULT NULL,
  `prenda_tipo_id` tinyint(3) unsigned NOT NULL,
  `prenda_subtipo_id` int(11) unsigned NOT NULL,
  `gramaje_id` tinyint(3) DEFAULT NULL,
  `kilataje` decimal(11,2) DEFAULT NULL,
  `gramos` decimal(11,2) DEFAULT NULL,
  `cantidad` tinyint(3) NOT NULL,
  `numero_plazos` int(11) DEFAULT NULL,
  `importe` decimal(11,2) NOT NULL,
  `importe_avaluo` decimal(11,2) NOT NULL,
  `descripcion` varchar(250) NOT NULL DEFAULT '',
  `comentario` varchar(100) NOT NULL DEFAULT '',
  `imei_celular` varchar(15) DEFAULT NULL,
  `tipo_serie_vehiculo` enum('FACTURA','PEDIMENTO') DEFAULT NULL,
  `numero_serie_vehiculo` varchar(50) DEFAULT NULL,
  `fotografia_imagen` varchar(250) DEFAULT NULL,
  `motivo_cancelacion` varchar(500) DEFAULT NULL,
  `caja_corte_id` int(11) unsigned DEFAULT NULL,
  `estatus` enum('ACTIVO','LIQUIDADA','ADJUDICADA','LIQUIDACION INTERNA','CANCELADA') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_autorizacion` datetime DEFAULT NULL,
  `usuario_autorizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  `fecha_adjudicacion` datetime DEFAULT NULL,
  `usuario_adjudicacion` int(11) DEFAULT NULL,
  `fecha_liquidacion_interna` datetime DEFAULT NULL,
  `usuario_liquidacion_interna` int(11) DEFAULT NULL,
  PRIMARY KEY (`boleta_empeno_id`,`sucursal_id`),
  UNIQUE KEY `UNIQUE_folio` (`sucursal_id`,`folio`),
  KEY `caja_id_boletas_empeno` (`caja_id`),
  KEY `usuario_id_boletas_empeno` (`usuario_id`),
  KEY `cliente_id_boletas_empeno_idx` (`cliente_id`),
  KEY `identificacion_tipo_id_boletas_empeno_idx` (`identificacion_tipo_id`),
  KEY `prenda_tipo_id_boletas_empeno_idx` (`prenda_tipo_id`),
  KEY `prenda_subtipo_id_boletas_empeno_idx` (`prenda_subtipo_id`),
  KEY `gramaje_id_boletas_empeno_idx` (`gramaje_id`),
  KEY `caja_corte_id_boletas_empeno_idx` (`caja_corte_id`),
  CONSTRAINT `caja_corte_id_boletas_empeno` FOREIGN KEY (`caja_corte_id`) REFERENCES `cajas_corte` (`caja_corte_id`),
  CONSTRAINT `caja_id_boletas_empeno` FOREIGN KEY (`caja_id`) REFERENCES `cajas` (`caja_id`),
  CONSTRAINT `cliente_id_boletas_empeno` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`cliente_id`),
  CONSTRAINT `identificacion_tipo_id_boletas_empeno` FOREIGN KEY (`identificacion_tipo_id`) REFERENCES `identificaciones_tipos` (`identificacion_tipo_id`),
  CONSTRAINT `prenda_subtipo_id_boletas_empeno` FOREIGN KEY (`prenda_subtipo_id`) REFERENCES `prendas_subtipos` (`prenda_subtipo_id`),
  CONSTRAINT `prenda_tipo_id_boletas_empeno` FOREIGN KEY (`prenda_tipo_id`) REFERENCES `prendas_tipos` (`prenda_tipo_id`),
  CONSTRAINT `sucursal_id_boletas_empeno` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursales` (`sucursal_id`),
  CONSTRAINT `usuario_id_boletas_empeno` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `boletas_empeno`
--

LOCK TABLES `boletas_empeno` WRITE;
/*!40000 ALTER TABLE `boletas_empeno` DISABLE KEYS */;
INSERT INTO `boletas_empeno` VALUES (45,1,9,28,'BE00000001','2020-03-04 13:31:31','2020-06-04',45,NULL,NULL,30,6,4,15,1,200.00,4.00,1,3,800.00,1040.00,'NO SE','','',NULL,'','boletaEmpenoBE00000001.jpg',NULL,NULL,'LIQUIDADA','2020-03-04 13:31:31',28,'2020-03-13 14:17:48',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(46,1,9,28,'BE00000002','2018-03-04 13:32:12','2018-09-08',46,NULL,NULL,30,5,10,18,0,0.00,0.00,4,5,400.00,520.00,'NI IDEA','','',NULL,'','boletaEmpenoBE00000002.jpg',NULL,NULL,'LIQUIDADA','2020-03-04 13:32:12',28,'2020-03-05 16:07:39',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(47,1,9,28,'BE00000003','2020-03-04 13:32:49','2020-06-04',45,45,32,30,6,4,15,1,200.00,4.00,1,3,800.00,1040.00,'NO SE','',NULL,NULL,NULL,'boletaEmpenoBE00000001.jpg','PARA VER',NULL,'CANCELADA','2020-03-04 13:32:49',28,NULL,NULL,NULL,NULL,'2020-03-04 13:33:56',28,NULL,NULL,NULL,NULL),(48,1,9,28,'BE00000004','2020-03-04 13:34:13','2020-06-04',45,45,33,30,6,4,15,1,200.00,0.00,1,3,0.00,0.00,'NO SE','',NULL,NULL,NULL,'boletaEmpenoBE00000001.jpg','SI',NULL,'CANCELADA','2020-03-04 13:34:13',28,'2020-03-05 16:07:39',28,NULL,NULL,'2020-03-13 14:17:48',28,NULL,NULL,NULL,NULL),(73,1,9,28,'BE00000025','2020-03-10 16:04:27','2020-08-10',46,46,51,30,5,10,18,0,0.00,0.00,4,5,400.00,520.00,'NI IDEA','',NULL,NULL,NULL,'boletaEmpenoBE00000002.jpg','BILLETE FALSO',NULL,'CANCELADA','2020-03-05 16:04:27',28,NULL,NULL,NULL,NULL,'2020-03-05 16:07:39',28,NULL,NULL,NULL,NULL),(74,1,9,28,'BE00000026','2020-03-10 16:04:27','2020-06-10',45,48,51,30,6,4,15,1,200.00,4.00,1,3,800.00,1040.00,'NO SE','',NULL,NULL,NULL,'boletaEmpenoBE00000001.jpg','BILLETE FALSO',NULL,'CANCELADA','2020-03-05 16:04:27',28,NULL,NULL,NULL,NULL,'2020-03-05 16:07:39',28,NULL,NULL,NULL,NULL),(75,1,9,28,'BE00000027','2020-03-10 16:09:38','2020-08-10',46,46,52,30,5,10,18,1,200.00,4.00,4,5,400.00,520.00,'NI IDEA','',NULL,NULL,NULL,'boletaEmpenoBE00000002.jpg',NULL,NULL,'LIQUIDADA','2020-03-05 16:09:38',28,'2020-03-06 16:07:48',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(76,1,9,28,'BE00000028','2020-03-10 16:09:38','2020-06-10',45,48,52,30,6,4,15,1,200.00,4.00,1,3,800.00,1040.00,'NO SE','',NULL,NULL,NULL,'boletaEmpenoBE00000001.jpg',NULL,NULL,'LIQUIDADA','2020-03-05 16:09:38',28,'2020-03-18 15:03:47',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(77,1,9,28,'BE00000029','2020-03-05 16:13:27','2020-08-05',77,NULL,NULL,30,5,18,25,0,0.00,0.00,4,5,1000.00,1300.00,'PARA TRABAJAR','','','FACTURA','4SD5F4S21F2S345S','boletaEmpenoBE00000029.jpg',NULL,NULL,'LIQUIDADA','2020-03-05 16:13:27',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(78,1,9,28,'BE00000030','2020-03-05 16:25:25','2020-08-05',77,77,53,30,5,18,25,0,0.00,0.00,4,5,1000.00,1300.00,'PARA TRABAJAR','',NULL,NULL,NULL,'boletaEmpenoBE00000029.jpg',NULL,NULL,'LIQUIDADA','2020-03-05 16:25:25',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(79,1,9,28,'BE00000031','2020-03-05 16:27:48','2020-08-05',77,78,54,30,5,18,25,0,0.00,0.00,4,5,1000.00,1300.00,'PARA TRABAJAR','',NULL,NULL,NULL,'boletaEmpenoBE00000029.jpg',NULL,NULL,'LIQUIDADA','2020-03-05 16:27:48',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(80,1,9,28,'BE00000032','2020-03-05 16:30:07','2020-08-05',77,79,55,30,5,18,25,0,0.00,0.00,4,5,1000.00,1300.00,'PARA TRABAJAR','',NULL,NULL,NULL,'boletaEmpenoBE00000029.jpg',NULL,NULL,'LIQUIDACION INTERNA','2020-03-05 16:30:07',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-13 14:53:20',27),(81,1,9,28,'BE00000033','2020-03-10 10:01:45','2020-06-10',81,NULL,NULL,30,5,4,15,1,200.00,0.00,1,3,0.00,0.00,'NO SE ','','',NULL,'','boletaEmpenoBE00000033.jpg','ERA UN BILLETE FALSO',NULL,'CANCELADA','2020-03-10 10:01:45',28,NULL,NULL,NULL,NULL,'2020-03-10 10:39:24',28,NULL,NULL,NULL,NULL),(82,1,9,28,'BE00000034','2020-03-10 10:41:19','2020-06-10',82,NULL,NULL,30,5,4,15,1,200.00,0.00,1,3,0.00,0.00,'NO SE ','','',NULL,'','boletaEmpenoBE00000034.jpg','MUCHAS ',NULL,'CANCELADA','2020-03-10 10:41:20',28,NULL,NULL,NULL,NULL,'2020-03-10 10:43:32',28,NULL,NULL,NULL,NULL),(83,1,9,28,'BE00000035','2019-03-10 11:58:05','2019-06-10',83,NULL,NULL,31,5,4,15,1,200.00,5.00,5,3,1000.00,1300.00,'NO SE ','','',NULL,'','boletaEmpenoBE00000035.jpg',NULL,NULL,'LIQUIDACION INTERNA','2020-03-10 11:58:06',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-13 14:57:36',27),(84,1,9,28,'BE00000036','2020-03-10 12:33:16','2020-06-10',84,NULL,NULL,31,5,4,15,1,200.00,0.00,5,3,0.00,0.00,'NO SE ','','',NULL,'','boletaEmpenoBE00000036.jpg','SI',NULL,'CANCELADA','2020-03-10 12:33:16',28,NULL,NULL,NULL,NULL,'2020-03-10 12:34:16',28,NULL,NULL,NULL,NULL),(85,1,9,28,'BE00000037','2019-03-10 12:34:34','2019-06-10',85,NULL,NULL,31,5,4,15,1,200.00,20.00,4,3,4000.00,5200.00,'SI ','','',NULL,'','boletaEmpenoBE00000037.jpg',NULL,NULL,'LIQUIDADA','2020-03-10 12:34:34',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(86,1,9,28,'BE00000038','2021-03-10 12:35:08','2021-03-10',85,85,56,31,5,4,15,1,200.00,20.00,4,3,4000.00,5200.00,'SI ','',NULL,NULL,NULL,'boletaEmpenoBE00000037.jpg',NULL,NULL,'ADJUDICADA','2020-03-10 12:35:08',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(87,1,9,28,'BE00000039','2019-03-10 12:38:21','2019-06-10',87,NULL,NULL,31,5,4,15,1,900.00,10.00,2,3,9000.00,11700.00,'NO SE ','','',NULL,'','boletaEmpenoBE00000039.jpg','SI',NULL,'LIQUIDACION INTERNA','2020-03-10 12:38:21',28,'2020-03-13 14:45:03',28,'2020-03-13 14:47:04',28,NULL,NULL,NULL,NULL,'2020-03-13 14:50:36',27),(88,1,9,28,'BE00000040','2019-03-10 12:40:35','2019-06-10',87,87,57,31,5,4,15,1,200.00,0.00,2,3,0.00,0.00,'NO SE ','',NULL,NULL,NULL,'boletaEmpenoBE00000039.jpg','ERA UN BILLETE FALSO',NULL,'CANCELADA','2020-03-10 12:40:35',28,NULL,NULL,NULL,NULL,'2020-03-10 12:43:38',28,NULL,NULL,NULL,NULL),(89,1,9,28,'BE00000041','2019-03-17 14:27:07','2019-10-05',46,75,74,30,5,10,18,1,200.00,4.00,4,5,400.00,520.00,'NI IDEA','',NULL,NULL,NULL,'boletaEmpenoBE00000002.jpg',NULL,NULL,'LIQUIDACION INTERNA','2020-03-12 14:27:07',28,'2020-03-13 14:26:17',28,NULL,NULL,NULL,NULL,'2020-03-13 14:27:47',28,'2020-03-13 14:53:20',27),(90,1,9,27,'BE00000042','2018-03-13 08:48:02','2018-08-22',90,NULL,NULL,32,5,4,15,1,200.00,50.00,10,3,10000.00,13000.00,'OTRA','','',NULL,'','boletaEmpenoBE00000042.jpg',NULL,NULL,'LIQUIDADA','2020-03-13 08:48:03',27,'2020-03-13 09:23:38',27,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(91,1,9,27,'BE00000043','2020-03-13 08:58:34','2020-08-13',91,NULL,NULL,32,5,18,25,0,0.00,0.00,1,5,1000.00,1300.00,'UN CARRO','','','FACTURA','8745524422','boletaEmpenoBE00000043.jpg',NULL,NULL,'LIQUIDADA','2020-03-13 08:58:34',27,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(92,1,9,27,'BE00000044','2020-03-13 09:00:14','2020-05-13',92,NULL,NULL,32,5,8,24,0,0.00,0.00,1,2,100.00,130.00,'ESTA BIEN','','452ASAS',NULL,'','boletaEmpenoBE00000044.jpg',NULL,NULL,'LIQUIDADA','2020-03-13 09:00:15',27,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(93,1,9,27,'BE00000045','2018-03-19 09:11:47','2018-08-19',91,91,82,32,5,18,25,0,0.00,0.00,1,5,1000.00,1300.00,'UN CARRO','',NULL,NULL,NULL,'boletaEmpenoBE00000043.jpg',NULL,NULL,'LIQUIDACION INTERNA','2020-03-13 09:11:47',27,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-13 09:44:33',27),(94,1,9,27,'BE00000046','2018-03-26 09:29:03','2018-06-26',90,90,86,32,5,4,15,1,200.00,50.00,10,3,10000.00,13000.00,'OTRA','',NULL,NULL,NULL,'boletaEmpenoBE00000042.jpg',NULL,NULL,'LIQUIDACION INTERNA','2020-03-13 09:29:03',27,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-13 09:44:33',27),(95,1,9,28,'BE00000047','2020-03-13 12:55:28','2020-08-13',95,NULL,NULL,34,5,18,25,0,0.00,0.00,1,5,400.00,520.00,'NO SE','','','FACTURA','DSAFJSKLFJAS4S56FA','boletaEmpenoBE00000047.jpg',NULL,NULL,'LIQUIDACION INTERNA','2020-03-13 12:55:28',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-13 15:12:49',28),(96,1,9,27,'BE00000048','2020-03-13 14:11:18','2020-06-13',96,NULL,NULL,32,5,4,15,2,500.00,10.00,10,3,5000.00,6500.00,'BOLETA QUE ES TIPO ORO','SI','',NULL,'','boletaEmpenoBE00000048.jpg',NULL,NULL,'LIQUIDADA','2020-03-13 14:11:19',27,NULL,NULL,'2020-03-13 14:13:00',28,NULL,NULL,NULL,NULL,NULL,NULL),(97,1,9,27,'BE00000049','2020-03-13 14:13:38','2020-05-13',97,NULL,NULL,32,6,8,24,0,0.00,0.00,1,2,0.00,0.00,'CELULAR','','OTRA',NULL,'','boletaEmpenoBE00000049.jpg','NO SE',NULL,'CANCELADA','2020-03-13 14:13:38',27,NULL,NULL,NULL,NULL,'2020-03-19 12:28:27',28,NULL,NULL,NULL,NULL),(98,1,9,28,'BE00000050','2020-03-31 14:19:07','2020-06-30',45,45,90,30,6,4,15,1,200.00,4.00,1,3,800.00,1040.00,'NO SE','',NULL,NULL,NULL,'boletaEmpenoBE00000001.jpg',NULL,NULL,'LIQUIDADA','2020-03-13 14:19:07',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(99,1,9,28,'BE00000051','2020-05-26 14:23:15','2020-08-26',45,98,91,30,6,4,15,1,200.00,4.00,1,3,800.00,1040.00,'NO SE','',NULL,NULL,NULL,'boletaEmpenoBE00000001.jpg',NULL,NULL,'LIQUIDACION INTERNA','2020-03-13 14:23:15',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-13 14:53:20',27),(100,1,9,27,'BE00000052','2020-03-13 14:48:06','2020-06-13',100,NULL,NULL,32,5,4,15,1,500.00,0.00,10,3,0.00,0.00,'OTRA','','',NULL,'','boletaEmpenoBE00000052.jpg','SI',NULL,'CANCELADA','2020-03-13 14:48:07',27,NULL,NULL,'2020-03-13 14:49:04',28,'2020-03-13 16:51:10',28,NULL,NULL,NULL,NULL),(101,1,9,28,'BE00000053','2018-03-19 09:11:47','2018-03-19',96,96,99,32,5,4,15,2,500.00,10.00,10,3,5000.00,6500.00,'BOLETA QUE ES TIPO ORO','SI',NULL,NULL,NULL,'boletaEmpenoBE00000048.jpg',NULL,NULL,'ADJUDICADA','2020-03-13 16:56:53',28,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-19 08:58:04',28,NULL,NULL),(102,1,9,28,'BE00000054','2020-03-18 09:51:35','2020-05-18',102,NULL,NULL,30,5,8,24,0,0.00,0.00,2,2,0.00,0.00,'SI ','','154854544545343',NULL,'','boletaEmpenoBE00000054.jpg','ERA BILLETE FALSO',NULL,'CANCELADA','2020-03-18 09:51:36',28,NULL,NULL,NULL,NULL,'2020-03-18 11:00:25',28,NULL,NULL,NULL,NULL),(103,1,9,28,'BE00000055','2020-03-18 12:42:13','2020-06-18',103,NULL,NULL,30,5,4,15,1,200.00,0.00,1,3,0.00,0.00,'SI QUIERE ','','',NULL,'','boletaEmpenoBE00000055.jpg','SI',NULL,'CANCELADA','2020-03-18 12:42:14',28,NULL,NULL,NULL,NULL,'2020-03-19 14:18:52',28,NULL,NULL,NULL,NULL),(109,1,9,28,'BE00000061','2019-03-20 10:43:38','2019-05-20',109,NULL,NULL,30,5,8,24,0,0.00,0.00,1,2,200.00,260.00,'OK ','','123456789123415',NULL,'','boletaEmpenoBE00000061.jpg',NULL,NULL,'LIQUIDACION INTERNA','2020-03-20 10:43:38',28,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-20 10:54:41',28,'2020-03-20 11:47:04',28),(110,1,9,28,'BE00000062','2019-03-20 10:45:13','2019-08-20',110,NULL,NULL,30,5,18,25,0,0.00,0.00,2,5,400.00,520.00,'NO SE ','','','FACTURA','SDF84A5F465AS4FF','boletaEmpenoBE00000062.jpg',NULL,NULL,'LIQUIDACION INTERNA','2020-03-20 10:45:13',28,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-20 10:54:41',28,'2020-03-20 11:47:04',28),(111,1,9,28,'BE00000063','2019-03-20 10:49:04','2019-06-20',111,NULL,NULL,30,5,4,15,1,200.00,5.00,12,3,1000.00,1300.00,'NO SE ','','',NULL,'','boletaEmpenoBE00000063.jpg',NULL,NULL,'LIQUIDADA','2020-03-20 10:49:04',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(112,1,9,28,'BE00000064','2020-03-20 10:50:00','2020-06-20',112,NULL,NULL,30,5,4,15,1,200.00,2.00,1,3,400.00,520.00,'NO SE ','','',NULL,'','boletaEmpenoBE00000064.jpg',NULL,NULL,'LIQUIDADA','2020-03-20 10:50:01',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(113,1,9,28,'BE00000065','2019-03-20 10:50:41','2019-06-20',113,NULL,NULL,31,5,4,15,1,200.00,1.00,1,3,200.00,260.00,'NO SE ','','',NULL,'','boletaEmpenoBE00000065.jpg',NULL,NULL,'LIQUIDACION INTERNA','2020-03-20 10:50:41',28,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-20 10:57:34',28,'2020-03-20 11:53:13',28),(114,1,9,28,'BE00000066','2019-03-20 10:51:19','2019-08-20',114,NULL,NULL,31,5,10,18,0,0.00,0.00,15,5,400.00,520.00,'NO SE ','','',NULL,'','boletaEmpenoBE00000066.jpg',NULL,NULL,'LIQUIDACION INTERNA','2020-03-20 10:51:20',28,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-20 10:57:34',28,'2020-03-20 11:53:13',28),(115,1,9,28,'BE00000067','2019-03-20 10:52:26','2019-05-20',115,NULL,NULL,31,5,8,24,0,0.00,0.00,1,2,200.00,260.00,'NO SE ','','486454845615464',NULL,'','boletaEmpenoBE00000067.jpg',NULL,NULL,'ACTIVO','2020-03-20 10:52:26',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(116,1,9,28,'BE00000068','2020-03-20 10:53:07','2020-08-20',116,NULL,NULL,31,5,18,25,0,0.00,0.00,2,5,1000.00,1300.00,'NO SE ','','','PEDIMENTO','AF48A4S8F45AS56F4A5S4F646ASD','boletaEmpenoBE00000068.jpg',NULL,NULL,'ACTIVO','2020-03-20 10:53:07',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(118,1,9,28,'BE00000070','2020-03-23 12:14:19','2020-05-23',118,NULL,NULL,30,5,8,24,0,0.00,0.00,2,2,5000.00,6500.00,'NO SE ','','125213541465466',NULL,'','boletaEmpenoBE00000070.jpg',NULL,NULL,'LIQUIDADA','2020-03-23 12:14:20',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(119,1,10,28,'BE00000071','2020-03-23 14:47:51','2020-05-23',119,NULL,NULL,30,5,8,24,0,0.00,0.00,1,2,400.00,520.00,'NO SE ','','123456789123456',NULL,'','boletaEmpenoBE00000071.jpg',NULL,NULL,'LIQUIDADA','2020-03-23 14:47:51',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `boletas_empeno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cajas`
--

DROP TABLE IF EXISTS `cajas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cajas` (
  `caja_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` char(2) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`caja_id`),
  UNIQUE KEY `UNIQUE_codigo` (`codigo`),
  UNIQUE KEY `UNIQUE_descripcion` (`descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cajas`
--

LOCK TABLES `cajas` WRITE;
/*!40000 ALTER TABLE `cajas` DISABLE KEYS */;
INSERT INTO `cajas` VALUES (9,'02','CAJA A','ACTIVO','2019-12-12 11:55:39',0,NULL,NULL,NULL,NULL),(10,'03','CAJA B','ACTIVO','2019-12-12 11:55:52',0,NULL,NULL,NULL,NULL),(11,'01','CAJA INTEGRADORA','ACTIVO','2019-12-12 11:56:12',0,NULL,NULL,NULL,NULL),(12,'04','CAJA C','ACTIVO','2020-01-29 14:23:03',27,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `cajas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cajas_corte`
--

DROP TABLE IF EXISTS `cajas_corte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cajas_corte` (
  `caja_corte_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `caja_id` tinyint(3) unsigned NOT NULL,
  `fecha` datetime NOT NULL,
  `usuario_realiza` int(11) unsigned NOT NULL,
  `tipo` enum('ARQUEO','CIERRE') NOT NULL DEFAULT 'ARQUEO',
  `saldo_inicial` decimal(8,2) NOT NULL,
  `importe_teorico` decimal(8,2) NOT NULL,
  `mil` tinyint(3) unsigned NOT NULL,
  `quinientos` tinyint(3) unsigned NOT NULL,
  `doscientos` tinyint(3) unsigned NOT NULL,
  `cien` tinyint(3) unsigned NOT NULL,
  `cincuenta` tinyint(3) unsigned NOT NULL,
  `veinte` tinyint(3) unsigned NOT NULL,
  `diez` tinyint(3) unsigned NOT NULL,
  `cinco` tinyint(3) unsigned NOT NULL,
  `dos` tinyint(3) unsigned NOT NULL,
  `uno` tinyint(3) unsigned NOT NULL,
  `cincuenta_centavos` tinyint(3) unsigned NOT NULL,
  `veinte_centavos` tinyint(3) unsigned NOT NULL,
  `diez_centavos` tinyint(3) unsigned NOT NULL,
  `cinco_centavos` tinyint(3) unsigned NOT NULL,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`caja_corte_id`),
  KEY `usuario_realiza_cajas_corte` (`usuario_realiza`),
  KEY `usuario_creacion_cajas_corte` (`usuario_creacion`),
  KEY `usuario_actualizacion_cajas_corte` (`usuario_actualizacion`),
  KEY `usuario_eliminacion_cajas_corte` (`usuario_eliminacion`),
  KEY `cajas_cajas_corte_idx` (`caja_id`),
  CONSTRAINT `cajas_cajas_corte` FOREIGN KEY (`caja_id`) REFERENCES `cajas` (`caja_id`),
  CONSTRAINT `usuario_actualizacion_cajas_corte` FOREIGN KEY (`usuario_actualizacion`) REFERENCES `usuarios` (`usuario_id`),
  CONSTRAINT `usuario_creacion_cajas_corte` FOREIGN KEY (`usuario_creacion`) REFERENCES `usuarios` (`usuario_id`),
  CONSTRAINT `usuario_eliminacion_cajas_corte` FOREIGN KEY (`usuario_eliminacion`) REFERENCES `usuarios` (`usuario_id`),
  CONSTRAINT `usuario_realiza_cajas_corte` FOREIGN KEY (`usuario_realiza`) REFERENCES `usuarios` (`usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cajas_corte`
--

LOCK TABLES `cajas_corte` WRITE;
/*!40000 ALTER TABLE `cajas_corte` DISABLE KEYS */;
/*!40000 ALTER TABLE `cajas_corte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cajas_corte_arqueos`
--

DROP TABLE IF EXISTS `cajas_corte_arqueos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cajas_corte_arqueos` (
  `caja_corte_id` int(11) unsigned NOT NULL,
  `caja_id` tinyint(3) unsigned NOT NULL,
  `renglon` tinyint(3) unsigned NOT NULL,
  `tipo` enum('BOLETA','PAGO','MOVIMIENTO CAJA') NOT NULL DEFAULT 'BOLETA',
  `referencia_id` int(11) unsigned NOT NULL,
  `importe` decimal(8,2) NOT NULL,
  `importe_interno` decimal(8,2) NOT NULL,
  PRIMARY KEY (`caja_corte_id`,`renglon`),
  KEY `caja_id_cajas_corte_arqueos_idx` (`caja_id`),
  CONSTRAINT `caja_corte_id_cajas_corte_arqueos` FOREIGN KEY (`caja_corte_id`) REFERENCES `cajas_corte` (`caja_corte_id`),
  CONSTRAINT `caja_id_cajas_corte_arqueos` FOREIGN KEY (`caja_id`) REFERENCES `cajas` (`caja_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cajas_corte_arqueos`
--

LOCK TABLES `cajas_corte_arqueos` WRITE;
/*!40000 ALTER TABLE `cajas_corte_arqueos` DISABLE KEYS */;
/*!40000 ALTER TABLE `cajas_corte_arqueos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clientes` (
  `cliente_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sucursal_id` tinyint(3) unsigned NOT NULL,
  `codigo` char(6) NOT NULL DEFAULT '',
  `nombre` varchar(100) NOT NULL DEFAULT '',
  `apellido_paterno` varchar(100) NOT NULL DEFAULT '',
  `apellido_materno` varchar(100) NOT NULL DEFAULT '',
  `fecha_nacimiento` date NOT NULL,
  `genero` enum('FEMENINO','MASCULINO','SIN ESPECIFICAR') NOT NULL DEFAULT 'FEMENINO',
  `rfc` varchar(13) NOT NULL DEFAULT '',
  `curp` varchar(18) NOT NULL DEFAULT '',
  `telefono_01` varchar(10) NOT NULL,
  `telefono_02` varchar(10) NOT NULL,
  `correo_electronico` varchar(50) NOT NULL,
  `ocupacion_id` tinyint(3) unsigned DEFAULT NULL,
  `identificacion_tipo_id` tinyint(3) unsigned DEFAULT NULL,
  `numero_identificacion` varchar(50) NOT NULL,
  `calle` varchar(250) NOT NULL DEFAULT '',
  `numero_exterior` varchar(15) NOT NULL DEFAULT '',
  `numero_interior` varchar(15) NOT NULL DEFAULT '',
  `asentamiento_id` int(11) unsigned NOT NULL,
  `cotitular_nombre` varchar(100) NOT NULL DEFAULT '',
  `cotitular_apellido_paterno` varchar(100) NOT NULL DEFAULT '',
  `cotitular_apellido_materno` varchar(100) NOT NULL DEFAULT '',
  `beneficiario_nombre` varchar(100) NOT NULL DEFAULT '',
  `beneficiario_apellido_paterno` varchar(100) NOT NULL DEFAULT '',
  `beneficiario_apellido_materno` varchar(100) NOT NULL DEFAULT '',
  `comentarios` varchar(250) NOT NULL,
  `fotografia_imagen` varchar(250) NOT NULL,
  `identificacion_imagen_frente` varchar(250) NOT NULL,
  `identificacion_imagen_reverso` varchar(250) NOT NULL,
  `estatus` enum('ACTIVO','NO DESEADO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`cliente_id`,`sucursal_id`),
  UNIQUE KEY `UNIQUE_codigo` (`sucursal_id`,`codigo`),
  KEY `ocupacion_id_clientes_idx` (`ocupacion_id`),
  KEY `identificacion_id_identificaciones_tipos_idx` (`identificacion_tipo_id`),
  KEY `asentamiento_id_clientes_idx` (`asentamiento_id`),
  CONSTRAINT `asentamiento_id_clientes` FOREIGN KEY (`asentamiento_id`) REFERENCES `asentamientos` (`asentamiento_id`),
  CONSTRAINT `identificacion_tipo_id_clientes` FOREIGN KEY (`identificacion_tipo_id`) REFERENCES `identificaciones_tipos` (`identificacion_tipo_id`),
  CONSTRAINT `ocupacion_id_clientes` FOREIGN KEY (`ocupacion_id`) REFERENCES `ocupaciones` (`ocupacion_id`),
  CONSTRAINT `sucursal_id_clientes` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursales` (`sucursal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (30,1,'000001','MARIA','PEREZ','SOSA','2019-11-06','MASCULINO','','ASF56ASD5F9A5SDF56','','','',5,5,'HOLA','LACNADS ','','',5,'','','','','','','AS','perfilCliente000001.jpg','identificacion_imagen_frontal_000001.jpg','identificacion_imagen_trasera_000001.jpg','ACTIVO','2019-11-06 11:39:45',0,'2020-03-03 11:18:12',28,NULL,NULL),(31,1,'000002','LUIS','LOPEZ','MEZA','2019-11-06','MASCULINO','','ASKJFAA4DS8G4F5D6A','','','',NULL,5,'','ASDF','1','',5,'','','','','','','','perfilCliente000002.jpg','identificacion_imagen_frontal_000002.jpg','identificacion_imagen_trasera_000002.jpg','ACTIVO','2019-11-06 12:44:19',0,'2020-02-10 12:31:10',27,NULL,NULL),(32,1,'000003','OMAR','GOMEZ','','2019-11-11','MASCULINO','ARIKKKK','ASF48AS4F51AS64F4A','6677895231','6678201220','',NULL,5,'A124556','REPUBLICA DE BRASIL','1500','90',5,'MARIA','SANCHEZ ','OSUNA','JORGE','GOMEZ','LARA','ES EXCELENTE CLIENTE','perfilCliente000003.jpg','identificacion_imagen_frontal_000003.jpg','identificacion_imagen_trasera_000003.jpg','ACTIVO','2019-11-11 11:00:54',0,'2020-03-13 13:59:40',27,NULL,NULL),(33,1,'000004','MARIO','MADRID','SADF','2019-11-11','MASCULINO','','ASDF54AS85F48AS68F','','','',NULL,5,'','ASDFASDFDS','','',5,'','','','','','','','perfilCliente000004.jpg','identificacion_imagen_frontal_000004.jpg','identificacion_imagen_trasera_000004.jpg','NO DESEADO','2019-11-11 11:05:31',0,'2020-02-10 12:32:29',28,NULL,NULL),(34,1,'000005','JOSE','LOPEZ','GOMEZ','2019-12-11','SIN ESPECIFICAR','','JDKELSP123456SD897','6671458798','','JOSE@GMAIL.COM',NULL,5,'','INFONAVIT','2','',5,'','','','','','','','perfilCliente000005.jpg','identificacion_imagen_frontal_000005.jpg','identificacion_imagen_trasera_000005.jpg','ACTIVO','2019-12-11 13:07:04',0,'2020-02-10 12:34:22',28,NULL,NULL),(35,1,'000006','Santiago','Castro','','2019-12-19','MASCULINO','','FASDF545SA6D4F51SA','66715489','','',NULL,5,'','Otra','','',5,'','','','','','','','perfilCliente006.jpg','identificacion_imagen_frontal_006.jpg','identificacion_imagen_trasera_006.jpg','ACTIVO','2019-12-19 14:40:10',0,'2020-01-23 14:29:57',28,NULL,NULL),(38,1,'000007','jdflñkasdf','askjfdl','','1998-10-20','MASCULINO','asdfaf516as4d','1515fa1d5f5a15d6f1','6675465454','','luiwdsa@as.dfcom',5,5,'','asdf','2','',5,'','','','','','','','perfilCliente000007.jpg','identificacion_imagen_frontal_000007.jpg','identificacion_imagen_trasera_000007.jpg','ACTIVO','2020-01-24 09:41:35',27,NULL,NULL,NULL,NULL),(39,1,'000008','DANIEL','ESPINOZA','LOPEZ','2019-11-06','MASCULINO','','DEL789622656565656','','','',NULL,5,'','OTRA','','',5,'','','','','','','','perfilCliente000008.jpg','identificacion_imagen_frontal_000008.jpg','identificacion_imagen_trasera_000008.jpg','ACTIVO','2020-02-07 15:39:50',28,'2020-02-08 11:40:10',27,NULL,NULL),(40,1,'000009','JOSE ','MADRID','','2019-11-06','MASCULINO','54F4A5F5A5SF4','SA5F656F6AS4DF545A','6671954546','','JOSE@GMAIL.COM',5,5,'AJDIJAS5A84F6A4854D8546S','NO SE ','1','',6,'','','','','','','','perfilClienteNUEVO.jpg','identificacion_imagen_frontal_NUEVO.jpg','identificacion_imagen_trasera_NUEVO.jpg','ACTIVO','2020-03-03 11:12:32',28,NULL,NULL,NULL,NULL),(41,1,'000000','VARIOS','VARIOS','VARIOS','2020-01-01','MASCULINO','0','000000000000000000','000000000','','000000000',5,5,'0','0','','',5,'','','','','','','','0','0','','ACTIVO','2020-03-03 11:12:32',28,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes_huellas`
--

DROP TABLE IF EXISTS `clientes_huellas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clientes_huellas` (
  `cliente_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sucursal_id` tinyint(3) unsigned NOT NULL,
  `huella` longblob NOT NULL,
  PRIMARY KEY (`cliente_id`,`sucursal_id`),
  CONSTRAINT `cliente_id_sucursal_id_clientes_huellas` FOREIGN KEY (`cliente_id`, `sucursal_id`) REFERENCES `clientes` (`cliente_id`, `sucursal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes_huellas`
--

LOCK TABLES `clientes_huellas` WRITE;
/*!40000 ALTER TABLE `clientes_huellas` DISABLE KEYS */;
INSERT INTO `clientes_huellas` VALUES (30,1,_binary '\0�U\�*\�s\\�A7	�q��U�ܮJS�&�ҲҚ�\"� +2y\"��MפCo;[f�T|�\'�ⅴ�Y�.�J\�_�W\�\���\0�u\����:\�P�m$\�X��1�=L�A�\"��\�l-,G{\��\�i\��\�Ӱބ8鹑�\�\"\�+�\'�a\�\�\�+M\�A4d	&S:\�\�\n㘶R\�\n�\�,%\�e=\�\�Mf�BׂX\�3�u�@�@�x��P}\�Q@\"v�1�\n��uZ����\�g7:~\����8(��\�\�)!\�\��E&\r88K2�\0\�(f�_HHy��l%ȿ\�\�\Z7k\�.�\"p\�b�\�=\�\r�M�X���nEV�4\�-\�8DI��ER�mG\�H�WxL\�m�9s\�s\\��o\0�6\�*\�s\\�A7	�qp�U��[�\�ڔOi��\�y%��ݰb�NT.�NWR1�\�(jh++~tszf�\"f�2\�[#Y\�\�*\�992gy���rf�ރ�\�-���\��~7\�:�\�\�N���\��T$\���ho\�ܳx�\�c�\�\n�6ۨ�n\"ᶌ����\'\����ŋYŸ��~�s;�Lu؃�h/�rL�݂\��/|�\�Z\�\�k�WOyF\�Cp\�\�%\�{,6z��&���q\�q\�>��3¡�=�A+ �\���/�B\'t�J�\�\n���Hj�\�K\�e���k9)�ô\�`�Q5\�=y;g��B ̪�I˘3��o\0�\�*\�s\\�A7	�q�\�U�w8���v�~4\�F���\�5��6��:hT�\n@wE�ge\�\rV<U\�\�B��D\�@Ϲ�\�\'Vg���\�;�QB�\��?t�\�/�\�\�i2�A{����0�!0u\'xJ�\�#Gړ\�u\�.\�\����%t \�+1������\�\r��iЦh7Ut�IR,Cumׯ\��\�\�ob\�1a�B�^�#|\�q�\�\�R��\\�\��x\�Sf\'���4Ȭ�\�i\�#\�\�_�~�\��GmĂ�!\�}�r},N�kp�\Z\�t\��PN3��۞ǁ��v\�|0�n�)\��F��\�M��|x�Ãd,���.\�\�y��/�cT	�\�\�}���=�\�<\�\�4\�	D���f!�2\Z����%�\�5^*�JH�\�\�Cnj�\�9	���M\�s�eY�b\�o\0\�\�*\�s\\�A7	�qp�U�p�L\�x\��޶Th���n��=��I\�؆\�\��Mj<^\�^\��Ϧ�\�\�?Όݘ\�+<?Zzt3��Mf�\Z&\�,\�~6�\�>�y�C\�\�\0ֽ=����{�G�B|��9��u��sϞښTp�\����\���PK�d���1�O��\"	#\�t\�i7 ��~3`N)9<��j�,��2\�A�0n���\��\��\0�#��\�l�&�k\�巽�E#�\"hpY�⮄���+\�}	\�\�3�WBZf�_X�\�\�ʓ�Pe�\�|Q�[\��\�\�*Dӿ\��q��s�=g[r\�\�\��1\�D@��;si��bp͢����6�\�h1���s�2{%\�敶����d��\�7 ��,}>3��m\�\�Fv\�]��U�Ʃ��Y�\�\�\�8�$s\�ka[o\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0'),(31,1,_binary '\0��\�*\�s\\�A7	�q�\�U��\�\rC\'}\Z\�}0\�\��e\��Z���\�ʋ\�\Z{\�L�%�O\�\��c\n��r�o\n�N\�\�DO\�\�/�脼ހ!�\�3\�w�\'�]�\��V:\'�,�!N�8f���\Z�^���\��n`Y�\�\�f[�n́�cőO&f�U���E��\�\�ɑ�I�5�\�r�ʭU���x6zni�\nB���HU�[$��2���Gm�?\�\�-�\�>B\�*\�lZV\���\�|�\�\�B\�*U\�\�8�!zH\�YFOU�\�!�\Z�\�H9��\�\r�\�*MR&�\�0\�\�Pv���k����\"�dj�:��\�\Z�\�\Z%\�&ڼ�M\��o���\��7����3z�\�-�{^&���^rD_�b�~k��!�\"i��h�9h\�����`\��\�/�\�\�i��blٜ\�\�o\0�~\�*\�s\\�A7	�q�\�U�^]_��9d�jl�\�\"�\�N��\'^�\�랆�ǥ���xm�u��[8\'BlY�?\��,�d�L��V^�\��d�\�3\�^�yź�\�\�Q�R\�Ο��q\rȳ|R�JM \�%\�ަފ��a��i���\�\�\�\'Ǘ�4n�\�\��|��r��c,R_{���?\�n��\�\�\��w��\�\�!`���>Fqe\n\�R\���W�\�gn\�D7\�Ƹ�6=�5��\�\�<�-�\� F*LC�*\�@(|f�q�.Uˢ;�����aN�U{�\�^XH#/\�a��d\�\�ݣa�~Т%9�mV/\� ݭ:�\�_W\�\�\�#i��+\0.Ē>\�v�9W\�o�D]�.x�N\��2IW�\��2KV{<\�D�? \�ܵy�\���s^��r\Z\�킫\\\�o\0�\�*\�s\\�A7	�qp\�U�ͤw}AWU�\�\�Г\�`�3��\n\�x��z\'�_&8�:\�yz�\n�e��\�ng\�-\�&�z�ȵXL��s6M�0[A�\0S{���6��5ܱ{�\�\�U7}R\�1�j�7\�A\�EMw�D�2pL�5��Fv\�\�*0���~�+�\�x\�vl|T��gIQ�\�W�\�m�\�I��l\�kW����Ty�I\�\�UM����\n拟��\�.\Z7�\\Dc�É�-�y1%\�١C\��+�sN�<��F�L���E�S\�U\�Y^\��B\��\�\�A&_#\���v\�w�{0;seB�\�FY1����cTE�\'�0��Ɓ[i�\�O`�\rOHQ5rj3��\�\'�en�i>	r7_�EA,\�˔�&2`\�^#FS\�p�˧HB\'�7��\�8o\0\�\�*\�s\\�A7	�q�\�U�\��G\��+\�vm)\'5/;?>M�\�g�d\��\�\�ڋ\�\�\���\�T^\�K\�6eܵ=\�AA��t\�\�d\�hnF9���J����.\Z�\�\�഻I\�g��$uW\�Z_�ⲬL��<:G�S�\�^\r�\�\�\�e�\�\�7;e3\�\�\�\r��0]ѳ���\�Ģ{է�\n \�e\�{+�`D�3�\�|<�6PH|ά�\�,6��Cܣ�\�\�A\���j\�0�N��g\�}\�y�\�0\�W\�\�.f��V\"^9$9�Dfr#�/\�h\�k�r=!�\�A�\�\�q~\�/�{��ڈ7\�&�ÌC�Q |�\n\�\Zk\�Ǭ�r\�\��)e��յ�ZcT�>�I\��t6�\�T�8\��\�$aT���S�H�/^a�Gz�à\0�\�\����\�`_��J�\�\�\�k� T\�\�L��o\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0'),(32,1,_binary '\0�~\�*\�s\\�A7	�q��U��0&[���\�	xj�j_4�\'�Y\���\�d\��qeY��+K8G�\�\�9\nwϚN�\�\�U\�F�d[q�_	i9�Z\�v۞e΁�W��&�\��Y�W�?\��W\�u\�GB�{�\�07֭�z���#e]ʽ��+?6�\��l�lc��\�\"�i��\�\�c\�\�Zp0\�\r�8�t�eQ?GE4�\r\��\r\�D+	�p\�d���hx\�Cu+��z�\�u\�\�\�\�(i���\'\�%NĆ)\�+�K�K�3m���A�tLN)�\�Q��#-�Y�X�\�9dQ�{����CPG�7X�����9����\�u\�@\�>K\rd/Å�DyLǿ�i��\\��.V�\��\�X�g�\�.(\\`|\r׈\�\����NB�wSp\�,���>/�n�\�G�o\0�~\�*\�s\\�A7	�q�U��π��w��`uX��\�݉\�\�~g\ZK1\�\�ԕ\��;\�0_v`��:\r\�\�M�)r��@\�hyS�H�$+�\�\'u�w\��\�՘O�\�\�bG\�a����\�\�����\r\�Rө�u\�8$ӏ�M��X/�O\nPH�lQwq  �RO�j�\��\�P��\��ӕ\��3��K5�Hq�/\�6\��#^\�C�.fTQ\�g��8:�\n\�j(��?���zY�EIG�2�\�ӟ\�KT\�B%Y\�/� �}��\��<R\����!�j��\�U�\�\�O�쌕�bj|\�r~PC�8\�\n]\rR�L_�V�p�\�뢢\�\Zz\no�9-jdx�\\\�\�Z\� Z\�Ҟ�3�����^B�m�gW����D鑅dL\�2�%7\�U\�{\�<h}\\Pv$�Ũ\�4	\�g\�Sb,IGo\0��\�*\�s\\�A7	�q�U�w;�x��\�\�<�\�E}SW׻�\���k�Ms������G���MW��\"@ͽ{�\�J�qyq~�\�\�#���!\�J\0]\r��)~ 6ė���	�&P\�t��\�\�8\�!\�y?6cQ�Q\�\�+^�c\�038�7�~�:x�\r\�s��� ��Ԑ�&ݏs@\�]\�57�K\�Ǣ���*2}�P�Y\r��w\�\�b��J��K:����\��\�eU\�f\r\��\�Ā��MdQ�M�g/ni��l㩏x�\ra	��d�\��U�¤\�C\�E�h\�6\�\��\�zj3x�A wy\�\�A;\�G\�\�{\���\�}(`���D<8�[+ 9LL�]��\�nx��\�g}&�\�g\�験p�^�\�\�\�%�tyjQ\�m?]\��bk	U��9�y\�a�p��\n\nao\0\�~\�*\�s\\�A7	�q0�U�#�+R:����=0�=��E߻\�a�\�\�[*o�5\�\�;f8�TK�\�\�GG\�Ksr\rUAV\�\�\nm?B\"\\+Dl��\�#�o�?M�B�G\�d쬰F/&F�b\rO%\\�I\rl\��D�\�.�-�\�=O\�A�8����m}�\��G\Z#r�aN����+\�\n\�\�8���\�\�ŵ:>�WE{+]#�\�\�\�I\\���c�Y\�p\Z=\\\Zw\�,�\�r�;\�t0�ex1G_;�ʫ���������Y\�}Ȭ\�\� 4�����[_��\�d\Z\�*N\�P0\�˷�+а\�in8����\�\�\�\�غ\Z���y�ͻl\�J{\�\Z$3!\r��֝��J={_`��\�HHoƆUx\�`\r̀�cE\��SҮ\���$-\�&�[\��k:1�E}Xh\Z��\�ޢ4p\�5�o�`q�`@q�`@q�`Pq�`Pq�``q�``q�`Hk�`Hk�`pq�`pq�`Tk�`Tk�`�q�`�q�``k�``k�`lk�`lk�`xk�`'),(33,1,_binary '\0�~\�*\�s\\�A7	�q0BU�\�cR\�\�M�\�\�J�bӈ\�\�}\�\Z��,h5i�z��3�\�\'`-�G���\�\�b9\�\n8��.�\0%gu��pK��vo\�c��{\�uNY�=yҎ�S�K�l�+�r2���\�y�f؈l\�ґ2�d�̑(Z���Z̀���3�V��(��lπTe\�7#���XlF\�(�\� \"1��X\0�\�\�E\�]\'\��vK,_�s����\�x�I\�,3��=\�`yJ���s\�K��f�\�t`Z\� N\�\�YlʢE\")XM(Ɵ�\�u�6&<�9��o|t[܇b�\�f=\�m�\�y\\��hT��}\�J\�n�������u�yᓡ:�\�B\r��Ǵl��\\�`�ƴ�µ�\�.3��m�W\�a\�{�\�hy9��mҳf\��\�cE�\����Lo\0�~\�*\�s\\�A7	�q0_U�\"Ü����p������\��_gws��\Z\���\�\�\��*o&��y�ҋ!I�����\��R&��V�����B\'р<ðI]T{X�G�Ҋ��\�_�\�IȪ��b�TEG�\�\�n~�=l�}avWΡ\�z�\�\�\�bɑ\�EHy\\<�ͻ�\�OjOӡ<�\�e\�\�3(�|N\�龸&\�\��\�0�\�%�Bj��\�\�\�~.�d\�~00�Ω�\�\�ŶFv��<|b>?\�	a�w�q�2N$\�V�\��Lj��%\�׃�\�@��NM7�>Xo�\�[\�LG�\���ڔ�N7�@�.\�\�\�!�=��P�\�A\�.��ր�&\�kR�L\�=��6\��`\�?o\�\�l�:��3\�\�l^��JVI�\��Íu�\"atpڪ\�\��\�\�k o\0�\�*\�s\\�A7	�q0CU�0\�H\��1�� e+e�K�\�f\�n\�I\�\�\�j>�k\���`\�7\��*�,��x�}���ȁ\�l�0$Ѱ�t3���A<�\�\�Y��0���\�;Y�)��\�k\�\�L�\���>�\"\�\����\n`(;FQ\��\��D_�26�?�\r\�*Cl\�\rN�\�^s%HJ[o\�\�\�\�\�\�\�\�x4\�\rc{�#\�ҋ\���}u�zO��&\��/(*ٙ]uS\���\�\�\�K�����\�F�>H&MHB\�/VѓD\����\�A��\�\�X��♼�n�6���\�\��\�@�\Z�\r�_\�@\0\�\���@��R\�\��c�s\�\�y0\'�\�?�lP��\n\n;K\��|3?sJ*k94��*���ʐ	\0\"t��t?\�\�/\"�cANgdUvn̹�a{\��T\�o\0\�\�*\�s\\�A7	�q�U�\�d���s���&�!,\���a�ז�\�s\�@��A�c��wH\�\�\��湙�����	<+!���^XR�փ+\�2� \�5��ew�P�`KϼvL�\0�\'/l\�Б\r�DzM\�?Ymbo[\�Vm\��W\��ʺ�.�3JQ�\�$��2�9\�¾��\�#�ur�\�XU�T;J��V7�	Q㹱B\�\�΁lg�\�-b��\�:\�jP�W̫\�\0J|�z\�� \�)�X�ܶF\�@%��\��)��\�rc\�6qYO�\��\�qA\�-�\�Q~7G1�� �����(�<�\�	�dJ\�\r1+\��\�4\�++\\�=Tc�)A`du\rs��g���r���6a\���dL\�ԫJ]$�z�L�\\�I\�U2w\��\0>�\�	`T:9�H��E}\��\�؞\\\�8\�j�o�5?a�\�kc\�`c<16��#q\�}C̡�\"���35\r\�_\"8::e]\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0'),(34,1,_binary '\0�\�*\�s\\�A7	�qpHU��!\�\�-�\�%-��i}�\rN;�jH\�\��K�#���m��Im\��E\�L�O}6���biIG�	WG\�\�t��D��L\�\�\\re\�\\sǤ�\\\�\�~����&\�y�[N\0\�o!xUG\�䄇�F�_��$\�$�\�\�2u^��\�)�\�P�\�v-�mܦ|\�\�\�8H\�Y}�\Z\�=0\rk�_ �24\�\�$5\�\�\�\�]	��`t\"L�G_��U\�\�*{E�}�L&S3ijp�#\r���b�J�\�\�Wm|T�@�\�l�ŋR���m#:P�&(#;�N:�ϸ:~�\�pQ\�\��A^ƀ\����pu\�Ղ�\�|\�?JE��O�V�?��1�Q~�C\ZKn?�>O�\�,/�N�f��i2k\� \�³V��W\���W�n\�z.O)�CӃd�o\0��\�*\�s\\�A7	�q�U�{E7�C�\�0\�Q$�l+r�@�@�\��iXu��-�\\>�R\���Յse��G$s\�j}�\�8x�j}��,��\�\�2�MV\�\�p\�Q4j��R�Mz\Z�qv���0C\�@���ipݫ@G�w\�\�\�:\0���_�GԺL���\�\�v��\�\�F�\�$\'J��}\��u^�ͷ�\��946�T\�*\�؊9��\�1%3\�\�Q&�#V\��I�\�\"գ�\'1!\�\����T�\�u^<]e�\�����Ϭ�T��\��ŚWq=��\�K_�C\�\rdi�Acc �mx��=:\�.\�\�\�o�\r���6}���\���^�0���_;-\�\�˸Q I_�w\�\�!!dmI��6�Da\�C\�\Z\�\���]Bw\�;xg������r�M�\�\�o\0�~\�*\�s\\�A7	�qpNU�]~YH\"�n(�n����\��Ly�����\�\�\�V�\0��3̈\0\� \�\�3�\�s�\� z\��\�P�6�O��,s5\�\�Z�g@��G��(zR?�5�?��G��U~\�\� Y\�܀J���K��\�_b\�m\�\�)j>7p��\�z���ݙ�\0�T�p��Na����j��`h[�&�`\�\���6�H\��İ���\�I��\�\�[-u|21��\�^�4�5\�g]&\��d� �\�ڣ�qp��\�\ZP�	�\�i`\���\�\"�G��ߘGĐx)�mř$��%���\Zr�\�W�Թ�\��{YӼAؓc3�sX�Q�G�Ҙh�EW\�i�iΰ&|p=Y>b\�p|\�j$\�h��J0����\�#eo~\�\�G:xt;$\Z\�\����$�0\��D:o\0\�\�*\�s\\�A7	�qpKU���!�\\�\�̑\�/�� �3\�Gt>/6S�W�\rG1f����B;]V1\�\�آթF\�\�ȅ\��3�h�r \�^<�Ҹ��|]���^\�\�\�Y���\�^�����W�\�MW\�p�s\�ߦ\0ͥ������(�&\�Ymy�\��?�H\r�]���u8\0	k���d\�w�G�g�\�\��Ұ\�\�(\'\�O:,nJ)�\�\�D���ι�\��Lx\"�y!\0\�e*R�R�>\�h#vZƊ��fM�\�?O�Ay�\�\�V�E ��\Z�F^�|\�^� -$�\�vg�A8EI\�3�jx��j\��y\�yizq�S�_\��1��U5��[\\Ġ;�\�=�>��`C�\�\�È\�&\�\n8���\�\Z;�WT��X/I?`M�2��.�\��՞\�f��\�o\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0'),(35,1,_binary '\0��\�*\�s\\�A7	�q0�U�1��;#�I�qED�\'STT�dvej�W����Ԁ\�\����Q�:í^\"\�m\�y纂6�,/�n\�U�\�6�\�\�_�RީU��\�d�\�G\�Ԛ�\��-/\�{�\\�D�\"z\�cj\�Z&���/HT��ρW�n\�B㜀�\�&Sp�h\�t�k\�b��\�Oվ\�d\�\�J�(Osz�\'\�JT\��Qw_��\�i����ד\Z�\�RvE\�WJ\�v,~ڸ���4a�N�5�7���\��N\0\�*\�ekb��ō��\�N� \�\�ƙo9m��%�7rYB\nJ��M6r�3�\0\0\�\�hPT�bޑ \��eSl\"hQ�:rއ&?\�\��\�Z�\�\����b�\�\�<\�Jxa\�:$��D�2�耢8Z\�P8pK�&W\�[\�\�\��۱\�\\�p�7�\�Ė�7\�\��L o\0�~\�*\�s\\�A7	�q�U��\rnX�����˪\�\�\�t\�\�Ycy\�\�\�K�F\�VO\�T��׭\�x>�˂KV��x\�>�\�%��I\�\����x\����;{\�\�9�\r<qL����\�k�\��]k \�\�|\�\�#\��;�\�Ǫ\�h�,T~\�N8���yLu!������u��\�\�XC|-R\�\�\�ɖ�\�g\�m\�sh��]\�]VR\�\"ny\�\nˤP0vп\�\�\�I�&\��~\�ë\�3,]\n\� \�19\� Dl_�^\�u\n?\�C�,�\�q\�N�Ju�̏�\��\��\�Q;�\�D�w%��.eݻ�Sۘ\�k\�b\r�T9Ri\�\�\r\�\0�de\���oiD\��q7\�[�\�ע5�+t7d`nz\\Py\�U:-�0!�E�n\�7}|Q؈#\�͌,P�o\0��\�*\�s\\�A7	�qp�U�/JP�\�Ks�\�\\�\�eOp��kiO4\�)\��\�W©\�۴O\�\�9��(,vHϨЈL*�7\�O\n#���$�,y?A�t�\�#,WUCS���=B�};��\�!�rx\�D_5\�P4KW�(liWL\��\�\�`�S�|\�b��4\����q.� Dq[:6iY�\�\n�\�.8�Z��w~2\�x�z���\�=\\�=�*S�\�\�վ��\�$\�\�appf�\�wN=Ҟ�\��k\�K\rN���\'�\�\�\\���\Z�F\����I�\������	?���w\�\�\"uɯ0���\Z\�\�O�� >\���⇌�CM\�O�|2�t�\\�I\Z�H�t�0\�\Z�\�<�\�vmΐr7.mc�eT\�\�OZ��(�!>!���2�5\�HN�y�O�@�\�u\�tZ�o\0\�~\�*\�s\\�A7	�q�HU���8k5�\�_\�]<#bOaL\�(A\�\��\�_Hn\�AG�!4�h\�B\�U[y��V\"\����ÉG\�q�_J2\��!��ʐnJ{�_~(}l0�+@��E��\�nC\�`(\�\��gN�bu�4kv��\�\�}\�\�v*\�ϙ���\0�p�dޮ�\�XV���Pr�vW�\�)\�|�j\�X;v!HzIz�\�\��jkk3f�F�s\��1�\�G��dEb�m\���\�2�\�\�\�gl \�on�������;�\�֎\��\�\�\�ȕț��k\�\�j4i\�\��\�h�j\0\�ӑ��\���7\0\��\�\�79/��^|[����vo,	\\^�;\�\'��\�q��n5�\�b\�PN�\r?�\�8��������L\�jd�+\�\r1�e8\�\�\�,+�b-�\�yկڞ\�jo\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0'),(38,1,_binary '\0�\�*\�s\\�A7	�qpDU��\�#�\"Q�\�\�l\�5 u=\�3цv�\�;\Z{84Jۍ�ۛY�%	_�\0\�<S\�G�s_&�BKl�v9Fy7\�d�\�\�Q�6B\n�\�n\�n=q2t\�\�\���H�j\�\��0F�1@$\�h\rar+��B�^x�Q��{݇\�KtNb�M{|��%*��\��\��\�\� <:@\�\�6�(��u����\�N\�)��\�k\�g���@��/\�\�<u�����d\�\��\ntp*�\��\�E\�ff\����v�G� \���E:�#aN��\�}�_}aGK�@�\�Tv犚�Gx�T`������FFHB/6:�J��ΏN���\��2\�x�:\�kT���j�\�~M2���A;$2�+\�\���b�w�!4;w&ݼu{\�z*}Y��A\Z���)J��_��\�o\0�\�*\�s\\�A7	�q�DU��LH\�3�\��R\�Hq@\\@\��\�t��魕J�����N\�\�\�4w\�,k\'\�tus\n,�H�座�P��B\��Y5#\�\�\��(\���	�/.kzPYOP�\�tu��0CR�SYR�\�\�\\�\�89�C\�\�>��\�\�\r]\�e�\n\�KS\�i,\�ī��(�|�fӚ0��= \�\��WQUT��\�\����\���5B=S\��8��1���\�Tēө�hp�\�\�\�R\�z�Q	>\n_$�\�hh<��Ql\�\�=6+.�N�|\"J\�\�bj\�3���bw{��\�\Z�\��\��eV��}\���p��x\�OF�����(\�\\-J�:\�\�\"\�G\n�0�!\�\�+l��\�F�K0B \�k-RB=>\�h^\�\��ץ�t�o\0�\�*\�s\\�A7	�q�U�d5ݣQ�uG�\�MƜܨ�1=\�cO\�V=���\0\r=\'c\�]\�,�hȺ\�K(\�\�\�	\�\�`;��\���|E��΀\�\�LA�#џ\�C��)X9n7<�\'\�u\��s8�3i�M�\�Y$�7l$\�bt��̣ax�Wb��kr�8A�\�\n\��\�\�\�E��\�B$�Ϙ�2R,ths�\�ِ�\�\�_b)�9q%Y\r�d\Z3#W\"9�*�Q�JCo��\�k�\�zP�\0\0bT�5�cJ\�\�v���t��82+\�Jѥ~]�e��`�\�Pz>���ڸv:��,ɲ=ԥ\�iRdư�#\�#�g�\�%�\n\�	=6�e1�BX��\�N\�z8f�\�\�uV�����z3?�@ul,j�\�\�8�\�\"\0�lE\rv\�c�-�x�M�zچN���o\0\�\�*\�s\\�A7	�q�U�@o�ʒ�\�(�ZaIH\�\�F\n�\�>hΖ%��3��Ci*6�\�\�\�\�.~LdIV���գt\��d�\�\�\�\�\�,r�#����$���7_���tw�M2h�\�A��tn(�I͙r\�U�\��\�\\Qˤ6/_�\'��O��:�?��\�~�W\�X�M\�FZ�����\�,p�5%$@�\���?�3��&�\�\�\�\"�\�\�y�۞3\�\�\\��gf\�\�\n~��s�\�]yw�c�0\��\�-/|&�\�b�L�{����\�ۦ\�\����\�\0v�<d;�\"̍XGY�TVQ6<\�\�\�rE\�yiO\����3Vm���˪&V�p�Z�\�&V+P8)\�w��HI\��\�\�\�g:\��c�,a0Mi�9\�^�H�b4z\'�2Ał6��\�;>� �o\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0'),(39,1,_binary '\0�~\�*\�s\\�A7	�q�U��|��tRE\�\�\Z|v$\�vS��bw\�0\��q`˃\�\�y\rO���-�e�!��\�\�r6\0z=ȹՒvȽ\Zw\'w8#hr�{�d<\�\�~�\�p7\�\�&ƟC�Lڶ�\�n����\��AQ^yޛY%����\�\��vQ\"Pg}P���Lf�\rCZ`\��\�\�\�\�I3�i��j\�\�p*E|:�c$,\Z�N\�&�\��\��4b1F2)�\�1\�Fai[v�J�ɀ��rd�\��\�ȵy�=;\�N*w�Q�;\�R\�\�l7T\r/7VQ9ny��\�i��C�RL\�]0Q���\'\��G\�뮚4�\�&\�%��X���t��xr�\��\0�G}��ra�6\�<�co\�fs�?;���\���\��x0�MBn���	��tʆo\0��\�*\�s\\�A7	�q��U�.Y\�G	�>\�6\�rk=0��\"\Z�*�3���9$���\�\�\�.\�}��T��P� \0X\�;\�8��؉\�&vK\�\�\�\ZD��`i�N>����닪-M\�r<�:nF�\��\�ɪ7xx��k\�c;\�2-��\�\�\��\ZUek-��-�t�4\Zj�p\r#\�^�\�U\�ִ��B̬h����E;A�:��6�V\� ��$UfI>26�\�C%2k�\�*Tп�\rUǥ~�;7\Zg}봓\�\�\��>D�EGs#*j�OK]\�p�tꖕ���=i\Z�\�\0�ȩ0F\�wb\n\"��vBj\�&�lN\�)dT�$w\"`=S}�pʛ<�\�\"0�\�\�\�tN\\wմ���t���O�L5J�SL(\'����`䓹��V[5=\��K��\�c\�R�#�o\0�\�*\�s\\�A7	�q��U�\��\Z$3�\�{�n�<*u>緯-0\�[jM)\�v̡\��\�m\�&�=A`\�\�\n{^D\�\0\�\�&t\�2*�\�܅\�\�M�Tl/K\�\"\�ۂAG��S�\�t\���x]���W\�\�\�\�$hk�OJ1ĘZ\�ѣ����\�3N�Q&��\�\\\���\�\�ğ)D�\�\�O�\�RV�>\�N�\�\��\�8&�L\�z\�[ˍd�\�4s��\�f�8\���\�\��n[I��\�ϯ%\'�B*������n�\"E�\Z��ɜ���)�.\�^�ek \�S�\�\�\��esV_ʉ�Qis�T��\�%|9��5�!�;\ZM\�ษ�\�=L���v\�%\�F�����MH��<�\�G�K@ɭ����W@\��0cݵ�O\�\�\�щ\�\�\�>��\�\��A�\�*�Z\�\�#��\�o\0\�~\�*\�s\\�A7	�q�U��|��tRE\�\�\Z|v$\�vS��bw\�0\��q`˃\�\�y\rO���-�e�!��\�\�r6\0z=ȹՒvȽ\Zw\'w8#hr�{�d<\�\�~�\�p7\�\�&ƟC�Lڶ�\�n����\��AQ^yޛY%����\�\��vQ\"Pg}P���Lf�\rCZ`\��\�\�\�\�I3�i��j\�\�p*E|:�c$,\Z�N\�&�\��\��4b1F2)�\�1\�Fai[v�J�ɀ��rd�\��\�ȵy�=;\�N*w�Q�;\�R\�\�l7T\r/7VQ9ny��\�i��C�RL\�]0Q���\'\��G\�뮚4�\�&\�%��X���t��xr�\��\0�G}��ra�6\�<�co\�fs�?;���\���\��x0�MBn���	��tʆo\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0'),(40,1,_binary '\0��\�*\�s\\�A7	�qp�U�H��<\�R Ҷ/K=��\�\\%\�f�\�\���Kh\�m��&0�\�Vk\�����k&�\�?\�7��\�Jͱ}y\r\�\�\�\rZbh\�-؋H#\�{mW\r!��2)\�UE�\��Q\�lˌ�\�I\'Ȑ� �Q�H�Q�R��\\���z6�\�pJO�Y�b�^c��X���\�py��xI�K]X�\�Q\Z	�\�>n�\�\�X\0\�\�0\�qĲR�p֛\�\�H�1\�]A\�|=ҙ��xͦ\��9\�wj�c��WK\0Y=\�VS$\��%&\�j弪6j\�T�\�g�׳p��\�\���oZz\\��V��\\�k\�\�#HLfc�C\��d\'��h\�O�\�_\�Dbtw/�{�nR�\�w��� \�hK3\�\������\�Z;\�trF�\��\�¤7<\�����#eo\0�~\�*\�s\\�A7	�qp�U�\�k�6\�\�3]�6	*\�r���\�ى]�+\�\�j���\n:u7�L�\���no��\�ƴ\�`J�\�q�\�C�$.\�p;byy�\�aT�\�\�\�̽U�\�\�k���%E\�TV�\��)��#�\�\�wȢ\�\�]c\�\�\�.�ŬM��;;[0����\�qq5�G\�˾\����xs0��[	\�\r�|U��G��>#&��7�.�ܽ�Z+s���%�k\�\���gs���?�0A\�|T�=4\Z�\�jW`|���?6\��ױ\r+�K�� �m�\�t@WE\�xA\��L\�\�ߔ��\�\�Y�#n%�66\�B�k@Rp�	�1:�sѾ.��\Z�\�\�{�S\�$�Z�0\�m\�\�ו��\�Z�ˑ\�/Nv\�7\��\�}\�T�\�}�\'L��S���貶ܜ�\�\���Q�o\0�~\�*\�s\\�A7	�qp�U�N\�\�-I�8�\r\�q9�\�H m���&�o�m\�\�\�hz\�Lִ�\�lo\���J$ƀ�x��;L�\�NR\�\�\�l9\�04&n\��U\�&�\�_ҭ]\�0Q�\\0��Ф�[a@.�&_X�� �-\�~\�w\�F�s\r�Gv�\�oֹQğ�\Z�g9\�Wb\�L\� ЉF�~�&\�R1���Z����ӂ��p0	� \��\��\�֣\�z�HSv{\��^\�%r�\�l\�ש8����Ջ>\�QuUO�T\�a��:.�c�\�7��\��$\�m�z\�愠\�$\�0:�\�\�h����p�H�Q�������2\�5���z��\�ʫa��|�\�\�Zآ\�^\�A\���\�އt�ܶ�{ɟ^)\�v�o\�.�\��E\�Z4\�\0\�&H�v\�p����o\0\�\�*\�s\\�A7	�q��U��\��\"\�q\�\�;F&�\�A~\�\�Tt\�b�\���X�y^}K\�\�Q8P�\�*��\�\�yp�D�\�>õn�D(\�pϩE���\�R/��xU�H���%/\ZW\�EO�=\0A&v\�e\��\'�\�\�5�\�cOy\�\�!�\�&\�\�N6�+@TS;\�{\�0�}\�H@\�F�\�n\�\���QF\�&Y<�\�KVz\�7�\�;\0V��̗n��?��F\�� ����\�\�\0xƹ���[l=��j\�{����\nЀ��\�\� z0�F�uo�J;�&��\��\�\0�\�	C_�_@$Ƹ\'\�\�j�\�\Z�2�\�&h\�qD�\�\�n\�w\"�i9TCydh#�\�\�pF�z��Vf�e\�Lg�_�\�P��\�Y��\�=��*�U%\�\���\�ui}5\�{�bG\�/\�50o\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0');
/*!40000 ALTER TABLE `clientes_huellas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cobros_tipos`
--

DROP TABLE IF EXISTS `cobros_tipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cobros_tipos` (
  `cobro_tipo_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` char(2) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`cobro_tipo_id`),
  UNIQUE KEY `UNIQUE_codigo` (`codigo`),
  UNIQUE KEY `UNIQUE_descripcion` (`descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cobros_tipos`
--

LOCK TABLES `cobros_tipos` WRITE;
/*!40000 ALTER TABLE `cobros_tipos` DISABLE KEYS */;
INSERT INTO `cobros_tipos` VALUES (1,'01','INTERES','ACTIVO','2020-02-12 11:14:17',27,NULL,NULL,NULL,NULL),(2,'02','ALMACENAJE','ACTIVO','2020-02-12 11:14:47',27,NULL,NULL,NULL,NULL),(3,'03','MANEJO','ACTIVO','2020-02-12 11:14:57',27,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `cobros_tipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresas`
--

DROP TABLE IF EXISTS `empresas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `empresas` (
  `empresa_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `razon_social` varchar(250) NOT NULL,
  `nombre_comercial` varchar(250) NOT NULL,
  `rfc` varchar(13) NOT NULL DEFAULT '',
  `curp` varchar(18) NOT NULL DEFAULT '',
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`empresa_id`),
  UNIQUE KEY `UNIQUE` (`rfc`,`nombre_comercial`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresas`
--

LOCK TABLES `empresas` WRITE;
/*!40000 ALTER TABLE `empresas` DISABLE KEYS */;
INSERT INTO `empresas` VALUES (4,'EVORA S.A DE C.V','CASA EMPEÑO EVORA','EVRS123456677','EV6789123456798900','ACTIVO','2019-09-22 12:44:26',0,'2020-02-08 12:40:15',27,'2019-09-22 12:44:50',0),(5,'CASTILLA','CASTILLA A.C','CAST6789RF990','CAS6789HT908776898','ACTIVO','2019-12-12 09:47:24',0,'2020-01-31 10:38:04',27,NULL,NULL);
/*!40000 ALTER TABLE `empresas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estados`
--

DROP TABLE IF EXISTS `estados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `estados` (
  `estado_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` char(3) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`estado_id`),
  UNIQUE KEY `UNIQUE_codigo` (`codigo`),
  UNIQUE KEY `UNIQUE_descripcion` (`descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estados`
--

LOCK TABLES `estados` WRITE;
/*!40000 ALTER TABLE `estados` DISABLE KEYS */;
INSERT INTO `estados` VALUES (40,'001','SINALOA','ACTIVO','2019-09-28 08:44:27',0,'2020-03-17 09:37:30',28,NULL,NULL),(41,'002','CAMPECHE','ACTIVO','2019-10-04 10:07:50',0,NULL,NULL,NULL,NULL),(42,'003','CHAPAS','ACTIVO','2019-10-09 08:54:49',0,NULL,NULL,NULL,NULL),(44,'004','JALISCO','ACTIVO','2019-10-09 09:00:28',0,NULL,NULL,NULL,NULL),(45,'005','AGUASCALIENTES','ACTIVO','2019-10-09 09:01:41',0,NULL,NULL,NULL,NULL),(46,'006','CIUDAD DE MÉXICO','ACTIVO','2019-10-10 08:50:46',0,NULL,NULL,NULL,NULL),(47,'007','HIDALGO','ACTIVO','2019-10-10 08:53:51',0,NULL,NULL,NULL,NULL),(48,'008','GUERRERO','ACTIVO','2019-10-23 10:33:11',0,NULL,NULL,NULL,NULL),(49,'009','DURANGO','ACTIVO','2019-10-28 10:30:41',0,NULL,NULL,NULL,NULL),(50,'010','COLIMA','ACTIVO','2019-10-28 10:39:24',0,NULL,NULL,NULL,NULL),(51,'011','CHIHUAHUA','ACTIVO','2019-10-30 09:00:30',0,NULL,NULL,NULL,NULL),(56,'012','SINALOADOS','ACTIVO','2019-11-19 11:07:40',0,NULL,NULL,NULL,NULL),(57,'013','SINALOATRES','ACTIVO','2019-11-19 11:08:56',0,NULL,NULL,NULL,NULL),(58,'014','BAJA CALIFORNIA','INACTIVO','2019-12-04 12:17:47',0,NULL,NULL,'2019-12-17 12:29:01',0),(60,'015','OTRO','INACTIVO','2020-02-07 16:00:10',28,NULL,NULL,'2020-03-17 08:30:20',28),(61,'016','OTROS','INACTIVO','2020-02-07 16:08:22',28,NULL,NULL,NULL,NULL),(62,'017','BAJA CALIFORNIA SUR','ACTIVO','2020-03-14 08:37:28',28,NULL,NULL,NULL,NULL),(63,'018','DISTRITO FEDERAL','ACTIVO','2020-03-14 08:46:19',28,NULL,NULL,NULL,NULL),(64,'019','QUINTANA ROO','ACTIVO','2020-03-14 09:04:36',28,NULL,NULL,NULL,NULL),(65,'020','YUCATÁN','ACTIVO','2020-03-14 09:10:06',28,NULL,NULL,NULL,NULL),(66,'021','SAN LUIS POTOSI','INACTIVO','2020-03-17 08:31:43',28,NULL,NULL,'2020-03-17 08:31:43',28);
/*!40000 ALTER TABLE `estados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folios`
--

DROP TABLE IF EXISTS `folios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `folios` (
  `folio_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) NOT NULL,
  `serie` char(5) NOT NULL,
  `consecutivo` int(11) unsigned NOT NULL,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`folio_id`),
  UNIQUE KEY `UNIQUE` (`descripcion`),
  KEY `usuario_creacion_folios` (`usuario_creacion`),
  KEY `usuario_actualizacion_folios` (`usuario_actualizacion`),
  KEY `usuario_eliminacion_folios` (`usuario_eliminacion`),
  CONSTRAINT `usuario_actualizacion_folios` FOREIGN KEY (`usuario_actualizacion`) REFERENCES `usuarios` (`usuario_id`),
  CONSTRAINT `usuario_creacion_folios` FOREIGN KEY (`usuario_creacion`) REFERENCES `usuarios` (`usuario_id`),
  CONSTRAINT `usuario_eliminacion_folios` FOREIGN KEY (`usuario_eliminacion`) REFERENCES `usuarios` (`usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folios`
--

LOCK TABLES `folios` WRITE;
/*!40000 ALTER TABLE `folios` DISABLE KEYS */;
INSERT INTO `folios` VALUES (1,'BOLETAS DE EMPEÑO','BE',72,'ACTIVO','2019-12-19 15:23:05',9,'2020-03-23 14:47:51',28,NULL,NULL),(2,'SUCURSALES','S2',1,'ACTIVO','2019-12-21 11:48:32',9,'2019-12-21 11:54:20',9,NULL,NULL),(3,'SUCURSALES 2','SU',1,'ACTIVO','2019-12-21 11:49:51',9,NULL,NULL,NULL,NULL),(4,'OTRO','O',1,'ACTIVO','2019-12-21 11:50:01',9,NULL,NULL,NULL,NULL),(5,'SUCURSALES 3','SU',1,'ACTIVO','2019-12-21 11:54:20',9,NULL,NULL,NULL,NULL),(6,'SUCURSALESAS','S245',1,'ACTIVO','2019-12-21 11:55:03',9,'2020-02-10 09:47:04',27,NULL,NULL),(7,'OTRO 2','O2',1,'ACTIVO','2019-12-21 12:06:40',9,NULL,NULL,NULL,NULL),(8,'OTRO 6','O2',1,'INACTIVO','2019-12-21 12:06:51',9,'2019-12-21 12:23:51',28,'2019-12-21 12:23:51',28),(12,'BOLETAS DE EMPEÑO CULIACAN','BEC',2,'ACTIVO','2020-01-22 13:32:40',28,'2020-01-22 13:35:29',28,NULL,NULL),(13,'MOVIMIENTOS DE CAJA','MC',10,'ACTIVO','2020-01-23 13:16:34',27,'2020-03-19 11:31:31',28,NULL,NULL),(14,'REFRENDOS Y LIQUIDACIONES ','RL',96,'ACTIVO','2020-01-30 11:03:34',27,'2020-03-23 14:48:58',28,NULL,NULL),(15,'OTROS2','O2',1,'ACTIVO','2020-02-10 09:42:00',27,NULL,NULL,NULL,NULL),(16,'OTRA 2','O1',1,'ACTIVO','2020-02-10 09:42:11',27,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `folios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folios_procesos`
--

DROP TABLE IF EXISTS `folios_procesos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `folios_procesos` (
  `sucursal_id` tinyint(3) unsigned NOT NULL,
  `folio_id` smallint(5) unsigned NOT NULL,
  `proceso_id` int(11) unsigned NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`sucursal_id`,`folio_id`,`proceso_id`),
  KEY `sucursal_id_folios_procesos` (`sucursal_id`),
  KEY `proceso_id_folios_procesos` (`proceso_id`),
  KEY `usuario_creacion_folios_procesos` (`usuario_creacion`),
  KEY `usuario_actualizacion_folios_procesos` (`usuario_actualizacion`),
  KEY `folio_id_folios_procesos` (`folio_id`),
  CONSTRAINT `folio_id_folios_procesos` FOREIGN KEY (`folio_id`) REFERENCES `folios` (`folio_id`),
  CONSTRAINT `proceso_id_folios_procesos` FOREIGN KEY (`proceso_id`) REFERENCES `procesos` (`proceso_id`),
  CONSTRAINT `sucursal_id_folios_procesos` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursales` (`sucursal_id`),
  CONSTRAINT `usuario_actualizacion_folios_procesos` FOREIGN KEY (`usuario_actualizacion`) REFERENCES `usuarios` (`usuario_id`),
  CONSTRAINT `usuario_creacion_folios_procesos` FOREIGN KEY (`usuario_creacion`) REFERENCES `usuarios` (`usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folios_procesos`
--

LOCK TABLES `folios_procesos` WRITE;
/*!40000 ALTER TABLE `folios_procesos` DISABLE KEYS */;
INSERT INTO `folios_procesos` VALUES (1,1,36,'2019-12-26 11:14:28',27,NULL,NULL),(1,1,41,'2019-12-26 09:39:23',27,NULL,NULL),(1,1,61,'2019-10-30 12:19:14',9,'2019-12-26 11:25:54',27),(1,13,63,'2020-01-23 13:17:24',27,NULL,NULL),(1,14,62,'2020-01-31 16:48:28',27,NULL,NULL),(6,12,61,'2020-01-22 13:33:39',28,NULL,NULL);
/*!40000 ALTER TABLE `folios_procesos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formas_pago`
--

DROP TABLE IF EXISTS `formas_pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `formas_pago` (
  `forma_pago_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` char(2) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`forma_pago_id`),
  UNIQUE KEY `UNIQUE_codigo` (`codigo`),
  UNIQUE KEY `UNIQUE_descripcion` (`descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formas_pago`
--

LOCK TABLES `formas_pago` WRITE;
/*!40000 ALTER TABLE `formas_pago` DISABLE KEYS */;
INSERT INTO `formas_pago` VALUES (4,'01','EFECTIVO','ACTIVO','2019-09-22 12:26:21',0,'2020-01-31 17:02:16',27,NULL,NULL),(5,'02','TARJETA','ACTIVO','2019-09-22 12:26:27',0,'2020-01-31 17:02:16',27,'2019-09-22 12:26:27',0),(6,'03','TRANSFERENCIA BANCARIA','ACTIVO','2019-09-22 12:31:02',0,'2020-01-31 17:02:16',27,'2019-09-22 12:31:02',0);
/*!40000 ALTER TABLE `formas_pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gramajes`
--

DROP TABLE IF EXISTS `gramajes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gramajes` (
  `gramaje_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `prenda_tipo_id` tinyint(3) unsigned NOT NULL,
  `codigo` char(2) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`gramaje_id`),
  UNIQUE KEY `UNIQUE_codigo` (`codigo`),
  UNIQUE KEY `UNIQUE_descripcion` (`prenda_tipo_id`,`descripcion`),
  KEY `prenda_tipo_id_gramajes` (`prenda_tipo_id`),
  CONSTRAINT `prenda_tipo_id_gramajes` FOREIGN KEY (`prenda_tipo_id`) REFERENCES `prendas_tipos` (`prenda_tipo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gramajes`
--

LOCK TABLES `gramajes` WRITE;
/*!40000 ALTER TABLE `gramajes` DISABLE KEYS */;
INSERT INTO `gramajes` VALUES (1,4,'01','8 KILATES','ACTIVO','2019-10-04 08:59:19',0,'2020-01-31 17:00:28',27,NULL,NULL),(2,4,'02','10 KILATES','ACTIVO','2019-11-11 11:21:34',0,'2020-01-31 17:00:28',27,NULL,NULL),(4,4,'03','14 KILATES ','ACTIVO','2020-01-20 10:39:12',0,'2020-01-31 17:00:28',27,NULL,NULL),(6,3,'04','OTRO','ACTIVO','2020-02-07 17:03:04',28,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `gramajes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `identificaciones_tipos`
--

DROP TABLE IF EXISTS `identificaciones_tipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `identificaciones_tipos` (
  `identificacion_tipo_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` char(2) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `valida_curp` enum('SI','NO') NOT NULL,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`identificacion_tipo_id`),
  UNIQUE KEY `UNIQUE_codigo` (`codigo`),
  UNIQUE KEY `UNIQUE_descripcion` (`descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `identificaciones_tipos`
--

LOCK TABLES `identificaciones_tipos` WRITE;
/*!40000 ALTER TABLE `identificaciones_tipos` DISABLE KEYS */;
INSERT INTO `identificaciones_tipos` VALUES (5,'01','IFE','SI','ACTIVO','2019-09-22 12:34:15',0,NULL,NULL,NULL,NULL),(6,'02','PASSPORTE','NO','ACTIVO','2020-01-29 09:55:01',27,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `identificaciones_tipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimientos_caja`
--

DROP TABLE IF EXISTS `movimientos_caja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movimientos_caja` (
  `movimiento_caja_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sucursal_id` tinyint(3) unsigned NOT NULL,
  `caja_id` tinyint(3) unsigned NOT NULL,
  `usuario_id` int(11) unsigned NOT NULL,
  `folio` char(10) NOT NULL DEFAULT '',
  `fecha` datetime NOT NULL,
  `concepto` varchar(250) NOT NULL DEFAULT '',
  `motivo_cancelacion` varchar(500) DEFAULT NULL,
  `caja_corte_id` int(11) unsigned DEFAULT NULL,
  `estatus` enum('ACTIVO','CANCELADO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`movimiento_caja_id`,`sucursal_id`),
  UNIQUE KEY `UNIQUE_folio` (`sucursal_id`,`folio`),
  KEY `caja_id_movimientos_caja` (`caja_id`),
  KEY `usuario_id_movimientos_caja` (`usuario_id`),
  KEY `caja_corte_id_movimientos_caja_idx` (`caja_corte_id`),
  CONSTRAINT `caja_corte_id_movimientos_caja` FOREIGN KEY (`caja_corte_id`) REFERENCES `cajas_corte` (`caja_corte_id`),
  CONSTRAINT `caja_id_movimientos_caja` FOREIGN KEY (`caja_id`) REFERENCES `cajas` (`caja_id`),
  CONSTRAINT `sucursal_id_movimientos_caja` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursales` (`sucursal_id`),
  CONSTRAINT `usuario_id_movimientos_caja` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimientos_caja`
--

LOCK TABLES `movimientos_caja` WRITE;
/*!40000 ALTER TABLE `movimientos_caja` DISABLE KEYS */;
INSERT INTO `movimientos_caja` VALUES (32,1,9,27,'MC00000001','2020-02-04 14:26:39','ENTRADA DE CAJA DE MATRIZ','SI',NULL,'CANCELADO','2020-02-04 14:26:40',27,'2020-03-19 12:28:01',28,'2020-03-19 13:44:49',28),(35,1,9,28,'MC00000002','2020-02-05 09:16:32','LAPTOP',NULL,NULL,'ACTIVO','2020-02-05 09:16:33',27,'2020-02-06 11:19:46',28,NULL,NULL),(36,1,9,27,'MC00000003','2020-02-05 09:30:19','LAPTOP',NULL,NULL,'ACTIVO','2020-02-05 09:30:19',27,NULL,NULL,NULL,NULL),(38,1,9,27,'MC00000004','2020-02-05 09:54:56','TELEFONO',NULL,NULL,'ACTIVO','2020-02-05 09:54:57',27,'2020-02-05 09:54:57',27,NULL,NULL),(39,1,9,28,'MC00000005','2020-02-06 09:28:40','ENTRADA',NULL,NULL,'ACTIVO','2020-02-06 09:28:40',28,'2020-02-06 11:24:27',28,NULL,NULL),(40,1,9,28,'MC00000006','2020-02-13 15:13:13','OTRO',NULL,NULL,'ACTIVO','2020-02-13 15:13:14',28,NULL,NULL,NULL,NULL),(43,1,9,28,'MC00000007','2020-02-13 15:14:38','OTRASS',NULL,NULL,'ACTIVO','2020-02-13 15:14:38',28,'2020-02-13 15:15:09',28,NULL,NULL),(48,1,9,27,'MC00000008','2020-02-14 08:44:47','OTROS','SI',NULL,'CANCELADO','2020-02-14 08:44:48',27,'2020-03-19 11:27:58',28,'2020-03-19 14:18:31',28),(49,1,9,28,'MC00000009','2020-03-19 11:31:31','OTROS','NO SE ',NULL,'CANCELADO','2020-03-19 11:31:31',28,'2020-03-19 11:35:28',28,'2020-03-19 14:15:14',28);
/*!40000 ALTER TABLE `movimientos_caja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimientos_caja_detalles`
--

DROP TABLE IF EXISTS `movimientos_caja_detalles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movimientos_caja_detalles` (
  `movimiento_caja_id` int(11) unsigned NOT NULL,
  `sucursal_id` tinyint(3) unsigned NOT NULL,
  `renglon` tinyint(3) unsigned NOT NULL,
  `tipo` enum('ENTRADA','SALIDA') NOT NULL DEFAULT 'ENTRADA',
  `concepto` varchar(250) NOT NULL DEFAULT '',
  `importe` decimal(8,2) NOT NULL,
  PRIMARY KEY (`movimiento_caja_id`,`sucursal_id`,`renglon`),
  CONSTRAINT `movimiento_caja_id_movimientos_caja_detalles` FOREIGN KEY (`movimiento_caja_id`, `sucursal_id`) REFERENCES `movimientos_caja` (`movimiento_caja_id`, `sucursal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimientos_caja_detalles`
--

LOCK TABLES `movimientos_caja_detalles` WRITE;
/*!40000 ALTER TABLE `movimientos_caja_detalles` DISABLE KEYS */;
INSERT INTO `movimientos_caja_detalles` VALUES (32,1,1,'ENTRADA','ENTRADA DE CAJA DE MATRIZ A FRUTERIA',1852.00),(32,1,2,'ENTRADA','ENTRADA DE LAPTOP',4566.00),(35,1,1,'ENTRADA','ENTRADA DE LAPTOP',1200.00),(36,1,1,'ENTRADA','SALIDAD DE LAPTOP',120.00),(38,1,1,'SALIDA','TELEFONO SALIDA',4567.00),(39,1,1,'ENTRADA','ENTRADA',400.00),(40,1,1,'SALIDA','SALIDA',100.00),(43,1,1,'ENTRADA','ENTRADA',100.00),(48,1,1,'ENTRADA','ENTRADA LAPTOP',1500.00),(48,1,2,'SALIDA','SALIDAD LAPTOP',500.00),(48,1,3,'ENTRADA','ENTRADA TELFONO',400.00),(49,1,1,'ENTRADA','ENTRADA',1000.00);
/*!40000 ALTER TABLE `movimientos_caja_detalles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimientos_caja_tipos`
--

DROP TABLE IF EXISTS `movimientos_caja_tipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movimientos_caja_tipos` (
  `movimiento_caja_tipo_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(250) NOT NULL,
  `tipo` enum('ENTRADA','SALIDA') NOT NULL DEFAULT 'ENTRADA',
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`movimiento_caja_tipo_id`),
  UNIQUE KEY `UNIQUE` (`tipo`,`descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimientos_caja_tipos`
--

LOCK TABLES `movimientos_caja_tipos` WRITE;
/*!40000 ALTER TABLE `movimientos_caja_tipos` DISABLE KEYS */;
INSERT INTO `movimientos_caja_tipos` VALUES (9,'EFECTIVO','ENTRADA','ACTIVO','2020-01-30 09:45:14',27,NULL,NULL,NULL,NULL),(10,'EFECTIVO','SALIDA','ACTIVO','2020-01-30 09:45:14',27,NULL,NULL,NULL,NULL),(11,'DEPOSITO POR PRESTAMO','ENTRADA','ACTIVO','2020-01-30 16:09:43',27,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `movimientos_caja_tipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `municipios`
--

DROP TABLE IF EXISTS `municipios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `municipios` (
  `municipio_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `estado_id` tinyint(3) unsigned NOT NULL,
  `codigo` char(5) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`municipio_id`),
  UNIQUE KEY `UNIQUE_codigo` (`codigo`),
  UNIQUE KEY `UNIQUE_descripcion` (`estado_id`,`descripcion`),
  CONSTRAINT `estado_id_municipios` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`estado_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `municipios`
--

LOCK TABLES `municipios` WRITE;
/*!40000 ALTER TABLE `municipios` DISABLE KEYS */;
INSERT INTO `municipios` VALUES (25,40,'00001','CULIACÁN','ACTIVO','2019-09-28 08:52:43',0,'2020-01-31 16:56:50',27,NULL,NULL),(26,40,'00002','MAZATLÁN','ACTIVO','2019-10-11 10:14:48',0,NULL,NULL,NULL,NULL),(27,41,'00003','OTROERERER','INACTIVO','2020-01-22 16:11:01',28,'2020-02-07 15:57:37',28,'2020-03-14 11:29:24',0),(28,44,'00004','ANGOSTURA','ACTIVO','2020-01-22 16:22:35',28,'2020-01-29 09:41:51',27,NULL,NULL);
/*!40000 ALTER TABLE `municipios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocupaciones`
--

DROP TABLE IF EXISTS `ocupaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ocupaciones` (
  `ocupacion_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` char(3) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`ocupacion_id`),
  UNIQUE KEY `UNIQUE_codigo` (`codigo`),
  UNIQUE KEY `UNIQUE_descripcion` (`descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocupaciones`
--

LOCK TABLES `ocupaciones` WRITE;
/*!40000 ALTER TABLE `ocupaciones` DISABLE KEYS */;
INSERT INTO `ocupaciones` VALUES (5,'001','EMPLEADO','INACTIVO','2019-09-22 12:27:45',0,'2020-02-07 15:44:39',28,'2019-12-11 13:28:20',0),(8,'002','EJECUTIVO','ACTIVO','2020-01-20 10:10:32',0,'2020-02-07 15:43:39',28,NULL,NULL),(9,'003','OTRA','ACTIVO','2020-01-22 16:09:15',28,'2020-01-31 16:51:22',27,NULL,NULL),(16,'004','VARIAS','ACTIVO','2020-01-24 16:22:57',28,'2020-01-31 16:51:22',27,NULL,NULL),(17,'005','COMERCIANTE','ACTIVO','2020-02-07 15:42:21',28,NULL,NULL,NULL,NULL),(18,'006','OTRAS','ACTIVO','2020-02-07 15:43:39',28,NULL,NULL,NULL,NULL),(19,'007','OTRAG','ACTIVO','2020-02-08 08:24:49',27,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `ocupaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagos`
--

DROP TABLE IF EXISTS `pagos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pagos` (
  `pago_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sucursal_id` tinyint(3) unsigned NOT NULL,
  `caja_id` tinyint(3) unsigned NOT NULL,
  `usuario_id` int(11) unsigned NOT NULL,
  `folio` varchar(10) NOT NULL,
  `fecha` datetime NOT NULL,
  `cliente_id` int(11) unsigned NOT NULL,
  `forma_pago_id` tinyint(3) unsigned DEFAULT NULL,
  `tipo_recibo` enum('LIQUIDACION','REFRENDO','PARCIAL') NOT NULL DEFAULT 'LIQUIDACION',
  `fotografia_imagen` varchar(250) DEFAULT NULL,
  `motivo_cancelacion` varchar(500) DEFAULT NULL,
  `total_pagar` decimal(11,2) NOT NULL,
  `importe_pagado` decimal(11,2) NOT NULL,
  `caja_corte_id` int(11) unsigned DEFAULT NULL,
  `estatus` enum('ACTIVO','CANCELADO','PAGO PARCIAL DE INTERESES','LIQUIDACION INTERNA') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`pago_id`,`sucursal_id`),
  UNIQUE KEY `UNIQUE_folio` (`sucursal_id`,`folio`),
  KEY `caja_id_pagos` (`caja_id`),
  KEY `usuario_id_pagos` (`usuario_id`),
  KEY `forma_pago_id_pagos_idx` (`forma_pago_id`),
  KEY `cliente_id_pagos_idx` (`cliente_id`),
  KEY `caja_corte_id_pagos_idx` (`caja_corte_id`),
  CONSTRAINT `caja_corte_id_pagos` FOREIGN KEY (`caja_corte_id`) REFERENCES `cajas_corte` (`caja_corte_id`),
  CONSTRAINT `caja_id_pagos` FOREIGN KEY (`caja_id`) REFERENCES `cajas` (`caja_id`),
  CONSTRAINT `cliente_id_pagos` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`cliente_id`),
  CONSTRAINT `forma_pago_id_pagos` FOREIGN KEY (`forma_pago_id`) REFERENCES `formas_pago` (`forma_pago_id`),
  CONSTRAINT `sucursal_id_pagos` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursales` (`sucursal_id`),
  CONSTRAINT `usuario_id_pagos` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagos`
--

LOCK TABLES `pagos` WRITE;
/*!40000 ALTER TABLE `pagos` DISABLE KEYS */;
INSERT INTO `pagos` VALUES (32,1,9,28,'RL00000001','2020-03-04 13:32:49',30,5,'REFRENDO','reciboLiqRL00000001.jpg','PARA VER',2.72,3.00,NULL,'CANCELADO','2020-03-04 13:32:50',28,NULL,NULL,'2020-03-04 13:33:56',28),(33,1,9,28,'RL00000003','2020-03-04 13:34:13',30,6,'REFRENDO','reciboLiqRL00000003.jpg','SI',2.72,3.00,NULL,'CANCELADO','2020-03-04 13:34:14',28,NULL,NULL,'2020-03-13 14:17:49',28),(36,1,9,28,'RL00000005','2020-03-05 12:21:56',30,4,'PARCIAL','reciboLiqRL00000005.jpg',NULL,4.52,5.00,NULL,'PAGO PARCIAL DE INTERESES','2020-03-05 12:21:56',28,NULL,NULL,NULL,NULL),(37,1,9,28,'RL00000006','2020-03-05 12:27:57',30,4,'PARCIAL','reciboLiqRL00000006.jpg',NULL,28.56,30.00,NULL,'PAGO PARCIAL DE INTERESES','2020-03-05 12:27:57',28,NULL,NULL,NULL,NULL),(38,1,9,28,'RL00000007','2020-03-05 12:28:44',30,4,'PARCIAL','reciboLiqRL00000007.jpg','EL BILLETE ERA FALSO',8.52,9.00,NULL,'CANCELADO','2020-03-05 12:28:44',28,NULL,NULL,'2020-03-05 12:30:19',28),(51,1,9,28,'RL00000028','2020-03-10 16:04:27',30,4,'REFRENDO','reciboLiqRL00000028.jpg','BILLETE FALSO',783.93,800.00,NULL,'CANCELADO','2020-03-05 16:04:27',28,NULL,NULL,'2020-03-05 16:07:40',28),(52,1,9,28,'RL00000030','2020-03-10 16:09:38',30,4,'REFRENDO','reciboLiqRL00000030.jpg',NULL,783.93,800.00,NULL,'ACTIVO','2020-03-05 16:09:39',28,NULL,NULL,NULL,NULL),(53,1,9,28,'RL00000032','2020-03-05 16:25:25',30,4,'REFRENDO','reciboLiqRL00000032.jpg',NULL,0.00,0.00,NULL,'ACTIVO','2020-03-05 16:25:25',28,NULL,NULL,NULL,NULL),(54,1,9,28,'RL00000034','2020-03-05 16:27:48',30,4,'REFRENDO','reciboLiqRL00000034.jpg',NULL,3.40,3.40,NULL,'ACTIVO','2020-03-05 16:27:48',28,NULL,NULL,NULL,NULL),(55,1,9,28,'RL00000036','2020-03-05 16:30:07',30,4,'REFRENDO','reciboLiqRL00000036.jpg',NULL,2.04,2.04,NULL,'ACTIVO','2020-03-05 16:30:07',28,NULL,NULL,NULL,NULL),(56,1,9,28,'RL00000038','2020-03-10 12:35:08',31,4,'REFRENDO','reciboLiqRL00000038.jpg',NULL,12.24,15.00,NULL,'ACTIVO','2020-03-10 12:35:08',28,NULL,NULL,NULL,NULL),(57,1,9,28,'RL00000040','2020-03-10 12:40:35',31,4,'REFRENDO','reciboLiqRL00000040.jpg','ERA UN BILLETE FALSO',12.24,15.00,NULL,'CANCELADO','2020-03-10 12:40:35',28,NULL,NULL,'2020-03-10 12:43:38',28),(74,1,9,28,'RL00000047','2020-03-17 14:27:07',30,4,'REFRENDO','reciboLiqRL00000047.jpg',NULL,8.57,9.00,NULL,'ACTIVO','2020-03-12 14:27:08',28,NULL,NULL,NULL,NULL),(82,1,9,27,'RL00000056','2020-03-19 09:11:47',32,4,'REFRENDO','reciboLiqRL00000056.jpg',NULL,10.40,10.40,NULL,'ACTIVO','2020-03-13 09:11:47',27,NULL,NULL,NULL,NULL),(83,1,9,27,'RL00000058','2020-03-27 09:13:17',32,4,'PARCIAL','reciboLiqRL00000058.jpg',NULL,476.00,476.00,NULL,'PAGO PARCIAL DE INTERESES','2020-03-13 09:13:18',27,NULL,NULL,NULL,NULL),(84,1,9,27,'RL00000059','2020-03-13 09:17:46',32,4,'PARCIAL','reciboLiqRL00000059.jpg',NULL,138.00,200.00,NULL,'PAGO PARCIAL DE INTERESES','2020-03-13 09:17:46',27,NULL,NULL,NULL,NULL),(85,1,9,27,'RL00000060','2020-03-13 09:23:38',32,4,'PARCIAL','reciboLiqRL00000060.jpg',NULL,1566.00,1700.00,NULL,'PAGO PARCIAL DE INTERESES','2020-03-13 09:23:38',27,NULL,NULL,NULL,NULL),(86,1,9,27,'RL00000061','2020-03-26 09:29:03',32,4,'REFRENDO','reciboLiqRL00000061.jpg',NULL,23116.00,24000.00,NULL,'ACTIVO','2020-03-13 09:29:03',27,NULL,NULL,NULL,NULL),(87,1,9,27,'RL00000063','2020-03-13 09:31:39',32,4,'LIQUIDACION','reciboLiqRL00000063.jpg',NULL,100.34,200.00,NULL,'ACTIVO','2020-03-13 09:31:39',27,NULL,NULL,NULL,NULL),(88,1,9,27,'RL00000064','2020-03-13 09:44:33',41,NULL,'LIQUIDACION',NULL,NULL,1200.00,0.00,NULL,'LIQUIDACION INTERNA','2020-03-13 00:00:00',27,NULL,NULL,NULL,NULL),(89,1,9,27,'RL00000065','2020-03-13 09:44:33',41,NULL,'LIQUIDACION',NULL,NULL,12000.00,0.00,NULL,'LIQUIDACION INTERNA','2020-03-13 00:00:00',27,NULL,NULL,NULL,NULL),(90,1,9,28,'RL00000066','2020-03-31 14:19:07',30,4,'REFRENDO','reciboLiqRL00000066.jpg',NULL,63.44,70.00,NULL,'ACTIVO','2020-03-13 14:19:07',28,NULL,NULL,NULL,NULL),(91,1,9,28,'RL00000068','2020-05-26 14:23:15',30,4,'REFRENDO','reciboLiqRL00000068.jpg',NULL,137.09,137.09,NULL,'ACTIVO','2020-03-13 14:23:15',28,NULL,NULL,NULL,NULL),(92,1,9,28,'RL00000070','2020-03-13 14:24:43',30,4,'PARCIAL','reciboLiqRL00000070.jpg','SI',66.64,66.64,NULL,'CANCELADO','2020-03-13 14:24:43',28,NULL,NULL,'2020-03-13 14:26:17',28),(93,1,9,27,'RL00000071','2020-03-13 14:50:36',41,NULL,'LIQUIDACION',NULL,NULL,13500.00,0.00,NULL,'LIQUIDACION INTERNA','2020-03-13 00:00:00',27,NULL,NULL,NULL,NULL),(94,1,9,27,'RL00000072','2020-03-13 14:53:20',41,NULL,'LIQUIDACION',NULL,NULL,1852.20,0.00,NULL,'LIQUIDACION INTERNA','2020-03-13 00:00:00',27,NULL,NULL,NULL,NULL),(95,1,9,27,'RL00000073','2020-03-13 14:53:20',41,NULL,'LIQUIDACION',NULL,NULL,1058.40,0.00,NULL,'LIQUIDACION INTERNA','2020-03-13 00:00:00',27,NULL,NULL,NULL,NULL),(96,1,9,27,'RL00000074','2020-03-13 14:57:36',41,NULL,'LIQUIDACION',NULL,NULL,2254.60,0.00,NULL,'LIQUIDACION INTERNA','2020-03-13 00:00:00',27,NULL,NULL,NULL,NULL),(97,1,9,28,'RL00000075','2020-03-13 15:03:43',41,NULL,'LIQUIDACION',NULL,NULL,608.08,0.00,NULL,'LIQUIDACION INTERNA','2020-03-13 00:00:00',28,NULL,NULL,NULL,NULL),(98,1,9,28,'RL00000076','2020-03-13 15:12:49',41,NULL,'LIQUIDACION',NULL,NULL,608.08,0.00,NULL,'LIQUIDACION INTERNA','2020-03-13 00:00:00',28,NULL,NULL,NULL,NULL),(99,1,9,28,'RL00000077','2020-03-17 16:56:53',32,4,'REFRENDO','reciboLiqRL00000077.jpg',NULL,61.20,62.00,NULL,'ACTIVO','2020-03-13 16:56:53',28,NULL,NULL,NULL,NULL),(103,1,9,28,'RL00000085','2020-08-25 08:35:46',30,4,'REFRENDO','reciboLiqRL00000085.jpg',NULL,411.26,500.00,NULL,'ACTIVO','2020-03-20 08:35:46',28,NULL,NULL,NULL,NULL),(105,1,9,28,'RL00000089','2020-05-26 11:17:09',30,4,'REFRENDO','reciboLiqRL00000089.jpg',NULL,172.20,200.00,NULL,'ACTIVO','2020-03-20 11:17:10',28,NULL,NULL,NULL,NULL),(106,1,9,28,'RL00000091','2020-03-20 11:47:04',30,NULL,'LIQUIDACION',NULL,NULL,660.00,0.00,NULL,'LIQUIDACION INTERNA','2020-03-20 00:00:00',28,NULL,NULL,NULL,NULL),(107,1,9,28,'RL00000092','2020-03-20 11:53:13',31,NULL,'LIQUIDACION',NULL,NULL,600.00,0.00,NULL,'LIQUIDACION INTERNA','2020-03-20 00:00:00',28,NULL,NULL,NULL,NULL),(108,1,9,28,'RL00000093','2020-03-20 12:16:11',30,4,'LIQUIDACION','reciboLiqRL00000093.jpg',NULL,827.20,900.00,NULL,'ACTIVO','2020-03-20 12:16:11',28,NULL,NULL,NULL,NULL),(115,1,9,28,'RL00000094','2020-03-23 12:23:47',30,5,'LIQUIDACION','reciboLiqRL00000094.jpg',NULL,6671.68,7000.00,NULL,'ACTIVO','2020-03-23 12:23:47',28,NULL,NULL,NULL,NULL),(116,1,10,28,'RL00000095','2020-03-23 14:48:58',30,4,'LIQUIDACION','reciboLiqRL00000095.jpg',NULL,400.36,401.00,NULL,'ACTIVO','2020-03-23 14:48:58',28,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `pagos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagos_detalles`
--

DROP TABLE IF EXISTS `pagos_detalles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pagos_detalles` (
  `pago_id` int(11) unsigned NOT NULL,
  `renglon` tinyint(3) unsigned NOT NULL,
  `boleta_empeno_id` int(11) unsigned NOT NULL,
  `abono_capital` decimal(11,2) DEFAULT NULL,
  `dias_intereses` int(11) DEFAULT NULL,
  `meses_intereses` int(11) DEFAULT NULL,
  `semanas_ppi` int(11) DEFAULT NULL,
  `fecha_autorizacion` datetime DEFAULT NULL,
  `usuario_autorizacion` int(11) DEFAULT NULL,
  `motivo_autorizacion` varchar(500) DEFAULT NULL,
  `porcentaje_promocion` decimal(4,2) DEFAULT NULL,
  `dia_promocion` enum('LUNES','MARTES','MIERCOLES','JUEVES','VIERNES','SABADO','DOMINGO') DEFAULT NULL,
  `dias_minimo_promocion` tinyint(3) DEFAULT NULL,
  `prenda_tipo_id` tinyint(3) DEFAULT NULL,
  `porcentaje_interes_interna` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`pago_id`,`renglon`),
  KEY `boleta_id_pagos_detalles_idx` (`boleta_empeno_id`),
  CONSTRAINT `boleta_id_pagos_detalles` FOREIGN KEY (`boleta_empeno_id`) REFERENCES `boletas_empeno` (`boleta_empeno_id`),
  CONSTRAINT `pago_id_pagos_detalles` FOREIGN KEY (`pago_id`) REFERENCES `pagos` (`pago_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagos_detalles`
--

LOCK TABLES `pagos_detalles` WRITE;
/*!40000 ALTER TABLE `pagos_detalles` DISABLE KEYS */;
INSERT INTO `pagos_detalles` VALUES (32,1,45,0.00,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(33,1,45,0.00,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(36,1,46,0.00,NULL,NULL,1,'2020-03-05 12:22:17',28,'NO TENGO DINERO',NULL,NULL,NULL,NULL,NULL),(37,1,46,0.00,NULL,NULL,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(38,1,46,0.00,NULL,NULL,1,'2020-03-05 12:28:56',28,'A',NULL,NULL,NULL,NULL,NULL),(51,1,46,0.00,737,NULL,NULL,'2020-03-05 16:04:44',29,'NO SE',NULL,NULL,NULL,NULL,NULL),(51,2,48,0.00,6,NULL,NULL,NULL,NULL,NULL,10.00,'MARTES',NULL,4,NULL),(52,1,46,0.00,737,NULL,NULL,'2020-03-05 16:10:05',28,'NO SE ',NULL,NULL,NULL,NULL,NULL),(52,2,48,0.00,6,NULL,NULL,NULL,NULL,NULL,10.00,'MARTES',NULL,4,NULL),(53,1,77,0.00,1,NULL,NULL,'2020-03-05 16:26:47',29,'NO SE',NULL,NULL,NULL,NULL,NULL),(54,1,78,0.00,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(55,1,79,0.00,1,NULL,NULL,NULL,NULL,NULL,40.00,'JUEVES',NULL,18,NULL),(56,1,85,0.00,1,NULL,NULL,NULL,NULL,NULL,10.00,'MARTES',NULL,4,NULL),(57,1,87,0.00,1,NULL,NULL,NULL,NULL,NULL,10.00,'MARTES',NULL,4,NULL),(74,1,75,0.00,7,NULL,NULL,NULL,NULL,NULL,10.00,'MARTES',NULL,10,NULL),(82,1,91,0.00,6,NULL,NULL,'2020-03-13 09:12:06',28,'SI',NULL,NULL,NULL,NULL,NULL),(83,1,90,0.00,NULL,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(84,1,90,0.00,NULL,NULL,1,'2020-03-13 09:18:18',28,'SE BONIFICO',NULL,NULL,NULL,NULL,NULL),(85,1,90,0.00,NULL,NULL,7,'2020-03-13 09:24:27',28,'SE BONIFICO',NULL,NULL,NULL,NULL,NULL),(86,1,90,0.00,744,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(87,1,92,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(88,1,93,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20.00),(89,1,94,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20.00),(90,1,45,0.00,27,NULL,NULL,'2020-03-13 14:20:23',28,'SI',NULL,NULL,NULL,NULL,NULL),(91,1,98,0.00,56,NULL,NULL,NULL,NULL,NULL,10.00,'MARTES',NULL,4,NULL),(92,1,89,0.00,NULL,NULL,7,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(93,1,87,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,50.00),(94,1,89,NULL,5,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00),(94,2,80,NULL,5,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00),(95,1,99,NULL,5,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00),(96,1,83,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00),(97,1,95,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00),(98,1,95,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00),(99,1,96,0.00,4,NULL,NULL,NULL,NULL,NULL,10.00,'MARTES',NULL,4,NULL),(103,1,76,0.00,168,NULL,NULL,NULL,NULL,NULL,10.00,'MARTES',30,4,NULL),(106,1,109,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10.00),(106,2,110,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10.00),(107,1,113,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00),(107,2,114,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00),(108,1,76,NULL,10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(115,1,111,NULL,369,NULL,NULL,'2020-03-23 12:24:02',28,'NO SE ',NULL,NULL,NULL,NULL,NULL),(115,2,112,NULL,3,NULL,NULL,'2020-03-23 12:24:14',28,'NO SE ',NULL,NULL,NULL,NULL,NULL),(115,3,118,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(116,1,119,NULL,1,NULL,NULL,'2020-03-23 14:50:09',29,'NO SE ',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `pagos_detalles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagos_detalles_cobros_tipos`
--

DROP TABLE IF EXISTS `pagos_detalles_cobros_tipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pagos_detalles_cobros_tipos` (
  `pago_id` int(11) unsigned NOT NULL,
  `renglon_detalle` tinyint(3) unsigned NOT NULL,
  `renglon` tinyint(3) NOT NULL,
  `tipo` enum('INTERESES','BONIFICACION','PPI') NOT NULL,
  `cobro_tipo_id` tinyint(3) unsigned NOT NULL,
  `importe` decimal(11,6) DEFAULT NULL,
  `porcentaje` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`pago_id`,`renglon_detalle`,`renglon`,`tipo`),
  KEY `cobro_tipo_id_pagos_detalles_idx` (`cobro_tipo_id`),
  KEY `renglon_detalle_pagos_detalles_idx` (`renglon_detalle`),
  KEY `pago_id_pagos_detalle_idx` (`pago_id`),
  CONSTRAINT `cobro_tipo_id_pagos_detalle` FOREIGN KEY (`cobro_tipo_id`) REFERENCES `cobros_tipos` (`cobro_tipo_id`),
  CONSTRAINT `pago_id_pagos_det` FOREIGN KEY (`pago_id`) REFERENCES `pagos_detalles` (`pago_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagos_detalles_cobros_tipos`
--

LOCK TABLES `pagos_detalles_cobros_tipos` WRITE;
/*!40000 ALTER TABLE `pagos_detalles_cobros_tipos` DISABLE KEYS */;
INSERT INTO `pagos_detalles_cobros_tipos` VALUES (32,1,1,'INTERESES',1,0.800000,0.10),(32,1,2,'INTERESES',2,0.960000,0.12),(32,1,3,'INTERESES',3,0.960000,0.12),(33,1,1,'INTERESES',1,0.800000,0.10),(33,1,2,'INTERESES',2,0.960000,0.12),(33,1,3,'INTERESES',3,0.960000,0.12),(36,1,1,'BONIFICACION',1,1.470588,0.10),(36,1,1,'PPI',1,2.800000,0.10),(36,1,2,'BONIFICACION',2,1.764706,0.12),(36,1,2,'PPI',2,3.360000,0.12),(36,1,3,'BONIFICACION',3,1.764706,0.12),(36,1,3,'PPI',3,3.360000,0.12),(37,1,1,'PPI',1,8.400000,0.10),(37,1,2,'PPI',2,10.080000,0.12),(37,1,3,'PPI',3,10.080000,0.12),(38,1,1,'BONIFICACION',1,0.294118,0.10),(38,1,1,'PPI',1,2.800000,0.10),(38,1,2,'BONIFICACION',2,0.352941,0.12),(38,1,2,'PPI',2,3.360000,0.12),(38,1,3,'BONIFICACION',3,0.352941,0.12),(38,1,3,'PPI',3,3.360000,0.12),(51,1,1,'INTERESES',1,294.800000,0.10),(51,1,1,'BONIFICACION',1,58.823529,0.10),(51,1,1,'PPI',1,9.729412,0.10),(51,1,2,'INTERESES',2,353.760000,0.12),(51,1,2,'BONIFICACION',2,70.588235,0.12),(51,1,2,'PPI',2,11.675294,0.12),(51,1,3,'INTERESES',3,353.760000,0.12),(51,1,3,'BONIFICACION',3,70.588235,0.12),(51,1,3,'PPI',3,11.675294,0.12),(51,2,1,'INTERESES',1,4.800000,0.10),(51,2,1,'BONIFICACION',1,0.479412,0.10),(51,2,2,'INTERESES',2,5.760000,0.12),(51,2,2,'BONIFICACION',2,0.575294,0.12),(51,2,3,'INTERESES',3,5.760000,0.12),(51,2,3,'BONIFICACION',3,0.575294,0.12),(52,1,1,'INTERESES',1,294.800000,0.10),(52,1,1,'BONIFICACION',1,58.823529,0.10),(52,1,1,'PPI',1,9.729412,0.10),(52,1,2,'INTERESES',2,353.760000,0.12),(52,1,2,'BONIFICACION',2,70.588235,0.12),(52,1,2,'PPI',2,11.675294,0.12),(52,1,3,'INTERESES',3,353.760000,0.12),(52,1,3,'BONIFICACION',3,70.588235,0.12),(52,1,3,'PPI',3,11.675294,0.12),(52,2,1,'INTERESES',1,4.800000,0.10),(52,2,1,'BONIFICACION',1,0.479412,0.10),(52,2,2,'INTERESES',2,5.760000,0.12),(52,2,2,'BONIFICACION',2,0.575294,0.12),(52,2,3,'INTERESES',3,5.760000,0.12),(52,2,3,'BONIFICACION',3,0.575294,0.12),(53,1,1,'INTERESES',1,1.000000,0.10),(53,1,1,'BONIFICACION',1,1.000000,0.10),(53,1,2,'INTERESES',2,1.200000,0.12),(53,1,2,'BONIFICACION',2,1.200000,0.12),(53,1,3,'INTERESES',3,1.200000,0.12),(53,1,3,'BONIFICACION',3,1.200000,0.12),(54,1,1,'INTERESES',1,1.000000,0.10),(54,1,2,'INTERESES',2,1.200000,0.12),(54,1,3,'INTERESES',3,1.200000,0.12),(55,1,1,'INTERESES',1,1.000000,0.10),(55,1,1,'BONIFICACION',1,0.400000,0.10),(55,1,2,'INTERESES',2,1.200000,0.12),(55,1,2,'BONIFICACION',2,0.480000,0.12),(55,1,3,'INTERESES',3,1.200000,0.12),(55,1,3,'BONIFICACION',3,0.480000,0.12),(56,1,1,'INTERESES',1,4.000000,0.10),(56,1,1,'BONIFICACION',1,0.400000,0.10),(56,1,2,'INTERESES',2,4.800000,0.12),(56,1,2,'BONIFICACION',2,0.480000,0.12),(56,1,3,'INTERESES',3,4.800000,0.12),(56,1,3,'BONIFICACION',3,0.480000,0.12),(57,1,1,'INTERESES',1,4.000000,0.10),(57,1,1,'BONIFICACION',1,0.400000,0.10),(57,1,2,'INTERESES',2,4.800000,0.12),(57,1,2,'BONIFICACION',2,0.480000,0.12),(57,1,3,'INTERESES',3,4.800000,0.12),(57,1,3,'BONIFICACION',3,0.480000,0.12),(74,1,1,'INTERESES',1,2.800000,0.10),(74,1,1,'BONIFICACION',1,0.279412,0.10),(74,1,2,'INTERESES',2,3.360000,0.12),(74,1,2,'BONIFICACION',2,0.335294,0.12),(74,1,3,'INTERESES',3,3.360000,0.12),(74,1,3,'BONIFICACION',3,0.335294,0.12),(82,1,1,'INTERESES',1,6.000000,0.10),(82,1,1,'BONIFICACION',1,2.941176,0.10),(82,1,2,'INTERESES',2,7.200000,0.12),(82,1,2,'BONIFICACION',2,3.529412,0.12),(82,1,3,'INTERESES',3,7.200000,0.12),(82,1,3,'BONIFICACION',3,3.529412,0.12),(83,1,1,'PPI',1,140.000000,0.10),(83,1,2,'PPI',2,168.000000,0.12),(83,1,3,'PPI',3,168.000000,0.12),(84,1,1,'BONIFICACION',1,29.411765,0.10),(84,1,1,'PPI',1,70.000000,0.10),(84,1,2,'BONIFICACION',2,35.294118,0.12),(84,1,2,'PPI',2,84.000000,0.12),(84,1,3,'BONIFICACION',3,35.294118,0.12),(84,1,3,'PPI',3,84.000000,0.12),(85,1,1,'BONIFICACION',1,29.411765,0.10),(85,1,1,'PPI',1,490.000000,0.10),(85,1,2,'BONIFICACION',2,35.294118,0.12),(85,1,2,'PPI',2,588.000000,0.12),(85,1,3,'BONIFICACION',3,35.294118,0.12),(85,1,3,'PPI',3,588.000000,0.12),(86,1,1,'INTERESES',1,7440.000000,0.10),(86,1,1,'PPI',1,641.176471,0.10),(86,1,2,'INTERESES',2,8928.000000,0.12),(86,1,2,'PPI',2,769.411765,0.12),(86,1,3,'INTERESES',3,8928.000000,0.12),(86,1,3,'PPI',3,769.411765,0.12),(87,1,1,'INTERESES',1,0.100000,0.10),(87,1,2,'INTERESES',2,0.120000,0.12),(87,1,3,'INTERESES',3,0.120000,0.12),(88,1,1,'INTERESES',1,58.823529,0.10),(88,1,2,'INTERESES',2,70.588235,0.12),(88,1,3,'INTERESES',3,70.588235,0.12),(89,1,1,'INTERESES',1,588.235294,0.10),(89,1,2,'INTERESES',2,705.882353,0.12),(89,1,3,'INTERESES',3,705.882353,0.12),(90,1,1,'INTERESES',1,21.600000,0.10),(90,1,1,'BONIFICACION',1,2.941176,0.10),(90,1,2,'INTERESES',2,25.920000,0.12),(90,1,2,'BONIFICACION',2,3.529412,0.12),(90,1,3,'INTERESES',3,25.920000,0.12),(90,1,3,'BONIFICACION',3,3.529412,0.12),(91,1,1,'INTERESES',1,44.800000,0.10),(91,1,1,'BONIFICACION',1,4.479412,0.10),(91,1,2,'INTERESES',2,53.760000,0.12),(91,1,2,'BONIFICACION',2,5.375294,0.12),(91,1,3,'INTERESES',3,53.760000,0.12),(91,1,3,'BONIFICACION',3,5.375294,0.12),(92,1,1,'PPI',1,19.600000,0.10),(92,1,2,'PPI',2,23.520000,0.12),(92,1,3,'PPI',3,23.520000,0.12),(93,1,1,'INTERESES',1,1323.529412,0.10),(93,1,2,'INTERESES',2,1588.235294,0.12),(93,1,3,'INTERESES',3,1588.235294,0.12),(94,1,1,'INTERESES',1,38.000000,0.10),(94,1,2,'INTERESES',2,45.600000,0.12),(94,1,3,'INTERESES',3,45.600000,0.12),(94,2,1,'INTERESES',1,95.000000,0.10),(94,2,2,'INTERESES',2,114.000000,0.12),(94,2,3,'INTERESES',3,114.000000,0.12),(95,1,1,'INTERESES',1,76.000000,0.10),(95,1,2,'INTERESES',2,91.200000,0.12),(95,1,3,'INTERESES',3,91.200000,0.12),(96,1,1,'INTERESES',1,369.000000,0.10),(96,1,2,'INTERESES',2,442.800000,0.12),(96,1,3,'INTERESES',3,442.800000,0.12),(97,1,1,'INTERESES',1,61.200000,0.10),(97,1,2,'INTERESES',2,73.440000,0.12),(97,1,3,'INTERESES',3,73.440000,0.12),(98,1,1,'INTERESES',1,61.200000,0.10),(98,1,2,'INTERESES',2,73.440000,0.12),(98,1,3,'INTERESES',3,73.440000,0.12),(99,1,1,'INTERESES',1,20.000000,0.10),(99,1,1,'BONIFICACION',1,2.000000,0.10),(99,1,2,'INTERESES',2,24.000000,0.12),(99,1,2,'BONIFICACION',2,2.400000,0.12),(99,1,3,'INTERESES',3,24.000000,0.12),(99,1,3,'BONIFICACION',3,2.400000,0.12),(106,1,1,'INTERESES',1,5.882353,0.10),(106,1,2,'INTERESES',2,7.058824,0.12),(106,1,3,'INTERESES',3,7.058824,0.12),(106,2,1,'INTERESES',1,11.764706,0.10),(106,2,2,'INTERESES',2,14.117647,0.12),(106,2,3,'INTERESES',3,14.117647,0.12),(107,1,1,'INTERESES',1,0.000000,0.10),(107,1,2,'INTERESES',2,0.000000,0.12),(107,1,3,'INTERESES',3,0.000000,0.12),(107,2,1,'INTERESES',1,0.000000,0.10),(107,2,2,'INTERESES',2,0.000000,0.12),(107,2,3,'INTERESES',3,0.000000,0.12),(108,1,1,'INTERESES',1,8.000000,0.10),(108,1,2,'INTERESES',2,9.600000,0.12),(108,1,3,'INTERESES',3,9.600000,0.12),(115,1,1,'INTERESES',1,369.000000,0.10),(115,1,1,'BONIFICACION',1,294.117647,0.10),(115,1,2,'INTERESES',2,442.800000,0.12),(115,1,2,'BONIFICACION',2,352.941176,0.12),(115,1,3,'INTERESES',3,442.800000,0.12),(115,1,3,'BONIFICACION',3,352.941176,0.12),(115,2,1,'INTERESES',1,1.200000,0.10),(115,2,1,'BONIFICACION',1,1.176471,0.10),(115,2,2,'INTERESES',2,1.440000,0.12),(115,2,2,'BONIFICACION',2,1.411765,0.12),(115,2,3,'INTERESES',3,1.440000,0.12),(115,2,3,'BONIFICACION',3,1.411765,0.12),(115,3,1,'INTERESES',1,5.000000,0.10),(115,3,2,'INTERESES',2,6.000000,0.12),(115,3,3,'INTERESES',3,6.000000,0.12),(116,1,1,'INTERESES',1,0.400000,0.10),(116,1,1,'BONIFICACION',1,0.294118,0.10),(116,1,2,'INTERESES',2,0.480000,0.12),(116,1,2,'BONIFICACION',2,0.352941,0.12),(116,1,3,'INTERESES',3,0.480000,0.12),(116,1,3,'BONIFICACION',3,0.352941,0.12);
/*!40000 ALTER TABLE `pagos_detalles_cobros_tipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prendas_subtipos`
--

DROP TABLE IF EXISTS `prendas_subtipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prendas_subtipos` (
  `prenda_subtipo_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `prenda_tipo_id` tinyint(3) unsigned NOT NULL,
  `codigo` char(3) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`prenda_subtipo_id`),
  UNIQUE KEY `UNIQUE_codigo` (`codigo`),
  UNIQUE KEY `UNIQUE_descripcion` (`prenda_tipo_id`,`descripcion`),
  KEY `prenda_tipo_id_prendas_subtipos` (`prenda_tipo_id`),
  CONSTRAINT `prenda_tipo_id_prendas_subtipos` FOREIGN KEY (`prenda_tipo_id`) REFERENCES `prendas_tipos` (`prenda_tipo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prendas_subtipos`
--

LOCK TABLES `prendas_subtipos` WRITE;
/*!40000 ALTER TABLE `prendas_subtipos` DISABLE KEYS */;
INSERT INTO `prendas_subtipos` VALUES (5,13,'001','ROLEX','ACTIVO','2019-12-19 11:28:09',0,'2020-01-31 16:58:13',27,NULL,NULL),(6,13,'002','CARTIER','ACTIVO','2019-12-19 11:28:18',0,'2020-01-31 16:58:13',27,NULL,NULL),(7,13,'003','CITIZEN','ACTIVO','2019-12-19 11:28:43',0,'2020-01-31 16:58:13',27,NULL,NULL),(8,13,'004','NÁUTICA','ACTIVO','2019-12-19 11:29:01',0,'2020-01-31 16:58:13',27,NULL,NULL),(9,13,'005','MICHAEL KORS','ACTIVO','2019-12-19 11:29:17',0,'2020-01-31 16:58:13',27,NULL,NULL),(10,13,'006','DKNY','ACTIVO','2019-12-19 11:29:31',0,'2020-01-31 16:58:13',27,NULL,NULL),(11,13,'007','CALVIN KLEIN','ACTIVO','2019-12-19 11:29:51',0,'2020-01-31 16:58:13',27,NULL,NULL),(12,4,'008','ESCLAVA','ACTIVO','2019-12-19 11:30:51',0,'2020-01-31 16:58:13',27,NULL,NULL),(13,4,'009','CADENA','ACTIVO','2019-12-19 11:31:01',0,'2020-01-31 16:58:13',27,NULL,NULL),(14,4,'010','PULSERA','ACTIVO','2019-12-19 11:31:16',0,'2020-01-31 16:58:13',27,NULL,NULL),(15,4,'011','ARETES','ACTIVO','2019-12-19 11:31:32',0,'2020-01-31 16:58:13',27,NULL,NULL),(17,5,'012','ESCLAVA','ACTIVO','2019-12-19 11:36:25',0,'2020-01-31 16:58:13',27,NULL,NULL),(18,10,'013','HP','ACTIVO','2020-01-20 15:57:14',0,'2020-01-31 16:58:13',27,NULL,NULL),(23,13,'014','ROLEXS','ACTIVO','2020-02-07 16:58:56',28,NULL,NULL,NULL,NULL),(24,8,'015','SAMSUNG','ACTIVO','2020-03-03 08:33:41',28,NULL,NULL,NULL,NULL),(25,18,'016','PICKUP','ACTIVO','2020-03-03 09:02:32',28,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `prendas_subtipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prendas_tipos`
--

DROP TABLE IF EXISTS `prendas_tipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prendas_tipos` (
  `prenda_tipo_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` char(2) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `capturar_gramaje` enum('SI','NO') NOT NULL,
  `capturar_imei_celular` enum('SI','NO') DEFAULT 'NO',
  `capturar_serie_vehiculo` enum('SI','NO') DEFAULT 'NO',
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`prenda_tipo_id`),
  UNIQUE KEY `UNIQUE_codigo` (`codigo`),
  UNIQUE KEY `UNIQUE_descripcion` (`descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prendas_tipos`
--

LOCK TABLES `prendas_tipos` WRITE;
/*!40000 ALTER TABLE `prendas_tipos` DISABLE KEYS */;
INSERT INTO `prendas_tipos` VALUES (3,'01','INMUEBLES','NO','NO','NO','ACTIVO','2019-09-22 12:35:05',0,'2020-01-31 16:57:06',27,'2019-09-22 12:35:10',0),(4,'02','ORO','SI','NO','NO','ACTIVO','2019-09-22 12:35:10',0,'2020-01-31 16:57:06',27,NULL,NULL),(5,'03','PLATA','SI','NO','NO','ACTIVO','2019-12-19 11:24:54',0,'2020-01-31 16:57:06',27,NULL,NULL),(6,'04','DIAMANTES','SI','NO','NO','ACTIVO','2019-12-19 11:25:06',0,'2020-01-31 16:57:06',27,NULL,NULL),(7,'05','MOTO','NO','NO','NO','ACTIVO','2019-12-19 11:25:19',0,'2020-01-31 16:57:06',27,NULL,NULL),(8,'06','CELULARES','NO','SI','NO','ACTIVO','2019-12-19 11:25:44',0,'2020-01-31 16:57:06',27,NULL,NULL),(9,'07','TABLETS','NO','NO','NO','ACTIVO','2019-12-19 11:25:51',0,'2020-01-31 16:57:06',27,NULL,NULL),(10,'08','LAPTOPS','NO','NO','NO','ACTIVO','2019-12-19 11:26:00',0,'2020-03-02 11:25:32',28,NULL,NULL),(11,'09','PANTALLAS','NO','NO','NO','ACTIVO','2019-12-19 11:26:07',0,'2020-01-31 16:57:47',27,NULL,NULL),(12,'10','VIDEOJUEGOS','NO','NO','NO','ACTIVO','2019-12-19 11:26:24',0,'2020-01-31 16:57:47',27,NULL,NULL),(13,'11','RELOJES','NO','NO','NO','ACTIVO','2019-12-19 11:27:35',0,'2020-01-31 16:57:58',27,NULL,NULL),(14,'12','OTRO','NO','NO','NO','ACTIVO','2020-02-07 16:02:06',28,NULL,NULL,NULL,NULL),(15,'13','OTRAS','NO','NO','NO','ACTIVO','2020-02-07 16:37:51',28,NULL,NULL,NULL,NULL),(16,'14','VARIOS','NO','NO','NO','ACTIVO','2020-02-07 16:41:55',28,NULL,NULL,NULL,NULL),(17,'15','CONSOLA','NO','NO','NO','INACTIVO','2020-03-02 11:33:35',28,NULL,NULL,'2020-03-03 10:22:43',0),(18,'16','CARRO','NO','NO','SI','ACTIVO','2020-03-02 15:28:42',28,NULL,NULL,NULL,NULL),(19,'17','ELECTRONICA CELULARES','SI','NO','NO','INACTIVO','2020-03-03 08:42:00',28,NULL,NULL,'2020-03-03 08:44:44',0),(20,'18','OTROS','NO','SI','NO','INACTIVO','2020-03-03 10:15:39',28,NULL,NULL,'2020-03-03 10:19:55',0),(21,'19','ELECTRODOMESTICO','NO','NO','NO','INACTIVO','2020-03-03 10:18:31',28,NULL,NULL,'2020-03-03 10:18:31',28),(22,'20','PRUEBAS','NO','SI','NO','ACTIVO','2020-03-13 08:35:56',28,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `prendas_tipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `procesos`
--

DROP TABLE IF EXISTS `procesos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `procesos` (
  `proceso_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `proceso_padre_id` int(11) unsigned DEFAULT NULL,
  `descripcion` varchar(50) NOT NULL,
  `nombre_menu` varchar(50) NOT NULL,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`proceso_id`),
  KEY `proceso_padre_id_procesos` (`proceso_padre_id`),
  CONSTRAINT `proceso_padre_id_procesos` FOREIGN KEY (`proceso_padre_id`) REFERENCES `procesos` (`proceso_id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procesos`
--

LOCK TABLES `procesos` WRITE;
/*!40000 ALTER TABLE `procesos` DISABLE KEYS */;
INSERT INTO `procesos` VALUES (33,NULL,'Procesos','tsmProcesosPadre','ACTIVO','2019-12-05 09:46:39',0,'2019-12-18 13:31:00',0,NULL,NULL),(36,NULL,'Catálogos','tsmCatalogos','ACTIVO','2019-12-13 10:42:43',0,'2019-12-18 13:31:31',0,NULL,NULL),(37,36,'Generales','tsmGenerales','ACTIVO','2019-12-13 10:42:43',0,'2019-12-18 13:34:48',0,NULL,NULL),(38,37,'CLIENTES','tsmClientes','ACTIVO','2019-12-13 10:42:43',0,'2020-02-04 08:38:38',0,NULL,NULL),(41,36,'Configuración','tsmConfiguracion','ACTIVO','2019-12-13 10:42:43',0,'2019-12-18 13:34:48',0,NULL,NULL),(43,37,'Ocupaciones','tsmOcupaciones','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(44,37,'Asentamientos','tsmAsentamientos','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(45,37,'Municipios','tsmMunicipios','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(46,37,'ESTADOS','tsmEstados','ACTIVO','2019-12-18 14:46:06',0,'2020-03-14 09:19:15',0,NULL,NULL),(47,37,'TIPOS DE PRENDAS','tsmTiposDePrendas','ACTIVO','2019-12-18 14:46:06',0,'2020-03-14 13:00:18',0,NULL,NULL),(48,37,'Subtipos de prendas','tsmTipoDeSub','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(49,37,'Gramajes','tsmGramajes','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(50,41,'Empresas','tsmEmpresas','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(51,41,'SUCURSALES','tsmSucursales','ACTIVO','2019-12-18 14:46:06',0,'2020-03-17 12:27:11',0,NULL,NULL),(52,41,'Tipos de identificaciones','tsmTipoDeIdentificacion','ACTIVO','2019-12-18 14:46:06',0,'2020-01-23 14:54:22',0,NULL,NULL),(53,41,'Formas de pago','tsmFormaPago','ACTIVO','2019-12-18 14:46:06',0,'2020-01-23 14:54:22',0,NULL,NULL),(54,41,'Tipos de asentamientos','tsmTiposAsentamientos','ACTIVO','2019-12-18 14:46:06',0,'2020-01-23 14:54:22',0,NULL,NULL),(55,41,'Cajas','tsmCajas','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(56,41,'Tipos de movimientos de caja','tsmTiposMovimientosCaja','ACTIVO','2019-12-18 14:46:06',0,'2020-01-23 14:54:22',0,NULL,NULL),(57,41,'Tipos de cobros','tsmTiposCobros','ACTIVO','2019-12-18 14:46:06',0,'2020-01-23 14:54:22',0,NULL,NULL),(58,41,'Parámetros del sistema','tsmParametrosDelSistema','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(59,41,'Procesos','tsmProcesos','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(60,41,'Usuarios','tsmUsuarios','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(61,33,'BOLETAS DE EMPEÑO','tsmBoletasDeEmpeno','ACTIVO','2019-12-18 14:46:06',0,'2020-03-18 11:49:49',0,NULL,NULL),(62,33,'Recibos de Liquidación y Refrendos','tsmRecibosLiqRefrendos','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(63,33,'MOVIMIENTOS DE CAJA','tsmMovimientosCaja','ACTIVO','2019-12-18 14:46:06',0,'2020-03-19 11:21:46',0,NULL,NULL),(64,33,'Adjudicación de Boletas','tsmAjudicacionBoletas','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(65,33,'Liquidación Interna de Boletas','tsmLiqInternaBoletas','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(66,NULL,'Reportes','tsmReportes','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(67,66,'Reportes Contables','tsmRepContables','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(68,66,'Relación de Cortes de Caja','tsmRepCortesCaja','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(69,66,'Relación de Movimientos de Caja','tsmRepMovCaja','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(70,66,'Relación de Recibos','tsmRepRecibos','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(71,66,'Relación de Prendas','tsmRepPrendas','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(72,66,'Relación de Boletas','tsmRepBoletas','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(73,41,'Folios','tsmFolios','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(74,41,'Folios por proceso','tsmFoliosProceso','ACTIVO','2019-12-18 14:46:06',0,NULL,NULL,NULL,NULL),(75,33,'CORTE DE CAJA','tsmCorteCaja','ACTIVO','2020-03-21 08:44:56',0,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `procesos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subprocesos`
--

DROP TABLE IF EXISTS `subprocesos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subprocesos` (
  `subproceso_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `proceso_id` int(11) unsigned NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `nombre_menu` varchar(50) NOT NULL,
  PRIMARY KEY (`subproceso_id`),
  KEY `IN01` (`proceso_id`,`subproceso_id`),
  CONSTRAINT `proceso_id_subprocesos` FOREIGN KEY (`proceso_id`) REFERENCES `procesos` (`proceso_id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subprocesos`
--

LOCK TABLES `subprocesos` WRITE;
/*!40000 ALTER TABLE `subprocesos` DISABLE KEYS */;
INSERT INTO `subprocesos` VALUES (1,43,'Nuevo','tsbNuevo'),(2,43,'Guardar','tsbGuardar'),(3,43,'Eliminar','tsbEliminar'),(4,43,'Buscar','tsbBuscar'),(5,44,'Nuevo','tsbNuevo'),(6,44,'Guardar','tsbGuardar'),(7,44,'Eliminar','tsbEliminar'),(8,44,'Buscar','tsbBuscar'),(9,45,'Nuevo','tsbNuevo'),(10,45,'Guardar','tsbGuardar'),(11,45,'Eliminar','tsbEliminar'),(12,45,'Buscar','tsbBuscar'),(13,46,'Nuevo','tsbNuevo'),(14,46,'Guardar','tsbGuardar'),(15,46,'Eliminar','tsbEliminar'),(16,46,'Buscar','tsbBuscar'),(17,47,'Nuevo','tsbNuevo'),(18,47,'Guardar','tsbGuardar'),(19,47,'Eliminar','tsbEliminar'),(20,47,'Buscar','tsbBuscar'),(21,48,'Nuevo','tsbNuevo'),(22,48,'Guardar','tsbGuardar'),(23,48,'Eliminar','tsbEliminar'),(24,48,'Buscar','tsbBuscar'),(25,49,'Nuevo','tsbNuevo'),(26,49,'Guardar','tsbGuardar'),(27,49,'Eliminar','tsbEliminar'),(28,49,'Buscar','tsbBuscar'),(29,50,'Nuevo','tsbNuevo'),(30,50,'Guardar','tsbGuardar'),(31,50,'Eliminar','tsbEliminar'),(32,50,'Buscar','tsbBuscar'),(33,51,'Nuevo','tsbNuevo'),(34,51,'Guardar','tsbGuardar'),(35,51,'Eliminar','tsbEliminar'),(36,51,'Buscar','tsbBuscar'),(37,52,'Nuevo','tsbNuevo'),(38,52,'Guardar','tsbGuardar'),(39,52,'Eliminar','tsbEliminar'),(40,52,'Buscar','tsbBuscar'),(41,53,'Nuevo','tsbNuevo'),(42,53,'Guardar','tsbGuardar'),(43,53,'Eliminar','tsbEliminar'),(44,53,'Buscar','tsbBuscar'),(45,54,'Nuevo','tsbNuevo'),(46,54,'Guardar','tsbGuardar'),(47,54,'Eliminar','tsbEliminar'),(48,54,'Buscar','tsbBuscar'),(49,55,'Nuevo','tsbNuevo'),(50,55,'Guardar','tsbGuardar'),(51,55,'Eliminar','tsbEliminar'),(52,55,'Buscar','tsbBuscar'),(53,56,'Nuevo','tsbNuevo'),(54,56,'Guardar','tsbGuardar'),(55,56,'Eliminar','tsbEliminar'),(56,56,'Buscar','tsbBuscar'),(57,57,'Nuevo','tsbNuevo'),(58,57,'Guardar','tsbGuardar'),(59,57,'Eliminar','tsbEliminar'),(60,57,'Buscar','tsbBuscar'),(61,58,'Nuevo','tsbNuevo'),(62,58,'Guardar','tsbGuardar'),(63,58,'Eliminar','tsbEliminar'),(64,58,'Buscar','tsbBuscar'),(65,59,'Nuevo','tsbNuevo'),(66,59,'Guardar','tsbGuardar'),(67,59,'Eliminar','tsbEliminar'),(68,59,'Buscar','tsbBuscar'),(69,60,'Nuevo','tsbNuevo'),(70,60,'Guardar','tsbGuardar'),(71,60,'Eliminar','tsbEliminar'),(72,60,'Buscar','tsbBuscar'),(73,61,'Nuevo','tsbNuevo'),(74,61,'Guardar','tsbGuardar'),(75,61,'Eliminar','tsbEliminar'),(76,61,'Buscar','tsbBuscar'),(77,62,'Nuevo','tsbNuevo'),(78,62,'Guardar','tsbGuardar'),(79,62,'Eliminar','tsbEliminar'),(80,62,'Buscar','tsbBuscar'),(81,63,'Nuevo','tsbNuevo'),(82,63,'Guardar','tsbGuardar'),(83,63,'Eliminar','tsbEliminar'),(84,63,'Buscar','tsbBuscar'),(85,64,'Nuevo','tsbNuevo'),(86,64,'Guardar','tsbGuardar'),(87,64,'Eliminar','tsbEliminar'),(88,64,'Buscar','tsbBuscar'),(89,65,'Nuevo','tsbNuevo'),(90,65,'Guardar','tsbGuardar'),(91,65,'Eliminar','tsbEliminar'),(92,65,'Buscar','tsbBuscar'),(93,67,'Nuevo','tsbNuevo'),(94,67,'Guardar','tsbGuardar'),(95,67,'Eliminar','tsbEliminar'),(96,67,'Buscar','tsbBuscar'),(97,68,'Nuevo','tsbNuevo'),(98,68,'Guardar','tsbGuardar'),(99,68,'Eliminar','tsbEliminar'),(100,68,'Buscar','tsbBuscar'),(101,69,'Nuevo','tsbNuevo'),(102,69,'Guardar','tsbGuardar'),(103,69,'Eliminar','tsbEliminar'),(104,69,'Buscar','tsbBuscar'),(105,70,'Nuevo','tsbNuevo'),(106,70,'Guardar','tsbGuardar'),(107,70,'Eliminar','tsbEliminar'),(108,70,'Buscar','tsbBuscar'),(109,71,'Nuevo','tsbNuevo'),(110,71,'Guardar','tsbGuardar'),(111,71,'Eliminar','tsbEliminar'),(112,71,'Buscar','tsbBuscar'),(113,72,'Nuevo','tsbNuevo'),(114,72,'Guardar','tsbGuardar'),(115,72,'Eliminar','tsbEliminar'),(116,72,'Buscar','tsbBuscar'),(117,38,'Nuevo','tsbNuevo'),(118,38,'Guardar','tsbGuardar'),(119,38,'Buscar','tsbBuscar'),(120,73,'Nuevo','tsbNuevo'),(121,73,'Guardar','tsbGuardar'),(122,73,'Eliminar','tsbEliminar'),(123,73,'Buscar','tsbBuscar'),(124,74,'Nuevo','tsbNuevo'),(125,74,'Guardar','tsbGuardar'),(126,74,'Eliminar','tsbEliminar'),(127,74,'Buscar','tsbBuscar'),(128,46,'Reactivar','tsbReactivar'),(129,47,'Reactivar','tsbReactivar'),(130,51,'Reactivar','tsbReactivar'),(131,61,'Reactivar','tsbReactivar'),(132,63,'Reactivar','tsbReactivar'),(133,75,'Nuevo','tsbNuevo'),(134,75,'Guardar','tsbGuardar'),(135,75,'Eliminar','tsbEliminar'),(136,75,'Buscar','tsbBuscar');
/*!40000 ALTER TABLE `subprocesos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursales`
--

DROP TABLE IF EXISTS `sucursales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sucursales` (
  `sucursal_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `empresa_id` tinyint(3) unsigned NOT NULL,
  `codigo` char(3) NOT NULL DEFAULT '',
  `nombre` varchar(50) NOT NULL DEFAULT '',
  `representante_legal` varchar(250) NOT NULL DEFAULT '',
  `calle` varchar(250) NOT NULL DEFAULT '',
  `numero_exterior` varchar(15) NOT NULL DEFAULT '',
  `numero_interior` varchar(15) NOT NULL DEFAULT '',
  `asentamiento_id` int(11) unsigned NOT NULL,
  `telefono_01` varchar(10) NOT NULL,
  `telefono_02` varchar(10) NOT NULL,
  `correo_electronico` varchar(50) NOT NULL,
  `inicio_operaciones` date NOT NULL,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`sucursal_id`),
  UNIQUE KEY `UNIQUE_codigo` (`codigo`),
  UNIQUE KEY `UNIQUE_nombre` (`empresa_id`,`nombre`),
  KEY `empresa_id_sucursales` (`empresa_id`),
  KEY `asentamiento_id_sucursales` (`asentamiento_id`),
  CONSTRAINT `asentamiento_id_sucursales` FOREIGN KEY (`asentamiento_id`) REFERENCES `asentamientos` (`asentamiento_id`),
  CONSTRAINT `empresa_id_sucursales` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`empresa_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursales`
--

LOCK TABLES `sucursales` WRITE;
/*!40000 ALTER TABLE `sucursales` DISABLE KEYS */;
INSERT INTO `sucursales` VALUES (1,4,'001','LOS MOCHIS','JOSE LUIS OCHA GOMEZ','LAS ROSAS BLVD.','5','',5,'6671458787','','JOSE@GMAIL.COM','2010-02-02','ACTIVO','2020-03-18 14:54:53',28,NULL,NULL,'2019-10-30 12:40:25',0),(6,4,'002','CULIACÁN','MARIA LOPEZ GOMEZ','LAZRO CARDENAS BLVD','1','',5,'6671547898','','MARIA@GMAIL.COM','2009-01-27','ACTIVO','2020-01-31 15:35:32',28,NULL,NULL,NULL,NULL),(9,4,'003','MAZATLÁN','JOSE LUIS OCHA GOMEZ','LAS ROSAS BLVD.','5','',5,'6671458787','','JOSE@GMAIL.COM','2010-02-02','ACTIVO','2020-01-31 15:35:32',28,NULL,NULL,'2019-10-30 12:40:25',0),(10,4,'004','OTRA','ASDFASFDSAD','ASDFSDA','1','',5,'5646841515','','ASDFASDF@ASDF.COM','2019-11-11','ACTIVO','2020-01-31 15:35:32',28,NULL,NULL,NULL,NULL),(11,5,'005','CULIACAN CASTILLA','JULIO','OTRA','5','',5,'6671954689','','JULIO@GMAIL.COM','1989-01-02','ACTIVO','2020-01-31 15:35:32',28,NULL,NULL,NULL,NULL),(12,4,'006','AHOME','JOSE LOPEZ','BLVD. ZONA A','5','',5,'6674546454','','JOSE@GMAIL.COM','2014-06-10','ACTIVO','2020-01-31 09:50:03',27,NULL,NULL,NULL,NULL),(13,4,'007','COSALÁ','MARIA JOSE','BLVD. JOSE MARIA','2','',5,'6674578998','','MARIA@GMAIL.COM','1964-06-10','ACTIVO','2020-01-31 10:27:32',27,NULL,NULL,NULL,NULL),(15,4,'008','BADIRAGUATO','JOSE MANUEL','BLVD. DAIZ ','2','',5,'6671854541','','JOSEMANUEL@GMAIL.COM','2020-02-04','ACTIVO','2020-03-17 12:30:11',28,'2020-03-18 09:09:07',0,NULL,NULL);
/*!40000 ALTER TABLE `sucursales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursales_cobros_tipos`
--

DROP TABLE IF EXISTS `sucursales_cobros_tipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sucursales_cobros_tipos` (
  `sucursal_id` tinyint(3) unsigned NOT NULL,
  `cobro_tipo_id` tinyint(3) unsigned NOT NULL,
  `porcentaje` decimal(4,2) NOT NULL,
  PRIMARY KEY (`sucursal_id`,`cobro_tipo_id`),
  KEY `sucursal_id_sucursales_cobros_tipos` (`sucursal_id`),
  KEY `cobro_tipo_id_sucursales_cobros_tipos` (`cobro_tipo_id`),
  CONSTRAINT `cobro_tipo_id_sucursales_cobros_tipos` FOREIGN KEY (`cobro_tipo_id`) REFERENCES `cobros_tipos` (`cobro_tipo_id`),
  CONSTRAINT `sucursal_id_sucursales_cobros_tipos` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursales` (`sucursal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursales_cobros_tipos`
--

LOCK TABLES `sucursales_cobros_tipos` WRITE;
/*!40000 ALTER TABLE `sucursales_cobros_tipos` DISABLE KEYS */;
INSERT INTO `sucursales_cobros_tipos` VALUES (1,1,3.00),(1,2,3.60),(1,3,3.60);
/*!40000 ALTER TABLE `sucursales_cobros_tipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursales_gramajes`
--

DROP TABLE IF EXISTS `sucursales_gramajes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sucursales_gramajes` (
  `sucursal_id` tinyint(3) unsigned NOT NULL,
  `gramaje_id` tinyint(3) unsigned NOT NULL,
  `precio_tope` decimal(8,2) NOT NULL,
  PRIMARY KEY (`sucursal_id`,`gramaje_id`),
  KEY `sucursal_id_sucursales_gramajes` (`sucursal_id`),
  KEY `gramaje_id_sucursales_gramajes` (`gramaje_id`),
  CONSTRAINT `gramaje_id_sucursales_gramajes` FOREIGN KEY (`gramaje_id`) REFERENCES `gramajes` (`gramaje_id`),
  CONSTRAINT `sucursal_id_sucursales_gramajes` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursales` (`sucursal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursales_gramajes`
--

LOCK TABLES `sucursales_gramajes` WRITE;
/*!40000 ALTER TABLE `sucursales_gramajes` DISABLE KEYS */;
INSERT INTO `sucursales_gramajes` VALUES (1,1,200.00),(1,2,400.00),(1,4,300.00),(6,1,100.00),(6,2,200.00),(6,4,300.00),(10,1,8.00),(10,2,20.00),(13,1,5.00),(13,2,6.00),(13,4,7.00),(15,1,1.00),(15,2,2.00),(15,4,3.00);
/*!40000 ALTER TABLE `sucursales_gramajes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursales_impresoras`
--

DROP TABLE IF EXISTS `sucursales_impresoras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sucursales_impresoras` (
  `sucursal_id` tinyint(3) unsigned NOT NULL,
  `impresion_ticket_boleta` enum('SI','NO') NOT NULL,
  `impresora_ticket_boleta` varchar(250) DEFAULT NULL,
  `impresora_boleta` varchar(250) DEFAULT NULL,
  `impresora_recibos` varchar(250) DEFAULT NULL,
  `impresion_notas_venta` enum('SI','NO') NOT NULL,
  `impresora_notas_venta` varchar(250) DEFAULT NULL,
  `impresora_ticket_divisas` varchar(250) DEFAULT NULL,
  `impresora_informes` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`sucursal_id`),
  KEY `sucursales_impresoras` (`sucursal_id`),
  CONSTRAINT `sucursales_impresoras` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursales` (`sucursal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursales_impresoras`
--

LOCK TABLES `sucursales_impresoras` WRITE;
/*!40000 ALTER TABLE `sucursales_impresoras` DISABLE KEYS */;
/*!40000 ALTER TABLE `sucursales_impresoras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursales_parametros`
--

DROP TABLE IF EXISTS `sucursales_parametros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sucursales_parametros` (
  `sucursal_id` tinyint(3) unsigned NOT NULL,
  `dias_cobro_minimo` tinyint(3) DEFAULT NULL,
  `dias_cobro_cuota_diaria` tinyint(3) DEFAULT NULL,
  `dias_cobrar_cuota_daria_especial` tinyint(3) DEFAULT NULL,
  `dias_liquidar_boletas` tinyint(3) DEFAULT NULL,
  `dias_refrendar_boletas` tinyint(3) DEFAULT NULL,
  `dias_remate_prendas` tinyint(3) DEFAULT NULL,
  `avaluo_calculo` decimal(11,2) DEFAULT NULL,
  `avaluo_impresion` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`sucursal_id`),
  KEY `sucursal_id_parametros` (`sucursal_id`),
  CONSTRAINT `sucursal_id_parametros` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursales` (`sucursal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursales_parametros`
--

LOCK TABLES `sucursales_parametros` WRITE;
/*!40000 ALTER TABLE `sucursales_parametros` DISABLE KEYS */;
INSERT INTO `sucursales_parametros` VALUES (1,1,0,0,2,5,0,30.00,70.00),(15,0,0,0,0,0,0,0.00,0.00);
/*!40000 ALTER TABLE `sucursales_parametros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursales_plazos`
--

DROP TABLE IF EXISTS `sucursales_plazos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sucursales_plazos` (
  `sucursal_id` tinyint(3) unsigned NOT NULL,
  `prenda_tipo_id` tinyint(3) unsigned NOT NULL,
  `plazo` decimal(4,2) NOT NULL,
  PRIMARY KEY (`sucursal_id`,`prenda_tipo_id`),
  KEY `sucursal_id_plazos` (`sucursal_id`),
  KEY `prenda_tipo_id_plazos` (`prenda_tipo_id`),
  CONSTRAINT `prenda_tipo_id_plazos` FOREIGN KEY (`prenda_tipo_id`) REFERENCES `prendas_tipos` (`prenda_tipo_id`),
  CONSTRAINT `sucursal_id_plazos` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursales` (`sucursal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursales_plazos`
--

LOCK TABLES `sucursales_plazos` WRITE;
/*!40000 ALTER TABLE `sucursales_plazos` DISABLE KEYS */;
INSERT INTO `sucursales_plazos` VALUES (1,3,4.00),(1,4,3.00),(1,5,7.89),(1,6,7.00),(1,7,4.00),(1,8,2.00),(1,9,7.00),(1,10,5.00),(1,11,6.00),(1,18,5.00),(6,3,4.00),(6,4,2.00),(15,3,1.00),(15,4,2.00),(15,5,3.00),(15,6,4.00),(15,7,5.00),(15,8,6.00),(15,9,7.00),(15,10,8.00),(15,11,9.00),(15,12,10.00),(15,13,11.00);
/*!40000 ALTER TABLE `sucursales_plazos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursales_promociones`
--

DROP TABLE IF EXISTS `sucursales_promociones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sucursales_promociones` (
  `sucursal_promocion_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sucursal_id` tinyint(3) unsigned NOT NULL,
  `dia` enum('LUNES','MARTES','MIERCOLES','JUEVES','VIERNES','SABADO','DOMINGO') NOT NULL DEFAULT 'LUNES',
  `dias_minimo` tinyint(3) NOT NULL,
  `porcentaje` decimal(4,2) NOT NULL,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  PRIMARY KEY (`sucursal_promocion_id`),
  KEY `sucursal_id_promociones` (`sucursal_id`),
  CONSTRAINT `sucursal_id_promociones` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursales` (`sucursal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursales_promociones`
--

LOCK TABLES `sucursales_promociones` WRITE;
/*!40000 ALTER TABLE `sucursales_promociones` DISABLE KEYS */;
INSERT INTO `sucursales_promociones` VALUES (26,6,'SABADO',0,30.00,'ACTIVO'),(37,1,'JUEVES',5,40.00,'ACTIVO'),(38,1,'MARTES',30,10.00,'ACTIVO');
/*!40000 ALTER TABLE `sucursales_promociones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursales_promociones_detalles`
--

DROP TABLE IF EXISTS `sucursales_promociones_detalles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sucursales_promociones_detalles` (
  `sucursal_promocion_id` int(11) unsigned NOT NULL,
  `renglon` tinyint(3) unsigned NOT NULL,
  `prenda_tipo_id` tinyint(3) unsigned NOT NULL,
  KEY `prenda_tipo_id_promociones` (`prenda_tipo_id`),
  KEY `sucursal_promocion_id_promociones_idx` (`sucursal_promocion_id`),
  CONSTRAINT `prenda_tipo_id_promociones` FOREIGN KEY (`prenda_tipo_id`) REFERENCES `prendas_tipos` (`prenda_tipo_id`),
  CONSTRAINT `sucursal_promcion_id_promociones` FOREIGN KEY (`sucursal_promocion_id`) REFERENCES `sucursales_promociones` (`sucursal_promocion_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursales_promociones_detalles`
--

LOCK TABLES `sucursales_promociones_detalles` WRITE;
/*!40000 ALTER TABLE `sucursales_promociones_detalles` DISABLE KEYS */;
INSERT INTO `sucursales_promociones_detalles` VALUES (26,1,12),(26,2,13),(37,1,18),(38,1,4);
/*!40000 ALTER TABLE `sucursales_promociones_detalles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `usuario_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria, autoincremento',
  `nombre` varchar(100) NOT NULL DEFAULT '',
  `apellido_paterno` varchar(100) NOT NULL DEFAULT '',
  `apellido_materno` varchar(100) NOT NULL DEFAULT '',
  `telefono_01` varchar(10) NOT NULL,
  `telefono_02` varchar(10) NOT NULL,
  `correo_electronico` varchar(50) NOT NULL DEFAULT '',
  `usuario` varchar(30) NOT NULL,
  `contrasena` varchar(255) NOT NULL DEFAULT '',
  `intentos` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `autorizar` enum('SI','NO') DEFAULT 'NO',
  `modificar_movimientos` enum('SI','NO') DEFAULT 'NO',
  `estatus` enum('ACTIVO','INACTIVO','SUSPENDIDO') NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion` int(11) unsigned DEFAULT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` int(11) unsigned DEFAULT NULL,
  `fecha_eliminacion` datetime DEFAULT NULL,
  `usuario_eliminacion` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`usuario_id`),
  UNIQUE KEY `UNIQUE` (`usuario`),
  KEY `usuario_creacion_usuarios` (`usuario_creacion`),
  KEY `usuario_actualizacion_usuarios` (`usuario_actualizacion`),
  KEY `usuario_eliminacion_usuarios` (`usuario_eliminacion`),
  CONSTRAINT `usuario_actualizacion_usuarios` FOREIGN KEY (`usuario_actualizacion`) REFERENCES `usuarios` (`usuario_id`),
  CONSTRAINT `usuario_creacion_usuarios` FOREIGN KEY (`usuario_creacion`) REFERENCES `usuarios` (`usuario_id`),
  CONSTRAINT `usuario_eliminacion_usuarios` FOREIGN KEY (`usuario_eliminacion`) REFERENCES `usuarios` (`usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (9,'Luis','Gomez','Diaz','667195456','','luis@gmail.com','daizluis','daizluis',0,'NO','NO','ACTIVO','2019-12-04 12:55:24',9,'2019-12-17 11:21:25',NULL,NULL,NULL),(10,'Luis','Lopez','Gomez','667145789','','Luis_admin@gmail.com','Luis_admin','123',0,'SI','NO','ACTIVO','2019-12-11 13:57:00',9,'2019-12-17 11:16:13',NULL,NULL,NULL),(11,'Mario','Marquez','Vaca','677545644','','Mario_lopez','Mario_lopez','Mario_lopez',0,'SI','NO','ACTIVO','2019-12-13 09:16:16',9,'2019-12-13 09:23:05',NULL,NULL,NULL),(12,'Luis ','Jose','Maria','6671456878','','Jose','Jose','Jose',0,'NO','NO','ACTIVO','2019-12-16 14:56:44',9,NULL,NULL,NULL,NULL),(13,'Jose','Maria','Woods','6674458787','','woods','woods','woods',0,'NO','NO','ACTIVO','2019-12-16 15:16:22',9,NULL,NULL,NULL,NULL),(16,'jjj','jjjj','kkkkk','6545451121','','Jose jjj','Jose jjj','Jose jjj',0,'SI','NO','ACTIVO','2019-12-16 15:19:44',9,NULL,NULL,NULL,NULL),(18,'fasdfasdf','asdfsdaf','asdfs','4545646541','','sadfadsf','sadfadsf','sadfadsf',0,'SI','NO','ACTIVO','2019-12-16 15:25:05',9,NULL,NULL,NULL,NULL),(20,'alksfjd','akslfdj','asfdas','6671848485','','nose 4','nose 4','nose 4',0,'SI','NO','ACTIVO','2019-12-16 15:32:58',9,NULL,NULL,NULL,NULL),(21,'Oscar','Valadez','','6667154564','','Lopez@gmail.com','Lopez5','Lopez5',0,'NO','NO','ACTIVO','2019-12-16 15:52:33',9,'2019-12-17 11:12:31',NULL,NULL,NULL),(22,'Mary Angel','Castro','','6671457897','','mary@gmail.com','Mary','123',0,'SI','NO','ACTIVO','2019-12-17 11:30:29',9,'2019-12-17 12:15:13',NULL,NULL,NULL),(26,'Jeni','Lopez','','6674545456','','jeni@gmail.com','jeni_12','123',0,'SI','NO','INACTIVO','2019-12-17 12:01:28',9,'2019-12-17 12:18:52',NULL,'2019-12-17 12:29:48',9),(27,'LUIS ALONSO','LOPEZ','','6667154545','','LUIS@GMAIL.COM','LUIS_15','123456',0,'NO','NO','ACTIVO','2019-12-18 10:59:02',NULL,'2020-03-21 10:14:30',27,NULL,NULL),(28,'NUBIA ADILENE','VALDEZ','BATALLA','6671458798','','','ADMIN','NUBIA',0,'SI','SI','ACTIVO','2019-12-19 12:07:50',NULL,'2020-03-21 09:56:23',28,NULL,NULL),(29,'Omar','Gomez','','6671457897','','omar@gmail.com','omar_admin','123',0,'SI','NO','ACTIVO','2019-12-30 08:48:50',NULL,'2019-12-30 08:55:05',NULL,NULL,NULL);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_cajas`
--

DROP TABLE IF EXISTS `usuarios_cajas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_cajas` (
  `usuario_id` int(11) unsigned NOT NULL,
  `sucursal_id` tinyint(3) unsigned NOT NULL,
  `caja_id` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`usuario_id`,`sucursal_id`,`caja_id`),
  KEY `sucursal_id_usuarios_cajas` (`sucursal_id`),
  KEY `caja_id_usuarios_cajas` (`caja_id`),
  CONSTRAINT `caja_id_usuarios_cajas` FOREIGN KEY (`caja_id`) REFERENCES `cajas` (`caja_id`),
  CONSTRAINT `sucursal_id_usuarios_cajas` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursales` (`sucursal_id`),
  CONSTRAINT `usuario_id_usuarios_cajas` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_cajas`
--

LOCK TABLES `usuarios_cajas` WRITE;
/*!40000 ALTER TABLE `usuarios_cajas` DISABLE KEYS */;
INSERT INTO `usuarios_cajas` VALUES (9,1,11),(21,1,9),(22,1,10),(26,1,9),(27,1,9),(27,1,10),(27,1,12),(28,1,9),(28,1,10),(28,1,11),(28,1,12),(13,6,10),(22,6,9),(26,6,10),(28,6,9),(28,6,10),(28,6,11),(22,9,11),(26,9,11),(26,10,9),(26,10,10),(26,10,11),(18,11,10),(22,11,9),(22,11,10),(22,11,11);
/*!40000 ALTER TABLE `usuarios_cajas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_huellas`
--

DROP TABLE IF EXISTS `usuarios_huellas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_huellas` (
  `usuario_id` int(11) unsigned NOT NULL,
  `huella` mediumblob NOT NULL,
  PRIMARY KEY (`usuario_id`),
  CONSTRAINT `usuario_id` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_huellas`
--

LOCK TABLES `usuarios_huellas` WRITE;
/*!40000 ALTER TABLE `usuarios_huellas` DISABLE KEYS */;
INSERT INTO `usuarios_huellas` VALUES (27,_binary '\0�\�*\�s\\�A7	�qp�U�ꍍ񑢜4Ks#�y��E8ѣ.xԖ�U\�\�v\"X\'%��}Sz\�!�L�\�x\�\�و�dG:l�\�\�\�X�fO�\�\\\�ʞN\�Z\�Ss�G\n\ZBūyǟ7�SmOzM�i\�ե\�W\�Jĩ\�\�bMӌ��le�@E�#ϑފ]u���:,\r\�48\�(/���}q��M�\�\�qF�5J\\G\�0\�D�\�55����\�\�1\�\"�n/;\�Y\�yT\�\�\�gE\�\�o��\n\���?\�&VLŮ\n\n.\�	�\�M�k{޶92�i\�L�w\��&��ၴY\�|\�ۓ�5�d�\�\nA/\�ɂr<X�\�,��JL�\�~\�;�#)�\�d�g\�(\�v�_q�2\�\�Ϟ\�\�}`S�R�*(�￥J^茛\�\�S��sJe\���m�`3�5oW\0o\0��\�*\�s\\�A7	�q��U�\�G\�)\�����W\�+\�>r#�#]\�I)���v�\�Bٿ܍��i�yK�!\�`ZP�\�X\n054���\���kf`%\�I���z\�2�oW\�_\�����\�\�.C��qL�.�?w[�fn-y�!���[Ą*CƊ&�`\�\�0j�\�E\�;�\�+�AkE����w^/�u�\��(��t|e\�X\�\�Mc=�_1;�c��\Z��\�L\�%��Gɓ\�U]w\�\�s�:\Z\�}\n�$\�kq{�+�xs�?�_���\�C\��\�h�]a\�\�fp�\�\r�P5�\��P����\�lJ�w}[\�^�O��Q���\�\��\���pQ\�ى�Z9��B\�\�H�s�bԗq��|_\n*$�\�\0׍����j��Y		�`*k\�!\�t\��\�3\0E}OP0ٸ?��z�\�\�Q~���o\0��\�*\�s\\�A7	�qp�U���\�\�qLR�_L\�\�U=jf\�����\\3��gp�}��\�B�EuĜb.Ʒ�-:�~��\03=�\"Pe\����l�j\�H�2D���E\��\�W\�T*D9�gJ�-C8�����\�>���z\�U��\�1��lo\��Cecl�j�w\���?1�9�yT��.\�:L\�ԡX��TU8l;\�\���(��\r+?���`�\�;\�y{?b\Z�\r�\�f^;?Z\�/W��c��BW\�.�D@5�7}�m�#�<ˑK�4��k�!\"�\�H _�O�]k\�ԋN��&_eR�Lc3iV��)ٽT�\�j�\���c���J�0�$�0}\�\�\n\"\"v�v\�����\�`�FG�PJW@\�\�/�_��BR\n{9 Tl6�%����\�9I§i;Fo\0\�~\�*\�s\\�A7	�q��U�%�uP=ם?uj�\�	�״\�aCR�\�4k-H;->@\�1\����O\��\�\�\�\�nv|T��\�\�iN��5t|��قT\�\\�\�#\�5A\�Y-g�$�J�\�\�*\0�\�;x�<�b\�\�I\���$$r�S]�����(\�S\��/�(c\�\�\�g�4d�s\�\�i�޹\�0\�\�{�H\�	oy��r��ҙ�{B�\�(�\�ֵ \��ҧ\�b�m=D���t6\�o�\�<o\�6y\�\�\�!�;�\\��k~d���*<6)\�x�v\�$f<\�\�\"\�gy\�Ę�>�Yf\�\�%���.\�D~+�\\�\��͚%�אI����\�q\�x\�����N���z�\�s��\�I|\�\�l`<m!<�[��\�~��O�$Q\�?o\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0'),(28,_binary '\0�\�*\�s\\�A7	�q�\�U��W˾\�y>H�\�֒\��}{(-RC�v\���	�[� �YJ}Ñ/��Cth�X��\�ZJ9�\�V���\r�\���\�ezg֊x�BJ�\rSS�\'��H&\�\�0��\�F�\�\�\"�{9\��j�\�\".];p���ҕ\�-�\�ZxXcb��\�i�\�;\�v���\�O�Z\'(EϬ�Δ;\r�\�}E2\�l�4\�,�X!�������4�Iǩ�K^Ns\�\�LJ�\�5\�}}\�h\Z=l�lQ~�:�KZ�c\�\���\�\�yE\�F��\� Cou��A�؅ug&.\�\�\�\�+~EB\��cȆ�L\�g�;\�V\�\�u/-D \�xj@\�.�l!�3\�_6�\r\�$�ȃD`[\�K\�)���\�\�t\�,q�\��[�\�W(\�/Qۆ`d䔗^`\"%\�L�pI�K\�o\0�p\�*\�s\\�A7	�q��U�M\�r��ih9p�\�s�� \r�b���K��Fhs\0n\�\�He�UqĎdy�d�7ș��\�Q]@C���\�?-�\�F\�`A!U\�a�p|\�\��\�ӏf_�*\rCf\�;�Ł��W\�%��\�!9Tم�\�\'�\0\�A\�/zϒ���W xCbA�T!\�\�>P�\�s�a!�4�$�FH�>\�V��L+\"�<g|R���4\Z�8�	��n)e�R�1\0g���\�\�\�\�إY	K\�eQ)�P��JG.\�\�D�\Z��\�g~\�\�[�jT��\"\"��i׬�\�CȱڷCh����\�\�A:-�}T�>	@�\��\\\�7&fv�{�&�U2]���\��ө�+�\�c\�}�\n�T�H~���s?\��o\0�x\�*\�s\\�A7	�qp�U���FЅ��SF�L\�|�YW�K��J]����\'��0�ǽ^J{X g�z\�w�-t�yy�\�1xƴ���}ʰ��T~\�\�\�Y\��\�\�� CB\ZgF4\�=��˘d=Y3z�b�\n��\�싚�HE�y�\�Oޝ��5\�\�(3�6\'���\�\'\���v���\�\�DPtK�\�\�s���Af�(O*�K��6�.\�<Dë�\�!\��\�-�8�\�\�{�{Jy��AC\�S�\�}\�7\�,\�3���\���.\�t^1\�\�)��\�t���\�\�\�\�4�젯,(}{5\���N{�f\�%_��\�f^$䊊�(9\�ñ\�s@L�ŁC諰g��\�0�\�01\0\�¡�&��&\�&\��B\'1�^4О\�]=��w?�Յēo\0\�}\�*\�s\\�A7	�q��U�\�XY�CV��z�f��\n\�\�&* Y^\�\�}mv\�\n\�$9�9.\"\�,QLgs9�}����>h�����蹻��\��\�r\�\�\�\�r�\�	�\�+ƚ]e\�\�{LK\�i�,W:�\�g����tE\�8�\r\�Jh<\n�p䪸�9\'\�;�>t�\�[�\�)�bDq�w/�\�\�R�d\�K6{M�tYן�\"~�-\��\�ݢ\�/X�]\�\�%\�u�c���\��\�-\�v���U��KT�`\�F\�w�8ۊ�.�\�6x��\�\�\"����b\�h�\�wZ\��\�\�Z�\�S$|��^p\�7l%v�\�@Ϝ#Ӌ(Ӳ\�\�\�\�\�01WG�\�a1n��\�\�W$�Vژ \�n��J\�l�D�\���J\��ԏڻ�\�S.d0�\�i�稯pĐ�\�GDo����\0\0\0\0\0\0\0\0\0\0\0\0����\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0����\0\0\0\0\0\0\0\0����\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0'),(29,_binary '\0�+\�*\�s\\�A7	�q��U�\�βZx^LZ\�\�\��j�\��Ӌ�\�M�rw�B2�<\�d\�BS\�`�Fr�\0lCqT\�i�\�ݚ��\�7��	\"\�K�4%����\�\��-��̮�\�ZhQ�\�L\"}Ip�ԛC(�B0��0�7۪���\�\�>cy�6�\�e�\r�J/_��)\�[R��\�\���r&g�{\���u�4\"B�n�|�iH\�z\�\Z��\Z�[3ϒ10\�\� pC��?7u\�%��D�@蜖]ꡈ\�\�\�&�\�-c ��%;]\�I40\�4�׸R�9\�.<���7\�ie�5ަ�j<3>�\�I�D���o\0�a\�*\�s\\�A7	�q�\�U��\�\Zl&&ل0ՇM�9�N0R\�����%bv	��=g\����8\�QC�\�b�;��y�\��3���\�嚅��\�J�Y\�\'CZ\�\�ԷT�\�V\�L�դ\'|��_\�\�\�=�꬟x\� @�)\�#+\�\\U��w�I�`\"$\�\�\\^s����hP�Y\��}�V@\�ۢ\Z�pj��B�5H��\�G����4�Vș+R9Et�����\�}��\�\�==\�JX۵��˜\�fd����f\n\��\��+)�\\\�z*\�ɪ\�\�x�\�]\�[\0��1��\�`����}#=EY�-�\�\�\�)�S \�\�\�j�.��R��F������@�\�B���\�gʊBI\�\�\"p��k�~Zj\�\�o\0�J\�*\�s\\�A7	�qp\�U�J�Rq�brH/r-$\��fk���5�\�F��ҤB͐�OF�A\�#Z��Z\���\�P�Â�\\)+\�%o	�r��\��\�X^ߡ�6\'���~\�QvĖH\�\n\�1\�Ȅ}\�\�\�\��<+VO2\�\�6�)\0J�\��\�!2*\�n7L\Zĵ��:0I#\�w{��f\�Ut�\n��6q-�i\�f\��`FXe�F)��\�u ���y>\�)\�\�\�12�z����)q-�\�\\�\�\�\Z,A\�[/��]\�\�9�#�ZĪ�Xj���\�\�/��\�J�fk����]\��Y�BH�W�\�A\�[�F��\�\�\�~t��-y�YE\�Q�5�)�H\�#zo\0\�`\�*\�s\\�A7	�q0�U��W\�G�����s���9_�z\���R�\�<~�mM�;y\�ӭ�\�9�#vR�y\\�e\�/�r\���i/��gm\�+�L\�Eu\�`�m���֍�X�3j�����Ba[\�\�CU�J�\���_Ze�8\'⭀�<8�Df\�\�XCXW\�6Uy\�;⚎ӌ�ّ1�^\�0��\n���2�\�[�Um\�U\�ߐ\�w %�e2\�\�)�\�Q���b!\�W\���P+\n�y�\�!L,`X_+\�\��R8\�\�O�$\�\�k\�c-\r�\�e�s�	XGL1�\��N��$�\��\�j� \Z�q�s\�\�v�E`&\�\�^Z�����\�\�\',.7Bc0?+1c��Bo�L��|&�Iႏ���o\��\�\��]T_����\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\�\�X_\�\�X_�\�X_�\�X_\0b_\0b_b_b_b_b_Hb_Hb_\�X_\�X_$\�X_$\�X_0\�X_0\�X_<\�X_<\�X_H\�X_H\�X_T\�X_T\�X_`\�X_`\�X_p\�X_p\�X_lb_lb_|\�X_|\�X_�\�X_�\�X_�\�X_�\�X_�\�X_�\�X_xb_xb_hb_hb_|b_|b_�b_�b_�b_�b_�b_');
/*!40000 ALTER TABLE `usuarios_huellas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_permisos`
--

DROP TABLE IF EXISTS `usuarios_permisos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_permisos` (
  `usuario_id` int(11) unsigned NOT NULL,
  `sucursal_id` tinyint(3) unsigned NOT NULL,
  `permisos` mediumtext,
  PRIMARY KEY (`usuario_id`,`sucursal_id`),
  KEY `sucursal_id_usuarios_permisos` (`sucursal_id`),
  CONSTRAINT `sucursal_id_usuarios_permisos` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursales` (`sucursal_id`),
  CONSTRAINT `usuario_id_usuarios_permisos` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_permisos`
--

LOCK TABLES `usuarios_permisos` WRITE;
/*!40000 ALTER TABLE `usuarios_permisos` DISABLE KEYS */;
INSERT INTO `usuarios_permisos` VALUES (27,1,'76 | 77 | 78 | 79 | 80 | 81 | 82 | 83 | 84 | 85 | 87 | 88 | 89 | 90 | 91 | 92 | 133 | 136 | 117 | 119 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 15 | 16 | 128 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31 | 32 | 33 | 34 | 35 | 36 | 37 | 38 | 39 | 40 | 41 | 42 | 43 | 44 | 45 | 46 | 47 | 48 | 49 | 50 | 51 | 52 | 53 | 54 | 55 | 56 | 57 | 58 | 59 | 60 | 61 | 62 | 63 | 64 | 65 | 66 | 67 | 68 | 69 | 70 | 71 | 72 | 120 | 121 | 122 | 123 | 124 | 125 | 126 | 127 | 93 | 94 | 95 | 96 | 97 | 98 | 99 | 100 | 101 | 102 | 103 | 104 | 105 | 106 | 107 | 108 | 109 | 110 | 111 | 112 | 113 | 114 | 115 | 116 '),(27,6,'74 | 75 | 76 | 77 | 78 | 79 | 80 | 81 | 82 | 83 | 84 | 85 | 86 | 87 | 88 | 89 | 90 | 91 | 92 | 29 | 30 | 31 | 32 | 33 | 34 | 35 | 36 | 37 | 38 | 39 | 40 | 41 | 42 | 43 | 44 | 45 | 46 | 47 | 48 | 49 | 50 | 51 | 52 | 53 | 54 | 55 | 56 | 57 | 58 | 59 | 60 | 61 | 62 | 63 | 64 | 65 | 66 | 67 | 68 | 69 | 70 | 71 | 72 | 93 | 94 | 95 | 96 | 97 | 98 | 99 | 100 | 101 | 102 | 103 | 104 | 105 | 106 | 107 | 108 | 109 | 110 | 111 | 112 | 113 | 114 | 115 | 116 | 117 | 1 | 5 | 9 | 13 | 20 | 24 | 28 '),(28,1,'76 | 131 | 77 | 78 | 79 | 80 | 81 | 82 | 83 | 84 | 132 | 85 | 86 | 87 | 88 | 89 | 90 | 91 | 92 | 133 | 134 | 135 | 136 | 117 | 118 | 119 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 128 | 17 | 18 | 19 | 20 | 129 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31 | 32 | 33 | 34 | 35 | 36 | 130 | 37 | 38 | 39 | 40 | 41 | 42 | 43 | 44 | 45 | 46 | 47 | 48 | 49 | 50 | 51 | 52 | 53 | 54 | 55 | 56 | 57 | 58 | 59 | 60 | 61 | 62 | 63 | 64 | 65 | 66 | 67 | 68 | 69 | 70 | 71 | 72 | 120 | 121 | 122 | 123 | 124 | 125 | 126 | 127 | 93 | 94 | 95 | 96 | 97 | 98 | 99 | 100 | 101 | 102 | 103 | 104 | 105 | 106 | 107 | 108 | 109 | 110 | 111 | 112 | 113 | 114 | 115 | 116 | 73 | 74 | 75 '),(28,6,'73 | 77 | 78 | 79 | 80 | 81 | 82 | 83 | 84 | 85 | 86 | 87 | 88 | 89 | 90 | 91 | 92 | 117 | 119 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31 | 32 | 33 | 34 | 35 | 36 | 37 | 38 | 39 | 40 | 41 | 42 | 43 | 44 | 45 | 46 | 47 | 48 | 49 | 50 | 51 | 52 | 53 | 54 | 55 | 56 | 57 | 58 | 59 | 60 | 61 | 62 | 63 | 64 | 65 | 66 | 67 | 68 | 69 | 70 | 71 | 72 | 120 | 121 | 122 | 123 | 124 | 125 | 126 | 127 '),(29,1,'69 | 70 | 71 | 72 ');
/*!40000 ALTER TABLE `usuarios_permisos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-24  8:11:53
