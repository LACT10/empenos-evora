﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmEmpresas : Form
    {
        public Controller.clsCtrlEmpresas ctrlEmpresa;
        public Core.clsCoreView clsView;

        public int _intUlitmoCodigo;
        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }

        public List<Properties_Class.clsEmpresa> lsDeshacerEmpresa;

        public frmEmpresas()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();
            this.ctrlEmpresa = new Controller.clsCtrlEmpresas();
            this.ctrlEmpresa.Paginacion();
            this.lsDeshacerEmpresa = new List<Properties_Class.clsEmpresa>();
            this.lsDeshacerEmpresa.Add(this.ctrlEmpresa.empresa);
        }
 

        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlEmpresa.empresa.intId == 0) return;
            if (this.clsView.confirmacionEliminar("Empresas"))
            {
                this.ctrlEmpresa.Eliminar();
                if (this.ctrlEmpresa.bolResultado)
                {
                    this.ctrlEmpresa.Paginacion();
                    this.limpiar_textbox();
                    this.clsView.mensajeExitoDB("empresa", "elimino");
                }
            }
        }     

        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlEmpresa.bolResutladoPaginacion)
            {
                txtRfc.Text = this.lsDeshacerEmpresa[0].strRFC.ToString();
                txtRazonSocial.Text = this.lsDeshacerEmpresa[0].strRazonSocial.ToString();
                txtNombreComercial.Text = this.lsDeshacerEmpresa[0].strNombreComercial.ToString();
                txtCURP.Text = this.lsDeshacerEmpresa[0].strCURP.ToString();
                this.clsView.principal.epMensaje.Clear();
                txtRazonSocial.Focus();
            }
        }

        private void tsbBuscar_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();

        }

        private void BtnBusqueda_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();
        }

        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlEmpresa.PrimeroPaginacion();
            if (!this.ctrlEmpresa.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlEmpresa.SiguentePaginacion();
            if (!this.ctrlEmpresa.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlEmpresa.AtrasPaginacion();
            if (!this.ctrlEmpresa.bolResultado) return;
            this.llenar_formulario();
        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlEmpresa.UltimoPaginacion();
            if (!this.ctrlEmpresa.bolResultado) return;
            this.llenar_formulario();
        }
        private void TsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();
            this.ctrlEmpresa.empresa.limpiarProps();
            
        }
        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {
            /*txtId.Text = "";
            txtDescripcion.Text = "";*/
        }

        private void TxtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);

        }

        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            txtNombreComercialAnt.Text = "";
            this.clsView.principal.epMensaje.Clear();
            txtRazonSocial.Focus();
        }
        private void txtNombreComercial_Leave(object sender, EventArgs e)
        {
            validarComercialRFC(txtNombreComercial);
        }

        private void txtRfc_Leave(object sender, EventArgs e)
        {
            validarComercialRFC(txtRfc);
        }

        private bool validacion()
        {

            bool bolValidacion = true;
            string strRfc = txtRfc.Text.TrimStart();
            string strRazonSocial = txtRazonSocial.Text.TrimStart();
            string strNombreComercial = txtNombreComercial.Text.TrimStart();
            string strCURP = txtCURP.Text.TrimStart();


            if (strRfc == "" || strRfc.Length != 13)
            {
                txtRfc.Focus();
                this.clsView.principal.epMensaje.SetError(txtRfc, this.clsView.mensajeErrorValiForm2("El campo RFC es obligatorio y debe ser de 13 caracteres, favor de ingresar un RFC"));                
                bolValidacion = false;
            }

            if (strCURP == "" || strCURP.Length != 18)
            {
                txtCURP.Focus();
                this.clsView.principal.epMensaje.SetError(txtCURP, this.clsView.mensajeErrorValiForm2("El campo CURP es obligatorio y debe ser de 18 caracteres, favor de ingresar un CURP"));                
                bolValidacion = false;
            }
            if (strRazonSocial == "")
            {
                txtRazonSocial.Focus();
                this.clsView.principal.epMensaje.SetError(txtRazonSocial, this.clsView.mensajeErrorValiForm2("El campo razon socail es obligatorio , favor de ingresar un razon socail"));                
                bolValidacion = false;
            }

            if (strNombreComercial == "")
            {
                txtRazonSocial.Focus();
                this.clsView.principal.epMensaje.SetError(txtNombreComercial, this.clsView.mensajeErrorValiForm2("El campo nombre comercial es obligatorio , favor de ingresar un nombre comercial"));                
                bolValidacion = false;
            }
            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();

            if (validacion())
            {

                this.ctrlEmpresa.empresa.strRFC = txtRfc.Text.ToString();
                this.ctrlEmpresa.empresa.strRazonSocial = txtRazonSocial.Text.ToString();
                this.ctrlEmpresa.empresa.strNombreComercial = txtNombreComercial.Text.ToString();
                this.ctrlEmpresa.empresa.strCURP = txtCURP.Text.ToString();
                this.ctrlEmpresa.empresa.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlEmpresa.Guardar();

                if (this.ctrlEmpresa.mdlEmpresas.bolResultado)
                {
                    this.limpiar_textbox();
                    this.ctrlEmpresa.Paginacion();
                    this.clsView.mensajeExitoDB("empresa", "guardó");

                }
            }
        }

        public void mostraBusqueda()
        {
            if (this.ctrlEmpresa.bolResutladoPaginacion)
            {
                this.ctrlEmpresa.CargarFrmBusqueda();

                if (this.ctrlEmpresa.bolResultadoBusqueda)
                {
                    this.llenar_formulario();
                    txtRazonSocial.Focus();
                }
            }
            
        }

        public void llenar_formulario()
        {
            if (this.ctrlEmpresa.empresa.strRFC.Length == 0) return;
            this.clsView.principal.epMensaje.Clear();
            txtRfc.Text = this.ctrlEmpresa.empresa.strRFC;
            txtRazonSocial.Text = this.ctrlEmpresa.empresa.strRazonSocial;
            txtNombreComercial.Text = this.ctrlEmpresa.empresa.strNombreComercial;
            txtNombreComercialAnt.Text = this.ctrlEmpresa.empresa.strNombreComercial;
            txtCURP.Text = this.ctrlEmpresa.empresa.strCURP;
        }

        public void agregarDeshacer()
        {
            this.lsDeshacerEmpresa.RemoveAt(0);
            this.lsDeshacerEmpresa.Add(this.ctrlEmpresa.empresa);
        }

        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void FrmEmpresas_Load(object sender, EventArgs e)
        {


            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);
        }


        public void validarComercialRFC(TextBox textbox) 
        {
            if (txtNombreComercial.Text != txtNombreComercialAnt.Text && txtRfc.Text != "")
            {
                this.ctrlEmpresa.ValidarComercialRFC(txtNombreComercial.Text, txtRfc.Text);
                if (this.ctrlEmpresa.bolResultado)
                {
                    this.clsView.principal.epMensaje.SetError(textbox, this.clsView.mensajeErrorValiForm2("El RFC debe ser único en el nombre comercial, favor de verificar"));
                    textbox.Text = "";
                    textbox.Focus();
                }
            }
        }

    }
}
