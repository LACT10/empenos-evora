﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmCorteCaja : Form
    {        
          
        public Core.clsCoreView clsView = new Core.clsCoreView();
        public Controller.clsCtrlCorteCaja ctrlCorteCaja;
        


        public bool bolNuevo = false;
                
        public frmCorteCaja()
        {
            InitializeComponent();

            this.ctrlCorteCaja = new Controller.clsCtrlCorteCaja();
        }

        private void frmCorteCaja_Load(object sender, EventArgs e)
        {
          
            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                }/*,
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }*/
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);

            this.ctrlCorteCaja.objDataSet = this.clsView.login.objCajas;
            this.ctrlCorteCaja.bolResultado = true;
            //El booleando es el resultado si obtiene valores de la base de datos. Se asignar a otro booleando para controlar la el secuncia de codigo
            this.ctrlCorteCaja.bolResutladoPaginacion = true;

            this.limpiar_textbox();


            /*Buscar el utlimo corte realizado*/
            this.ctrlCorteCaja.UltimoCorteCierre(this.clsView.login.intSucursalId);
            if (this.ctrlCorteCaja.mdlCorteCaja.bolResultado) 
            {
                txtUltimoCorte.Text = this.ctrlCorteCaja.corteCaja.dtFechaUlimoCorteCierre.ToString("dd/MM/yyyy");
                dtpFecha.MaxDate = this.ctrlCorteCaja.corteCaja.dtFechaUlimoCorteCierre.AddDays(1);

            }
            /*Regresar las cajas que tiene el usuario*/
            //this.regresarCajas();

            if (this.clsView.bolPermisosGuardar)
            {
                btnFooterGuardar.Enabled = true;
                tsbGuardar.Enabled = true;
            }
            else
            {
                btnFooterGuardar.Enabled = false;
                tsbGuardar.Enabled = false;
            }

            if (this.clsView.login.usuario.validarMovimientos() && this.clsView.bolPermisosEliminar)
            {
                tsbEliminar.Enabled = true;
            }
            else
            {
                tsbEliminar.Enabled = false;
            }

        }


        private void tsbBuscar_Click(object sender, EventArgs e)
        {
          

        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            /*f (this.ctrlBoletaEmpeno.boletasEmpeno.intId > 0)
            {
                llenar_formulario();
            }
            else
            {
                txtBoleta.Enabled = true;
                this.limpiar_textbox();
            }
            txtBoleta.Focus();*/
        }

        private void btnPrimeroPag_Click(object sender, EventArgs e)
        {            
            /*this.ctrlCorteCaja.PrimeroPaginacion();            
            if (!this.ctrlCorteCaja.bolResutladoPaginacion) return;
            this.paginacionCajas();   
            this.llenar_formulario();*/
        }

        private void btnAdelante_Click(object sender, EventArgs e)
        {
            
            /*this.ctrlCorteCaja.SiguentePaginacion();
            if (!this.ctrlCorteCaja.bolResutladoPaginacion) return;
            this.paginacionCajas();
            this.llenar_formulario();*/

        }

        private void btnAtras_Click(object sender, EventArgs e)
        {            
            /*this.ctrlCorteCaja.AtrasPaginacion();
            if (!this.ctrlCorteCaja.bolResutladoPaginacion) return;
            this.paginacionCajas();
            this.llenar_formulario();*/

        }

        private void btnUlitmoPag_Click(object sender, EventArgs e)
        {            
            /*this.ctrlCorteCaja.UltimoPaginacion();            
            if (!this.ctrlCorteCaja.bolResutladoPaginacion) return;
            this.paginacionCajas();            
            this.llenar_formulario();*/

        }

        private void tsbNuevo_Click(object sender, EventArgs e)
        {

            this.limpiar_textbox();
            
            cmbTipo.SelectedIndex = 0;
            /*dtpFecha.MaxDate = DateTime.Today;
            dtpFecha.Text = DateTime.Today.ToString();*/
            lblDescEstatus.Visible = true;
            lblEstatus.Visible = true;
            lblEstatus.Text = "NUEVO";
            /*Regresar las cajas que tiene el usuario*/
            this.regresarCajas();


        }

        private void tsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();

        }
        private void DateTimePicker4_ValueChanged(object sender, EventArgs e)
        {

        }

     
       
        private void dtpFecha_Leave(object sender, EventArgs e)
        {
            
        }


        public bool validacion_cantidad(bool bolValidacion) 
        {
            bool bolValidacionCantidad = false;

            string strBilletes1000 = txtBilletes1000.Text;
            string strBilletes500 = txtBilletes500.Text;
            string strBilletes200 = txtBilletes200.Text;
            string strBilletes100 = txtBilletes100.Text;
            string strBilletes50 = txtBilletes50.Text;
            string strBilletes20 = txtBilletes20.Text;
            string strMonedas10 = txtMonedas10.Text;
            string strMonedas5 = txtMonedas5.Text;
            string strMonedas2 = txtMonedas2.Text;
            string strMonedas1 = txtMonedas1.Text;
            string strMonedas50C = txtMonedas50c.Text;
            string strMonedas20C = txtMonedas20c.Text;
            string strMonedas10C = txtMonedas10c.Text;
            string strMonedas5C = txtMonedas5c.Text;

            if (strBilletes1000 != "") { bolValidacionCantidad = true; }
            if (strBilletes500 != "") { bolValidacionCantidad = true; }
            if (strBilletes200 != "") { bolValidacionCantidad = true; }
            if (strBilletes100 != "") { bolValidacionCantidad = true; }
            if (strBilletes50 != "") { bolValidacionCantidad = true; }
            if (strBilletes20 != "") { bolValidacionCantidad = true; }
            if (strMonedas10 != "") { bolValidacionCantidad = true; }
            if (strMonedas5 != "") { bolValidacionCantidad = true; }
            if (strMonedas2 != "") { bolValidacionCantidad = true; }
            if (strMonedas1 != "") { bolValidacionCantidad = true; }
            if (strMonedas50C != "") { bolValidacionCantidad = true; }
            if (strMonedas20C != "") { bolValidacionCantidad = true; }
            if (strMonedas10C != "") { bolValidacionCantidad = true; }
            if (strMonedas5C != "") { bolValidacionCantidad = true; }

            if (!bolValidacionCantidad) 
            {
                this.clsView.principal.epMensaje.SetError(cmbTipo, this.clsView.mensajeErrorValiForm2("Favor de ingresar por lo menos un billete o moneda"));
            }

            return bolValidacion;
        }
  

        public bool validacion()
        {
            this.clsView.principal.epMensaje.Clear();
            bool bolValidacion = true;

            int intTipo = cmbTipo.SelectedIndex;

            if (intTipo == 0)
            {
                cmbTipo.Focus();
                this.clsView.principal.epMensaje.SetError(cmbTipo, this.clsView.mensajeErrorValiForm2("El campo tipo es obligatorio, favor de selecionar un tipo "));
                bolValidacion = false;
            }

            bolValidacion = this.validacion_cantidad(bolValidacion);

            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();
            if (this.validacion())
            {

                this.ctrlCorteCaja.corteCaja.dtFechaCorte = dtpFecha.Value;
                this.ctrlCorteCaja.corteCaja.strTipo = cmbTipo.SelectedItem.ToString();
                this.ctrlCorteCaja.corteCaja.intBilletes1000 = int.Parse((txtBilletes1000.Text == "")? "0": txtBilletes1000.Text);
                this.ctrlCorteCaja.corteCaja.intBilletes500 = int.Parse((txtBilletes500.Text == "") ? "0" : txtBilletes500.Text);
                this.ctrlCorteCaja.corteCaja.intBilletes200 = int.Parse((txtBilletes200.Text == "") ? "0" : txtBilletes200.Text);
                this.ctrlCorteCaja.corteCaja.intBilletes100 = int.Parse((txtBilletes100.Text == "") ? "0" : txtBilletes100.Text);
                this.ctrlCorteCaja.corteCaja.intBilletes50 = int.Parse((txtBilletes50.Text == "") ? "0" : txtBilletes50.Text);
                this.ctrlCorteCaja.corteCaja.intBilletes20 = int.Parse((txtBilletes20.Text == "") ? "0" : txtBilletes20.Text);
                this.ctrlCorteCaja.corteCaja.intMonedas10 = int.Parse((txtMonedas10.Text == "") ? "0" : txtMonedas10.Text);
                this.ctrlCorteCaja.corteCaja.intMonedas5 = int.Parse((txtMonedas5.Text == "") ? "0" : txtMonedas5.Text);
                this.ctrlCorteCaja.corteCaja.intMonedas2 = int.Parse((txtMonedas2.Text == "") ? "0" : txtMonedas2.Text);
                this.ctrlCorteCaja.corteCaja.intMonedas1 = int.Parse((txtMonedas1.Text == "") ? "0" : txtMonedas1.Text);
                this.ctrlCorteCaja.corteCaja.intMonedas50c = int.Parse((txtMonedas50c.Text == "") ? "0" : txtMonedas50c.Text);
                this.ctrlCorteCaja.corteCaja.intMonedas20c = int.Parse((txtMonedas20c.Text == "") ? "0" : txtMonedas20c.Text);
                this.ctrlCorteCaja.corteCaja.intMonedas10c = int.Parse((txtMonedas10c.Text == "") ? "0" : txtMonedas10c.Text);
                this.ctrlCorteCaja.corteCaja.intMonedas5c = int.Parse((txtMonedas5c.Text == "") ? "0" : txtMonedas5c.Text);

                this.ctrlCorteCaja.corteCaja.intSucursal = this.clsView.login.intSucursalId;
                this.ctrlCorteCaja.corteCaja.intUsuario = this.clsView.login.usuario.intId;

                this.ctrlCorteCaja.corteCaja.strQueryBoletaEmpeno = " UPDATE boletas_empeno " +
                                                                      " SET caja_corte_id = @id " +
                                                                      " WHERE boleta_empeno_id IN( {0} ); ";
                this.ctrlCorteCaja.corteCaja.strQueryBoletaEmpenoRefrendos = " UPDATE boletas_empeno " +
                                                                      " SET caja_corte_id = @id " +
                                                                      " WHERE boleta_empeno_id IN( {0} ); ";
                this.ctrlCorteCaja.corteCaja.strQueryBoletaEmpenoSinPago = " UPDATE boletas_empeno " +
                                                                      " SET caja_corte_id = @id " +
                                                                      " WHERE boleta_empeno_id IN( {0} ); ";
                this.ctrlCorteCaja.corteCaja.strQueryPago = " UPDATE pagos " +
                                                                      " SET caja_corte_id = @id " +
                                                                      " WHERE pago_id IN( {0} ); ";
                this.ctrlCorteCaja.corteCaja.strQueryMovimientoEntradadCaja = " UPDATE movimientos_caja " +
                                                                      " SET caja_corte_id = @id " +
                                                                      " WHERE movimiento_caja_id IN( {0} ); ";
                this.ctrlCorteCaja.corteCaja.strQueryMovimientoSalidadCaja = " UPDATE movimientos_caja " +
                                                                      " SET caja_corte_id = @id " +
                                                                      " WHERE movimiento_caja_id IN( {0} ); ";


                string strQueryCajaCorteDetallesValuesTemplate = "(@id, {0}, '{1}', {2}, '{3}')||";
                string strQueryCajaCorteDetallesValues = "";
                int intRenglon = 0;

                if (this.ctrlCorteCaja.corteCaja.strListaBoletasId != "")
                {                    
                    this.ctrlCorteCaja.corteCaja.strQueryCajaCorteDetallesBoletasId = "INSERT cajas_corte_detalles (caja_corte_id, renglon, tipo_referencia, referencia_id, tipo) VALUES {0}";
                    foreach (string row in this.ctrlCorteCaja.corteCaja.strListaBoletasId.Split(','))
                    {
                        strQueryCajaCorteDetallesValues += String.Format(strQueryCajaCorteDetallesValuesTemplate, ++intRenglon, "BOLETA", row, "INGRESO");
                    }

                    int intStartIndex = strQueryCajaCorteDetallesValues.LastIndexOf("||");
                    strQueryCajaCorteDetallesValues = strQueryCajaCorteDetallesValues.Remove(intStartIndex).Insert(intStartIndex, ";").Replace("||", ",");
                    this.ctrlCorteCaja.corteCaja.strQueryCajaCorteDetallesBoletasId = String.Format(this.ctrlCorteCaja.corteCaja.strQueryCajaCorteDetallesBoletasId, strQueryCajaCorteDetallesValues);
                }

                if (this.ctrlCorteCaja.corteCaja.strListaBoletasRefrendosId != "") 
                {                    
                    strQueryCajaCorteDetallesValues = "";
                    this.ctrlCorteCaja.corteCaja.strQueryCajaCorteDetallesBoletasRefrendosId = "INSERT cajas_corte_detalles (caja_corte_id, renglon, tipo_referencia, referencia_id, tipo) VALUES {0}";
                    foreach (string row in this.ctrlCorteCaja.corteCaja.strListaBoletasRefrendosId.Split(',')) 
                    {
                        strQueryCajaCorteDetallesValues += String.Format(strQueryCajaCorteDetallesValuesTemplate, ++intRenglon, "BOLETA", row, "INGRESO");
                    }

                    int intStartIndex = strQueryCajaCorteDetallesValues.LastIndexOf("||");
                    strQueryCajaCorteDetallesValues = strQueryCajaCorteDetallesValues.Remove(intStartIndex).Insert(intStartIndex, ";").Replace("||", ",");
                    this.ctrlCorteCaja.corteCaja.strQueryCajaCorteDetallesBoletasRefrendosId = String.Format(this.ctrlCorteCaja.corteCaja.strQueryCajaCorteDetallesBoletasRefrendosId, strQueryCajaCorteDetallesValues);
                }

                if (this.ctrlCorteCaja.corteCaja.strListaBoletasSinPagoId != "") 
                {                    
                    strQueryCajaCorteDetallesValues = "";
                    this.ctrlCorteCaja.corteCaja.strQueryCajaCorteDetallesBoletasSinPagoId = "INSERT cajas_corte_detalles (caja_corte_id, renglon, tipo_referencia, referencia_id, tipo) VALUES {0}";                    
                    foreach (string row in this.ctrlCorteCaja.corteCaja.strListaBoletasSinPagoId.Split(','))
                    {
                        strQueryCajaCorteDetallesValues += String.Format(strQueryCajaCorteDetallesValuesTemplate, ++intRenglon, "BOLETA", row, "EGRESO");
                    }

                    int intStartIndex = strQueryCajaCorteDetallesValues.LastIndexOf("||");
                    strQueryCajaCorteDetallesValues = strQueryCajaCorteDetallesValues.Remove(intStartIndex).Insert(intStartIndex, ";").Replace("||", ",");
                    this.ctrlCorteCaja.corteCaja.strQueryCajaCorteDetallesBoletasSinPagoId = String.Format(this.ctrlCorteCaja.corteCaja.strQueryCajaCorteDetallesBoletasSinPagoId, strQueryCajaCorteDetallesValues);

                }

                if (this.ctrlCorteCaja.corteCaja.strListaPagoId != "")
                {                    
                    strQueryCajaCorteDetallesValues = "";
                    this.ctrlCorteCaja.corteCaja.strQueryCajaCorteDetallesPagoId = "INSERT cajas_corte_detalles (caja_corte_id, renglon, tipo_referencia, referencia_id, tipo) VALUES {0}";
                    foreach (string row in this.ctrlCorteCaja.corteCaja.strListaPagoId.Split(','))
                    {
                        strQueryCajaCorteDetallesValues += String.Format(strQueryCajaCorteDetallesValuesTemplate, ++intRenglon, "PAGO", row, "INGRESO");
                    }

                    int intStartIndex = strQueryCajaCorteDetallesValues.LastIndexOf("||");
                    strQueryCajaCorteDetallesValues = strQueryCajaCorteDetallesValues.Remove(intStartIndex).Insert(intStartIndex, ";").Replace("||", ",");
                    this.ctrlCorteCaja.corteCaja.strQueryCajaCorteDetallesPagoId = String.Format(this.ctrlCorteCaja.corteCaja.strQueryCajaCorteDetallesPagoId, strQueryCajaCorteDetallesValues);

                }

                if (this.ctrlCorteCaja.corteCaja.strListaMovimientoEntradaId != "")
                {                    
                    strQueryCajaCorteDetallesValues = "";
                    this.ctrlCorteCaja.corteCaja.strQueryCajaCorteDetallesMovimientoEntradadId = "INSERT cajas_corte_detalles (caja_corte_id, renglon, tipo_referencia, referencia_id, tipo) VALUES {0}";
                    foreach (string row in this.ctrlCorteCaja.corteCaja.strListaMovimientoEntradaId.Split(','))
                    {
                        strQueryCajaCorteDetallesValues += String.Format(strQueryCajaCorteDetallesValuesTemplate, ++intRenglon, "MOVIMIENTO CAJA", row, "INGRESO");
                    }

                    int intStartIndex = strQueryCajaCorteDetallesValues.LastIndexOf("||");
                    strQueryCajaCorteDetallesValues = strQueryCajaCorteDetallesValues.Remove(intStartIndex).Insert(intStartIndex, ";").Replace("||", ",");
                    this.ctrlCorteCaja.corteCaja.strQueryCajaCorteDetallesMovimientoEntradadId = String.Format(this.ctrlCorteCaja.corteCaja.strQueryCajaCorteDetallesMovimientoEntradadId, strQueryCajaCorteDetallesValues);

                }

                if (this.ctrlCorteCaja.corteCaja.strListaMovimientoSalidadId != "")
                {                    
                    strQueryCajaCorteDetallesValues = "";
                    this.ctrlCorteCaja.corteCaja.strQueryCajaCorteDetallesMovimientoSalidadId = "INSERT cajas_corte_detalles (caja_corte_id, renglon, tipo_referencia, referencia_id, tipo) VALUES {0}";
                    foreach (string row in this.ctrlCorteCaja.corteCaja.strListaMovimientoSalidadId.Split(','))
                    {
                        strQueryCajaCorteDetallesValues += String.Format(strQueryCajaCorteDetallesValuesTemplate, ++intRenglon, "MOVIMIENTO CAJA", row, "EGRESO");
                    }

                    int intStartIndex = strQueryCajaCorteDetallesValues.LastIndexOf("||");
                    strQueryCajaCorteDetallesValues = strQueryCajaCorteDetallesValues.Remove(intStartIndex).Insert(intStartIndex, ";").Replace("||", ",");
                    this.ctrlCorteCaja.corteCaja.strQueryCajaCorteDetallesMovimientoSalidadId = String.Format(this.ctrlCorteCaja.corteCaja.strQueryCajaCorteDetallesMovimientoSalidadId, strQueryCajaCorteDetallesValues);

                }


                this.ctrlCorteCaja.corteCaja.strQueryBoletaEmpeno = String.Format(this.ctrlCorteCaja.corteCaja.strQueryBoletaEmpeno, this.ctrlCorteCaja.corteCaja.strListaBoletasId);
                this.ctrlCorteCaja.corteCaja.strQueryBoletaEmpenoRefrendos = String.Format(this.ctrlCorteCaja.corteCaja.strQueryBoletaEmpenoRefrendos, this.ctrlCorteCaja.corteCaja.strListaBoletasRefrendosId);
                this.ctrlCorteCaja.corteCaja.strQueryBoletaEmpenoSinPago = String.Format(this.ctrlCorteCaja.corteCaja.strQueryBoletaEmpenoSinPago, this.ctrlCorteCaja.corteCaja.strListaBoletasSinPagoId);
                this.ctrlCorteCaja.corteCaja.strQueryPago = String.Format(this.ctrlCorteCaja.corteCaja.strQueryPago, this.ctrlCorteCaja.corteCaja.strListaPagoId);
                this.ctrlCorteCaja.corteCaja.strQueryMovimientoEntradadCaja = String.Format(this.ctrlCorteCaja.corteCaja.strQueryMovimientoEntradadCaja, this.ctrlCorteCaja.corteCaja.strListaMovimientoEntradaId);
                this.ctrlCorteCaja.corteCaja.strQueryMovimientoSalidadCaja = String.Format(this.ctrlCorteCaja.corteCaja.strQueryMovimientoSalidadCaja, this.ctrlCorteCaja.corteCaja.strListaMovimientoSalidadId);

                this.ctrlCorteCaja.Guardar();

                if (this.ctrlCorteCaja.mdlCorteCaja.bolResultado) 
                {
                    //Hacer un llamado a la funcion para obtener los datos del registro(modificado)
                    this.regresarRegistro("guardó");
                }

            }
        }

        private void txtCodigoCliente_KeyPress(object sender, KeyPressEventArgs e)
        {

            this.clsView.soloNumVali(e);

        }
        private void txtPrestamo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumValiDec(sender, e);
        }

        private void txtCantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }


        private void txtCodigoPrendaTipo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtCodigoEmpleado_KeyPress(object sender, KeyPressEventArgs e)
        {

            this.clsView.soloNumVali(e);

        }

        private void txtIMEI_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtCodigoSubTipoPrenda_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

      

        private void limpiar_textbox()
        {
            cmbTipo.SelectedIndex = 0;            
            this.clsView.principal.epMensaje.Clear();                        

            txtBilletes1000.Text = "";
            txtBilletes500.Text = "";
            txtBilletes200.Text = "";
            txtBilletes100.Text = "";
            txtBilletes50.Text = "";
            txtBilletes20.Text = "";
            txtMonedas10.Text = "";
            txtMonedas5.Text = "";
            txtMonedas2.Text = "";
            txtMonedas1.Text = "";

            txtMonedas50c.Text = "";
            txtMonedas20c.Text = "";
            txtMonedas10c.Text = "";
            txtMonedas5c.Text = "";

            txtTotalImporteFisico.Text = "";
            txtDiferencia.Text = "";



        }


        private void btnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

       
        private void txtCodigoIdentificacion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

   
      
        private void btnBusqueda_Click(object sender, EventArgs e)
        {
            //mostraBusqueda();

        }

        public void buscarCaja(int intCajaId) 
        {
            
            this.ctrlCorteCaja.BuscarCaja(this.clsView.login.intSucursalId, intCajaId, dtpFecha.Value);
            if (this.ctrlCorteCaja.mdlCorteCaja.bolResultado) 
            {
                llenar_formulario();
            }
        }

        public void buscarCajaIntegradora() 
        {
            this.ctrlCorteCaja.BuscarCajaIntegradora(this.clsView.login.intSucursalId, dtpFecha.Value);
            if (this.ctrlCorteCaja.mdlCorteCaja.bolResultado)
            {
                llenar_formulario();
            }
        }

        public void llenar_formulario()
        {

            this.limpiar_textbox();

            lblCorteCaja.Text = "Corte de la caja " + this.ctrlCorteCaja.corteCaja.caja.strDescripcion.Replace("CAJA ", "");

            txtCapital.Text = this.clsView.convertMoneda(this.ctrlCorteCaja.corteCaja.decCapital.ToString());
            txtInteres.Text = this.clsView.convertMoneda(this.ctrlCorteCaja.corteCaja.decInteres.ToString());
            txtAlmacenaje.Text = this.clsView.convertMoneda(this.ctrlCorteCaja.corteCaja.decAlmacenaje.ToString());
            txtManejo.Text = this.clsView.convertMoneda(this.ctrlCorteCaja.corteCaja.decManejo.ToString());
            txtBonificacion.Text = this.clsView.convertMoneda(this.ctrlCorteCaja.corteCaja.decBonificacinones.ToString());
            txtTotalLiqBoletas.Text = this.clsView.convertMoneda(this.ctrlCorteCaja.corteCaja.decLiqBoletas.ToString());
            txtEntradasCaja.Text = this.clsView.convertMoneda(this.ctrlCorteCaja.corteCaja.decEntradasCaja.ToString());
            txtSalidasCaja.Text = this.clsView.convertMoneda(this.ctrlCorteCaja.corteCaja.decSalidadCaja.ToString());
            txtTotalIngresos.Text = this.clsView.convertMoneda(this.ctrlCorteCaja.corteCaja.decTotalIngresos.ToString());
            txtBoletasEmpenadas.Text = this.clsView.convertMoneda(this.ctrlCorteCaja.corteCaja.decBoletasEmpenadas.ToString());
            txtIngresos.Text = this.clsView.convertMoneda(this.ctrlCorteCaja.corteCaja.decTotalIngresos.ToString());
            txtEgresos.Text = this.clsView.convertMoneda(this.ctrlCorteCaja.corteCaja.decTotalEgresos.ToString());
            txtTotalEngresos.Text = this.clsView.convertMoneda(this.ctrlCorteCaja.corteCaja.decTotalEgresos.ToString());
            txtSaldoInicial.Text = this.clsView.convertMoneda(this.ctrlCorteCaja.corteCaja.decSaldoInicial.ToString());
            txtSaldoFinal.Text = this.clsView.convertMoneda(this.ctrlCorteCaja.corteCaja.decSaldoFinal.ToString());

            if (this.ctrlCorteCaja.corteCaja.caja.strDescripcion == this.clsView.login.strCajaIntregradora)
            {

                this.textbox_cantidades(true);

            }
            else 
            {
                this.textbox_cantidades(false);
            }
        }

      
        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void btnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        

        private void tsbEliminar_Click(object sender, EventArgs e)
        {
           /* if (this.ctrlBoletaEmpeno.boletasEmpeno.intId > 0 && this.ctrlBoletaEmpeno.boletasEmpeno.strEstatus == this.ctrlBoletaEmpeno.boletasEmpeno.dicEstatus["ACTIVO"] && this.clsView.login.usuario.validarMovimientos())
            {
                if (this.clsView.confirmacionAccion("Motivo de cancelación", "¿Desea cancelar este registro?"))
                {
                    frmMotivosCancelacion frmMotivos = new frmMotivosCancelacion();
                    frmMotivos.strTipo = "G";
                    frmMotivos.ShowDialog();
                    if (frmMotivos.bolResultado)
                    {
                        this.ctrlBoletaEmpeno.boletasEmpeno.strMotivosCancelacion = frmMotivos.strMotivo;
                        this.ctrlBoletaEmpeno.boletasEmpeno.dtFecha = DateTime.Now;
                        this.ctrlBoletaEmpeno.boletasEmpeno.intUsuario = this.clsView.login.usuario.intId;


                        this.ctrlBoletaEmpeno.Cancelar();
                        if (this.ctrlBoletaEmpeno.bolResultado)
                        {
                            //Hacer un llamado a la funcion para obtener los datos del registro(modificado)
                            this.regresarRegistro("eliminó");

                        }
                    }
                }
            }*/


        }

        private void btnVerCancelacion_Click(object sender, EventArgs e)
        {
            /*frmMotivosCancelacion frmMotivos = new frmMotivosCancelacion();
            frmMotivos.strTipo = "V";
            frmMotivos.strMotivo = this.ctrlBoletaEmpeno.boletasEmpeno.strMotivosCancelacion + " \r\n\r\n\r\n Usuario cancelación: " + this.ctrlBoletaEmpeno.boletasEmpeno.strUsuarioEliminacion + "\r\n  Fecha: " + this.ctrlBoletaEmpeno.boletasEmpeno.strFechaEliminacion;
            frmMotivos.ShowDialog();*/
        }

        private void btnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /*Metodo que se ultilza para regresar las cajas que tiene el usuario*/
        public void regresarCajas() 
        {
            bool bolCajaIntredradora = false;
            int intCajaId = 0;
            foreach (DataRow row in this.clsView.login.objCajas.Tables[0].Rows)
            {
                if (row["descripcion"].ToString() == this.clsView.login.strCajaIntregradora)
                {
                    this.ctrlCorteCaja.corteCaja.caja.strDescripcion = this.clsView.login.strCajaIntregradora;
                    lblCorteCaja.Text = "Corte de la caja " + this.clsView.login.strCajaIntregradora.Replace("CAJA ", "");
                    bolCajaIntredradora = true;
                    intCajaId = int.Parse(row["caja_id"].ToString());
                   this.textbox_cantidades(true);
                    break;
                }
            }

            if (!bolCajaIntredradora)
            {

                lblCorteCaja.Text = "Corte de la caja " + this.clsView.login.objCajas.Tables[0].Rows[0]["descripcion"].ToString().Replace("CAJA ", "");
                gbCantidadBilletes.Enabled = true; ;
                intCajaId = int.Parse(this.clsView.login.objCajas.Tables[0].Rows[0]["caja_id"].ToString());
                this.textbox_cantidades(false);
                this.buscarCaja(intCajaId);
            }
            else 
            {
                this.buscarCajaIntegradora();
            }

            


            this.ctrlCorteCaja.intCountIndex = 0;
            this.ctrlCorteCaja.bolPrimer = true;
        }

        public void paginacionCajas() 
        {
            bool bolCajaIntredradora = false;
            int intCajaId = 0;

            if (this.ctrlCorteCaja.corteCaja.caja.strDescripcion == this.clsView.login.strCajaIntregradora)
            {
                
                bolCajaIntredradora = true;                
                this.textbox_cantidades(true);                
            }


            if (!bolCajaIntredradora)
            {                
                gbCantidadBilletes.Enabled = true; ;
                intCajaId = this.ctrlCorteCaja.corteCaja.caja.intId;
                this.textbox_cantidades(false);
                this.buscarCaja(intCajaId);
            }
            else
            {
                this.buscarCajaIntegradora();
            }
        }

        public void textbox_cantidades(bool bolEstatus) 
        {
            txtBilletes1000.ReadOnly = bolEstatus;
            txtBilletes500.ReadOnly = bolEstatus;
            txtBilletes200.ReadOnly = bolEstatus;
            txtBilletes100.ReadOnly = bolEstatus;
            txtBilletes50.ReadOnly = bolEstatus;
            txtBilletes20.ReadOnly = bolEstatus;
            txtMonedas10.ReadOnly = bolEstatus;
            txtMonedas1.ReadOnly = bolEstatus;
            txtMonedas50c.ReadOnly = bolEstatus;
            txtMonedas20c.ReadOnly = bolEstatus;
            txtMonedas10c.ReadOnly = bolEstatus;
            txtMonedas5c.ReadOnly = bolEstatus;


        }


        public void regresarRegistro(string stMsj)
        {
            this.limpiar_textbox();            
            this.clsView.mensajeExitoDB(this.Text.ToLower(), stMsj);

            //this.ctrlCorteCaja.BuscarCorteCaja(this.ctrlCorteCaja.corteCaja.intId);
            this.llenar_formulario();            
        }


        public void calcularImporteFisico() 
        {
            decimal dec1000 = decimal.Parse((txtBilletes1000.Text == "") ? "0" : txtBilletes1000.Text);
            decimal dec500 = decimal.Parse((txtBilletes500.Text == "") ? "0" : txtBilletes500.Text);
            decimal dec200 = decimal.Parse((txtBilletes200.Text == "") ? "0" : txtBilletes200.Text);
            decimal dec100 = decimal.Parse((txtBilletes100.Text == "") ? "0" : txtBilletes100.Text);
            decimal dec50 = decimal.Parse((txtBilletes50.Text == "") ? "0" : txtBilletes50.Text);
            decimal dec20 = decimal.Parse((txtBilletes20.Text == "") ? "0" : txtBilletes20.Text);
            decimal decM20 = decimal.Parse((txtMonedas10.Text == "") ? "0" : txtMonedas10.Text);
            decimal decM5 = decimal.Parse((txtMonedas5.Text == "") ? "0" : txtMonedas5.Text);
            decimal decM2 = decimal.Parse((txtMonedas2.Text == "") ? "0" : txtMonedas2.Text);
            decimal decM1 = decimal.Parse((txtMonedas1.Text == "") ? "0" : txtMonedas1.Text);

            decimal decM50C = decimal.Parse((txtMonedas50c.Text == "") ? "0" : txtMonedas50c.Text);
            decimal decM20C = decimal.Parse((txtMonedas20c.Text == "") ? "0" : txtMonedas20c.Text);
            decimal decM10C = decimal.Parse((txtMonedas10c.Text == "") ? "0" : txtMonedas10c.Text);
            decimal decM5C = decimal.Parse((txtMonedas5c.Text == "") ? "0" : txtMonedas5c.Text);


            decimal decTotal1000 = 1000 * dec1000;
            decimal decTotal500 = 500 * dec500;
            decimal decTotal200 = 200 * dec200;
            decimal decTotal100 = 100 * dec100;
            decimal decTotal50 = 50 * dec50;
            decimal decTotal20 = 20 * dec20;

            decimal decTotalM10 = 10 * decM20;
            decimal decTotalM5 = 5 * decM5;
            decimal decTotalM2 = 2 * decM2;
            decimal decTotalM1 = 1 * decM1;

            decimal decTotalM50C = (50 * decM50C) / 100;
            decimal decTotalM20C = (20 * decM20C) / 100;
            decimal decTotalM10C = (10 * decM10C) / 100;
            decimal decTotalM5C = (5 * decM5C) / 100;

            decimal decTotalImporteFisico = decTotal1000 + decTotal500 + decTotal200 + decTotal100 + decTotal50 + decTotal20 + decTotalM10 + decTotalM5 + decTotalM2 + decTotalM1 + decTotalM50C + decTotalM20C + decTotalM10C + decTotalM5C;
            txtTotalImporteFisico.Text = this.clsView.convertMoneda(decTotalImporteFisico.ToString());
            txtDiferencia.Text = this.clsView.convertMoneda((this.ctrlCorteCaja.corteCaja.decSaldoFinal - decTotalImporteFisico).ToString());



        }

        private void txtBilletes1000_Leave(object sender, EventArgs e)
        {
            this.calcularImporteFisico();
        }

        private void txtBilletes500_Leave(object sender, EventArgs e)
        {
            this.calcularImporteFisico();
        }

        private void txtBilletes200_Leave(object sender, EventArgs e)
        {
            this.calcularImporteFisico();
        }

        private void txtBilletes100_Leave(object sender, EventArgs e)
        {
            this.calcularImporteFisico();
        }

        private void txtBilletes50_Leave(object sender, EventArgs e)
        {
            this.calcularImporteFisico();
        }

        private void txtBilletes20_Leave(object sender, EventArgs e)
        {
            this.calcularImporteFisico();
        }

        private void txtMonedas10_Leave(object sender, EventArgs e)
        {
            this.calcularImporteFisico();
        }

        private void txtMonedas5_Leave(object sender, EventArgs e)
        {
            this.calcularImporteFisico();
        }

        private void txtMonedas2_Leave(object sender, EventArgs e)
        {
            this.calcularImporteFisico();
        }

        private void txtMonedas1_Leave(object sender, EventArgs e)
        {
            this.calcularImporteFisico();
        }

        private void txtMonedas50c_Leave(object sender, EventArgs e)
        {
            this.calcularImporteFisico();
        }

        private void txtMonedas20c_Leave(object sender, EventArgs e)
        {
            this.calcularImporteFisico();
        }

        private void txtMonedas10c_Leave(object sender, EventArgs e)
        {
            this.calcularImporteFisico();
        }

        private void txtMonedas5c_Leave(object sender, EventArgs e)
        {
            this.calcularImporteFisico();
        }

        private void txtBilletes1000_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtBilletes200_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtBilletes50_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtMonedas10_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtMonedas2_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtMonedas50c_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtMonedas10c_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtBilletes500_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtBilletes100_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtBilletes20_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtMonedas5_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtMonedas1_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtMonedas20c_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtMonedas5c_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void btnFooterPrimeroPag_Click(object sender, EventArgs e)
        {
            this.ctrlCorteCaja.PrimeroPaginacion();
            if (!this.ctrlCorteCaja.bolResutladoPaginacion) return;
            this.paginacionCajas();
            //this.buscarCaja(this.ctrlCorteCaja.corteCaja.caja.intId);
            this.llenar_formulario();
        }

        private void btnFooterAtrasPag_Click(object sender, EventArgs e)
        {
            this.ctrlCorteCaja.AtrasPaginacion();
            if (!this.ctrlCorteCaja.bolResutladoPaginacion) return;
            this.paginacionCajas();
            //this.buscarCaja(this.ctrlCorteCaja.corteCaja.caja.intId);
            this.llenar_formulario();
        }

        private void btnFooterAdelantePag_Click(object sender, EventArgs e)
        {
            this.ctrlCorteCaja.SiguentePaginacion();
            if (!this.ctrlCorteCaja.bolResutladoPaginacion) return;
            this.paginacionCajas();
            //this.buscarCaja(this.ctrlCorteCaja.corteCaja.caja.intId);
            this.llenar_formulario();
        }

        private void btnFooterUlitmoPag_Click(object sender, EventArgs e)
        {
            this.ctrlCorteCaja.UltimoPaginacion();
            if (!this.ctrlCorteCaja.bolResutladoPaginacion) return;
            this.paginacionCajas();
            //this.buscarCaja(this.ctrlCorteCaja.corteCaja.caja.intId);
            this.llenar_formulario();
        }
    }
}
