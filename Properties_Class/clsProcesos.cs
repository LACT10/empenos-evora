﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsProcesos :clsAbsBase
    {
        
        public int _intProcesoPadreId;
        public string _strProcesoPadreDescripcion;
        public string _strNombreMenu;
        public string _strQuerySubProcesos;
        public string _strQuerySubProcesosActualizar;
        public string _strQuerySubProcesosEliminar;

        public int intProcesoPadreId { get => _intProcesoPadreId; set => _intProcesoPadreId = value; }

        public string strProcesoPadreDescripcion { get => _strProcesoPadreDescripcion; set => _strProcesoPadreDescripcion = value; }
        public string strNombreMenu { get => _strNombreMenu; set => _strNombreMenu = value; }    


        public string strQuerySubProcesos { get => _strQuerySubProcesos; set => _strQuerySubProcesos = value; }

        public string strQuerySubProcesosActualizar { get => _strQuerySubProcesosActualizar; set => _strQuerySubProcesosActualizar = value; }
        public string strQuerySubProcesosEliminar { get => _strQuerySubProcesosEliminar; set => _strQuerySubProcesosEliminar = value; }

        public clsProcesos()
        {
            this.intProcesoPadreId = 0;
            this.strProcesoPadreDescripcion = "";
            this.strNombreMenu = "";
            this.strQuerySubProcesos = "";
            this.strQuerySubProcesosActualizar = "";
            this.strQuerySubProcesosEliminar = "";
            this.strDescripcion = "";
            this.strEstatus = "ACTIVO";
        }

        public void limpiarProps()
        {
            this.intProcesoPadreId = 0;
            this.strProcesoPadreDescripcion = "";
            this.strNombreMenu = "";
            this.strQuerySubProcesos = "";
            this.strQuerySubProcesosActualizar = "";
            this.strQuerySubProcesosEliminar = "";
            this.strDescripcion = "";
            this.strEstatus = "ACTIVO";

            this.limpiarPropsBase();
        }

    }
}
