﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsMovimientosCaja : clsAbsBase
    {
        public string _strFolio;        
        public string _strHora;
        public string _strConcepto;
        public DateTime _dtFechaMovimiento;        
        public string _strQueryMovimientos;
        public string _strMotivosCancelacion;
        public string _strUsuarioEliminacion;
        public string _strFechaEliminacion;

        public string strFolio { get => _strFolio; set => _strFolio = value; }
        public string strHora { get => _strHora; set => _strHora = value; }
        public string strConcepto { get => _strConcepto; set => _strConcepto = value; }
        public DateTime dtFechaMovimiento { get => _dtFechaMovimiento; set => _dtFechaMovimiento = value; }
        
        public string strQueryMovimientos { get => _strQueryMovimientos; set => _strQueryMovimientos = value; }
        public string strMotivosCancelacion { get => _strMotivosCancelacion; set => _strMotivosCancelacion = value; }

        public string strUsuarioEliminacion { get => _strUsuarioEliminacion; set => _strUsuarioEliminacion = value; }
        public string strFechaEliminacion { get => _strFechaEliminacion; set => _strFechaEliminacion = value; }

        public clsUsuario empleado = new clsUsuario();
        public clsCaja caja = new clsCaja();

        public clsMovimientosCaja() 
        {
            this.intNumCerosConsecutivo = 10;
            this.strFolio = "";
            this.strHora = "";
            this.strConcepto = "";
            this.dtFechaMovimiento =  DateTime.Now;            
            this.strQueryMovimientos = "";
            this.strMotivosCancelacion = "";
            this.strUsuarioEliminacion = "";
            this.strFechaEliminacion = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];
        }

        public void limpiarProps()
        {

            this.strFolio = "";
            this.strHora = "";
            this.strConcepto = "";
            this.dtFechaMovimiento = DateTime.Now;            
            this.strQueryMovimientos = "";
            this.strMotivosCancelacion = "";
            this.strUsuarioEliminacion = "";
            this.strFechaEliminacion = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];

            
            this.limpiarPropsBase();
            this.empleado.limpiarProps();
            this.caja.limpiarProps();

        }
    }

}
