﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsSucursalParametro
    {
        
        
        public int _intDiasCobroMinimo;
        public int _intDiasCobroCuotaDiaria;
        public int _intDiasCobroCuotaDiariaEspecial;
        public int _intDiasLiquidarBoletas;//Gracia
        public int _intDiasRefrendarBoletas;//Gracia
        public int _intDiasRematePrendas;

        public decimal _decAvaluo;
        public decimal _decAvaluoImpresion;


        public int intDiasCobroMinimo { get => _intDiasCobroMinimo; set => _intDiasCobroMinimo = value; }
        public int intDiasCobroCuotaDiaria { get => _intDiasCobroCuotaDiaria; set => _intDiasCobroCuotaDiaria = value; }
        public int intDiasCobroCuotaDiariaEspecial { get => _intDiasCobroCuotaDiariaEspecial; set => _intDiasCobroCuotaDiariaEspecial = value; }        
        public int intDiasLiquidarBoletas { get => _intDiasLiquidarBoletas; set => _intDiasLiquidarBoletas = value; }
        public int intDiasRefrendarBoletas { get => _intDiasRefrendarBoletas; set => _intDiasRefrendarBoletas = value; }
        public int intDiasRematePrendas { get => _intDiasRematePrendas; set => _intDiasRematePrendas = value; }        
        public decimal decAvaluo { get => _decAvaluo; set => _decAvaluo = value; }
        public decimal decAvaluoImpresion { get => _decAvaluoImpresion; set => _decAvaluoImpresion = value; }

        public  clsSucursalParametro()
        {
            this.intDiasCobroMinimo = 0;
            this.intDiasCobroCuotaDiaria = 0;
            this.intDiasCobroCuotaDiariaEspecial = 0;            
            this.intDiasLiquidarBoletas = 0;
            this.intDiasRefrendarBoletas = 0;
            this.intDiasRematePrendas = 0;            
            this.decAvaluo = 0;
            this.decAvaluoImpresion = 0;
    }

        public void limpiarProps()
        {
            this.intDiasCobroMinimo = 0;
            this.intDiasCobroCuotaDiaria = 0;
            this.intDiasCobroCuotaDiariaEspecial = 0;           
            this.intDiasLiquidarBoletas = 0;////Gracia
            this.intDiasRefrendarBoletas = 0;////Gracia
            this.intDiasRematePrendas = 0;            
            this.decAvaluo = 0;
            this.decAvaluoImpresion = 0;
        }
    }
}
