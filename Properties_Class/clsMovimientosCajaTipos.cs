﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsMovimientosCajaTipos : clsAbsBase
    {
        private string _strTipo;
        public string strTipo { get => _strTipo; set => _strTipo = value; }

        public clsMovimientosCajaTipos()
        {            
            this.strTipo = "";            
            this.strDescripcion = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];
        }

        public void limpiarProps()
        {
            
            this.strTipo = "";
            this.strDescripcion = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];

            this.limpiarPropsBase();
        }
    }
}
 