﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsConstante 
    {
        //Variable que se utlizar para los intentos de login
        public  int intIntentos = 3;
        //Variable que se utlizar para asginar los dias en que los dividen el porcentaje de intereses, manejo y almacenaje (Esto es para obtener la cuota diaria) 
        public  int intDiasMesCuota = 30;
        //Variable que se utlizar para asginar los dias en que los dividen el ppi
        public  int intDiasSemanas = 7;
        //Variable para asignar los dias de mes
        public  int intDiasMes = 30;
        //Variable que se usar como clientes varios
        public string strCodigoClientesVarios = "000000";
        /*Ditionary que se utilizar para los parametros de sucursales*/
        public Dictionary<string, string> dicParametrosSucursal = new Dictionary<string, string>()
        {
            {"dias_cobro_minimo","NUM. MÍNIMO DÍAS A COBRAR"},            
            {"dias_cobro_cuota_diaria", "DÍAS PARA COBRAR CUOTA DIARIA"},
            {"dias_cobrar_cuota_daria_especial", "DÍAS PARA COBRAR CUOTA DIARIA ESPECIAL"},
            {"dias_liquidar_boletas", "DÍAS DE GRACIA PARA LIQUIDAR BOLETAS"},
            {"dias_refrendar_boletas","DÍAS DE GRACIA PARA REFRENDAR BOLETAS"},
            {"dias_remate_prendas", "DÍAS PARA EL REMATE DE PRENDAS"},
            {"avaluo", "AVALÚO CÁLCULO"},
            {"avaluo_impresion", "AVALÚO IMPRESIÓN"}
        };

        /*Ditionary que se utilizar para los tipo de recibo*/
        public Dictionary<string, string> dicTipoRecibo = new Dictionary<string, string>()
        {
            {"LIQ","LIQUIDACION"},
            {"REF", "REFRENDO"},
            {"PAR", "PARCIAL"}            
        };





        public clsConstante() 
        {
        
        }


    }
}
