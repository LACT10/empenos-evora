﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsAdjudicacionBoletas : clsAbsBase
    {
        private string _strCodigo;
        public DateTime _dtAjudicacionFecha;
        public string _strUsuarioEliminacion;
        public string _strFechaEliminacion;
        public string _strQueryDetalles;
        public string _strQueryBoletaEmpenoEstatus;


        public string strCodigo { get => _strCodigo; set => _strCodigo = value; }
        public DateTime dtAjudicacionFecha { get => _dtAjudicacionFecha; set => _dtAjudicacionFecha = value; }
        public string strUsuarioEliminacion { get => _strUsuarioEliminacion; set => _strUsuarioEliminacion = value; }
        public string strFechaEliminacion { get => _strFechaEliminacion; set => _strFechaEliminacion = value; }
        public string strQueryDetalles { get => _strQueryDetalles; set => _strQueryDetalles = value; }
        public string strQueryBoletaEmpenoEstatus { get => _strQueryBoletaEmpenoEstatus; set => _strQueryBoletaEmpenoEstatus = value; }


        public clsUsuario empleado = new clsUsuario();
        public clsCliente cliente = new clsCliente();
        public clsCaja caja = new clsCaja();
        public clsUsuario usuarioCreacion = new clsUsuario();
        public clsUsuario usuarioActulizacion = new clsUsuario();
        public clsUsuario usuarioAutorizacion = new clsUsuario();


        public clsAdjudicacionBoletas() 
        {
            this.intNumCerosConsecutivo = 3;
            this.strCodigo = "";
            this.strUsuarioEliminacion = "";
            this.strFechaEliminacion = "";
            this.strQueryDetalles = "";
            this.strQueryBoletaEmpenoEstatus = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];
        }

        public void limpiarProps()
        {

            this.intId = 0;
            this.strCodigo = "";
            this.strUsuarioEliminacion = "";
            this.strFechaEliminacion = "";
            this.strQueryDetalles = "";
            this.strQueryBoletaEmpenoEstatus = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];

            this.limpiarPropsBase();            
            this.cliente.limpiarProps();            
            this.empleado.limpiarProps();
            this.caja.limpiarProps();
            this.usuarioCreacion.limpiarProps();
            this.usuarioActulizacion.limpiarProps();
            this.usuarioAutorizacion.limpiarProps();
        }
    }
}
