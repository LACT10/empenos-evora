﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public abstract class clsAbsBase
    {

        private int _intId;
        private string _strDescripcion;
        private DateTime _dtFecha;
        private string _strEstatus;
        private int _intUsuario;
        private int _intSucursal;
        private int _intNumCero;
        private string _strNumCero;
        private int _intProcesoMenuId;
        private int _intFolioId;
        private int _intRowIndex;
        public string _strQueryConsecutivoFolio;
        private int _intNumCerosConsecutivo;

        public int intId { get => _intId; set => _intId = value; }
        public string strDescripcion { get => _strDescripcion; set => _strDescripcion = value; }
        public string strEstatus { get => _strEstatus; set => _strEstatus = value; }
        public DateTime dtFecha { get => _dtFecha; set => _dtFecha = value; }        
        public int intUsuario { get => _intUsuario; set => _intUsuario = value; }
        public int intSucursal { get => _intSucursal; set => _intSucursal = value; }
        public int intNumCero { get => _intNumCero; set => _intNumCero = value; }
        public string strNumCero { get => _strNumCero; set => _strNumCero = value; }
        public int intProcesoMenuId { get => _intProcesoMenuId; set => _intProcesoMenuId = value; }
        public int intFolioId { get => _intFolioId; set => _intFolioId = value; }
        public int intRowIndex { get => _intRowIndex; set => _intRowIndex = value; }

        public string strQueryConsecutivoFolio { get => _strQueryConsecutivoFolio; set => _strQueryConsecutivoFolio = value; }

        public int intNumCerosConsecutivo { get => _intNumCerosConsecutivo; set => _intNumCerosConsecutivo = value; }




        public Dictionary<string, string> dicEstatus = new Dictionary<string, string>()
        {
            {"ACTIVO","ACTIVO"},
            {"INACTIVO","INACTIVO"},
            {"NO DESEADO","NO DESEADO"},
            {"SUSPENDIDO" ,"SUSPENDIDO"},
            {"CANCELADO" , "CANCELADO"},
            {"PPI" , "PAGO PARCIAL DE INTERESES"},
            {"LIQUIDADA" , "LIQUIDADA"},
            {"LIQUIDACION INTERNA" , "LIQUIDACION INTERNA"},
            {"ADJUDICADA" , "ADJUDICADA"}

        };

        public clsAbsBase()
        {
            this.intId = 0;
            this.dtFecha = DateTime.Now;
            this.intUsuario = 0;
            this.intSucursal = 0;
            this.intProcesoMenuId = 0;
            this.intFolioId = 0;
            this.strQueryConsecutivoFolio = " UPDATE folios " +
                    " SET " +
                    " consecutivo = (consecutivo +1), " +
                    " fecha_actualizacion = @fecha, " +
                    " usuario_actualizacion = @usuario " +
                    " WHERE folio_id = @folio_id";
        }
        //<summary>
        //Metodo que se utlizar para cambiar el estatus de un registro
        //</summar>
        public void cambioEstatus( string strEstatus)
        {
            this.strEstatus = strEstatus;
        }

        public void limpiarPropsBase()
        {
            this.intId = 0;
            this.dtFecha = DateTime.Now;
            this.intUsuario = 0;
            this.intSucursal = 0;
            this.intProcesoMenuId = 0;
            this.intFolioId = 0;
            this.strQueryConsecutivoFolio = " UPDATE folios " +
                  " SET " +
                  " consecutivo = (consecutivo +1), " +
                  " fecha_actualizacion = @fecha, " +
                  " usuario_actualizacion = @usuario " +
                  " WHERE folio_id = @folio_id";

        }

        public void agregarZero()
        {
            this.strNumCero.PadLeft(this.intNumCero, '0');
        }

        public string generaCodigoConsecutivo(string txtTexto)
        {
            return txtTexto.PadLeft(this.intNumCerosConsecutivo, '0');
        }

    }
}
