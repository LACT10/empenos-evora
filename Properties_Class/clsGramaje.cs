﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsGramaje : clsAbsBase
    {
        private string _strCodigo;
        public string strCodigo { get => _strCodigo; set => _strCodigo = value; }


        private decimal _decPrecioTope;
        public decimal decPrecioTope { get => _decPrecioTope; set => _decPrecioTope = value; }

        public clsPrendasTipo prendasTipo = new clsPrendasTipo();

        public clsGramaje()
        {
            this.intNumCerosConsecutivo = 2;
            this.strCodigo = "";
            this.strDescripcion = "";
            this.strEstatus = "ACTIVO";

            /*Dato es de la tabla sucursales_gramajes*/
            this.decPrecioTope = 0;
        }

        public void limpiarProps()
        {
            this.strCodigo = "";
            this.strDescripcion = "";
            this.strEstatus = "ACTIVO";

            /*Dato es de la tabla sucursales_gramajes*/
            this.decPrecioTope = 0;

            this.limpiarPropsBase();
            this.prendasTipo.limpiarProps();
        }

        public void agregarCero()
        {
            this.strNumCero = this.strCodigo;
            this.intNumCero = 2;
            this.agregarZero();
            this.strCodigo = this.strNumCero;
        }
    }
}
