﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsSucursal : clsAbsBase
    {
        public string _strCodigo;
        public string _strNombre;
        public string _strRepresentanteLegal;
        public string _strCalle;
        public string _strNumeroExterior;
        public string _strNumeroInterior;
        public string _strCorreoElectronico;
        public string _strTelefono1;
        public string _strTelefono2;
        public string _strQueryCobrosTipos;
        public string _strQueryGramajes;
        public string _strQueryPlazos;
        public string _strQueryParametros;
        public DateTime _dtInicioOperaciones;

        public string strCodigo { get => _strCodigo; set => _strCodigo = value; }        
        public string strNombre { get => _strNombre; set => _strNombre = value; }
        public string strRepresentanteLegal { get => _strRepresentanteLegal; set => _strRepresentanteLegal = value; }
        public string strTelefono1 { get => _strTelefono1; set => _strTelefono1 = value; }
        public string strTelefono2 { get => _strTelefono2; set => _strTelefono2 = value; }
        public string strCalle { get => _strCalle; set => _strCalle = value; }
        public string strNumeroExterior { get => _strNumeroExterior; set => _strNumeroExterior = value; }
        public string strNumeroInterior { get => _strNumeroInterior; set => _strNumeroInterior = value; }
        public string strCorreoElectronico { get => _strCorreoElectronico; set => _strCorreoElectronico = value; }

        public string strQueryCobrosTipos { get => _strQueryCobrosTipos; set => _strQueryCobrosTipos = value; }
        public string strQueryGramajes { get => _strQueryGramajes; set => _strQueryGramajes = value; }

        public string strQueryPlazos { get => _strQueryPlazos; set => _strQueryPlazos = value; }
        public string strQueryParametros { get => _strQueryParametros; set => _strQueryParametros = value; }
        public DateTime dtInicioOperaciones { get => _dtInicioOperaciones; set => _dtInicioOperaciones = value; }

        public clsEmpresa empresa = new clsEmpresa();
        public clsAsentamiento asentamiento = new clsAsentamiento();
        //public clsSucursalParametro parametro = new clsSucursalParametro();

        public clsSucursal()
        {
            this.intNumCerosConsecutivo = 3;
            this.strCodigo = "";
            this.strNombre = "";
            this.strRepresentanteLegal = "";
            this.strCalle = "";
            this.strNumeroExterior = "";
            this.strNumeroInterior = "";
            this.strCorreoElectronico = "";
            this.dtInicioOperaciones = DateTime.MinValue;
            this.strEstatus = this.dicEstatus["ACTIVO"];
        }


        public void limpiarProps()
        {

            this.strCodigo = "";
            this.strNombre = "";
            this.strRepresentanteLegal = "";
            this.strCalle = "";
            this.strNumeroExterior = "";
            this.strNumeroInterior = "";
            this.strCorreoElectronico = "";
            this.dtInicioOperaciones = DateTime.MinValue;
            this.strEstatus = this.dicEstatus["ACTIVO"];

            this.limpiarPropsBase();
            this.empresa.limpiarProps();
            this.asentamiento.limpiarProps();
            //this.parametro.limpiarProps();
        }

        public void agregarCero()
        {
            this.strNumCero = this.strCodigo;
            this.intNumCero = 3;
            this.agregarZero();
            this.strCodigo = this.strNumCero;
        }
    }
}
