﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsIdentificacionesTipo : clsAbsBase
    {
        private string _strCodigo;
        private string _strValidarCurp;
        public string strCodigo { get => _strCodigo; set => _strCodigo = value; }
        
        public string strValidarCurp { get => _strValidarCurp; set => _strValidarCurp = value; }

        public clsIdentificacionesTipo()
        {
            this.intNumCerosConsecutivo = 2;
            this.strCodigo = "";
            this.strDescripcion = "";
            this.strValidarCurp = "NO";
            this.strEstatus = this.dicEstatus["ACTIVO"];
        }

        public void limpiarProps()
        {
            
            this.strCodigo = "";
            this.strDescripcion = "";
            this.strValidarCurp = "NO";
            this.strEstatus = this.dicEstatus["ACTIVO"];

            this.limpiarPropsBase();            
        }

        public void agregarCero()
        {
            this.strNumCero = this.strCodigo;
            this.intNumCero = 2;
            this.agregarZero();
            this.strCodigo = this.strNumCero;
        }
    }
}

