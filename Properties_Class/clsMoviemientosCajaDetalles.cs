﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsMoviemientosCajaDetalles : clsAbsBase
    {

        private decimal _decImporte;

        public decimal decImporte { get => _decImporte; set => _decImporte = value; }


        public clsSucursal sucursalMovimiento = new clsSucursal();

        public clsMovimientosCajaTipos movimientosCajaTipos = new clsMovimientosCajaTipos();


        public clsMoviemientosCajaDetalles() 
        {
            this.decImporte = 0;
        
        }

        public void limpiarProps()
        {
            this.decImporte = 0;
            

            this.limpiarPropsBase();
            this.sucursalMovimiento.limpiarProps();
            this.movimientosCajaTipos.limpiarProps();
        }


    }
}
