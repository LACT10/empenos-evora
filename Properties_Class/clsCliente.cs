﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
   public class clsCliente : clsAbsBase
    {

        public string _strCodigo;
        public string _strNombre;
        public string _strApellidoPaterno;
        public string _strApellidoMaterno;
        public string _strGenero;
        public DateTime _dtFechaNacimiento;
        public string _strCorreoElectronico;
        public string _strTelefono01;
        public string _strTelefono02;
        public string _strRFC;
        public string _strCURP;
        public string _strCalle;
        public string _strComentario;
        public string _strNumeroExterior;
        public string _strNumeroInterior;
        public string _strCotitularNombre;
        public string _strCotitularApellidoPaterno;
        public string _strCotitularApellidoMaterno;
        public string _strBeneficiarioNombre;
        public string _strBeneficiarioApellidoPaterno;
        public string _strBeneficiarioApellidoMaterno;
        public string _strFotografiaImagen;
        public string _strIdentificacionImagenFrente;
        public string _strIdentificacionImagenReverso;
        public MemoryStream _msHuella;
        public string _strQueryHuella;
        public string _strNumeroIdentificacion;


        public string strCodigo { get => _strCodigo; set => _strCodigo = value; }
        public string strNombre { get => _strNombre; set => _strNombre = value; }
        public string strApellidoPaterno { get => _strApellidoPaterno; set => _strApellidoPaterno = value; }
        public string strApellidoMaterno { get => _strApellidoMaterno; set => _strApellidoMaterno = value; }
        public string strGenero { get => _strGenero; set => _strGenero = value; }
        public DateTime dtFechaNacimiento { get => _dtFechaNacimiento; set => _dtFechaNacimiento = value; }
        public string strCorreoElectronico { get => _strCorreoElectronico; set => _strCorreoElectronico = value; }
        public string strTelefono01 { get => _strTelefono01; set => _strTelefono01 = value; }
        public string strTelefono02 { get => _strTelefono02; set => _strTelefono02 = value; }
        public string strRFC { get => _strRFC; set => _strRFC = value; }
        public string strCURP { get => _strCURP; set => _strCURP = value; }
        public string strCalle { get => _strCalle; set => _strCalle = value; }
        public string strNumeroExterior  { get => _strNumeroExterior; set => _strNumeroExterior = value; }
        public string strNumeroInterior { get => _strNumeroInterior; set => _strNumeroInterior = value; }
        public string strCotitularNombre { get => _strCotitularNombre; set => _strCotitularNombre = value; }
        public string strCotitularApellidoPaterno { get => _strCotitularApellidoPaterno; set => _strCotitularApellidoPaterno = value; }
        public string strCotitularApellidoMaterno { get => _strCotitularApellidoMaterno; set => _strCotitularApellidoMaterno = value; }
        public string strBeneficiarioNombre { get => _strBeneficiarioNombre; set => _strBeneficiarioNombre = value; }
        public string strBeneficiarioApellidoPaterno { get => _strBeneficiarioApellidoPaterno; set => _strBeneficiarioApellidoPaterno = value; }
        public string strBeneficiarioApellidoMaterno { get => _strBeneficiarioApellidoMaterno; set => _strBeneficiarioApellidoMaterno = value; }
        public string strFotografiaImagen { get => _strFotografiaImagen; set => _strFotografiaImagen = value; }
        public string strIdentificacionImagenFrente { get => _strIdentificacionImagenFrente; set => _strIdentificacionImagenFrente = value; }
        public string strIdentificacionImagenReverso { get => _strIdentificacionImagenReverso; set => _strIdentificacionImagenReverso = value; }
        public string strComentario { get => _strComentario; set => _strComentario = value; }
        public MemoryStream msHuella { get => _msHuella; set => _msHuella = value; }

        public string strQueryHuella { get => _strQueryHuella; set => _strQueryHuella = value; }

        public string strNumeroIdentificacion { get => _strNumeroIdentificacion; set => _strNumeroIdentificacion = value; }



        public clsSucursal sucursal = new clsSucursal();

        public clsAsentamiento asentamiento = new clsAsentamiento();

        public clsIdentificacionesTipo identificacionesTipo = new clsIdentificacionesTipo();

        public clsOcupacion ocupacion = new clsOcupacion();


        public Dictionary<string, string> dicGenero = new Dictionary<string, string>()
        {
            {"MASCULINO","MASCULINO"},
            {"FEMENINO","FEMENINO"}

        };

        public clsCliente()
        {
            this.intNumCerosConsecutivo = 6;
            this.strCodigo = "";
            this.strNombre = "";
            this.strApellidoPaterno = "";
            this.strApellidoMaterno = "";
            this.strGenero = "";
            this.strCorreoElectronico = "";
            this.strTelefono01 = "";
            this.strTelefono02 = "";
            this.strRFC = "";
            this.strCURP = "";
            this.strCalle = "";
            this.strComentario = "";
            this.strNumeroExterior = "";
            this.strNumeroInterior = "";
            this.strCotitularNombre = "";
            this.strCotitularApellidoPaterno = "";
            this.strCotitularApellidoMaterno = "";
            this.strBeneficiarioNombre = "";
            this.strBeneficiarioApellidoPaterno = "";
            this.strBeneficiarioApellidoMaterno = "";
            this.strFotografiaImagen = "";
            this.strIdentificacionImagenFrente = "";
            this.strIdentificacionImagenReverso = "";
            this.strQueryHuella = "";
            this.strNumeroIdentificacion = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];
        }

        public void limpiarProps()
        {
             

            this.strCodigo = "";
            this.strNombre = "";
            this.strApellidoPaterno = "";
            this.strApellidoMaterno = "";
            this.strGenero = "";            
            this.strCorreoElectronico = "";
            this.strTelefono01 = "";
            this.strTelefono02 = "";
            this.strRFC = "";
            this.strCURP = "";
            this.strCalle = "";
            this.strComentario = "";
            this.strNumeroExterior = "";
            this.strNumeroInterior = "";
            this.strCotitularNombre = "";
            this.strCotitularApellidoPaterno = "";
            this.strCotitularApellidoMaterno = "";
            this.strBeneficiarioNombre = "";
            this.strBeneficiarioApellidoPaterno = "";
            this.strBeneficiarioApellidoMaterno = "";
            this.strFotografiaImagen = "";
            this.strIdentificacionImagenFrente = "";
            this.strIdentificacionImagenReverso = "";            
            this.strQueryHuella = "";
            this.strNumeroIdentificacion = "";            
            this.strEstatus = this.dicEstatus["ACTIVO"];

            this.limpiarPropsBase();
            this.asentamiento.limpiarProps();
            this.sucursal.limpiarProps();
            this.identificacionesTipo.limpiarProps();
            this.ocupacion.limpiarProps();
        }

        public void agregarCero()
        {
            this.strNumCero = this.strCodigo;
            this.intNumCero = 3;
            this.agregarZero();
            this.strCodigo = this.strNumCero;
        }

        public bool validarClienteNoDeseado()         
        {

            bool bolValidar = false;
            if (this.strEstatus == this.dicEstatus["NO DESEADO"]) 
            {
                bolValidar = true;

            }

            return bolValidar;
        }

    }
}
