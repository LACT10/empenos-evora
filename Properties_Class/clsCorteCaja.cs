﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsCorteCaja : clsAbsBase
    {
        private DateTime _dtFechaCorte;
        private DateTime _dtFechaUlimoCorteCierre;
        private string _strTipo;
        private decimal _decCapital;
        private decimal _decAbonoCapital;
        
        private decimal _decInteres;
        private decimal _decAlmacenaje;
        private decimal _decManejo;
        private decimal _decBonificacinones;
        private decimal _decLiqBoletas;
        private decimal _decEntradasCaja;
        private decimal _decSalidadCaja;        
        private decimal _decTotalIngresos;
        private decimal _decBoletasEmpenadas;
        private decimal _decTotalEgresos;
        private decimal _decSaldoInicial;
        private decimal _decSaldoFinal;
        private decimal _decImporteTeorico;

        /*CANTIDAD DE BILLETES Y MONEDAS POR DENOMINACION*/
        private int _intBilletes1000;
        private int _intBilletes500;
        private int _intBilletes200;
        private int _intBilletes100;
        private int _intBilletes50;
        private int _intBilletes20;
        private int _intMonedas10;
        private int _intMonedas5;
        private int _intMonedas2;
        private int _intMonedas1;
        private int _intMonedas50c;
        private int _intMonedas20c;
        private int _intMonedas10c;
        private int _intMonedas5c;

        private string _strListaBoletasId;
        private string _strListaBoletasRefrendosId;
        private string _strListaBoletasSinPagoId;


        private string _strListaPagoId;
        private string _strListaMovimientoEntradaId;
        private string _strListaMovimientoSalidadId;

        private string _strQueryBoletaEmpeno;
        private string _strQueryBoletaEmpenoRefrendos;
        private string _strQueryBoletaEmpenoSinPago;

        private string _strQueryPago;
        private string _strQueryMovimientoEntradadCaja;
        private string _strQueryMovimientoSalidadCaja;

        private string _strQueryCajaCorteDetallesBoletasId;
        private string _strQueryCajaCorteDetallesBoletasRefrendosId;
        private string _strQueryCajaCorteDetallesBoletasSinPagoId;
        private string _strQueryCajaCorteDetallesPagoId;
        private string _strQueryCajaCorteDetallesMovimientoEntradadId;
        private string _strQueryCajaCorteDetallesMovimientoSalidadId;


        public DateTime dtFechaCorte { get => _dtFechaCorte; set => _dtFechaCorte = value; }

        public DateTime dtFechaUlimoCorteCierre { get => _dtFechaUlimoCorteCierre; set => _dtFechaUlimoCorteCierre = value; }

        public string strTipo { get => _strTipo; set => _strTipo = value; }

        public decimal decCapital { get => _decCapital; set => _decCapital = value; }
        public decimal decAbonoCapital { get => _decAbonoCapital; set => _decAbonoCapital = value; }
        public decimal decInteres { get => _decInteres; set => _decInteres = value; }
        public decimal decAlmacenaje { get => _decAlmacenaje; set => _decAlmacenaje = value; }
        public decimal decManejo { get => _decManejo; set => _decManejo = value; }
        public decimal decBonificacinones { get => _decBonificacinones; set => _decBonificacinones = value; }
        public decimal decLiqBoletas { get => _decLiqBoletas; set => _decLiqBoletas = value; }
        public decimal decEntradasCaja { get => _decEntradasCaja; set => _decEntradasCaja = value; }
        public decimal decSalidadCaja { get => _decSalidadCaja; set => _decSalidadCaja = value; }
        public decimal decTotalIngresos { get => _decTotalIngresos; set => _decTotalIngresos = value; }
        public decimal decBoletasEmpenadas { get => _decBoletasEmpenadas; set => _decBoletasEmpenadas = value; }
        public decimal decTotalEgresos { get => _decTotalEgresos; set => _decTotalEgresos = value; }
        public decimal decSaldoInicial { get => _decSaldoInicial; set => _decSaldoInicial = value; }
        public decimal decSaldoFinal { get => _decSaldoFinal; set => _decSaldoFinal = value; }
        public decimal decImporteTeorico { get => _decImporteTeorico; set => _decImporteTeorico = value; }

        



        /*CANTIDAD DE BILLETES Y MONEDAS POR DENOMINACION*/
        public int intBilletes1000 { get => _intBilletes1000; set => _intBilletes1000 = value; }
        public int intBilletes500 { get => _intBilletes500; set => _intBilletes500 = value; }
        public int intBilletes200 { get => _intBilletes200; set => _intBilletes200 = value; }
        public int intBilletes100 { get => _intBilletes100; set => _intBilletes100 = value; }
        public int intBilletes50 { get => _intBilletes50; set => _intBilletes50 = value; }
        public int intBilletes20 { get => _intBilletes20; set => _intBilletes20 = value; }
        public int intMonedas10 { get => _intMonedas10; set => _intMonedas10 = value; }
        public int intMonedas5 { get => _intMonedas5; set => _intMonedas5 = value; }
        public int intMonedas2 { get => _intMonedas2; set => _intMonedas2 = value; }
        public int intMonedas1 { get => _intMonedas1; set => _intMonedas1 = value; }
        public int intMonedas50c { get => _intMonedas50c; set => _intMonedas50c = value; }
        public int intMonedas20c { get => _intMonedas20c; set => _intMonedas20c = value; }
        public int intMonedas10c { get => _intMonedas10c; set => _intMonedas10c = value; }
        public int intMonedas5c { get => _intMonedas5c; set => _intMonedas5c = value; }

        public string strListaBoletasId { get => _strListaBoletasId; set => _strListaBoletasId = value; }
        public string strListaBoletasRefrendosId { get => _strListaBoletasRefrendosId; set => _strListaBoletasRefrendosId = value; }
        public string strListaBoletasSinPagoId { get => _strListaBoletasSinPagoId; set => _strListaBoletasSinPagoId = value; }
        public string strListaPagoId { get => _strListaPagoId; set => _strListaPagoId = value; }
        public string strListaMovimientoEntradaId { get => _strListaMovimientoEntradaId; set => _strListaMovimientoEntradaId = value; }
        public string strListaMovimientoSalidadId { get => _strListaMovimientoSalidadId; set => _strListaMovimientoSalidadId = value; }


        public string strQueryBoletaEmpeno { get => _strQueryBoletaEmpeno; set => _strQueryBoletaEmpeno = value; }
        public string strQueryBoletaEmpenoRefrendos { get => _strQueryBoletaEmpenoRefrendos; set => _strQueryBoletaEmpenoRefrendos = value; }
        public string strQueryBoletaEmpenoSinPago { get => _strQueryBoletaEmpenoSinPago; set => _strQueryBoletaEmpenoSinPago = value; }
        public string strQueryPago { get => _strQueryPago; set => _strQueryPago = value; }
        public string strQueryMovimientoEntradadCaja { get => _strQueryMovimientoEntradadCaja; set => _strQueryMovimientoEntradadCaja = value; }
        public string strQueryMovimientoSalidadCaja { get => _strQueryMovimientoSalidadCaja; set => _strQueryMovimientoSalidadCaja = value; }

        public string strQueryCajaCorteDetallesBoletasId { get => _strQueryCajaCorteDetallesBoletasId; set => _strQueryCajaCorteDetallesBoletasId = value; }
        public string strQueryCajaCorteDetallesBoletasRefrendosId { get => _strQueryCajaCorteDetallesBoletasRefrendosId; set => _strQueryCajaCorteDetallesBoletasRefrendosId = value; }
        public string strQueryCajaCorteDetallesBoletasSinPagoId { get => _strQueryCajaCorteDetallesBoletasSinPagoId; set => _strQueryCajaCorteDetallesBoletasSinPagoId = value; }
        public string strQueryCajaCorteDetallesPagoId { get => _strQueryCajaCorteDetallesPagoId; set => _strQueryCajaCorteDetallesPagoId = value; }
        public string strQueryCajaCorteDetallesMovimientoEntradadId { get => _strQueryCajaCorteDetallesMovimientoEntradadId; set => _strQueryCajaCorteDetallesMovimientoEntradadId = value; }
        public string strQueryCajaCorteDetallesMovimientoSalidadId { get => _strQueryCajaCorteDetallesMovimientoSalidadId; set => _strQueryCajaCorteDetallesMovimientoSalidadId = value; }



        public clsCaja caja = new clsCaja();

        public clsCorteCaja() 
        {
            this.strTipo = "";
            this.strDescripcion = "";
            this.decCapital = 0;
            this.decAbonoCapital = 0;
            this.decInteres = 0;
            this.decAlmacenaje = 0;
            this.decManejo = 0;
            this.decBonificacinones = 0;
            this.decLiqBoletas = 0;
            this.decEntradasCaja = 0;
            this.decSalidadCaja = 0;
            this.decTotalIngresos = 0;
            this.decBoletasEmpenadas = 0;
            this.decTotalEgresos = 0;
            this.decImporteTeorico = 0;

            this.intBilletes1000 = 0;
            this.intBilletes500 = 0;
            this.intBilletes200 = 0;
            this.intBilletes100 = 0;
            this.intBilletes50 = 0;
            this.intBilletes20 = 0;
            this.intMonedas10 = 0;
            this.intMonedas5 = 0;
            this.intMonedas2 = 0;
            this.intMonedas1 = 0;
            this.intMonedas50c = 0;
            this.intMonedas20c = 0;
            this.intMonedas10c = 0;
            this.intMonedas5c = 0;

            this.strListaBoletasId = "";
            this.strListaBoletasRefrendosId = "";
            this.strListaBoletasSinPagoId = "";
            
            this.strListaPagoId = "";
            this.strListaMovimientoEntradaId = "";
            this.strListaMovimientoSalidadId = "";

            this.strQueryBoletaEmpeno = "";
            this.strQueryBoletaEmpenoRefrendos = "";
            this.strQueryBoletaEmpenoSinPago = "";
            this.strQueryPago = "";
            this.strQueryMovimientoEntradadCaja = "";
            this.strQueryMovimientoSalidadCaja = "";

            this.strQueryCajaCorteDetallesBoletasId = "";
            this.strQueryCajaCorteDetallesBoletasRefrendosId = "";
            this.strQueryCajaCorteDetallesBoletasSinPagoId = "";
            this.strQueryCajaCorteDetallesPagoId = "";
            this.strQueryCajaCorteDetallesMovimientoEntradadId = "";
            this.strQueryCajaCorteDetallesMovimientoSalidadId = "";
            this.strEstatus = "ACTIVO";


        }

        

        public decimal calcularLiqBoletas() 
        {
            return this.decCapital  + this.decInteres + this.decAlmacenaje + this.decManejo - this.decBonificacinones;
        }

        public decimal calcularTotalIngresos() 
        {
            return this.decLiqBoletas + this.decEntradasCaja;
        }

        public decimal calcularTotalEgresos() 
        {
            return this.decBoletasEmpenadas + this.decSalidadCaja;
        }

        public decimal calcularSaldoFinal()
        {
            return this.decSaldoInicial + this.decTotalIngresos - this.decTotalEgresos;
        }

        public void limpiarProps()
        {
            this.strTipo = "";
            this.strDescripcion = "";
            this.decCapital = 0;
            this.decAbonoCapital = 0;
            this.decInteres = 0;
            this.decAlmacenaje = 0;
            this.decManejo = 0;
            this.decBonificacinones = 0;
            this.decLiqBoletas = 0;
            this.decEntradasCaja = 0;
            this.decSalidadCaja = 0;
            this.decTotalIngresos = 0;
            this.decBoletasEmpenadas = 0;
            this.decTotalEgresos = 0;
            this.decImporteTeorico = 0;
            this.strEstatus = "ACTIVO";

            this.intBilletes1000 = 0;
            this.intBilletes500 = 0;
            this.intBilletes200 = 0;
            this.intBilletes100 = 0;
            this.intBilletes50 = 0;
            this.intBilletes20 = 0;
            this.intMonedas10 = 0;
            this.intMonedas5 = 0;
            this.intMonedas2 = 0;
            this.intMonedas1 = 0;
            this.intMonedas50c = 0;
            this.intMonedas20c = 0;
            this.intMonedas10c = 0;
            this.intMonedas5c = 0;

            this.strListaBoletasId = "";
            this.strListaBoletasRefrendosId = "";
            this.strListaBoletasSinPagoId = "";
            this.strListaPagoId = "";
            this.strListaMovimientoEntradaId = "";
            this.strListaMovimientoSalidadId = "";

            this.strQueryBoletaEmpeno = "";
            this.strQueryBoletaEmpenoRefrendos = "";
            this.strQueryBoletaEmpenoSinPago = "";
            this.strQueryPago = "";
            this.strQueryMovimientoEntradadCaja = "";
            this.strQueryMovimientoSalidadCaja = "";

            this.strQueryCajaCorteDetallesBoletasId = "";
            this.strQueryCajaCorteDetallesBoletasRefrendosId = "";
            this.strQueryCajaCorteDetallesBoletasSinPagoId = "";
            this.strQueryCajaCorteDetallesPagoId = "";
            this.strQueryCajaCorteDetallesMovimientoEntradadId = "";
            this.strQueryCajaCorteDetallesMovimientoSalidadId = "";
            this.limpiarPropsBase();
            this.caja.limpiarProps();
        }
    }
}
