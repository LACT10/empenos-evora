﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora.Properties_Class
{
    public class clsTextbox : Form
    {

        public TextBox _txtGenerico;
        public Button _BtnGenerico;
        public DateTimePicker _dtpGenerico;
        public RadioButton _rbtGenerico;
        public NumericUpDown _nudGenerico;
        public string  _strBusqueda;



        public TextBox txtGenerico { get => _txtGenerico; set => _txtGenerico = value; }

        public Button BtnGenerico { get => _BtnGenerico; set => _BtnGenerico = value; }

        public DateTimePicker dtpGenerico { get => _dtpGenerico; set => _dtpGenerico = value; }

        public RadioButton rbtGenerico { get => _rbtGenerico; set => _rbtGenerico = value; }

        public NumericUpDown nudGenerico { get => _nudGenerico; set => _nudGenerico = value; }

        public string   strBusqueda { get => _strBusqueda; set => _strBusqueda = value; }



        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // clsTextbox
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Name = "clsTextbox";
            this.Load += new System.EventHandler(this.clsTextbox_Load);
            this.ResumeLayout(false);

        }

        private void clsTextbox_Load(object sender, EventArgs e)
        {

        }
    }
}
