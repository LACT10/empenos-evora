﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace EmpenosEvora.Properties_Class
{
    public class clsLogin : clsAbsBase
    {
        public int _intEmpresaId;
        public int _intSucursalId;
        public int _intUsuarioId;
        public int _intUsuarioIntentos;
        public string _strUsuario;
        public string _strSucursal;
        public string _strEmpresa;
        public string _strPermisos;
        public string _strIdsSubProcesos;
        public string[] _arrStrIdsSubProcesos;
        public string _strQueryPermisosAccessoProcessoUsuario;
        public DataSet _objCajas;
        public DataSet _objProcesos;
        public DataSet _objSubProcesos;

        public int intUsuarioId { get => _intUsuarioId; set => _intUsuarioId = value; }
        public int intEmpresaId { get => _intEmpresaId; set => _intEmpresaId = value; }
        public int intSucursalId { get => _intSucursalId; set => _intSucursalId = value; }
        public int intUsuarioIntentos { get => _intUsuarioIntentos; set => _intUsuarioIntentos = value; }

        public string strUsuario { get => _strUsuario; set => _strUsuario = value; }
        public string strSucursal { get => _strSucursal; set => _strSucursal = value; }
        public string strEmpresa { get => _strEmpresa; set => _strEmpresa = value; }
        public string strPermisos { get => _strPermisos; set => _strPermisos = value; }
        
        public string strIdsSubProcesos { get => _strIdsSubProcesos; set => _strIdsSubProcesos = value; }

        public string[] arrStrIdsSubProcesos { get => _arrStrIdsSubProcesos; set => _arrStrIdsSubProcesos = value; }

        public string strQueryPermisosAccessoProcessoUsuario { get => _strQueryPermisosAccessoProcessoUsuario; set => _strQueryPermisosAccessoProcessoUsuario = value; }

        public DataSet objCajas { get => _objCajas; set => _objCajas = value; }

        public DataSet objProcesos { get => _objProcesos; set => _objProcesos = value; }

        public DataSet objSubProcesos { get => _objSubProcesos; set => _objSubProcesos = value; }

        public clsUsuario usuario = new clsUsuario();

        public clsCaja caja = new clsCaja();

        public string strCajaIntregradora = "CAJA INTEGRADORA";



        public clsLogin() 
        {
     
            this.intEmpresaId = 0;
            this.intSucursalId = 0;
            this.strPermisos = "";
            
        }

        public void limpiarProps()
        {

            this.intEmpresaId = 0;
            this.intSucursalId = 0;
            this.strIdsSubProcesos = "";
            this.strPermisos = "";            
            this.objCajas = null;
            this.objProcesos = null;
            this.objSubProcesos = null;
            this.usuario.limpiarProps();
        }




    }
}
