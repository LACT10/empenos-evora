﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsPrendasTipo : clsAbsBase
    {
        public string _strCodigo;
        public string _strCapturarGramaje;
        public string _strCapturarEmailCelular;
        public string _strCapturarSerieVehiculo;
        public bool _bolCapturarGramaje;
        public bool _bolCapturarEmailCelular;
        public bool _bolCapturarSerieVehiculo;


        public string strCodigo { get => _strCodigo; set => _strCodigo = value; }        

                
       public string strCapturarGramaje { get => _strCapturarGramaje; set => _strCapturarGramaje = value; }
        public string strCapturarEmailCelular { get => _strCapturarEmailCelular; set => _strCapturarEmailCelular = value; }
        public string strCapturarSerieVehiculo { get => _strCapturarSerieVehiculo; set => _strCapturarSerieVehiculo = value; }
        public bool bolCapturarGramaje { get => _bolCapturarGramaje; set => _bolCapturarGramaje = value; }
        public bool bolCapturarEmailCelular { get => _bolCapturarEmailCelular; set => _bolCapturarEmailCelular = value; }
        public bool bolCapturarSerieVehiculo { get => _bolCapturarSerieVehiculo; set => _bolCapturarSerieVehiculo = value; }

        public Dictionary<string, string> dicCapturar = new Dictionary<string, string>()
        {
            {"SI","SI"},
            {"NO","NO"},
        };    

        public clsPrendasTipo()
        {
            this.intNumCerosConsecutivo = 2;
            this.strCodigo = "";
            this.strDescripcion = "";
            this.strCapturarGramaje = this.dicCapturar["NO"];
            this.strCapturarEmailCelular = this.dicCapturar["NO"];
            this.strCapturarSerieVehiculo = this.dicCapturar["NO"];
            this.bolCapturarGramaje = false;
            this.bolCapturarEmailCelular = false;
            this.bolCapturarSerieVehiculo = false;
            this.strEstatus = this.dicEstatus["ACTIVO"];
        }

        public void limpiarProps()
        {            
            this.strCodigo = "";
            this.strDescripcion = "";
            this.strCapturarGramaje = this.dicCapturar["NO"];
            this.bolCapturarGramaje = false;
            this.bolCapturarEmailCelular = false;
            this.bolCapturarSerieVehiculo = false;
            this.strEstatus = this.dicEstatus["ACTIVO"];

            this.limpiarPropsBase();
        }

        public bool asignarCapturar(string strCapturar)
        {
            return (strCapturar == this.dicCapturar["NO"])? false : true;
        }

        public void agregarCero()
        {
            this.strNumCero = this.strCodigo;
            this.intNumCero = 2;
            this.agregarZero();
            this.strCodigo = this.strNumCero;
        }
    }
}