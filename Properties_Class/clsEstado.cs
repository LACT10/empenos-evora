﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsEstado : clsAbsBase
    {
        private string _strCodigo;       
        public string strCodigo { get => _strCodigo; set => _strCodigo = value; }
        
        public clsEstado()
        {
            this.intNumCerosConsecutivo = 3;
            this.strCodigo = "";
            this.strDescripcion = "";
            this.strEstatus = "ACTIVO";            
        }

        public void limpiarProps()
        {            
            this.strCodigo = "";
            this.strDescripcion = "";
            this.strEstatus = "ACTIVO";

            this.limpiarPropsBase();
        }

        public void agregarCero() 
        {
            this.strNumCero = this.strCodigo;
            this.intNumCero = 3;            
            this.strCodigo = this.strNumCero;
        }
    }
}
