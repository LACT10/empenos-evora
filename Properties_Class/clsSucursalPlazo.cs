﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsSucursalPlazo : clsAbsBase
    {


        private float _ftPlazo;

        public float ftPlazo { get => _ftPlazo; set => _ftPlazo = value; }


        public clsSucursal sucursal = new clsSucursal();

        public clsPrendasTipo prendasTipo = new clsPrendasTipo();




        public clsSucursalPlazo()
        {
            this.ftPlazo = 0;
            this.strDescripcion = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];
        }

        public void limpiarProps()
        {
            this.ftPlazo = 0;
            this.strDescripcion = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];

            this.limpiarPropsBase();
            this.sucursal.limpiarProps();
            this.prendasTipo.limpiarProps();
        }
    }
}
