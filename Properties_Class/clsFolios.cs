﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsFolios : clsAbsBase
    {


        private string _strSerie;
        private int _intConsecutivo;
        public string strSerie { get => _strSerie; set => _strSerie = value; }
        public int intConsecutivo { get => _intConsecutivo; set => _intConsecutivo = value; }


        public clsFolios() 
        {
            this.strSerie = "";
            this.strDescripcion = "";
            this.intConsecutivo = 0;
            this.strEstatus = "ACTIVO";
        }

        public void limpiarProps()
        {
            this.strSerie = "";
            this.strDescripcion = "";
            this.intConsecutivo = 0;
            this.strEstatus = "ACTIVO";

            this.limpiarPropsBase();
        }

        //Metado que se utilizar para regresar el consecutivo de un folio
        public string regresarFolioConsecutivo(int intNumCerosConsecutivo) 
        {
            return this.strSerie.PadRight(intNumCerosConsecutivo - this.intConsecutivo.ToString().Length, '0') + this.intConsecutivo;
        }

        public string regresarFolioConsecutivoAuto(int intNumCerosConsecutivo, string txtTexto) 
        {
            return this.strSerie.PadRight(intNumCerosConsecutivo -  txtTexto.Length, '0') + txtTexto;
        }
    }
}
