﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsSucursalGramaje : clsAbsBase
    {


        private float _ftPreciotope;

        public float ftPreciotope { get => _ftPreciotope; set => _ftPreciotope = value; }


        public clsSucursal sucursal = new clsSucursal();

        public clsPrendasTipo prendasTipo = new clsPrendasTipo();




        public clsSucursalGramaje()
        {
            this.ftPreciotope = 0;
            this.strDescripcion = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];
        }

        public void limpiarProps()
        {
            this.ftPreciotope = 0;
            this.strDescripcion = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];

            this.limpiarPropsBase();
            this.sucursal.limpiarProps();
            this.prendasTipo.limpiarProps();
        }
    }
}
