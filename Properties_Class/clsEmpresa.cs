﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsEmpresa : clsAbsBase
    {

        private string _strRazonSocial;

        private string _strNombreComercial;

        private string _strRFC;
        private string _strCURP;

        public string strRazonSocial { get => _strRazonSocial; set => _strRazonSocial = value; }

        public string strNombreComercial { get => _strNombreComercial; set => _strNombreComercial = value; }

        public string strRFC { get => _strRFC; set => _strRFC = value; }

        public string strCURP { get => _strCURP; set => _strCURP = value; }

        public clsEmpresa()
        {
            this.strRazonSocial = "";
            this.strNombreComercial = "";
            this.strRFC = "";
            this.strCURP = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];
        }

        public void limpiarProps()
        {
            this.strRazonSocial = "";
            this.strNombreComercial = "";
            this.strRFC = "";
            this.strCURP = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];

            this.limpiarPropsBase();
        }
    }
}
