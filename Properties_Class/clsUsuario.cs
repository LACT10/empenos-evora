﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsUsuario : clsAbsBase
    {

        public string _strNombre;
        public string _strApellidoPaterno;
        public string _strApellidoMaterno;
        public string _strTelefono01;
        public string _strTelefono02;
        public string _strCorreoElectronico;
        public string _strUsuario;
        public string _strContrasena;
        public string _strAutorizar;
        public string _strModificarMovimientos;
        public byte _byteIntentos;
        public MemoryStream _msHuella;        
        public string _strQueryCajaUsuario;
        public string _strQueryPermisosUsuario;
        public string _strQueryCajaUsuarioDelete;
        public string _strQueryPermisosUsuarioDelete;
        public bool _bolHuella;

        public string strNombre { get => _strNombre; set => _strNombre = value; }
        public string strApellidoPaterno { get => _strApellidoPaterno; set => _strApellidoPaterno = value; }
        public string strApellidoMaterno { get => _strApellidoMaterno; set => _strApellidoMaterno = value; }
        public string strTelefono01 { get => _strTelefono01; set => _strTelefono01 = value; }
        public string strTelefono02 { get => _strTelefono02; set => _strTelefono02 = value; }
        public string strCorreoElectronico { get => _strCorreoElectronico; set => _strCorreoElectronico = value; }
        public string strUsuario { get => _strUsuario; set => _strUsuario = value; }
        public string strContrasena { get => _strContrasena; set => _strContrasena = value; }
        public byte byteIntentos { get => _byteIntentos; set => _byteIntentos = value; }

        public string strAutorizar { get => _strAutorizar; set => _strAutorizar = value; }

        public string strModificarMovimientos { get => _strModificarMovimientos; set => _strModificarMovimientos = value; }
        public MemoryStream msHuella { get => _msHuella; set => _msHuella = value; }
        

        public string strQueryCajaUsuario { get => _strQueryCajaUsuario; set => _strQueryCajaUsuario = value; }

        public string strQueryPermisosUsuario { get => _strQueryPermisosUsuario; set => _strQueryPermisosUsuario = value; }

        public string  strQueryCajaUsuarioDelete { get => _strQueryCajaUsuarioDelete; set => _strQueryCajaUsuarioDelete = value; }

        public string strQueryPermisosUsuarioDelete { get => _strQueryPermisosUsuarioDelete; set => _strQueryPermisosUsuarioDelete = value; }

        public bool bolHuella { get => _bolHuella; set => _bolHuella = value; }



        public Dictionary<string, string> dicModificarMovimientos = new Dictionary<string, string>()
        {
            {"SI","SI"},
            {"NO","NO"}
            
        };


        public Dictionary<string, string> dicAutorizar = new Dictionary<string, string>()
        {
            {"SI","SI"},
            {"NO","NO"}

        };



        public clsUsuario()
        {


            this.strNombre = "";
            this.strApellidoPaterno = "";
            this.strApellidoMaterno = "";
            this.strTelefono01 = "";
            this.strTelefono02 = "";
            this.strCorreoElectronico = "";
            this.strUsuario = "";
            this.strContrasena = "";
            this.strAutorizar = "0";
            this.strModificarMovimientos = "0";
            this.byteIntentos = 0;
            this.strEstatus = this.dicEstatus["ACTIVO"];            
            this.strQueryPermisosUsuario = "";
            this.strQueryCajaUsuario = "";
            this.strQueryCajaUsuarioDelete = "";
            this.strQueryPermisosUsuarioDelete = "";
            this.bolHuella = false;

        }

        public void limpiarProps()
        {
            this.strNombre = "";
            this.strApellidoPaterno = "";
            this.strApellidoMaterno = "";
            this.strTelefono01 = "";
            this.strTelefono02 = "";
            this.strCorreoElectronico = "";
            this.strUsuario = "";
            this.strContrasena = "";
            this.strAutorizar = "0";
            this.strModificarMovimientos = "0";
            this.byteIntentos = 0;
            this.strEstatus = this.dicEstatus["ACTIVO"];            
            this.strQueryPermisosUsuario = "";
            this.strQueryCajaUsuario = "";
            this.strQueryCajaUsuarioDelete = "";
            this.strQueryPermisosUsuarioDelete = "";
            this.bolHuella = false;
            this.limpiarPropsBase();
        }

        public bool validarAutorizacion() 
        {
            bool bolValidar = false;


            if (this.strAutorizar == this.dicAutorizar["SI"]) 
            {
                bolValidar = true;
            }
            return bolValidar;
        }

        public bool validarMovimientos()
        {
            bool bolValidar = false;


            if (this.strModificarMovimientos == this.dicModificarMovimientos["SI"])
            {
                bolValidar = true;
            }
            return bolValidar;
        }
    }
}
