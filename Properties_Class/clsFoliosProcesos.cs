﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsFoliosProcesos : clsAbsBase
    {


        public clsFolios folio = new clsFolios();

        public clsProcesos procesos = new clsProcesos();

        public clsFoliosProcesos()
        {

        }

        public void limpiarProps()
        {
            this.limpiarPropsBase();
            this.folio.limpiarProps();
            this.procesos.limpiarProps();
        }
    }
}
