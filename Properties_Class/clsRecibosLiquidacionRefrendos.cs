﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsRecibosLiquidacionRefrendos : clsAbsBase
    {

        public string _strFolio;
        public DateTime _dtFechaPago;
        public string _strHora;
        public string _strTipoRecibo;
        public string _strMotivosCancelacion;
        public decimal _decTotalPagar;
        public decimal _decImportePagado;
        public string _strFotografiaImagen;
        public string _strQueryPagosDetallesCobrosTipos;
        public string _strQueryPagosDetalles;
        public string _strQueryBoletaEmpenoFechaVencimiento;
        public string _strQueryBoletaEmpenoEstatus;
        public string _strQueryCrearBoletaEmpeno;
        public string _strQueryConsecutivoFolioBoleta;
        public string _strQueryActualizarBoletasDetalles;
        public string _strQueryActualizarBoletasGeneradas;
        public int _intFolioIdBoleta;
        public int _intCountFolioBoleta;
        public string _strListaBoletasGeneradas;
        public string _strIdsBoletasGeneradas;
        public string _strIdsBoletasDetalles;
        public string _strUsuarioEliminacion;
        public string _strFechaEliminacion;

        public DateTime dtFechaPago { get => _dtFechaPago; set => _dtFechaPago = value; }
        public string strHora { get => _strHora; set => _strHora = value; }
        public string strFolio { get => _strFolio; set => _strFolio = value; }

        public string strTipoRecibo { get => _strTipoRecibo; set => _strTipoRecibo = value; }
        public string strMotivosCancelacion { get => _strMotivosCancelacion; set => _strMotivosCancelacion = value; }
        public decimal decTotalPagar { get => _decTotalPagar; set => _decTotalPagar = value; }

        public decimal decImportePagado { get => _decImportePagado; set => _decImportePagado = value; }
        
        public string strFotografiaImagen { get => _strFotografiaImagen; set => _strFotografiaImagen = value; }

        public string strQueryPagosDetallesCobrosTipos { get => _strQueryPagosDetallesCobrosTipos; set => _strQueryPagosDetallesCobrosTipos = value; }
        public string strQueryPagosDetalles { get => _strQueryPagosDetalles; set => _strQueryPagosDetalles = value; }
        public string strQueryBoletaEmpenoFechaVencimiento { get => _strQueryBoletaEmpenoFechaVencimiento; set => _strQueryBoletaEmpenoFechaVencimiento = value; }

        public string  strQueryBoletaEmpenoEstatus { get => _strQueryBoletaEmpenoEstatus; set => _strQueryBoletaEmpenoEstatus = value; }
        public string strQueryCrearBoletaEmpeno { get => _strQueryCrearBoletaEmpeno; set => _strQueryCrearBoletaEmpeno = value; }

        public string strQueryConsecutivoFolioBoleta { get => _strQueryConsecutivoFolioBoleta; set => _strQueryConsecutivoFolioBoleta = value; }

        public string strQueryActualizarBoletasDetalles { get => _strQueryActualizarBoletasDetalles; set => _strQueryActualizarBoletasDetalles = value; }

        public string strQueryActualizarBoletasGeneradas { get => _strQueryActualizarBoletasGeneradas; set => _strQueryActualizarBoletasGeneradas = value; }

        public int intFolioIdBoleta { get => _intFolioIdBoleta; set => _intFolioIdBoleta = value; }
        public int intCountFolioBoleta { get => _intCountFolioBoleta; set => _intCountFolioBoleta = value; }

        public string strListaBoletasGeneradas { get => _strListaBoletasGeneradas; set => _strListaBoletasGeneradas = value; }

        public string strIdsBoletasGeneradas { get => _strIdsBoletasGeneradas; set => _strIdsBoletasGeneradas = value; }

        public string strIdsBoletasDetalles { get => _strIdsBoletasDetalles; set => _strIdsBoletasDetalles = value; }

        public string strUsuarioEliminacion { get => _strUsuarioEliminacion; set => _strUsuarioEliminacion = value; }
        public string strFechaEliminacion { get => _strFechaEliminacion; set => _strFechaEliminacion = value; }




        public clsUsuario empleado = new clsUsuario();
        public clsCliente cliente = new clsCliente();
        public clsFormPago formPago = new clsFormPago();
        public clsCaja caja = new clsCaja();
        public clsUsuario usuarioCreacion = new clsUsuario();        
        public clsUsuario usuarioActulizacion = new clsUsuario();
        public clsUsuario usuarioAutorizacion = new clsUsuario();

        public clsRecibosLiquidacionRefrendos()
        {
            this.intNumCerosConsecutivo = 10;
            this.strDescripcion = "";
            this.strFolio = "";            
            this.dtFechaPago = DateTime.Now;
            this.strHora = "";

            this.strTipoRecibo = "";
            this.strMotivosCancelacion = "";
            this.decTotalPagar = 0;
            this.decImportePagado = 0;
            this.strFotografiaImagen = "";
            this.strQueryPagosDetallesCobrosTipos = "";
            this.strQueryPagosDetalles = "";
            this.strQueryCrearBoletaEmpeno = "";
            this.strQueryActualizarBoletasDetalles = "";
            this.strQueryActualizarBoletasGeneradas = "";
            this.strQueryBoletaEmpenoFechaVencimiento = "";
            this.intCountFolioBoleta = 0;
            this.intFolioIdBoleta = 0;
            this.strQueryConsecutivoFolioBoleta = " UPDATE folios " +
                    " SET " +
                    " consecutivo = (consecutivo + 1), " +
                    " fecha_actualizacion = @fecha, " +
                    " usuario_actualizacion = @usuario " +
                    " WHERE folio_id = @folio_id_boleta";

            this.strEstatus = this.dicEstatus["ACTIVO"];
            this.strListaBoletasGeneradas = "";
            this.strIdsBoletasGeneradas = "";
            this.strIdsBoletasDetalles =  "";
            this.strUsuarioEliminacion = "";
            this.strFechaEliminacion = "";
    }

        /*public string consecutivoFolioBoleta() 
        {

            return this.strQueryConsecutivoFolioBoleta = " UPDATE folios " +
                    " SET " +
                    " consecutivo = (consecutivo +" + this.intCountFolioBoleta + "), " +
                    " fecha_actualizacion = @fecha, " +
                    " usuario_actualizacion = @usuario " +
                    " WHERE folio_id = @folio_id_boleta";
        }*/


        public void limpiarProps()
        {

           
            this.strFolio = "";          
            this.dtFechaPago = DateTime.Now;
            this.strHora = "";
            this.strTipoRecibo = "";
            this.strMotivosCancelacion = "";
            this.decTotalPagar = 0;
            this.decImportePagado = 0;
            this.strFotografiaImagen = "";
            this.strQueryPagosDetallesCobrosTipos = "";
            this.strQueryPagosDetalles = "";
            this.strQueryCrearBoletaEmpeno = "";
            this.strQueryActualizarBoletasDetalles = "";
            this.strQueryActualizarBoletasGeneradas = "";
            this.strQueryBoletaEmpenoFechaVencimiento = "";
            this.intCountFolioBoleta = 0;
            this.intFolioIdBoleta = 0;
            this.strListaBoletasGeneradas = "";
            this.strIdsBoletasGeneradas = "";
            this.strIdsBoletasDetalles = "";
            this.strUsuarioEliminacion = "";
            this.strFechaEliminacion = "";

            this.limpiarPropsBase();
            this.cliente.limpiarProps();
            this.formPago.limpiarProps();
            this.empleado.limpiarProps();           
            this.caja.limpiarProps();
            this.usuarioCreacion.limpiarProps();
            this.usuarioActulizacion.limpiarProps();
            this.usuarioAutorizacion.limpiarProps();
        }
    }
}
