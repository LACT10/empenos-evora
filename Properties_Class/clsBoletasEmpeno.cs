﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsBoletasEmpeno : clsAbsBase
    {

        public string _strFolio;
        public string _strBoletaInicial;
        public int _intBoletaInicialId;
        public DateTime _dtFechaBoleta;
        public string _strHora;
        public int _intPlazo;
        public DateTime _dtFechaVencimiento;        
        public decimal _decPrestamo;
        public decimal _decImporteAvaluo;
        public int _intCantidad;
        public string _strComentario;        
        public float _flKilataje;
        public float _flGramos;
        public decimal _decPPI;
        public string _strFotografiaImagen;
        public int _intReferenciaId;
        public string _strPagoFolio;
        public string _strPagoFecha;
        public string _strBoletaRefrendo;
        public string _strBoletaRefrendada;
        public string _strIMEICelular;
        public string _strNumeroSerieVehiculo;
        public string _strTipoSerieVehiculo;
        public string _strMotivosCancelacion;
        public string _strUsuarioEliminacion;
        public string _strFechaEliminacion;

        public string strFolio { get => _strFolio; set => _strFolio = value; }
        public string strBoletaInicial { get => _strBoletaInicial; set => _strBoletaInicial = value; }
        public int intBoletaInicialId { get => _intBoletaInicialId; set => _intBoletaInicialId = value; }
        public DateTime dtFechaBoleta { get => _dtFechaBoleta; set => _dtFechaBoleta = value; }
        public string strHora { get => _strHora; set => _strHora = value; }
        public int intPlazo { get => _intPlazo; set => _intPlazo = value; }
        public DateTime dtFechaVencimiento { get => _dtFechaVencimiento; set => _dtFechaVencimiento = value; }        
        public decimal decPrestamo { get => _decPrestamo; set => _decPrestamo = value; }
        public decimal decImporteAvaluo { get => _decImporteAvaluo; set => _decImporteAvaluo = value; }
        public int intCantidad { get => _intCantidad; set => _intCantidad = value; }
        public string strComentario { get => _strComentario; set => _strComentario = value; }
        public float flKilataje { get => _flKilataje; set => _flKilataje = value; }
        public float flGramos { get => _flGramos; set => _flGramos = value; }

        public int intReferenciaId { get => _intReferenciaId; set => _intReferenciaId = value; }

        public decimal decPPI { get => _decPPI; set => _decPPI = value; }
        public string strFotografiaImagen { get => _strFotografiaImagen; set => _strFotografiaImagen = value; }

        public string strPagoFolio { get => _strPagoFolio; set => _strPagoFolio = value; }
        public string strPagoFecha { get => _strPagoFecha; set => _strPagoFecha = value; }
        public string strBoletaRefrendo { get => _strBoletaRefrendo; set => _strBoletaRefrendo = value; }
        public string strBoletaRefrendada { get => _strBoletaRefrendada; set => _strBoletaRefrendada = value; }

        public string strIMEICelular { get => _strIMEICelular; set => _strIMEICelular = value; }
        public string strNumeroSerieVehiculo { get => _strNumeroSerieVehiculo; set => _strNumeroSerieVehiculo = value; }       

        public string strTipoSerieVehiculo { get => _strTipoSerieVehiculo; set => _strTipoSerieVehiculo = value; }

        public string strMotivosCancelacion { get => _strMotivosCancelacion; set => _strMotivosCancelacion = value; }

        public string strUsuarioEliminacion { get => _strUsuarioEliminacion; set => _strUsuarioEliminacion = value; }
        public string strFechaEliminacion { get => _strFechaEliminacion; set => _strFechaEliminacion = value; }



        public clsUsuario empleado = new clsUsuario();
        public clsCliente cliente = new clsCliente();
        public clsPrendasTipo prendaTipo = new clsPrendasTipo();
        public clsPrendasSubtipo subTipoPrenda = new clsPrendasSubtipo();
        public clsIdentificacionesTipo identificacionesTipo = new clsIdentificacionesTipo();
        public clsGramaje gramaje = new clsGramaje();
        public clsSucursalGramaje sucursalGramaje = new clsSucursalGramaje();
        public clsSucursalPlazo sucursalPlazo = new clsSucursalPlazo();
        public clsCaja caja = new clsCaja();
        public clsUsuario usuarioCreacion = new clsUsuario();
        public clsUsuario usuarioActulizacion = new clsUsuario();
        public clsUsuario usuarioAutorizacion = new clsUsuario();

        public clsBoletasEmpeno() 
        {
            this.intNumCerosConsecutivo = 10;
            this.strDescripcion = "";
            this.strFolio =  "";
            this.strBoletaInicial = "";
            this.dtFechaBoleta = DateTime.Now;
            this.strHora = "";
            this.intPlazo = 0;
            this.dtFechaVencimiento = DateTime.Now;            
            this.decPrestamo = 0;
            this.decImporteAvaluo = 0;
            this.intCantidad = 0;
            this.strComentario = "";
            this.flKilataje = 0;
            this.flGramos = 0;
            this.decPPI = 0;
            this.strFotografiaImagen = "";
            this.intReferenciaId = 0;
            this.strPagoFolio = "";
            this.strPagoFecha = "";
            this.strBoletaRefrendo = "";
            this.strBoletaRefrendada = "";
            this.strIMEICelular = "";
            this.strNumeroSerieVehiculo = "";
            this.strTipoSerieVehiculo = "";
            this.strMotivosCancelacion = "";
            this.strUsuarioEliminacion = "";
            this.strFechaEliminacion = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];
        }

        public void limpiarProps()
        {
            
            this.strDescripcion = "";
            this.strFolio = "";
            this.strBoletaInicial = "";
            this.dtFechaBoleta = DateTime.Now;
            this.strHora = "";
            this.intPlazo = 0;
            this.dtFechaVencimiento = DateTime.Now;            
            this.decPrestamo = 0;
            this.decImporteAvaluo = 0;
            this.intCantidad = 0;
            this.strComentario = "";
            this.flKilataje = 0;
            this.flGramos = 0;
            this.decPPI = 0;
            this.strFotografiaImagen = "";
            this.intReferenciaId = 0;
            this.strEstatus = this.dicEstatus["ACTIVO"];
            this.strPagoFolio = "";
            this.strPagoFecha = "";
            this.strBoletaRefrendo = "";
            this.strBoletaRefrendada = "";
            this.strIMEICelular = "";
            this.strNumeroSerieVehiculo = "";
            this.strTipoSerieVehiculo = "";
            this.strMotivosCancelacion = "";
            this.strUsuarioEliminacion = "";
            this.strFechaEliminacion = "";

            this.limpiarPropsBase();
            this.cliente.limpiarProps();
            this.empleado.limpiarProps();
            this.prendaTipo.limpiarProps();
            this.subTipoPrenda.limpiarProps();
            this.identificacionesTipo.limpiarProps();
            this.gramaje.limpiarProps();
            this.sucursalGramaje.limpiarProps();
            this.sucursalPlazo.limpiarProps();
            this.caja.limpiarProps();
            this.usuarioCreacion.limpiarProps();
            this.usuarioActulizacion.limpiarProps();
            this.usuarioAutorizacion.limpiarProps();
    }
        
    }
}