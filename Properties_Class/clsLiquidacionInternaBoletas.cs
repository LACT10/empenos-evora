﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsLiquidacionInternaBoletas : clsAbsBase
    {

        public string _strFolio;
        public DateTime _dtFechaInterna;
        public string _strHora;
        public string _strFormaCalculoIntereses;    
        public decimal _decTotalPagar;
        public decimal _decImportePagado;

        public string _strQueryPagosDetallesCobrosTipos;
        public string _strQueryPagosDetalles;
        public string _strQueryBoletaEmpenoFechaVencimiento;
        public string _strQueryBoletaEmpenoEstatus;
        public string _strQueryCrearBoletaEmpeno;
        public string _strQueryConsecutivoFolioBoleta;
        public string _strQueryActualizarBoletasDetalles;
        public string _strQueryActualizarBoletasGeneradas;
        public int _intFolioIdPago;
        public int _intCountFolioBoleta;
        public string _strListaBoletasGeneradas;
        public string _strIdsBoletasGeneradas;
        public string _strIdsBoletasDetalles;
        public List<string> _lsQuerysTraccionesPagos;
        public List<string> _lsQuerysTraccionesPagosDetalles;
        public List<string> _lsQuerysTraccionesPagosDetallesCobrosTipos;


        public DateTime dtFechaInterna { get => _dtFechaInterna; set => _dtFechaInterna = value; }
        public string strHora { get => _strHora; set => _strHora = value; }
        public string strFolio { get => _strFolio; set => _strFolio = value; }
        public string strFormaCalculoIntereses { get => _strFormaCalculoIntereses; set => _strFormaCalculoIntereses = value; }
        public decimal decTotalPagar { get => _decTotalPagar; set => _decTotalPagar = value; }

        public decimal decImportePagado { get => _decImportePagado; set => _decImportePagado = value; }
         
        public string strQueryPagosDetallesCobrosTipos { get => _strQueryPagosDetallesCobrosTipos; set => _strQueryPagosDetallesCobrosTipos = value; }
        public string strQueryPagosDetalles { get => _strQueryPagosDetalles; set => _strQueryPagosDetalles = value; }
        public string strQueryBoletaEmpenoFechaVencimiento { get => _strQueryBoletaEmpenoFechaVencimiento; set => _strQueryBoletaEmpenoFechaVencimiento = value; }

        public string strQueryBoletaEmpenoEstatus { get => _strQueryBoletaEmpenoEstatus; set => _strQueryBoletaEmpenoEstatus = value; }
        public string strQueryCrearBoletaEmpeno { get => _strQueryCrearBoletaEmpeno; set => _strQueryCrearBoletaEmpeno = value; }

        public string strQueryConsecutivoFolioBoleta { get => _strQueryConsecutivoFolioBoleta; set => _strQueryConsecutivoFolioBoleta = value; }

        public string strQueryActualizarBoletasDetalles { get => _strQueryActualizarBoletasDetalles; set => _strQueryActualizarBoletasDetalles = value; }

        public string strQueryActualizarBoletasGeneradas { get => _strQueryActualizarBoletasGeneradas; set => _strQueryActualizarBoletasGeneradas = value; }

        public int intFolioIdPago { get => _intFolioIdPago; set => _intFolioIdPago = value; }
        public int intCountFolioBoleta { get => _intCountFolioBoleta; set => _intCountFolioBoleta = value; }

        public string strListaBoletasGeneradas { get => _strListaBoletasGeneradas; set => _strListaBoletasGeneradas = value; }

        public string strIdsBoletasGeneradas { get => _strIdsBoletasGeneradas; set => _strIdsBoletasGeneradas = value; }

        public string strIdsBoletasDetalles { get => _strIdsBoletasDetalles; set => _strIdsBoletasDetalles = value; }


        public List<string> lsQuerysTraccionesPagos { get => _lsQuerysTraccionesPagos; set => _lsQuerysTraccionesPagos = value; }

        public List<string> lsQuerysTraccionesPagosDetalles { get => _lsQuerysTraccionesPagosDetalles; set => _lsQuerysTraccionesPagosDetalles = value; }

        public List<string> lsQuerysTraccionesPagosDetallesCobrosTipos { get => _lsQuerysTraccionesPagosDetallesCobrosTipos; set => _lsQuerysTraccionesPagosDetallesCobrosTipos = value; }

        

        public clsUsuario empleado = new clsUsuario();
        public clsCliente cliente = new clsCliente();
        public clsFormPago formPago = new clsFormPago();
        public clsCaja caja = new clsCaja();
        public clsUsuario usuarioCreacion = new clsUsuario();
        public clsUsuario usuarioActulizacion = new clsUsuario();
        public clsUsuario usuarioAutorizacion = new clsUsuario();
        public clsRecibosLiquidacionRefrendos pagos = new clsRecibosLiquidacionRefrendos();

        public clsLiquidacionInternaBoletas()
        {
            this.intNumCerosConsecutivo = 10;
            this.strDescripcion = "";
            this.strFolio = "";
            this.dtFechaInterna = DateTime.Now;
            this.strHora = "";
            this.strFormaCalculoIntereses = "";
            this.decTotalPagar = 0;
            this.decImportePagado = 0;            
            this.strQueryPagosDetallesCobrosTipos = "";
            this.strQueryPagosDetalles = "";
            this.strQueryCrearBoletaEmpeno = "";
            this.strQueryActualizarBoletasDetalles = "";
            this.strQueryActualizarBoletasGeneradas = "";
            this.strQueryBoletaEmpenoFechaVencimiento = "";
            this.intCountFolioBoleta = 0;
            this.intFolioIdPago = 0;
            this.strQueryConsecutivoFolioBoleta = " UPDATE folios " +
                    " SET " +
                    " consecutivo = (consecutivo + 1), " +
                    " fecha_actualizacion = @fecha, " +
                    " usuario_actualizacion = @usuario " +
                    " WHERE folio_id = @folio_id_boleta";

            this.strEstatus = this.dicEstatus["ACTIVO"];
            this.strListaBoletasGeneradas = "";
            this.strIdsBoletasGeneradas = "";
            this.strIdsBoletasDetalles = "";            
        }

        /*public string consecutivoFolioBoleta() 
        {

            return this.strQueryConsecutivoFolioBoleta = " UPDATE folios " +
                    " SET " +
                    " consecutivo = (consecutivo +" + this.intCountFolioBoleta + "), " +
                    " fecha_actualizacion = @fecha, " +
                    " usuario_actualizacion = @usuario " +
                    " WHERE folio_id = @folio_id_boleta";
        }*/


        public void limpiarProps()
        {


            this.strFolio = "";
            this._dtFechaInterna = DateTime.Now;
            this.strHora = "";
            this.strFormaCalculoIntereses = "";
            this.decTotalPagar = 0;
            this.decImportePagado = 0;            
            this.strQueryPagosDetallesCobrosTipos = "";
            this.strQueryPagosDetalles = "";
            this.strQueryCrearBoletaEmpeno = "";
            this.strQueryActualizarBoletasDetalles = "";
            this.strQueryActualizarBoletasGeneradas = "";
            this.strQueryBoletaEmpenoFechaVencimiento = "";
            this.intCountFolioBoleta = 0;
            this.intFolioIdPago = 0;
            this.strListaBoletasGeneradas = "";
            this.strIdsBoletasGeneradas = "";
            this.strIdsBoletasDetalles = "";            

            this.limpiarPropsBase();
            this.cliente.limpiarProps();
            this.formPago.limpiarProps();
            this.empleado.limpiarProps();
            this.caja.limpiarProps();
            this.usuarioCreacion.limpiarProps();
            this.usuarioActulizacion.limpiarProps();
            this.usuarioAutorizacion.limpiarProps();
        }
    }
}
