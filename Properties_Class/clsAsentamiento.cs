﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsAsentamiento : clsAbsBase
    {

        private string _strCiudad;
        private string _strCodigoPostal;
        
        public string strCiudad { get => _strCiudad; set => _strCiudad = value; }
        public string strCodigoPostal { get => _strCodigoPostal; set => _strCodigoPostal = value; }

        public clsMunicipio municipio = new clsMunicipio();
        
        public clsAsentamientosTipos asentamientosTipos = new clsAsentamientosTipos();
        
        public clsAsentamiento()
        {
            this.strCiudad = "";
            this.strCodigoPostal = "";
            this.strDescripcion = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];
        }

        public void limpiarProps()
        {
            this.strCiudad = "";
            this.strCodigoPostal = "";
            this.strDescripcion = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];

            this.limpiarPropsBase();
            this.municipio.limpiarProps();            
            this.asentamientosTipos.limpiarProps();
            
        }
    }
}
