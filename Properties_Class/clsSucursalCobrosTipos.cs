﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsSucursalCobrosTipos : clsAbsBase
    {
        
        private float _ftPorcentaje;
        public float ftPorcentaje { get => _ftPorcentaje; set => _ftPorcentaje = value; }
        public clsSucursal sucursal = new clsSucursal();
        public clsCobrosTipos cobrosTipos = new clsCobrosTipos();

        public clsSucursalCobrosTipos()
        {
            this.ftPorcentaje = 0;
            this.strEstatus = this.dicEstatus["ACTIVO"];
        }

        public void limpiarProps()
        {
            this.ftPorcentaje = 0;
            this.strEstatus = this.dicEstatus["ACTIVO"];

            this.limpiarPropsBase();
            this.sucursal.limpiarProps();
            this.cobrosTipos.limpiarProps();
        }
    }
}
