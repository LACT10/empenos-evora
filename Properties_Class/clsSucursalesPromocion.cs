﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
    public class clsSucursalesPromocion : clsAbsBase
    {
        
        private decimal _decPorcentaje;
        private string _strDia;
        private decimal _decDiaMin;
        private string _strQueryDetalles;

        public decimal decPorcentaje { get => _decPorcentaje; set => _decPorcentaje = value; }

        public string strDia { get => _strDia; set => _strDia = value; }
        public decimal decDiaMin { get => _decDiaMin; set => _decDiaMin = value; }

        public string strQueryDetalles { get => _strQueryDetalles; set => _strQueryDetalles = value; }

        

        public clsSucursal sucursal = new clsSucursal();

        public clsPrendasTipo prendasTipo = new clsPrendasTipo();


        public clsSucursalesPromocion()
        {

            this.strDia = "";
            this.decPorcentaje = 0;
            this.strQueryDetalles = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];
        }

        public void limpiarProps()
        {
            this.strDia = "";
            this.decPorcentaje = 0;
            this.strQueryDetalles = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];

            this.limpiarPropsBase();
            this.sucursal.limpiarProps();
            this.prendasTipo.limpiarProps();
        }

    }
}
