﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpenosEvora.Properties_Class
{
   public class clsCaja : clsAbsBase
    {

        private string _strCodigo;
        public string strCodigo { get => _strCodigo; set => _strCodigo = value; }

        public clsCaja()
        {
            this.intNumCerosConsecutivo = 2;
            this.strCodigo = "";
            this.strDescripcion = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];
        }

        public void limpiarProps()
        {            
            this.strCodigo = "";
            this.strDescripcion = "";
            this.strEstatus = this.dicEstatus["ACTIVO"];

            this.limpiarPropsBase();            
        }

        public void agregarCero()
        {
            this.strNumCero = this.strCodigo;
            this.intNumCero = 2;
            this.agregarZero();
            this.strCodigo = this.strNumCero;
        }
    }
}
