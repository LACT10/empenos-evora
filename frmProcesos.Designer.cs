﻿namespace EmpenosEvora
{
    partial class frmProcesos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtProcesosPadreId = new System.Windows.Forms.TextBox();
            this.tsAcciones = new System.Windows.Forms.ToolStrip();
            this.tsbNuevo = new System.Windows.Forms.ToolStripButton();
            this.tsbGuardar = new System.Windows.Forms.ToolStripButton();
            this.tsbEliminar = new System.Windows.Forms.ToolStripButton();
            this.tsbBuscar = new System.Windows.Forms.ToolStripButton();
            this.btnUndo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrimeroPag = new System.Windows.Forms.ToolStripButton();
            this.btnAtras = new System.Windows.Forms.ToolStripButton();
            this.btnAdelante = new System.Windows.Forms.ToolStripButton();
            this.btnUlitmoPag = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.btnCierra = new System.Windows.Forms.ToolStripButton();
            this.txtDescripcionProcesosPadre = new System.Windows.Forms.TextBox();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNombreMenu = new System.Windows.Forms.TextBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.btnBusqueda = new System.Windows.Forms.Button();
            this.btnFooterCierra = new System.Windows.Forms.Button();
            this.btnBusquedaPadre = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvSubProcesosProceso = new System.Windows.Forms.DataGridView();
            this.label15 = new System.Windows.Forms.Label();
            this.btnFooterGuardar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tsAcciones.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSubProcesosProceso)).BeginInit();
            this.SuspendLayout();
            // 
            // txtProcesosPadreId
            // 
            this.txtProcesosPadreId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtProcesosPadreId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtProcesosPadreId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProcesosPadreId.Location = new System.Drawing.Point(600, 72);
            this.txtProcesosPadreId.MaxLength = 3;
            this.txtProcesosPadreId.Name = "txtProcesosPadreId";
            this.txtProcesosPadreId.Size = new System.Drawing.Size(44, 20);
            this.txtProcesosPadreId.TabIndex = 545;
            this.txtProcesosPadreId.Visible = false;
            // 
            // tsAcciones
            // 
            this.tsAcciones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbNuevo,
            this.tsbGuardar,
            this.tsbEliminar,
            this.tsbBuscar,
            this.btnUndo,
            this.toolStripSeparator2,
            this.btnPrimeroPag,
            this.btnAtras,
            this.btnAdelante,
            this.btnUlitmoPag,
            this.toolStripSeparator1,
            this.toolStripButton3,
            this.btnCierra});
            this.tsAcciones.Location = new System.Drawing.Point(0, 0);
            this.tsAcciones.Name = "tsAcciones";
            this.tsAcciones.Size = new System.Drawing.Size(740, 25);
            this.tsAcciones.TabIndex = 544;
            this.tsAcciones.Text = "toolStrip2";
            // 
            // tsbNuevo
            // 
            this.tsbNuevo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbNuevo.Image = global::EmpenosEvora.Properties.Resources.AddFile_16x;
            this.tsbNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbNuevo.Name = "tsbNuevo";
            this.tsbNuevo.Size = new System.Drawing.Size(23, 22);
            this.tsbNuevo.Text = "Nuevo";
            this.tsbNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical90;
            this.tsbNuevo.Click += new System.EventHandler(this.TsbNuevo_Click);
            // 
            // tsbGuardar
            // 
            this.tsbGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbGuardar.Image = global::EmpenosEvora.Properties.Resources.Save_16x;
            this.tsbGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGuardar.Name = "tsbGuardar";
            this.tsbGuardar.Size = new System.Drawing.Size(23, 22);
            this.tsbGuardar.Tag = "";
            this.tsbGuardar.Text = "Guardar";
            this.tsbGuardar.Click += new System.EventHandler(this.TsbGuardar_Click);
            // 
            // tsbEliminar
            // 
            this.tsbEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbEliminar.Image = global::EmpenosEvora.Properties.Resources.Cancel_16x;
            this.tsbEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEliminar.Name = "tsbEliminar";
            this.tsbEliminar.Size = new System.Drawing.Size(23, 22);
            this.tsbEliminar.Text = "Desactivar";
            this.tsbEliminar.ToolTipText = "Desactivar";
            this.tsbEliminar.Click += new System.EventHandler(this.TsbEliminar_Click);
            // 
            // tsbBuscar
            // 
            this.tsbBuscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBuscar.Image = global::EmpenosEvora.Properties.Resources.VBSearch_16x;
            this.tsbBuscar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBuscar.Name = "tsbBuscar";
            this.tsbBuscar.Size = new System.Drawing.Size(23, 22);
            this.tsbBuscar.Text = "Buscar";
            this.tsbBuscar.Click += new System.EventHandler(this.TsbBuscar_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUndo.Image = global::EmpenosEvora.Properties.Resources.Undo_16x;
            this.btnUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(23, 22);
            this.btnUndo.Text = "Deshacer";
            this.btnUndo.Click += new System.EventHandler(this.BtnUndo_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrimeroPag
            // 
            this.btnPrimeroPag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrimeroPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeftGroup_16xSM;
            this.btnPrimeroPag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrimeroPag.Name = "btnPrimeroPag";
            this.btnPrimeroPag.Size = new System.Drawing.Size(23, 22);
            this.btnPrimeroPag.Text = "Primero";
            this.btnPrimeroPag.Click += new System.EventHandler(this.BtnPrimeroPag_Click);
            // 
            // btnAtras
            // 
            this.btnAtras.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAtras.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronLeft_16x;
            this.btnAtras.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(23, 22);
            this.btnAtras.Text = "Atras";
            this.btnAtras.Click += new System.EventHandler(this.BtnAtras_Click);
            // 
            // btnAdelante
            // 
            this.btnAdelante.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdelante.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronRight_16x;
            this.btnAdelante.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdelante.Name = "btnAdelante";
            this.btnAdelante.Size = new System.Drawing.Size(23, 22);
            this.btnAdelante.Text = "Siguente";
            this.btnAdelante.Click += new System.EventHandler(this.BtnAdelante_Click);
            // 
            // btnUlitmoPag
            // 
            this.btnUlitmoPag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUlitmoPag.Image = global::EmpenosEvora.Properties.Resources.CollapseChevronRightGroup_16x;
            this.btnUlitmoPag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUlitmoPag.Name = "btnUlitmoPag";
            this.btnUlitmoPag.Size = new System.Drawing.Size(23, 22);
            this.btnUlitmoPag.Text = "Ultimo";
            this.btnUlitmoPag.Click += new System.EventHandler(this.BtnUlitmoPag_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::EmpenosEvora.Properties.Resources.Print_16x;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "Imprimir";
            this.toolStripButton3.ToolTipText = "Imprimir";
            // 
            // btnCierra
            // 
            this.btnCierra.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnCierra.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCierra.Name = "btnCierra";
            this.btnCierra.Size = new System.Drawing.Size(23, 22);
            this.btnCierra.Text = "Salir";
            this.btnCierra.Click += new System.EventHandler(this.BtnCierra_Click);
            // 
            // txtDescripcionProcesosPadre
            // 
            this.txtDescripcionProcesosPadre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcionProcesosPadre.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcionProcesosPadre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcionProcesosPadre.Location = new System.Drawing.Point(96, 72);
            this.txtDescripcionProcesosPadre.MaxLength = 250;
            this.txtDescripcionProcesosPadre.Name = "txtDescripcionProcesosPadre";
            this.txtDescripcionProcesosPadre.Size = new System.Drawing.Size(584, 20);
            this.txtDescripcionProcesosPadre.TabIndex = 4;
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(8, 40);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(63, 13);
            this.lblCodigo.TabIndex = 543;
            this.lblCodigo.Text = "Descripción";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcion.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcion.Location = new System.Drawing.Point(96, 40);
            this.txtDescripcion.MaxLength = 50;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(184, 20);
            this.txtDescripcion.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 546;
            this.label1.Text = "Proceso padre";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(368, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 548;
            this.label2.Text = "Nombre de menú";
            // 
            // txtNombreMenu
            // 
            this.txtNombreMenu.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNombreMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreMenu.Location = new System.Drawing.Point(472, 40);
            this.txtNombreMenu.MaxLength = 50;
            this.txtNombreMenu.Name = "txtNombreMenu";
            this.txtNombreMenu.Size = new System.Drawing.Size(248, 20);
            this.txtNombreMenu.TabIndex = 3;
            // 
            // txtId
            // 
            this.txtId.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtId.Location = new System.Drawing.Point(232, 40);
            this.txtId.MaxLength = 3;
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(44, 20);
            this.txtId.TabIndex = 554;
            this.txtId.Visible = false;
            // 
            // btnBusqueda
            // 
            this.btnBusqueda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBusqueda.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusqueda.Location = new System.Drawing.Point(288, 40);
            this.btnBusqueda.Name = "btnBusqueda";
            this.btnBusqueda.Size = new System.Drawing.Size(32, 23);
            this.btnBusqueda.TabIndex = 2;
            this.btnBusqueda.UseVisualStyleBackColor = true;
            this.btnBusqueda.Click += new System.EventHandler(this.btnBusqueda_Click);
            // 
            // btnFooterCierra
            // 
            this.btnFooterCierra.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterCierra.Image = global::EmpenosEvora.Properties.Resources.Exit_16x;
            this.btnFooterCierra.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterCierra.Location = new System.Drawing.Point(648, 273);
            this.btnFooterCierra.Name = "btnFooterCierra";
            this.btnFooterCierra.Size = new System.Drawing.Size(75, 40);
            this.btnFooterCierra.TabIndex = 7;
            this.btnFooterCierra.Text = "Salir";
            this.btnFooterCierra.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterCierra.UseVisualStyleBackColor = true;
            this.btnFooterCierra.Click += new System.EventHandler(this.btnFooterCierra_Click);
            // 
            // btnBusquedaPadre
            // 
            this.btnBusquedaPadre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBusquedaPadre.Image = global::EmpenosEvora.Properties.Resources.Search_16x;
            this.btnBusquedaPadre.Location = new System.Drawing.Point(688, 72);
            this.btnBusquedaPadre.Name = "btnBusquedaPadre";
            this.btnBusquedaPadre.Size = new System.Drawing.Size(32, 23);
            this.btnBusquedaPadre.TabIndex = 5;
            this.btnBusquedaPadre.UseVisualStyleBackColor = true;
            this.btnBusquedaPadre.Click += new System.EventHandler(this.btnBusquedaPadre_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvSubProcesosProceso);
            this.groupBox1.Location = new System.Drawing.Point(8, 104);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(712, 144);
            this.groupBox1.TabIndex = 556;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Subprocesos";
            // 
            // dgvSubProcesosProceso
            // 
            this.dgvSubProcesosProceso.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSubProcesosProceso.Location = new System.Drawing.Point(16, 24);
            this.dgvSubProcesosProceso.Name = "dgvSubProcesosProceso";
            this.dgvSubProcesosProceso.Size = new System.Drawing.Size(680, 104);
            this.dgvSubProcesosProceso.TabIndex = 561;
            this.dgvSubProcesosProceso.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSubProcesosProceso_CellClick);
            this.dgvSubProcesosProceso.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSubProcesosProceso_CellContentClick);
            this.dgvSubProcesosProceso.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvSubProcesosProceso_KeyDown);
            this.dgvSubProcesosProceso.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvSubProcesosProceso_KeyPress);
            // 
            // label15
            // 
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(-258, 240);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(974, 2);
            this.label15.TabIndex = 551;
            // 
            // btnFooterGuardar
            // 
            this.btnFooterGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooterGuardar.Image = global::EmpenosEvora.Properties.Resources.Save_16x;
            this.btnFooterGuardar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFooterGuardar.Location = new System.Drawing.Point(568, 273);
            this.btnFooterGuardar.Name = "btnFooterGuardar";
            this.btnFooterGuardar.Size = new System.Drawing.Size(75, 40);
            this.btnFooterGuardar.TabIndex = 6;
            this.btnFooterGuardar.Text = "Guardar";
            this.btnFooterGuardar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFooterGuardar.UseVisualStyleBackColor = true;
            this.btnFooterGuardar.Click += new System.EventHandler(this.btnFooterGuardar_Click);
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(-119, 256);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(974, 2);
            this.label3.TabIndex = 557;
            // 
            // frmProcesos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(216)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(740, 318);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnFooterGuardar);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.btnBusqueda);
            this.Controls.Add(this.btnFooterCierra);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNombreMenu);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtProcesosPadreId);
            this.Controls.Add(this.tsAcciones);
            this.Controls.Add(this.btnBusquedaPadre);
            this.Controls.Add(this.txtDescripcionProcesosPadre);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.txtDescripcion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProcesos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Procesos";
            this.Load += new System.EventHandler(this.frmProcesos_Load);
            this.tsAcciones.ResumeLayout(false);
            this.tsAcciones.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSubProcesosProceso)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtProcesosPadreId;
        private System.Windows.Forms.ToolStrip tsAcciones;
        private System.Windows.Forms.ToolStripButton tsbNuevo;
        private System.Windows.Forms.ToolStripButton tsbGuardar;
        private System.Windows.Forms.ToolStripButton tsbEliminar;
        private System.Windows.Forms.ToolStripButton tsbBuscar;
        private System.Windows.Forms.ToolStripButton btnUndo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnPrimeroPag;
        private System.Windows.Forms.ToolStripButton btnAtras;
        private System.Windows.Forms.ToolStripButton btnAdelante;
        private System.Windows.Forms.ToolStripButton btnUlitmoPag;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton btnCierra;
        private System.Windows.Forms.Button btnBusquedaPadre;
        private System.Windows.Forms.TextBox txtDescripcionProcesosPadre;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNombreMenu;
        private System.Windows.Forms.Button btnFooterCierra;
        private System.Windows.Forms.Button btnBusqueda;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnFooterGuardar;
        private System.Windows.Forms.DataGridView dgvSubProcesosProceso;
        private System.Windows.Forms.Label label3;
    }
}