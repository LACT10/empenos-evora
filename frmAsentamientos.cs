﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmpenosEvora
{
    public partial class frmAsentamientos : Form
    {
       
        public Controller.clsCtrlAsentamiento ctrlAsentamiento;
        public Controller.clsCtrlMunicipio ctrlMunicipio;
        public Controller.clsCtrlEstado ctrlEstado;
        public Controller.clsCtrlAsentamientosTipos ctrlAsentamientosTipos;
        public Core.clsCoreView clsView;

        public int _intUlitmoCodigo;
        public int intUlitmoCodigo { get => _intUlitmoCodigo; set => _intUlitmoCodigo = value; }

        public List<Properties_Class.clsAsentamiento> lsDeshacerAsentamiento;



        public frmAsentamientos()
        {
            InitializeComponent();
            this.clsView = new Core.clsCoreView();
            this.ctrlAsentamiento = new Controller.clsCtrlAsentamiento();
            this.ctrlAsentamiento.Paginacion();

            this.ctrlMunicipio = new Controller.clsCtrlMunicipio();
            this.ctrlMunicipio.Paginacion();

            /*this.ctrlEstado = new Controller.clsCtrlEstado();
            this.ctrlEstado.Paginacion();*/

            this.ctrlAsentamientosTipos = new Controller.clsCtrlAsentamientosTipos();
            this.ctrlAsentamientosTipos.Paginacion();

            this.lsDeshacerAsentamiento = new List<Properties_Class.clsAsentamiento>();
            this.lsDeshacerAsentamiento.Add(this.ctrlAsentamiento.asentamiento);

            
            string gluis = this.ctrlAsentamiento.asentamiento.strDescripcion;

        }


        private void TsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void BtnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }
        private void BtnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        private void TsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlAsentamiento.asentamiento.intId == 0) return;

            if (this.clsView.confirmacionEliminar("Asentamiento"))
            {
                this.ctrlAsentamiento.Eliminar();
                if (this.ctrlAsentamiento.bolResultado)
                {
                    this.ctrlAsentamiento.Paginacion();
                    this.limpiar_textbox();
                    this.clsView.mensajeExitoDB("asentamiento", "elimino");
                }
            }
        }

        private void BtnUndo_Click(object sender, EventArgs e)
        {
            if (this.ctrlAsentamiento.bolResutladoPaginacion)
            {
                this.clsView.principal.epMensaje.Clear();
                txtCodigoPostal.Text = this.lsDeshacerAsentamiento[0].strCodigoPostal.ToString(); 
                txtCiudad.Text = this.lsDeshacerAsentamiento[0].strCiudad;
                txtDescripcion.Text = this.lsDeshacerAsentamiento[0].strDescripcion.ToString();
                txtDescripcionAnt.Text = this.lsDeshacerAsentamiento[0].strDescripcion.ToString();
                txAsentamientoTipoId.Text = this.lsDeshacerAsentamiento[0].asentamientosTipos.intId.ToString();
                txtCodigoAsentamientoTipo.Text = this.lsDeshacerAsentamiento[0].asentamientosTipos.strCodigo;
                txtDescripcionAsentamientoTipo.Text = this.lsDeshacerAsentamiento[0].asentamientosTipos.strDescripcion;
                txtMunicipioId.Text = this.lsDeshacerAsentamiento[0].municipio.intId.ToString();
                txtMunicipioCodigo.Text = this.lsDeshacerAsentamiento[0].municipio.strCodigo;
                txtMunicipioDescripacion.Text = this.lsDeshacerAsentamiento[0].municipio.strDescripcion;
                txtEstadoId.Text = this.lsDeshacerAsentamiento[0].municipio.estado.intId.ToString();
                txtEstadoCodigo.Text = this.lsDeshacerAsentamiento[0].municipio.estado.strCodigo.ToString();
                txtEstadoDescripcion.Text = this.lsDeshacerAsentamiento[0].municipio.estado.strDescripcion;
                

            }
        }

        private void TsbBuscar_Click(object sender, EventArgs e)
        {


            Dictionary<int, Properties_Class.clsTextbox> dicFocus = new Dictionary<int, Properties_Class.clsTextbox>()
            {
                { 0,new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcion, strBusqueda = "asentamientos"}
                },
                { 1,new Properties_Class.clsTextbox()
                                { txtGenerico = txtCiudad, strBusqueda = "asentamientos"}
                },
                { 2,new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoPostal, strBusqueda = "asentamientos"}
                },
                { 3, new Properties_Class.clsTextbox()
                                { txtGenerico = txtMunicipioCodigo, strBusqueda = "municipio"}
                },
                { 4,new Properties_Class.clsTextbox()
                                { txtGenerico = txtMunicipioDescripacion, strBusqueda = "municipio"}
                },
                { 5,new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoAsentamientoTipo, strBusqueda = "tipoAsentamientos"}
                },
                { 6,new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcionAsentamientoTipo, strBusqueda = "tipoAsentamientos"}
                },
                { 7,new Properties_Class.clsTextbox()
                                { txtGenerico = txtEstadoCodigo, strBusqueda = "municipio"}
                },
                { 8,new Properties_Class.clsTextbox()
                                { txtGenerico = txtEstadoDescripcion, strBusqueda = "municipio"}
                },
            };
            foreach (KeyValuePair<int, Properties_Class.clsTextbox> data in dicFocus)
            {
                if (data.Value.txtGenerico.Focused)
                {
                    if (data.Value.strBusqueda == "asentamientos")
                    {
                        this.mostraBusqueda();
                    }
                    else if (data.Value.strBusqueda == "tipoAsentamientos")
                    {
                        this.mostraBusquedaAsentamientoTipo();
                    }
                    else
                    {
                        this.mostraBusquedaMunicipio();
                    }
                }

            }
        }


        private void BtnBusqueda_Click(object sender, EventArgs e)
        {
            this.mostraBusqueda();
        }

        private void BtnBusquedaMunicipio_Click(object sender, EventArgs e)
        {
            this.mostraBusquedaMunicipio();
        }


        private void BtnBusquedaAsentamientoTipo_Click(object sender, EventArgs e)
        {
            this.mostraBusquedaAsentamientoTipo();
        }

        private void BtnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlAsentamiento.PrimeroPaginacion();
            this.llenar_formulario();
        }

        private void BtnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlAsentamiento.SiguentePaginacion();
            this.llenar_formulario();
        }

        private void BtnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlAsentamiento.AtrasPaginacion();
            this.llenar_formulario();
        }

        private void BtnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlAsentamiento.UltimoPaginacion();
            this.llenar_formulario();
        }
        private void TsbNuevo_Click(object sender, EventArgs e)
        {
            this.limpiar_textbox();            
            this.ctrlAsentamiento.asentamiento.limpiarProps();
            txtDescripcion.Focus();

            this.clsView.ocultarGuardar(tsAcciones, btnFooterGuardar, "NUEVO");


        }
        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {
            //txtDescripcion.Text = "";
        }

        private void TxtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);

        }
        /* private void TxtCodigo_Leave(object sender, EventArgs e)
         {
             this.ctrlAsentamiento.Buscar(txtCodigo.Text);
             this.llenar_formulario();
         }*/

        private void txtDescripcion_Leave(object sender, EventArgs e)
        {
            validarDescripcionMunicipio();
        }

        private void TxtMunicipioCodigo_Leave(object sender, EventArgs e)
        {
            this.ctrlMunicipio.Buscar(txtMunicipioCodigo.Text);
            if (!this.ctrlMunicipio.bolResultado)                
            {
                txtMunicipioId.Text = "";
                txtMunicipioCodigo.Text = "";
                txtMunicipioDescripacion.Text = "";
                txtEstadoId.Text = "";
                txtEstadoCodigo.Text = "";
                txtEstadoDescripcion.Text = "";

                return;
            };
            txtMunicipioId.Text = this.ctrlMunicipio.municipio.intId.ToString();
            txtMunicipioCodigo.Text = this.ctrlMunicipio.municipio.strCodigo;
            txtMunicipioDescripacion.Text = this.ctrlMunicipio.municipio.strDescripcion;
            txtEstadoId.Text = this.ctrlMunicipio.municipio.estado.intId.ToString();
            txtEstadoCodigo.Text = this.ctrlMunicipio.municipio.estado.strCodigo;
            txtEstadoDescripcion.Text = this.ctrlMunicipio.municipio.estado.strDescripcion;
            //Limpiar la descripcion anterior para validar municipios
            txtDescripcionAnt.Text = "";

            validarDescripcionMunicipio();
        }

        private void TxtCodigoAsentamientoTipo_Leave(object sender, EventArgs e)
        {

            this.ctrlAsentamientosTipos.Buscar(txtCodigoAsentamientoTipo.Text);
            if (!this.ctrlAsentamientosTipos.bolResultado) {
                this.limpiar_textbox();
                return;
            };
            txAsentamientoTipoId.Text = this.ctrlAsentamientosTipos.asentamientosTipo.intId.ToString();
            txtCodigoAsentamientoTipo.Text = this.ctrlAsentamientosTipos.asentamientosTipo.strCodigo;
            txtDescripcionAsentamientoTipo.Text = this.ctrlAsentamientosTipos.asentamientosTipo.strDescripcion;
        }

     

        private void TxtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloTextoVali(e);
        }

        private void TxtCodigoPostal_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);
            //Limpiar la descripcion anterior 
            txtDescripcion.Focus();
            txtDescripcionAnt.Text = "";
            this.clsView.principal.epMensaje.Clear();


        }

        private bool validacion()
        {

            bool bolValidacion = true;
            string strCiudad = txtCiudad.Text.TrimStart();
            string strCodigoPostal = txtCodigoPostal.Text.TrimStart();
            string strDescripcion = txtDescripcion.Text.TrimStart();
            string strDescripcionAsentamientoTipo = txtDescripcionAsentamientoTipo.Text.TrimStart();
            string strDescripcionMunicipio = txtMunicipioDescripacion.Text.TrimStart();

            if (strCodigoPostal == "")
            {
                txtCodigoPostal.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigoPostal, this.clsView.mensajeErrorValiForm2("El campo código postal es obligatorio, favor de ingresar un código postal"));
                bolValidacion = false;
            }


            if (strCiudad == "")
            {
                txtCiudad.Focus();
                this.clsView.principal.epMensaje.SetError(txtCiudad, this.clsView.mensajeErrorValiForm2("El campo ciudad postal es obligatorio, favor de ingresar un ciudad "));
                bolValidacion = false;
            }

            if (strDescripcion == "")
            {
                txtDescripcion.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("El campo descripción es obligatorio, favor de ingresar una descripción ") );
                bolValidacion = false;
            }

            if (strDescripcionAsentamientoTipo == "")
            {
                txtCodigoAsentamientoTipo.Focus();
                this.clsView.principal.epMensaje.SetError(txtDescripcionAsentamientoTipo, this.clsView.mensajeErrorValiForm2("El campo tipo asentamiento es obligatorio, favor de ingresar un tipo asentamiento "));
                bolValidacion = false;
            }

            if (strDescripcionMunicipio == "")
            {
                txtMunicipioCodigo.Focus();
                this.clsView.principal.epMensaje.SetError(txtMunicipioDescripacion, this.clsView.mensajeErrorValiForm2("El campo municipio es obligatorio, favor de ingresar un municipio "));
                bolValidacion = false;
            }

            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();

            if (validacion())
            {
                this.ctrlAsentamiento.asentamiento.strCiudad = txtCiudad.Text.ToString();
                this.ctrlAsentamiento.asentamiento.strCodigoPostal = txtCodigoPostal.Text.ToString();
                this.ctrlAsentamiento.asentamiento.strDescripcion = txtDescripcion.Text.ToString();
                this.ctrlAsentamiento.asentamiento.municipio.intId = Int32.Parse((txtMunicipioId.Text == "") ? "0" : txtMunicipioId.Text);
                this.ctrlAsentamiento.asentamiento.asentamientosTipos.intId = Int32.Parse((txAsentamientoTipoId.Text == "") ? "0" : txAsentamientoTipoId.Text);
                this.ctrlAsentamiento.asentamiento.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlAsentamiento.Guardar();

                if (this.ctrlAsentamiento.mdlAsentamiento.bolResultado)
                {
                    this.limpiar_textbox();
                    this.ctrlAsentamiento.Paginacion();
                    this.clsView.mensajeExitoDB("asentamiento", "guardó");

                }
            }
        }
       
        public void mostraBusqueda() 
        {
            if (this.ctrlAsentamiento.bolResutladoPaginacion)
            {
                this.ctrlAsentamiento.CargarFrmBusqueda();
                if (this.ctrlAsentamiento.bolResultadoBusqueda)
                {
                    this.llenar_formulario();
                    txtDescripcion.Focus();
                }
            }
            
        }

        public void mostraBusquedaMunicipio() 
        {
            this.ctrlMunicipio.Paginacion();
            if (this.ctrlMunicipio.bolResutladoPaginacion)
            {
                ctrlMunicipio.CargarFrmBusqueda();
                if (this.ctrlMunicipio.bolResultadoBusqueda)
                {
                    txtMunicipioId.Text = this.ctrlMunicipio.municipio.intId.ToString();
                    txtMunicipioCodigo.Text = this.ctrlMunicipio.municipio.strCodigo;
                    txtMunicipioDescripacion.Text = this.ctrlMunicipio.municipio.strDescripcion;

                    txtEstadoId.Text = this.ctrlMunicipio.municipio.estado.intId.ToString();
                    txtEstadoCodigo.Text = this.ctrlMunicipio.municipio.estado.strCodigo;
                    txtEstadoDescripcion.Text = this.ctrlMunicipio.municipio.estado.strDescripcion;
                    //Limpiar la descripcion anterior para validar municipios
                    txtDescripcionAnt.Text = "";
                    txtMunicipioCodigo.Focus();


                    validarDescripcionMunicipio();
                }
            }

            

        }
        public void mostraBusquedaAsentamientoTipo()
        {
            this.ctrlAsentamientosTipos.Paginacion();
            if (this.ctrlAsentamientosTipos.bolResutladoPaginacion)
            {
                ctrlAsentamientosTipos.CargarFrmBusqueda();
                if (this.ctrlAsentamientosTipos.bolResultadoBusqueda)
                {
                    txAsentamientoTipoId.Text = this.ctrlAsentamientosTipos.asentamientosTipo.intId.ToString();
                    txtCodigoAsentamientoTipo.Text = this.ctrlAsentamientosTipos.asentamientosTipo.strCodigo;
                    txtDescripcionAsentamientoTipo.Text = this.ctrlAsentamientosTipos.asentamientosTipo.strDescripcion;
                    txtCodigoAsentamientoTipo.Focus();
                }
            }

            
        }

       

        public void llenar_formulario()
        {

            if (this.ctrlAsentamiento.asentamiento.strCodigoPostal.Length == 0) return;
            this.clsView.principal.epMensaje.Clear();
            txtCodigoPostal.Text = this.ctrlAsentamiento.asentamiento.strCodigoPostal;
            txtCiudad.Text = this.ctrlAsentamiento.asentamiento.strCiudad;
            txtDescripcion.Text = this.ctrlAsentamiento.asentamiento.strDescripcion;
            txtDescripcionAnt.Text = this.ctrlAsentamiento.asentamiento.strDescripcion;
            
            txtMunicipioId.Text = this.ctrlAsentamiento.asentamiento.municipio.intId.ToString();
            txtMunicipioCodigo.Text = this.ctrlAsentamiento.asentamiento.municipio.strCodigo;
            txtMunicipioDescripacion.Text = this.ctrlAsentamiento.asentamiento.municipio.strDescripcion;
            txAsentamientoTipoId.Text = this.ctrlAsentamiento.asentamiento.asentamientosTipos.intId.ToString();
            txtCodigoAsentamientoTipo.Text = this.ctrlAsentamiento.asentamiento.asentamientosTipos.strCodigo;
            txtDescripcionAsentamientoTipo.Text = this.ctrlAsentamiento.asentamiento.asentamientosTipos.strDescripcion;

            txtEstadoId.Text = this.ctrlAsentamiento.asentamiento.municipio.estado.intId.ToString();
            txtEstadoCodigo.Text = this.ctrlAsentamiento.asentamiento.municipio.estado.strCodigo;
            txtEstadoDescripcion.Text = this.ctrlAsentamiento.asentamiento.municipio.estado.strDescripcion;

            this.clsView.permisosMovimientoMenu(tsAcciones, this.ctrlAsentamiento.asentamiento.strEstatus);
            this.clsView.ocultarGuardar(tsAcciones, btnFooterGuardar, this.ctrlAsentamiento.asentamiento.strEstatus);
            

        }

        public void agregarDeshacer()
        {
            if (this.ctrlAsentamiento.bolResutladoPaginacion)
            {
                this.lsDeshacerAsentamiento.RemoveAt(0);
                this.lsDeshacerAsentamiento.Add(this.ctrlAsentamiento.asentamiento);
            }
        }

        public void validarDescripcionMunicipio() 
        {
            if (txtDescripcion.Text != txtDescripcionAnt.Text && txtMunicipioId.Text != "")
            {
                this.ctrlAsentamiento.validarDescripcionMunicipio(txtDescripcion.Text, txtMunicipioId.Text);
                if (this.ctrlAsentamiento.bolResultado)
                {
                    this.clsView.principal.epMensaje.SetError(txtDescripcion, this.clsView.mensajeErrorValiForm2("La descripción en el municipio debe ser única, favor de verificar"));
                    txtDescripcion.Text = "";
                    txtDescripcion.Focus();
                }
            }
        }


        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void FrmAsentamientos_Load(object sender, EventArgs e)
        {            
            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                },
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);
        }


    }
}
