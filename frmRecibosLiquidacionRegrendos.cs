﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UI_Support;

namespace EmpenosEvora
{
    public partial class frmRecibosLiquidacionRegrendos : Form
    {


        public string _strFoto;
        public PictureBox _pbFoto;
        public bool _bolPermisos;
        public string _strMotivoBonificacion;

        public string _strIndexRow;
        public decimal _decAcumCapital;
        public decimal _decAcumIntereses;
        public decimal _decAcumBonificaciones;
        public decimal _decAcumSubtotal;
        public decimal _decAcumAbonoCap;
        public decimal _decAcumPPI;
        public decimal _decAcumPagar;
        public DataRow _objDataSetSingleAutorizarBonificacion;
        public DateTime _dtpFechaAutorizacionBonificacion;
        public int _intTotalDetallesSel;


        public Controller.clsCtrlRecibosLiquidacionRefrendos ctrlRefrendos;
        public Controller.clsCtrlClientes ctrlClientes;
        public Controller.clsCtrlFoliosProcesos ctrlFoliosProcesos;
        public Controller.clCtrlFormaPago ctrlFormaPago;
        public Controller.clsCtrlBoletaEmpeno ctrlBoletaEmpeno;
        public Controller.clsCtrlSucursalParametros ctrlSucursalParametros;
        public Controller.clsCtrlSucursalesCobrosTipos ctrlSucursalCobrosTipos;
        public Controller.clsCtrlSucursalesPromocion ctrlSucursalesPromocion;
        public Controller.clsCtrlFolios ctrlFolios;
        public Controller.clsCtrlProcesos ctrlProcesos;
        public Properties_Class.clsConstante clsConstante;

        private DPFP.Template template;
        public Core.clsCoreView clsView = new Core.clsCoreView();
        public Controller.clsCtrlUsuarios ctrlUsuario;
        public Controller.clsCtrlFolios ctrlFoliosAuto = new Controller.clsCtrlFolios();

        public bool bolNuevo = false;


        public string strFoto { get => _strFoto; set => _strFoto = value; }

        public PictureBox pbFoto { get => _pbFoto; set => _pbFoto = value; }

        public bool bolPermisos { get => _bolPermisos; set => _bolPermisos = value; }

        public int intTotalDetallesSel { get => _intTotalDetallesSel; set => _intTotalDetallesSel = value; }

        public DataRow objDataSetSingleAutorizarBonificacion { get => _objDataSetSingleAutorizarBonificacion; set => _objDataSetSingleAutorizarBonificacion = value; }

        public DateTime dtpFechaAutorizacionBonificacion { get => _dtpFechaAutorizacionBonificacion; set => _dtpFechaAutorizacionBonificacion = value; }

        public List<Properties_Class.clsRecibosLiquidacionRefrendos> lsDeshacerRefrendos;

        public List<int> lsBoletaEmpenoId;
        
        public string strIndexRow { get => _strIndexRow; set => _strIndexRow = value; }


        public string strMotivoBonificacion { get => _strMotivoBonificacion; set => _strMotivoBonificacion = value; }


        /*Variables que se utlizan para calcular totales*/
        public decimal decAcumCapital { get => _decAcumCapital; set => _decAcumCapital = value; }
        public decimal decAcumIntereses { get => _decAcumIntereses; set => _decAcumIntereses = value; }
        public decimal decAcumBonificaciones { get => _decAcumBonificaciones; set => _decAcumBonificaciones = value; }
        public decimal decAcumSubtotal { get => _decAcumSubtotal; set => _decAcumSubtotal = value; }
        public decimal decAcumAbonoCap { get => _decAcumAbonoCap; set => _decAcumAbonoCap = value; }
        public decimal decAcumPPI { get => _decAcumPPI; set => _decAcumPPI = value; }
        public decimal decAcumPagar { get => _decAcumPagar; set => _decAcumPagar = value; }
        public frmRecibosLiquidacionRegrendos()
        {
            InitializeComponent();

            this.ctrlRefrendos = new Controller.clsCtrlRecibosLiquidacionRefrendos();

            this.ctrlClientes = new Controller.clsCtrlClientes();

            this.ctrlFoliosProcesos = new Controller.clsCtrlFoliosProcesos();

            this.ctrlUsuario = new Controller.clsCtrlUsuarios();

            this.ctrlFormaPago = new Controller.clCtrlFormaPago();

            this.ctrlBoletaEmpeno = new Controller.clsCtrlBoletaEmpeno();

            this.ctrlSucursalParametros = new Controller.clsCtrlSucursalParametros();
            this.ctrlSucursalCobrosTipos = new Controller.clsCtrlSucursalesCobrosTipos();
            this.ctrlSucursalesPromocion = new Controller.clsCtrlSucursalesPromocion();

            //Procesos id de la boleta de empeño
            this.ctrlProcesos = new Controller.clsCtrlProcesos();
            this.ctrlProcesos.BuscarDescripcion("Boletas de Empeño");

            this.ctrlFolios = new Controller.clsCtrlFolios();
            

            this.clsConstante = new Properties_Class.clsConstante();

            //this.ctrlSucursalParametros.Buscar(this.clsView.login.intSucursalId.ToString());


            this.strFoto = "";

            this.lsDeshacerRefrendos = new List<Properties_Class.clsRecibosLiquidacionRefrendos>();
            this.lsDeshacerRefrendos.Add(this.ctrlRefrendos.refrendos);

            this.lsBoletaEmpenoId = new List<int>();
            

            rbtLiquidacion.CheckedChanged += new EventHandler(rbtRefrendos_CheckedChanged);
            rbtRefrendo.CheckedChanged += new EventHandler(rbtRefrendos_CheckedChanged);
            rbtPPI.CheckedChanged += new EventHandler(rbtRefrendos_CheckedChanged);
            
            dgvDetalles.Columns["Boleta"].Width = 80;
            dgvDetalles.Columns["Fecha"].Width = 70;
            dgvDetalles.Columns["Vence"].Width = 70;
            dgvDetalles.Columns["Sem"].Width = 40;
            dgvDetalles.Columns["Día"].Width = 40;
            dgvDetalles.Columns["Sem"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvDetalles.Columns["Día"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvDetalles.Columns["Capital"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvDetalles.Columns["Intereses"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvDetalles.Columns["Bonificar"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvDetalles.Columns["Subtotal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvDetalles.Columns["Abonocap"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvDetalles.Columns["PPIAplica"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvDetalles.Columns["Pagar"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            inicializarTotales();
        }

        private void frmRecibosLiquidacionRegrendos_Load(object sender, EventArgs e)
        {

            //Quitar ultima fila de la tabla
            dgvDetalles.AllowUserToAddRows = false;
            txtFolio.MaxLength = this.ctrlRefrendos.refrendos.intNumCerosConsecutivo;
            this.Text = this.Text + " " + "[Caja: " + this.clsView.login.caja.strDescripcion + "]";

            this.ctrlFoliosAuto.BuscarFolioProceso(this.clsView.strProcesosId, this.clsView.login.intSucursalId.ToString());
            

            this.ctrlSucursalParametros.Buscar(this.clsView.login.intSucursalId.ToString());

            this.ctrlRefrendos.Paginacion(this.clsView.login.intSucursalId);

            this.ctrlFolios.BuscarFolioProceso(this.ctrlProcesos.procesos.intId.ToString(), this.clsView.login.intSucursalId.ToString());

            List<Properties_Class.clsTextbox> lsExtraBtn = new List<Properties_Class.clsTextbox>()
            {
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "footerGuardar", BtnGenerico = btnFooterGuardar}
                }/*,
                { new Properties_Class.clsTextbox()
                                { strBusqueda = "frmBuscar", BtnGenerico = btnBusqueda}
                }*/
            };

            this.clsView.accionMenu(tsAcciones, lsExtraBtn);


            //dtpFecha.MaxDate = DateTime.Today;

            this.bolPermisos = tsbGuardar.Enabled;

            lblMesesPPI.Visible = false;
            txtSemanasPPI.Visible = false;

            if (this.clsView.login.usuario.validarMovimientos() && this.clsView.bolPermisosEliminar)
            {
                tsbEliminar.Enabled = true;
                
            }
            else
            {
                tsbEliminar.Enabled = false;
            }
        }


        private void tsbBuscar_Click(object sender, EventArgs e)
        {
            Dictionary<int, Properties_Class.clsTextbox> dicFocus = new Dictionary<int, Properties_Class.clsTextbox>()
            {
               { 0, new Properties_Class.clsTextbox()
                                { txtGenerico = txtFolio, strBusqueda = "pago"}
                },
                { 1, new Properties_Class.clsTextbox()
                                  { txtGenerico = txtCodigoCliente, strBusqueda = "cliente"}
                },
                { 2, new Properties_Class.clsTextbox()
                                { txtGenerico = txtNombreCliente, strBusqueda = "cliente"}
                },
                { 3, new Properties_Class.clsTextbox()
                                { txtGenerico = txtCodigoFormaPago, strBusqueda = "formaPago"}
                },
                { 4, new Properties_Class.clsTextbox()
                                { txtGenerico = txtDescripcionFormaPago, strBusqueda = "formaPago"}
                }
        };
            foreach (KeyValuePair<int, Properties_Class.clsTextbox> data in dicFocus)
            {
                if (data.Value.txtGenerico.Focused ) 
                { 
                    if (data.Value.strBusqueda == "pago")
                    {
                        this.mostraBusqueda();
                    }
                    else if (data.Value.strBusqueda == "cliente")
                    {
                        this.mostraBusquedaCliente();
                    }
                    else if (data.Value.strBusqueda == "formaPago") 
                    {
                        this.mostraBusquedaFormaPago();
                    }
                   
                }
            }

           /* if (rbtLiquidacion.Focus() || rbtRefrendo.Focus() || rbtPPI.Focus() || txtSemanasPPI.Focus())
            {
                this.mostraBusqueda();
            }*/



        }

        private void tsbEliminar_Click(object sender, EventArgs e)
        {
            if (this.ctrlRefrendos.refrendos.intId > 0) 
            {
                if (this.clsView.confirmacionAccion("Motivo de cancelación", "¿Desea cancelar este registro?"))
                {
                    frmMotivosCancelacion frmMotivos = new frmMotivosCancelacion();
                    frmMotivos.strTipo = "G";
                    frmMotivos.ShowDialog();
                    if (frmMotivos.bolResultado)
                    {
                        this.ctrlRefrendos.refrendos.strMotivosCancelacion = frmMotivos.strMotivo;
                        this.ctrlRefrendos.refrendos.dtFecha = DateTime.Now;
                        this.ctrlRefrendos.refrendos.intUsuario = this.clsView.login.usuario.intId;
                        this.ctrlRefrendos.refrendos.strQueryActualizarBoletasDetalles = " UPDATE boletas_empeno " +
                                                                           " SET estatus =  'ACTIVO' ," +
                                                                           "     fecha_actualizacion = '" + this.ctrlRefrendos.refrendos.dtFecha.ToString("yyyy-MM-dd HH:mm:ss") + "' ," +
                                                                           "     usuario_actualizacion = " + this.clsView.login.usuario.intId + " " +
                                                                           " WHERE boleta_empeno_id IN( " + this.ctrlRefrendos.refrendos.strIdsBoletasDetalles + " ); ";
                        if (this.ctrlRefrendos.refrendos.strIdsBoletasGeneradas != "") 
                        {
                            this.ctrlRefrendos.refrendos.strQueryActualizarBoletasGeneradas = " UPDATE boletas_empeno " +
                                                                         " SET estatus =  'CANCELADA' ," +
                                                                         "     fecha_eliminacion = '" + this.ctrlRefrendos.refrendos.dtFecha.ToString("yyyy-MM-dd HH:mm:ss") + "' ," +
                                                                         "     usuario_eliminacion = " + this.clsView.login.usuario.intId + "," +
                                                                         "     motivo_cancelacion =  '" + this.ctrlRefrendos.refrendos.strMotivosCancelacion + "' ," +
                                                                         "     gramos = 0, " +
                                                                         "     importe = 0," +
                                                                         "     importe_avaluo = 0" +
                                                                          " WHERE boleta_empeno_id IN( " + this.ctrlRefrendos.refrendos.strIdsBoletasGeneradas + " ); "; 
                          
                        }
                        

                        this.ctrlRefrendos.Cancelar();
                        if (this.ctrlRefrendos.bolResultado) 
                        {


                            this.clsView.mensajeExitoDB(this.Text, "canceló");                            
                            this.limpiar_textbox();
                            this.ctrlRefrendos.Paginacion(this.clsView.login.intSucursalId);
                            this.ctrlRefrendos.Buscar(this.ctrlRefrendos.refrendos.strFolio);
                            this.llenar_formulario();
                            
                        }
                    }
                }
            }
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {


            if (this.ctrlRefrendos.refrendos.intId > 0)
            {
                llenar_formulario();
            }
            else
            {
                txtFolio.Enabled = true;
                this.limpiar_textbox();
            }
            txtFolio.Focus();            
        }

        private void btnPrimeroPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlRefrendos.PrimeroPaginacion();
            
            txtFolio.Enabled = true;
            if (!this.ctrlRefrendos.bolResultado) return;

            this.llenar_formulario();
        }

        private void btnAdelante_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlRefrendos.SiguentePaginacion();            
            txtFolio.Enabled = true;
            if (!this.ctrlRefrendos.bolResultado) return;
            this.llenar_formulario();

        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlRefrendos.AtrasPaginacion();
            
            txtFolio.Enabled = true;
            if (!this.ctrlRefrendos.bolResultado) return;
            this.llenar_formulario();

        }

        private void btnUlitmoPag_Click(object sender, EventArgs e)
        {
            this.agregarDeshacer();
            this.ctrlRefrendos.UltimoPaginacion();            
            txtFolio.Enabled = true;
            if (!this.ctrlRefrendos.bolResultado) return;
            this.llenar_formulario();

        }

        private void tsbNuevo_Click(object sender, EventArgs e)
        {            
            this.limpiar_textbox();
            btnUndo.Visible = true;
            this.ctrlRefrendos.refrendos.limpiarProps();
            txtFolio.Enabled = false;
            txtFolio.Text = "AUTOGENERADO";
            lblEstatus.Text = "NUEVO";
            lblEstatus.Visible = true;
            lblDescEstatus.Visible = true;

            txtNombreEmpleado.Text = this.clsView.login.usuario.strNombre + " " + this.clsView.login.usuario.strApellidoPaterno + " " + this.clsView.login.usuario.strApellidoMaterno;

            txtHora.Text = DateTime.Now.ToString("HH:mm:ss");

            txtCodigoCaja.Text = this.clsView.login.caja.strCodigo;
            txtDescripcionCaja.Text = this.clsView.login.caja.strDescripcion;
            dtpFecha.MinDate = DateTime.Today;
            
            this.bolNuevo = true;

            if (this.clsView.bolPermisosGuardar)
            {
                tsbGuardar.Visible = true;
                tsbGuardar.Enabled = true;
                btnFooterGuardar.Visible = true;
                btnFooterGuardar.Enabled = true;
            }
            else
            {                
                tsbGuardar.Enabled = false;
                btnFooterGuardar.Enabled = false;
                btnFooterGuardar.Visible = false;
            }

             txtCodigoCliente.Focus();

             


        }

        private void tsbGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();

        }
        private void DateTimePicker4_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtCodigoCliente_Leave(object sender, EventArgs e)
        {
            if (this.ctrlRefrendos.refrendos.intId > 0 || txtCodigoCliente.Text == "") 
            {

                this.ctrlRefrendos.refrendos.cliente.limpiarProps();


                this.llenar_formulario_clientes();
                return;
            }
                        
            string strCodigo = this.ctrlClientes.cliente.generaCodigoConsecutivo(txtCodigoCliente.Text);            
            this.ctrlRefrendos.BuscarClientesBoletasActivas(strCodigo, this.clsView.login.intSucursalId);
            if (!(this.ctrlRefrendos.mdlRefrendos.bolResultado))
            {
                txtClienteId.Text = "";
                txtCodigoCliente.Text = "";

                txtNombreCliente.Text = "";
                this.ctrlRefrendos.refrendos.cliente.limpiarProps();
                
            }


            // this.llenar_formulario();
            this.llenar_formulario_clientes();


        }

        private void txtFormaPago_Leave(object sender, EventArgs e)
        {
            if (this.ctrlRefrendos.refrendos.intId > 0) return;

            string strCodigo = this.ctrlFormaPago.formaPago.generaCodigoConsecutivo(txtCodigoFormaPago.Text);
            this.ctrlFormaPago.Buscar(strCodigo);
            if (!(this.ctrlFormaPago.mdlFormaPago.bolResultado))
            {
                txtFormaPaogId.Text = "";
                txtCodigoFormaPago.Text = "";
                txtDescripcionFormaPago.Text = "";
                return;
            }

            this.llenar_formulario_forma_pago();
        }

        private void dtpFecha_Leave(object sender, EventArgs e)
        {

        }

        private void btnBusquedaCliente_Click(object sender, EventArgs e)
        {
            this.mostraBusquedaCliente();
        }

        private void btnBusquedaFormaPago_Click(object sender, EventArgs e)
        {
            this.mostraBusquedaFormaPago();
        }



        private void btnVerificarHuellaCliente_Click(object sender, EventArgs e)
        {
            if (this.ctrlRefrendos.refrendos.intId > 0) return;
            this.ctrlRefrendos.PaginacionBoletaActivasCliente(this.clsView.login.intSucursalId);
            VerificationForm frmVerificarHuella = new VerificationForm();
            frmVerificarHuella.objDataSet = this.ctrlRefrendos.objDataSet;

            frmVerificarHuella.ShowDialog();

            if (frmVerificarHuella.bolResultado)
            {
                this.ctrlRefrendos.BuscarHuella(frmVerificarHuella.objDataSetSingle);
                this.llenar_formulario_clientes();
                if (rbtLiquidacion.Checked || rbtRefrendo.Checked)
                {
                    buscarDetalles();
                }
                else if (rbtPPI.Checked)
                {

                }
            }

        }

        public bool validacion()
        {
            bool bolValidacion = true;

             string strFolio = txtFolio.Text;
             
             string strNombreEmpleado = txtNombreEmpleado.Text;
             string strFechaPago = dtpFecha.Text;
             string strHora = txtHora.Text;
             
             
             string strClienteId = txtClienteId.Text;
             string strFormaPagoId = txtFormaPaogId.Text;
             decimal decTotal = decimal.Parse(txtAcumPagar.Text.Replace("$", "").Replace(",", ""));
             decimal decEfectivo = decimal.Parse(txtEfectivo.Text.Replace("$", "").Replace(",", ""));
            

            if (strFolio == "")
             {
                 btnBusqueda.Focus();
                 this.clsView.principal.epMensaje.SetError(btnBusqueda, this.clsView.mensajeErrorValiForm2("El campo boleta es obligatorio, favor de ingresar una boleta "));
                 bolValidacion = false;
             }

             
             if (strClienteId == "")
             {
                 txtCodigoCliente.Focus();
                 this.clsView.principal.epMensaje.SetError(txtCodigoCliente, this.clsView.mensajeErrorValiForm2("El campo cliente es obligatorio, favor de ingresar un cliente "));
                 bolValidacion = false;
             }
             

             if (this.strFoto == "" && this.bolNuevo)
             {
                 btnFotografia.Focus();
                 this.clsView.principal.epMensaje.SetError(btnFotografia, this.clsView.mensajeErrorValiForm2("La fotografía es obligatoria, favor de capturarla "));
                 bolValidacion = false;
             }

            if (strFormaPagoId == "")
            {
                txtCodigoFormaPago.Focus();
                this.clsView.principal.epMensaje.SetError(txtCodigoFormaPago, this.clsView.mensajeErrorValiForm2("El campo forma de pago es obligatorio, favor de ingresar una forma de pago "));
                bolValidacion = false;
            }

            if (this.intTotalDetallesSel == 0) 
            {
                this.clsView.mensajeErrorValiForm2("Seleccionar al menos una boleta para este recibo ");
                bolValidacion = false;
            }
            if (decEfectivo == 0 && decTotal > 0) 
            {
                txtEfectivo.Focus();                
                this.clsView.principal.epMensaje.SetError(txtEfectivo, this.clsView.mensajeErrorValiForm2("El campo recibido es obligatorio, favor de ingresar un importe"));
                bolValidacion = false;
            }

            if (!rbtLiquidacion.Checked && !rbtRefrendo.Checked && !rbtPPI.Checked)
            {
                //rbtPPI.Focus();                
                this.clsView.principal.epMensaje.SetError(rbtPPI, this.clsView.mensajeErrorValiForm2("Seleccionar el tipo de recibo")); 
                bolValidacion = false;
            }

            return bolValidacion;
        }

        public void guardar()
        {
            this.clsView.principal.epMensaje.Clear();
            this.lsBoletaEmpenoId.Clear();

            if (this.validacion())
            {

                if (this.bolNuevo) 
                {

                    //Folio de pagos
                    Controller.clsCtrlFolios ctrlFolios = new Controller.clsCtrlFolios();
                    ctrlFolios.BuscarFolioProceso(this.clsView.strProcesosId, this.clsView.login.intSucursalId.ToString());
                    if (ctrlFolios.folio.intConsecutivo == 0)
                    {
                        this.clsView.mensajeErrorValiForm2("No es posible generar el folio, comuníquese con el administrador.");
                        tsbGuardar.Enabled = false;
                        btnFooterGuardar.Enabled = false;
                    }
                    else 
                    {
                        this.ctrlRefrendos.refrendos.strFolio = ctrlFolios.folio.regresarFolioConsecutivo(this.ctrlRefrendos.refrendos.intNumCerosConsecutivo);
                    }
                    
                    
                }

                this.strFoto = "reciboLiq" + this.ctrlRefrendos.refrendos.strFolio + ".jpg";

                this.ctrlRefrendos.refrendos.strHora = txtHora.Text;
                this.ctrlRefrendos.refrendos.dtFechaPago = Convert.ToDateTime(dtpFecha.Value.ToString("yyyy-MM-dd") + " " + this.ctrlRefrendos.refrendos.strHora);                                               
                this.ctrlRefrendos.refrendos.cliente.intId = int.Parse(txtClienteId.Text);
                this.ctrlRefrendos.refrendos.formPago.intId = (txtFormaPaogId.Text != "")?int.Parse(txtFormaPaogId.Text): 0; 
                this.ctrlRefrendos.refrendos.decTotalPagar = decimal.Parse(txtCobroTotal.Text.Replace("$", "").Replace(",", ""));
                this.ctrlRefrendos.refrendos.decImportePagado = decimal.Parse(txtEfectivo.Text.Replace("$", "").Replace(",", ""));

                //this.ctrlRefrendos.refrendos.strDescripcion = txtDescripcion.Text;
                this.ctrlRefrendos.refrendos.strFotografiaImagen = this.strFoto;
                this.ctrlRefrendos.refrendos.intSucursal = this.clsView.login.intSucursalId;
                this.ctrlRefrendos.refrendos.caja.intId = this.clsView.login.caja.intId;
                this.ctrlRefrendos.refrendos.intUsuario = this.clsView.login.usuario.intId;
                this.ctrlRefrendos.refrendos.empleado.intId = this.clsView.login.usuario.intId;
                this.ctrlRefrendos.refrendos.intProcesoMenuId = int.Parse(this.clsView.strProcesosId);
                
                //Dependo el tipo de radio botton se asingar el tipo de recbio
                if (rbtLiquidacion.Checked)
                {
                    this.ctrlRefrendos.refrendos.strTipoRecibo = "LIQUIDACION";
                    this.ctrlRefrendos.refrendos.strEstatus = this.ctrlRefrendos.refrendos.dicEstatus["ACTIVO"];
                }
                else if (rbtRefrendo.Checked)
                {
                    this.ctrlRefrendos.refrendos.strTipoRecibo = "REFRENDO";
                    this.ctrlRefrendos.refrendos.strEstatus = this.ctrlRefrendos.refrendos.dicEstatus["ACTIVO"];
                }
                else if (rbtPPI.Checked)
                {
                    this.ctrlRefrendos.refrendos.strTipoRecibo = "PARCIAL";
                    this.ctrlRefrendos.refrendos.strEstatus = this.ctrlRefrendos.refrendos.dicEstatus["PPI"];
                }

                if (rbtLiquidacion.Checked)
                {
                    guardarDetalles();
                }
                else if (rbtRefrendo.Checked)
                {
                    guardarDetallesRefrendos();
                }
                else if (rbtPPI.Checked) 
                {
                    guardarDetallesPPI();
                }

                //Procesos id pagos
                this.ctrlFoliosProcesos.BuscarFolioProcesoMenu(int.Parse(this.clsView.strProcesosId), this.clsView.login.intSucursalId);
                if (this.ctrlFoliosProcesos.mdlFolioProcesos.bolResultado)
                {
                    this.ctrlRefrendos.refrendos.intFolioId = this.ctrlFoliosProcesos.foliosProcesos.folio.intId;

                    this.ctrlRefrendos.Guardar();
                }
                else
                {
                    this.clsView.mensajeErrorValiForm2("No es posible generar el folio, comuníquese con el administrador.");
                }

                if (this.ctrlRefrendos.mdlRefrendos.bolResultado)
                {
                    if (!(this.pbFoto == null))
                    {
                        string pathImagenBoleta = Application.StartupPath.Replace("bin\\Debug", "Resources\\reciboRefrendos\\");
                        this.pbFoto.Image.Save(pathImagenBoleta + this.ctrlRefrendos.refrendos.strFotografiaImagen, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }

                    this.limpiar_textbox();
                    this.ctrlRefrendos.Paginacion(this.clsView.login.intSucursalId);

                    this.ctrlRefrendos.Buscar(this.ctrlRefrendos.refrendos.strFolio);
                    this.llenar_formulario();

                    txtFolio.Enabled = true;
                    tsbGuardar.Visible = false;
                    btnFooterGuardar.Visible = false;
                    
                    this.clsView.mensajeExitoDB(this.Text, "guardó");
                }             
            }
        }

        private void txtCodigoCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }

        private void txtCodigoEmpleado_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumVali(e);
        }


        public void llenar_formulario_clientes()
        {

            if (this.ctrlRefrendos.refrendos.cliente.intId == 0) 
            {
                limpiar_tipo_recibo();
                return;
            }
            

            txtClienteId.Text = this.ctrlRefrendos.refrendos.cliente.intId.ToString();
            txtCodigoCliente.Text = this.ctrlRefrendos.refrendos.cliente.strCodigo;
            txtNombreCliente.Text = this.ctrlRefrendos.refrendos.cliente.strNombre;
   
            if (rbtLiquidacion.Checked || rbtRefrendo.Checked || rbtPPI.Checked)
            {
                this.cargarDetalles();
            }
        }

        public void llenar_formulario_forma_pago()
        {
            if (this.ctrlFormaPago.formaPago.intId == 0) return;
            txtFormaPaogId.Text = this.ctrlFormaPago.formaPago.intId.ToString();
            txtCodigoFormaPago.Text = this.ctrlFormaPago.formaPago.strCodigo;
            txtDescripcionFormaPago.Text = this.ctrlFormaPago.formaPago.strDescripcion;


        }





        private void limpiar_textbox()
        {
            this.clsView.limpiar_textbox(this);            
            dtpFecha.MinDate = DateTime.Today;
            dtpFecha.Text = DateTime.Today.ToString();            
            rbtLiquidacion.Checked = false;
            rbtPPI.Checked = false;
            rbtRefrendo.Checked = false;
            this.lsBoletaEmpenoId.Clear();
            this.clsView.principal.epMensaje.Clear();
            btnVerCancelacion.Visible = false;
            this.bolNuevo = false;
            this.strFoto = "";
            this.strIndexRow = "";
            lblDescripcionBoletaRefrendo.Visible = false;
            lblListaBoletasRefrendadas.Visible = false;
            lblListaBoletasRefrendadas.Text = "";
            lblDescEstatus.Visible = false;
            lblEstatus.Visible = false;
            if (this.clsView.bolPermisosNuevo)
            {
                tsbNuevo.Enabled = true;
            }
            else 
            {
                tsbNuevo.Enabled = false;
            }

            if (this.clsView.login.usuario.validarMovimientos() && this.clsView.bolPermisosGuardar)
            {
                tsbGuardar.Visible = true;
                btnFooterGuardar.Visible = true;
                tsbGuardar.Enabled = true;                
            }
            else 
            {
                tsbGuardar.Visible = true;                
                tsbGuardar.Enabled = false;
                btnFooterGuardar.Visible = false;
            }


            if (this.clsView.login.usuario.validarMovimientos() && this.clsView.bolPermisosEliminar)
            {
                tsbEliminar.Enabled = true;
            }
            else 
            {
                tsbEliminar.Enabled = false;
            }

            btnUndo.Enabled = true;
            rbtLiquidacion.Enabled = true;
            rbtRefrendo.Enabled = true;
            rbtPPI.Enabled = true;
            lblMesesPPI.Visible = false;
            txtSemanasPPI.Visible = false;
            txtSemanasPPI.Enabled = true;

            limpiar_tipo_recibo();
        }

        private void limpiar_tipo_recibo() 
        {

            dgvDetalles.Rows.Clear();
            txtClienteId.Text = "";
            txtCodigoCliente.Text = "";
            txtNombreCliente.Text = "";
            txtBonificacion.Text = "";
            txtBonificacion.Enabled = false;
            lblBoletaBonifiacionFolio.Text = "";
            txtAbonoCapital.Text = "";
            txtAbonoCapital.Enabled = false;
            lblBoletaAbonoCapFolio.Text = "";


            inicializarTotales();
        }

        private void mostraBusqueda()
        {
            if (this.ctrlRefrendos.bolResutladoPaginacion)
            {
                this.ctrlRefrendos.CargarFrmBusquedaRangoFecha();
                if (this.ctrlRefrendos.bolResultadoBusqueda)
                {
                    this.llenar_formulario();
                    txtFolio.Focus();
                }
                
            }
        }



        private void mostraBusquedaCliente()
        {
            if (this.ctrlRefrendos.refrendos.intId > 0) return;
            this.ctrlRefrendos.PaginacionBoletaActivasCliente(this.clsView.login.intSucursalId);
            if (this.ctrlRefrendos.bolResutladoPaginacion)
            {
                this.ctrlRefrendos.CargarFrmBusquedaClientesBoletasActivas();

                if (this.ctrlRefrendos.bolResultadoBusqueda)
                {
                    this.llenar_formulario_clientes();
                   
                    
                }
            }

        }


        private void mostraBusquedaFormaPago()
        {
            if (this.ctrlRefrendos.refrendos.intId > 0) return;
            this.ctrlFormaPago.Paginacion();
            if (this.ctrlFormaPago.bolResutladoPaginacion)
            {
                this.ctrlFormaPago.CargarFrmBusqueda();

                if (this.ctrlFormaPago.bolResultado)
                {
                    this.llenar_formulario_forma_pago();
                }
            }

        }


        private void btnFooterGuardar_Click(object sender, EventArgs e)
        {
            this.guardar();
        }

        private void btnFotografia_Click(object sender, EventArgs e)
        {
            if (this.ctrlRefrendos.refrendos.intId > 0) return;
            if (!this.bolNuevo)
            {
                this.clsView.principal.epMensaje.SetError(txtFolio, this.clsView.mensajeErrorValiForm2("Deber de tener valor el campo folio para registrar fotografía"));
            }
            else
            {

                frmFoto frmFoto = new frmFoto();
                frmFoto.ShowDialog();
                this.strFoto = "Imagen";
                this.pbFoto = frmFoto.pbGenerico;
            }
        }



        private void btnBusqueda_Click(object sender, EventArgs e)
        {
            mostraBusqueda();
        }



        public void llenar_formulario()
        {
            btnUndo.Enabled = false;
            this.limpiar_textbox();
            if (this.ctrlRefrendos.refrendos.intId == 0)
            {
                lblDescEstatus.Visible = false;
                lblEstatus.Visible = false;
                return;
            }
            txtFolio.Enabled = true;

            txtFolio.Text = this.ctrlRefrendos.refrendos.strFolio;
            
            dtpFecha.MinDate = this.ctrlRefrendos.refrendos.dtFechaPago;
            dtpFecha.Text = this.ctrlRefrendos.refrendos.dtFechaPago.ToString();
            txtHora.Text = this.ctrlRefrendos.refrendos.strHora;            
            txtClienteId.Text = this.ctrlRefrendos.refrendos.cliente.intId.ToString();
            txtCodigoCliente.Text = this.ctrlRefrendos.refrendos.cliente.strCodigo;
            txtNombreCliente.Text = this.ctrlRefrendos.refrendos.cliente.strNombre;
            txtNombreEmpleado.Text = this.ctrlRefrendos.refrendos.empleado.strNombre;
            if (this.ctrlRefrendos.refrendos.formPago.intId > 0) 
            {
                txtFormaPaogId.Text = this.ctrlRefrendos.refrendos.formPago.intId.ToString();
                txtCodigoFormaPago.Text = this.ctrlRefrendos.refrendos.formPago.strCodigo;
                txtDescripcionFormaPago.Text = this.ctrlRefrendos.refrendos.formPago.strDescripcion;
            }

            if (this.ctrlRefrendos.refrendos.strTipoRecibo == this.clsConstante.dicTipoRecibo["LIQ"])
            {
                rbtLiquidacion.Checked = true;
            }
            else if (this.ctrlRefrendos.refrendos.strTipoRecibo == this.clsConstante.dicTipoRecibo["REF"])
            {
                rbtRefrendo.Checked = true;

            }
            else if (this.ctrlRefrendos.refrendos.strTipoRecibo == this.clsConstante.dicTipoRecibo["PAR"]) 
            {
                rbtPPI.Checked = true;
            } 
            
            
            this.strFoto = this.ctrlRefrendos.refrendos.strFotografiaImagen;
            lblEstatus.Text = this.ctrlRefrendos.refrendos.strEstatus;

            txtCodigoCaja.Text = this.ctrlRefrendos.refrendos.caja.strCodigo;
            txtDescripcionCaja.Text = this.ctrlRefrendos.refrendos.caja.strDescripcion;

            txtCobroTotal.Text = this.clsView.convertMoneda(this.ctrlRefrendos.refrendos.decTotalPagar.ToString());
            txtEfectivo.Text = this.clsView.convertMoneda(this.ctrlRefrendos.refrendos.decImportePagado.ToString());


           


            lblDescEstatus.Visible = true;
            lblEstatus.Visible = true;
          
            tsbGuardar.Enabled = false;
            btnFooterGuardar.Visible = false;

            if (this.ctrlRefrendos.refrendos.strEstatus == this.ctrlRefrendos.refrendos.dicEstatus["CANCELADO"])
            {
                tsbEliminar.Enabled = false;
                btnVerCancelacion.Visible = true;

            }
            else if (this.clsView.login.usuario.validarMovimientos() && this.clsView.bolPermisosEliminar && this.ctrlRefrendos.refrendos.strEstatus == this.ctrlRefrendos.refrendos.dicEstatus["ACTIVO"])
            {
                tsbEliminar.Enabled = true;
                btnVerCancelacion.Visible = false;
            }
            else
            {
                tsbEliminar.Enabled = false;
                btnVerCancelacion.Visible = false;
            }

            if (rbtLiquidacion.Checked || rbtRefrendo.Checked)
            {
                dgvDetalles.Columns["PPIAplica"].HeaderText = "PPI Aplicado";
            }
            else if (rbtPPI.Checked)
            {
                dgvDetalles.Columns["PPIAplica"].HeaderText = "PPI x Aplicar";
            }


            txtSemanasPPI.Value = 0;
            rbtLiquidacion.Enabled = false;
            rbtRefrendo.Enabled = false;
            rbtPPI.Enabled = false;


            calcularCambio();
            buscarPagosDetalles(this.ctrlRefrendos.refrendos.intId);
        }

        public void agregarDeshacer()
        {
            this.lsDeshacerRefrendos.RemoveAt(0);
            this.lsDeshacerRefrendos.Add(this.ctrlRefrendos.refrendos);
        }

        /*Sobre escribir el metodo existente ProcessCmdKey*/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*Metodo que ejecuta teclas especiales*/
            if (this.clsView.teclasEspeciales(this.Text.ToLower(), tsAcciones, this, keyData))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void btnFooterCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtFolio_Leave(object sender, EventArgs e)
        {

            if (txtFolio.Text == "") 
            {
                limpiar_textbox();
                return;
            }
            
            if (txtFolio.Text.Length < this.ctrlRefrendos.refrendos.intNumCerosConsecutivo)
            {
                if (this.ctrlFoliosAuto.folio.intConsecutivo == 0)
                {
                    this.clsView.mensajeErrorValiForm2("No es posible generar el folio, comuníquese con el administrador.");
                    tsbGuardar.Enabled = false;
                    btnFooterGuardar.Enabled = false;
                }
                else
                {
                    txtFolio.Text = this.ctrlFoliosAuto.folio.regresarFolioConsecutivoAuto(this.ctrlRefrendos.refrendos.intNumCerosConsecutivo, txtFolio.Text);
                }
            }

            
            this.ctrlRefrendos.Buscar(txtFolio.Text);
            if (this.ctrlRefrendos.mdlRefrendos.bolResultado)
            {
                this.llenar_formulario();
            }
            else 
            {
                this.ctrlRefrendos.refrendos.limpiarProps();
                this.limpiar_textbox();
                txtFolio.Text = "";
                txtFolio.Focus();
                
            }
        }



        private void rbtRefrendo_CheckedChanged(object sender, EventArgs e)
        {

        }

        //Metodo que controlar los radio buttons del formulario
        private void rbtRefrendos_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb.Checked )
            {


                bool bolMostrarCamposRefrendadas = false;
                bool bolMostrarCamposPPI = false;

                if (rbtRefrendo.Checked)
                {
                    bolMostrarCamposRefrendadas = true;
                }
                else if (rbtPPI.Checked)
                {
                    bolMostrarCamposPPI = true;
                    txtSemanasPPI.Value = 1;
                }

                lblDescripcionBoletaRefrendo.Visible = bolMostrarCamposRefrendadas;
                lblListaBoletasRefrendadas.Visible = bolMostrarCamposRefrendadas;
                lblMesesPPI.Visible = bolMostrarCamposPPI;
                txtSemanasPPI.Visible = bolMostrarCamposPPI;

                if (txtClienteId.Text != "") 
                {
                    this.ctrlRefrendos.refrendos.strTipoRecibo = "";
                    cargarDetalles();
                }
                
            }
            

        }

        
        private void buscarDetalles() 
        {
            dgvDetalles.Rows.Clear();
            txtBonificacion.Enabled = false;
            lblBoletaBonifiacionFolio.Text = "";
            txtAbonoCapital.Text = "";
            txtAbonoCapital.Enabled = false;
            lblBoletaAbonoCapFolio.Text = "";
            string strDiaProm = this.regresarDiaFecha();
            this.ctrlRefrendos.PaginacionDetallesBoletasActivas(int.Parse(txtClienteId.Text), strDiaProm, this.clsView.login.intSucursalId);
            if (this.ctrlRefrendos.bolResultado)
            {
                try
                {
                    decimal decInteres = 0;
                    decimal decImporte = 0;

                    int intCobroTipoIdInteres = 0;
                    int intCobroTipoIdAlmacenaje = 0;
                    int intCobroTipoIdManejo = 0;

                    string strCobroTipoId = "";
                    string strCobroTipoNombres = "";

                    decimal decPorcentajeInteres = 0;
                    decimal decPorcentajeAlmacenaje = 0;
                    decimal decPorcentajeManejo = 0;
                    decimal decPrestamo = 0;

                    string strPorcentajeCobrosTipo = "";

                    decimal decImporteInteres = 0;
                    decimal decImporteAlmacenaje = 0;
                    decimal decImporteManejo = 0;
                    decimal decImporteInteresPPI = 0;
                    decimal decImporteAlmacenajePPI = 0;
                    decimal decImporteManejoPPI = 0;
                    decimal decTotalIntereses = 0;
                    decimal decTotalPPI = 0;                    
                    decimal decSubTotal = 0;

                    string strImporteCobrosTipo = "";

                    decimal decPorcentajeTotal = 0;

                    decimal decPPI = 0;

                    string strDiaPromDet = "";
                    decimal decPorcentajeProm = 0;
                    decimal decPromocion = 0;
                    //string strPrendaTipoId = "";
                     
                    foreach (DataRow row in this.ctrlRefrendos.objDataSetSecundario.Tables[0].Rows)
                    {
                        //decImporte = decimal.Parse(row["importe"].ToString();
                        DateTime dtpFechaDB = Convert.ToDateTime(row["fecha"]);
                        this.ctrlSucursalCobrosTipos.Paginacion(this.clsView.login.intSucursalId);
                        //Hacer recorido para obtener valores de los tipo de cobros que se van a utilizar en refrendos 
                        foreach (DataRow rowCT in this.ctrlSucursalCobrosTipos.objDataSet.Tables[0].Rows)
                        {
                            //Dependiendo de tipo de cobros asignar valores
                            if (rowCT["descripcion"].ToString() == "INTERES")
                            {
                                

                                intCobroTipoIdInteres = int.Parse(rowCT["cobro_tipo_id"].ToString());
                                decPorcentajeInteres = decimal.Parse(rowCT["porcentaje"].ToString()) / this.clsConstante.intDiasMesCuota;
                                decPorcentajeTotal += decPorcentajeInteres;

                                strCobroTipoId += rowCT["cobro_tipo_id"].ToString() + "|";
                                strCobroTipoNombres += rowCT["descripcion"].ToString() + "|";
                                strPorcentajeCobrosTipo += decPorcentajeInteres.ToString() + "|";
                                
                            }
                            else if (rowCT["descripcion"].ToString() == "ALMACENAJE")
                            {
                                intCobroTipoIdAlmacenaje = int.Parse(rowCT["cobro_tipo_id"].ToString());
                                decPorcentajeAlmacenaje = decimal.Parse(rowCT["porcentaje"].ToString()) / this.clsConstante.intDiasMesCuota;
                                decPorcentajeTotal += decPorcentajeAlmacenaje;

                                strCobroTipoId += rowCT["cobro_tipo_id"].ToString() + "|";
                                strCobroTipoNombres += rowCT["descripcion"].ToString() + "|";
                                strPorcentajeCobrosTipo += decPorcentajeAlmacenaje.ToString() + "|";
                            }
                            else if (rowCT["descripcion"].ToString() == "MANEJO")
                            {
                                intCobroTipoIdManejo = int.Parse(rowCT["cobro_tipo_id"].ToString());
                                decPorcentajeManejo = decimal.Parse(rowCT["porcentaje"].ToString()) / this.clsConstante.intDiasMesCuota;
                                decPorcentajeTotal += decPorcentajeManejo;

                                strCobroTipoId += rowCT["cobro_tipo_id"].ToString() + "|";
                                strCobroTipoNombres += rowCT["descripcion"].ToString() + "|";
                                strPorcentajeCobrosTipo += decPorcentajeManejo.ToString() + "|";
                            }
                        }



                        //Calcular dias de diferencia entre dos fechas
                        int intTotalDias = int.Parse((dtpFecha.Value - dtpFechaDB).TotalDays.ToString());
                        //Asignar el numero de dias minimo para el cobro de intreses
                        int intDiasCobroMinimo = int.Parse(row["dias_cobro_minimo"].ToString());
                        //Verificar si la diferencias de dias se entuentra dentro de los dias minimo
                        int intDiasInteres = (intTotalDias > intDiasCobroMinimo) ? (intDiasCobroMinimo +(intTotalDias - intDiasCobroMinimo)) : intDiasCobroMinimo;
                        decPrestamo = decimal.Parse(row["importe"].ToString());
                        if (intDiasCobroMinimo > 0)
                        {
                           
                            /*Calcular importe de interes por cada tipo de cobro*/
                            if (decPorcentajeInteres > 0 ) 
                            {
                                decImporteInteres = (((decPrestamo * decPorcentajeInteres) / 100) * intDiasInteres);
                                decImporteInteresPPI = (((decPrestamo * decPorcentajeInteres) / 100) * this.clsConstante.intDiasSemanas);
                                strImporteCobrosTipo += decImporteInteres.ToString() + "|";
                            }

                            if (decPorcentajeAlmacenaje > 0) 
                            {
                                decImporteAlmacenaje = (((decPrestamo * decPorcentajeAlmacenaje) / 100) * intDiasInteres);
                                decImporteAlmacenajePPI = (((decPrestamo * decPorcentajeAlmacenaje) / 100) * this.clsConstante.intDiasSemanas);
                                strImporteCobrosTipo += decImporteAlmacenaje.ToString() + "|";
                            }

                            if (decPorcentajeManejo > 0) 
                            {
                                decImporteManejo = (((decPrestamo * decPorcentajeManejo) / 100) * intDiasInteres);
                                decImporteManejoPPI = (((decPrestamo * decPorcentajeManejo) / 100) * this.clsConstante.intDiasSemanas);
                                strImporteCobrosTipo += decImporteManejo.ToString() + "|";
                            }
                            
                            
                            /*Calcular el total de intereses*/
                            decTotalIntereses = decImporteInteres + decImporteAlmacenaje + decImporteManejo;
                            /*Calcular el total de PPI*/
                            decTotalPPI = decImporteInteresPPI + decImporteAlmacenajePPI + decImporteManejoPPI;
                            decSubTotal = decPrestamo + decTotalIntereses;
                            if (rbtPPI.Checked)
                            {
                                //decPPI = ((decTotalPPI * (txtSemanasPPI.Value * this.clsConstante.intDiasSemanas)) * decPorcentajeTotal) / 100;
                                decPPI = ((decPrestamo * decPorcentajeTotal) / 100) * (txtSemanasPPI.Value * this.clsConstante.intDiasSemanas);
                            }
                            else 
                            {
                                decPPI = decimal.Parse(row["ppi"].ToString());
                            }

                            if (rbtRefrendo.Checked)
                            {
                                strDiaPromDet = strDiaProm;
                                decPorcentajeProm = decimal.Parse(row["porcentaje_promocion"].ToString());
                                decPromocion = (decTotalIntereses * decPorcentajeProm) / 100;
                            }
                            else 
                            {
                                strDiaPromDet = "";                                
                                decPorcentajeProm = 0;
                                decPromocion = 0;
                            }
                        }
                                                
                        dgvDetalles.Rows.Add(
                            "",//diaMinimoPromocion
                            decPorcentajeTotal,
                            strDiaPromDet,//Dia de promocion
                            decPorcentajeProm,//Porcentaje de promocion
                            strCobroTipoNombres,
                            intDiasCobroMinimo,
                            row["prenda_tipo_id"],
                            strCobroTipoId,
                            strPorcentajeCobrosTipo,
                            strImporteCobrosTipo,
                            0,//importeCobroTipoBonificacion
                            0,
                            0,
                            "",//movitovs
                            row["boleta_empeno_id"],
                            row["folio"],
                            row["fecha"],
                            row["fecha_vencimiento"],
                            0,
                            intDiasInteres,
                            this.clsView.convertMoneda(row["importe"].ToString()),
                            this.clsView.convertMoneda(decTotalIntereses.ToString()),
                            this.clsView.convertMoneda(decPromocion.ToString()),
                            this.clsView.convertMoneda(decSubTotal.ToString()),
                            false,
                            this.clsView.convertMoneda("0"),
                            this.clsView.convertMoneda(decPPI.ToString()),
                            this.clsView.convertMoneda("0")
                           );
                        strCobroTipoId = "";
                        strPorcentajeCobrosTipo = "";
                        strImporteCobrosTipo = "";
                        strCobroTipoNombres = "";
                        decPorcentajeTotal = 0;



                    }

                    inicializarTotales();
                }
                catch (Exception e) { }
                
            }

        }

        private void lkbBonifiacion_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (this.ctrlRefrendos.refrendos.intId > 0) return;

            if (this.strIndexRow == "")
            {
                this.clsView.mensajeErrorValiForm2("Debe seleccionar la boleta");
                return;
            }


            frmBonificar frmBonificar = new frmBonificar();
            frmBonificar.ShowDialog();
            if (frmBonificar.bolResultado)
            {
                this.objDataSetSingleAutorizarBonificacion = frmBonificar.objDataSetSingle;
                this.dtpFechaAutorizacionBonificacion = DateTime.Now;
                this.strMotivoBonificacion = frmBonificar.strMotivo;
                txtBonificacion.Enabled = true;
                txtBonificacion.Focus();
                
                lblBoletaBonifiacionFolio.Visible = true;
                lblBoletaBonifiacionFolio.Text = dgvDetalles.Rows[int.Parse(this.strIndexRow)].Cells["Boleta"].Value.ToString();
                
                string strDia = regresarDiaFecha();
                              
                /*this.ctrlSucursalesPromocion.BuscarTipoPrenda(this.clsView.login.intSucursalId, dgvDetalles.Rows[int.Parse(this.strIndexRow)].Cells["prendaTipoId"].Value.ToString(), strDia);
                if (this.ctrlSucursalesPromocion.bolResultado) 
                {                    
                    decimal decIntereses = 0;
                    if (rbtPPI.Checked)
                    {
                         decIntereses = decimal.Parse(dgvDetalles.Rows[int.Parse(this.strIndexRow)].Cells["PPIAplica"].Value.ToString().Replace("$", "").Replace(",", ""));
                    }
                    else 
                    {
                        decIntereses = decimal.Parse(dgvDetalles.Rows[int.Parse(this.strIndexRow)].Cells["Intereses"].Value.ToString().Replace("$", "").Replace(",", ""));
                        
                    }

                    decimal decBonificacion = decIntereses * (this.ctrlSucursalesPromocion.sucursalesPromocion.decPorcentaje / 100);
                    string strBonificacion = this.clsView.convertMoneda(decBonificacion.ToString());
                    txtBonificacion.Text = strBonificacion;
                    
                }*/
            }
            else 
            {
                lblBoletaBonifiacionFolio.Visible = false;
                lblBoletaBonifiacionFolio.Text = "";
            }
            
        }

        private void lkbAbonoCapital_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (this.ctrlRefrendos.refrendos.intId > 0) return;
            if (this.strIndexRow == "")
            {
                this.clsView.mensajeErrorValiForm2("Debe seleccionar la boleta");
                return;
            }
            else if (!rbtRefrendo.Checked) 
            {
                this.clsView.mensajeErrorValiForm2("Debe activar Refrendo para abonar a capital");
                return;
            }
            txtAbonoCapital.Enabled = true;
            txtAbonoCapital.Focus();

            lblBoletaAbonoCapFolio.Visible = true;
            lblBoletaAbonoCapFolio.Text = dgvDetalles.Rows[int.Parse(this.strIndexRow)].Cells["Boleta"].Value.ToString();

        }



        private void dgvDetalles_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (this.ctrlRefrendos.refrendos.intId > 0) return;
            string header = dgvDetalles.Columns[dgvDetalles.CurrentCell.ColumnIndex].HeaderText;
            
            this.decAcumCapital = 0;
            this.decAcumIntereses = 0;
            this.decAcumBonificaciones = 0;
            this.decAcumSubtotal = 0;
            this.decAcumAbonoCap = 0;
            this.decAcumPPI = 0;
            this.decAcumPagar = 0;
            this.intTotalDetallesSel = 0;
            int intDias = 0;
            int intDiasMinimoPromocino = 0;

            this.strIndexRow = "";
            decimal decCapital = decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["Capital"].Value.ToString().Replace("$", "").Replace(",", ""));
            decimal decSubtotal = decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["Subtotal"].Value.ToString().Replace("$", "").Replace(",", ""));
            decimal decTotalIntereses = decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["Intereses"].Value.ToString().Replace("$", "").Replace(",", ""));
            decimal decPPI = decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["PPIAplica"].Value.ToString().Replace("$", "").Replace(",", ""));
            decimal decBonificar = decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["Bonificar"].Value.ToString().Replace("$", "").Replace(",", ""));
            decimal decPagar = 0;
            decimal decAbonoCapital = 0;

            dgvDetalles.Rows[e.RowIndex].Cells["Sel"].Value = Convert.ToBoolean(dgvDetalles.Rows[e.RowIndex].Cells["Sel"].EditedFormattedValue);
            if (Convert.ToBoolean(dgvDetalles.Rows[e.RowIndex].Cells["Sel"].EditedFormattedValue))
            {
                this.strIndexRow = e.RowIndex.ToString();
            }
            else 
            {
                this.strIndexRow = "";
                dgvDetalles.Rows[e.RowIndex].Cells["fechaAutorizacion"].Value = "";
                dgvDetalles.Rows[e.RowIndex].Cells["usuarioAutorizacion"].Value = "";
                dgvDetalles.Rows[e.RowIndex].Cells["motivoAutorizacion"].Value = "";                
                
            }
            
            foreach (DataGridViewRow row in dgvDetalles.Rows)
            {


                if (Convert.ToBoolean(row.Cells["Sel"].Value.ToString()))
                {

                    this.intTotalDetallesSel++;
                    this.decAcumCapital += decimal.Parse(row.Cells["Capital"].Value.ToString().Replace("$", "").Replace(",", ""));
                    this.decAcumIntereses += decimal.Parse(row.Cells["Intereses"].Value.ToString().Replace("$", "").Replace(",", ""));
                    this.decAcumBonificaciones += decimal.Parse(row.Cells["Bonificar"].Value.ToString().Replace("$", "").Replace(",", ""));
                    this.decAcumSubtotal += decimal.Parse(row.Cells["Subtotal"].Value.ToString().Replace("$", "").Replace(",", ""));
                    this.decAcumAbonoCap += decimal.Parse(row.Cells["AbonoCap"].Value.ToString().Replace("$", "").Replace(",", ""));
                    this.decAcumPPI += decimal.Parse(row.Cells["PPIAplica"].Value.ToString().Replace("$", "").Replace(",", ""));


                     decCapital = decimal.Parse(row.Cells["Capital"].Value.ToString().Replace("$", "").Replace(",", ""));
                     decSubtotal = decimal.Parse(row.Cells["Subtotal"].Value.ToString().Replace("$", "").Replace(",", ""));
                     decTotalIntereses = decimal.Parse(row.Cells["Intereses"].Value.ToString().Replace("$", "").Replace(",", ""));
                     decBonificar = decimal.Parse(row.Cells["Bonificar"].Value.ToString().Replace("$", "").Replace(",", ""));
                     decPPI = decimal.Parse(row.Cells["PPIAplica"].Value.ToString().Replace("$", "").Replace(",", ""));
                    decAbonoCapital = decimal.Parse(row.Cells["AbonoCap"].Value.ToString().Replace("$", "").Replace(",", ""));

                    if (rbtLiquidacion.Checked)
                    {
                        row.Cells["Pagar"].Value = this.clsView.convertMoneda((decCapital + decTotalIntereses - decBonificar - decPPI).ToString());
                    }
                    else if (rbtRefrendo.Checked)
                    {
                        row.Cells["Pagar"].Value = this.clsView.convertMoneda((decTotalIntereses - decBonificar + decAbonoCapital - decPPI).ToString());
                    }
                    else if (rbtPPI.Checked)
                    {
                        row.Cells["Pagar"].Value = this.clsView.convertMoneda((decPPI - decBonificar).ToString());

                    }

                    this.decAcumPagar += decimal.Parse(row.Cells["Pagar"].Value.ToString().Replace("$", "").Replace(",", ""));

                }                                  
                else 
                {

                    

                    decimal decDiaPromocion = 0;
                    if (rbtRefrendo.Checked)
                    {

                        intDias = int.Parse(row.Cells["Día"].Value.ToString());


                        
                        
                        if (row.Cells["diaProcentaje"].Value.ToString() != "") 
                        {
                            if (row.Cells["diaMinimoPromocion"].Value.ToString() != "") 
                            {
                                intDiasMinimoPromocino = int.Parse(row.Cells["diaMinimoPromocion"].Value.ToString());
                            }
                            
                            decTotalIntereses = decimal.Parse(row.Cells["Intereses"].Value.ToString().Replace("$", "").Replace(",", ""));

                            if (intDias >= intDiasMinimoPromocino)
                            {
                                decDiaPromocion = (decTotalIntereses * decimal.Parse(row.Cells["diaProcentaje"].Value.ToString()) / 100);                                
                            }
                            
                            
                        }
                        

                    }

                    //decPagar = decDiaPromocion - decAcumPPI;
                    row.Cells["Bonificar"].Value = this.clsView.convertMoneda(decDiaPromocion.ToString());
                    row.Cells["AbonoCap"].Value = this.clsView.convertMoneda("0");
                    row.Cells["Pagar"].Value = this.clsView.convertMoneda("0");


                }

            
            }


            txtAcumCapital.Text = this.clsView.convertMoneda(this.decAcumCapital.ToString());
            txtAcumIntereses.Text = this.clsView.convertMoneda(this.decAcumIntereses.ToString());
            txtAcumBonificaciones.Text = this.clsView.convertMoneda(this.decAcumBonificaciones.ToString());
            txtAcumSubTotal.Text = this.clsView.convertMoneda(this.decAcumSubtotal.ToString());
            txtAcumAbonoCap.Text = this.clsView.convertMoneda(this.decAcumAbonoCap.ToString());
            txtAcumPPI.Text = this.clsView.convertMoneda(this.decAcumPPI.ToString());
            txtAcumPagar.Text = this.clsView.convertMoneda(this.decAcumPagar.ToString());
            txtCobroTotal.Text = txtAcumPagar.Text;
            calcularCambio();

        }


       /* private void dgvDetalles_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (this.ctrlRefrendos.refrendos.intId > 0) return;
            string header = dgvDetalles.Columns[dgvDetalles.CurrentCell.ColumnIndex].HeaderText;

            if (header == "Sel")
            {
                decimal decSubtotal = decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["Subtotal"].Value.ToString().Replace("$", "").Replace(",", ""));
                decimal decIntereses = decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["Intereses"].Value.ToString().Replace("$", "").Replace(",", ""));
                decimal decBonificar = decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["Bonificar"].Value.ToString().Replace("$", "").Replace(",", ""));
                decimal decAplicar = decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["PPIAplica"].Value.ToString().Replace("$", "").Replace(",", ""));
                decimal decPagar = 0;


                if (Convert.ToBoolean(dgvDetalles.Rows[e.RowIndex].Cells["Sel"].EditedFormattedValue))
                {

                    this.strIndexRow = e.RowIndex.ToString();


                    if (rbtLiquidacion.Checked)
                    {
                        dgvDetalles.Rows[e.RowIndex].Cells["Pagar"].Value = this.clsView.convertMoneda((decSubtotal - decAplicar - decBonificar).ToString());
                    }
                    else if (rbtRefrendo.Checked)
                    {
                        dgvDetalles.Rows[e.RowIndex].Cells["Pagar"].Value = this.clsView.convertMoneda((decIntereses - decAplicar - decBonificar).ToString());
                    }
                    else if (rbtPPI.Checked)
                    {                        
                        dgvDetalles.Rows[e.RowIndex].Cells["Pagar"].Value = dgvDetalles.Rows[e.RowIndex].Cells["PPIAplica"].Value.ToString();

                    }

                    this.decAcumCapital += decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["Capital"].Value.ToString().Replace("$", "").Replace(",", ""));
                    this.decAcumIntereses += decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["Intereses"].Value.ToString().Replace("$", "").Replace(",", ""));
                    this.decAcumBonificaciones += decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["Bonificar"].Value.ToString().Replace("$", "").Replace(",", ""));
                    this.decAcumSubtotal += decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["Subtotal"].Value.ToString().Replace("$", "").Replace(",", ""));
                    this.decAcumAbonoCap += decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["AbonoCap"].Value.ToString().Replace("$", "").Replace(",", ""));
                    this.decAcumPPI += decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["PPIAplica"].Value.ToString().Replace("$", "").Replace(",", ""));
                    this.decAcumPagar += decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["Pagar"].Value.ToString().Replace("$", "").Replace(",", ""));

                    txtAcumCapital.Text = this.clsView.convertMoneda(this.decAcumCapital.ToString());
                    txtAcumIntereses.Text = this.clsView.convertMoneda(this.decAcumIntereses.ToString());
                    txtAcumBonificaciones.Text = this.clsView.convertMoneda(this.decAcumBonificaciones.ToString());
                    txtAcumSubTotal.Text = this.clsView.convertMoneda(this.decAcumSubtotal.ToString());
                    txtAcumAbonoCap.Text = this.clsView.convertMoneda(this.decAcumAbonoCap.ToString());
                    txtAcumPPI.Text = this.clsView.convertMoneda(this.decAcumPPI.ToString());
                    txtAcumPagar.Text = this.clsView.convertMoneda(this.decAcumPagar.ToString());
                    txtCobroTotal.Text = txtAcumPagar.Text;
                    calcularCambio();
                }
                else
                {

                    this.strIndexRow = "";

                    if (decimal.Parse(txtAcumCapital.Text.Replace(",", "").Replace("$", "")) > 0)
                    {
                        this.decAcumCapital -= decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["Capital"].Value.ToString().Replace("$", "").Replace(",", ""));
                        txtAcumCapital.Text = this.clsView.convertMoneda(this.decAcumCapital.ToString());
                    }

                    if (decimal.Parse(txtAcumIntereses.Text.Replace(",", "").Replace("$", "")) > 0)
                    {
                        this.decAcumIntereses -= decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["Intereses"].Value.ToString().Replace("$", "").Replace(",", ""));
                        txtAcumIntereses.Text = this.clsView.convertMoneda(this.decAcumIntereses.ToString());
                    }

                    if (decimal.Parse(txtAcumBonificaciones.Text.Replace(",", "").Replace("$", "")) > 0)
                    {
                        this.decAcumBonificaciones -= decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["Bonificar"].Value.ToString().Replace("$", "").Replace(",", ""));
                        txtAcumBonificaciones.Text = this.clsView.convertMoneda(this.decAcumBonificaciones.ToString());
                    }

                    if (decimal.Parse(txtAcumSubTotal.Text.Replace(",", "").Replace("$", "")) > 0)
                    {
                        this.decAcumSubtotal -= decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["Subtotal"].Value.ToString().Replace("$", "").Replace(",", ""));
                        txtAcumSubTotal.Text = this.clsView.convertMoneda(this.decAcumSubtotal.ToString());
                    }

                    if (decimal.Parse(txtAcumAbonoCap.Text.Replace(",", "").Replace("$", "")) > 0)
                    {
                        this.decAcumAbonoCap -= decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["AbonoCap"].Value.ToString().Replace("$", "").Replace(",", ""));
                        txtAcumAbonoCap.Text = this.clsView.convertMoneda(this.decAcumAbonoCap.ToString());
                    }

                    if (decimal.Parse(txtAcumPPI.Text.Replace(",", "").Replace("$", "")) > 0)
                    {
                        this.decAcumPPI -= decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["PPIAplica"].Value.ToString().Replace("$", "").Replace(",", ""));
                        txtAcumPPI.Text = this.clsView.convertMoneda(this.decAcumPPI.ToString());
                    }

                    if (decimal.Parse(txtAcumPagar.Text.Replace(",", "").Replace("$", "")) > 0)
                    {
                        this.decAcumPagar -= decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["Pagar"].Value.ToString().Replace("$", "").Replace(",", ""));
                        txtAcumPagar.Text = this.clsView.convertMoneda(this.decAcumPagar.ToString());
                    }

                    txtCobroTotal.Text = txtAcumPagar.Text;
                    calcularCambio();

                    decimal decDiaPromocion = 0;
                    if (rbtRefrendo.Checked)
                    {
                        decDiaPromocion = (decIntereses * decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["diaProcentaje"].Value.ToString()) / 100);

                    }

                    
                    dgvDetalles.Rows[e.RowIndex].Cells["Bonificar"].Value = this.clsView.convertMoneda(decDiaPromocion.ToString());
                    dgvDetalles.Rows[e.RowIndex].Cells["AbonoCap"].Value = this.clsView.convertMoneda("0");
                    dgvDetalles.Rows[e.RowIndex].Cells["Pagar"].Value = this.clsView.convertMoneda("0");
                }

            }
        }*/

        public void limpiarTotales() 
        {
            this.decAcumCapital = 0;
            this.decAcumIntereses = 0;
            this.decAcumBonificaciones = 0;
            this.decAcumSubtotal = 0;
            this.decAcumAbonoCap = 0;
            this.decAcumPPI = 0;
            this.decAcumPagar = 0;

            txtAcumCapital.Text = "";
            txtAcumIntereses.Text = "";
            txtAcumBonificaciones.Text = "";
            txtAcumSubTotal.Text = "";
            txtAcumAbonoCap.Text = "";
            txtAcumPPI.Text = "";
            txtAcumPagar.Text = "";
            txtCobroTotal.Text = txtAcumPagar.Text;
            calcularCambio();
        }
        private void dgvDetalles_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

           

        }


        public void cargarDetalles() 
        {
            /*bool bolMostrarCamposRefrendadas = false;
            bool bolMostrarCamposPPI = false;*/
            limpiarTotales();

            /*if (rbtRefrendo.Checked)
            {
                bolMostrarCamposRefrendadas = true;
            }
            else if (rbtPPI.Checked)
            {
                bolMostrarCamposPPI = true;
                txtSemanasPPI.Value = 1;
            }*/

            if (!(this.ctrlRefrendos.refrendos.intId > 0))
            {            
                buscarDetalles();
            }
            


           /* lblDescripcionBoletaRefrendo.Visible = bolMostrarCamposRefrendadas;
            lblListaBoletasRefrendadas.Visible = bolMostrarCamposRefrendadas;
            lblMesesPPI.Visible = bolMostrarCamposPPI;
            txtSemanasPPI.Visible = bolMostrarCamposPPI;*/
            lblListaBoletasRefrendadas.Text = this.ctrlRefrendos.refrendos.strListaBoletasGeneradas;

        }

        private void btnFooterCierra_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chkDeselecionarBoleta_CheckedChanged(object sender, EventArgs e)
        {
            if (this.ctrlRefrendos.refrendos.intId > 0) return;
            if (chkDeselecionarBoleta.Checked) 
            {
                decimal decBonificar = 0;
                foreach (DataGridViewRow row in dgvDetalles.Rows) 
                {


                    if (Convert.ToBoolean(row.Cells["Sel"].Value.ToString()))
                    {
                        row.Cells["Sel"].Value = false;
                        this.decAcumCapital = 0;
                        this.decAcumIntereses = 0;
                        this.decAcumBonificaciones = 0;
                        this.decAcumSubtotal = 0;
                        this.decAcumAbonoCap = 0;
                        this.decAcumPPI = 0;
                        this.decAcumPagar = 0;
                        decBonificar = decimal.Parse(row.Cells["Bonificar"].Value.ToString().Replace("$", "").Replace(",", ""));
                        if (decBonificar > 0) 
                        {
                            row.Cells["Subtotal"].Value = this.clsView.convertMoneda((decimal.Parse(row.Cells["Subtotal"].Value.ToString().Replace("$", "").Replace(",", "")) - decBonificar).ToString());
                        }                        
                        row.Cells["Bonificar"].Value = this.clsView.convertMoneda("0");
                        row.Cells["AbonoCap"].Value = this.clsView.convertMoneda("0");
                        row.Cells["Pagar"].Value = this.clsView.convertMoneda("0");
                    }
                }

                txtAcumCapital.Text = this.clsView.convertMoneda(this.decAcumCapital.ToString());
                txtAcumIntereses.Text = this.clsView.convertMoneda(this.decAcumIntereses.ToString());
                txtAcumBonificaciones.Text = this.clsView.convertMoneda(this.decAcumBonificaciones.ToString());
                txtAcumSubTotal.Text = this.clsView.convertMoneda(this.decAcumSubtotal.ToString());
                txtAcumAbonoCap.Text = this.clsView.convertMoneda(this.decAcumAbonoCap.ToString());
                txtAcumPPI.Text = this.clsView.convertMoneda(this.decAcumPPI.ToString());
                txtAcumPagar.Text = this.clsView.convertMoneda(this.decAcumPagar.ToString());

                txtCobroTotal.Text = txtAcumPagar.Text;
                calcularCambio();

                //Limpiar el row index
                this.strIndexRow = "";
            }
        }

             private void txtBonificacion_Leave(object sender, EventArgs e)
                 {
                        if (txtBonificacion.Text != "") 
                         {
                         decimal decBonificacion = decimal.Parse(txtBonificacion.Text.Replace("$", "").Replace(",", ""));

                         int intIndexRow = int.Parse(this.strIndexRow);


                         this.clsView.principal.epMensaje.Clear();
                         decimal decCapital = decimal.Parse(dgvDetalles.Rows[intIndexRow].Cells["Capital"].Value.ToString().Replace("$", "").Replace(",", ""));
                         decimal decSubtotal = 0;
                         decimal decBonificar = decimal.Parse(txtBonificacion.Text.Replace("$", "").Replace(",", ""));
                         decimal decBonificarPas = decimal.Parse(dgvDetalles.Rows[intIndexRow].Cells["Bonificar"].Value.ToString().Replace("$", "").Replace(",", ""));
                         decimal decPPI = decimal.Parse(dgvDetalles.Rows[intIndexRow].Cells["PPIAplica"].Value.ToString().Replace("$", "").Replace(",", ""));
                         
                         if (rbtPPI.Checked)
                         {
                             if (decBonificar > decPPI) 
                             {
                                 this.clsView.principal.epMensaje.SetError(txtBonificacion, this.clsView.mensajeErrorValiForm2("La bonificación debe ser menor ó igual a los intereses PPI calculados"));
                                 txtBonificacion.Text = "";
                                 txtBonificacion.Focus();
                                 return;
                             }

                         }
                         else 
                         {
                             if (decBonificar > decimal.Parse(dgvDetalles.Rows[intIndexRow].Cells["Intereses"].Value.ToString().Replace("$", "").Replace(",", "")))
                             {

                                 this.clsView.principal.epMensaje.SetError(txtBonificacion, this.clsView.mensajeErrorValiForm2("La bonificación debe ser menor ó igual a los intereses calculados"));
                                 txtBonificacion.Text = "";
                                 txtBonificacion.Focus();
                                 return;
                             }
                         }
                            dgvDetalles.Rows[intIndexRow].Cells["Bonificar"].Value = this.clsView.convertMoneda(txtBonificacion.Text);
                            this.decAcumCapital = 0;
                             this.decAcumIntereses = 0;
                             this.decAcumBonificaciones = 0;
                             this.decAcumSubtotal = 0;
                             this.decAcumAbonoCap = 0;
                             this.decAcumPPI = 0;
                             this.decAcumPagar = 0;
                             //decimal decPrestamo = 0;
                             foreach (DataGridViewRow row in dgvDetalles.Rows)
                             {

                                 if (Convert.ToBoolean(row.Cells["Sel"].Value.ToString()))
                                 {


                                    



                                     decimal decBonifica = decimal.Parse(row.Cells["Bonificar"].Value.ToString().Replace("$", "").Replace(",", ""));
                                     decimal decTotalIntereses = decimal.Parse(row.Cells["Intereses"].Value.ToString().Replace("$", "").Replace(",", ""));
                                     decimal decAbonoCapital = decimal.Parse(row.Cells["AbonoCap"].Value.ToString().Replace("$", "").Replace(",", ""));
                                     decPPI = decimal.Parse(row.Cells["PPIAplica"].Value.ToString().Replace("$", "").Replace(",", ""));
                                     decSubtotal = decCapital + decTotalIntereses - decBonifica;



                                     row.Cells["Subtotal"].Value = this.clsView.convertMoneda((decSubtotal).ToString());

                                     if (rbtLiquidacion.Checked)
                                     {
                                         row.Cells["Pagar"].Value = this.clsView.convertMoneda((decSubtotal - decPPI).ToString());
                                     }
                                     else if (rbtRefrendo.Checked)
                                     {
                                         row.Cells["Pagar"].Value = this.clsView.convertMoneda((decTotalIntereses - decBonifica + decAbonoCapital - decPPI).ToString());
                                     }
                                     else if (rbtPPI.Checked)
                                     {
                                         row.Cells["Pagar"].Value = this.clsView.convertMoneda((decPPI - decBonifica).ToString());

                                     }
                                     
                                     this.decAcumPagar += decimal.Parse(row.Cells["Pagar"].Value.ToString().Replace("$", "").Replace(",", ""));

                                    this.decAcumCapital += decimal.Parse(row.Cells["Capital"].Value.ToString().Replace("$", "").Replace(",", ""));
                                    this.decAcumIntereses += decimal.Parse(row.Cells["Intereses"].Value.ToString().Replace("$", "").Replace(",", ""));
                                    this.decAcumBonificaciones += decimal.Parse(row.Cells["Bonificar"].Value.ToString().Replace("$", "").Replace(",", ""));
                                    this.decAcumSubtotal += decimal.Parse(row.Cells["Subtotal"].Value.ToString().Replace("$", "").Replace(",", ""));
                                    this.decAcumAbonoCap += decimal.Parse(row.Cells["AbonoCap"].Value.ToString().Replace("$", "").Replace(",", ""));
                                    this.decAcumPPI += decimal.Parse(row.Cells["PPIAplica"].Value.ToString().Replace("$", "").Replace(",", ""));
                                 }
                                 else 
                                 { 

                                 }
                             }

                             dgvDetalles.Rows[intIndexRow].Cells["fechaAutorizacion"].Value = this.dtpFechaAutorizacionBonificacion;
                             dgvDetalles.Rows[intIndexRow].Cells["usuarioAutorizacion"].Value = this.objDataSetSingleAutorizarBonificacion.ItemArray[0].ToString();
                             dgvDetalles.Rows[intIndexRow].Cells["motivoAutorizacion"].Value = this.strMotivoBonificacion;
           

                        txtBonificacion.Text = "";
                             txtBonificacion.Enabled = false;

                             lblBoletaBonifiacionFolio.Visible = false;
                             lblBoletaBonifiacionFolio.Text = "";

                             txtAcumCapital.Text = this.clsView.convertMoneda(this.decAcumCapital.ToString());
                             txtAcumIntereses.Text = this.clsView.convertMoneda(this.decAcumIntereses.ToString());
                             txtAcumBonificaciones.Text = this.clsView.convertMoneda(this.decAcumBonificaciones.ToString());
                             txtAcumSubTotal.Text = this.clsView.convertMoneda(this.decAcumSubtotal.ToString());
                             txtAcumAbonoCap.Text = this.clsView.convertMoneda(this.decAcumAbonoCap.ToString());
                             txtAcumPPI.Text = this.clsView.convertMoneda(this.decAcumPPI.ToString());
                             txtAcumPagar.Text = this.clsView.convertMoneda(this.decAcumPagar.ToString());
                             txtCobroTotal.Text = txtAcumPagar.Text;
                             calcularCambio();

                     }
                 }

        /* private void txtBonificacion_Leave(object sender, EventArgs e)
         {
                if (txtBonificacion.Text != "") 
                 {
                 decimal decBonificacion = decimal.Parse(txtBonificacion.Text.Replace("$", "").Replace(",", ""));

                 int intIndexRow = int.Parse(this.strIndexRow);


                 this.clsView.principal.epMensaje.Clear();
                 decimal decCapital = decimal.Parse(dgvDetalles.Rows[intIndexRow].Cells["Capital"].Value.ToString().Replace("$", "").Replace(",", ""));
                 decimal decSubtotal = 0;
                 decimal decBonificar = decimal.Parse(txtBonificacion.Text.Replace("$", "").Replace(",", ""));
                 decimal decBonificarPas = decimal.Parse(dgvDetalles.Rows[intIndexRow].Cells["Bonificar"].Value.ToString().Replace("$", "").Replace(",", ""));
                 decimal decPPI = decimal.Parse(dgvDetalles.Rows[intIndexRow].Cells["PPIAplica"].Value.ToString().Replace("$", "").Replace(",", ""));
                 dgvDetalles.Rows[intIndexRow].Cells["Bonificar"].Value = this.clsView.convertMoneda(txtBonificacion.Text);
                 if (rbtPPI.Checked)
                 {
                     if (decBonificar > decPPI) 
                     {
                         this.clsView.principal.epMensaje.SetError(txtBonificacion, this.clsView.mensajeErrorValiForm2("La bonificación debe ser menor ó igual a los intereses PPI calculados"));
                         txtBonificacion.Text = "";
                         txtBonificacion.Focus();
                         return;
                     }

                 }
                 else 
                 {
                     if (decBonificar > decimal.Parse(dgvDetalles.Rows[intIndexRow].Cells["Intereses"].Value.ToString().Replace("$", "").Replace(",", "")))
                     {

                         this.clsView.principal.epMensaje.SetError(txtBonificacion, this.clsView.mensajeErrorValiForm2("La bonificación debe ser menor ó igual a los intereses calculados"));
                         txtBonificacion.Text = "";
                         txtBonificacion.Focus();
                         return;
                     }
                 }

                     this.decAcumCapital = 0;
                     this.decAcumIntereses = 0;
                     this.decAcumBonificaciones = 0;
                     this.decAcumSubtotal = 0;
                     this.decAcumAbonoCap = 0;
                     this.decAcumPPI = 0;
                     this.decAcumPagar = 0;
                     //decimal decPrestamo = 0;
                     foreach (DataGridViewRow row in dgvDetalles.Rows)
                     {

                         if (Convert.ToBoolean(row.Cells["Sel"].Value.ToString()))
                         {


                             this.decAcumCapital += decimal.Parse(row.Cells["Capital"].Value.ToString().Replace("$", "").Replace(",", ""));
                             this.decAcumIntereses += decimal.Parse(row.Cells["Intereses"].Value.ToString().Replace("$", "").Replace(",", ""));
                             this.decAcumBonificaciones += decimal.Parse(row.Cells["Bonificar"].Value.ToString().Replace("$", "").Replace(",", ""));
                             this.decAcumSubtotal += decimal.Parse(row.Cells["Subtotal"].Value.ToString().Replace("$", "").Replace(",", ""));
                             this.decAcumAbonoCap += decimal.Parse(row.Cells["AbonoCap"].Value.ToString().Replace("$", "").Replace(",", ""));
                             this.decAcumPPI += decimal.Parse(row.Cells["PPIAplica"].Value.ToString().Replace("$", "").Replace(",", ""));



                             decimal decBonifica = decimal.Parse(row.Cells["Bonificar"].Value.ToString().Replace("$", "").Replace(",", ""));
                             decimal decTotalIntereses = decimal.Parse(row.Cells["Intereses"].Value.ToString().Replace("$", "").Replace(",", ""));
                             decimal decAbonoCapital = decimal.Parse(row.Cells["AbonoCap"].Value.ToString().Replace("$", "").Replace(",", ""));
                             decPPI = decimal.Parse(row.Cells["PPIAplica"].Value.ToString().Replace("$", "").Replace(",", ""));
                             decSubtotal = decCapital + decTotalIntereses - decBonifica;



                             row.Cells["Subtotal"].Value = this.clsView.convertMoneda((decSubtotal).ToString());

                             if (rbtLiquidacion.Checked)
                             {
                                 row.Cells["Pagar"].Value = this.clsView.convertMoneda((decSubtotal - decPPI).ToString());
                             }
                             else if (rbtRefrendo.Checked)
                             {
                                 row.Cells["Pagar"].Value = this.clsView.convertMoneda((decTotalIntereses - decBonifica + decAbonoCapital - decPPI).ToString());
                             }
                             else if (rbtPPI.Checked)
                             {
                                 row.Cells["Pagar"].Value = this.clsView.convertMoneda((decPPI - decBonifica).ToString());

                             }

                             this.decAcumSubtotal += decSubtotal;
                             this.decAcumPagar += decimal.Parse(row.Cells["Pagar"].Value.ToString().Replace("$", "").Replace(",", ""));

                         }
                         else 
                         { 

                         }
                     }

                     dgvDetalles.Rows[intIndexRow].Cells["fechaAutorizacion"].Value = this.dtpFechaAutorizacionBonificacion;
                     dgvDetalles.Rows[intIndexRow].Cells["usuarioAutorizacion"].Value = this.objDataSetSingleAutorizarBonificacion.ItemArray[0].ToString();
                     dgvDetalles.Rows[intIndexRow].Cells["motivoAutorizacion"].Value = this.strMotivoBonificacion;*/
        /*dgvDetalles.Rows[intIndexRow].Cells["diaPromocion"].Value = "";
        dgvDetalles.Rows[intIndexRow].Cells["diaProcentaje"].Value = "";
        dgvDetalles.Rows[intIndexRow].Cells["prendaTipoId"].Value = "";*/

        /*txtBonificacion.Text = "";
             txtBonificacion.Enabled = false;

             lblBoletaBonifiacionFolio.Visible = false;
             lblBoletaBonifiacionFolio.Text = "";

             txtAcumCapital.Text = this.clsView.convertMoneda(this.decAcumCapital.ToString());
             txtAcumIntereses.Text = this.clsView.convertMoneda(this.decAcumIntereses.ToString());
             txtAcumBonificaciones.Text = this.clsView.convertMoneda(this.decAcumBonificaciones.ToString());
             txtAcumSubTotal.Text = this.clsView.convertMoneda(this.decAcumSubtotal.ToString());
             txtAcumAbonoCap.Text = this.clsView.convertMoneda(this.decAcumAbonoCap.ToString());
             txtAcumPPI.Text = this.clsView.convertMoneda(this.decAcumPPI.ToString());
             txtAcumPagar.Text = this.clsView.convertMoneda(this.decAcumPagar.ToString());
             txtCobroTotal.Text = txtAcumPagar.Text;
             calcularCambio();

     }
 }*/


        private void txtAbonoCapital_Leave(object sender, EventArgs e)
        {
            if (txtAbonoCapital.Text != "")
            {

                int intIndexRow = int.Parse(this.strIndexRow);
                if (decimal.Parse(txtAbonoCapital.Text) > decimal.Parse(dgvDetalles.Rows[intIndexRow].Cells["Capital"].Value.ToString().Replace("$", "").Replace(",", "")))
                {
                    this.clsView.principal.epMensaje.SetError(txtAbonoCapital, this.clsView.mensajeErrorValiForm2("El abono a capital debe ser menor ó igual al capital de la boleta"));
                    txtAbonoCapital.Text = "";
                    txtAbonoCapital.Focus();
                }
                else 
                {
                    this.clsView.principal.epMensaje.Clear();
                    decimal decAbonoCapital = decimal.Parse(txtAbonoCapital.Text);
                    decimal decPagar = decimal.Parse(dgvDetalles.Rows[intIndexRow].Cells["Pagar"].Value.ToString().Replace("$", "").Replace(",", ""));

                    dgvDetalles.Rows[intIndexRow].Cells["AbonoCap"].Value = this.clsView.convertMoneda(txtAbonoCapital.Text);

                    dgvDetalles.Rows[intIndexRow].Cells["Pagar"].Value = this.clsView.convertMoneda((decPagar + decAbonoCapital).ToString());
                    
                    this.decAcumAbonoCap += decAbonoCapital;
                    this.decAcumPagar += decAbonoCapital;
                    txtAcumAbonoCap.Text = this.clsView.convertMoneda(this.decAcumAbonoCap.ToString());
                    txtAcumPagar.Text = this.clsView.convertMoneda(this.decAcumPagar.ToString());

                    txtCobroTotal.Text = txtAcumPagar.Text;
                    calcularCambio();

                    txtAbonoCapital.Text = "";
                    txtAbonoCapital.Enabled = false;
                    lblBoletaAbonoCapFolio.Visible = false;
                    lblBoletaAbonoCapFolio.Text = "";

                    
                }
             }
         }

        public void guardarDetalles()
        {            
          
            string pago_id = "@id";
           
            int countDetalles = dgvDetalles.Rows.Count;            
            
            this.ctrlRefrendos.refrendos.strQueryPagosDetallesCobrosTipos = "INSERT INTO pagos_detalles_cobros_tipos(pago_id, renglon_detalle, renglon, cobro_tipo_id, tipo , importe, porcentaje) VALUES";
            this.ctrlRefrendos.refrendos.strQueryPagosDetalles = "INSERT INTO pagos_detalles(   pago_id,  renglon,  boleta_empeno_id,   dias_intereses,  fecha_autorizacion,  usuario_autorizacion, motivo_autorizacion) VALUES";
            this.ctrlRefrendos.refrendos.strQueryBoletaEmpenoEstatus = " UPDATE boletas_empeno " +
                                                                       " SET estatus = CASE " +
                                                                       " {0} " +
                                                                       " ELSE estatus END " +
                                                                       " WHERE boleta_empeno_id IN( {1} ); ";
            string strQueryPagosDetallesCobrosTipos = "";
            string strQueryPagosDetalles = "";
            int intReglonDetalles = 1;
            int intReglonDetallesCobrosTipo = 1;
            string strBoletasIds = "";
            string strEstatusUpdate = "";            

            int i = 0;

            foreach (DataGridViewRow row in dgvDetalles.Rows)
            {

                if (Convert.ToBoolean(row.Cells["Sel"].Value.ToString())) 
                {
                    strBoletasIds += row.Cells["boleta_empeno_id"].Value.ToString()+", ";
                    strEstatusUpdate += " WHEN boleta_empeno_id = " + row.Cells["boleta_empeno_id"].Value.ToString() + " THEN 'LIQUIDADA' ";

                    if (row.Cells["usuarioAutorizacion"].Value.ToString() == "")
                    {
                        row.Cells["usuarioAutorizacion"].Value = 0;
                    }

                    if (int.Parse(row.Cells["usuarioAutorizacion"].Value.ToString()) == 0)
                    {


                        strQueryPagosDetalles += "(@id ," +
                        " " + intReglonDetalles + ", " +
                        " " + row.Cells["boleta_empeno_id"].Value.ToString() + " , " +
                        " " + row.Cells["Día"].Value.ToString().Replace("$", "").Replace(",", "") + " , " +
                        " null, " +
                        " null,  " +
                        " null  " +") || ";
                    }
                    else 
                    {
                        string strDiaPorcentaje = row.Cells["diaProcentaje"].Value.ToString();
                        
                        string strDiaPromocion = row.Cells["diaPromocion"].Value.ToString() ;
                        string strPrendaTipoId = row.Cells["prendaTipoId"].Value.ToString();

                        if (strDiaPorcentaje == "") 
                        {
                            strDiaPorcentaje = "null";
                            strPrendaTipoId = "null";
                        }

                        if (strDiaPromocion == "")
                        {
                            strDiaPromocion = "null";
                        }
                        else 
                        {
                            strDiaPromocion = "'"+row.Cells["diaPromocion"].Value.ToString()+"'";
                        }


                        strQueryPagosDetalles += "(@id ," +
                         " " + intReglonDetalles + ", " +
                         " " + row.Cells["boleta_empeno_id"].Value.ToString() + " , " +
                         " " + row.Cells["Día"].Value.ToString().Replace("$", "").Replace(",", "") + " , " +
                         " '" + Convert.ToDateTime(row.Cells["fechaAutorizacion"].Value.ToString()).ToString("yyyy-MM-dd HH:mm:ss") + "' , " +
                         " " + row.Cells["usuarioAutorizacion"].Value.ToString() + " , " +
                         " '" + row.Cells["motivoAutorizacion"].Value.ToString() + "'  " +") || ";
                    }


                    string[] cobrosTipoId = row.Cells["cobroTipoId"].Value.ToString().Split('|');
                    string[] porcentajeCobroTipo = row.Cells["porcentajeCobroTipo"].Value.ToString().Split('|');
                    string[] importeCobroTipo = row.Cells["importeCobroTipo"].Value.ToString().Split('|');

                    decimal decIntereses = decimal.Parse(row.Cells["Intereses"].Value.ToString().Replace("$", "").Replace(",", ""));
                    decimal decBonificar = decimal.Parse(row.Cells["Bonificar"].Value.ToString().Replace("$", "").Replace(",", ""));
                    decimal decPPIAplica = decimal.Parse(row.Cells["PPIAplica"].Value.ToString().Replace("$", "").Replace(",", ""));

                    foreach (string cobros in cobrosTipoId) 
                    {
                   
                        if (i != 3)
                        {
                            if (cobros != "|" && porcentajeCobroTipo[i] != "|" && importeCobroTipo[i] != "|")
                            {
                                strQueryPagosDetallesCobrosTipos += " (@id ," +
                                    " " + intReglonDetalles + " , " +
                                    " " + intReglonDetallesCobrosTipo + " , " +
                                    " " + cobros + " ," +
                                    " '" + "INTERESES" + "' , " +
                                    " " + importeCobroTipo[i] + " , " +
                                    " " + porcentajeCobroTipo[i] + ") ||";

                                if (decBonificar > 0)
                                {
                                    strQueryPagosDetallesCobrosTipos += " (@id ," +
                                    " " + intReglonDetalles + " , " +
                                    " " + intReglonDetallesCobrosTipo + " , " +
                                    " " + cobros + " ," +
                                    " '" + "BONIFICACION" + "' , " +
                                    " " + (decBonificar * decimal.Parse(porcentajeCobroTipo[i]) / decimal.Parse(row.Cells["porcentajeTotal"].Value.ToString())) + " , " +
                                    " " + porcentajeCobroTipo[i] + ") ||";
                                }
                                if (decPPIAplica > 0)
                                {
                                    strQueryPagosDetallesCobrosTipos += " (@id ," +
                                        " " + intReglonDetalles + " , " +
                                        " " + intReglonDetallesCobrosTipo + " , " +
                                        " " + cobros + " ," +
                                        " '" + "PPI" + "' , " +
                                        " " + (decPPIAplica * decimal.Parse(porcentajeCobroTipo[i]) / decimal.Parse(row.Cells["porcentajeTotal"].Value.ToString())) + " , " +
                                        " " + porcentajeCobroTipo[i] + ") ||";
                                }
                                intReglonDetallesCobrosTipo++;
                                i++;
                            }
                        }
                        else
                        {
                            break;
                        }

                    }
                    i = 0;

                    intReglonDetallesCobrosTipo = 1;
                    intReglonDetalles++;
                }

                
            }


            this.ctrlRefrendos.refrendos.strQueryPagosDetallesCobrosTipos += strQueryPagosDetallesCobrosTipos;
            this.ctrlRefrendos.refrendos.strQueryPagosDetalles += strQueryPagosDetalles;

            int intStrIndex = this.ctrlRefrendos.refrendos.strQueryPagosDetallesCobrosTipos.LastIndexOf("||");
            int intStrIndex2 = this.ctrlRefrendos.refrendos.strQueryPagosDetalles.LastIndexOf("||");
            int intStrIndex3 = strBoletasIds.LastIndexOf(",");

            strBoletasIds = strBoletasIds.Remove(intStrIndex3).Insert(intStrIndex3, " ");
            this.ctrlRefrendos.refrendos.strQueryPagosDetallesCobrosTipos = this.ctrlRefrendos.refrendos.strQueryPagosDetallesCobrosTipos.Remove(intStrIndex).Insert(intStrIndex, ";").Replace("||", ",");
            this.ctrlRefrendos.refrendos.strQueryPagosDetalles = this.ctrlRefrendos.refrendos.strQueryPagosDetalles.Remove(intStrIndex2).Insert(intStrIndex2, ";").Replace("||", ",");
            this.ctrlRefrendos.refrendos.strQueryBoletaEmpenoEstatus = String.Format(this.ctrlRefrendos.refrendos.strQueryBoletaEmpenoEstatus, strEstatusUpdate, strBoletasIds);

        }

        public void guardarDetallesRefrendos()
        {


            
            string pago_id = "@id";

            int countDetalles = dgvDetalles.Rows.Count;
            
            this.ctrlRefrendos.refrendos.strQueryPagosDetallesCobrosTipos = "INSERT INTO pagos_detalles_cobros_tipos(pago_id, renglon_detalle, renglon, cobro_tipo_id, tipo, importe, porcentaje) VALUES";
            this.ctrlRefrendos.refrendos.strQueryPagosDetalles = "INSERT INTO pagos_detalles(pago_id , renglon , boleta_empeno_id ,abono_capital , dias_intereses ,  fecha_autorizacion , usuario_autorizacion, motivo_autorizacion, porcentaje_promocion, dia_promocion, prenda_tipo_id , dias_minimo_promocion) VALUES";
            this.ctrlRefrendos.refrendos.strQueryCrearBoletaEmpeno = "INSERT INTO boletas_empeno(sucursal_id, caja_id, usuario_id , folio , fecha , vencimiento , boleta_inicial_id , boleta_refrendo_id , pago_refrendo_id , cliente_id , identificacion_tipo_id, prenda_tipo_id , prenda_subtipo_id , gramaje_id , kilataje , gramos , cantidad , numero_plazos , importe , importe_avaluo , descripcion , comentario , fotografia_imagen , estatus, fecha_creacion , usuario_creacion ) VALUES";
            this.ctrlRefrendos.refrendos.strQueryBoletaEmpenoEstatus = " UPDATE boletas_empeno " +
                                                                      " SET estatus = CASE " +
                                                                      " {0} " +
                                                                      " ELSE estatus END " +
                                                                      " WHERE boleta_empeno_id IN( {1} ); ";

            string strQueryPagosDetallesCobrosTipos = "";
            string strQueryPagosDetalles = "";
            string strQueryCrearBoletaEmpeno = "";

            int intReglonDetalles = 1;
            int intReglonDetallesCobrosTipo = 1;
            string strBoletasIds = "";
            string strEstatusUpdate = "";

            int i = 0;
            int j = 0;
            
            //Trame el folio id de la boleta            
            this.ctrlRefrendos.refrendos.intFolioIdBoleta = this.ctrlProcesos.procesos.intId;

            foreach (DataGridViewRow row in dgvDetalles.Rows)
            {

                if (Convert.ToBoolean(row.Cells["Sel"].Value.ToString()))
                {
                    strBoletasIds += row.Cells["boleta_empeno_id"].Value.ToString() + ", ";
                    strEstatusUpdate += " WHEN boleta_empeno_id = " + row.Cells["boleta_empeno_id"].Value.ToString() + " THEN 'LIQUIDADA' ";

                    this.ctrlBoletaEmpeno.Buscar(row.Cells["Boleta"].Value.ToString());

                    if (row.Cells["usuarioAutorizacion"].Value.ToString() == "")
                    {
                        row.Cells["usuarioAutorizacion"].Value = 0;
                    }


                    string strDiaPorcentaje = row.Cells["diaProcentaje"].Value.ToString();
                    string strDiaPromocion = "'" + row.Cells["diaPromocion"].Value.ToString() + "'";
                    string strPrendaTipoId = row.Cells["prendaTipoId"].Value.ToString();
                    string strDiaMinimoPromocion = row.Cells["diaMinimoPromocion"].Value.ToString();

                    if (strDiaPorcentaje != "" && int.Parse(row.Cells["usuarioAutorizacion"].Value.ToString()) > 0)
                    {
                        strDiaPorcentaje = "null";
                        strPrendaTipoId = "null";
                        strDiaMinimoPromocion = "null";
                        strDiaPromocion = "null";

                    }                    
                    else if(strDiaPorcentaje == "") 
                    {
                        strDiaPorcentaje = "null";
                        strPrendaTipoId = "null";
                        strDiaMinimoPromocion = "null";
                        strDiaPromocion = "null";
                    }

               



                    if (strDiaPorcentaje != "" && int.Parse(row.Cells["usuarioAutorizacion"].Value.ToString()) == 0)
                    {

                        strQueryPagosDetalles += "(@id ," +
                            " " + intReglonDetalles + ", " +
                            " " + row.Cells["boleta_empeno_id"].Value.ToString() + " , " +                                                        
                            " " + row.Cells["AbonoCap"].Value.ToString().Replace("$", "").Replace(",", "") + " , " +
                            " " + row.Cells["Día"].Value.ToString().Replace("$", "").Replace(",", "") + " , " +                            
                            " null , " +
                            " null ," +
                            " null ," +
                             " " + strDiaPorcentaje + " , " +
                             " " + strDiaPromocion + " ,  " +
                             " " + strPrendaTipoId + " ,  " +
                            " " + strDiaMinimoPromocion + ") || ";
                    }
                    else
                    {


                        strQueryPagosDetalles += "(@id ," +
                           " " + intReglonDetalles + ", " +
                           " " + row.Cells["boleta_empeno_id"].Value.ToString() + " , " +
                           " " + row.Cells["AbonoCap"].Value.ToString().Replace("$", "").Replace(",", "") + " , " +
                           " " + row.Cells["Día"].Value.ToString().Replace("$", "").Replace(",", "") + " , " +                           
                           " '" + Convert.ToDateTime(row.Cells["fechaAutorizacion"].Value.ToString()).ToString("yyyy-MM-dd HH:mm:ss") + "' , " +
                           " " + row.Cells["usuarioAutorizacion"].Value.ToString() + " , "+
                           " '" + row.Cells["motivoAutorizacion"].Value.ToString() + "' , " +
                            " " + strDiaPorcentaje + " , " +
                             " " + strDiaPromocion + " ,  " +
                             " " + strPrendaTipoId + " ,  " +
                            " " + strDiaMinimoPromocion + ") || ";
                    }


                    strQueryCrearBoletaEmpeno += "(" +
                       " " + this.clsView.login.intSucursalId + " , " +
                       " " + this.clsView.login.caja.intId + " , " +
                       " " + this.clsView.login.usuario.intId + " , " +
                       "  '@folio"+j++ +"', " +
                       " '" + this.ctrlRefrendos.refrendos.dtFechaPago.ToString("yyyy-MM-dd HH:mm:ss") + "' , " +
                       " '" + dtpFecha.Value.AddMonths(int.Parse(this.ctrlBoletaEmpeno.boletasEmpeno.sucursalPlazo.ftPlazo.ToString())).ToString("yyyy-MM-dd HH:mm:ss") + "' , " +
                       " " + this.ctrlBoletaEmpeno.boletasEmpeno.intBoletaInicialId + " , " +
                       " " + this.ctrlBoletaEmpeno.boletasEmpeno.intId + " , " +
                       "  @id , " +
                       " " + this.ctrlBoletaEmpeno.boletasEmpeno.cliente.intId + " , " +
                       " " + this.ctrlBoletaEmpeno.boletasEmpeno.identificacionesTipo.intId + " , " +
                       " " + this.ctrlBoletaEmpeno.boletasEmpeno.prendaTipo.intId + " , " +
                       " " + this.ctrlBoletaEmpeno.boletasEmpeno.subTipoPrenda.intId + " , " +
                       " " + this.ctrlBoletaEmpeno.boletasEmpeno.gramaje.intId + " , " +
                       " " + this.ctrlBoletaEmpeno.boletasEmpeno.flKilataje + " , " +
                       " " + this.ctrlBoletaEmpeno.boletasEmpeno.flGramos + " , " +
                       " " + this.ctrlBoletaEmpeno.boletasEmpeno.intCantidad + " , " +
                       " " + this.ctrlBoletaEmpeno.boletasEmpeno.intPlazo + " , " +
                       " " + (this.ctrlBoletaEmpeno.boletasEmpeno.decPrestamo - decimal.Parse(row.Cells["AbonoCap"].Value.ToString().Replace("$", "").Replace(",", ""))).ToString() + " , " +
                       " " + (this.ctrlBoletaEmpeno.boletasEmpeno.decPrestamo + (this.ctrlBoletaEmpeno.boletasEmpeno.decPrestamo * (this.ctrlSucursalParametros.sucursalParametro.decAvaluo / 100))) + " , " +
                       " '" + this.ctrlBoletaEmpeno.boletasEmpeno.strDescripcion + "' , " +
                       " '" + this.ctrlBoletaEmpeno.boletasEmpeno.strComentario + "' , " +
                       " '" + this.ctrlBoletaEmpeno.boletasEmpeno.strFotografiaImagen + "' , " +
                       " '" + this.ctrlBoletaEmpeno.boletasEmpeno.strEstatus + "' , " +
                       " '" + this.ctrlRefrendos.refrendos.dtFecha.ToString("yyyy-MM-dd HH:mm:ss") + "' , " +
                       " " + this.clsView.login.usuario.intId + "  " +
                   ") ||";

                 

                    //iFolioBoleta++;
                    this.ctrlRefrendos.refrendos.intCountFolioBoleta++;

                    string[] cobrosTipoId = row.Cells["cobroTipoId"].Value.ToString().Split('|');
                    string[] porcentajeCobroTipo = row.Cells["porcentajeCobroTipo"].Value.ToString().Split('|');
                    string[] importeCobroTipo = row.Cells["importeCobroTipo"].Value.ToString().Split('|');

                    //
                    //
                    decimal decIntereses = decimal.Parse(row.Cells["Intereses"].Value.ToString().Replace("$", "").Replace(",", ""));
                    decimal decBonificar = decimal.Parse(row.Cells["Bonificar"].Value.ToString().Replace("$", "").Replace(",", ""));
                    decimal decPPIAplica = decimal.Parse(row.Cells["PPIAplica"].Value.ToString().Replace("$", "").Replace(",", ""));


                    foreach (string cobros in cobrosTipoId)
                    {

                        if (i != 3)
                        {
                            if (cobros != "|" && porcentajeCobroTipo[i] != "|" && importeCobroTipo[i] != "|")
                            {
                                strQueryPagosDetallesCobrosTipos += " (@id ," +
                                    " " + intReglonDetalles + " , " +
                                    " " + intReglonDetallesCobrosTipo + " , " +
                                    " " + cobros + " ," +
                                    " '" + "INTERESES" + "' , "+
                                    " " + importeCobroTipo[i] + " , " +
                                    " " + porcentajeCobroTipo[i] + ") ||";
                                
                                if (decBonificar > 0) 
                                {
                                    strQueryPagosDetallesCobrosTipos += " (@id ," +
                                    " " + intReglonDetalles + " , " +
                                    " " + intReglonDetallesCobrosTipo + " , " +
                                    " " + cobros + " ," +
                                    " '" + "BONIFICACION" + "' , " +
                                    " " + (decBonificar * decimal.Parse(porcentajeCobroTipo[i]) / decimal.Parse(row.Cells["porcentajeTotal"].Value.ToString())) + " , " +
                                    " " + porcentajeCobroTipo[i] + ") ||";
                                }
                                if (decPPIAplica > 0) 
                                {
                                    strQueryPagosDetallesCobrosTipos += " (@id ," +
                                        " " + intReglonDetalles + " , " +
                                        " " + intReglonDetallesCobrosTipo + " , " +
                                        " " + cobros + " ," +
                                        " '" + "PPI" + "' , " +
                                        " " + (decPPIAplica * decimal.Parse(porcentajeCobroTipo[i]) / decimal.Parse(row.Cells["porcentajeTotal"].Value.ToString())) + " , " +
                                        " " + porcentajeCobroTipo[i] + ") ||";
                                }
                                intReglonDetallesCobrosTipo++;
                                i++;
                            }
                        }
                        else
                        {
                            break;
                        }

                    }
                    i = 0;


                    intReglonDetallesCobrosTipo = 1;
                    intReglonDetalles++;
                }


            }


            this.ctrlRefrendos.refrendos.strQueryPagosDetallesCobrosTipos += strQueryPagosDetallesCobrosTipos;
            this.ctrlRefrendos.refrendos.strQueryPagosDetalles += strQueryPagosDetalles;

            int intStrIndex = this.ctrlRefrendos.refrendos.strQueryPagosDetallesCobrosTipos.LastIndexOf("||");
            int intStrIndex2 = this.ctrlRefrendos.refrendos.strQueryPagosDetalles.LastIndexOf("||");
            int intStrIndex3 = strBoletasIds.LastIndexOf(",");
            int intStrIndex4 = strQueryCrearBoletaEmpeno.LastIndexOf("||");

            strBoletasIds = strBoletasIds.Remove(intStrIndex3).Insert(intStrIndex3, " ");
            strQueryCrearBoletaEmpeno = strQueryCrearBoletaEmpeno.Remove(intStrIndex4).Insert(intStrIndex4, ";").Replace("||", ",");
            this.ctrlRefrendos.refrendos.strQueryPagosDetallesCobrosTipos = this.ctrlRefrendos.refrendos.strQueryPagosDetallesCobrosTipos.Remove(intStrIndex).Insert(intStrIndex, ";").Replace("||", ",");
            this.ctrlRefrendos.refrendos.strQueryPagosDetalles = this.ctrlRefrendos.refrendos.strQueryPagosDetalles.Remove(intStrIndex2).Insert(intStrIndex2, ";").Replace("||", ",");
            this.ctrlRefrendos.refrendos.strQueryBoletaEmpenoEstatus = String.Format(this.ctrlRefrendos.refrendos.strQueryBoletaEmpenoEstatus, strEstatusUpdate, strBoletasIds);
            this.ctrlRefrendos.refrendos.strQueryCrearBoletaEmpeno += strQueryCrearBoletaEmpeno;

        }


        public void guardarDetallesPPI()
        {
            string pago_id = "@id";

            int countDetalles = dgvDetalles.Rows.Count;

            this.ctrlRefrendos.refrendos.strQueryPagosDetallesCobrosTipos = "INSERT INTO pagos_detalles_cobros_tipos(pago_id, renglon_detalle, renglon, cobro_tipo_id, tipo, importe, porcentaje) VALUES";
            this.ctrlRefrendos.refrendos.strQueryPagosDetalles = "INSERT INTO pagos_detalles(pago_id, renglon,  boleta_empeno_id,  abono_capital,    semanas_ppi,  fecha_autorizacion, usuario_autorizacion,  motivo_autorizacion) VALUES";
            
            this.ctrlRefrendos.refrendos.strQueryBoletaEmpenoFechaVencimiento = " UPDATE boletas_empeno " +
                                                                      " SET vencimiento = (CASE " +
                                                                      " {0} " +
                                                                      " ELSE vencimiento END)," +
                                                                      " usuario_actualizacion =  {1}," +
                                                                      " fecha_actualizacion = '{2}' " +                                                                      
                                                                      " WHERE boleta_empeno_id IN( {3} ); ";

            string strQueryPagosDetallesCobrosTipos = "";
            string strQueryPagosDetalles = "";
            
            int intReglonDetalles = 1;
            int intReglonDetallesCobrosTipo = 1;
            string strBoletasIds = "";
            string strFechaVencimientoUpdate = "";
            
            int i = 0;
            int j = 0;

            //Trame el folio id de la boleta            
            this.ctrlRefrendos.refrendos.intFolioIdBoleta = this.ctrlProcesos.procesos.intId;

            foreach (DataGridViewRow row in dgvDetalles.Rows)
            {

                if (Convert.ToBoolean(row.Cells["Sel"].Value.ToString()))
                {
                    strBoletasIds += row.Cells["boleta_empeno_id"].Value.ToString() + ", ";
                    strFechaVencimientoUpdate += " WHEN boleta_empeno_id = " + row.Cells["boleta_empeno_id"].Value.ToString() + " THEN '" +
Convert.ToDateTime(row.Cells["Vence"].Value.ToString()).AddDays(int.Parse(txtSemanasPPI.Value.ToString()) * this.clsConstante.intDiasSemanas).ToString("yyyy-MM-dd HH:mm:ss") +"' ";

                    this.ctrlBoletaEmpeno.Buscar(row.Cells["Boleta"].Value.ToString());

                    if (row.Cells["usuarioAutorizacion"].Value.ToString() == "") 
                    { 
                        row.Cells["usuarioAutorizacion"].Value = 0; 
                    }

                    if (int.Parse(row.Cells["usuarioAutorizacion"].Value.ToString()) == 0)
                    {
                        
                        strQueryPagosDetalles += "(@id ," +
                            " " + intReglonDetalles + ", " +
                            " " + row.Cells["boleta_empeno_id"].Value.ToString() + " , " +
                            " " + row.Cells["AbonoCap"].Value.ToString().Replace("$", "").Replace(",", "") + " , " +                          
                            " " + txtSemanasPPI.Value + " , " +
                            " null , " +
                            " null ," +
                            " null "+ ") || ";
                    }
                    else
                    {                                                
                        strQueryPagosDetalles += "(@id ," +
                           " " + intReglonDetalles + ", " +
                           " " + row.Cells["boleta_empeno_id"].Value.ToString() + " , " +
                           " " + row.Cells["AbonoCap"].Value.ToString().Replace("$", "").Replace(",", "") + " , " +                           
                           " " + txtSemanasPPI.Value + " , " +
                           " '" + Convert.ToDateTime(row.Cells["fechaAutorizacion"].Value.ToString()).ToString("yyyy-MM-dd HH:mm:ss") + "' , " +
                           " " + row.Cells["usuarioAutorizacion"].Value.ToString() + " , " +
                           " '" + row.Cells["motivoAutorizacion"].Value.ToString() + "'  " +") || ";
                    }

                    //iFolioBoleta++;
                    this.ctrlRefrendos.refrendos.intCountFolioBoleta++;

                    string[] cobrosTipoId = row.Cells["cobroTipoId"].Value.ToString().Split('|');
                    string[] porcentajeCobroTipo = row.Cells["porcentajeCobroTipo"].Value.ToString().Split('|');
                    string[] importeCobroTipo = row.Cells["importeCobroTipo"].Value.ToString().Split('|');

                    //                    
                    decimal decIntereses = decimal.Parse(row.Cells["Intereses"].Value.ToString().Replace("$", "").Replace(",", ""));
                    decimal decBonificar = decimal.Parse(row.Cells["Bonificar"].Value.ToString().Replace("$", "").Replace(",", ""));
                    decimal decPPIAplica = decimal.Parse(row.Cells["PPIAplica"].Value.ToString().Replace("$", "").Replace(",", ""));


                    foreach (string cobros in cobrosTipoId)
                    {

                        if (i != 3)
                        {
                            if (cobros != "|" && porcentajeCobroTipo[i] != "|" && importeCobroTipo[i] != "|")
                            {
                                
                                if (decBonificar > 0)
                                {
                                    strQueryPagosDetallesCobrosTipos += " (@id ," +
                                    " " + intReglonDetalles + " , " +
                                    " " + intReglonDetallesCobrosTipo + " , " +
                                    " " + cobros + " ," +
                                    " '" + "BONIFICACION" + "' , " +
                                    " " + (decBonificar * decimal.Parse(porcentajeCobroTipo[i]) / decimal.Parse(row.Cells["porcentajeTotal"].Value.ToString())) + " , " +
                                    " " + porcentajeCobroTipo[i] + ") ||";
                                }
                                if (decPPIAplica > 0)
                                {
                                    strQueryPagosDetallesCobrosTipos += " (@id ," +
                                        " " + intReglonDetalles + " , " +
                                        " " + intReglonDetallesCobrosTipo + " , " +
                                        " " + cobros + " ," +
                                        " '" + "PPI" + "' , " +
                                        " " + (decPPIAplica * decimal.Parse(porcentajeCobroTipo[i]) / decimal.Parse(row.Cells["porcentajeTotal"].Value.ToString())) + " , " +
                                        " " + porcentajeCobroTipo[i] + ") ||";
                                }
                                intReglonDetallesCobrosTipo++;
                                i++;
                            }
                        }
                        else
                        {
                            break;
                        }

                    }
                    i = 0;


                    intReglonDetallesCobrosTipo = 1;
                    intReglonDetalles++;
                }


            }


            this.ctrlRefrendos.refrendos.strQueryPagosDetallesCobrosTipos += strQueryPagosDetallesCobrosTipos;
            this.ctrlRefrendos.refrendos.strQueryPagosDetalles += strQueryPagosDetalles;

            int intStrIndex = this.ctrlRefrendos.refrendos.strQueryPagosDetallesCobrosTipos.LastIndexOf("||");
            int intStrIndex2 = this.ctrlRefrendos.refrendos.strQueryPagosDetalles.LastIndexOf("||");
            int intStrIndex3 = strBoletasIds.LastIndexOf(",");
            

            strBoletasIds = strBoletasIds.Remove(intStrIndex3).Insert(intStrIndex3, " ");
            
            this.ctrlRefrendos.refrendos.strQueryPagosDetallesCobrosTipos = this.ctrlRefrendos.refrendos.strQueryPagosDetallesCobrosTipos.Remove(intStrIndex).Insert(intStrIndex, ";").Replace("||", ",");
            this.ctrlRefrendos.refrendos.strQueryPagosDetalles = this.ctrlRefrendos.refrendos.strQueryPagosDetalles.Remove(intStrIndex2).Insert(intStrIndex2, ";").Replace("||", ",");
            this.ctrlRefrendos.refrendos.strQueryBoletaEmpenoFechaVencimiento = String.Format(this.ctrlRefrendos.refrendos.strQueryBoletaEmpenoFechaVencimiento, strFechaVencimientoUpdate, this.clsView.login.usuario.intId, this.ctrlRefrendos.refrendos.dtFecha.ToString("yyyy-MM-dd HH:mm:ss"), strBoletasIds);            

        }

        private void btnVerCancelacion_Click(object sender, EventArgs e)
        {
            frmMotivosCancelacion frmMotivos = new frmMotivosCancelacion();
            frmMotivos.strTipo = "V";
            frmMotivos.strMotivo = this.ctrlRefrendos.refrendos.strMotivosCancelacion + " \r\n\r\n\r\n Usuario cancelación: " + this.ctrlRefrendos.refrendos.strUsuarioEliminacion+ "\r\n  Fecha: " + this.ctrlRefrendos.refrendos.strFechaEliminacion;
            frmMotivos.ShowDialog();       
        }

        private void dtpFecha_ValueChanged(object sender, EventArgs e)
        {
            //if (!(dgvDetalles.Rows.Count > 0)) return;
            if (this.ctrlRefrendos.refrendos.intId > 0 || (this.ctrlRefrendos.refrendos.intId == 0 && this.bolNuevo == false)) return;
            decimal decPrestamo = 0;
            decimal decPorcentajeInteres = 0;
            decimal decPorcentajeAlmacenaje = 0;
            decimal decPorcentajeManejo = 0;

            decimal decImporteInteres = 0;
            decimal decImporteAlmacenaje = 0;
            decimal decImporteManejo = 0;
            decimal decImporteInteresPPI = 0;
            decimal decImporteAlmacenajePPI = 0;
            decimal decImporteManejoPPI = 0;
            decimal decTotalIntereses = 0;
            decimal decTotalPPI = 0;

            string strImporteCobrosTipo = "";
            decimal decSubTotal = 0;
            decimal decAbonoCapital = 0;
            decimal decCapital = 0;
            decimal decBonificar = 0;
            decimal decPPI = 0;

            int intDias = 0;
            int intDiasMinimoPromocino = 0;


            this.decAcumCapital = 0;
            this.decAcumIntereses = 0;
            this.decAcumBonificaciones = 0;
            this.decAcumSubtotal = 0;
            this.decAcumAbonoCap = 0;
            this.decAcumPPI = 0;
            this.decAcumPagar = 0;

            int i = 0;
            string strDiaProm = "";
            if (rbtRefrendo.Checked) 
            {
                strDiaProm = this.regresarDiaFecha();
                this.ctrlSucursalesPromocion.BuscarPromocionDia(this.clsView.login.intSucursalId, strDiaProm);
            }

            //decimal decPrestamo = 0;
            foreach (DataGridViewRow row in dgvDetalles.Rows)
            {

                row.Cells["Bonificar"].Value = this.clsView.convertMoneda("0");
                row.Cells["fechaAutorizacion"].Value = "";
                row.Cells["usuarioAutorizacion"].Value = "";
                row.Cells["motivoAutorizacion"].Value = "";
                //Calcular dias de diferencia entre dos fechas
                int intTotalDias = int.Parse((dtpFecha.Value - Convert.ToDateTime(row.Cells["Fecha"].Value)).TotalDays.ToString());
                //Asignar el numero de dias minimo para el cobro de intreses
                int intDiasCobroMinimo = int.Parse(row.Cells["diasMinimo"].Value.ToString());
                //Verificar si la diferencias de dias se entuentra dentro de los dias minimo
                int intDiasInteres = (intTotalDias > intDiasCobroMinimo) ? (intDiasCobroMinimo + (intTotalDias - intDiasCobroMinimo)) : intDiasCobroMinimo;

                row.Cells["Día"].Value = intDiasInteres;

                string[] cobrosTipoId = row.Cells["cobroTipoId"].Value.ToString().Split('|');
                string[] porcentajeCobroTipo = row.Cells["porcentajeCobroTipo"].Value.ToString().Split('|');
                string[] importeCobroTipo = row.Cells["importeCobroTipo"].Value.ToString().Split('|');
                string[] nombreCobroTipo = row.Cells["cobrosTipoNombre"].Value.ToString().Split('|');

                foreach (string row2 in cobrosTipoId)
                {
                        if (cobrosTipoId.GetLength(0) == i) break;
                        //Dependiendo de tipo de cobros asignar valores
                        if (nombreCobroTipo[i] == "INTERES")
                        {
                        decPorcentajeInteres = decimal.Parse(porcentajeCobroTipo[i]);                          
                        }
                        else if (nombreCobroTipo[i] == "ALMACENAJE")
                        {
                        decPorcentajeAlmacenaje = decimal.Parse(porcentajeCobroTipo[i]);
                        }
                        else if (nombreCobroTipo[i] == "MANEJO")
                        {
                        decPorcentajeManejo = decimal.Parse(porcentajeCobroTipo[i]);
                        }

                        i++;
                }

                i = 0;

                if (intDiasCobroMinimo > 0)
                {
                    decCapital = decimal.Parse(row.Cells["Capital"].Value.ToString().Replace("$","").Replace(",",""));
                    /*Calcular importe de interes por cada tipo de cobro*/
                    if (decPorcentajeInteres > 0)
                    {
                        decImporteInteres = (((decCapital * decPorcentajeInteres) / 100) * intDiasInteres);
                        decImporteInteresPPI = (((decCapital * decPorcentajeInteres) / 100) * this.clsConstante.intDiasSemanas);
                        strImporteCobrosTipo += decImporteInteres.ToString() + "|";
                    }

                    if (decPorcentajeAlmacenaje > 0)
                    {
                        decImporteAlmacenaje = (((decCapital * decPorcentajeAlmacenaje) / 100) * intDiasInteres);
                        decImporteAlmacenajePPI = (((decCapital * decPorcentajeAlmacenaje) / 100) * this.clsConstante.intDiasSemanas);
                        strImporteCobrosTipo += decImporteAlmacenaje.ToString() + "|";
                    }

                    if (decPorcentajeManejo > 0)
                    {
                        decImporteManejo = (((decCapital * decPorcentajeManejo) / 100) * intDiasInteres);
                        decImporteManejoPPI = (((decCapital * decPorcentajeManejo) / 100) * this.clsConstante.intDiasSemanas);
                        strImporteCobrosTipo += decImporteManejo.ToString() + "|";
                    }
                    
                    decBonificar = decimal.Parse(row.Cells["Bonificar"].Value.ToString().Replace("$", "").Replace(",", ""));
                    decPPI = decimal.Parse(row.Cells["PPIAplica"].Value.ToString().Replace("$", "").Replace(",", ""));
                    /*Calcular el total de intereses*/
                    decTotalIntereses = decImporteInteres + decImporteAlmacenaje + decImporteManejo;

                    /*Calcular el total de PPI*/
                    decTotalPPI = decImporteInteresPPI + decImporteAlmacenajePPI + decImporteManejoPPI;
                    
                    decAbonoCapital = decimal.Parse(row.Cells["AbonoCap"].Value.ToString().Replace("$", "").Replace(",", ""));


                    row.Cells["importeCobroTipo"].Value = strImporteCobrosTipo;
                    row.Cells["Intereses"].Value = this.clsView.convertMoneda(decTotalIntereses.ToString());
                    

                    if (rbtRefrendo.Checked) 
                    {

                        decBonificar = 0;

                        if (this.ctrlSucursalesPromocion.bolResultado)
                        {
                            intDias = int.Parse(row.Cells["Día"].Value.ToString());
                            
                            foreach (DataRow rowProm in this.ctrlSucursalesPromocion.objDataSet.Tables[0].Rows)
                            {

                                intDiasMinimoPromocino = int.Parse(rowProm["dias_minimo"].ToString());
                                if (rowProm["prenda_tipo_id"].ToString() == row.Cells["prendaTipoId"].Value.ToString() && intDias >= intDiasMinimoPromocino)
                                {
                                    row.Cells["diaProcentaje"].Value = rowProm["porcentaje"].ToString();
                                    row.Cells["diaPromocion"].Value = strDiaProm;
                                    row.Cells["diaMinimoPromocion"].Value = intDiasMinimoPromocino;
                                    decBonificar = (decTotalIntereses * decimal.Parse(rowProm["porcentaje"].ToString()) / 100);
                                    break;
                                }
                               
                            }
                        }
                        else 
                        {
                            row.Cells["diaProcentaje"].Value = "";
                            row.Cells["diaPromocion"].Value = "";                           
                        }

                      
                        row.Cells["Bonificar"].Value = this.clsView.convertMoneda(decBonificar.ToString());
                    }


                    decSubTotal = decCapital + decTotalIntereses - decBonificar;
                    row.Cells["Subtotal"].Value = this.clsView.convertMoneda(decSubTotal.ToString());

                    if (Convert.ToBoolean(row.Cells["Sel"].Value))
                    {

                        

                        if (rbtLiquidacion.Checked)
                        {
                            row.Cells["Pagar"].Value = this.clsView.convertMoneda((decCapital + decTotalIntereses - decBonificar - decPPI).ToString());
                        }
                        else if (rbtRefrendo.Checked)
                        {
                            row.Cells["Pagar"].Value = this.clsView.convertMoneda((decTotalIntereses - decBonificar + decAbonoCapital - decPPI).ToString());
                        }
                        else if (rbtPPI.Checked)
                        {
                            row.Cells["Pagar"].Value = this.clsView.convertMoneda((decPPI - decBonificar).ToString());

                        }


                        strImporteCobrosTipo = "";
                        this.decAcumCapital += decCapital;
                        this.decAcumIntereses += decTotalIntereses;
                        this.decAcumBonificaciones += decBonificar;
                        this.decAcumSubtotal += decSubTotal;
                        this.decAcumAbonoCap += decAbonoCapital;
                        this.decAcumPPI += decPPI;
                        this.decAcumPagar += decimal.Parse(row.Cells["Pagar"].Value.ToString().Replace("$","").Replace(",",""));

                    }


                    strImporteCobrosTipo = "";



                }
            }

            txtAcumCapital.Text = this.clsView.convertMoneda(this.decAcumCapital.ToString());
            txtAcumIntereses.Text = this.clsView.convertMoneda(this.decAcumIntereses.ToString());
            txtAcumBonificaciones.Text = this.clsView.convertMoneda(this.decAcumBonificaciones.ToString());
            txtAcumSubTotal.Text = this.clsView.convertMoneda(this.decAcumSubtotal.ToString());
            txtAcumAbonoCap.Text = this.clsView.convertMoneda(this.decAcumAbonoCap.ToString());
            txtAcumPPI.Text = this.clsView.convertMoneda(this.decAcumPPI.ToString());
            txtAcumPagar.Text = this.clsView.convertMoneda(this.decAcumPagar.ToString());            
            txtCobroTotal.Text = txtAcumPagar.Text;
            calcularCambio();
        }


        public void buscarPagosDetalles(int intPagoId) 
        {
            this.ctrlRefrendos.BuscarPagosDetalles(intPagoId);
            decimal decCapital = 0;
            decimal decTotalIntereses = 0;
            decimal decBonificar = 0;
            decimal decAbonoCapital = 0;
            decimal decPPI = 0;


            decimal decSubTotal = 0;
            decimal decPagar = 0;
            decimal decIntereses = 0;

            string strSemMes = "";

            foreach (DataRow row in this.ctrlRefrendos.objDataSetSecundario.Tables[0].Rows)
            {

                decCapital = decimal.Parse(row["importe"].ToString());
                decTotalIntereses = decimal.Parse(row["intereses"].ToString());
                decBonificar = decimal.Parse(row["bonificaciones"].ToString());
                decAbonoCapital = decimal.Parse(row["abono_capital"].ToString());
                decPPI = decimal.Parse(row["intereses_ppi"].ToString());

                decSubTotal = (decCapital + decTotalIntereses) - decBonificar;
                if (rbtLiquidacion.Checked)
                {
                    decPagar = decSubTotal - decimal.Parse(row["intereses_ppi"].ToString()); 
                    decIntereses = decimal.Parse(row["intereses"].ToString());
                }
                else if (rbtRefrendo.Checked)
                {
                    decPagar = (decTotalIntereses - decBonificar) + decAbonoCapital - decimal.Parse(row["intereses_ppi"].ToString());
                    decIntereses = decimal.Parse(row["intereses"].ToString());
                } 
                else if (rbtPPI.Checked) 
                {
                    decIntereses = decimal.Parse(row["intereses_ppi"].ToString());
                    decCapital = 0;
                    decSubTotal = decIntereses - decBonificar;
                    decPagar = decSubTotal;
                }

                if (this.ctrlRefrendos.refrendos.strEstatus == this.ctrlRefrendos.refrendos.dicEstatus["LIQUIDACION INTERNA"])
                {
                    strSemMes = row["meses_intereses"].ToString();
                    dgvDetalles.Columns[17].HeaderText = "Mes";
                }
                else 
                {
                    strSemMes = row["semanas_ppi"].ToString();
                    dgvDetalles.Columns[17].HeaderText = "Sem";
                }

                dgvDetalles.Rows.Add(
                            "",//diaMinimoPromocion
                           "",//porcentajeTotal
                            "",//diaPromocion
                            "",//diaProcentaje
                            "",//cobrosTipoNombre
                            "",//diasMinimo
                           "",//prendaTipoId
                           "",//cobroTipoId
                           "",// porcentajeCobroTipo
                           "",//importeCobroTipo
                           "",//importeCobroTipoBonificacion
                           "",//fechaAutorizacion
                           "",// usuarioAutorizacion
                           "",//motivoAutorizacion
                           "",//boleta_empeno_id
                            row["boleta_folio"],//Boleta
                            row["fecha"],//Fecha
                            row["fecha_vencimiento"],//Vence
                            strSemMes,//Sem || MES
                            row["dias_intereses"],//Día
                            this.clsView.convertMoneda(decCapital.ToString()),//Capital
                            this.clsView.convertMoneda(decIntereses.ToString()),//Intereses
                            this.clsView.convertMoneda(row["bonificaciones"].ToString()),//Bonificar
                            this.clsView.convertMoneda(this.clsView.convertMoneda(decSubTotal.ToString())),//Subtotal
                            true,//    Sel
                            this.clsView.convertMoneda(row["abono_capital"].ToString()),//AbonoCap
                            this.clsView.convertMoneda(row["intereses_ppi"].ToString()),//PPIAplica
                            this.clsView.convertMoneda(this.clsView.convertMoneda(decPagar.ToString()))//  Pagar
                           );


                this.decAcumCapital += decCapital;
                this.decAcumIntereses += decIntereses;
                this.decAcumBonificaciones += decBonificar;
                this.decAcumSubtotal += decSubTotal;
                this.decAcumAbonoCap += decAbonoCapital;
                this.decAcumPPI += decPPI;
                this.decAcumPagar += decPagar;
            }
            txtSemanasPPI.Enabled = false;

            txtAcumCapital.Text = this.clsView.convertMoneda(this.decAcumCapital.ToString());
            txtAcumIntereses.Text = this.clsView.convertMoneda(this.decAcumIntereses.ToString());
            txtAcumBonificaciones.Text = this.clsView.convertMoneda(this.decAcumBonificaciones.ToString());
            txtAcumSubTotal.Text = this.clsView.convertMoneda(this.decAcumSubtotal.ToString());
            txtAcumAbonoCap.Text = this.clsView.convertMoneda(this.decAcumAbonoCap.ToString());
            txtAcumPPI.Text = this.clsView.convertMoneda(this.decAcumPPI.ToString());
            txtAcumPagar.Text = this.clsView.convertMoneda(this.decAcumPagar.ToString());
            

        }


        public void inicializarTotales() 
        {

            this.decAcumCapital = 0;
            this.decAcumIntereses = 0;
            this.decAcumBonificaciones = 0;
            this.decAcumSubtotal = 0;
            this.decAcumAbonoCap = 0;
            this.decAcumPPI = 0;
            this.decAcumPagar = 0;

            txtAcumCapital.Text = this.clsView.convertMoneda("0");
            txtAcumIntereses.Text = this.clsView.convertMoneda("0");
            txtAcumBonificaciones.Text = this.clsView.convertMoneda("0");
            txtAcumSubTotal.Text = this.clsView.convertMoneda("0");
            txtAcumAbonoCap.Text = this.clsView.convertMoneda("0");
            txtAcumPPI.Text = this.clsView.convertMoneda("0");
            txtAcumPagar.Text = this.clsView.convertMoneda("0");
            txtCobroTotal.Text = this.clsView.convertMoneda("0");
            txtEfectivo.Text = this.clsView.convertMoneda("0");
            txtCambio.Text = this.clsView.convertMoneda("0");
        }

        private void btnCierra_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tsAcciones_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
        private void txtEfectivo_Enter(object sender, EventArgs e)
        {
            txtEfectivo.Text = "";
        }

        private void txtEfectivo_Leave(object sender, EventArgs e)
        {
            if (txtEfectivo.Text == "" || decimal.Parse(txtCobroTotal.Text.Replace("$","").Replace(",","")) > decimal.Parse(txtEfectivo.Text.Replace("$", "").Replace(",", ""))) 
            {
                txtEfectivo.Text = this.clsView.convertMoneda("0");
                txtCambio.Text = this.clsView.convertMoneda("0");
                return;
            }

            calcularCambio();
        }

        private void txtSemanasPPI_Leave(object sender, EventArgs e)
        {

        
        }

        public void calcularCambio() 
        {
            if (txtEfectivo.Text == "") return;

            decimal decEffectivo = decimal.Parse(txtEfectivo.Text.Replace("$", "").Replace(",", ""));
            if (decEffectivo == 0) 
            {
                txtCambio.Text = this.clsView.convertMoneda("0");                
                return;
            }
            if (txtCobroTotal.Text == "") 
            {
                txtCobroTotal.Text = this.clsView.convertMoneda("0");
            }
            txtEfectivo.Text = this.clsView.convertMoneda(txtEfectivo.Text);
            txtCambio.Text = this.clsView.convertMoneda( (decEffectivo - decimal.Parse(txtCobroTotal.Text.Replace("$", "").Replace(",", ""))).ToString());
        }

        private void txtEfectivo_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.clsView.soloNumValiDec(sender, e);
        }

        private void txtSemanasPPI_ValueChanged(object sender, EventArgs e)
        {

            if (this.ctrlRefrendos.refrendos.intId > 0) return;
            decimal decIntereses = 0;
            decimal decPorcentajeTotal = 0;
            decimal decPPI = 0;
            decimal decBonificacion = 0;
            decimal decPagar = 0;
            decimal decPrestamo = 0;
            this.decAcumPPI = 0;
            this.decAcumPagar = 0;


            int i = 0;
            foreach (DataGridViewRow row in dgvDetalles.Rows)
            {
               
                decPrestamo = decimal.Parse(row.Cells["Capital"].Value.ToString().Replace("$", "").Replace("," ,""));
                decPorcentajeTotal = decimal.Parse(row.Cells["porcentajeTotal"].Value.ToString());
                decPagar = decimal.Parse(row.Cells["Pagar"].Value.ToString().Replace("$", "").Replace(",", ""));
                decPPI = ((decPrestamo * decPorcentajeTotal) / 100) * (txtSemanasPPI.Value * this.clsConstante.intDiasSemanas);
                decBonificacion = decimal.Parse(row.Cells["Bonificar"].Value.ToString().Replace("$", "").Replace(",", ""));
                row.Cells["PPIAplica"].Value = this.clsView.convertMoneda(decPPI.ToString());
                row.Cells["Sel"].Value = false;
                row.Cells["Pagar"].Value = this.clsView.convertMoneda("0");
                
                /*if (Convert.ToBoolean(row.Cells["Sel"].Value.ToString()))
                {
                    decPagar = decPagar - decBonificacion;
                    this.decAcumPPI = decPPI;
                    this.decAcumPagar += decPagar;
                }*/
            }

            txtEfectivo.Text = this.clsView.convertMoneda("0");
            txtCambio.Text = this.clsView.convertMoneda("0");
            txtCobroTotal.Text = this.clsView.convertMoneda("0");


            txtAcumPPI.Text = this.clsView.convertMoneda(this.decAcumPPI.ToString());
            txtAcumPagar.Text = this.clsView.convertMoneda(this.decAcumPagar.ToString());
            calcularCambio();
        }

        private void dgvDetalles_CellValidated(object sender, DataGridViewCellEventArgs e)
        {

           
                
        }

        private void txtBonificacion_TextChanged(object sender, EventArgs e)
        {

        }

        private void dgvDetalles_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
           
        }

        private void dgvDetalles_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            string header = dgvDetalles.Columns[dgvDetalles.CurrentCell.ColumnIndex].HeaderText;

            if (header == "Sel")
            {
                decimal decIntereses = decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["Intereses"].Value.ToString().Replace("$", "").Replace(",", ""));
                decimal decAplicar = decimal.Parse(dgvDetalles.Rows[e.RowIndex].Cells["PPIAplica"].Value.ToString().Replace("$", "").Replace(",", ""));

                if (decIntereses < decAplicar)
                {
                    this.clsView.mensajeErrorValiForm2("El pago parcial de intereses es mayor a los intereses a la fecha, debe refrendar la boleta");
                    dgvDetalles.Rows[e.RowIndex].Cells["Sel"].Value = false;
                    var cell = (sender as DataGridView).CurrentCell;
                    cell.Value = false;

                }

            }
        }

        /*Funcion que se utilizar para regresar el dia de la fecha selecionada*/
        private string regresarDiaFecha()
        {
            string strDia = "";
            if (dtpFecha.Value.DayOfWeek.ToString() == "Monday")
            {
                strDia = "LUNES";
            }
            else if (dtpFecha.Value.DayOfWeek.ToString() == "Tuesday")
            {
                strDia = "MARTES";
            }
            else if (dtpFecha.Value.DayOfWeek.ToString() == "Wednesday")
            {
                strDia = "MIERCOLES";
            }
            else if (dtpFecha.Value.DayOfWeek.ToString() == "Thursday")
            {
                strDia = "JUEVES";
            }
            else if (dtpFecha.Value.DayOfWeek.ToString() == "Friday")
            {
                strDia = "VIERNES";
            }
            else if (dtpFecha.Value.DayOfWeek.ToString() == "Saturday")
            {
                strDia = "SABADO";
            }
            else if (dtpFecha.Value.DayOfWeek.ToString() == "Sunday")
            {
                strDia = "DOMINGO";
            }

            return strDia;
        }

        private void txtCambio_TextChanged(object sender, EventArgs e)
        {

        }

        private void dgvDetalles_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void lkbCliente_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (txtClienteId.Text == "") return;
            Controller.clsCtrlBoletaEmpeno ctrlBoletaEmpeno = new Controller.clsCtrlBoletaEmpeno();
            Controller.clsCtrlClientes ctrlClientes = new Controller.clsCtrlClientes();
            ctrlClientes.Buscar(txtCodigoCliente.Text);
            ctrlBoletaEmpeno.PaginacionCliente(int.Parse(txtClienteId.Text));
            if (ctrlBoletaEmpeno.mdlBoletaEmpeno.bolResultado)
            {
                ctrlBoletaEmpeno.CargarFrmBusquedaRangoFechaBoleta(ctrlClientes.cliente);
            }
        }

        private void txtCodigoCliente_Layout(object sender, LayoutEventArgs e)
        {

        }
    }
}
